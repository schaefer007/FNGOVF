function adjustTaxRates(taxGroup) {
	txgrpDOM = document.getElementById('TAXGROUP');
	taxGroup = txgrpDOM.options[txgrpDOM.selectedIndex].value;
	openWait('Adjusting Tax Rates based on modified Tax Group . . .');
	gtrDOM = document.getElementById('GOODSTAXRATE');
	if(gtrDOM.options) {
		currentValue = gtrDOM.options[gtrDOM.selectedIndex].value;
	} else {
		currentValue = gtrDOM.value;
	}
	URL = 'runAJAXRequest.php?AJAXFunction=buildFormField&table=vendorform&column=GOODSTAXRATE&sessionObj=vendorform&INITIALIZE=true&AsAssoc=true&value=' + currentValue + '&TAXGROUP=' + taxGroup;
	makeAjaxRequest(URL,updateGoodsTaxRate);
	strDOM = document.getElementById('SERVICESTAXRATE');
	if(strDOM.options) {
		currentValue = strDOM.options[gtrDOM.selectedIndex].value;
	} else {
		currentValue = strDOM.value;
	}
	currentValue = document.getElementById('SERVICESTAXRATE').value;
	URL = 'runAJAXRequest.php?AJAXFunction=buildFormField&table=vendorform&column=SERVICESTAXRATE&sessionObj=vendorform&INITIALIZE=true&AsAssoc=true&value=' + currentValue + '&TAXGROUP=' + taxGroup;
	makeAjaxRequest(URL,updateServicesTaxRate);
	setTimeout(checkAJAXRequests,1000);
	//document.getElementById('FOB').focus();
	return;
}
function updateGoodsTaxRate(htmlData) {
	trEnd = '</tr>';
	rtnData = unescape(htmlData).toString().replace('<html>','').replace('<body>','');
	rtnData = rtnData.substring(rtnData.indexOf('<td'));
	rtnData = rtnData.replace(trEnd,"");
	//document.getElementById('row_GOODSTAXRATE').innerHTML = rtnData;
	$("#row_GOODSTAXRATE").html(rtnData);
	if (document.getElementById('TAXGROUP').selectedIndex && document.getElementById('TAXGROUP').selectedIndex > 0) {
		document.getElementById('GOODSTAXRATE').disabled = false;
	} else {
		document.getElementById('GOODSTAXRATE').disabled = true;
	}
	AJAXRequests--;
	//document.getElementById('FOB').focus();
}
function updateServicesTaxRate(htmlData) {
	trEnd = '</tr>';
	rtnData = unescape(htmlData).toString().replace('<html>','').replace('<body>','');
	rtnData = rtnData.substring(rtnData.indexOf('<td'));
	rtnData = rtnData.replace(trEnd,"");
	//document.getElementById('row_SERVICESTAXRATE').innerHTML = rtnData;
	$("#row_SERVICESTAXRATE").html(rtnData);
	if (document.getElementById('TAXGROUP').selectedIndex && document.getElementById('TAXGROUP').selectedIndex > 0) {
		document.getElementById('SERVICESTAXRATE').disabled = false;
	} else {
		document.getElementById('SERVICESTAXRATE').disabled = true;
	}
	AJAXRequests--;
	//document.getElementById('FOB').focus();
}
function checkCorpVendMast() {
	cvmValue = $("#CORPVENDMAST").val;
	var sendAlert = false;
	if (cvmValue && cvmValue == 'NEW') {
		for (i=0;i<terminalStates.length;i++) {
			buttonObjs = document.getElementsByTagName("INPUT");
			for (b=0;b<buttonObjs.length;b++) {
				if (buttonObjs[b].type == 'button' && buttonObjs[b].value == terminalStates[i]) {
					buttonObjs[b].disabled = true;
					setErrorBG('row_CORPVENDMAST');
					document.getElementById('CVMSelectorEdit').focus();
					sendAlert = true;
				}
			}
		}
	if (sendAlert) {
		alert('Corporate Vendor Master must be set before Final Approval');
	}
	}
}
function checkSaveAllowed() {
	allowsave = false;
	buttonObjs = document.getElementsByTagName("INPUT");
	for (i=0;i<buttonObjs.length;i++) {
		if(buttonObjs[i].type == 'button' && buttonObjs[i].value != 'Save' && buttonObjs[i].disabled != true) {
			var allowSave = true;
		}
	}
	if (allowSave != true) {
		$('#saveButton').attr('disabled','disabled');
		$('#saveButton').css('visibility','hidden');
	}
}
function getCommodityLevels(level) {
	comCat1 = document.getElementById('COMMODITYCAT');
	comCat2 = document.getElementById('COMMODITYCAT2');
	comCat3 = document.getElementById('COMMODITYCAT3');
	comCat4 = document.getElementById('COMMODITYCAT4');
	if (comCat1 && level > 1) {
		commodity1 = comCat1[comCat1.selectedIndex].value;
	} else {
		commodity1 = '';
	}
	if (comCat2 && level > 2) {
		commodity2 = comCat2[comCat2.selectedIndex].value;
	} else {
		commodity2 = '';
	}
	if (comCat3 && level > 3) {
		commodity3 = comCat3[comCat3.selectedIndex].value;
	} else {
		commodity3 = '';
	}
	commodity4 = '';
	for (i=level;i<=4;i++) {
		if (i == 2) {
			URL = 'runAJAXRequest.php?AJAXFunction=buildCommodityField&table=vendorform&column=COMMODITYCAT2&sessionObj=vendorform&INITIALIZE=true&value=' + commodity2 + '&COMMODITYCAT=' + commodity1 + '&COMMODITYCAT2=' + commodity2 + '&COMMODITYCAT3=' + commodity3 + '&COMMODITYCAT4=' + commodity4 + '&level=' + i;
			//makeAjaxRequest(URL,updateCommodityCat2);
			//$.get(
			//		URL,
			//		function(responseText){
			//			updateCommodityCat2(responseText);
			//		},
			//		"html"
			//);
			  jQuery.ajax({
				  url: URL,
				  success: function(responseText){
					  updateCommodityCat2(responseText);
				  },
				  async: false
			  })
		}
		if (i == 3) {
			URL = 'runAJAXRequest.php?AJAXFunction=buildCommodityField&table=vendorform&column=COMMODITYCAT3&sessionObj=vendorform&INITIALIZE=true&value=' + commodity3 + '&COMMODITYCAT=' + commodity1 + '&COMMODITYCAT2=' + commodity2 + '&COMMODITYCAT3=' + commodity3 + '&COMMODITYCAT4=' + commodity4 + '&level=' + i;
			//makeAjaxRequest(URL,updateCommodityCat3);
			//$.get(
			//		URL,
			//		function(responseText){
			//			updateCommodityCat3(responseText);
			//		},
			//		"html"
			//);
			  jQuery.ajax({
				  url: URL,
				  success: function(responseText){
					  updateCommodityCat3(responseText);
				  },
				  async: false
			  })
		}
		if (i == 4) {
			URL = 'runAJAXRequest.php?AJAXFunction=buildCommodityField&table=vendorform&column=COMMODITYCAT4&sessionObj=vendorform&INITIALIZE=true&value=' + commodity2 + '&COMMODITYCAT=' + commodity1 + '&COMMODITYCAT2=' + commodity2 + '&COMMODITYCAT3=' + commodity3 + '&COMMODITYCAT4=' + commodity4 + '&level=' + i;
			//makeAjaxRequest(URL,updateCommodityCat4);
			//$.get(
			//		URL,
			//		function(responseText){
			//			updateCommodityCat4(responseText);
			//		},
			//		"html"
			//);
			  jQuery.ajax({
				  url: URL,
				  success: function(responseText){
					  updateCommodityCat4(responseText);
				  },
				  async: false
			  })
		}
	}
	setTimeout(checkAJAXRequests,1000);
	if (level == 2) {
		$('#COMMODITYCAT2').focus();
	}
	if (level == 3) {
		$('#COMMODITYCAT3').focus();
	}
	if (level == 4) {
		$('#COMMODITYDAT4').focus();
	}
	return;
}
function updateCommodityCat2(htmlData) {
	rtnData = unescape(htmlData).toString().replace('<html>','').replace('<body>','');
	rtnData = rtnData.substring(rtnData.indexOf('<td'));
	rtnData = rtnData.replace('</tr>','');
	$("#row_COMMODITYCAT2").html(rtnData);
	$("#COMMODITYCAT2").focus();
}
function updateCommodityCat3(htmlData) {
	rtnData = unescape(htmlData).toString().replace('<html>','').replace('<body>','');
	rtnData = rtnData.substring(rtnData.indexOf('<td'));
	rtnData = rtnData.replace('</tr>','');
	$("#row_COMMODITYCAT3").html(rtnData);
	$("#COMMODITYCAT3").focus();
}
function updateCommodityCat4(htmlData) {
	rtnData = unescape(htmlData).toString().replace('<html>','').replace('<body>','');
	rtnData = rtnData.substring(rtnData.indexOf('<td'));
	rtnData = rtnData.replace('</tr>','');
	$("#row_COMMODITYCAT4").html(rtnData);
	$("#COMMODITYCAT4").focus();
}
function setCountry() {
	stateObj = document.getElementById('VENDORSTATE');
	if (stateObj) {
		stateVal = stateObj[stateObj.selectedIndex].text;
		stateArr = stateVal.split(', ');
		if (stateArr.length > 1) {
			country = stateArr[stateArr.length-1];
			countryObj = document.getElementById('VENDORCOUNTRY');
			if (countryObj) {
				for (i=0;i<countryObj.options.length;i++) {
					if (countryObj.options[i].value == country) {
						countryObj.selectedIndex = i;
					}
				}
			}
		}
	setRequiredByCountry();
	}
}
function setRequiredByCountry() {
	var ddObj = document.getElementById('VENDORCOUNTRY');
	if (ddObj && ddObj.selectedIndex) {
	  if (ddObj[ddObj.selectedIndex].text.substring(ddObj[ddObj.selectedIndex].text.indexOf('-')+2).toUpperCase() == 'UNITED STATES') {
		var htmlData = document.getElementById('row_FEDERALID').innerHTML;
		    htmlData = htmlData.replace("&nbsp;&nbsp;&nbsp;","*&nbsp;");
		//document.getElementById('row_FEDERALID').innerHTML = htmlData;
			$("#row_FEDERALID").html(htmlData);
		//var htmlData2 = document.getElementById('row_GSTLICENSE').innerHTML;
    	//	htmlData2 = htmlData2.replace("*&nbsp;","&nbsp;&nbsp;&nbsp;");
		//	$("#row_GSTLICENSE").html(htmlData2);
	} else {
		var htmlData = document.getElementById('row_FEDERALID').innerHTML;
		    htmlData = htmlData.replace("*&nbsp;","&nbsp;&nbsp;&nbsp;");
		//document.getElementById('row_FEDERALID').innerHTML = htmlData;
			$("#row_FEDERALID").html(htmlData);
		if (ddObj && ddObj[ddObj.selectedIndex].text.substring(ddObj[ddObj.selectedIndex].text.indexOf('-')+2).toUpperCase() == 'CANADA') {
			//var htmlData2 = document.getElementById('row_GSTLICENSE').innerHTML;
    		//	htmlData2 = htmlData2.replace("&nbsp;&nbsp;&nbsp;","*&nbsp;");
			//	$("#row_GSTLICENSE").html(htmlData2);
		} else {
			//var htmlData2 = document.getElementById('row_GSTLICENSE').innerHTML;
			//	htmlData2 = htmlData2.replace("*&nbsp;","&nbsp;&nbsp;&nbsp;");
		    //  $("#row_GSTLICENSE").html(htmlData2);
		}
	}
  }
	setState();
}
function setState() {
	countryObj = document.getElementById('VENDORCOUNTRY');
	if (countryObj) {
		stateObj = document.getElementById('VENDORSTATE');
		if (stateObj) {
			if (stateObj.selectedIndex) {
				currentValue = stateObj[stateObj.selectedIndex].value;
			} else {
				currentValue = '';
			}
			if (countryObj.selectedIndex) {
				currentCountry = countryObj[countryObj.selectedIndex].value;
			} else {
				currentCountry = '';
			}
				stateObj.selectedIndex = 0;
				URL = 'runAJAXRequest.php?AJAXFunction=buildFormField&table=vendorform&column=VENDORSTATE&sessionObj=vendorform&INITIALIZE=true&AsAssoc=true&value=' + currentValue + '&VENDORCOUNTRY=' + currentCountry;
				makeAjaxRequest(URL,updateVendorState);
				setTimeout(checkAJAXRequests,1000);
		}
	}
}
function updateVendorState(htmlData) {
	trEnd = '</tr>';
	rtnData = unescape(htmlData).toString().replace('<html>','').replace('<body>','');
	rtnData = rtnData.substring(rtnData.indexOf('<td'));
	rtnData = rtnData.replace(trEnd,"");
	$("#row_VENDORSTATE").html(rtnData);
	AJAXRequests--;
	var optionCount = $("#VENDORSTATE option").length;
	if (optionCount == 0) {
		$("#VENDORSTATE").val("");
		$("#VENDORSTATE").attr("readonly",true);
	}
}

function setDisableAutoComplete() {
	var testVal = $("#CVMSelector").val();
	if (testVal && testVal.length > 0) {
		$("#CVMSelector").prop('disabled', true);
		$("#CVMSelectorEdit").css("visibility","visible");
		$("#CVMSelectorDelete").css("visibility","visible");
	}
	var testVal = $("#REMITTOSelector").val();
	if (testVal && testVal.length > 0) {
		$("#REMITTOSelector").prop('disabled', true);
		$("#REMITTOSelectorEdit").css("visibility","visible");
		$("#REMITTOSelectorDelete").css("visibility","visible");
	}
	var testVal = $("#OLDVENDORSelector").val();
	if (testVal && testVal.length > 0) {
		$("#OLDVENDORSelector").prop('disabled', true);
		$("#OLDVENDORSelectorEdit").css("visibility","visible");
		$("#OLDVENDORSelectorDelete").css("visibility","visible");
	}
}
function getCorpVendorDefaults(action) {
	var corpVendor = '';
	var corpVendObj = document.getElementById('CORPVENDMAST');
	if (corpVendObj) {
		corpVendor = corpVendObj.value;
	}
	if (corpVendor.trim() != '' && corpVendor.trim() != 'NEW') {
		var schema = '';
		var schemaObj = document.getElementById('formSchema');
		if (schemaObj) {
			schema = schemaObj.value;
		}
		var termsDesc = '';
		var minority  = '';
		URL = 'runAJAXRequest.php?AJAXFunction=getCorpVendorDefaults&corpVendor='+corpVendor+'&schema='+schema+'&callback=?';
		$.getJSON(URL, function(data) {
			$.each(data, function(i, val){
				termsDesc = val.termDesc;
				minority  = val.minority;
			});
			if (termsDesc != '') {
				$("#corpTerms").html("Terms should be: "+termsDesc);
				$("#row_CORPTERMS").css("visibility","visible");
			} else {
				setDefaultTerms();
			//	$("#row_CORPTERMS").css("visibility","hidden");
			}
		//	if (minority != '') {
			if (minority == 'Y') {
				$('[name=MINORITYFLAG]').prop('disabled',false);
				$('[name=MINORITYFLAG][value='+minority+']').prop('checked',true);
				$('[name=MINORITYFLAG]').prop('disabled',true);
				if (!document.getElementById('#h_MINORITYFLAG')) {
				  $('#vendorRequest').append('<input type="hidden" name="MINORITYFLAG" id="h_MINORITYFLAG" value="' + minority + '">');
				}
				$('#h_MINORITYFLAG').val(minority);
			} else {
			   if (action && action != 'edit') {
				$('[name=MINORITYFLAG]').prop('checked',false);
			   }
				$('[name=MINORITYFLAG]').prop('disabled',false);
				if (document.getElementById('h_MINORITYFLAG')) {
				   $('#h_MINORITYFLAG').remove();
				}
			}
		});
	} else {
		setDefaultTerms();
		$('[name=D_Minority]').prop('disabled',false);
	}
}
function goToHome() {
	goToView('home');
}
function setDefaultTerms() {
	$("#corpTerms").html('Company Policy: Net 45');
}
function openCpyDialogue() {
		openMyPopUp('cpyVendorPrompt');
}