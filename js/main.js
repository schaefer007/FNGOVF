String.prototype.trim = function() { return this.replace(/^\s+|\s+$/g, ''); };
var AJAXRequests = 0;

function setErrorBG(wkrow) {
	x = document.getElementById(wkrow);
	x.bgColor = "#f25659";
}
function qzhighlight(wkrow) {
  x = document.getElementById(wkrow);
  x.bgColor = "#E4ECED";
  }

function goToView(View) {
	theForm = document.getElementById('navForm');
	theForm.view.value = View;
	theForm.submit();
}

function buildFormArrays() {
	if (confirm('Building the Forms Arrays may take several minutes.  Continue?')) {
		goToView('buildFormArrays');
	}
}
function qznormal(wkrow) {
  x = document.getElementById(wkrow);
  x.bgColor = "#FFFFFF";
  }

function goToEdit(theForm, Name) {
	theForm.NAME.value = Name;
	theForm.submit();
}
 
function goToForm(theForm, ID) {
	openWait('Please wait while your form loads . . .');
	theForm.instanceID.value = ID;
	theForm.view.value = 'editForm';
	theForm.submit();
}
//Determine browser and version. 

function Browser() {

  var ua, s, i;

  this.isIE    = false;
  this.isNS    = false;
  this.version = null;

  ua = navigator.userAgent;

  s = "MSIE";
  if ((i = ua.indexOf(s)) >= 0) {
    this.isIE = true;
    this.version = parseFloat(ua.substr(i + s.length));
    return;
  }

  s = "Netscape6/";
  if ((i = ua.indexOf(s)) >= 0) {
    this.isNS = true;
    this.version = parseFloat(ua.substr(i + s.length));
    return;
  }

  // Treat any other "Gecko" browser as NS 6.1.

  s = "Gecko";
  if ((i = ua.indexOf(s)) >= 0) {
    this.isNS = true;
    this.version = 6.1;
    return;
  }
}

var browser = new Browser();

// Global object to hold drag information.

var dragObj = new Object();
dragObj.zIndex = 0;

function dragStart(event, id) {

  var el;
  var x, y;

  // If an element id was given, find it. Otherwise use the element being
  // clicked on.

  if (id)
    dragObj.elNode = document.getElementById(id);
  else {
    if (browser.isIE)
      dragObj.elNode = window.event.srcElement;
    if (browser.isNS)
      dragObj.elNode = event.target;

    // If this is a text node, use its parent element.

    if (dragObj.elNode.nodeType == 3)
      dragObj.elNode = dragObj.elNode.parentNode;
  }

  // Get cursor position with respect to the page.

  if (browser.isIE) {
    x = window.event.clientX + document.documentElement.scrollLeft
      + document.body.scrollLeft;
    y = window.event.clientY + document.documentElement.scrollTop
      + document.body.scrollTop;
  }
  if (browser.isNS) {
    x = event.clientX + window.scrollX;
    y = event.clientY + window.scrollY;
  }

  // Save starting positions of cursor and element.

  dragObj.cursorStartX = x;
  dragObj.cursorStartY = y;
  dragObj.elStartLeft = dragObj.elNode.offsetLeft;
  dragObj.elStartTop = dragObj.elNode.offsetTop;
  //dragObj.elStartLeft  = parseInt(dragObj.elNode.style.left, 10);
  //dragObj.elStartTop   = parseInt(dragObj.elNode.style.top,  10);

  if (isNaN(dragObj.elStartLeft)) dragObj.elStartLeft = 0;
  if (isNaN(dragObj.elStartTop))  dragObj.elStartTop  = 0;

  // Update element's z-index.

  dragObj.elNode.style.zIndex = ++dragObj.zIndex;
  dragObj.elNode.style.cursor = "move";
  // Capture mousemove and mouseup events on the page.

  if (browser.isIE) {
    document.attachEvent("onmousemove", dragGo);
    document.attachEvent("onmouseup",   dragStop);
    window.event.cancelBubble = true;
    window.event.returnValue = false;
  }
  if (browser.isNS) {
    document.addEventListener("mousemove", dragGo,   true);
    document.addEventListener("mouseup",   dragStop, true);
    event.preventDefault();
  }
}

function dragGo(event) {

  var x, y;

  // Get cursor position with respect to the page.

  if (browser.isIE) {
    x = window.event.clientX + document.documentElement.scrollLeft
      + document.body.scrollLeft;
    y = window.event.clientY + document.documentElement.scrollTop
      + document.body.scrollTop;
  }
  if (browser.isNS) {
    x = event.clientX + window.scrollX;
    y = event.clientY + window.scrollY;
  }

  // Move drag element by the same amount the cursor has moved.

  dragObj.elNode.style.left = (dragObj.elStartLeft + x - dragObj.cursorStartX) + "px";
  dragObj.elNode.style.top  = (dragObj.elStartTop  + y - dragObj.cursorStartY) + "px";

  if (browser.isIE) {
    window.event.cancelBubble = true;
    window.event.returnValue = false;
  }
  if (browser.isNS)
    event.preventDefault();
}

function dragStop(event) {

  // Clear the drag element global.

  dragObj.elNode.style.cursor = "auto";
  dragObj.elNode = null;

  // Stop capturing mousemove and mouseup events.

  if (browser.isIE) {
    document.detachEvent("onmousemove", dragGo);
    document.detachEvent("onmouseup",   dragStop);
  }
  if (browser.isNS) {
    document.removeEventListener("mousemove", dragGo,   true);
    document.removeEventListener("mouseup",   dragStop, true);
  }
}

function limitSize(field,size) {
	if (field.value.length > size) {
		field.value = field.value.substring(0,size);
	} 
}
function openWait(message) {
	if (!document.getElementById('waitBox')) {
		var newDiv = document.createElement('div');
		newDiv.setAttribute("id",'waitBox');
		newDiv.innerHTML = '<div id="waitBox">' + message + '</div>';
		document.body.appendChild(newDiv);
	} else {
		document.getElementById('waitBox').innerHTML = message;
	}
	$("#waitBox").modal();
}
function closeWait() {
	$.modal.close();
	$("#waitBox").hide();
}

function makeAjaxRequest(URL,callBack) {
	if (URL=="")
	  {
	  return;
	  }
	if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  var xmlhttp=new XMLHttpRequest();
	  }
	else
	  {// code for IE6, IE5
	  var xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
	xmlhttp.onreadystatechange=function()
	  {
	  if (xmlhttp.readyState==4 && xmlhttp.status==200)
	    {
		response = escape(xmlhttp.responseText.toString());
	    callBack(response);
	    }
	  }
	var httpTimeOut = setTimeout(function(){xmlhttp.abort(); closeWait();},5000);
	xmlhttp.open("GET",URL+"&AJAX=true",true);
	AJAXRequests++;
	xmlhttp.send();
}
function checkAJAXRequests() {
	if (AJAXRequests == 0) {
		closeWait();
	} else {
		setTimeout(checkAJAXRequests,1000);
	}
}
function isNumberKey(evt) {
	var charCode = (evt.which) ? evt.which : evt.keyCode
	if (charCode > 32 && ((charCode < 48 && charCode != 40 && charCode != 41 && charCode != 45) || charCode > 57)) {
		return false;
	} else {
		return true;
	}
}
function checkAll(checkObj, checkFormID) {
	if ($("#checkall",$('#'+checkFormID)).attr('checked') == 'checked') {
		var isChecked = true;
	} else {
		var isChecked = false;
	}
	$(':checkbox',$('#'+checkFormID)).prop('checked', isChecked);
}
function closeMyPopUp(popUpElement) {
	$("#"+popUpElement).css("display","none");
}
function openMyPopUp(popUpElement) {
	$("#"+popUpElement).css("display","block");
}
