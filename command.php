<?php
$pathArray = explode('/',__FILE__);
foreach ($pathArray as $pathElement) {
    if (strtolower($pathElement) != 'command.php') {
        $filePath .= $pathElement.'/';
    } else {
        break;
    }
}
chdir($filePath);
//chdir('/www/zendsvr6/htdocs/FNGForms/');
include('includes/allowedCommands.php');
function loadClass($class) {
	if (file_exists('classes/class.'.$class.'.php')) {
		include ('classes/class.'.$class.'.php');
	}
}
spl_autoload_register('loadClass');
require ('includes/c_config.php');
set_include_path(get_include_path().PATH_SEPARATOR.$config['applicationRoot'].'/plm/classes/'.PATH_SEPARATOR.$config['applicationRoot'].'/plm/');
include_once('classes/class.workFlowInstance.php');
require_once('classes/class.db.php');
require_once('classes/class.base.php');
include_once('includes/vendorMap.php');
//require_once('includes/vendorArrays.php');
define('APPLICATION', 'flexNgateWF');
define('intWFPROCESS_ID_VENDOR_REQUEST', 1);
$thisFormEmail = $config['defaultEmail'];
// Get connection for use throughout
$connect = new Db();
$cxn['*LOCAL'] = $connect->Get_Db_Connection();
foreach ($argv as $argument) {
	//error_log('argument: '.$argument,0);
	$argArray = explode('=',$argument);
	switch ($argArray[0]) {
		case '--AJAXFunction': $_REQUEST['AJAXFunction'] = $argArray[1];
							   $interface = $argArray[1];
		 				 	break;
		case '--AJAX'  		 : $_REQUEST['AJAX'] = $argArray[1];
						    break;
	}
}
// locale settings
setlocale(LC_MONETARY, 'en_US');
error_log('running command interface');
$interface .= '.php';
//error_log("Interface is $interface",0);
if (isset($interface) and in_array($interface, $allowedCommands) and file_exists('AJAX/'.$interface)) {
	require ('AJAX/'.$interface);
}

?>