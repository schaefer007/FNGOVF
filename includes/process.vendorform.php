<?php
error_log('process.vendorform beginning');
define('FORMTYPE', "vendorform");
require_once('classes/class.vendorform.php');
require_once('classes/class.vend.php');
//set_include_path(get_include_path().PATH_SEPARATOR.'/www/zendsvr/htdocs/FNG/plm/classes/'.PATH_SEPARATOR.'/www/zendsvr/htdocs/FNG/plm/');
require_once('classes/class.formrequest.php');
require_once('ProcessObject.class.php');
require_once('ProcessInstance.class.php');
$errorText = array();
if (isset($_SESSION[APPLICATION]['lastInstanceID'])) {
	unset($_SESSION[APPLICATION]['lastInstanceID']);
}
if (isset($_REQUEST['action'])) {
	$action = $_REQUEST['action'];
} else {
	$action = '';
}
if (isset($_REQUEST['VENDORACTION'])) {
	$vendorAction = $_REQUEST['VENDORACTION'];
}
if (!isset($_REQUEST['reqID']) or $_REQUEST['reqID'] == null) {
	$_REQUEST['reqID'] = $_SESSION['intUser'];
}
$facilityData = array();
foreach ($_SESSION[APPLICATION]['userFacilities'] as $facilityInfo) {
	$facilityKey = $facilityInfo['DBase'].':'.$facilityInfo['Plant'].':'.$facilityInfo['Company'];
	$facilityData[$facilityKey] = $facilityInfo['Name'];
}
if (isset($_REQUEST['FACILITY'])) {
	$facilityName = $facilityData[$_REQUEST['FACILITY']];
	$facilityArray = explode(':',$_REQUEST['FACILITY']);
	if (trim($facilityArray[0]) == 'CMSDATMX') {
		$dbSchema = 'CMSDAT';
	} else {
		$dbSchema = $facilityArray[0];
	}
	$dbCode = $facilityArray[0];
	$plant = $facilityArray[1];
	$company = $facilityArray[2];
	$fwCompId = new fwcompid();
	$fwCompId->select(array('FWFUT02'),array('WHERE'=>array('FWDATC'=>$facilityArray[0],'FWPLTC'=>$facilityArray[1],'FWGLCO'=>$facilityArray[2])));
	if ($fwCompRow = $fwCompId->getnext()) {
	    $dbName = $fwCompRow['FWFUT02'];
	}
}
$vendorArrayFile = 'va_'.str_replace(array(' ',':'),array('_','-'),$_SESSION[APPLICATION]['FACILITY']).'.php';
if (file_exists('includes/'.$vendorArrayFile)) {
	include_once('includes/'.$vendorArrayFile);
}
$formRequest = new formrequest();
if (!isset($_SESSION[APPLICATION]['vendorform'])) {
	$_SESSION[APPLICATION]['vendorform'] = new vendorform();
}
if (isset($_REQUEST['old_VFID']) and $_REQUEST['old_VFID'] != '') {
	$key = $_REQUEST['old_VFID'];
} else {
	if (isset($_REQUEST['VFID']) and $_REQUEST['VFID'] != "") {
		$key = $_REQUEST['VFID'];
	}
}
if (isset($key)) {
	// check to see if form is still active.  If form exists and is not New or Active it cannot be updated.
	$formRequest->select(array('"processingInstanceID"','"status"'),array('WHERE'=>array('formID'=>$key)));
	if ($formRequestRow = $formRequest->getnext()) {
		if ($formRequestRow['status'] != 'New' and $formRequestRow['status'] != 'Active' and $formRequestRow['status'] != 'Re-Submit') {
			$errorText[] = 'Request has been completed and can no longer be modified.';
		}
	}
$_REQUEST['modified'] = 'N';
$_REQUEST['directorAuthRequired'] = 'N';
if (count($errorText) == 0) {
	$_SESSION[APPLICATION]['vendorform']->select(null,array('WHERE'=>array('VFID'=>$key)));
	if ($vendorRow = $_SESSION[APPLICATION]['vendorform']->getnext()) {
		foreach($vendorRow as $vkey => $value) {
			if(isset($_REQUEST) && trim($_REQUEST[$vkey]) != trim($value)) {
				$_REQUEST['modified'] = 'Y';
				break;
			}
		}
		if ($action != 'Delete') {
			$action = 'Edit';
			if (!isset($_REQUEST['old_VFID']) or $_REQUEST['old_VFID'] == '') {
				$_REQUEST['old_VFID'] = $_REQUEST['VFID'];
			}
		}
	} else {
		if ($action != 'Delete') {
			$action = 'Add';
		} else {
			base::write_log("Vendor Form not found.  Delete not possible.","E");
			$errorText[] = 'Vendor Form not found.  Delete not possible.';
		}
	}
}
} else {
	if ($action != 'Delete') {
		$action = 'Add';
	}
}
if (isset($_REQUEST['selectVendor'])) {
	$selectVendor = $_REQUEST['selectVendor'];
} else {
	$selectVendor = '';
}
if (isset($_REQUEST['VENDORCODE'])) {
	$vendorCode = $_REQUEST['VENDORCODE'];
} else {
	$vendorCode = '';
}
if ($selectVendor == 'NEW' or $vendorAction == 'Add') {
	$vendorCode = strtoupper($vendorCode);
	$vendTable = new vend();
	$vendTable->select(array('BTVEND'),array('WHERE'=>array('BTVEND'=>$vendorCode)));
	if ($vendTable->getnext()) {
		base::write_log("Vendor code provided already exists but NEW was specified","E");
		$errorText[] = 'Vendor code provided already exists but NEW was specified.';
	}
	$formTable = new vendorform();
	$formTable->select(array('VENDORCODE'),array('WHERE'=>array('VENDORCODE'=>$vendorCode,'STATUS'=>array('New','Active'))));
	if ($formRow = $formTable->getnext()) {
	    base::write_log('Vendor code provided already exists on another active form but NEW was specified');
	    $errorText[] = 'Vendor code provided already exists on another active form but NEW was specified';
	}
}

if (count($errorText) == 0) {
	switch ($action) {
		case 'Add'      :   $vendorSeq = new Db();
							$vendorSeq->connect($cxm);
							$_REQUEST['VFID'] = $vendorSeq->getNextSequence('"dbsystem"', 'vendorFormSeq');
							if (isset($vendorSeq)) {
								unset($vendorSeq);
							}
							if (isset($_REQUEST['CORPVENDMAST']) and !empty($_REQUEST['CORPVENDMAST'])) {
								$eftDb = new db();
								$eftDb->connect();
								$eftDb->execute("select DA0GMT from FNGDWS.VEND join FNGDWS.VENX on BTVEND = DA0VEND WHERE MFRESP = '".$_REQUEST['CORPVENDMAST']."' and DA0GMT = '2'");
								if ($eftDb->getNextAssoc()) {
									$_REQUEST['EFTREQUIRED'] = 'Y';
								}
								unset($eftDb);
							}
						 	if ($_SESSION[APPLICATION]['vendorform']->insert()) {
								// Checks for tooling and commodity users.
								if($_SESSION[APPLICATION]['vendorform']->needsDirectorApproval($_REQUEST['VFID'])) {
									$_REQUEST['directorAuthRequired'] = 'Y';
								}
						 		// Create workflow instance
						 		$biMember = false;
						 		foreach($_SESSION[APPLICATION]['userRoles'] as $uRoleArr) {
						 			if ($uRoleArr['intRoleID'] == vf_BITeamRole) {
						 				$biMember = true;
						 			}
						 		}
						 		if ($selectVendor == 'NEW' or !$biMember) {
						 			$formRequest->getProcessInstance()->setWFProcessID($_SESSION[APPLICATION]['requestedForm']);
						 		} else {
						 			$formRequest->getProcessInstance()->setWFProcessID(intWFPROCESS_ID_VENDOR_CHANGE);
						 		}						 		
						 		$formRequest->getProcessInstance()->getWFProcess()->load($formRequest->getProcessInstance()->getWFProcessID());
						 		$formRequest->getProcessInstance()->setCurrentProcessStateID($formRequest->getProcessInstance()->getWFProcess()->getStartingProcessStateID());
						 		$formRequest->getProcessInstance()->save();
						 		$formRequest->setProcessInstanceID($formRequest->getProcessInstance()->getProcessInstanceID());
						 		$_REQUEST['processingInstanceID'] = $formRequest->getProcessInstanceID();
						 		// create formrequest.
						 		$_REQUEST['facilityName'] = $facilityName;
						 		$_REQUEST['dbSchema'] = $dbSchema;
						 		//$dbName = 'ASI520PROD';
						 		if (isset($dbName)) {
						 		    error_log('retrieved dbName is '.$dbName);
						 		} else {
						 		    error_log('dbName not retrieved from fwcompid');
						 		}
						 		error_log('session dbName is '.$_SESSION[APPLICATION]['dbName']);
						 		$_REQUEST['dbName'] = trim($_SESSION[APPLICATION]['dbName']);
						 		$_REQUEST['plant'] = $plant;
						 		$_REQUEST['company'] = $company;
						 		$_REQUEST['formType'] = FORMTYPE;
						 		$_REQUEST['requestText'] = $_REQUEST['VENDORNAME'].":".$_REQUEST['REQREASON'];
						 		$_REQUEST['formID'] = $_REQUEST['VFID'];
						 		$_REQUEST['status'] = 'New';
						 		$_REQUEST['stsID'] = $_REQUEST['reqID'];
						 		$_REQUEST['reqTimeStamp'] = date('Y-m-d-H.i.s'.'.000000');
						 		$_REQUEST['stsTimeStamp'] = $_REQUEST['reqTimeStamp'];
						 		if ($formRequest->insert()) {
									base::write_log("Vendor Form Successfully Added.","S");
						 	 		$errorText[] = 'Vendor Form Successfully Added.';
						 	 		$view = 'editForm';
						 	 		$_REQUEST['view'] = 'editForm';
						 	 		$_REQUEST['instanceID'] = $_REQUEST['processingInstanceID'];
						 		} else {
									base::write_log("Vendor Form Add Failed","E");
						 			$errorText[] = 'Vendor FormAdd Failed.';
						 		}
							} else {
								base::write_log("Vendor Form Add Failed","E");
								$errorText[] = 'Vendor Form Add Failed.';
							}

							break;
		case 'Edit'		:	if (isset($_REQUEST['CORPVENDMAST']) and !empty($_REQUEST['CORPVENDMAST']) and $_REQUEST['CORPVENDMAST'] != 'NEW' and $_REQUEST['CORPVENDMAST'] != $vendorRow['CORPVENDMAST']) {
								$eftDb = new db();
								$eftDb->connect();
								$eftDb->execute("select DA0GMT from FNGDWS.VEND join FNGDWS.VENX on BTVEND = DA0VEND WHERE MFRESP = '".$_REQUEST['CORPVENDMAST']."' and DA0GMT = '2'");
								if ($eftDb->getNextAssoc()) {
									$_REQUEST['EFTREQUIRED'] = 'Y';
								}
								unset($eftDb);
							}
							if ($_SESSION[APPLICATION]['vendorform']->update()) {
								// Checks for tooling and commodity users.
								if($_SESSION[APPLICATION]['vendorform']->needsDirectorApproval($key)) {
									$_REQUEST['directorAuthRequired'] = 'Y';
								}
								$formRequest->select(array('"processingInstanceID"','"reqID"'),array('WHERE'=>array('formID'=>$_REQUEST['VFID'])));
								if ($formRow = $formRequest->getnext()) {
									$_REQUEST['processingInstanceID'] = $formRow['processingInstanceID'];
									$_REQUEST['stsID'] = $_REQUEST['REQID'];
									if (isset($formRow['reqID'])) {
										$_REQUEST['reqID'] = $formRow['reqID'];
									}
								//	$_REQUEST['status'] = 'Active';
									$_REQUEST['stsTimeStamp'] = date('Y-m-d-H.i.s'.'.000000');
									$_REQUEST['requestText'] = $_REQUEST['VENDORNAME'].":".$_REQUEST['REQREASON'];
									if ($formRequest->update()) {
										base::write_log("Vendor Form Successfully Updated","S");
										$errorText[] = 'Vendor Form Successfully Updated.';
										$view = 'editForm';
										$_REQUEST['view'] = 'editForm';
									} else {
										base::write_log("Update of Form Request failed","E");
										$errorText[] = 'Update of Form Request failed.';
									}
								} else {
								 base::write_log("Existing Form Request Not Found","E");
								 $errorText[] = 'Existing Form Request Not Found';
								}
							} else {
								base::write_log("Vendor Form Update Failed","E");
								$errorText[] = 'Vendor Form Update Failed.';
							}
							if ($vendorRow['COMMODITYCAT'] != $_REQUEST['COMMODITYCAT'] or
									$vendorRow['COMMODITYCAT2'] != $_REQUEST['COMMODITYCAT2'] or
									$vendorRow['COMMODITYCAT3'] != $_REQUEST['COMMODITYCAT3'] or
									$vendorRow['COMMODITYCAT4'] != $_REQUEST['COMMODITYCAT4']) {
								// The following code was modified to include a reflow function.
								// The reflow is a transition from the Director Approval State back
								//  to the BI Team pre-approval state.  It has no authorized users
								//  and is only used internally.  A flag is passed to indicate that
								//  authorization checks are not to occur since they would always
								//  fail due to the lack of authorization for this transition.
								$formRequest->loadForWorkflow($_REQUEST['processingInstanceID']);
								if ($_REQUEST['directorAuthRequired'] == 'Y') {
									$formRequest->resendNotices();
								} else {
									// If the commoditys have changed and director authorization is no longer required and
									// this workflow is at a custom condition step, then reflow the form back to the BI Team
									//  pre-approval step.
									$objProcessInstance = new ProcessInstance();
									$objProcessInstance->loadForDisplay($_REQUEST['processingInstanceID'], $formRequest);
									if ($objProcessInstance->getCurrentProcessState()->getCustomConditions()) {
										$objProcessInstance->workflowAction($formRequest, vf_reflowTransition, vf_DirectorRole, 'Automatic Reflow',false);
										$view = 'home';
									}
								}
									}
							
							break;
		case 'Delete'	:   if ($_SESSION[APPLICATION]['vendorform']->delete()) {
								$formRequest->select(array('"processingInstanceID"'),array('WHERE'=>array('formID'=>$_REQUEST['VFID'])));
								if ($formRow = $formRequest->getnext()) {
									$_REQUEST['processingInstanceID'] = $formRow['processingInstanceID'];
									$_REQUEST['status'] = 'Deleted';
									$_REQUEST['stsID'] = $_REQUEST['reqID'];
									$formRequest->update();
								}
								base::write_log("Vendor Form Successfully deleted.","S");
								$errorText[] = 'Vendor Form Successfully deleted.';
								$view = 'home';
								$_REQUEST['view'] = 'home';
								break;
		}
	}
	$objProcessInstance = new ProcessInstance();
	$objProcessInstance->loadForDisplay($_REQUEST['processingInstanceID'],$formRequest);

	// set request variables for intNextRoleID and strNextRole and strNextAction
	$_REQUEST['intNextRoleID'] = base::getNextRoleID($objProcessInstance);
	$_REQUEST['strNextRole'] = base::getNextRole($objProcessInstance);
	$_REQUEST['strNextAction'] = base::getNextAction($objProcessInstance);

	$formRequest->update();
}

?>