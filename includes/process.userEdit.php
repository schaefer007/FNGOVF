<?php 
require_once('classes/class.account.php');
require_once('classes/class.tbluser.php');

function updateTblUser($EmailAddress) {
	global $errorText;
	global $userDb;
	$tblUser = new tbluser();
	$tblUser->select(array('"intUserID"'),array('WHERE'=>array('strEmail'=>$EmailAddress)));
	if ($tblUserRow = $tblUser->getnext()) {
		$_REQUEST['intUserID'] = $tblUserRow['intUserID'];
	}
	$_REQUEST['strFirstName'] = $_REQUEST['FNAME'];
	$_REQUEST['strLastName']  = $_REQUEST['LNAME'];
	$_REQUEST['strEmail']     = $_REQUEST['EMAIL'];
	$_REQUEST['strDisplayName']= $_REQUEST['NICKNAME'];
	$_REQUEST['strLoginName'] = $_REQUEST['EMAIL'];
	if (isset($_REQUEST['PASSWORD1']) and !empty($_REQUEST['PASSWORD1'])) {
		$_REQUEST['strPassword'] = md5($_REQUEST['PASSWORD1']);
	} else {
		if (isset($_REQUEST['strPassword'])) {
			unset($_REQUEST['strPassword']);
		}
	}
	if (!isset($_REQUEST['intUserID'])) {
		$success = $tblUser->insert();
		if ($success) {
			$errorText[] = 'User successfully activated with a status of '.$_REQUEST['STATUS'];
		} else {
			$errorText[] = 'User activation with a Status of '.$_REQUEST['STATUS'].' failed.';
		}
	} else {
		$success = $tblUser->update();
		if (!$success) {
			$errorText[] = 'User update failed on table "tbluser".';
		}
	}
	return $success;
}

$errorText = array();
$userDb = new account();
if ($_REQUEST['ID#'] and $_REQUEST['ID#'] > 0) {
	$ID = $_REQUEST['ID#'];
} 
	if ($_REQUEST['action'] == 'Reset') {
		include('includes/process.pwdReset.php');
	} else {
		if ($_REQUEST['PASSWORD1']) {
			$Password1 = $_REQUEST['PASSWORD1']; 
		} else {
			$Password1 = "";
		}
		if ($_REQUEST['PASSWORD2']) {
			$Password2 = $_REQUEST['PASSWORD2'];
		} else {
			$Password2 = "";
		}
		if ($Password1 or $Password2) {
			if ($Password1 == $Password2) {
				if (strlen($Password1) < 5) {
					$errorText[] = 'Password must contain between 5 and 10 characters';
				} else {
					$_REQUEST['PASSWORD'] = md5($Password1);
				}
			} else {
				$errorText[] = 'Passwords do not match.';
			}
		}
		$dupCount = 0;
		$userDb->select(array('ID#'),array('WHERE'=>array('EMAIL'=>$_REQUEST['EMAIL'])));
		while($chkRow = $userDb->getnext()) {
			if ($chkRow['ID#'] != $_REQUEST['ID#']) {
				$dupCount ++;
			}
		}
		if ($dupCount > 0) {
			$errorText[] = "A user already exists with that Email address. {$_REQUEST['action']} not processed.";	
		}
		
		    // If no errors update the user 
			$userDb->select(null,array('WHERE'=>array('ID#'=>$_REQUEST['ID#'])));
			if (count($errorText) == 0 and $userRow = $userDb->getnext()) {
				$_REQUEST['LASTUPDDT'] = date('Y-m-d-H.i.s.'.'000000');
				$_REQUEST['LASTUPDBY'] = $_SESSION['user'];
			switch ($_REQUEST['action']) {
				case 'Edit':  $userDb->update();
							  updateTblUser($userRow['EMAIL']);
				 			   break;
			}
			$_REQUEST['PASSWORD1'] = "";
			$_REQUEST['PASSWORD2'] = "";
		}
	}
?>