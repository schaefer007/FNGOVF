<?php 
require_once('classes/class.account.php');
require_once('classes/class.mail.php');
function getRandPWD($length) {
	$underscores = 2;
	
	$password = "";
	for ($i=0;$i<$length;$i++) {
		$c = mt_rand(1,7);
		switch ($c) {
			case ($c<-2):  $password .= mt_rand(0,9);
						   break;
			case ($c<=4):  $password .= chr(mt_rand(65,90));
						   break;
			case ($c<=6):  $password .= chr(mt_rand(97,122));
						   break;
			case 7		:  $len = strlen($password);
						   if ($underscores > 0 and $len > 0 and $len < ($length-1) and $password[$len - 1] != "_") {
						   	 $password .= "_";
						   	 $underscores --;
						   } else {
						   	 $i--;
						   	 continue;
						   }
						   break;
		}
	}
	return $password;
}
$account = new account();
$selected = false;
if (isset($_SESSION['loggedIn']) and $_SESSION['loggedIn'] and isset($_REQUEST['ID#'])) {
	$ID = $_REQUEST['ID#'];
	$account->select(null, array('WHERE'=>array('ID#'=>$ID)));
	if ($row = $account->getnext()) {
		if ($row['STATUS'] != 'D') {
	    	$selected = true;
		}
	}
}
if ((!isset($_SESSION['loggedIn']) or !$_SESSION['loggedIn']) and isset($_REQUEST['userEmail']) and trim($_REQUEST['userEmail']) != '') {
	$userEmail = $_REQUEST['userEmail'];
	$account->select(null, array('WHERE'=>array('EMAIL'=>$userEmail)));
	if ($row = $account->getnext()) {
		if ($row['STATUS'] == 'A') {
		  $_REQUEST['ID#'] = $row['ID#'];
		  $selected = true;
		}
	}
}
if ($selected) {
	$_REQUEST['PCHNGREQ'] = 'Y';
	$newPassword = getRandPWD(10);
	$_REQUEST['PASSWORD'] = md5($newPassword);
	$_REQUEST['LASTUPDDT'] = date('Y-m-d-H.i.s.'.'000000');
	$_REQUEST['LASTUPDBY'] = $_REQUEST['ID#'];
	$_REQUEST['STATUS'] = 'A';
	$_REQUEST['INVLOGIN'] = '0';
	$email = new email();
	$email->setType('HTML');
	$email->addRecipient(trim($row['EMAIL']));
	$email->addBCC('gslater@arbsol.com');
	$email->addSubject("Password Reset for ".trim($row['EMAIL']));
	$message = "<p class=\"contentTitle\">Password Reset</p>\n
		<p>Your new password is $newPassword.  This password will remain active for 24 hours.  You must sign in before that time has expired.  You will be required to change your password when you sign in.</p>\n
		<p style=\"clear:both;\">Visit the Flex-N-Gate Change Request Portal at: <a href=\"".$config['webURL']."\">".$config['webURL']."</a></p>";
	$email->addMessage($message);
	if ($email->send()) {
		$account->update();
		$errorText[] = 'Your password has been reset successfully.  You should receive an email shortly with your new password.  You must login in with that password within the next 24 hours or it becomes invalid.';
	} else {
		$errorText[] = 'A problem was encoutered while trying to send you your new password.  Your password has not been reset.';
	}
	unset($email);
} else {
	$errorText[] = 'Account not found.  Password Reset not performed.';
	if (!$_SESSION['loggedIn']) {
		$errorText[] = 'Please provide a valid email address before requesting your account password be reset.';
	}
}
if (!$_SESSION['loggedIn']) {
	$view = 'login';
	$_REQUEST['view'] = 'login';
}
?>