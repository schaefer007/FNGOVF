<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Flex-N-Gate</title>
 <link rel="stylesheet" type="text/css" href="styles/fngMain.css" />
 <link rel="stylesheet" type="text/css" href="js/chromatable/css/style.css" />
<script type="text/javascript" src="js/phoneMask.js"></script>
<script type="text/javascript" src="js/dFilter.js"></script>
<script type="text/javascript" src="js/jQuery.js"></script>
<script type="text/javascript" src="js/jquery.simplemodal-1.4.3.js"></script>
<script type="text/javascript" src="js/chromatable/jquery.chromatable.js"></script>
<script type="text/javascript" src="js/main.js"></script>
<!--[if IE]>
<style>
#divider {
background: url('http://172.29.3.109/fng/media/IEDropShadow.jpg') bottom repeat-x;
padding-bottom:3px;
}
</style>
<![endif]-->
</head>

<body>
<div id="brandWrapper">
	<div id="brandHead">
	    <img src="media/fngLogo.jpg"></img>
		<div class="clear"></div>
	 	<div id="welcomeRight">Forms Portal</div>
	</div>
</div>

<div class="clear"></div>
<div id="divider">
   <div id="dividercontent">
   </div>
</div>

<div id="contentWrapper">

	<div id="content">
