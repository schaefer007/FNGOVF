<?php 
require_once('classes/class.account.php');
require_once('classes/class.mail.php');
$errorText = array();
$userDb = new account();
if ($_REQUEST['ID#'] and $_REQUEST['ID#'] > 0) {
	$ID = $_REQUEST['ID#'];
} 
	if ($_REQUEST['PASSWORD1']) {
		$Password1 = $_REQUEST['PASSWORD1']; 
	} else {
		$Password1 = "";
	}
	if ($_REQUEST['PASSWORD2']) {
		$Password2 = $_REQUEST['PASSWORD2'];
	} else {
		$Password2 = "";
	}
	if ($Password1 or $Password2) {
		if ($Password1 == $Password2) {
			if (strlen($Password1) < 5) {
				$errorText[] = 'Password must contain between 5 and 10 characters';
			} else {
				$_REQUEST['PASSWORD'] = base::hashPassword($Password1);
			}
		} else {
			$errorText[] = 'Passwords do not match.';
		}
	}
	$dupCount = 0;
	$userDb->select(array('ID#'),array('WHERE'=>array('EMAIL'=>$_REQUEST['EMAIL'])));
	while($chkRow = $userDb->getnext()) {
		if (!isset($ID) or $chkRow['ID#'] != $ID) {
			$dupCount ++;
		}
	}
	if ($dupCount > 0) {
		$errorText[] = "A user already exists with that Email address. {$_REQUEST['action']} not processed.";
	}
	if (count($errorText) == 0) {
		$_REQUEST['LASTUPDDT'] = date('Y-m-d-H.i.s.'.'000000');
		$_REQUEST['LASTUPDBY'] = $_SESSION[APPLICATION]['user'];
		$_REQUEST['AUTHORITY'] = 'User';
		switch ($_REQUEST['action']) {
			case 'Edit':  $userDb->update();
			 			   break;
			case 'Add'	 : if (!isset($ID)) {
								$userSeq = new Db();
								$userSeq->connect($cxn['*LOCAL']);
								$_REQUEST['ID#'] = $userSeq->getNextSequence($config['schema'], 'userSeq');
								if (isset($userSeq)) {
									unset($userSeq);
								}
						   }
						   $_REQUEST['STATUS'] = 'P';
						   $_REQUEST['PCHNGREQ'] = 'N';
						   if ($userDb->insert()) {
						   	$errorText[] = 'You\'ve been successfully registered.<br> You\'ll receive an Email when your registration has been approved.';
						   	$email = new email();
						   	$email->setType('HTML');
						   	$email->addSender($config['defaultEmail']);
						   	$email->addRecipient($config['defaultEmail']);
						   	$email->addSubject('New User Registered');
						   	$message = file_get_contents('includes/emailHeader.php');
						   	$message = str_replace('"styles/','"'.$config['webURL'].'styles/',$message);
						   	$message = str_replace('"js/','"'.$config['webURL'].'js/',$message);
						   	$message = str_replace('"media/','"'.$config['webURL'].'media/',$message);
						   	$message .= "<p>User ".trim($_REQUEST['FNAME'])." ".trim($_REQUEST['LNAME'])." has just registered for access on the Flex-n-gate forms submission site. Please log in to the site at <a href=\"".$config['webURL']."\">Flex-n-gate Forms Portal</a> to complete their registration by activating them and assigning them to facilities and roles.</p>\n";
							$message .= file_get_contents('includes/emailFooter.php');
						   	$email->addMessage($message);
						   	$email->send();
							$view = 'login';
							$_REQUEST['view'] = 'login';							
						   } else {
						   	$errorText[] = 'You\'re registration failed.  Please try again or contact support.';
						   }
						   break;
		}
		$_REQUEST['PASSWORD1'] = "";
		$_REQUEST['PASSWORD2'] = "";
	}

if (isset($userDb)) {
	unset($userDb);
}

?>