<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<!--  after declaration -->
<?php
if (!isset($viewTitle)) {
	$viewTitle = 'Home';
}
if (isset($_REQUEST['fromPage'])) {
	$fromPage = $_REQUEST['fromPage'];
} else {
	$fromPage = '';
}

$logAction = 'goToView(\'login\');';
$logText = 'Sign&nbsp;In';
$navArray = array();
if (isset($_SESSION[APPLICATION]['module']) and file_exists($_SESSION[APPLICATION]['module'].'/views/help.'.$targetPage) or file_exists('views/help.'.$targetPage)) {
	$navArray[] = array("navText"=>'Help',"onClick"=>'onclick="showHelp(\''.$targetPage.'\',\''.$fromPage.'\');"');
}
if (array_key_exists('loggedIn',$_SESSION[APPLICATION]) and $_SESSION[APPLICATION]['loggedIn']) {
  $logAction = 'goToView(\'logoff\');';
  $logText = 'Sign&nbsp;Out';
if (isset($_SESSION[APPLICATION]['module']) and $_SESSION[APPLICATION]['module'] == 'admin') {
  $navArray[] =	array("navText"=>"Maintain","subMenu"=>array(array('onClick'=>'onclick="goToView(\'databaseList\');"','text'=>'Servers'),array('onClick'=>'onclick="goToView(\'userList\');"','text'=>'Users'),array('onClick'=>'onclick="goToView(\'workflowManage\');"','text'=>'Workflow'),array('onClick'=>'onclick="buildFormArrays();"','text'=>'Form Arrays'),array('onClick'=>'onclick="goToView(\'config\');"','text'=>'Configuration'),array('onClick'=>'onclick="goToView(\'conditionalUserList\');"','text'=>'Cond. Users')));
} else {
  $navArray[] = array("navText"=>"My Account",'onClick'=>'onclick="goToView(\'userEdit\');"','text'=>'My Account');
}
  $navArray[] = array("navText"=>"Forms",'onClick'=>'onclick="goToView(\'home\');"','text'=>'Forms');
}
$homeView = 'home';
?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<?php
if (isset($viewMetaTags)) {
	if (is_array($viewMetaTags)) {
		foreach($viewMetaTags as $metaDataRow) {
			echo $metaDataRow."\n";
		}
	} else {
		echo $metaDataRow."\n";
	}
}
?>
<title>Flex-N-Gate - <?php echo $viewTitle?></title>
<?php
if (isset($cssArray) and is_array($cssArray)) {
  	foreach($cssArray as $cssFile) {
  	echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"$cssFile\" />";
  	}
}
 ?>
 <link rel="stylesheet" type="text/css" href="styles/fngMain.css" />
 <link rel="stylesheet" type="text/css" href="js/chromatable/css/style.css" />
<script type="text/javascript" src="js/phoneMask.js"></script>
<script type="text/javascript" src="js/dFilter.js"></script>
<script type="text/javascript" src="js/jQuery.js"></script>
<script type="text/javascript" src="js/jquery.simplemodal-1.4.3.js"></script>
<script type="text/javascript" src="js/chromatable/jquery.chromatable.js"></script>
<?php
if (isset($jsArray)  and is_array($jsArray)) {
	foreach ($jsArray as $jsFile) {
		echo "<script type=\"text/javascript\" src=\"$jsFile\"></script>\n";
	}
}
?>
<script type="text/javascript" src="js/main.js"></script>
<!--[if IE]>
<style>
#divider {
background: url('media/IEDropShadow.jpg') bottom repeat-x;
padding-bottom:3px;
}
</style>
<![endif]-->
<script>
function showHelp(View,fromPage) {
	theForm = document.getElementById('navForm');
	theForm.view.value = 'help';
	theForm.helpView.value = View;
	if (fromPage) {
		theForm.fromPage.value = fromPage;
	}
	theForm.submit();
}
</script>
</head>

<body>
<div id="brandWrapper">
	<div id="brandHead">
	    <a href="<?php echo $_SERVER['PHP_SELF']?>"><img src="media/fngLogo.jpg"></img></a>
			<form name="navForm" id="navForm" method="post" action="<?php echo $_SERVER['PHP_SELF']?>">
			<input type="hidden" name="view"></input>
			<input type="hidden" name="helpView"></input>
			<input type="hidden" name="fromPage"></input>
			</form>

		<div class="clear"></div>
	 	<div id="welcomeRight"><?php echo $viewTitle?></div>
	</div>
</div>

<div class="clear"></div>
<div id="divider">
   <div id="dividercontent">
		<div class="navWrapper">
		<?php
		foreach ($navArray as $navItem) {
			if ($navItem['navText']) {
				if (array_key_exists('onClick',$navItem)) {
					echo '<div class="navigation" '.$navItem['onClick'].'><div class="navText">'.$navItem['navText']."</div>";
					$endDiv = '';
				} else {
					echo '<div class="navContainer"><div class="navheader"><div class="navText">'.$navItem['navText']."</div>";
					$endDiv = "</div>";
				}
				if (array_key_exists('subMenu',$navItem) and is_array($navItem['subMenu'])) {
					foreach($navItem['subMenu'] as $subMenu) {
						echo '<div class="navItem" '.$subMenu['onClick'].'><div class="navText">'.$subMenu['text'].'</div></div>';
					}
				}
				echo '</div>';
				echo $endDiv;
			}
		}
		?>
		<div class="navigation" onclick="<?php echo $logAction?>"><span class="navText"><?php echo $logText ?></span></div>
	  </div>
   </div>
</div>

<div id="contentWrapper">

	<div id="content">
