<?php
if (array_key_exists('module',$_SESSION[APPLICATION]) and $_SESSION[APPLICATION]['module']) {
	$module = $_SESSION[APPLICATION]['module'];
} else {
	$module = '';
}

// Find and include the header from the includes folder.
if (!isset($_REQUEST['AJAX']) or $_REQUEST['AJAX'] != 'Y') {
	if (file_exists($module.'/includes/header.php')) {
		include($module.'/includes/header.php');
	} else {
		if (file_exists('includes/header.php')) {
			include('includes/header.php');
		} else {
			base::missingPage('header.php');
		}
	}
}
// Find and include the view from the views folder.
if (file_exists($module.'/views/view.'.$targetPage)) {
	include($module.'/views/view.'.$targetPage);
} else {
	if (file_exists('views/view.'.$targetPage)) {
		include('views/view.'.$targetPage);
	} else {
		base::missingPage('view.'.$targetPage);
	}
}

// Find and include the Footer from the includes folder.
//  Footer is optional so no error message is displayed if
//  it cannot be found.
if (!isset($_REQUEST['AJAX']) or $_REQUEST['AJAX'] != 'Y') {
	if (file_exists($module.'/includes/footer.php')) {
		include($module.'/includes/footer.php');
	} else {
		if (file_exists('includes/footer.php')) {
			include('includes/footer.php');
		}
	}
}

?>