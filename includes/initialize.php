<?php
error_reporting(E_ALL ^ E_STRICT);
function loadClass($class) {
	if (file_exists('classes/class.'.$class.'.php')) {
		include ('classes/class.'.$class.'.php');
	}
}
spl_autoload_register('loadClass');
require ('includes/c_config.php');
set_include_path(get_include_path().PATH_SEPARATOR.$config['applicationRoot'].'/plm/classes/'.PATH_SEPARATOR.$config['applicationRoot'].'/plm/');
require_once('classes/class.log.php');
register_shutdown_function( "base::write_log" );

include_once('classes/class.workFlowInstance.php');
require_once('classes/class.db.php');
require_once('classes/class.base.php');
include_once('includes/vendorMap.php');
//require_once('includes/vendorArrays.php');
define('APPLICATION', 'flexNgateWF');
define('intWFPROCESS_ID_VENDOR_REQUEST', 1);
define('intWFPROCESS_ID_VENDOR_CHANGE',2);
$thisFormEmail = $config['defaultEmail'];
if (!isset($AJAX) or $AJAX != true) {
	$workFlowArray = new workFlowInstanceArray();
	$availableForms = $workFlowArray->loadAllForms();
	$allFormsAvailable = array();
}
// Get connection for use throughout
$connect = new Db();
$cxn['*LOCAL'] = $connect->Get_Db_Connection();
if (isset($_REQUEST['view'])) {
	$inputView = $_REQUEST['view'];
} else {
	$inputView = '';
}
// start a session
if (isset($_REQUEST['session_id'])) {
	$sessionID = $_REQUEST['session_id'];
	session_id($sessionID);
}
if (!session_start()) {
  echo "Unable to establish a session:  Try again later.";
  error_log("unable to establish a session");
} else {
	if (!isset($_SESSION[APPLICATION]['lastUpdKey'])) {
		$_SESSION[APPLICATION]['lastUpdKey'] = '';
}
include('plm/common.php');
if (!array_key_exists('view',$_REQUEST) and isset($_SESSION[APPLICATION]['targetView'])) {
	$_REQUEST['view'] = $_SESSION[APPLICATION]['targetView'];
}
// determine which view to display
  if (isset($_REQUEST['view']) and $_REQUEST['view'] != "") {
    $view = $_REQUEST['view'];
  } else {
  	if (!isset($view) or $view == "") {
  		if (array_key_exists('loggedIn',$_SESSION[APPLICATION]) and $_SESSION[APPLICATION]['loggedIn'] == true) {
  			$view = 'home';
		}
  	}
  }
// If not on the home view, user must be logged in.
if (!isset($view) or ($view != 'help' and $view != 'home' and $view != 'login' and $view != 'changePWD' and $view != 'registerUser' and $view != 'pwdReset' and $_SESSION[APPLICATION]['loggedIn'] != true)) {
	$view = 'login';
	$_SESSION[APPLICATION]['loginRequest'] = $_REQUEST;
	$_REQUEST['view'] = 'login';
	$cmd = '';
	$_REQUEST['cmd'] = '';
}
if ($view == 'login' and array_key_exists('loggedIn',$_SESSION[APPLICATION]) and $_SESSION[APPLICATION]['loggedIn']) {
	$view = 'home';
}

// instantiate halt flag for later reference.
  if (!isset($haltProcessing)) {
  	$haltProcessing = "";
  }

// get command function to process
  if (isset($_REQUEST['cmd'])) {
  	$cmd = $_REQUEST['cmd'];
  } else {
  	$cmd = "";
  }
}
// locale settings
setlocale(LC_MONETARY, 'en_US');
date_default_timezone_set('America/Detroit');
?>