<?php
require_once('classes/class.account.php');
require_once('classes/class.tbluser.php');
include_once('classes/class.viewuserfacilityxr.php');
// sort function to handle userFacilities;
function srtCompare($a, $b) {
	return strcmp($a['Name'], $b['Name']);
}
if (isset($_REQUEST['userEmail'])) {
	$userEmail = $_REQUEST['userEmail'];
}
if (isset($_REQUEST['password'])) {
	$password = md5($_REQUEST['password']);
	// password changed from a straight md5 hash to a proprietary algorythm
	$NewPassword = base::hashPassword($_REQUEST['password']);
}
$account = new account();
// retrieve the users account
$account->select(null, array('WHERE'=>array('EMAIL'=>$userEmail)));
if ($row = $account->getnext()) {
	// if a password change is required, the user has 24 hours to complete it.
	// calculate the elapsed time since the last account update.  If more than
	// 24 hours has elapsed, set the account status to inactive.
	if ($row['PCHNGREQ'] == 'Y' and $row['STATUS'] == 'A') {
		$hours = substr($row['LASTUPDDT'],11,2);
		$minutes = substr($row['LASTUPDDT'],14,2);
		$seconds = substr($row['LASTUPDDT'],17,2);
		$months = substr($row['LASTUPDDT'],5,2);
		$years = substr($row['LASTUPDDT'],1,4);
		$days = substr($row['LASTUPDDT'],8,2);
		$elapsedHours = (time() - mktime($hours,$minutes,$seconds,$months,$days,$years)) / 3600;
		if ($elapsedHours > 24) {
			$_REQUEST['STATUS'] = 'I';
			$_REQUEST['ID#'] = $row['ID#'];
			$account->update();
			$row['STATUS'] = 'I';
		}
	}
	// If this account is still using a straight md5 hash convert them to the new
	//  proprietary algorythm.
	if ($row['PASSWORD'] == $password) {
		$_REQUEST['PASSWORD'] = $NewPassword;
		$_REQUEST['ID#'] = $row['ID#'];
		if ($account->update()) {
			$account->select(null, array('WHERE'=>array('EMAIL'=>$userEmail)));
			if (!$row = $account->getnext()) {
				$NewPassword = $password;
			}
		}
	}
	// if the user's password matches the one stored and their account is active
	//  log them into the application and setup the session variables
	if ($row['PASSWORD'] == $NewPassword and $row['STATUS'] == 'A') {
		$_SESSION[APPLICATION]['loggedIn'] = true;
		$_SESSION[APPLICATION]['userName'] = trim($row['FNAME']).' '.trim($row['LNAME']);
		$_SESSION[APPLICATION]['user'] = $row['ID#'];
		$_SESSION[APPLICATION]['email'] = $row['EMAIL'];
		$_SESSION[APPLICATION]['module'] = strtolower($row['AUTHORITY']);
		$_SESSION[APPLICATION]['change'] = trim($row['PCHNGREQ']);
	// retrieve workflow user definition and store in session for use by workflow processes
		$tblUser = new tbluser();
		$tblUser->select(null,array('WHERE'=>array('strEmail'=>$row['EMAIL'])));
		if ($tblUserRow = $tblUser->getnext()) {
			if ($tblUserRow['strPassword'] != $password) {
			  $_REQUEST['intUserID'] = $tblUserRow['intUserID'];
			  $_REQUEST['strPassword'] = $password;
			  if ($tblUser->update()) {
			  	$tblUserRow['strPassword'] = $password;
			  }
			}
			$plmUser = new user();
			$plmUser->login($tblUserRow['strLoginName'], $_REQUEST['password']);
		//	$_SESSION['objUser'] = $tblUserRow['intUserID'];
			$_SESSION['intUser'] = $tblUserRow['intUserID'];
		}
	// user successfully logged in so reset the invalid login attempts.
		$_REQUEST['ID#'] = $row['ID#'];
		$_REQUEST['LASTLOGIN'] = date('Y-m-d-H.i.s.'.'000000');
	    $_REQUEST['INVLOGIN'] = 0;
	    $account->update();
	// switch view from login to home.
		if ($view == 'login') {
			$view = 'home';
		}
    // retrieve and store userRoles
        $userRoles = array();
        $userRole = new tbluserrolexr();
        $userRole->select(null,array('WHERE'=>array('intUserID'=>$_SESSION['intUser'])));
        while($userRoleRow = $userRole->getnext()) {
           $userRoles[] = $userRoleRow;
		}
		if (isset($userRole)) {
			unset($userRole);
		}
		$_SESSION[APPLICATION]['userRoles'] = $userRoles;
    // retrieve and store userFacilities
		$userFacility = new viewuserfacilityxr();
		$userFacility->select(null,array('WHERE'=>array('userID'=>$_SESSION['intUser'])));
		while ($userFacilityRow = $userFacility->getnext()) {
			if($userFacilityRow['FWFUT05'] != 'I') {
				$userFacilities[] = array('DBase' => $userFacilityRow['DBase'], 'Plant' => $userFacilityRow['Plant'], 'Company' => $userFacilityRow['Company'], 'Name' => $userFacilityRow['FWDATBMN']);
			}

		}
		if (isset($userFacility)) {
			unset($userFacility);
		}
		usort($userFacilities, "srtCompare");


//		$userFacilities = array();
//        $userFacility = new userfacilityxr();
//        $userFacility->select(null,array('WHERE'=>array('userID'=>$_SESSION['intUser'])));
//        while ($userFacilityRow = $userFacility->getnext()) {
//        	$userFacilities[] = array('DBase'=>$userFacilityRow['DBase'],'Plant'=>$userFacilityRow['Plant'],'Company'=>$userFacilityRow['Company']);
//        }
//        if (isset($userFacility)) {
//        	unset($userFacility);
//        }
        $facility = new fwcompid();
//        $count = 0;
//        foreach ($userFacilities as $userFacility) {
//        	$facility->select(null,array('WHERE'=>array('FWDATC'=>$userFacility['DBase'],'FWPLTC'=>$userFacility['Plant'],'FWGLCO'=>$userFacility['Company'])));
//        	if ($facilityRow = $facility->getnext()) {
//        		if (trim($facilityRow['FWFUT05']) != 'I') {
//        			$userFacilities[$count]['Name'] = $facilityRow['FWDATBMN'];
//        			$count++;
//        		} else {
//        			//unset($userFacilities[$count]);
//        			array_splice($userFacilities,$count,1);
//        		}
//        	}
//        }
//        usort($userFacilities, "srtCompare");

        $_SESSION[APPLICATION]['userFacilities'] = $userFacilities;

        $count = 0;
        $facility->select(null,null);
        	while ($allFacilityRow = $facility->getnext()) {
        		if (trim($allFacilityRow['FWFUT05']) != 'I') {
        			$allFacilities[$count] = array('DBase'=>$allFacilityRow['FWDATC'],'Plant'=>$allFacilityRow['FWPLTC'],'Company'=>$allFacilityRow['FWGLCO'],'Name'=>$allFacilityRow['FWDATBMN']);
        			$count++;
        		}
        	}
        usort($allFacilities, "srtCompare");

        $_SESSION[APPLICATION]['allFacilities'] = $allFacilities;

        if (isset($facility)) {
        	unset($facility);
        }
	} else {
		// if the passwords did not match and the user account is active, update the
		// last login attempt date/time and the number of invalid login attempts.
		if ($row['STATUS'] == 'A') {
			$_REQUEST['ID#'] = $row['ID#'];
			$_REQUEST['LASTLIATT'] = date('Y-m-d-H.i.s.'.'000000');
			// check the current date against the last login attempt.  If the day has changed
			//  reset the invalid login attempts
			if (substr($row['LASTLIATT'],0,10) < date('Y-m-d')) {
				$row['INVLOGIN'] = 0;
			}
			$_REQUEST['INVLOGIN'] = $row['INVLOGIN']+1;
			// If the number of invalid login attempts accumulated is greater than the number allowed
			// disable the user account.
			if ($_REQUEST['INVLOGIN'] == $config['loginattemptsallowed']) {
				$_REQUEST['STATUS'] = 'D';
				$errorText[] = "Your account has been disabled.  Please contact support for assistance.";
			}
			$account->update();
		}
		$errorText[] = "Invalid Login information.  Please contact support for assistance.";
	}
	} else {
		$errorText[] = "Invalid Login information.  Please try again or contact support for assistance.";
	}
	if (array_key_exists('loggedIn',$_SESSION[APPLICATION]) and $_SESSION[APPLICATION]['loggedIn']) {
		if ($row['PCHNGREQ'] != 'Y' and array_key_exists('loginRequest',$_SESSION[APPLICATION])) {
			foreach($_SESSION[APPLICATION]['loginRequest'] as $loginKey=>$loginValue) {
				$_REQUEST[$loginKey] = $loginValue;
				if ($loginKey == 'view') {
					$view = $loginValue;
				}
			}
			unset($_SESSION[APPLICATION]['loginRequest']);
		}
	}
base::write_log("User Logged In","S");
?>