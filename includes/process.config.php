<?php 

$configFile = "includes/c_config.php";
$configData = "<?php\n";
if (isset($_REQUEST['schema'])) {
	$out_schema = $_REQUEST['schema'];
} else {
	$out_schema = $config['schema'];
}
$configData .= '$config[\'schema\'] = \''.$out_schema.'\';'."\n";
if (isset($_REQUEST['adminUser']) and trim($_REQUEST['adminUser']) != '') {
	$outAdminUser = $_REQUEST['adminUser'];
} else {
	$outAdminUser = $config['adminUser'];
}
$configData .= '$config[\'adminUser\'] = \''.$outAdminUser.'\';'."\n";
if (isset($_REQUEST['adminPassword']) and trim($_REQUEST['adminPassword']) != '') {
	$outAdminPassword = base::hashPassword($_REQUEST['adminPassword']);
} else {
	$outAdminPassword = $config['adminPassword'];
}
$configData .= '$config[\'adminPassword\'] = \''.$outAdminPassword.'\';'."\n";
if (isset($_REQUEST['webURL']) and trim($_REQUEST['webURL']) != '') {
	$outWebURL = $_REQUEST['webURL'];
} else {
	$outWebURL = $config['webURL'];
}
if (trim($outWebURL) != "" and substr($outWebURL,strlen(trim($outWebURL))-1,1) != '/') {
	$outWebURL = trim($outWebURL).'/';
} 
$configData .= '$config[\'webURL\'] = \''.$outWebURL.'\';'."\n";
if (isset($_REQUEST['mailHost']) and trim($_REQUEST['mailHost']) != '') {
	$outMailHost = $_REQUEST['mailHost'];
} else {
	$outMailHost = $config['mail']['host'];
}
if (isset($_REQUEST['mailSSl']) and trim($_REQUEST['mailSSl']) != '') {
	if ($_REQUEST['mailSSl'] == 'Y') {
		$outMailSSl = 'true';
		$outMailHost = 'ssl://'.$outMailHost;
	} else {
		$outMailSSl = 'false';
	}
} else {
	$outMailSSl = $config['mail']['ssl'];
}
if (isset($_REQUEST['mailPort']) and trim($_REQUEST['mailPort']) != '') {
	$outMailPort = $_REQUEST['mailPort'];
} else {
	$outMailPort = $config['mail']['port'];
}
if (isset($_REQUEST['mailFrom']) and trim($_REQUEST['mailFrom']) != '') {
	$outMailFrom = $_REQUEST['mailFrom'];
} else {
	$outMailFrom = $config['mail']['from'];
}
if (isset($_REQUEST['mailUser']) and trim($_REQUEST['mailUser']) != '') {
	$outMailUser = $_REQUEST['mailUser'];
} else {
	$outMailUser = $config['mail']['username'];
}
if (isset($_REQUEST['mailPwd']) and trim($_REQUEST['mailPwd']) != '') {
	$outMailPwd = $_REQUEST['mailPwd'];
} else {
	$outMailPwd = $config['mail']['password'];
}
if (isset($_REQUEST['mailAuth']) and trim($_REQUEST['mailAuth']) != '') {
	if ($_REQUEST['mailAuth'] == 'Y') {
		$outMailAuth = 'true';
	} else {
		$outMailAuth = 'false';
	}
} else {
	$outMailAuth = $config['mail']['auth'];
}

$configData .= '$config[\'mail\'] = array(\'from\'=>\''.$outMailFrom.'\',\'auth\'=>'.$outMailAuth.',\'host\'=>\''.$outMailHost.'\',\'port\'=>'.$outMailPort.',\'username\'=>\''.$outMailUser.'\',\'password\'=>\''.$outMailPwd.'\',\'ssl\'=>'.$outMailSSl.');'."\n";

if ($handle = fopen($configFile, 'w')) {
	if (fwrite($handle, $configData)) {
	  fclose($handle);
	  $config['schema'] = $out_schema;
      $config['adminUser'] = $outAdminUser;
      $config['adminPassword'] = $outAdminPassword;
	  $config['webURL'] = $outWebURL;
	  if ($outMailAuth == 'true') {
	  	$outMailAuth = true;
	  } else {
	  	$outMailAuth = false;
	  }
	  if ($outMailSSl == 'true') {
	  	$outMailSSl = true;
	  } else {
	  	$outMailSSl = false;
	  }
	  $config['mail'] = array('from'=>$outMailFrom,'auth'=>$outMailAuth,'host'=>$outMailHost,'port'=>$outMailPort,'username'=>$outMailUser,'password'=>$outMailPwd,'ssl'=>$outMailSSl);
	  $errorText[] = "Data successfully saved.";
	} else {
		$errorText[] = "Error occured saving data.";	
	}
}
?>