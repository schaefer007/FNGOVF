<?php /* Smarty version Smarty-3.0.9, created on 2015-08-14 13:23:53
         compiled from "/www/zendsvr6/htdocs/Development/FNGForms/plm//templates/ec_currently_editing.tpl" */ ?>
<?php /*%%SmartyHeaderCode:57939035655ce242986f1a7-74675267%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd4a5a7c77be70f571f9cd9e97b1b62c153217a63' => 
    array (
      0 => '/www/zendsvr6/htdocs/Development/FNGForms/plm//templates/ec_currently_editing.tpl',
      1 => 1335540340,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '57939035655ce242986f1a7-74675267',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (isset($_SESSION['arrWorkingData'])&&isset($_SESSION['arrWorkingData']['objCurrentEC'])&&$_smarty_tpl->getVariable('intCurrentProgramCommonID')->value==$_SESSION['arrWorkingData']['objCurrentEC']->getProgramCommonID()){?>
	<?php $_smarty_tpl->tpl_vars['objCurrentEC'] = new Smarty_variable($_SESSION['arrWorkingData']['objCurrentEC'], null, null);?>
	<?php if ($_smarty_tpl->getVariable('objCurrentEC')->value->getECStatus()=="New"&&isset($_smarty_tpl->getVariable('blnEngineeringChangeWrite',null,true,false)->value)&&$_smarty_tpl->getVariable('blnEngineeringChangeWrite')->value){?>
		<?php if (strpos($_SERVER['PHP_SELF'],"program.php")!==false||strpos($_SERVER['PHP_SELF'],"tooling_budget.php")!==false){?>
			<div style="float:left;">
				<?php if (strpos($_SERVER['PHP_SELF'],"program.php")!==false){?><input type="button" value="Submit EC" onclick="javascript:submitEC();" /><?php }?>
				<input type="button" value="Stop Editing" onclick="javascript:stopEditing();" />
			</div>
		<?php }?>
		<div class="editing_for_ec">
			Currently editing <a href="ec_add.php?intEngineeringChangeID=<?php echo $_smarty_tpl->getVariable('objCurrentEC')->value->getEngineeringChangeID();?>
">EC <?php echo $_smarty_tpl->getVariable('objCurrentEC')->value->getECNumber();?>
</a>, any modifications to this program will be logged against this engineering change.
		</div>
	<?php }else{ ?>
		<?php if (strpos($_SERVER['PHP_SELF'],"program.php")!==false||strpos($_SERVER['PHP_SELF'],"tooling_budget.php")!==false){?>
			<div style="float:left;">
				<input type="button" value="Stop Viewing" onclick="javascript:stopEditing();" />
			</div>
		<?php }?>
		<div class="editing_for_ec">Currently viewing <a href="ec_add.php?intEngineeringChangeID=<?php echo $_smarty_tpl->getVariable('objCurrentEC')->value->getEngineeringChangeID();?>
">EC <?php echo $_smarty_tpl->getVariable('objCurrentEC')->value->getECNumber();?>
</a>.</div>
	<?php }?>
<?php }?>