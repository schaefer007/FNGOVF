<?php /* Smarty version Smarty-3.0.9, created on 2014-08-07 09:18:04
         compiled from "/www/zendsvr6/htdocs/FNGForms/plm//templates/workflow.tpl" */ ?>
<?php /*%%SmartyHeaderCode:13110514553e37c8ccdfda5-80985587%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '277772343a25c2889c0f6899c110e46ed5592692' => 
    array (
      0 => '/www/zendsvr6/htdocs/FNGForms/plm//templates/workflow.tpl',
      1 => 1356842272,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '13110514553e37c8ccdfda5-80985587',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!isset($_smarty_tpl->getVariable('blnEditable',null,true,false)->value)){?>
	<?php $_smarty_tpl->tpl_vars['blnEditable'] = new Smarty_variable(0, null, null);?>
<?php }?>

<?php if ($_smarty_tpl->getVariable('objWFProcess')->value->getWFProcessID()){?>
	<a name="process_diagram"></a>
	<div class="heading">
		<div class="text">
			<h3>Diagram</h3>
		</div>
		<div class="clL"></div>
	</div>
	<div class="clL"></div>

	<div style="float:left;">
		Process Path:
	</div>
	<div style="float:left;padding-right:20px;">
		<img src="<?php echo @strSITE_URL;?>
images/help.png" alt="?" help="Select a path and the diagram will update, highlighting the primary transitions for that path." />
	</div>
	<div class="process_path">
		<input type="radio" name="intProcessPathID" value="" checked="true" onclick="updateDiagram('', '<?php echo $_smarty_tpl->getVariable('objWFProcess')->value->getWFProcessID();?>
')" /> Show All
	</div>
	<?php  $_smarty_tpl->tpl_vars['objProcessPath'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('objWFProcess')->value->getProcessPathArray()->getArray(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['objProcessPath']->key => $_smarty_tpl->tpl_vars['objProcessPath']->value){
?>
		<div class="process_path">
			<input type="radio" name="intProcessPathID" value="<?php echo $_smarty_tpl->getVariable('objProcessPath')->value->getProcessPathID();?>
" onclick="updateDiagram('<?php echo $_smarty_tpl->getVariable('objProcessPath')->value->getProcessPathID();?>
', '<?php echo $_smarty_tpl->getVariable('objWFProcess')->value->getWFProcessID();?>
')" /> <?php echo $_smarty_tpl->getVariable('objProcessPath')->value->getPathName();?>

		</div>
	<?php }} ?>
	<br />

	<img src="<?php echo @strSITE_URL;?>
state_diagram.php?intWFProcessID=<?php echo $_smarty_tpl->getVariable('objWFProcess')->value->getWFProcessID();?>
" id="state_diagram" alt="" />
	<br />

	<a name="process_states"></a>
	<div class="heading">
		<div class="text">
			<h3>Process States</h3>
			<img src="<?php echo @strSITE_URL;?>
images/help.png" alt="?" help="Process States are the boxes from the diagram above." />
		</div>

		<?php if ($_smarty_tpl->getVariable('blnEditable')->value){?>
			<div class="command">
				<a href="javascript:loadProcessState()" class="text_action">
					<div class="icon"><img src="<?php echo @strSITE_URL;?>
images/add.png" alt="" title="Add Process State" /></div>
					<div class="text">Add Process State</div>
				</a>
				<div class="clL"></div>
			</div>
		<?php }?>
		<div class="clL"></div>
	</div>
	<div class="clL"></div>

	<table class="table table_process_states" id="process_states">
		<tr>
			<th>State Name</th>
			<th>Description</th>
			<?php if ($_smarty_tpl->getVariable('blnEditable')->value){?>
				<th></th>
				<th></th>
			<?php }?>
		</tr>
		<?php  $_smarty_tpl->tpl_vars['objProcessState'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('objWFProcess')->value->getProcessStateArray()->getArray(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['objProcessState']->key => $_smarty_tpl->tpl_vars['objProcessState']->value){
?>
			<?php $_template = new Smarty_Internal_Template("process_state_line.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
		<?php }} else { ?>
			<tr class="altBG0_0" id="no_process_states"><td class="output" colspan="3">There are no states for this workflow process.</td></tr>
		<?php } ?>
	</table>
	<br />

	<a name="process_transitions"></a>
	<div class="heading">
		<div class="text">
			<h3>Process Transitions</h3>
			<img src="<?php echo @strSITE_URL;?>
images/help.png" alt="?" help="Process Transitions are the directed lines from the diagram above. Transitions occur when the the specified <i>Roles</i> take the necessary <i>Actions</i>." />
		</div>

		<?php if ($_smarty_tpl->getVariable('blnEditable')->value){?>
			<div class="command">
				<a href="javascript:loadProcessTransition(null, '<?php echo $_smarty_tpl->getVariable('objWFProcess')->value->getWFProcessID();?>
')" class="text_action">
					<div class="icon"><img src="<?php echo @strSITE_URL;?>
images/add.png" alt="" title="Add Process Transition" /></div>
					<div class="text">Add Process Transition</div>
				</a>
				<div class="clL"></div>
			</div>
		<?php }?>
		<div class="clL"></div>
	</div>
	<div class="clL"></div>

	<li>Process transitions are essentially state changes that occur when a user (or group of users) takes action in the system.</li>
	<li>After each process transition, emails are sent out to those who are required to take the next action.</li>
	<div class="clL"></div>
	<br />

	<table class="table table_process_transitions" id="process_transitions">
		<tr>
			<th>Transition Name</th>
			<th>if Roles <img src="<?php echo @strSITE_URL;?>
images/help.png" alt="?" help="These roles are the ones who can take the corresponding action in order to make the transition occur.<br />Note: Not all roles are required, notice the ANDs and ORs between roles." /></th>
			<th>take Action <img src="<?php echo @strSITE_URL;?>
images/help.png" alt="?" help="This is the action the user is taking.<br />This text matches the exact text on the button the user clicks to take action." /></th>
			<th>move From State</th>
			<th>To State</th>
			<?php if ($_smarty_tpl->getVariable('blnEditable')->value){?>
				<th></th>
				<th></th>
			<?php }?>
		</tr>
		<?php  $_smarty_tpl->tpl_vars['objProcessTransition'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('objWFProcess')->value->getProcessTransitionArray()->getArray(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['objProcessTransition']->key => $_smarty_tpl->tpl_vars['objProcessTransition']->value){
?>
			<?php $_smarty_tpl->tpl_vars['objFromProcessState'] = new Smarty_variable($_smarty_tpl->getVariable('objWFProcess')->value->getProcessStateArray()->getObject($_smarty_tpl->getVariable('objProcessTransition')->value->getFromProcessStateID()), null, null);?>
			<?php $_smarty_tpl->tpl_vars['objToProcessState'] = new Smarty_variable($_smarty_tpl->getVariable('objWFProcess')->value->getProcessStateArray()->getObject($_smarty_tpl->getVariable('objProcessTransition')->value->getToProcessStateID()), null, null);?>
			<?php $_smarty_tpl->tpl_vars['intWFProcessID'] = new Smarty_variable($_smarty_tpl->getVariable('objWFProcess')->value->getWFProcessID(), null, null);?>
			<?php $_template = new Smarty_Internal_Template("process_transition_line.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
		<?php }} else { ?>
			<tr class="altBG0_0" id="no_process_transitions"><td class="output" colspan="7">There are no transitions for this workflow process.</td></tr>
		<?php } ?>
	</table>
<?php }?>