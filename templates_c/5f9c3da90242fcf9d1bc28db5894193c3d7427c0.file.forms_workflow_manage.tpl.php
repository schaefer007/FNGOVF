<?php /* Smarty version Smarty-3.0.9, created on 2014-08-07 09:18:01
         compiled from "/www/zendsvr6/htdocs/FNGForms/plm//templates/forms_workflow_manage.tpl" */ ?>
<?php /*%%SmartyHeaderCode:191690746153e37c89e3b865-98995559%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5f9c3da90242fcf9d1bc28db5894193c3d7427c0' => 
    array (
      0 => '/www/zendsvr6/htdocs/FNGForms/plm//templates/forms_workflow_manage.tpl',
      1 => 1354820985,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '191690746153e37c89e3b865-98995559',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div class="heading">
	<div class="text">
		<h2>Workflow Management</h2>
	</div>
 
	<div class="command command_h2">
		<a href="javascript:loadWorkflowProcess()" class="text_action">
			<div class="icon"><img src="images/add.png" alt="" title="Create Workflow Process" /></div>
			<div class="text">Create Workflow Process</div>
		</a>
		<div class="clL"></div>
	</div>
	<div class="clL"></div>
</div>

<div class="box">
	<form action="<?php echo $_SERVER['PHP_SELF'];?>
" method="get">
		<div class="filter filter_wf_process">
			<div class="label">
				Workflow Process:&nbsp;
				<a href="javascript:loadWorkflowProcess('Edit');"><img src="images/edit.png" alt="Edit" title="Edit Workflow Process" /></a>
			</div>
			<div class="input">
				<select name="intWFProcessID">
					<option></option>
					<?php  $_smarty_tpl->tpl_vars['objWFProcessSelect'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('objWFProcessArray')->value->getArray(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['objWFProcessSelect']->key => $_smarty_tpl->tpl_vars['objWFProcessSelect']->value){
?>
						<option value="<?php echo $_smarty_tpl->getVariable('objWFProcessSelect')->value->getWFProcessID();?>
" <?php if ($_smarty_tpl->getVariable('objWFProcessSelect')->value->getWFProcessID()==$_smarty_tpl->getVariable('objWFProcess')->value->getWFProcessID()){?>selected<?php }?>><?php echo $_smarty_tpl->getVariable('objWFProcessSelect')->value->getWFProcessName();?>
</option>
					<?php }} ?>
				</select>
			</div>
		</div>

		<div class="management_buttons">
			<input type="submit" value="Go" class="filter_button" />
		</div>
		<div class="clL"></div>
	</form>
	<div class="clL"></div>
	<br />

	<?php $_smarty_tpl->tpl_vars['blnEditable'] = new Smarty_variable(1, null, null);?>
	<?php $_template = new Smarty_Internal_Template("workflow.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
</div>
<div class="clL"></div>
<br />

<?php $_template = new Smarty_Internal_Template("footer.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>