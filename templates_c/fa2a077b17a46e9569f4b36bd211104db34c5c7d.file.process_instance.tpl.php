<?php /* Smarty version Smarty-3.0.9, created on 2014-07-14 14:28:49
         compiled from "/www/zendsvr6/htdocs/FNGForms/plm//templates/process_instance.tpl" */ ?>
<?php /*%%SmartyHeaderCode:101515427753c42161da2928-89063134%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'fa2a077b17a46e9569f4b36bd211104db34c5c7d' => 
    array (
      0 => '/www/zendsvr6/htdocs/FNGForms/plm//templates/process_instance.tpl',
      1 => 1356842132,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '101515427753c42161da2928-89063134',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_function_cycle')) include '/www/zendsvr6/htdocs/FNGForms/plm/Smarty/libs/plugins/function.cycle.php';
?><?php if (!isset($_smarty_tpl->getVariable('blnOverrideWorkflow',null,true,false)->value)){?>
	<?php $_smarty_tpl->tpl_vars['blnOverrideWorkflow'] = new Smarty_variable(0, null, null);?>
<?php }?>

<div id="process_instance">
	<a name="workflow"></a>
	<?php  $_smarty_tpl->tpl_vars['objProcessTransition'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('objProcessInstance')->value->getCurrentProcessState()->getProcessTransitionArray()->getArray(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['objProcessTransition']->key => $_smarty_tpl->tpl_vars['objProcessTransition']->value){
?>
		<input type="hidden" id="comment_required_<?php echo $_smarty_tpl->getVariable('objProcessTransition')->value->getProcessTransitionID();?>
" name="arrProcessTransitions[<?php echo $_smarty_tpl->getVariable('objProcessTransition')->value->getProcessTransitionID();?>
][blnCommentRequired]" value="<?php echo $_smarty_tpl->getVariable('objProcessTransition')->value->getCommentRequired();?>
" />
	<?php }} ?>
	<table class="table table_wf_processes">
		<tr>
			<th class="state" colspan="4">
				State &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<a href="javascript:processDefinition('<?php echo $_smarty_tpl->getVariable('objProcessInstance')->value->getWFProcessID();?>
')">View <?php echo $_smarty_tpl->getVariable('objProcessInstance')->value->getWFProcess()->getWFProcessName();?>
 Process Definition</a>
			</th>
			<th class="output"></th>
			<th class="state_type output">State Type</th>
		</tr>
		<tr>
			<th class="spacer">&nbsp;</th>

			<th class="role">Role</th>
			<th class="user">User</th>
			<th class="comment">Comment</th>
			<th class="action">Actions</th>
			<th class="date">Date</th>
		</tr>

		<?php  $_smarty_tpl->tpl_vars['objProcessTransitionInstance'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('objProcessInstance')->value->getProcessTransitionInstanceArray()->getArray(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['objProcessTransitionInstance']->key => $_smarty_tpl->tpl_vars['objProcessTransitionInstance']->value){
?>
			<tr class="subheader process_history">
				<td class="state output" colspan="5">
					<?php echo $_smarty_tpl->getVariable('objProcessTransitionInstance')->value->getProcessTransition()->getFromProcessState()->getProcessStateName();?>

					<?php if ($_smarty_tpl->getVariable('objProcessTransitionInstance')->value->getProcessTransition()->getFromProcessState()->getDescription()){?>
						<img src="<?php echo @strSITE_URL;?>
images/help.png" alt="?" help="<?php echo $_smarty_tpl->getVariable('objProcessTransitionInstance')->value->getProcessTransition()->getFromProcessState()->getDescription();?>
" />
					<?php }?>
				</td>
				<td class="state_type old_state output">Old State</td>
			</tr>
			<?php $_smarty_tpl->tpl_vars['blnFirstRole'] = new Smarty_variable(true, null, null);?>
			<?php  $_smarty_tpl->tpl_vars['objConditionInstance'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('objProcessTransitionInstance')->value->getConditionInstanceArray()->getArray(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['objConditionInstance']->key => $_smarty_tpl->tpl_vars['objConditionInstance']->value){
?>
				<?php $_smarty_tpl->tpl_vars['objRole'] = new Smarty_variable($_smarty_tpl->getVariable('objConditionInstance')->value->getRole(), null, null);?>
				<?php echo smarty_function_cycle(array('values'=>"altBG0_1,altBG0_0",'assign'=>"strBGClass",'name'=>"old_states_".($_smarty_tpl->getVariable('objProcessTransitionInstance')->value->getProcessTransitionInstanceID())),$_smarty_tpl);?>

				<tr class="<?php echo $_smarty_tpl->getVariable('strBGClass')->value;?>
 process_history">
					<td class="spacer"><?php if ($_smarty_tpl->getVariable('blnFirstRole')->value){?><img src="<?php echo @strSITE_URL;?>
images/transition.png" alt="" /><?php }else{ ?>&nbsp;<?php }?><?php $_smarty_tpl->tpl_vars['blnFirstRole'] = new Smarty_variable(false, null, null);?></td>
					<td class="role output">
						<a href="<?php echo $_smarty_tpl->getVariable('objRole')->value->getURL();?>
"><?php echo $_smarty_tpl->getVariable('objRole')->value->getRoleName();?>
</a>
					</td>
					<td class="user output">
						<?php $_smarty_tpl->tpl_vars['objUser'] = new Smarty_variable($_smarty_tpl->getVariable('objConditionInstance')->value->getUser(), null, null);?>
						<a href="<?php echo $_smarty_tpl->getVariable('objUser')->value->getUserPageURL();?>
"><?php echo $_smarty_tpl->getVariable('objUser')->value->getDisplayName();?>
</a><br />
					</td>
					<td class="comment output"><?php echo $_smarty_tpl->getVariable('objConditionInstance')->value->getComment();?>
</td>
					<td class="action output"><?php echo $_smarty_tpl->getVariable('objProcessTransitionInstance')->value->getProcessTransition()->getTransitionButton();?>
</td>
					<td class="date output"><?php echo formatDate($_smarty_tpl->getVariable('objConditionInstance')->value->getCompletedDate());?>
</td>
				</tr>
			<?php }} ?>
		<?php }} else { ?>
		<?php } ?>

		<tr class="subheader">
			<td class="state output" colspan="5">
				<?php echo $_smarty_tpl->getVariable('objProcessInstance')->value->getCurrentProcessState()->getProcessStateName();?>

				<?php if ($_smarty_tpl->getVariable('objProcessInstance')->value->getCurrentProcessState()->getDescription()){?>
					<img src="<?php echo @strSITE_URL;?>
images/help.png" alt="?" help="<?php echo $_smarty_tpl->getVariable('objProcessInstance')->value->getCurrentProcessState()->getDescription();?>
" />
				<?php }?>
			</td>
			<?php if ($_smarty_tpl->getVariable('objProcessInstance')->value->getRoleArray()->getArray()&&!$_smarty_tpl->getVariable('objProcessInstance')->value->getCurrentProcessState()->getFinalState()){?>
				<td class="state_type current_state output">Current State</td>
			<?php }else{ ?>
				<td class="state_type final_state output">Final State</td>
			<?php }?>
		</tr>
		<?php  $_smarty_tpl->tpl_vars['objRole'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('objProcessInstance')->value->getRoleArray()->getArray(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['objRole']->key => $_smarty_tpl->tpl_vars['objRole']->value){
?>
			<?php if ($_smarty_tpl->getVariable('objProcessInstance')->value->getCurrentProcessState()->getShowOnlyCurrentUser()&&!$_smarty_tpl->getVariable('objRole')->value->getUserArray()->objectIsSet($_SESSION['objUser']->getUserID())){?>
				<?php continue 1?>
			<?php }?>

			<?php echo smarty_function_cycle(array('values'=>"altBG0_1,altBG0_0",'assign'=>"strBGClass",'name'=>"current_state"),$_smarty_tpl);?>

			<?php $_smarty_tpl->tpl_vars['intRowspan'] = new Smarty_variable(max(1,count($_smarty_tpl->getVariable('objRole')->value->getUserArray()->getArray())), null, null);?>
			<?php $_smarty_tpl->tpl_vars['blnFirstUser'] = new Smarty_variable(true, null, null);?>
			<?php  $_smarty_tpl->tpl_vars['objProcessUser'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('objRole')->value->getUserArray()->getArray(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['objProcessUser']->key => $_smarty_tpl->tpl_vars['objProcessUser']->value){
?>
				<?php if ($_smarty_tpl->getVariable('objProcessInstance')->value->getCurrentProcessState()->getShowOnlyCurrentUser()){?>
					<?php $_smarty_tpl->tpl_vars['intRowspan'] = new Smarty_variable(1, null, null);?>
				 	<?php if ($_smarty_tpl->getVariable('objProcessUser')->value->getUserID()!=$_SESSION['objUser']->getUserID()){?>
						<?php continue 1?>
					<?php }?>
				<?php }?>
				<tr class="<?php echo $_smarty_tpl->getVariable('strBGClass')->value;?>
">
					<?php if ($_smarty_tpl->getVariable('blnFirstUser')->value){?>
						<td class="spacer" rowspan="<?php echo $_smarty_tpl->getVariable('intRowspan')->value;?>
">&nbsp;</td>
						<td class="role output" rowspan="<?php echo $_smarty_tpl->getVariable('intRowspan')->value;?>
">
							<a href="<?php echo $_smarty_tpl->getVariable('objRole')->value->getURL();?>
"><?php echo $_smarty_tpl->getVariable('objRole')->value->getRoleName();?>
</a>
						</td>
						<?php $_smarty_tpl->tpl_vars['blnFirstUser'] = new Smarty_variable(false, null, null);?>
					<?php }?>
					<td class="user output">
						<a href="<?php echo $_smarty_tpl->getVariable('objProcessUser')->value->getUserPageURL();?>
"><?php echo $_smarty_tpl->getVariable('objProcessUser')->value->getDisplayName();?>
</a><br />
					</td>
					<?php if ($_smarty_tpl->getVariable('objProcessUser')->value->getConditionInstance()->getConditionInstanceID()){?>
						<td class="comment output"><?php echo $_smarty_tpl->getVariable('objProcessUser')->value->getConditionInstance()->getComment();?>
</td>
						<td class="action output"><?php echo $_smarty_tpl->getVariable('objProcessUser')->value->getConditionInstance()->getProcessTransitionInstance()->getProcessTransition()->getTransitionButton();?>
</td>
						<td class="date output"><?php echo formatDate($_smarty_tpl->getVariable('objProcessUser')->value->getConditionInstance()->getCompletedDate());?>
</td>
					<?php }else{ ?>
						<td class="comment">
							<?php if ($_SESSION['objUser']->getUserID()==$_smarty_tpl->getVariable('objProcessUser')->value->getUserID()||$_smarty_tpl->getVariable('blnOverrideWorkflow')->value){?>
								<input type="text" id="comments_<?php echo $_smarty_tpl->getVariable('objRole')->value->getRoleID();?>
_<?php echo $_smarty_tpl->getVariable('objProcessUser')->value->getUserID();?>
" name="arrComments[<?php echo $_smarty_tpl->getVariable('objRole')->value->getRoleID();?>
][<?php echo $_smarty_tpl->getVariable('objProcessUser')->value->getUserID();?>
][txtComment]" />
							<?php }?>
						</td>
						<td class="actions" colspan="2">
							<?php  $_smarty_tpl->tpl_vars['objProcessTransition'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('objRole')->value->getProcessTransitionArray()->getArray(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['objProcessTransition']->key => $_smarty_tpl->tpl_vars['objProcessTransition']->value){
?>
								<input type="button" name="arrConditionInstances[<?php echo $_smarty_tpl->getVariable('objProcessTransition')->value->getProcessTransitionID();?>
][<?php echo $_smarty_tpl->getVariable('objRole')->value->getRoleID();?>
][<?php echo $_smarty_tpl->getVariable('objProcessUser')->value->getUserID();?>
][strTransitionButton]" value="<?php echo $_smarty_tpl->getVariable('objProcessTransition')->value->getTransitionButton();?>
"
									<?php if ($_SESSION['objUser']->getUserID()==$_smarty_tpl->getVariable('objProcessUser')->value->getUserID()||$_smarty_tpl->getVariable('blnOverrideWorkflow')->value){?>
										<?php $_smarty_tpl->tpl_vars['strJSCallbackFunction'] = new Smarty_variable('function(){}', null, null);?>
										<?php if ($_smarty_tpl->getVariable('objProcessTransition')->value->getJSCallbackFunction()){?>
											<?php $_smarty_tpl->tpl_vars['strJSCallbackFunction'] = new Smarty_variable($_smarty_tpl->getVariable('objProcessTransition')->value->getJSCallbackFunction(), null, null);?>
										<?php }?>
										onclick="workflowAction('<?php echo $_smarty_tpl->getVariable('objProcessInstance')->value->getProcessInstanceID();?>
', '<?php echo $_smarty_tpl->getVariable('objProcessTransition')->value->getProcessTransitionID();?>
', '<?php echo $_smarty_tpl->getVariable('objRole')->value->getRoleID();?>
', '<?php echo $_smarty_tpl->getVariable('objProcessUser')->value->getUserID();?>
', '<?php echo $_smarty_tpl->getVariable('objProcessInstance')->value->getProcessObject()->getProcessObjectClass();?>
', '<?php echo $_smarty_tpl->getVariable('objProcessInstance')->value->getProcessObject()->getID();?>
', $('#comments_<?php echo $_smarty_tpl->getVariable('objRole')->value->getRoleID();?>
_<?php echo $_smarty_tpl->getVariable('objProcessUser')->value->getUserID();?>
').val(), <?php echo $_smarty_tpl->getVariable('strJSCallbackFunction')->value;?>
)"
									<?php }else{ ?>
										disabled="true"
									<?php }?>
								/>
							<?php }} ?>
						</td>
					<?php }?>
					</td>
				</tr>
			<?php }} else { ?>
				<tr class="<?php echo $_smarty_tpl->getVariable('strBGClass')->value;?>
">
					<td class="spacer">&nbsp;</td>
					<td class="role output"><?php echo $_smarty_tpl->getVariable('objRole')->value->getRoleName();?>
</td>
					<td class="user user_undefined output">Undefined</td>
					<td class="comment">
						<?php if ($_smarty_tpl->getVariable('blnOverrideWorkflow')->value){?>
							<input type="text" id="comments_<?php echo $_smarty_tpl->getVariable('objRole')->value->getRoleID();?>
_no_user" name="arrComments[<?php echo $_smarty_tpl->getVariable('objRole')->value->getRoleID();?>
][no_user][txtComment]" />
						<?php }?>
					</td>
					<td class="actions" colspan="2">
					<?php  $_smarty_tpl->tpl_vars['objProcessTransition'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('objRole')->value->getProcessTransitionArray()->getArray(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['objProcessTransition']->key => $_smarty_tpl->tpl_vars['objProcessTransition']->value){
?>
						<input type="button" name="arrConditionInstances[<?php echo $_smarty_tpl->getVariable('objProcessTransition')->value->getProcessTransitionID();?>
][<?php echo $_smarty_tpl->getVariable('objRole')->value->getRoleID();?>
][no_user][strTransitionButton]" value="<?php echo $_smarty_tpl->getVariable('objProcessTransition')->value->getTransitionButton();?>
"
							<?php if ($_smarty_tpl->getVariable('blnOverrideWorkflow')->value){?>
								<?php $_smarty_tpl->tpl_vars['strJSCallbackFunction'] = new Smarty_variable('function(){}', null, null);?>
								<?php if ($_smarty_tpl->getVariable('objProcessTransition')->value->getJSCallbackFunction()){?>
									<?php $_smarty_tpl->tpl_vars['strJSCallbackFunction'] = new Smarty_variable($_smarty_tpl->getVariable('objProcessTransition')->value->getJSCallbackFunction(), null, null);?>
								<?php }?>
								onclick="workflowAction('<?php echo $_smarty_tpl->getVariable('objProcessInstance')->value->getProcessInstanceID();?>
', '<?php echo $_smarty_tpl->getVariable('objProcessTransition')->value->getProcessTransitionID();?>
', '<?php echo $_smarty_tpl->getVariable('objRole')->value->getRoleID();?>
', 'no_user', '<?php echo $_smarty_tpl->getVariable('objProcessInstance')->value->getProcessObject()->getProcessObjectClass();?>
', '<?php echo $_smarty_tpl->getVariable('objProcessInstance')->value->getProcessObject()->getID();?>
', $('#comments_<?php echo $_smarty_tpl->getVariable('objRole')->value->getRoleID();?>
_no_user').val(), <?php echo $_smarty_tpl->getVariable('strJSCallbackFunction')->value;?>
)"
							<?php }else{ ?>
								disabled="true"
							<?php }?>
						/>
					<?php }} ?>
					</td>
				</tr>
			<?php } ?>
		<?php }} else { ?>
		<?php } ?>
	</table>
</div>