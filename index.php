<?php
if (isset($_REQUEST['cmd']) and $_REQUEST['cmd'] == 'clearCache') {
	apc_clear_cache('user');
	apc_clear_cache();
}
require_once('includes/initialize.php');
//echo "<pre>"; print_r($_SESSION[APPLICATION]); echo "</pre>";
$page = $view.".php";
if ($cmd == "Save" or $cmd == "Delete" or ($cmd == "Logon" and (!isset($_SESSION[APPLICATION]['loggedIn']) or $_SESSION[APPLICATION]['loggedIn'] != true)) or ($view == "logoff" and $_SESSION[APPLICATION]['loggedIn'] == true)) {
	if (!isset($_REQUEST['updKey']) or $_REQUEST['updKey'] != $_SESSION[APPLICATION]['lastUpdKey']) {
		$targetPage = "process.".$page;
		if (array_key_exists('module',$_SESSION[APPLICATION]) and file_exists($_SESSION[APPLICATION]['module'].'/includes/'.$targetPage)) {
			include($_SESSION[APPLICATION]['module']."/includes/".$targetPage);
		} else {
			if (file_exists("includes/".$targetPage)) {
				include("includes/".$targetPage);
			} else {
				base::missingPage($targetPage);
			}
		}
		if (isset($_REQUEST['updKey'])) {
			$_SESSION[APPLICATION]['lastUpdKey'] = $_REQUEST['updKey'];
		}
	} else {
	    if (isset($_REQUEST['updKey']) and $_REQUEST['updKey'] == $_SESSION[APPLICATION]['lastUpdKey']) {
	        include ('includes/reDirect.php');
	    }
	}
}
if (!$haltProcessing) {
	if (array_key_exists('change',$_SESSION[APPLICATION]) and $_SESSION[APPLICATION]['change'] == 'Y') {
		$view = 'changePWD';
		$page = $view.'.php';
		$targetPage = $page;
		if (file_exists('controllers/'.$targetPage)) {
			include('controllers/'.$targetPage);
		} else {
			base::missingPage($targetPage);
		}
	} else {
		$page = $view.".php";
		$targetPage = $page;
		//if (isset($_SESSION[APPLICATION]['loggedIn']) and $_SESSION[APPLICATION]['loggedIn'] == true and !isset($_SESSION[APPLICATION]['targetView']) or $_SESSION[APPLICATION]['targetView'] != $view) {
		if (isset($_SESSION[APPLICATION]['loggedIn']) and $_SESSION[APPLICATION]['loggedIn'] == true and !isset($_SESSION[APPLICATION]['targetView']) or !array_key_exists('targetView',$_SESSION[APPLICATION]) or (is_array($_SESSION[APPLICATION]) and array_key_exists('targetView',$_SESSION[APPLICATION]) and $_SESSION[APPLICATION]['targetView'] != $view)) {
    		$_SESSION[APPLICATION]['targetView'] = $view;
		}
		if (array_key_exists('module', $_SESSION[APPLICATION]) and file_exists($_SESSION[APPLICATION]['module']."/controllers/".$targetPage)) {
			include($_SESSION[APPLICATION]['module']."/controllers/".$targetPage);
		} else {
			if (file_exists('controllers/'.$targetPage)) {
			  	include('controllers/'.$targetPage);
			} else {
				base::missingPage($targetPage);
			}
		}
	}
}
?>
