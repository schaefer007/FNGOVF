<?php
/* base class for common functions */
include_once 'classes/class.base.php';
/**
 * Dropdown class
 *
 * This class generates dropdown lists when given an array of option/value pairs,
 *  a dropdown list name and optionally the value currently selected.
 *
 * @author George L. Slater (Arbor Solutions, Inc)
 * @copyright 2009 - Arbor Solutions, Inc
 * @license GNU GPL
 * @version 1.0
 * @package Utility
 *
 */
class dropDown {
/**
 * Name of dropdown to generate
 *
 * @var string
 */
 protected $name;

 /**
  * Array of Option => Value pairs generated elsewhere
  *
  * @var array
  */
 protected  $ddArray;

 // Instantiate a Dropdown object, establish the name, data.

 /**
  * Constructor for dropDown class.
  *
  * Assign dropdown name, data.
  *
  * @param string $name
  * @param array $data
  * @return boolean
  */
 function __construct($name, $data) {
   if (isset($name)) {
     $this->name = $name;
   } else {
   // generate an error here.
   }
   if (isset($data)) {
     $this->ddArray = $data;
   } else {
   // generate an error here.
   }

   return true;
}

 // Handle cleanup of any objects created by this class.
/**
 * Destructor
 *
 * Perform cleanup of any objects created by this class.
 */
 function __destruct() {
 }


 // Build a dropdown and return output the results.
 /**
  * Build a dropdown list
  *
  * Build a dropdown list and output the results. Output is generated directly
  * to standard out.
  *
  * @param string $selectedValue
  */
 function bldDropDown($selectedValue, $required=true, $multiple=false, $AsAssoc=false) {

   $sizeText = '';
   $multipleText = '';
   if ($multiple != false and $multiple != 0) {
   	$multipleText = 'multiple';
   	if (is_int($multiple)) {
   		$sizeText = 'size="'.$multiple.'"';
   	}
   }

   $ddHTML = "<select name=\"".trim($this->name)."\" $multipleText $sizeText>\n";
   $ddHTML .= $this->getDropDownOptions($selectedValue, $required, $multiple, $AsAssoc);
   $ddHTML .= "</select>\n";

   return $ddHTML;
 }
 function getDropDownOptions($selectedValue, $required=true, $multOpt=false, $AsAssoc=false) {
 	if ($required) {
 		$optional = "";
 	} else {
 		$optional = "(optional)";
 	}
 	if ($multOpt) {
 		$multText = 'or more';
 	} else {
 		$multText = '';
 	}
    $optionHTML = "<option value=\"\">-- Select One $multText $optional --</option>";
 	if ($AsAssoc == true or base::is_assoc($this->ddArray)) {
     foreach ($this->ddArray as $Value => $Option) {
       if (trim($selectedValue) == trim($Value) or (count($this->ddArray) == 1 and $required == true)) {
         $selected = " selected";
       } else {
         $selected = "";
       }
       $optionHTML .= "<option value=\"".htmlspecialchars(trim($Value))."\"".$selected.">".htmlspecialchars(trim($Option))."</option>\n";
     }
   } else {
     foreach ($this->ddArray as $Option) {
     if (trim($selectedValue) == trim($Option)) {
     	$selected = " selected";
     } else {
     	$selected = "";
     }
     $optionHTML .= "<option".$selected.">".htmlspecialchars(trim($Option))."</option>\n";
   }
   }
   return $optionHTML;
 }
}
?>