<?php
require_once('classes/class.dropDown.php');
require_once('classes/class.radioButton.php');
require_once('classes/class.checkBox.php');
	/**
	 * base table Definitions.
	 *
	 * The table class defines all table interactions with the database.  This is an
	 * abstract class extended by actual table classes for use.  This table class
	 * uses information from the Db layer as well as the extending table classes to
	 * be fully aware of the table meta data and therefore can perform tasks such as
	 * editting data before performing table functions or creating output forms.
	 *
	 * @author George L. Slater (Arbor Solutions, Inc)
	 * @copyright 2011 - GRAR
	 * @version 1.0
	 * @package Db
	 */
abstract class table {

 /**
  * metaData array contains information about the table
  *
  * @var string
  */
 protected $metaData;

 /**
  * colum metaData array contains setup information about the table
  *
  * @var array
  */
 protected $columnMetaData;
 /**
  * Yes/No array to define valid values associated with a YN edit type
  *
  * @var array
  */
  protected $YNArray = array("Y"=>"Yes","N"=>"No");

  /**
  * tableName contains the name of the table being referenced
  *
  * @var array
  */
 protected $tableName;

   /**
  * resultRows contains the number of rows returned in the last result set
  *
  * @var int
  */
 protected $resultRows;

 /**
  * connection contains the connection to use throughout this class
  *
  * @var resource
  */
 protected $connection;

 protected $dbName;

 protected $db;
 	/**
	 * Constructor for table class.
	 *
	 * Sets the actual table name and instantiates an instance of the Db layer.
	 *
	 * @author George L. Slater (Arbor Solutions, Inc)
	 * @copyright 2011 - GRAR
	 * @version 1.0
	 * @package Db
	 */
  public function __construct() {
  	global $config;
  	global $cxn;
  	$this->columnMetaData = $this->fieldMetaData;
  	if (!isset($this->dbName) or empty($this->dbName)) {
  		$this->dbName = '*LOCAL';
  	}
  	if (isset($cxn[$this->dbName])) {
  		$this->connection = $cxn[$this->dbName];
  	} else {
  		$connDb = new db();
  		$cxn[$this->dbName] = $connDb->get_Db_Connection($this->dbName);
  		$this->connection = $cxn[$this->dbName];
  		unset($connDb);
  	}
  	$this->db = new db();
  	$this->db->connect($this->connection);
  	$this->tableName = get_class($this);
  	if ($this->thisIsQuoted) {
  		$this->tableName = '"'.strtolower($this->tableName).'"';
  	}
  	if (!isset($this->schema)) {
  		$this->schema = $config['schema'];
  	}
  	if (file_exists('includes/txtInfo.txt')) {
  		$this->encryptionKey = file_get_contents('includes/txtInfo.txt');
  	}
  }
	/**
	 * Destructor for table class.
	 *
	 * Perform cleanup operations.
	 *
	 * @author George L. Slater (Arbor Solutions, Inc)
	 * @copyright 2011 - GRAR
	 * @version 1.0
	 * @package Db
	 */
  public function __destruct() {
  	unset ($this->db);
  }
	/**
	 * Insert a row into the current table.
	 *
	 * Performs validity checking of all input fields.  Builds the insert statement based on
	 * the results of the validity checking, the fields provided in the Request array and
	 * the table meta data.  Then sends the statement to the db layer for execution.
	 *
	 * @author George L. Slater (Arbor Solutions, Inc)
	 * @copyright 2011 - GRAR
	 * @version 1.0
	 * @package Db
	 *
	 * @return bool
	 */
  public function insert($validate=true) {
  	if (!isset($this->db) or $this->db == null) {
  		$this->db = new db();
  		$this->db->connect($this->connection);
  	}
  	if ($validate == false or $this->infoIsValid('INSERT')) {
  		$delimiter = "";
  		$Fields = "";
  		$values = "";
  		foreach ($this->metaData['COLUMNS'] as $columnName => $columnData) {
  			if (array_key_exists($columnName, $_REQUEST) and isset($_REQUEST[$columnName])) {
  				if (array_key_exists('ISNUMERIC', $columnData) and $columnData['ISNUMERIC']) {
  					if ($_REQUEST[$columnName] != "" and is_numeric($_REQUEST[$columnName])) {
  						$values .= $delimiter.$_REQUEST[$columnName];
  					} else {
  						$values .= $delimiter."0";
  					}
  				} else {
  					if (array_key_exists($columnName,$this->fieldMetaData) and
  					     $this->fieldMetaData[$columnName] == "ALPHA_UC") {
  					   	$value = strtoupper($_REQUEST[$columnName]);
  					} else {
  					   	$value = $_REQUEST[$columnName];
  					}
  					if (isset($this->fieldMetaData[$columnName]) and is_array($this->fieldMetaData[$columnName]) and array_key_exists('ENCRYPTED',$this->fieldMetaData[$columnName]) and $this->fieldMetaData[$columnName]['ENCRYPTED'] and !empty($this->encryptionKey)) {
  						$value = "ENCRYPT_RC2('$value','$this->encryptionKey')";
  						$values .= $delimiter.trim($value);
  					} else {
  						$value = str_replace('\'','\'\'',$value);
  						$values .= $delimiter."'".trim($value)."'";
  					}
  				}
  				if ($this->thisIsQuoted) {
  					$Fields .= $delimiter.'"'.$columnName.'"';
  				} else {
  					$Fields .= $delimiter.$columnName;
  				}
  				$delimiter = ", ";
  			}
  		}
  		error_log('Inserting into '.$this->tableName.', database '.$this->dbName.', Schema '.$this->schema);
  		$query = "insert into ".trim($this->schema).".$this->tableName ($Fields) values($values)";
  		try {
  			$result = $this->db->execute($query);
  		} catch (Exception $e) {
  			base::error_handler($e);
  		}

  		return $result;
  	} else {
   		return false;
  	}
  }
  	/**
	 * Update a row in the current table.
	 *
	 * Performs validity checking of all input fields.  Builds the update statement based on
	 * the results of the validity checking, the fields provided in the Request array and
	 * the table meta data.  Then sends the statement to the db layer for execution.
	 *
	 * @author George L. Slater (Arbor Solutions, Inc)
	 * @copyright 2011 - GRAR
	 * @version 1.0
	 * @package Db
	 *
	 * @return bool
	 */
  public function update($validate=true) {
  	if (!isset($this->db) or $this->db == null) {
  		$this->db = new db();
  		$this->db->connect($this->connection);
  	}
  	if ($validate == false or $this->infoIsValid('UPDATE')) {
  		$where = "";
  		$value = "";
  		$Fields = "";
  		$delimiter = "";
	  	foreach ($this->metaData['COLUMNS'] as $columnName => $columnData) {
	  		if (isset($_REQUEST[$columnName]) and
	  				(!isset($this->metaData['COLUMNS'][$columnName]['IDENTITY']) or
	  						$this->metaData['COLUMNS'][$columnName]['IDENTITY'] != 'YES')) {
	  			if ($columnData['ISNUMERIC']) {
	  				if ($_REQUEST[$columnName] == "") {
	  				   if ($this->thisIsQuoted) {
	  				   		$Fields .= $delimiter."\"$columnName\" = '0'";
	  				   } else {
                       		$Fields .= $delimiter."$columnName = '0'";
	  				   }
	  				} else {
	  				   if ($this->thisIsQuoted) {
	  				   		$Fields .= $delimiter."\"$columnName\" = $_REQUEST[$columnName]";
	  				   } else {
	  				   		$Fields .= $delimiter."$columnName = $_REQUEST[$columnName]";
	  				   }
	  				}
	  			} else {
	  			    if (array_key_exists($columnName,$this->fieldMetaData) and
	  			    	   	$this->fieldMetaData[$columnName] == "ALPHA_UC") {
       					$value = strtoupper($_REQUEST[$columnName]);
     				} else {
       					$value = $_REQUEST[$columnName];
     				}
     				if (isset($this->fieldMetaData[$columnName]) and
     						is_array($this->fieldMetaData[$columnName]) and
     						array_key_exists('ENCRYPTED',$this->fieldMetaData[$columnName]) and
     						$this->fieldMetaData[$columnName]['ENCRYPTED'] and
     						!empty($this->encryptionKey)) {
     					$value = "ENCRYPT_RC2('$value','$this->encryptionKey')";
     					if ($this->thisIsQuoted) {
     						$Fields .= $delimiter."\"$columnName\" = $value";
     					} else {
     						$Fields .= $delimiter."$columnName = $value";
     					}
     				} else {
     					$value = str_replace('\'','\'\'',$value);
     					if ($this->thisIsQuoted) {
     						$Fields .= $delimiter."\"$columnName\" = '".$value."'";
     					} else {
	  						$Fields .= $delimiter."$columnName = '".$value."'";
     					}
     				}
	  			}
	  			$delimiter = ", ";
	  		}
	  	}
	  	$delimiter = "";
	  	foreach ($this->metaData['KEYS'] as $keyField) {
	  		if ($this->metaData['COLUMNS'][$keyField]['ISNUMERIC']) {
	  			if (isset($_REQUEST['old_'.$keyField]) and $_REQUEST['old_'.$keyField] != "") {
	  				if ($this->thisIsQuoted) {
	  					$where .= $delimiter." \"$keyfield\" = ".$_REQUEST['old_'.$keyField];
	  				} else {
	  					$where .= $delimiter." $keyField = ".$_REQUEST['old_'.$keyField];
	  				}
	  			} else {
	  				if ($this->thisIsQuoted) {
	  					$where .= $delimiter." \"$keyField\" = $_REQUEST[$keyField]";
	  				} else {
	  					$where .= $delimiter." $keyField = $_REQUEST[$keyField]";
	  				}
	  			}
	  		} else {
	  			if (isset($_REQUEST['old_'.$keyField]) and $_REQUEST['old_'.$keyField] != "") {
	  				$value = $_REQUEST['old_'.$keyField];
	  			} else {
	  				$value = $_REQUEST[$keyField];
	  			}
	  			$value = str_replace('\'','\'\'',$value);
	  			if ($this->thisIsQuoted) {
	  				$where .= $delimiter." \"$keyField\" = '".$value."'";
	  			} else {
	  				$where .= $delimiter." $keyField = '".$value."'";
	  			}
	  		}
	  		$delimiter = " and ";
	  	}
	  	error_log('Updating '.$this->tableName.', database '.$this->dbName.', Schema '.$this->schema);
	    $query = "update ".trim($this->schema).".$this->tableName set $Fields where $where";
	    try {
	    	$result = $this->db->execute($query);
	    } catch (Exception $e) {
  			base::error_handler($e);
			echo "<pre>";
			print_r($e);
			echo "</pre>";
  		}
	    return $result;
  	} else {
  		return false;
  	}
  }
  /**********************************************
  	/**
	 * Update all matching rows in the current table.
	 *
	 * Performs validity checking of all input fields.  Builds the update statement based on
	 * the results of the validity checking, the fields provided in the Request array and
	 * the table meta data.  Then sends the statement to the db layer for execution.
	 *
	 * @author George L. Slater (Arbor Solutions, Inc)
	 * @copyright 2011 - GRAR
	 * @version 1.0
	 * @package Db
	 *
	 * @return bool
	 */
  public function updateAll($setArray=null,$parms=null) {
  	if (!isset($this->db) or $this->db == null) {
  		$this->db = new db();
  		$this->db->connect($this->connection);
  	}
  	if (isset($setArray) and $setArray != null) {
  		$where = "";
  		$value = "";
  		$Fields = "";
  		$delimiter = "";
	  	foreach ($this->metaData['COLUMNS'] as $columnName => $columnData) {
	  		if (array_key_exists($columnName,$setArray)) {
	  			if ($columnData['ISNUMERIC']) {
	  				if ($setArray[$columnName] == "") {
                       $Fields .= $delimiter."$columnName = null";
	  				} else {
	  				   $Fields .= $delimiter."$columnName = $setArray[$columnName]";
	  				}
	  			} else {
	  			    if (array_key_exists($columnName,$this->fieldMetaData) and
	  			    	   	$this->fieldMetaData[$columnName] == "ALPHA_UC") {
       					$value = strtoupper($setArray[$columnName]);
     				} else {
       					$value = $setArray[$columnName];
     				}
	  				$value = str_replace('\'','\'\'',$value);
	  				$Fields .= $delimiter."$columnName = '".$value."'";
	  			}
	  			$delimiter = ", ";
	  		}
	  	}
	  	$delimiter = "";
  		if ($parms != null and array_key_exists('WHERE',$parms)) {
	  		$where = $this->build_where(null, $parms['WHERE']);
		} else {
      		$where = "";
  		}
	    $query = "update ".trim($this->schema).".$this->tableName set $Fields $where";
	    $result = $this->db->execute($query);
	    return $result;
  	} else {
  		return false;
  	}
  }
  	/**
	 * Delete all matching rows in the current table.
	 *
	 * Performs validity checking of all input fields.  Builds the delete statement based on
	 * the results of the validity checking, the fields provided in the Request array and
	 * the table meta data.  Then sends the statement to the db layer for execution.
	 *
	 * @author George L. Slater (Arbor Solutions, Inc)
	 * @copyright 2011 - GRAR
	 * @version 1.0
	 * @package Db
	 *
	 * @return bool
	 */
  public function deleteAll($parms=null) {
  	if (!isset($this->db) or $this->db == null) {
  		$this->db = new db();
  		$this->db->connect($this->connection);
  	}
  		$where = "";
  		$delimiter = "";
  		if ($parms != null and array_key_exists('WHERE',$parms)) {
	  		$where = $this->build_where(null, $parms['WHERE']);
		} else {
      		$where = "";
  		}
	    $query = "delete from ".trim($this->schema).".$this->tableName $where";
	    $result = $this->db->execute($query);
	    return $result;
  }
  /**********************************************
  	/**
	 * Delete a row from the current table.
	 *
	 * Performs validity checking of all key fields.  Builds the delete statement based on
	 * the results of the validity checking, the fields provided in the Request array and
	 * the table meta data.  Then sends the statement to the db layer for execution.
	 *
	 * @author George L. Slater (Arbor Solutions, Inc)
	 * @copyright 2011 - GRAR
	 * @version 1.0
	 * @package Db
	 *
	 * @return bool
	 */
  public function delete($validate=true) {
  	if (!isset($this->db) or $this->db == null) {
  		$this->db = new db();
  		$this->db->connect($this->connection);
  	}
  	if ($validate == false or $this->infoIsValid('DELETE')) {
  		$delimiter = "";
  		foreach ($this->metaData['KEYS'] as $Field) {
  			if (isset($_REQUEST['old_'.$Field]) and $_REQUEST['old_'.$Field] != "") {
  				$value = $_REQUEST['old_'.$Field];
  			} else {
  				$value = $_REQUEST[$Field];
  			}
  			if ($this->metaData['COLUMNS'][$Field]['ISNUMERIC']) {
  				if ($this->thisIsQuoted) {
  			        $where .= $delimiter."\"$Field\" = $value";
  				} else {
  					$where .= $delimiter."$Field = $value";
  				}
  			} else {
  				$value = str_replace('\'','\'\'',$value);
  				if ($this->thisIsQuoted) {
  					$where .= $delimiter."\"$Field\" = '".$value."'";
  				} else {
  					$where .= $delimiter."$Field = '".$value."'";
  				}
  			}
  			$delimiter = " and ";
  		}
		$query = "delete from ".trim($this->schema).".$this->tableName where $where";
		try {
    		$result = $this->db->execute($query);
		} catch (Exception $e) {
  			base::error_handler($e);
  		}
    	return $result;
  	} else {
  		return false;
  	}
  }
  	/**
	 * Select rows from the current table.
	 *
	 * Performs validity checking of all input fields.  Builds the select statement based on
	 * the results of the validity checking, the fields provided in the Request array and
	 * the table meta data.  Then sends the statement to the db layer for execution.  The
	 * function can optionally accept a list of fields to select, and a list of parameters
	 * that define the selection information to use in selecting certain rows.
	 *
	 * @author George L. Slater (Arbor Solutions, Inc)
	 * @copyright 2011 - GRAR
	 * @version 1.0
	 * @package Db
	 *
	 * @param array $fields
	 * @param array $parms
	 * @return bool
	 */
  public function select(array $fields=null, array $parms=null, $whereParm=null) {
  	if (!isset($this->db) or $this->db == null) {
  		$this->db = new db();
  		$this->db->connect($this->connection);
  	} else {
  		$this->db->reset();
  		$this->db->connect($this->connection);
  	}
  	if (!isset($this->metaData)) {
  		$this->getTableMeta();
  	}
    if ($fields != null) {
   		$fieldList = implode(',',$fields);
    } else {
    	$fieldList = '*';
    }

	if ($parms != null and array_key_exists('WHERE',$parms)) {
	  $where = $this->build_where($fields, $parms['WHERE']);
	} else {
      $where = "";
  	}
  	if (isset($parms['DISTINCT']) and $parms['DISTINCT']) {
  		$distinct = "distinct";
  	} else {
  		$distinct = "";
  	}
  	if ($parms != null and array_key_exists('ORDERBY',$parms)) {
  		$sortBy = $parms['ORDERBY'];
  		$delim = 'order by ';
  		$orderBy = "";
  		foreach ($sortBy as $sortField) {
  		//	if ($fields == null or in_array($sortField,$fields)) {
  			    $orderBy .= $delim.$sortField;
  			    $delim = ",";
  		//	}
  		}
  	} else {
  		if (!isset($whereParm) or strPos(strtoupper($whereParm),'ORDER BY') === false) {
  			$delim = "order by ";
 	 		$orderBy = "";
 	 		if (array_key_exists('KEYS',$this->metaData)) {
 		 		foreach ($this->metaData['KEYS'] as $key) {
 		 			if (($fields == null or in_array($key,$fields) and !empty($key))) {
 		 				if ($this->thisIsQuoted) {
 		 					$orderBy .= $delim.'"'.$key.'"';
 		 				} else {
  							$orderBy .= $delim.$key;
 		 				}
  						$delim = ",";
  					}
	 	 		}
 	 		}
 	 	}
  	}
  	if (isset($whereParm) and !empty($whereParm)) {
  		$where = $whereParm;
  	}
  	$query = "select $distinct $fieldList from ".trim($this->schema).".$this->tableName $where $orderBy";
  	try {
  	$result = $this->db->execute($query);
  	}
  	catch (Exception $e) {
  		$result = false;
  	}
   	return $result;
  }
  	/**
	 * Get the next row from the result set.
	 *
	 * Retrieves the row and returns it to the calling script for processing.
	 *
	 * @author George L. Slater (Arbor Solutions, Inc)
	 * @copyright 2011 - GRAR
	 * @version 1.0
	 * @package Db
	 *
	 * @return array
	 */
  public function getnext() {
    $row = $this->db->getNextAssoc();
    return $row;
  }
  public function getnextbysequence() {
  	$row = $this->db->getNextRow();
  	return $row;
  }
  	/**
	 * Return the number of rows affected by a DB operation.
	 *
	 * This function only returns row counts for Insert, Update, and Delete operations.
	 * The number or rows in a result set following a select are handled differently.
	 *
	 * @author George L. Slater (Arbor Solutions, Inc)
	 * @copyright 2011 - GRAR
	 * @version 1.0
	 * @package Db
	 *
	 * @return int
	 */
  public function count() {
	$num_Rows = $this->db->count();
	return $num_Rows;
	}
		/**
	 * Return the number of rows in a result set.
	 *
	 * This function returns the number of rows in a result set to the calling script.
	 *
	 * @author George L. Slater (Arbor Solutions, Inc)
	 * @copyright 2011 - GRAR
	 * @version 1.0
	 * @package Db
	 *
	 * @return int
	 */
  public function resultCount() {
    $num_Rows = $this->resultRows;
    return $num_Rows;
  }
  	/**
	 * Build a where clause for use in a select, update, or delete statement.
	 *
	 * @author George L. Slater (Arbor Solutions, Inc)
	 * @copyright 2011 - GRAR
	 * @version 1.0
	 * @package Db
	 *
	 * @param $array $fields
	 * @param $array $parms
	 * @return string
	 */
  private function build_where($fields, $parms) {
	if (!isset($this->metaData)) {
	 	$this->getTableMeta();
	}
	$delimiter = "where ";
	$where = "";
	if (isset($parms) and is_array($parms)) {
		foreach ($parms as $key=>$value) {
		  if ($this->thisIsQuoted and substr($key,0,1) == '"') {
		  	$testArray = explode('"',$key);
		  	$testKey = $testArray[1];
		  } else {
		  	$testKey = $key;
		  }
		  if (array_key_exists($testKey,$this->metaData['COLUMNS'])) {
		  	if ($this->metaData['COLUMNS'][$testKey]['ISNUMERIC']) {
		  		if (is_array($value)) {
		  			if ($this->thisIsQuoted) {
		  				$where .= $delimiter." \"$key\" in (";
		  			} else {
		  				$where .= $delimiter." $key in (";
		  			}
		  			$elemDelim = '';
		  			foreach ($value as $elem) {
		  				$where .= $elemDelim.$elem;
		  				$elemDelim = ',';
		  			}
		  			$where .= ')';
		  		} else {
		  		  if ($this->thisIsQuoted) {
		  		  	$where .= $delimiter."\"$key\"=$value";
		  		  } else {
		  		  	$where .= $delimiter."$key=$value";
		  		  }
		  		  $delimiter = " and ";
		  		}
		  	} else {
		  		if (is_array($value)) {
		  			$value = str_replace('\'','\'\'',$value);
		  			if ($this->thisIsQuoted) {
		  				$where .= $delimiter." \"$key\" in (";
		  			} else {
		  				$where .= $delimiter." $key in (";
		  			}
		  			$elemDelim = '';
		  			foreach ($value as $elem) {
		  				$where .= $elemDelim."'".$elem."'";
		  				$elemDelim = ',';
		  			}
		  			$where .= ')';
		  			$delimiter = " and ";
		  		} else {
		  			$value = str_replace('\'','\'\'',$value);
		  			if (!isset($this->metaData['KEYS']) or !in_array($key,$this->metaData['KEYS'])) {
		  		//		$value = strtoupper($value).'%';
		  		        $value = strtoupper($value);
		  		//		$where .= $delimiter."UCASE($key) like '".$value."'";
		  		        if ($this->thisIsQuoted) {
		  		        	$where .= $delimiter."UCASE(\"$key\") = '".$value."'";
		  		        } else {
		  		        	$where .= $delimiter."UCASE($key) = '".$value."'";
		  		        }
		  			} else {
		  				if ($this->thisIsQuoted) {
		  					$where .= $delimiter."\"$key\"='".$value."'";
		  				} else {
		  					$where .= $delimiter."$key='".$value."'";
		  				}
		  			}
					$delimiter = " and ";
		  		}
		  	}
 		  }
		}
	}
	$subSelect = "";
	if (isset($parms) and is_array($parms) and array_key_exists('UNIQUE',$parms) and array_key_exists('UNIQUEBY')) {
		$subSelect = $delimiter."$fields[0] not in (Select {$parms['UNIQUECOLUMN']} from {$parms['UNIQUETABLE']}";
		$subDelim = " where {$fields[0]} <> {$parms['UNIQUECURRENT']} and ";
		foreach ($parms['UNIQUEBY'] as $key => $value) {
			$subSelect .= $subDelim." $key = $value";
			$subDelim = " and ";
		}
		if ($subDelim == " and ") {
			$subSelect.=")";
		}

	}
	return $where.$subSelect;
  }
  	/**
	 * retrieve table meta data.
	 *
	 * This data is retrieved when necessary and stored for use by validation routines, Db
	 * operations, and form generation.
	 *
	 * @author George L. Slater (Arbor Solutions, Inc)
	 * @copyright 2011 - GRAR
	 * @version 1.0
	 * @package Db
	 *
	 * @return bool
	 */
public function getTableMeta() {
$this->metaData = apc_fetch($this->tableName.'_Meta',$inCache);
//$inCache = false;
if (!$inCache) {
$numArray = array('INTEGER','SMALLINT','BIGINT','FLOAT','DECFLOAT','NUMERIC','DECIMAL','REAL');
$metaDB = new db();
$metaDB->connect($this->connection);
if (substr($this->schema,0,1) == '"') {
	$metaArray = explode('"',$this->schema);
	$metaSchema = $metaArray[1];
} else {
	$metaSchema = strtoupper($this->schema);
}
if (substr($this->tableName,0,1) == '"') {
	$metaArray = explode('"',$this->tableName);
	$metaTable = $metaArray[1];
} else {
	$metaTable = strtoupper($this->tableName);
}
$result = $metaDB->getColumnMeta($metaSchema,$metaTable);
if (!$result) {
    return false;
} else {
	/* get column metadata */
	while ($meta = $metaDB->getNextAssoc()) {
		$meta['TYPE_NAME'] = trim($meta['TYPE_NAME']);
	    if (in_array(trim($meta['TYPE_NAME']),$numArray)) {
            $meta['NUMERIC'] = true;
              if (array_key_exists('LENGTH_PRECISION',$meta) and !empty($meta['LENGTH_PRECISION'])) {
            	$meta['COLUMN_SIZE'] = $meta['LENGTH_PRECISION'];
            }
            if (!array_key_exists('SCALE',$meta)) {
            	$meta['SCALE'] = $meta['NUM_SCALE'];
            }
            if (!array_key_exists('DECIMAL_DIGITS',$meta)) {
            	$meta['DECIMAL_DIGITS'] = $meta['NUM_SCALE'];
            }
            $meta['SCALE'] = $meta['COLUMN_SIZE'];
		} else {
			if (!$meta['CHAR_OCTET_LENGTH'] and $meta['TYPE_NAME'] == 'TIMESTMP') {
				$meta['COLUMN_SIZE'] = 26;
			}
			if (!$meta['CHAR_OCTET_LENGTH'] and $meta['TYPE_NAME'] == 'TIME') {
				$meta['COLUMN_SIZE'] = 8;
			}
			if (!array_key_exists('COLUMN_SIZE',$meta)) {
			    $meta['COLUMN_SIZE'] = $meta['CHAR_OCTET_LENGTH'];
			}

			$meta['DECIMAL_DIGITS'] = "";
			$meta['SCALE'] = "";
			$meta['NUMERIC'] = false;
		}

	    $this->metaData['COLUMNS'][$meta['COLUMN_NAME']] = array ('DATA_TYPE'=>$meta['TYPE_NAME'],
   		                                                 'ISNUMERIC'=>$meta['NUMERIC'],
   	    											 	 'LENGTH'=>$meta['COLUMN_SIZE'],
   	    											 	 'NUMERIC_SCALE'=>$meta['SCALE'],
         											 	 'NUMERIC_PRECISION'=>$meta['DECIMAL_DIGITS'],
                                                     	 'NOTNULL'=>!$meta['NULLABLE'],
                                                      	 'UNSIGNED'=>'',
	    		                                         'IDENTITY'=>$meta['IS_IDENTITY']);
	  }
	  if ($columnText = $metaDB->getColText($metaSchema,$metaTable)) {
	  	foreach ($columnText as $textKey=>$textRow) {
	  		$this->metaData['COLUMNS'][$textKey]['TEXT'] = $textRow['COLUMN_TEXT'];
	  	}
	  }
	  try {
	  if ($keys = $metaDB->getSQLPrimaryKeys($metaSchema, $metaTable)) {
	   		while ($keysInfo = $metaDB->getNextAssoc()) {
	   			$this->metaData['KEYS'][] = $keysInfo['COLUMN_NAME'];
	   		}
	  	}
	  } catch (Exception $e) {
	  	base::error_handler($e);
	  }
	  if (!array_key_exists('KEYS', $this->metaData)) {
	  	try {
		  	if ($keys = $metaDB->getDDSPrimaryKeys($metaSchema, $metaTable)) {
		  	  	if (is_array($keys)) {
		  			foreach ($keys as $value) {
		  				$this->metaData['KEYS'][] = trim($value);
		  			}
		  		}
			}
	    } catch (Exception $e) {
		  	base::error_handler($e);
		}
	  }

	if (isset($metaDB)) {
		unset($metaDB);
	}
}
apc_store($this->tableName.'_Meta',$this->metaData);
}
}
	/**
	 * Build an array of table meta data.
	 *
	 * This function uses meta data retrieved from the db as well as field metadata
	 * instantiated from the table class to build an array of information.
	 *
	 * @author George L. Slater (Arbor Solutions, Inc)
	 * @copyright 2011 - GRAR
	 * @version 1.0
	 * @package Db
	 *
	 * @param string $table
	 * @param string $valColumn
	 * @param string $selColumn
	 * @param array $options
	 * @return array
	 */
private function bldTableMetaValuesArray($table, $valColumn, $selColumn=null, $options=null) {
	//if (!isset($this->db) or $this->db == null) {
  //		$this->db = new db();
  //		$this->db->connect($this->connection);
//	}
	if (!class_exists($table)) {
		require_once("classes/class.$table.php");
	}
	$selTable = new $table();

	if ($selColumn != null) {
		$selFields = array($valColumn,$selColumn);
	} else {
		$selFields = array($valColumn);
	}
	$options['DISTINCT'] = 1;
	try{
	$result = $selTable->select($selFields,$options);
	$resultArray = array();
	while ($row = $selTable->getnextbysequence()) {
		if ($selColumn == null) {
		  $resultArray[]= trim($row[0]);
		} else {
		  $resultArray[trim($row[0])] = trim($row[1]);
		}
	}}
	catch(Exception $e) {

	}
	unset($selTable);
	return $resultArray;
}

/**
 * Validate a column based on it's type and defined editting characteristics
 *
 * validates a specific field value as provided in the $_REQUEST array in accordance
 * with the type of column and any editting information provided in the $updFields global
 * array.
 *
 * @param string $columnName
 * @param string $columnData
 *
 * @return bool
 */
private function isValid($columnName, $columnData) {
  global $errorText;
  global $errorColumn;
  $regInt = "/^[0-9]*$/";
  $regNum = "/^[0-9.]*$/";
  error_log($columnName.' data is -'.$columnData.'-');
  // Test for required fields that are missing (in the actual table class an array
  // of optional fields exists.  Those fields may b eleft blank.  All others require
  // a value.
  if ((!isset($this->fieldOptional) or
		!in_array($columnName,$this->fieldOptional)) and
		(!isset($_REQUEST[$columnName]) or trim($_REQUEST[$columnName]) == "")) {
  			if (!empty($this->metaData['COLUMNS'][$columnName]['TEXT'])) {
  				$errorText[] = $this->metaData['COLUMNS'][$columnName]['TEXT']." must be provided.";
  				$errorColumn[] = trim($columnName);
  			} else {
				$errorText[] = str_replace("_","&nbsp;",$columnName)." must be provided.";
				$errorColumn[] = trim($columnName);
  			}
  } else {
  	if (!isset($this->fieldOptional) or !in_array($columnName,$this->fieldOptional) or (isset($_REQUEST[$columnName]) and trim($_REQUEST[$columnName]) != "")) {
	// If fieldMetaData is defined, and this field exists in that array, edit based on those settings.
	if (isset($this->fieldMetaData) and array_key_exists($columnName,$this->fieldMetaData)) {
      switch ($this->fieldMetaData[$columnName]) {
      // Email address
      case "EMAIL"  : if (!base::check_email_address(trim($columnData))) {
      	                 return false;
      				  } else {
      					 return true;
      				  }
      				  break;
      // Integer - only digits are allowed and length must be within limits.
      case "INT"	: if (!preg_match($regInt,$columnData)) {
      					 return false;
      				  } elseif (($this->metaData['COLUMNS'][$columnName]['NUMERIC_SCALE'] != NULL and strlen($columnData) > $this->infoData['COLUMNS'][$columnName]['NUMERIC_SCALE']) or
      				             $this->metaData['COLUMNS'][$columnName]['NUMERIC_SCALE'] == NULL and strlen($columnData) > $this->infoData['COLUMNS'][$columnName]['LENGTH']) {
      				  	return false;
      				  } else {
      				     return true;
      				  }
      				  break;
      // Numeric - only digits and a decimal point are allowed and length of whole number
      //  and length of decimal positions must be within limits
      case "NUM"    : if (!preg_match($regNum,$columnData)) {
                         return false;}
                      if ($this->metaData['COLUMNS'][$columnName]['ISNUMERIC']) {
                      	if (str_pos(".",$columnData)) {
                      		$numArray = split(".",$columnData);
                       		if (strlen($numArray[0]) > $this->metaData['COLUMNS'][$columnName]['NUMERIC_SCALE']) {
                          		return false;}
                        	if (strlen($numArray[1]) > $this->metaData['COLUMNS'][$columnName]['NUMERIC_PRECISION']) {
                          		return false;}
                        	return true;
                      	} else {
                      		if (strlen($columnData) > $this->metaData['COLUMNS'][$columnName]['NUMERIC_SCALE']) {
                        		return false;
                      		} else {
                        		return true;
                      		}
                      		break;
                      	}
                      } else {
                      	if (strlen($columnData) > $this->metaData['COLUMNS'][$columnName]['LENGTH']) {
                      		return false;
                      	}
                      }
      // Yes/No selection field.  Value must be in YN Array.
      case "YN"		: if (!array_key_exists($columnData,$this->YNArray)) {
                      	return false;
      				  } else {
      					return true;
      				  }
      				  break;
      // Default is either an array validation, a table lookup, or a character field.  If the
      //  edit type is an array, then make sure the value supplied exists in the editting array.
      //  If the edit array is an associative array, then check for a key of FROMFILE.  If found
      //  then the validation is a table lookup and must contain a VALCOLUMN key at a minimum to
      //  define the table column from which lookup values are taken.  Optional keys are:
      //  SELCOLUMN in case there is a value to go with the selection, and WHERE to define @author gslater
      //  list of key=>value pairs used to select certain rows out of the FROMTABLE.
      //  Otherwise test the length of the field to see that it is within limits.

      //  FROMTABLE Example:     $fieldMetaData = array("FIELD1"=>array("FROMTABLE"=>"COURSE",
      //                                                                "SELCOLUMN"=>"NAME",
      //				                                                "VALCOLUMN"=>"DESCRIPTION"),
      //      Where Example:                            "FIELD2"=>array("FROMTABLE"=>"QUESTION",
      //                                                                "SELCOLUMN"=>"ID",
      //                                                                "VALCOLUMN"=>"DESCRIPTION",
      //                                                                "WHERE"=>array("TYPE"=>"M"));

      default		: if (is_array($this->fieldMetaData[$columnName]) and count($this->fieldMetaData[$columnName]) > 0) {
      	                if (base::is_assoc($this->fieldMetaData[$columnName]) and
      	                     array_key_exists('FROMTABLE',$this->fieldMetaData[$columnName]) and
      	                     array_key_exists('VALCOLUMN',$this->fieldMetaData[$columnName]) and
      	                	!array_key_exists('VALUES',$this->fieldMetaData[$columnName])) {
      	                   	if (array_key_exists('SELCOLUMN',$this->fieldMetaData[$columnName])) {
      	                   		$selColumn = $this->fieldMetaData[$columnName]['SELCOLUMN'];
      	                   	} else {
      	                   		$selColumn = null;
      	                   	}
      	                   	if (array_key_exists('WHERE',$this->fieldMetaData[$columnName])) {
      	                   		$where = $this->fieldMetaData[$columnName]['WHERE'];
      	                   		foreach ($where as $key => $value) {
      	                   			if (isset($this->metaData['COLUMNS'][$key])) {
      	                   				if ($this->metaData['COLUMNS'][$key]['ISNUMERIC']) {
      	                   					if (isset($_REQUEST[$key])) {
      	                   						$where[$key] = $_REQUEST[$key];
      	                   					}
      	                   				} else {
      	                   					if (isset($_REQUEST[$key])) {
      	                   						$where[$key] = "{$_REQUEST[$key]}";
      	                   					}
      	                   				}
      	                   			}
      	                   		}
      	                   	    if (array_key_exists('UNIQUE',$where)) {
      								$where['UNIQUETABLE'] = $this->tableName;
      	    						$where['UNIQUECOLUMN'] = $columnName;
      	    						if (array_key_exists('ISNUMERIC',$this->metaData[$columnName]) and $this->metaData[$columnName]['ISNUMERIC']) {
      	    							$where['UNIQUECURRENT'] = $columnData;
      	    						} else {
      	    							$where['UNIQUECURRENT'] = "\"$columnData\"";
      	    						}
      	    						foreach ($where['UNIQUEBY'] as $key => $value) {
      	    							if (isset($_REQUEST[$key])) {
      	    								if ($this->metaData['COLUMNS'][$key]['ISNUMERIC']) {
      	    									$where['UNIQUEBY'][$key] = $_REQUEST[$key];
      	        							} else {
      	        								$where['UNIQUEBY'][$key] = "\"{$_REQUEST[$key]}\"";
      	        							}
      	        						}
  									}
  								}
      	                   	} else {
      	                   		$where = null;
      	                   	}
      	                   	$this->fieldMetaData[$columnName]['VALUES'] =
      	                   	   $this->bldTableMetaValuesArray($this->fieldMetaData[$columnName]['FROMTABLE'],
      	                   	       $this->fieldMetaData[$columnName]['VALCOLUMN'], $selColumn, array('WHERE'=>$where));
      	                   	if ($columnData and $columnData != "") {
      	                    if (!$selColumn) {
     							if (!in_array(trim($columnData),$this->fieldMetaData[$columnName]['VALUES'])) {
     								$this->fieldMetaData[$columnName]['VALUES'] = array_merge(array(trim($columnData)),$this->fieldMetaData[$columnName]['VALUES']);
     							}
     						} else {
     							if (!array_key_exists(trim($columnData), $this->fieldMetaData[$columnName]['VALUES'])) {
     								$this->fieldMetaData[$columnName]['VALUES'] = array_merge(array(trim($columnData)=>trim($columnData)),$this->fieldMetaData[$columnName]['VALUES']);
								//	$this->fieldMetaData[$columnName]['VALUES'] = array(trim($columnData)=>trim($columnData)) + $this->fieldMetaData[$columnName]['VALUES'];
      	                   	    }
 							}
      	                   	}
      	                }
      	                if (array_key_exists('VALUES',$this->fieldMetaData[$columnName]) and count($this->fieldMetaData[$columnName]['VALUES']) > 0) {
	      	                if (base::is_assoc($this->fieldMetaData[$columnName]['VALUES'])) {
	      	                    if (array_key_exists($columnData,$this->fieldMetaData[$columnName]['VALUES'])) {
	      	                    	return true;
	      	                    } else {
	      	                    	return false;
	      	                    }
	      					} else {
	      					   	if (isset($storedJS)) {
	      	                		$this->fieldMetaData[$columnName]['JS'] = $storedJS;
	      	                	}
	      	                	error_log('column '.$columnName.' data is -'.$columnData.'-');
	      	                	if (!in_array(trim($columnData),$this->fieldMetaData[$columnName]['VALUES']) and !array_key_exists($columnData,$this->fieldMetaData[$columnName]['VALUES'])) {
	                        	    return false;
	 	                       	} else {
	                        	    return true;
	 	                       	}
 	     	                }
 	     	            } else {
 	     				    if (strlen($columnData) > $this->metaData['COLUMNS'][$columnName]['LENGTH']) {
 	     				       return false;
	      				    } else {
	      				       return true;
	   	   				    }
	      				}
      			      } else {
      			      	if (strlen($columnData) > $this->metaData['COLUMNS'][$columnName]['LENGTH']) {
      			      		return false;
      			      	} else {
      			      		return true;
      			      	}
      			      }
      				  break;
      }
    // Else no fieldMetaData array is defined so all edits will be based on the DB metaData
    //  retrieved.
	} else {
	// If this column has a numeric precision defined, make sure it is not being
	//  violated by the data supplied for this field.
      if ($this->metaData['COLUMNS'][$columnName]['ISNUMERIC']) {
      	if (strpos(".",$columnData)) {
      	   $numArray = explode(".",$columnData);
      	      if (!is_numeric($numArray[0]) or strlen($numArray[0]) > $this->metaData['COLUMNS'][$columnName]['NUMERIC_SCALE']) {
      	      	return false;}
      	      if (!is_numeric($numArray[1]) or strlen($numArray[1]) > $this->metaData['COLUMNS'][$columnName]['NUMERIC_PRECISION']) {
      	      	return false;}
      	      return true;
      	} else {
      		if (!is_numeric($columnData) or strlen($columnData) > $this->metaData['COLUMNS'][$columnName]['NUMERIC_SCALE']) {
      			return false;
      	   } else {
      	      return true;
      	   }
      	}
      	// Else not numeric so just check the field length.
     } else {
     	if ($this->metaData['COLUMNS'][$columnName]['DATA_TYPE'] == 'DATE') {
     		// perform date edits here
     		$dateError = false;
     		  // Get Date Parts (must be in ISO format (YYYY-MM-DD)
     		$numArray = explode("-",$columnData);
     		  // Make sure there are three parts
     		if (count($numArray) != 3) {
     			$dateError = true;
     		} else {
     			  // Make sure each part is numeric
     			foreach ($numArray as $dateValue) {
     				if (!is_numeric($dateValue)) {
     					$dateError = true;
     				}
     			}
     			// check if the date is valid
     			if (!$dateError) {
     				if (!checkdate($numArray[1], $numArray[2], $numArray[0])) {
     					$dateError = true;
     				}
     			}
     		}
     		// return results
     		if ($dateError) {
     			return false;
     		} else {
     			return true;
     		}

     	} else {
     		if (strlen($columnData) > $this->metaData['COLUMNS'][$columnName]['LENGTH']) {
     	   		return false;
     		} else {
        		return true;
     		}
     	}
     }
   }
  } else {
  	return true;
  }
  }
}
// Validate all submitted form fields.
/**
 * Validate all submitted form fields contained in the table.
 *
 * Validates all columns provided for update for the given table.
 *
 * @see isValid()
 * @param string $action
 * @return boolean
 */
 protected function infoIsValid($action=null) {
 global $errorText;
 global $errorColumn;
 if (!isset($this->db) or $this->db == null) {
  		$this->db = new db();
  		$this->db->connect($this->connection);
 }
 if (!isset($this->metaData)) {
  	$this->getTableMeta();
 }
 // initialize response to true then look for edit failures
 $DataOk = true;

 // check each field for valid data
 foreach ($this->metaData['COLUMNS'] as $columnName => $columnData) {
   if (isset($_REQUEST[$columnName])) {
     if (!$this->isValid($columnName, $_REQUEST[$columnName])) {
     	if (!empty($this->metaData['COLUMNS'][$columnName]['TEXT'])) {
     		$errorText[] = $this->metaData['COLUMNS'][$columnName]['TEXT']." - Data is missing or invalid.";
     		$errorColumn[] = trim($columnName);
     		error_log('Invalid data for column '.$columnName);
     	} else {
	       $errorText[] = $columnName." - Data is missing or invalid.";
	       $errorColumn[] = trim($columnName);
	       error_log('Invalid data for column '.$columnName);
     	}
       $DataOk = false;
     }
   } else {
   	if (!in_array($columnName,$this->fieldOptional) and array_key_exists($columnName,$this->fieldMetaData) and ($this->fieldMetaData[$columnName]['TYPE'] == 'radioButton' or $this->fieldMetaData[$columnName]['TYPE'] == 'checkBox')) {
   		if (!empty($this->metaData['COLUMNS'][$columnName]['TEXT'])) {
   			$errorText[] = $this->metaData['COLUMNS'][$columnName]['TEXT']." - Data is missing or invalid.";
   			$errorColumn[] = trim($columnName);
   			error_log('Invalid data for column '.$columnName);
   		} else {
   			$errorText[] = $columnName." - Data is missing or invalid.";
   			$errorColumn[] = trim($columnName);
   			error_log('Invalid data for column '.$columnName);
   		}
   		$DataOk = false;
   	}
   }
 }
 // check each key field to make sure it is specified.  All actions except select
 //  require full keys for operation.
 if ($action != null and $action != "Select") {
 	foreach ($this->metaData['KEYS'] as $keyField) {
 		if ($this->metaData['COLUMNS'][$keyField]['IDENTITY'] != 'YES' and !isset($_REQUEST[$keyField])) {
 			if (!empty($this->metaData['COLUMNS'][$keyField]['TEXT'])) {
 				$errorText[] = $this->metaData['COLUMNS'][$keyField]['TEXT']." - Data must be supplied.";
 				$errorColumn[] = $columnName;
 			} else {
 				$errorText[] = $keyField." - Data must be supplied.";
 				$errorColumn[] = $columnName;
 			}
 			$DataOk = false;
 		}
 	}
 }

 return $DataOk;
 }

 // Render a form output line.  This will be a table row with two cells.
 //  The left cell will contain the text and the right cell will contain the
 //  data or input fields.
 /**
  * Render a form output line
  *
  * Render a form line for a table column based on the $viewFields or $UpdFields global array
  * (if provided), and the column information retrieved for the table.  This function
  * builds a table row with two columns.  The first contains a text description of the
  * field and the second contains the output/input field.
  *
  * Parameter descriptions
  *  $columnName defines the name of the column in the table
  *  $columnData defines the current value of the column
  *  $allowKeyChange determines whether or not the key field for the file may be changed.
  *                  If set to true, then a special hidden form element will be created
  *                  to store the previous value of the key and that value will be used
  *                  when performing operations against the data.
  *  $overrides defines override attributes for the column.  This allows for the column to
  *             be generated with special characteristics when necessary.
  *             Override values that can be specified are:
  *                 TEXT - defines override column text to replace the standard column
  *                        text for the field.
  *                 JS   - defines override Javascript to associate with the form field.
  *                 SIZE - defines an override size for the column.
  *                 VALUE - defines an override value for the column.
  *                 TYPE - used to define an override type for the field.
  *                          The only override type supported at this time is 'DropDown'
  *                 DDVALUES - used to define a set of override values for a dropdown list
  *
  * @param string $columnName
  * @param string $columnData
  * @param string $allowKeyChange
  * @param array  $overrides
  * @return string
  */
 public function renderFormLine($columnName, $columnData, $allowKeyChange=true, $overrides=array()) {
 	if (!isset($this->db) or $this->db == null) {
  		$this->db = new db();
  		$this->db->connect($this->connection);
 	}
 	$templateArray = array("%text%","%name%","%value%","%size%","%maxlength%","%Js%","%multiple%","%Id%","%rowId%","%rows%","%rowClass%");
	$maxLength = 0;
	$Js = "";
	$multiple = "";
	$numRows = 0;
    $columnData = str_replace('"','&quot;',$columnData);
 if (!isset($this->metaData)) {
 	$this->getTableMeta();
 }
 if (isset($overrides) and array_key_exists('AsAssoc',$overrides)) {
 	$AsAssoc = $overrides['AsAssoc'];
 } else {
 	$AsAssoc = false;
 }
 // Generate form fields for any columns specified in either the viewFields or
 //  the updFields arrays.  If neither of those two arrays are present, then
 //  generate all fields as updatable by default.
 if (array_key_exists($columnName, $this->fieldMetaData) and is_array($this->fieldMetaData[$columnName]) and
     ($AsAssoc == true or base::is_assoc($this->fieldMetaData[$columnName])) and
     array_key_exists('FROMTABLE',$this->fieldMetaData[$columnName]) and
     array_key_exists('VALCOLUMN',$this->fieldMetaData[$columnName]) and
 	 (!array_key_exists('VALUES',$this->fieldMetaData[$columnName]) or
 	 (array_key_exists('INITIALIZE',$overrides) and $overrides['INITIALIZE'] == true))) {
     if (array_key_exists('SELCOLUMN',$this->fieldMetaData[$columnName])) {
     	$selColumn = $this->fieldMetaData[$columnName]['SELCOLUMN'];
     } else {
     	$selColumn = null;
     }
     if (array_key_exists('INITIALIZE',$overrides) and $overrides['INITIALIZE'] == true) {
     	$this->resetColumns($columnName);
     }
     if (array_key_exists('WHERE',$this->fieldMetaData[$columnName])) {
     	$where = $this->fieldMetaData[$columnName]['WHERE'];
        if (array_key_exists('UNIQUE',$where)) {
      		$where['UNIQUETABLE'] = $this->tableName;
      	    $where['UNIQUECOLUMN'] = $columnName;
      	    if ($this->metaData['COLUMNS'][$columnName]['ISNUMERIC']) {
      	    	$where['UNIQUECURRENT'] = $columnData;
      	    } else {
      	    	$where['UNIQUECURRENT'] = "\"$columnData\"";
      	    }
      	    foreach ($where['UNIQUEBY'] as $key => $value) {
      	    	if (isset($_REQUEST[$key])) {
      	    		if ($this->metaData['COLUMNS'][$key]['ISNUMERIC']) {
      	    			$where['UNIQUEBY'][$key] = $_REQUEST[$key];
      	        	} else {
      	        		$where['UNIQUEBY'][$key] = "\"{$_REQUEST[$key]}\"";
      	        	}
      	        }
  			}
  		}
     } else {
     	$where = null;
     }
     if (array_key_exists('ORDERBY',$this->fieldMetaData[$columnName])) {
     	$orderBy = $this->fieldMetaData[$columnName]['ORDERBY'];
     } else {
     	$orderBy = null;
     }
     $this->fieldMetaData[$columnName]['VALUES'] =
       	   $this->bldTableMetaValuesArray($this->fieldMetaData[$columnName]['FROMTABLE'],
           $this->fieldMetaData[$columnName]['VALCOLUMN'], $selColumn, array('WHERE'=>$where, 'ORDERBY'=>$orderBy));
       if ($columnData and $columnData != "") {
       if (!$selColumn) {
       	if (!in_array(trim($columnData),$this->fieldMetaData[$columnName]['VALUES']) and trim($columnData) != '') {
       		$this->fieldMetaData[$columnName]['VALUES'] = array_merge(array(trim($columnData)),$this->fieldMetaData[$columnName]['VALUES']);
       	}
       } else {
       	if (!array_key_exists(trim($columnData), $this->fieldMetaData[$columnName]['VALUES']) and trim($columnData) != '') {
       		$this->fieldMetaData[$columnName]['VALUES'] = array_merge(array(trim($columnData)=>trim($columnData)),$this->fieldMetaData[$columnName]['VALUES']);
		//	$this->fieldMetaData[$columnName]['VALUES'] = array(trim($columnData)=>trim($columnData)) + $this->fieldMetaData[$columnName]['VALUES'];
       	}
   	}
       }
 }

 if ((!isset($this->metaData['KEYS']) or !in_array($columnName,$this->metaData['KEYS']) or $allowKeyChange) and (!isset($this->hiddenFields) or !in_array($columnName,$this->hiddenFields))) {
 	if (in_array($columnName,$this->metaData['KEYS'])) {
 		if (isset($_REQUEST['old_'.$columnName]) and $_REQUEST['old_'.$columnName] != '') {
 			$oldValue = $_REQUEST['old_'.$columnName];
 		} else {
 			$oldValue = $columnData;
 		}
  		$oldKeyView = file_get_contents('views/view.hiddenFormField.php');
 		$templateData = array("","old_".$columnName,$oldValue,"","");
 		$oldKeyView = str_replace($templateArray,$templateData,$oldKeyView);
 	} else {
 		$oldKeyView = null;
 	}
 	if (!isset($this->fieldOptional) or !in_array($columnName,$this->fieldOptional)) {
	  	$required = true;
	   	$requiredIndicator = "*&nbsp;";
	} else {
	   	$required = false;
	   	$requiredIndicator = "&nbsp;&nbsp;&nbsp;";
	}
 	if ((isset($overrides['TYPE']) and $overrides['TYPE'] = 'DropDown') or
 			(array_key_exists($columnName, $this->fieldMetaData) and
 			(is_array($this->fieldMetaData[$columnName]) and !array_key_exists('TYPE',$this->fieldMetaData[$columnName]) or $this->fieldMetaData[$columnName]['TYPE']=='DropDown') and
 			((is_array($this->fieldMetaData[$columnName]['VALUES'])) or
		$this->fieldMetaData[$columnName] == 'YN'))) {
	    if ($this->fieldMetaData[$columnName] == 'YN') {
	    	$dropDown = new DropDown($columnName, $this->YNArray);
	    	$value = $dropDown->getDropDownOptions($columnData, $required);
	    	$size = 1;
	    } else {
	    	if (isset($overrides['MULTIPLE']) and $overrides['MULTIPLE'] != false) {
	    		$multopt = true;
	    	} else {
	    		$multopt = false;
	    	}
	    	if (isset($overrides['DDVALUES'])) {
	    		$dropDown = new DropDown($columnName, $overrides['DDVALUES']);
	    	} else {
	    		$dropDown = new DropDown($columnName, $this->fieldMetaData[$columnName]['VALUES']);
	    	}
 			$value = $dropDown->getDropDownOptions($columnData, $required, $multopt, $AsAssoc);
 			$size = 1;
	    }
	    if (isset($dropDown)) {
	    	unset($dropDown);
	    }
	    if (isset($this->fieldMetaData[$columnName]['JS'])) {
	    	$Js = $this->fieldMetaData[$columnName]['JS'];
	    }
	    $view = file_get_contents('views/view.dropdownFormField.php');
 	} else {
 		if ((isset($overrides['TYPE']) and $overrides['TYPE'] == 'radioButton') or
 				(isset($this->fieldMetaData[$columnName]['TYPE']) and $this->fieldMetaData[$columnName]['TYPE'] == 'radioButton' and
 						isset($this->fieldMetaData[$columnName]['VALUES']) and is_array($this->fieldMetaData[$columnName]['VALUES']) and
 						count($this->fieldMetaData[$columnName]['VALUES']) > 0)) {
 			if (isset($overrides['NAME'])) {
 				$rbName = $overrides['NAME'];
 			} else {
 				$rbName = $columnName;
 			}
 			if (isset($overrides['DDVALUES'])) {
 				$radioButton = new radioButton($rbName, $overrides['DDVALUES']);
 			} else {
 				$radioButton = new radioButton($rbName, $this->fieldMetaData[$columnName]['VALUES']);
 			}
 			if (isset($overrides)) {
 				$rbOptions = $overrides;
 			} else {
 				$rbOptions = array();
 			}
 			$rbOptions = array_merge($rbOptions, $this->fieldMetaData[$columnName]);
 			$value = $radioButton->bldRadioButtons($columnData,$required,$rbOptions,$AsAssoc);
 			$size = 1;
 			if (isset($radioButton)) {
 				unset($radioButton);
 			}
 			if (isset($this->fieldMetaData[$columnName]['JS'])) {
 				$Js = $this->fieldMetaData[$columnName]['JS'];
 			}
 			$view = file_get_contents('views/view.radioButtonFormField.php');
 		} else {
 		if ((isset($overrides['TYPE']) and $overrides['TYPE'] == 'checkBox') or
 				(isset($this->fieldMetaData[$columnName]['TYPE']) and $this->fieldMetaData[$columnName]['TYPE'] == 'checkBox' and
 						isset($this->fieldMetaData[$columnName]['VALUES']) and is_array($this->fieldMetaData[$columnName]['VALUES']) and
 						count($this->fieldMetaData[$columnName]['VALUES']) > 0)) {
 			if (isset($overrides['DDVALUES'])) {
 				$checkBox = new checkBox($columnName, $overrides['DDVALUES']);
 			} else {
 				$checkBox = new checkBox($columnName, $this->fieldMetaData[$columnName]['VALUES']);
 			}
 			$value = $checkBox->getCheckBoxOptions($columnData);
 			$size = 1;
 			if (isset($checkBox)) {
 				unset($checkBox);
 			}
 			if (isset($this->fieldMetaData[$columnName]['JS'])) {
 				$Js = $this->fieldMetaData[$columnName]['JS'];
 			}
 			$view = file_get_contents('views/view.checkBoxFormField.php');
 		} else {
		  		$value = $columnData;
 				if (isset($this->metaData['COLUMNS'][$columnName]['LENGTH'])) {
 					$maxLength = $this->metaData['COLUMNS'][$columnName]['LENGTH'];
 				} else {
 					$maxLength = '';
 				}
 				if (isset($this->metaData['COLUMNS'][$columnName]['NUMERIC_SCALE'])) {
 					$colScale = $this->metaData['COLUMNS'][$columnName]['NUMERIC_SCALE'];
 	 			} else {
 					$colScale = '';
 				}
 				if ($colScale != null and $colScale != 0) {
 					$maxLength ++;
 				}
 				if ($maxLength > 100 or ($overrides and array_key_exists('SIZE',$overrides) and $overrides['SIZE'] > 100)) {
 					if (isset($overrides) and array_key_exists('SIZE',$overrides) and $overrides['SIZE'] > $maxLength) {
 						$maxLength = $overrides['SIZE'];
 					}
 					$numRows = intval($maxLength/80) + 1;
 					$view = file_get_contents('views/view.textAreaFormField.php');
 				} else {
 					if (isset($this->fieldMetaData[$columnName]) and is_array($this->fieldMetaData[$columnName]) and array_key_exists('MASK',$this->fieldMetaData[$columnName]) and $this->fieldMetaData[$columnName]['MASK'] == true) {
 						$view = file_get_contents('views/view.passwordFormField.php');
 					} else {
 						$view = file_get_contents('views/view.textFormField.php');
 					}
 				}
 				if (isset($this->fieldMetaData[$columnName]['JS'])) {
			    	$Js = $this->fieldMetaData[$columnName]['JS'];
			    }
 			}
 		}
 	}
 	// Add phone number masking for Phone number fields.
 	if (array_key_exists($columnName,$this->fieldMetaData) and $this->fieldMetaData[$columnName] == 'PHONE') {
 		$Js = 'onkeydown="javascript:backspacerDOWN(this,event);" onkeyup="javascript:backspacerUP(this,event);"';
 	}
 	if (isset($this->metaData['COLUMNS'][$columnName]['TEXT'])) {
	  $columnText = $requiredIndicator.$this->metaData['COLUMNS'][$columnName]['TEXT'];
 	} else {
 		$columnText = '&nbsp;';
 	}
 	$rowClass = '';
	if (isset($overrides) and is_array($overrides)) {
		if (isset($overrides['TEXT'])) {
			$columnText = $overrides['TEXT'];
		}
		if (isset($overrides['SIZE'])) {
			$maxLength = $overrides['SIZE'];
		}
		if (isset($overrides['JS'])) {
			$Js = $overrides['JS'];
		}
		if (isset($overrides['VALUE'])) {
			$value = $overrides['VALUE'];
		}
		if (isset($overrides['NAME'])) {
			$columnName = $overrides['NAME'];
		}
		if (isset($overrides['CLASS'])) {
			$rowClass = 'class="'.$overrides['CLASS'].'"';
		}
		if (isset($overrides['MULTIPLE']) and $overrides['MULTIPLE'] != false) {
			$multiple = 'multiple="multiple"';
			if (is_int($overrides['MULTIPLE'])) {
				$size = $overrides['MULTIPLE'];
			} else {
				$size = 10;
			}
		}
	}
	if (!isset($size)) {
		$size = $maxLength;
	}
	$rowIdText = 'row_'.$columnName;
	$templateData = array($columnText,$columnName,$value,$size,$maxLength,$Js,$multiple,$columnName,$rowIdText,$numRows,$rowClass);
	$view = str_replace($templateArray,$templateData,$view);
	if (isset ($oldKeyView) and $oldKeyView != null) {
		$view .= $oldKeyView;
	}
 } else {
 	$view = file_get_contents('views/view.hiddenFormField.php');
 	$templateData  = array("",$columnName,trim($columnData),"","");
 	$view = str_replace($templateArray,$templateData,$view);
 }
 return $view;
}



 // Render form detail for the table.
 /**
  * Render a form output line for each field in the table
  *
  * Render a form line for each table column.  If key changes are allowed, then a separate line
  * will be rendered for the keys as a hidden field so that the old key can be preserved for
  * update purposes.
  *
  * @param array $row
  * @param string $allowKeyChange
  * @return string
  */
 public function renderFormData($row=null, $allowKeyChange=true) {
 	if (!isset($this->db) or $this->db == null) {
  		$this->db = new db();
  		$this->db->connect($this->connection);
 	}
 	if (!isset($this->metaData)) {
 		$this->getTableMeta();
 	}
 	if ($row == null) {
 		$row = array();
 	}
 	$output = "";
 	foreach ($this->metaData['COLUMNS'] as $columnName => $columnData) {
 		if(isset($_REQUEST[$columnName])) {
 			$currentValue = $_REQUEST[$columnName];
 		} else {
 			if (array_key_exists($columnName, $row)) {
 				$currentValue = $row[$columnName];
 			} else {
 				$currentValue = "";
 			}
 		}
 		$output .= $this->renderFormLine(trim($columnName), $currentValue, $allowKeyChange);
 	}

 	return $output;
}
 /**
  * Set/Unset optional fields for this table
  *
  * Add or Remove a field/array of fields to/from the $fieldOptional array.  This array
  * controls whether a field requires a value when performing an insert/update.
  *
  * @param string $action
  * @param array  $fieldList
  * @return boolean
  */
 public function optionalField($action=null, $fieldList=array()) {

 	if (!isset($this->metaData)) {
 		$this->getTableMeta();
 	}
    if (count($fieldList) > 0) {
 	foreach ($fieldList as $field) {
 		switch (strtoupper($action)) {
 			case 'SET'	: $this->fieldOptional[] = $field;
 						  break;
 			case 'UNSET': $oldOptional = $this->fieldOptional;
 						  $this->fieldOptional = null;
 			              $this->fieldOptional = array();
 			              foreach ($oldOptional as $oldField) {
 			              	if ($oldField != $field) {
 			              		$this->fieldOptional[] = $oldField;
 			              	}
 			              }
 			              break;
 		}
 	}
    } else {
    	foreach ($this->metaData['COLUMNS'] as $columnName => $columnData) {
    		switch (strtoupper($action)) {
 			case 'SET'	: $this->fieldOptional[] = $columnName;
 						  break;
 			case 'UNSET': $oldOptional = $this->fieldOptional;
 			              $this->fieldOptional = array();
 			              foreach ($oldOptional as $oldField) {
 			              	if ($oldField != $columnName) {
 			              		$this->fieldOptional[] = $oldField;
 			              	}
 			              }
 			              break;
 		}
    	}
    }

 	return true;
}
}
?>
