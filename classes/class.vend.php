<?php
require_once('class.table.php');
	/**
	 * import table Definitions.
	 * 
	 * The import table contains defintions of each RETS import that will be run
     * The import class extends the standard table class.
	 *
	 * @author George L. Slater (Arbor Solutions, Inc)
	 * @copyright 2011 - GRAR and Arbor Solutions, Inc
	 * @version 1.0
	 * @package Db
	 * @see class.table.php
	 */
class vend extends table {
  
  function __construct() {
  	global $config;
	// fieldMetaData defines special editting hand handling above and beyond what can be determined
	//  simply based on the field attributes. 
	/**
 	 * Fields Metadata
 	 *
 	 * This data identifies special field handling for edits and form display. 
 	 *
  	 *  Valid Edit types are:
 	 *
 	 *  ALPHA	=	all characters are allowed but embedded single quotes are doubled
 	 *  			during the database update.  Mixed case is allowed.
 	 *  ALPHA_UC=	all characters are allowed but alphabetic characters will be 
 	 *  			converted to Upper case during database operations.
 	 *  EMAIL   =   only valid email addresses are allowed.  Email addresses are checked
 	 *              for validity by format only.  No attempt it made to confirm that the 
 	 *              email address corresponds to an actual email account.
 	 *  FROMTABLE=	This is an associative array.  When FROMTABLE is specified, VALCOLUMN
 	 *              must also be specified (SELCOLUMN is optional as is the WHERE Key).
 	 *              FROMTABLE identifies a separate table from which values are derived as
 	 *              distinct values.  VALCOLUMN (and optionally SELCOLUMN) keys identify 
 	 *              the columns used to generate an associative array (and subsequently
 	 *              a dropdown for selection) of values.  Using a "WHERE" subclause the 
 	 *              data returned can be filtered to a select set of data based on input
 	 *              values.            
 	 *  INT		=	only digits are allowed.
 	 *  JS		=   Defines Javascript to be associated with the field when displayed in 
 	 *              an input form.
 	 *  NUM		=	only digits and a decimal point are allowed.
 	 *  VALUES	=	if the edit type is an array, then the value provided must be one of
 	 *  			the values stored in the array.  Input generates a dropdown list for
 	 *  			selection.
 	 *  YN		=	identifies a Yes/No field that behaves like an array edit type except
 	 *  			that the values are predefined to only allow "Y" and "N".
 	 *  UNIQUE  =   identifies this field as unique in the table based on a set of values.
 	 *  UNIQUEBY=	identifies a set of fields in which the field being defined must be 
 	 *              unique.  
 	 *  PHONE   =   inserts phone number masking during input.
 	 **/
 // 	global $dbName;
 // 	$dbName = 'ASI520PROD';
  	error_log('vend construction uses session variable dbSchema as '.$_SESSION[APPLICATION]['dbSchema']);
  	error_log('vend construction uses session variable dbName as '.$_SESSION[APPLICATION]['dbName']);
  	$this->schema = $_SESSION[APPLICATION]['dbSchema'];
  	$this->thisIsQuoted = false;
  	$this->dbName = $_SESSION[APPLICATION]['dbName'];
  	
    if (isset($GLOBALS['thisFormdbSchema'])) {
      error_log('vend construction sees thisFormdbSchema as '.$GLOBALS['thisFormdbSchema']);
      if (!empty($GLOBALS['thisFormdbSchema'])) {
          $this->schema = $GLOBALS['thisFormdbSchema'];
      }
    } else {
      error_log('vend construction cannot find thisFormdbSchema');
    }
    if (isset($GLOBALS['thisFormdbName'])) {
      error_log('vend construction sees thisFormdbName as '.$GLOBALS['thisFormdbName']);
      if (!empty($GLOBALS['thisFormdbName'])) {
          $this->dbName = $GLOBALS['thisFormdbName'];
      }
    } else {
      error_log('vend construction cannot find thisFormdbName');
    }

	$this->fieldMetaData = array('BTNAME'=>'ALPHA_UC',
								 'BTADR1'=>'ALPHA_UC',
								 'BTADR2'=>'ALPHA_UC',
								 'BTADR3'=>'ALPHA_UC',
								 'BTADR4'=>'ALPHA_UC',
								 'BTADR5'=>'ALPHA_UC',
								 'BTADR6'=>'ALPHA_UC',
								 'BTADR7'=>'ALPHA_UC',
								 'BTADR8'=>'ALPHA_UC',
								 'BTADR9'=>'ALPHA_UC',
								 'BTADR10'=>'ALPHA_UC',
								 'BTPOST'=>'ALPHA_UC',
								 'BTCITY'=>'ALPHA_UC',
								 'BTCLAS'=>'ALPHA_UC',
								 'BTCONT'=>'ALPHA_UC',
								 'BTVEND'=>'ALPHA_UC',
								 'BTGSTL'=>'ALPHA_UC',
								 'BTDUNS'=>'ALPHA_UC',
								 'BTEMAL'=>'ALPHA_UC',
								 'BTWPAG'=>'ALPHA_UC',
								 'BTCUSR'=>'ALPHA_UC',
								 'BTUUSR'=>'ALPHA_UC',
								 'BTFUT1'=>'ALPHA_UC',
								 'BTFUT2'=>'ALPHA_UC',
								 'BTFUT3'=>'ALPHA_UC',
								 'BTFUT4'=>'ALPHA_UC',
								 'BTFUT5'=>'ALPHA_UC',
								 'BTFUT10'=>'ALPHA_UC',
								 'BTFUT11'=>'ALPHA_UC',
								 'BTFUT12'=>'ALPHA_UC',
								 'BTFUT13'=>'ALPHA_UC',
								 'BTFUT14'=>'ALPHA_UC',
								 'BTFUT15'=>'ALPHA_UC');
  	parent::__construct();
  	$this->optionalField('set');
  }
function reset () {
	$this->__construct();
}
}
?>
