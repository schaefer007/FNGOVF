<?php
/**
 * Handle connections to the native i5 for program and file calls
 * 
 * This file contains the classes and methods necessary for establishing and working with a native i5 connection. 
 * 
 * @author George L. Slater (Arbor Solutions, Inc)
 * @copyright 2007 - Grand Rapids Commercial Alliance of Realtors(r)
 * @version 1.0
 * @package Utility
 * 
 */
/**
 * Work with native i5 connections
 * 
 * @access public
 * @author George L. Slater (Arbor Solutions, Inc)
 * @copyright 2007 - Grand Rapids Commercial Alliance of Realtors(r)
 * @version 1.0
 * @package Utility
 */
class Sys_Connect {
/**
 * Connection Resource
 *
 * @var connection_resource
 */
	protected $Connection;
/**
 * Was the connection resource established from within this script?
 *
 * @var boolean
 */
	protected $Local_Connection;
	/**
	 * Constructor for Sys_Connect class
	 *
	 * Initialize Connection for use by Class Methods and indicate that the connection is
	 * not a locally generated one.  If no connection is used to instantiate this class,
	 * a connection will be generated later and the local connection variable set to true.
	 * Setting the local connection variable controls whether or not this process closes
	 * the connection during cleanup.
	 * 
	 * @param connection_resource $Connection
	 */	
function __construct($Connection = null) {
	$this->Connection = $Connection;
	$this->Local_Connection = false;
}
	/**
	 * Destructor for Sys_Connect class
	 *
	 * Perform cleanup and destroy any objects created by class methods that are no longer needed. 
	 * If a connection was generated within this script, then close the connection.
	 */	
function __destruct() {
if (isset($i5_Db)) {
	unset($i5_Db);
}
if ($this->Local_Connection == true) {
	db2_close($this->Connection);
}	
}
/**
 * Get a connection to the specified system
 *
 * Using a DB2 connection (if not previously established, then a new connection will be established
 * within this script) retrieve the user Id and Password information related to the desired system 
 * connection, then use that information to create a connection to that system.
 * 
 * @param string $SysName
 * @return i5_connection_resource
 */
function Get_i5_Connection($SysName) {
	// Retrieve the login information for the target system from the local one.

	if (!isset($this->Connection)) {
		$this->Connection = db2_pconnect("","","");
		$this->Local_Connection = true;
	}
try {
	$i5_Db = new Db(null,false);
	$i5_Db->connect($this->Connection);
	$i5_Db->prepare("select Uid, Pwd from Qgpl.SysConn where SysName='$SysName'");
	$i5_Db->setOptions("i5_fetch_only",DB2_I5_FETCH_ON);
	$i5_Db->execute();
			while ($row = $i5_Db->getNextRow()) {
      		$UID = trim($row[0]);
      		$PWD = trim($row[1]);
    		}
}
catch (Exception $Error) {
	print base::Error_Handler($Error);
}

if (isset($i5_Db)) {
	unset($i5_Db);
}
if ($this->Local_Connection == true) {
	db2_close($this->Connection);
}
		$PConn = i5_connect($SysName, $UID, $PWD);
		if ($PConn === false) {
			print ("FAIL: Failed to connect to server: HAL <br>/n");
			$errorTab = i5_error();
			var_dump($errorTab);
			die();
		}
	unset($UID);
	unset($PWD);
	return $PConn;    	
    }
    // Close a connection handle previously openned.
/**
 * Close a native i5 Connection
 *
 * @param i5_connection_resource $PConn
 */
    function Close_i5_Connection($PConn) {
    	i5_close($PConn);
    }
    
function Get_Db_Connection($SysName=null) {
	// Retrieve the login information for the target system from the local one.
    if ($SysName == null) {
    	$this->Connection = db2_pconnect("","","");
    	$this->Local_Connection = true; 
     	return $this->Connection;
    } else {
    	return false;
    }
 }
    // Close a connection handle previously openned.
/**
 * Close a DB2 Connection
 *
 * @param Db2_connection_resource $PConn
 */
    function Close_Db_Connection($PConn = null) {
    	if ($PConn == null and $this->Local_Connection == true) {
    	db2_close($this->Connection);
    } else {
    	db2_close($PConn);
    }
    }
}