<?php
require_once('class.table.php');
	/**
	 * user_account table Definitions.
	 * 
	 * The user_account table contains defintions of each enrolled user as well as administrative
	 * users created on the GRASP system. The user_accounts class extends the standard table class.
	 *
	 * @author George L. Slater (Arbor Solutions, Inc)
	 * @copyright 2011 - GRASP
	 * @version 1.0
	 * @package Db
	 * @see class.table.php
	 */
class tbluser extends table {
  
  function __construct() {
  	global $config;
	// fieldMetaData defines special editting hand handling above and beyond what can be determined
	//  simply based on the field attributes. 
	/**
 	 * Fields Metadata
 	 *
 	 * This data identifies special field handling for edits and form display. 
 	 *
  	 *  Valid Edit types are:
 	 *
 	 *  ALPHA	=	all characters are allowed but embedded single quotes are doubled
 	 *  			during the database update.  Mixed case is allowed.
 	 *  ALPHA_UC=	all characters are allowed but alphabetic characters will be 
 	 *  			converted to Upper case during database operations.
 	 *  EMAIL   =   only valid email addresses are allowed.  Email addresses are checked
 	 *              for validity by format only.  No attempt it made to confirm that the 
 	 *              email address corresponds to an actual email account.
 	 *  FROMTABLE=	This is an associative array.  When FROMTABLE is specified, VALCOLUMN
 	 *              must also be specified (SELCOLUMN is optional as is the WHERE Key).
 	 *              FROMTABLE identifies a separate table from which values are derived as
 	 *              distinct values.  VALCOLUMN (and optionally SELCOLUMN) keys identify 
 	 *              the columns used to generate an associative array (and subsequently
 	 *              a dropdown for selection) of values.  Using a "WHERE" subclause the 
 	 *              data returned can be filtered to a select set of data based on input
 	 *              values.            
 	 *  INT		=	only digits are allowed.
 	 *  JS		=   Defines Javascript to be associated with the field when displayed in 
 	 *              an input form.
 	 *  NUM		=	only digits and a decimal point are allowed.
 	 *  VALUES	=	if the edit type is an array, then the value provided must be one of
 	 *  			the values stored in the array.  Input generates a dropdown list for
 	 *  			selection.
 	 *  YN		=	identifies a Yes/No field that behaves like an array edit type except
 	 *  			that the values are predefined to only allow "Y" and "N".
 	 *  UNIQUE  =   identifies this field as unique in the table based on a set of values.
 	 *  UNIQUEBY=	identifies a set of fields in which the field being defined must be 
 	 *              unique.  
 	 *  PHONE   =   inserts phone number masking during input.
 	 **/
  	$this->thisIsQuoted = true;
	$this->fieldMetaData = array("strUserStatus"=>array("VALUES"=>array(1=>"Active", 2=>"Disabled")));
  	parent::__construct();
  	$this->optionalField('set');
	}
  
}
?>
