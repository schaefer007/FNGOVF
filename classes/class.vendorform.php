<?php
require_once('class.table.php');
include_once("ProcessObject.class.php");
include_once('ProcessState.class.php');
include_once('ProcessTransition.class.php');
include_once('includes/constants.vendorform.php');
include_once('includes/vendorMap.php');
include_once('class.vend.php');
include_once('class.cont.php');


	/**
	 * import table Definitions.
	 *
	 * The import table contains defintions of each RETS import that will be run
     * The import class extends the standard table class.
	 *
	 * @author George L. Slater (Arbor Solutions, Inc)
	 * @copyright 2011 - GRAR and Arbor Solutions, Inc
	 * @version 1.0
	 * @package Db
	 * @see class.table.php
	 */
class vendorform extends table {
	private $storedFacility;
	public $fieldOptional = array();
  function 	__construct() {
  	global $config;
	// fieldMetaData defines special editting hand handling above and beyond what can be determined
	//  simply based on the field attributes.
	/**
 	 * Fields Metadata
 	 *
 	 * This data identifies special field handling for edits and form display.
 	 *
  	 *  Valid Edit types are:
 	 *
 	 *  ALPHA	=	all characters are allowed but embedded single quotes are doubled
 	 *  			during the database update.  Mixed case is allowed.
 	 *  ALPHA_UC=	all characters are allowed but alphabetic characters will be
 	 *  			converted to Upper case during database operations.
 	 *  EMAIL   =   only valid email addresses are allowed.  Email addresses are checked
 	 *              for validity by format only.  No attempt it made to confirm that the
 	 *              email address corresponds to an actual email account.
 	 *  FROMTABLE=	This is an associative array.  When FROMTABLE is specified, VALCOLUMN
 	 *              must also be specified (SELCOLUMN is optional as is the WHERE Key).
 	 *              FROMTABLE identifies a separate table from which values are derived as
 	 *              distinct values.  VALCOLUMN (and optionally SELCOLUMN) keys identify
 	 *              the columns used to generate an associative array (and subsequently
 	 *              a dropdown for selection) of values.  Using a "WHERE" subclause the
 	 *              data returned can be filtered to a select set of data based on input
 	 *              values.
 	 *  INT		=	only digits are allowed.
 	 *  JS		=   Defines Javascript to be associated with the field when displayed in
 	 *              an input form.
 	 *  NUM		=	only digits and a decimal point are allowed.
 	 *  VALUES	=	if the edit type is an array, then the value provided must be one of
 	 *  			the values stored in the array.  Input generates a dropdown list for
 	 *  			selection.
 	 *  YN		=	identifies a Yes/No field that behaves like an array edit type except
 	 *  			that the values are predefined to only allow "Y" and "N".
 	 *  UNIQUE  =   identifies this field as unique in the table based on a set of values.
 	 *  UNIQUEBY=	identifies a set of fields in which the field being defined must be
 	 *              unique.
 	 *  PHONE   =   inserts phone number masking during input.
 	 **/
  	global $TAXGROUP;
  	if (isset($vendorArray)) {
  		unset($vendorArray);
  	}
  	global $vendorArray;
  	if ((!is_array($vendorArray) or !isset($vendorArray) or count($vendorArray) == 0) and isset($_SESSION[APPLICATION]['FACILITY'])) {
  		$vendorArrayFile = 'va_'.str_replace(array(' ',':'),array('_','-'),$_SESSION[APPLICATION]['FACILITY']).'.php';
  		if (file_exists('includes/'.$vendorArrayFile)) {
  			include_once('includes/'.$vendorArrayFile);
  		}
  	}
  	$YN = array("Y"=>"Yes","N"=>"No");
  	$this->thisIsQuoted = false;
  	$this->schema = '"dbsystem"';

//  	$this->fieldMetaData = array("REQVENDTYPE"=>array("VALUES"=>array("1"=>"Production", "2"=>"Non-Production"),"TYPE"=>"radioButton"),
//  			                     "FRTCHARGES"=>array("VALUES"=>array("P"=>"Prepaid","C"=>"Collect","I"=>"Invoice"),"TYPE"=>"radioButton"),
//  								 "REQPURCHFREQ"=>array("VALUES"=>array("Weekly","Monthly","Bi-Monthly","Annually","Infrequent"),"TYPE"=>"radioButton"),
//  								 "REQFIRSTTIME"=>array("VALUES"=>$YN,"TYPE"=>"radioButton"),
//  								 "REQINACTIVEOLD"=>array("VALUES"=>$YN,"TYPE"=>"radioButton"),
//  								 "MINORITYFLAG"=>array("VALUES"=>$YN,"TYPE"=>"radioButton"),
//  								 "AUTOVOUCHER"=>array("VALUES"=>array('A'=>'Yes','N'=>'No'),"TYPE"=>"radioButton"),
//  								 "CHEQUEVOUCHER"=>array("VALUES"=>$YN,"TYPE"=>"radioButton"),
//  								 "USE1099"=>array("VALUES"=>$YN,"TYPE"=>"radioButtom"),
//  								 "CRTPLANSCHED"=>array("VALUES"=>$YN,"TYPE"=>"radioButton"),
//  								 "PLANSCHEDFREQ"=>array("VALUES"=>array("D"=>"Daily","W"=>"Weekly","B"=>"Bi-Weekly?","M"=>"Monthly","K"=>"K?")),
//  								 "PLANSCHEDDAY"=>array("VALUES"=>array("1"=>"Monday","2"=>"Tuesday","3"=>"Wednesday","4"=>"Thursday","5"=>"Friday","6"=>"Saturday","7"=>"Sunday")),
//  								 "PLANSCHEDDATE"=>array("VALUES"=>array("S"=>"Same Day","N"=>"Next Day"),"TYPE"=>"radioButton"),
//  								 "PLANSCHEDEDI"=>array("VALUES"=>$YN,"TYPE"=>"radioButton"),
//  								 "CRTSHIPSCHED"=>array("VALUES"=>$YN,"TYPE"=>"radioButton"),
//  								 "SHIPSCHEDFREQ"=>array("VALUES"=>array("D"=>"Daily","W"=>"Weekly","B"=>"Bi-Weekly?","M"=>"Monthly","K"=>"K?")),
//  								 "SHIPSCHEDDAY"=>array("VALUES"=>array("1"=>"Monday","2"=>"Tuesday","3"=>"Wednesday","4"=>"Thursday","5"=>"Friday","6"=>"Saturday","7"=>"Sunday")),
//  								 "SHIPSCHEDDATE"=>array("VALUES"=>array("S"=>"Same Day","N"=>"Next Day"),"TYPE"=>"radioButton"),
//  								 "SHIPSCHEDEDI"=>array("VALUES"=>$YN,"TYPE"=>"radioButton"),
//  								 "SENDEDI"=>array("VALUES"=>$YN,"TYPE"=>"radioButton"),
//  								 "LEADTIMEUNIT"=>array("VALUES"=>array("1"=>"Days","2"=>"Hours"),"TYPE"=>"radioButton"),
//  								 "ROUNDHRSTODAYS"=>array("VALUES"=>array("1"=>"Round Up","2"=>"Round Down","3"=>"No Rounding"),"TYPE"=>"radioButton"),
//  			 					 "SCANSUPPSERI"=>array("VALUES"=>$YN,"TYPE"=>"radioButton"),
//  								 "SHIPWEDAYS"=>array("VALUES"=>array("1"=>"Sat & Sunday","2"=>"Sun Only","3"=>"Do Not Skip"),"TYPE"=>"radioButton"),
//  								 "EDIREMITADV"=>array("VALUES"=>array("1"=>"Yes","2"=>"No")),
//  								 "EDIPARTPAYMENT"=>array("VALUES"=>array("1"=>"Yes","2"=>"No")),
//  								 "VOUCHERCONTROL"=>array("VALUES"=>array("1"=>"Rcpt/Ref Opt","2"=>"Rcp/Ref","3"=>"Rcpt"),"TYPE"=>"radioButton"),
//  								 "GENSCORECARD"=>array("VALUES"=>array("1"=>"Yes","2"=>"No")),
//  								 "VENDORSTATE"=>array("FROMTABLE"=>'prov',"VALCOLUMN"=>'B57CODE',"SELCOLUMN"=>'B57NAME'),
//  								 "VENDORCOUNTRY"=>array("FROMTABLE"=>'corg',"VALCOLUMN"=>'P6COD',"SELCOLUMN"=>'P6DES'),
//  								 "QCINSPECTOR"=>array("FROMTABLE"=>'kyic',"VALCOLUMN"=>'C8INSP',"SELCOLUMN"=>'C8DESC'),
//  								 "TERMSCODE"=>array("FROMTABLE"=>'code',"VALCOLUMN"=>'A9',"SELCOLUMN"=>'A30',"WHERE"=>array("A2"=>'NN')),
//  								 "FOB"=>array("FROMTABLE"=>'code',"VALCOLUMN"=>'A9',"SELCOLUMN"=>'A30',"WHERE"=>array("A2"=>'FF')),
//  								 "TAXGROUP"=>array("FROMTABLE"=>'txgp',"VALCOLUMN"=>'NKGRP',"SELCOLUMN"=>'NKDES','JS'=>'onChange="adjustTaxRates(this.options[this.selectedIndex].value);"'),
//  								 "GOODSTAXRATE"=>array("FROMTABLE"=>'txrt',"VALCOLUMN"=>'NIRTC',"SELCOLUMN"=>'NIDES','WHERE'=>array('NIGRP'=>$TAXGROUP)),
//  								 "SERVICESTAXRATE"=>array("FROMTABLE"=>'txrt',"VALCOLUMN"=>'NIRTC',"SELCOLUMN"=>'NIDES','WHERE'=>array('NIGRP'=>$TAXGROUP)),
//  								 "OSSTOCKROOM"=>array("FROMTABLE"=>'stkr',"VALCOLUMN"=>'AXSTKL',"SELCOLUMN"=>'AXLOCN'),
//  								 "TIMEZONE"=>array("FROMTABLE"=>'tmzn',"VALCOLUMN"=>'A82TMZN',"SELCOLUMN"=>'A82DESC'),
//  								 "DUMPINGCLASS"=>array("FROMTABLE"=>'vdcl',"VALCOLUMN"=>'BK9CODE',"SELCOLUMN"=>'BK9DES1'),
//  								 "USE1099"=>array("VALUES"=>$YN,"TYPE"=>"radioButton"),
//  								 "SCANSUPPSER"=>array("VALUES"=>$YN,"TYPE"=>"radioButton"),
//  								 "SHIPLEADUNIT"=>array("VALUES"=>array(1=>"Days",2=>"Hours"),"TYPE"=>"radioButton"),
//  								 "ROUNDTODAYS"=>array("VALUES"=>array(1=>"Round Up",2=>"Round Down",3=>"Do Not Round"),"TYPE"=>"radioButton"),
//  								 "REQEDIREMIT"=>array("VALUES"=>array(1=>"Yes",2=>"No"),"TYPE"=>"radioButton"),
//  								 "EDIPARTPAYMNT"=>array("VALUES"=>array(1=>"Yes",2=>"No"),"TYPE"=>"radioButton"),
//  								 "PROCSERIASN"=>array("VALUES"=>array(1=>"Yes",2=>"No"),"TYPE"=>"radioButton"),
//  								 "VENDORTYPE"=>array("VALUES"=>array(1=>"Standard",2=>"A/P Only"),"TYPE"=>"radioButton"),
//  								 "INITDSPSCHEDR"=>array("VALUES"=>array(1=>"By Ship Date",2=>"By Required Date"),"TYPE"=>"radioButton"),
//  								 "DFTCARRIER"=>array("FROMTABLE"=>'shcr',"VALCOLUMN"=>'RMCARC',"SELCOLUMN"=>'RMDES1'),
//  								 "DFTCSTBROKER"=>array("FROMTABLE"=>'cbcr',"VALCOLUMN"=>'XBCBRC',"SELCOLUMN"=>'XBDES1'),
//  								 "USRVERTEMPLATE"=>array("FROMTABLE"=>'vrth',"VALCOLUMN"=>'G3CODE',"SELCOLUMN"=>'G3DES1'),
//  					 			 "REPAIRLOCATION"=>array("FROMTABLE"=>'stkr',"VALCOLUMN"=>'AXSTKL',"SELCOLUMN"=>'AXLOCN'),
//  								 "COMMODITYCAT"=>array("FROMTABLE"=>'pgtp',"VALCOLUMN"=>'B26CCDE',"SELCOLUMN"=>'B26DES1'),
//  								 "APBANK"=>array("FROMTABLE"=>'cont',"VALCOLUMN"=>'IFBNK#',"SELCOLUMN"=>'IFBKNM'));

  	$this->fieldMetaData = array("REQVENDTYPE"=>array("VALUES"=>array("1"=>"Production", "2"=>"Non-Production"),"TYPE"=>"radioButton"),
  									"FRTCHARGES"=>array("VALUES"=>array("P"=>"Prepaid","C"=>"Collect","I"=>"Invoice"),"TYPE"=>"radioButton"),
						  			"REQPURCHFREQ"=>array("VALUES"=>array("Weekly","Monthly","Bi-Monthly","Annually","Infrequent"),"TYPE"=>"radioButton"),
						  			"REQFIRSTTIME"=>array("VALUES"=>$YN,"TYPE"=>"radioButton"),
						  	//		"VENDORCODE"=>"ALPHA_UC",
						  			"REQINACTIVEOLD"=>array("VALUES"=>$YN,"TYPE"=>"radioButton","JS"=>"onchange=(setInactiveReason());"),
  									"MINORITYFLAG"=>array("VALUES"=>$YN,"TYPE"=>"radioButton"),
  									"AUTOVOUCHER"=>array("VALUES"=>array('A'=>'Yes','N'=>'No'),"TYPE"=>"radioButton"),
						  			"CHEQUEVOUCHER"=>array("VALUES"=>$YN,"TYPE"=>"radioButton"),
						  			"USE1099"=>array("VALUES"=>$YN,"TYPE"=>"radioButtom"),
						  			"CRTPLANSCHED"=>array("VALUES"=>$YN,"TYPE"=>"radioButton"),
						  			"PLANSCHEDFREQ"=>array("VALUES"=>array("D"=>"Daily","W"=>"Weekly","B"=>"Bi-Weekly","M"=>"Monthly","K"=>"Weekday (Mon-Fri)")),
 						 			"PLANSCHEDDAY"=>array("VALUES"=>array("1"=>"Monday","2"=>"Tuesday","3"=>"Wednesday","4"=>"Thursday","5"=>"Friday","6"=>"Saturday","7"=>"Sunday")),
						  			"PLANSCHEDDATE"=>array("VALUES"=>array("S"=>"Same Day","N"=>"Next Day"),"TYPE"=>"radioButton"),
  									"PLANSCHEDEDI"=>array("VALUES"=>$YN,"TYPE"=>"radioButton"),
  									"CRTSHIPSCHED"=>array("VALUES"=>$YN,"TYPE"=>"radioButton"),
						  			"SHIPSCHEDFREQ"=>array("VALUES"=>array("D"=>"Daily","W"=>"Weekly","B"=>"Bi-Weekly","M"=>"Monthly","K"=>"Weekday (Mon-Fri)")),
 						 			"SHIPSCHEDDAY"=>array("VALUES"=>array("1"=>"Monday","2"=>"Tuesday","3"=>"Wednesday","4"=>"Thursday","5"=>"Friday","6"=>"Saturday","7"=>"Sunday")),
  									"SHIPSCHEDDATE"=>array("VALUES"=>array("S"=>"Same Day","N"=>"Next Day"),"TYPE"=>"radioButton"),
 						 			"SHIPSCHEDEDI"=>array("VALUES"=>$YN,"TYPE"=>"radioButton"),
  									"SENDEDI"=>array("VALUES"=>$YN,"TYPE"=>"radioButton"),
 						 			"LEADTIMEUNIT"=>array("VALUES"=>array("1"=>"Days","2"=>"Hours"),"TYPE"=>"radioButton"),
						  			"ROUNDHRSTODAYS"=>array("VALUES"=>array("1"=>"Round Up","2"=>"Round Down","3"=>"No Rounding"),"TYPE"=>"radioButton"),
  									"SCANSUPPSERI"=>array("VALUES"=>$YN,"TYPE"=>"radioButton"),
 						 			"SHIPWEDAYS"=>array("VALUES"=>array("1"=>"Sat & Sunday","2"=>"Sun Only","3"=>"Do Not Skip"),"TYPE"=>"radioButton"),
						  			"EDIREMITADV"=>array("VALUES"=>array("1"=>"Yes","2"=>"No")),
  									"EDIPARTPAYMENT"=>array("VALUES"=>array("1"=>"Yes","2"=>"No")),
  									"VOUCHERCONTROL"=>array("VALUES"=>array("1"=>"Rcpt/Ref Opt","2"=>"Rcp/Ref","3"=>"Rcpt"),"TYPE"=>"radioButton"),
						  			"GENSCORECARD"=>array("VALUES"=>array("1"=>"Yes","2"=>"No"),"TYPE"=>"radioButton"),
  									"VENDORSTATE"=>array("VALUES"=>$vendorArray[$_SESSION[APPLICATION]['FACILITY']]['prov']['none'],"JS"=>'onchange="setCountry();"'),
  									"VENDORCOUNTRY"=>array("VALUES"=>$vendorArray[$_SESSION[APPLICATION]['FACILITY']]['corg']['none'],"JS"=>'onchange="setRequiredByCountry();"'),
						  			"QCINSPECTOR"=>array("VALUES"=>$vendorArray[$_SESSION[APPLICATION]['FACILITY']]['kyic']['none']),
 						 			"TERMSCODE"=>array("VALUES"=>$vendorArray[$_SESSION[APPLICATION]['FACILITY']]['code']['NN']),
						  			"FOB"=>array("VALUES"=>$vendorArray[$_SESSION[APPLICATION]['FACILITY']]['code']['FF']),
 						 			"TAXGROUP"=>array("VALUES"=>$vendorArray[$_SESSION[APPLICATION]['FACILITY']]['txgp']['none'],'JS'=>'onChange="adjustTaxRates(this.options[this.selectedIndex].value);"'),
 						 			"GOODSTAXRATE"=>array("VALUES"=>$vendorArray[$_SESSION[APPLICATION]['FACILITY']]['txrt'][$TAXGROUP]),
 						 			"SERVICESTAXRATE"=>array("VALUES"=>$vendorArray[$_SESSION[APPLICATION]['FACILITY']]['txrt'][$TAXGROUP]),
						  			"OSSTOCKROOM"=>array("VALUES"=>$vendorArray[$_SESSION[APPLICATION]['FACILITY']]['stkr']['none']),
 						 			"TIMEZONE"=>array("VALUES"=>$vendorArray[$_SESSION[APPLICATION]['FACILITY']]['tmzn']['none']),
 						 			"DUMPINGCLASS"=>array("VALUES"=>$vendorArray[$_SESSION[APPLICATION]['FACILITY']]['vdcl']['none']),
 						 			"USE1099"=>array("VALUES"=>$YN,"TYPE"=>"radioButton"),
						  			"SCANSUPPSER"=>array("VALUES"=>$YN,"TYPE"=>"radioButton"),
						  			"SHIPLEADUNIT"=>array("VALUES"=>array(1=>"Days",2=>"Hours"),"TYPE"=>"radioButton"),
 						 			"ROUNDTODAYS"=>array("VALUES"=>array(1=>"Round Up",2=>"Round Down",3=>"Do Not Round"),"TYPE"=>"radioButton"),
  									"REQEDIREMIT"=>array("VALUES"=>array(1=>"Yes",2=>"No"),"TYPE"=>"radioButton"),
						  			"EDIPARTPAYMNT"=>array("VALUES"=>array(1=>"Yes",2=>"No"),"TYPE"=>"radioButton"),
  									"PROCSERIASN"=>array("VALUES"=>array(1=>"Yes",2=>"No"),"TYPE"=>"radioButton"),
  									"VENDORTYPE"=>array("VALUES"=>array(1=>"Standard",2=>"A/P Only"),"TYPE"=>"radioButton"),
  									"INITDSPSCHEDR"=>array("VALUES"=>array(1=>"By Ship Date",2=>"By Required Date"),"TYPE"=>"radioButton"),
						  			"DFTCARRIER"=>array("VALUES"=>$vendorArray[$_SESSION[APPLICATION]['FACILITY']]['shcr']['none']),
  									"DFTCSTBROKER"=>array("VALUES"=>$vendorArray[$_SESSION[APPLICATION]['FACILITY']]['cbcr']['none']),
  									"USRVERTEMPLATE"=>array("VALUES"=>$vendorArray[$_SESSION[APPLICATION]['FACILITY']]['vrth']['none']),
  									"REPAIRLOCATION"=>array("VALUES"=>$vendorArray[$_SESSION[APPLICATION]['FACILITY']]['stkr']['RPRL']),
						  			"COMMODITYCAT"=>array("VALUES"=>$vendorArray[$_SESSION[APPLICATION]['FACILITY']]['pgtp']['1'],"JS"=>"onchange='getCommodityLevels(2);'"),
						  			"COMMODITYCAT2"=>array("VALUES"=>array(),"JS"=>"onchange='getCommodityLevels(3);'"),
						  			"COMMODITYCAT3"=>array("VALUES"=>array(),"JS"=>"onchange='getCommodityLevels(4);'"),
						  			"COMMODITYCAT4"=>array("VALUES"=>array()),
  									"APBANK"=>array("VALUES"=>$vendorArray[$_SESSION[APPLICATION]['FACILITY']]['cont']['none']),
  									"EMAILADDRESS"=>"EMAIL",
  									"INACTIVEREASON"=>array("VALUES"=>$vendorArray[$_SESSION[APPLICATION]['FACILITY']]['irea']['3']),
  									"VENDORPHONE"=>array("JS"=>"onkeypress=\"return isNumberKey(event)\""),
  									"VENDORFAX"=>array("JS"=>"onkeypress=\"return isNumberKey(event)\""),
  									"MRPNOTIFICATION"=>array("VALUES"=>array("1"=>"Not Used","2"=>"Print Only","3"=>"Send"),"TYPE"=>"radioButton"),
  									"VENDORSTATUS"=>array("VALUES"=>array('I'=>'Inactive','A'=>'Active'),"TYPE"=>"radioButton","JS"=>"onchange=(setInactiveReason());"),
  									"POSENDMODE"=>array("VALUES"=>array(1=>"Printed",2=>"Send Mode",3=>"EDI",4=>"Not Required")));
  	$this->hiddenFields = array("ASNMAPPING");
  	$this->fieldOptional = array("EFTREQUIRED","VALIDATED","TIMEZONE","DFTCARRIER","OSSTOCKROOM","COMMODITYCAT2","COMMODITYCAT3","COMMODITYCAT4","GSTLICENSE","FEDERALID","QCINSPECTOR","REPAIRLOCATION","VENDORADDRESS2","VENDORADDRESS3","VENDORFAX","VENDORWEBADDRESS","DUNS","VENDORCLASS","REQOLDVENDCD","VENDORFAX","REMITTOVENDOR","DUMPINGCLASS","DFTCSTBROKER","USRVERTEMPLATE","INACTIVEREASON");
  	parent::__construct();
 // 	$this->optionalField('set');
   	if (isset($vendorArray) and is_array($vendorArray) and array_key_exists($_SESSION[APPLICATION]['FACILITY'], $vendorArray)) {
    		$this->storedFacility = $_SESSION[APPLICATION]['FACILITY'];
   	}
}
function reset () {
	$this->__construct();
}
public function getFieldOptional() {
	return $this->fieldOptional;
}
function resetColumns($columnName) {
	global $TAXGROUP;
	global $vendorArray;
	global $VENDORCOUNTRY;
	switch ($columnName) {
		case 'GOODSTAXRATE':	$this->fieldMetaData["GOODSTAXRATE"] = array("VALUES"=>$vendorArray[$_SESSION[APPLICATION]['FACILITY']]['txrt'][$TAXGROUP]);
								break;
		case 'SERVICESTAXRATE':	$this->fieldMetaData["SERVICESTAXRATE"] = array("VALUES"=>$vendorArray[$_SESSION[APPLICATION]['FACILITY']]['txrt'][$TAXGROUP]);
								break;
		case 'VENDORSTATE':		if (isset($VENDORCOUNTRY) and !empty($VENDORCOUNTRY)) {
									unset($this->fieldMetaData["VENDORSTATE"]['VALUES']);
									foreach ($vendorArray[$_SESSION[APPLICATION]['FACILITY']]['prov']['none'] as $provKey=>$provValue) {
										$stateParts = explode(',',$provValue);
										if (trim($stateParts[count($stateParts) - 1]) == trim($VENDORCOUNTRY)) {
											$this->fieldMetaData["VENDORSTATE"]['VALUES'][$provKey] = $provValue;
										}
									}
								} else {
									unset($this->fieldMetaData["VENDORSTATE"]['VALUES']);
									$this->fieldMetaData["VENDORSTATE"]['VALUES'] = $vendorArray[$_SESSION[APPLICATION]['FACILITY']]['prov']['none'];
								}
								break;
	}
}
private function checkFacilityChange() {
	global $vendorArray;
	$currentFacility = $_SESSION[APPLICATION]['FACILITY'];
	if ($this->storedFacility != $currentFacility or empty($vendorArray) or !is_array($vendorArray) or count($vendorArray) == 0) {
		$this->__construct();
	}
}
function getCommodityDD($level,$overrides=array()) {
	global $vendorArray;
	$ddFieldRow = file_get_contents('views/view.dropdownFormField.php');
	$templateArray = array("%text%","%name%","%value%","%size%","%maxlength%","%Js%","%multiple%","%Id%","%rowId%","%rows%","%rowClass%");
	$codeValue = '';
	$commodity1 = '';
	$commodity2 = '';
	$commodity3 = '';
	$commodity4 = '';
	$levelArray = array();
	if (isset($_REQUEST['COMMODITYCAT']) and $level > 1) {
		$codeValue .= trim($_REQUEST['COMMODITYCAT']);
		$commodity1 = $_REQUEST['COMMODITYCAT'];
	}
	if (isset($_REQUEST['COMMODITYCAT2']) and $level > 2) {
		$codeValue .= trim($_REQUEST['COMMODITYCAT2']);
		$commodity2 = $_REQUEST['COMMODITYCAT2'];
	}
	if (isset($_REQUEST['COMMODITYCAT3']) and $level > 3) {
		$codeValue .= trim($_REQUEST['COMMODITYCAT3']);
		$commodity3 = $_REQUEST['COMMODITYCAT3'];
	}
	if (isset($_REQUEST['COMMODITYCAT4'])) {
		$commodity4 = $_REQUEST['COMMODITYCAT4'];
	}

	switch($level) {
		case	1:	$dropDown = new dropDown('COMMODITYCAT',$this->fieldMetaData['COMMODITYCAT']['VALUES']);
					$dataDD = $dropDown->getDropDownOptions($commodity,true);
					if (isset($overrides) and is_array($overrides)) {
						$overrides = array_merge($overrides,array('TEXT'=>'&nbsp;&nbsp;Commodity Category'));
					} else {
						$overrides = array('TEXT'=>'&nbsp;&nbsp;Commodity Category');
					}
					$templateData = array('*&nbsp;Commodity Category','COMMODITYCAT',$dropDown->getDropDownOptions($commodity1,true),null,null,null,null,'COMMODITYCAT','row_COMMODITYCAT',null);
					$data = $this->renderFormLine('COMMODITYCAT',$_REQUEST['COMMODITYCAT'], null, $overrides);
					//	$data = str_replace($templateArray,$templateData,$ddFieldRow);
					break;
		case 	2:	if (!empty($codeValue) and !empty($commodity1)) {
						foreach ($vendorArray[$_SESSION[APPLICATION]['FACILITY']]['pgtp']['2'] as $pgtpKey=>$pgtpValue) {
							if (substr($pgtpValue,0,strlen($codeValue)) == $codeValue) {
								$levelArray[substr($pgtpKey,strlen($codeValue))] = substr($pgtpValue,strlen($codeValue));
							}
						}
					}
					$this->fieldMetaData['COMMODITYCAT2']['VALUES'] = $levelArray;
					$data = $this->renderFormLine('COMMODITYCAT2', $_REQUEST['COMMODITYCAT2'],null,$overrides);
				//	$dropDown = new dropDown('COMMODITYCAT2',$levelArray);
				//	$dataDD = $dropDown->getDropDownOptions($commodity2,false);
				//	$templateData = array('&nbsp;','COMMODITYCAT2',$dropDown->getDropDownOptions($commodity2,false),null,null,null,null,'COMMODITYCAT2','row_COMMODITYCAT2',null);
				//	$data = str_replace($templateArray,$templateData,$ddFieldRow);
					break;
		case 	3:	if (!empty($codeValue) and !empty($commodity2)) {
						foreach ($vendorArray[$_SESSION[APPLICATION]['FACILITY']]['pgtp']['3'] as $pgtpKey=>$pgtpValue) {
							if (substr($pgtpValue,0,strlen($codeValue)) == $codeValue) {
								$levelArray[substr($pgtpKey,strlen($codeValue))] = substr($pgtpValue,strlen($codeValue));
							}
						}
					}
					$this->fieldMetaData['COMMODITYCAT3']['VALUES'] = $levelArray;
					$data = $this->renderFormLine('COMMODITYCAT3', $_REQUEST['COMMODITYCAT3'],null,$overrides);
				//	$dropDown = new dropDown('COMMODITYCAT3',$levelArray);
				//	$dataDD = $dropDown->getDropDownOptions($commodity3,false);
				//	$templateData = array('&nbsp;','COMMODITYCAT3',$dropDown->getDropDownOptions($commodity3,false),null,null,null,null,'COMMODITYCAT3','row_COMMODITYCAT3',null);
				//	$data = str_replace($templateArray,$templateData,$ddFieldRow);
					break;
		case 	4:	if (!empty($codeValue) and !empty($commodity3)) {
						foreach ($vendorArray[$_SESSION[APPLICATION]['FACILITY']]['pgtp']['4'] as $pgtpKey=>$pgtpValue) {
							if (substr($pgtpValue,0,strlen($codeValue)) == $codeValue) {
								$levelArray[substr($pgtpKey,strLen($codeValue))] = substr($pgtpValue,strlen($codeValue));
							}
						}
					}
					$this->fieldMetaData['COMMODITYCAT4']['VALUES'] = $levelArray;
					$data = $this->renderFormLine('COMMODITYCAT4', $_REQUEST['COMMODITYCAT4'],null,$overrides);
				//	$dropDown = new dropDown('COMMODITYCAT4',$levelArray);
				//	$dataDD = $dropDown->getDropDownOptions($commodity4,false);
				//	$templateData = array('&nbsp;','COMMODITYCAT4',$dropDown->getDropDownOptions($commodity4,false),null,null,null,null,'COMMODITYCAT4','row_COMMODITYCAT4',null);
				//	$data = str_replace($templateArray,$templateData,$ddFieldRow);
					break;
	}
	unset($dropDown);
	return $data;
}
function infoIsValid($action, $testParent=false) {
	global $errorText;
	global $errorColumn;
	include('includes/vendorMap.php');
	$regPhone = "/^[0-9 xX()-]*$/";
	$this->checkFacilityChange();
	if (isset($_REQUEST['VENDORACTION']) and $_REQUEST['VENDORACTION'] == 'Add') {
		$this->optionalField('Unset',array('REQVENDTYPE','REQPURCHFREQ','REQFIRSTTIME','REQINACTIVEOLD'));
		$this->optionalField('Set',array('VENDORSTATUS'));
	} else {
		if (isset($_REQUEST['VENDORACTION']) and $_REQUEST['VENDORACTION'] != 'Add') {
			$this->optionalField('Unset',array('VENDORSTATUS'));
			$this->optionalField('Set',array('REQVENDTYPE','REQPURCHFREQ','REQFIRSTTIME','REQINACTIVEOLD','REQOLDVENDCD','ASNMAPPING'));
			$setArray = array();
			foreach ($this->metaData['COLUMNS'] as $columnName=>$columnData) {
				if (!in_array($columnName,$updateMap)) {
					$setArray[] = $columnName;
				}
			}
		}
	}

	if (isset($setArray) and is_array($setArray) and count($setArray) > 0) {
		$this->optionalField('Set',$setArray);
	}
	if (isset($_REQUEST['REQINACTIVEOLD']) and $_REQUEST['REQINACTIVEOLD'] == 'Y' and (!isset($_REQUEST['INACTIVEREASON']) or $_REQUEST['INACTIVEREASON'] == '')) {
		$errorText[] = 'Inactive Reason missing or invalid';
		$errorColumn[] = 'INACTIVEREASON';
	}
	if (isset($_REQUEST['VENDORSTATUS']) and $_REQUEST['VENDORSTATUS'] == 'I' and (!isset($_REQUEST['INACTIVEREASON']) or $_REQUEST['INACTIVEREASON'] == '')) {
		$errorText[] = 'Inactive Reason missing or invalid';
		$errorColumn[] = 'INACTIVEREASON';
	}
	if (isset($_REQUEST['VENDORPHONE']) and !preg_match($regPhone,$_REQUEST['VENDORPHONE'])) {
		$errorText[] = 'Incorrect Characters in Vendor Phone Number';
		$errorColumn[] = 'VENDORPHONE';
	}
	if (isset($_REQUEST['VENDORFAX']) and !preg_match($regPhone,$_REQUEST['VENDORFAX'])) {
		$errorText[] = 'Incorrect Characters in Vendor Fax Number';
		$errorColumn[] = 'VENDORFAX';
	}
	if (isset($_REQUEST['VENDORCOUNTRY'])) {
		$vendorCountryText = strtoupper($this->fieldMetaData['VENDORCOUNTRY']['VALUES'][$_REQUEST['VENDORCOUNTRY']]);
		if (substr($vendorCountryText,strpos($vendorCountryText,'-')+2) == 'UNITED STATES') {
			if (!isset($_REQUEST['FEDERALID']) or trim($_REQUEST['FEDERALID']) == '') {
				$errorText[] = 'Federal ID is required for vendors inside the United States of America';
				$errorColumn[] = 'FEDERALID';
			}
		}
		//if (substr($vendorCountryText,strpos($vendorCountryText,'-')+2) == 'CANADA') {
			//if (!isset($_REQUEST['GSTLICENSE']) or trim($_REQUEST['GSTLICENSE']) == '') {
				//$errorText[] = 'GST License is required for vendors inside Canada';
			//}
		//}
	}
	if (isset($_REQUEST['REMITTOVENDOR']) and isset($_REQUEST['APBANK'])) {
		$vendor = new vend();
		$remitBank = null; 
		if ($vendor->select(array('BTBANK'),array('WHERE'=>array('BTVEND'=>$_REQUEST['REMITTOVENDOR'])))) {
			if ($remitRow = $vendor->getnext()) {
				$remitBank = $remitRow['BTBANK'];
			}
		}
		unset ($vendor);
		$bank = new cont();
		if ($bank->select(array('IFCURR'),array('WHERE'=>array('IFCOM#'=>$_SESSION[APPLICATION]['company'],'IFBNK#'=>$remitBank)))) {
			if ($bankRow = $bank->getnext()) {
				$remitCurr = $bankRow['IFCURR'];
			}
		}
		unset($bankRow);
		if ($bank->select(array('IFCURR'),array('WHERE'=>array('IFCOM#'=>$_SESSION[APPLICATION]['company'],'IFBNK#'=>$_REQUEST['APBANK'])))) {
			if ($bankRow = $bank->getnext()) {
				$thisCurr = $bankRow['IFCURR'];
			}
		}
		unset ($bank);
		if (isset($remitCurr) and isset($thisCurr) and $remitCurr <> $thisCurr) {
			$errorText[] = 'Remit To Bank Currency is different than this vendors Bank Currency';
			$errorColumn[] = 'REMITTOVENDOR';
		}
	}
	if (count($errorText) == 0) {
		$formReq = new formrequest();
		error_log('VFID is '.$_REQUEST['VFID']);
		if (!isset($_REQUEST['VFID']) or
		!$formReq->select(array('"status"'),array('WHERE'=>array('formID'=>$_REQUEST['VFID']))) or 
		!$formRow = $formReq->getnext() or 
		($formRow['status'] == 'New' and $testParent != true)) {
			error_log('no check necessary in vendorform');
			return true;
		} else {
			error_log('checking parent class for errors');
		    return parent::infoIsValid($action);
		}
	} else {
		error_log('errors found in vendorform class');
		return false;
	}
}
function renderFormData($row=null,$allowKeyChange = true) {
	$this->checkFacilityChange();
	return parent::renderFormData($row,$allowKeyChange);
}
function renderFormLine($columnName, $columnData, $allowKeyChange=true, $overrides=array()) {
 	$this->checkFacilityChange();
	return parent::renderFormLine($columnName, $columnData, $allowKeyChange, $overrides);
}
function setValuesArray($columnName, $values) {
	if (isset($values) and is_array($values) and isset($columnName) and array_key_exists($columnName, $this->fieldMetaData)) {
		$this->fieldMetaData[$columnName]['VALUES'] = $values;
	}
}
public function setCustomConditions($objProcessInstance, $objProcessTransitionArray) {
//	switch($objProcessInstance->getCurrentProcessStateID()) {
//		case intPROCESS_STATE_ID_EC_SOLICITING_FEEDBACK:
//			$this->getECSolicitUserArray()->loadByEngineeringChangeID($this->getEngineeringChangeID());
			if (isset($_SESSION[APPLICATION]['COMMODITYCAT'])) {
				$thisCommodity[0] = trim($_SESSION[APPLICATION]['COMMODITYCAT']);
				$thisCommodity[1] = $thisCommodity[0];
				$thisCommodity[2] = $thisCommodity[1];
				$thisCommodity[3] = $thisCommodity[2];
				if (isset($_SESSION[APPLICATION]['COMMODITYCAT2']) and !empty($_SESSION[APPLICATION]['COMMODITYCAT2'])) {
					$thisCommodity[0] .= trim($_SESSION[APPLICATION]['COMMODITYCAT2']);
					$thisCommodity[1] .= trim($_SESSION[APPLICATION]['COMMODITYCAT2']);
					$thisCommodity[2] .= trim($_SESSION[APPLICATION]['COMMODITYCAT2']);
					if (isset($_SESSION[APPLICATION]['COMMODITYCAT3']) and !empty($_SESSION[APPLICATION]['COMMODITYCAT3'])) {
						$thisCommodity[0] .= trim($_SESSION[APPLICATION]['COMMODITYCAT3']);
						$thisCommodity[1] .= trim($_SESSION[APPLICATION]['COMMODITYCAT3']);
						if (isset($_SESSION[APPLICATION]['COMMODITYCAT4']) and !empty($_SESSION[APPLICATION]['COMMODITYCAT4'])) {
							$thisCommodity[0] .= trim($_SESSION[APPLICATION]['COMMODITYCAT4']);
						}
					}
				}
			}
			if (isset($_SESSION[APPLICATION]['dbSchema'])) {
				$thisFacility = $_SESSION[APPLICATION]['dbSchema'];
			}
//			error_log("\$thisFacility: [" . $thisFacility . "]");
			$arrRoleIDs[] = vf_BITeamRole;
			$arrRoleIDs[] = vf_generalRole;
			$objConditionalUserArray = new conditionalUserArray();
			if ($objConditionalUserArray->checkForTooling($thisFacility)) {
			    $objConditionalUserArray->loadForTooling($thisFacility);
			    $arrRoleIDs[] = vf_ToolingRole;
			} else {
				error_log(date('Y-m-d H:i:s').' conditional user processing for instance '.$objProcessInstance->getProcessInstanceID()."\n",3,'/usr/local/zendsvr6/var/log/conditionaluser.log');
				error_log(date('Y-m-d H:i:s').' commodity code is: '.$thisCommodity[0]."\n",3,'/usr/local/zendsvr6/var/log/conditionaluser.log');
				$i = 0;
				while ($i < 4) {
					if ($objConditionalUserArray->checkForCommodity($thisCommodity[$i]) or $i == 3) {
						$objConditionalUserArray->loadByCommodity($thisCommodity[$i]);
						$arrRoleIDs[] = vf_DirectorRole;
						$i = 4;
					} else {
						$i++;
					}
				}
			}

			//if($objConditionalUserArray->getArray())
			//	return;
			foreach($objConditionalUserArray->getArray() as $objConditionalUser) {
				$objConditionalRoleArray = new conditionalRoleArray();
				$objConditionalRoleArray->loadConditionalRolesByUser($objConditionalUser->getUserID(), $arrRoleIDs);
				foreach($objConditionalRoleArray->getArray() as $objConditionalRole) {
					if(!$objProcessInstance->getRoleArray()->objectIsSet($objConditionalRole->getRoleID())) {
						$objProcessInstance->getRoleArray()->setObject($objConditionalRole->getRoleID(), $objConditionalRole);
					}
 					if(!$objProcessInstance->getRoleArray()->getObject($objConditionalRole->getRoleID())->getUserArray()->objectIsSet($objConditionalUser->getUserID())) {
						$objProcessInstance->getRoleArray()->getObject($objConditionalRole->getRoleID())->getUserArray()->setObject($objConditionalUser->getUserID(), $objConditionalUser);
					}
				}
			}

			foreach($objProcessInstance->getRoleArray()->getArray() as $objRole) {
				foreach($objProcessTransitionArray->getArray() as $objProcessTransition) {
					$objProcessTransition->getConditionArray()->loadByTopLevelConditionID($objProcessTransition->getConditionID());
					$objConditionRoles = $objProcessTransition->getConditionArray()->getRoleArray();
					foreach($objConditionRoles->getArray() as $rolesObj) {
						$transitionRoles[] = $rolesObj->getRoleID();
					}
					if(!$objRole->getProcessTransitionArray()->objectIsSet($objProcessTransition->getProcessTransitionID()) and in_array($objRole->getRoleID(),$transitionRoles)) {
						$objRole->getProcessTransitionArray()->setObject($objProcessTransition->getProcessTransitionID(),$objProcessTransition);
					}
					unset ($transitionRoles);
					unset ($objConditionRoles);
					unset ($objProcessTransition);
				}
			}

		//	foreach($objProcessInstance->getRoleArray()->getArray() as $objRole) {
		//		$objRole->getProcessTransitionArray()->setArray($objProcessTransitionArray->getArray());
		//	}

//			break;
//	}
}

public function needsDirectorApproval($formID) {
	$this->select(null,array('WHERE'=>array('VFID'=>$formID)));
	if ($formRow = $this->getnext()) {
		error_log('getting custom conditions');
		if (isset($formRow['COMMODITYCAT'])) {
			$thisCommodity[0] = trim($formRow['COMMODITYCAT']);
			$thisCommodity[1] = $thisCommodity[0];
			$thisCommodity[2] = $thisCommodity[1];
			$thisCommodity[3] = $thisCommodity[2];
			if (isset($formRow['COMMODITYCAT2']) and !empty($formRow['COMMODITYCAT2'])) {
				$thisCommodity[0] .= trim($formRow['COMMODITYCAT2']);
				$thisCommodity[1] .= trim($formRow['COMMODITYCAT2']);
				$thisCommodity[2] .= trim($formRow['COMMODITYCAT2']);
				if (isset($formRow['COMMODITYCAT3']) and !empty($formRow['COMMODITYCAT3'])) {
					$thisCommodity[0] .= trim($formRow['COMMODITYCAT3']);
					$thisCommodity[1] .= trim($formRow['COMMODITYCAT3']);
					if (isset($formRow['COMMODITYCAT4']) and !empty($formRow['COMMODITYCAT4'])) {
						$thisCommodity[0] .= trim($formRow['COMMODITYCAT4']);
					}
				}
			}
		}
		$objConditionalUserArray = new conditionalUserArray();
		if ($objConditionalUserArray->checkForTooling($formRow['dbName'])) {
			return true;
		} else {
			$i = 0;
			while ($i < 4) {
				error_log(date('Y-m-d H:i:s').' conditional user processing for form Id '.$formID."\n",3,'/usr/local/zendsvr6/var/log/conditionaluser.log');
				if ($objConditionalUserArray->checkForCommodity($thisCommodity[$i])) {
					return true;
				} else {
					$i++;
				}
			}
			return false;
		}
	}
}

public function getCustomConditionUsers($formID) {
	$userArray = array();
	$this->select(null,array('WHERE'=>array('VFID'=>$formID)));
	if ($formRow = $this->getnext()) {
	    error_log('getting custom conditions');
		if (isset($formRow['COMMODITYCAT'])) {
			$thisCommodity[0] = trim($formRow['COMMODITYCAT']);
			$thisCommodity[1] = $thisCommodity[0];
			$thisCommodity[2] = $thisCommodity[1];
			$thisCommodity[3] = $thisCommodity[2];
 			if (isset($formRow['COMMODITYCAT2']) and !empty($formRow['COMMODITYCAT2'])) {
				$thisCommodity[0] .= trim($formRow['COMMODITYCAT2']);
				$thisCommodity[1] .= trim($formRow['COMMODITYCAT2']);
				$thisCommodity[2] .= trim($formRow['COMMODITYCAT2']);
				if (isset($formRow['COMMODITYCAT3']) and !empty($formRow['COMMODITYCAT3'])) {
					$thisCommodity[0] .= trim($formRow['COMMODITYCAT3']);
					$thisCommodity[1] .= trim($formRow['COMMODITYCAT3']);
					if (isset($formRow['COMMODITYCAT4']) and !empty($formRow['COMMODITYCAT4'])) {
						$thisCommodity[0] .= trim($formRow['COMMODITYCAT4']);
					}
				}
			}
		}
		//if (isset($_SESSION[APPLICATION]['dbSchema'])) {
		//	$thisFacility = $_SESSION[APPLICATION]['dbSchema'];
		//}
		$objConditionalUserArray = new conditionalUserArray();
		if ($objConditionalUserArray->checkForTooling($formRow['dbName'])) {
			$objConditionalUserArray->loadForTooling($formRow['dbName']);
			foreach($objConditionalUserArray->getArray() as $objConditionalUser) {
				if (!in_array($objConditionalUser->getUserID(),$userArray)) {
					$userArray[] = $objConditionalUser->getUserID();
					error_log('adding tooling user '.$objConditionalUser->getUserID().' for form '.$formID."\n",3,"/usr/local/zendsvr6/var/log/conditionaluser.log");
				}
			}
			return $userArray;
		} else {
			$i = 0;
			while ($i < 4) {
				error_log(date('Y-m-d H:i:s').' conditional user processing for form Id '.$formID."\n",3,'/usr/local/zendsvr6/var/log/conditionaluser.log');
				if ($objConditionalUserArray->checkForCommodity($thisCommodity[$i]) or $i == 3) {
					$objConditionalUserArray->loadByCommodity($thisCommodity[$i]);
					foreach($objConditionalUserArray->getArray() as $objConditionalUser) {
						if (!in_array($objConditionalUser->getUserID(),$userArray)) {
							$userArray[] = $objConditionalUser->getUserID();
						}
					}
					$i = 4;
				} else {
					$i++;
				}
			}
			return $userArray;
		}
	}
}

public function	evaluateProcessTransition($objProcessTransitionInstance) {
	switch ($objProcessTransitionInstance->getProcessTransition()->getFromProcessStateID()) {
		case vf_customState:
			switch ($objProcessTransitionInstance->getProcessTransitionID()) {
				case vf_ConditionalApprove:
				case vf_ConditionalResubmit:
				case vf_ConditionalReject:
				case vf_ConditionalPost:
					return true;
				    break;
			}
		break;
	}
}
public function validateWorkflowAction($intProcessTransitionID, $intRoleID, $processInstanceID, $formID) {
	global $errorText;
	global $errorColumn;
	global $vendorMap;
	$arrErrors = array();
	error_log('validating workflow in vendorform');
	$transition = new ProcessTransition($intProcessTransitionID);
	$this->select(null,array('WHERE'=>array('VFID'=>$formID)));
	if ($thisData = $this->getnext()) {
		if (trim($thisData['CORPVENDMAST']) == 'NEW') {
			if ($transition->getToProcessState(true)->getFinalState() == 1) {
			$arrErrors[] = "Corporate Vendor Master must be specified before posting.";
			error_log('corporate vendor master is missing');
			}
		}
		if ($thisData['VENDORACTION'] == 'Add') {
		    $vend = new vend();
		    $vend->select(array('BTVEND'),array('WHERE'=>array('BTVEND'=>strtoupper($thisData['VENDORCODE']))));
		    if ($thisRow = $vend->getnext()) {
		        $arrErrors[] = 'New Vendor Specified but Vendor already exists';
		    }
		} else {
		    $vend = new vend();
		    $vend->select(array('BTVEND'),array('WHERE'=>array('BTVEND'=>strtoupper($thisData['VENDORCODE']))));
		    if (!$thisRow = $vend->getnext()) {
		        $arrErrors[] = 'Vendor Change Specified but Vendor not found';
		    }
		}
	}
	unset($transition);
	if ($thisData['VALIDATED'] != 'Y') {
		foreach ($thisData as $thisKey=>$thisValue) {
			$_REQUEST[$thisKey] = $thisValue;
		}
		if (!$this->infoIsValid('UPDATE',true)) {
			$arrErrors = array_merge($arrErrors,$errorText);
		} else {
			$_REQUEST['VALIDATED'] = 'Y';
			$this->update(false);
		}
	}
	return $arrErrors;
}

	public function getResultSet($formID) {
		$this->select(null,array('WHERE'=>array('VFID'=>$formID)));
		if ($formRow = $this->getnext()) {
			return $formRow;
		}
	}

public function postForm($formID) {
	global $errorText;
	global $vendorMap;
	global $updateMap;
	include_once('classes/class.vend.php');
	include_once('classes/class.usrc.php');
	include_once('classes/class.venx.php');
	include_once('classes/class.cont.php');
	error_log('Posting form '.$formID);
	$nameArray = explode('@',$_SESSION[APPLICATION]['email']);
	$userName = $nameArray[0];
	if (strlen($userName) > 10) {
		$userName = substr($userName,0,10);
	}
	$this->select(null,array('WHERE'=>array('VFID'=>$formID)));
	if ($formRow = $this->getnext()) {
		if (isset($formRow['VENDORCODE'])) {
			$VendorCode = $formRow['VENDORCODE'];
			$vendor = new vend();
			$vendor->select(null,array('WHERE'=>array('BTVEND'=>$VendorCode)));
			$FacilityArray = explode(':',$_SESSION[APPLICATION]['FACILITY']);
			include_once('includes/vendorMap.php');
			// If state is a combined state/country then swap it back to just the state
			if (isset($formRow['VENDORSTATE']) and strPos($formRow['VENDORSTATE'],':') !== false) {
				$tempArray = array();
				$tempArray = explode(':',$formRow['VENDORSTATE']);
				$formRow['VENDORSTATE'] = $tempArray[0];
			}
			// End special state handling for combined state/country
			if ($vendorInfo = $vendor->getnext()) {
				foreach($vendorMap as $vendorCol=>$webCol) {
					if (isset($formRow[$webCol]) and $vendorInfo[$vendorCol] != $formRow[$webCol] and in_array($webCol,$updateMap)) {
						$_REQUEST[$vendorCol] = $formRow[$webCol];
					} else {
						if (isset($_REQUEST[$vendorCol])) {
							unset($_REQUEST[$vendorCol]);
						}
					}
				}
				if ($vendorInfo['BTCURR'] == '  ' and $vendorInfo['BTBANK'] != ' ') {
					$bank = new cont();
					$bank->select(array('IFCURR'),array('WHERE'=>array('IFCOM#'=>$FacilityArray[2],'IFBNK#'=>$vendorInfo['BTBANK'])));
					if($bankRow = $bank->getnext()) {
						$_REQUEST['BTCURR'] = $bankRow['IFCURR'];
					}
					unset($bank);
				}
			//	if (!isset($_REQUEST['BTSTAT'])) {
			//		$_REQUEST['BTSTAT'] = 'A';
			//	}
				if (!isset($_REQUEST['BTREAS'])) {
					$_REQUEST['BTREAS'] = ' ';
				}
				if (!isset($_REQUEST['BTVEND'])) {
					$_REQUEST['BTVEND'] = $VendorCode;
				}
				$_REQUEST['BTPGRP'] = trim($formRow['COMMODITYCAT']).trim($formRow['COMMODITYCAT2']).trim($formRow['COMMODITYCAT3']).trim($formRow['COMMODITYCAT4']);
				$_REQUEST['BTUUSR'] = 'FNGDWS';
				$_REQUEST['BTUDAT'] = date('Y-m-d');
				$_REQUEST['BTUTME'] = date('H:i:s');
				if ($vendor->update()) {
					$actionResult = 'Success';
					$usrc = new usrc();
					$_REQUEST['MFSRCE'] = 'MN';
					$_REQUEST['MFKEY1'] = 'VENDOR MASTER';
					$FacilityArray = explode(':',$_SESSION[APPLICATION]['FACILITY']);
					$fwCompId = new fwcompid();
					$fwCompId->select(array('FWFUT04'),array('WHERE'=>array('FWDATC'=>$FacilityArray[0],'FWPLTC'=>$FacilityArray[1],'FWGLCO'=>$FacilityArray[2])));
					if ($fwCompRow = $fwCompId->getnext()) {
						$_REQUEST['MFENT#'] = trim($fwCompRow['FWFUT04']);
					}
					if (empty($_REQUEST['MFENT#'])) {
						$_REQUEST['MFENT#'] = 100;
					}
					$_REQUEST['MFKEY2'] = $VendorCode;
					$_REQUEST['MFRESP'] = $formRow['CORPVENDMAST'];
					$usrc->select(null,array('WHERE'=>array('MFSRCE'=>'MN','MFKEY1'=>'VENDOR MASTER','MFKEY2'=>$VendorCode,'MFENT#'=>$_REQUEST['MFENT#'])));
					if ($usrcRow = $usrc->getnext()) {
						$usrc->update();
					} else {
						$usrc->insert();
					}
					//$_REQUEST['DA0VEND'] = $VendorCode;
					//$_REQUEST['DA0MRPN'] = $formRow['MRPNOTIFICATION'];
					//$venx = new venx();
					//$venx->select(null,array('WHERE'=>array('DA0VEND'=>$VendorCode)));
					//if ($venx->getnext()) {
					//	$venx->update();
					//} else {
					//	$venx->insert();
					//}
					if (isset($_REQUEST['BTNAME']) and $vendorInfo['BTNAME'] != $_REQUEST['BTNAME'] and trim($_REQUEST['BTNAME'] != '')) {
					    // Vendor name changed so change all active POH records with that same
					    //  vendor
					    include_once('classes/class.poh.php');
					    $POH = new poh();
					    error_log('formID: '.$formID.', changing PO Name from '.$vendorInfo['BTNAME'].' to '.$_REQUEST['BTNAME'].' where vendor is '.$_REQUEST['BTVEND']);
					    $POH->updateAll(array('KAOVNM'=>$_REQUEST['BTNAME']), array('WHERE'=>array('KAOVND'=>$_REQUEST['BTVEND'],'KACRCM'=>'1')));
					}
				} else {
					$actionResult = 'Fail';
				}
			} else {
				foreach($vendorMap as $vendorCol=>$webCol) {
					if (isset($formRow[$webCol])) {
						$_REQUEST[$vendorCol] = $formRow[$webCol];
					}
				}
				$_REQUEST['BTSTAT'] = 'A';
				$_REQUEST['BTREAS'] = ' ';
				if (!isset($_REQUEST['BTVEND'])) {
					$_REQUEST['BTVEND'] = $VendorCode;
				}
				$bank = new cont();
				$bank->select(array('IFCURR'),array('WHERE'=>array('IFCOM#'=>$FacilityArray[2],'IFBNK#'=>$formRow['APBANK'])));
				if($bankRow = $bank->getnext()) {
					$_REQUEST['BTCURR'] = $bankRow['IFCURR'];
				}
				unset($bank);
				$_REQUEST['BTCUSR'] = 'FNGDWS';
				$_REQUEST['BTUUSR'] = $_REQUEST['BTCUSR'];
				$_REQUEST['BTCDAT'] = date('Y-m-d');
				$_REQUEST['BTUDAT'] = $_REQUEST['BTCDAT'];
				$_REQUEST['BTCTME'] = date('H:i:s');
				$_REQUEST['BTUTME'] = $_REQUEST['BTCTME'];
				$_REQUEST['BTPGRP'] = trim($formRow['COMMODITYCAT']).trim($formRow['COMMODITYCAT2']).trim($formRow['COMMODITYCAT3']).trim($formRow['COMMODITYCAT4']);
				if ($vendor->insert()) {
					$actionResult = 'Success';
					$usrc = new usrc();
					$_REQUEST['MFSRCE'] = 'MN';
					$_REQUEST['MFKEY1'] = 'VENDOR MASTER';
					$FacilityArray = explode(':',$_SESSION[APPLICATION]['FACILITY']);
					$fwCompId = new fwcompid();
					$fwCompId->select(array('FWFUT04'),array('WHERE'=>array('FWDATC'=>$FacilityArray[0],'FWPLTC'=>$FacilityArray[1],'FWGLCO'=>$FacilityArray[2])));
					if ($fwCompRow = $fwCompId->getnext()) {
						$_REQUEST['MFENT#'] = trim($fwCompRow['FWFUT04']);
					}
					if (empty($_REQUEST['MFENT#'])) {
						$_REQUEST['MFENT#'] = 100;
					}
					$_REQUEST['MFKEY2'] = $VendorCode;
					$_REQUEST['MFRESP'] = $formRow['CORPVENDMAST'];
					$usrc->select(null,array('WHERE'=>array('MFSRCE'=>'MN','MFKEY1'=>'VENDOR MASTER','MFKEY2'=>$VendorCode,'MFENT#'=>$_REQUEST['MFENT#'])));
					if ($usrcRow = $usrc->getnext()) {
						$usrc->update();
					} else {
						$usrc->insert();
					}
					$_REQUEST['DA0VEND'] = $VendorCode;
					$_REQUEST['DA0MRPN'] = $formRow['MRPNOTIFICATION'];
					$venx = new venx();
					$venx->select(null,array('WHERE'=>array('DA0VEND'=>$VendorCode)));
					if ($venx->getnext()) {
						$venx->update();
					} else {
						$venx->insert();
					}
				} else {
					$actionResult = 'Fail';
				}
			}
			if ($actionResult == 'Success' and $formRow['REQINACTIVEOLD'] == 'Y' and !empty($formRow['REQOLDVENDCD'])) {
				$vendor->select(array('BTVEND'),array('WHERE'=>array('BTVEND'=>trim($formRow['REQOLDVENDCD']))));
				if ($oldVendInfo = $vendor->getnext()) {
					// Unset all vendor variables assigned in the previous vendor update
					foreach($vendorMap as $vendorCol=>$webCol) {
						if (isset($_REQUEST[$vendorCol])) {
							unset($_REQUEST[$vendorCol]);
						}
					}
					// set vendor variables needed in this vendor update
					$_REQUEST['BTVEND'] = $formRow['REQOLDVENDCD'];
					$_REQUEST['BTSTAT'] = 'I';
					$_REQUEST['BTREAS'] = $formRow['INACTIVEREASON'];
					$_REQUEST['BTUDAT'] = date('Y-m-d');
					$_REQUEST['BTUTME'] = date('H:i:s');
					$_REQUEST['BTUUSR'] = 'FNGDWS';
					$vendor->update();
				}
			}
		} else {
			$actionResult = 'Bad';
		}
	}
	if ($actionResult == 'Success') {
		return true;
	} else {
		return false;
	}
}
function getBody($objProcessInstance, $objProcessTransitionArray, $requestText) {
	global $config;
	$requestArray = explode(':',$requestText);
	if (count($requestArray) > 1) {
		$vendor = trim($requestArray[0]);
		$reason = trim($requestArray[1]);
	}
	$txtBody = "";
	$protArray = explode('/',$_SERVER['SERVER_PROTOCOL']);
	$protocol = strtolower($protArray[0]).'://';
	if ($_SERVER['SERVER_PORT'] != 80 and !empty($_SERVER['SERVER_PORT'])) {
		$port = ':'.trim($_SERVER['SERVER_PORT']);
	}
	$strRequestURL = $config['webURL']."/index.php?view=editForm&instanceID=".$objProcessInstance->getProcessInstanceID();
	$strMyRequestsURL = $config['webURL']."/index.php";
	$txtBody .= "<b>Vendor Maintenance - Action Required</b><br />";
	$txtBody .= "Vendor: " . $vendor. "<br />";
	$txtBody .= "Request Reason: " . $reason . "<br />";
	$txtBody .= "<br />";
	$txtBody .= "<a href=\"".$strRequestURL."\">Take action on this request</a><br />";
	$txtBody .= "<br />";
	$txtBody .= "<a href=\"$strMyRequestsURL\">View all submitted or actionable requests</a><br />";
	return $txtBody;
}
}
include_once('User.class.php');
class conditionalUserArray extends UserArray {
	function checkForCommodity($thisCommodity) {
		$thisLength = strlen($thisCommodity);
		$strSQL = "SELECT distinct tblUser.*
		FROM dbPLM.tblUser
		INNER JOIN dbPLM.tblconditionaluser
		ON tblconditionaluser.intUserID = tblUser.intUserID and
		tblconditionaluser.strConditionName = 'COMMODITY'
		INNER JOIN dbPLM.userfacilityxr
		ON tblUser.intUserID = userfacilityxr.userID
		WHERE tblconditionaluser.strconditionText = '$thisCommodity'";
		if(isset($_SESSION[APPLICATION]['dbSchema']) and $_SESSION[APPLICATION]['dbSchema'] != null) {
		$strSQL .= " AND userfacilityxr.DBase = '".$_SESSION[APPLICATION]['dbSchema']."'";
		}
		if(isset($_SESSION[APPLICATION]['plant']) and $_SESSION[APPLICATION]['plant'] != null) {
		$strSQL .= " AND userfacilityxr.Plant = '".$_SESSION[APPLICATION]['plant']."'";
		}
				if(isset($_SESSION[APPLICATION]['company']) and $_SESSION[APPLICATION]['company'] != null) {
				$strSQL .= " AND userfacilityxr.Company = ".$_SESSION[APPLICATION]['company'];
		}
			$rsResult = $this->getDB()->query($strSQL);
			if ($this->getDB()->fetch_assoc($rsResult)) {
				return true;
			}
		return false;
	}
	function loadByCommodity($thisCommodity) {
		$thisLength = strlen($thisCommodity);
		$strSQL = "SELECT distinct tblUser.*
		FROM dbPLM.tblUser
		INNER JOIN dbPLM.tblconditionaluser
		ON tblconditionaluser.intUserID = tblUser.intUserID and
		   tblconditionaluser.strConditionName = 'COMMODITY'
		INNER JOIN dbPLM.userfacilityxr
		ON tblUser.intUserID = userfacilityxr.userID
		WHERE tblconditionaluser.strconditionText = '$thisCommodity' or
		      tblconditionaluser.strconditionText = '*All'";
		if(isset($_SESSION[APPLICATION]['dbSchema']) and $_SESSION[APPLICATION]['dbSchema'] != null) {
			$strSQL .= " AND userfacilityxr.DBase = '".$_SESSION[APPLICATION]['dbSchema']."'";
		}
		if(isset($_SESSION[APPLICATION]['plant']) and $_SESSION[APPLICATION]['plant'] != null) {
			$strSQL .= " AND userfacilityxr.Plant = '".$_SESSION[APPLICATION]['plant']."'";
		}
		if(isset($_SESSION[APPLICATION]['company']) and $_SESSION[APPLICATION]['company'] != null) {
			$strSQL .= " AND userfacilityxr.Company = ".$_SESSION[APPLICATION]['company'];
		}

		$rsResult = $this->getDB()->query($strSQL);
		while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
			$this->_arrObjects[$arrRow["intUserID"]] = new User();
			$this->_arrObjects[$arrRow["intUserID"]]->setVarsFromRow($arrRow);
			$this->_arrObjects[$arrRow["intUserID"]]->setCommodity(true);
		//	error_log(date('Y-m-d h:i:s').' selected user: '.$arrRow['intUserID'].', '.$arrRow['strDisplayName']."\n",3,'/usr/local/zendsvr6/var/log/conditionaluser.log');
		}
	}
	function checkForTooling($thisFacility) {
		if (!isset($thisFacility)) {
			$thisFacility = $_SESSION[APPLICATION]['dbSchema'];
		}
		$this->userCount = 0;
		$strSQL = "SELECT distinct tblUser.*
		FROM dbPLM.tblUser
		INNER JOIN dbPLM.tblconditionaluser
		ON tblconditionaluser.intUserID = tblUser.intUserID and
		tblconditionaluser.strConditionName = 'TOOLING'
		INNER JOIN dbPLM.userfacilityxr
		ON tblUser.intUserID = userfacilityxr.userID
		WHERE tblconditionaluser.strconditionText = '$thisFacility'";
		if(isset($_SESSION[APPLICATION]['dbSchema']) and $_SESSION[APPLICATION]['dbSchema'] != null) {
		$strSQL .= " AND userfacilityxr.DBase = '".$_SESSION[APPLICATION]['dbSchema']."'";
		}
		if(isset($_SESSION[APPLICATION]['plant']) and $_SESSION[APPLICATION]['plant'] != null) {
		$strSQL .= " AND userfacilityxr.Plant = '".$_SESSION[APPLICATION]['plant']."'";
		}
		if(isset($_SESSION[APPLICATION]['company']) and $_SESSION[APPLICATION]['company'] != null) {
		$strSQL .= " AND userfacilityxr.Company = ".$_SESSION[APPLICATION]['company'];
		}
		$rsResult = $this->getDB()->query($strSQL);
		if ($this->getDB()->fetch_assoc($rsResult)) {
		  return true;
		}
		return false;
	}

	function loadForTooling($thisFacility) {
		if (!isset($thisFacility)) {
			$thisFacility = $_SESSION[APPLICATION]['dbSchema'];
		}
		$this->userCount = 0;
		$strSQL = "SELECT distinct tblUser.*
		FROM dbPLM.tblUser
		INNER JOIN dbPLM.tblconditionaluser
		ON tblconditionaluser.intUserID = tblUser.intUserID and
		tblconditionaluser.strConditionName = 'TOOLING'
		INNER JOIN dbPLM.userfacilityxr
		ON tblUser.intUserID = userfacilityxr.userID
		WHERE tblconditionaluser.strconditionText = '$thisFacility' or
		tblconditionaluser.strconditionText = '*All'";
		if(isset($_SESSION[APPLICATION]['dbSchema']) and $_SESSION[APPLICATION]['dbSchema'] != null) {
		$strSQL .= " AND userfacilityxr.DBase = '".$_SESSION[APPLICATION]['dbSchema']."'";
		}
		if(isset($_SESSION[APPLICATION]['plant']) and $_SESSION[APPLICATION]['plant'] != null) {
		$strSQL .= " AND userfacilityxr.Plant = '".$_SESSION[APPLICATION]['plant']."'";
		}
				if(isset($_SESSION[APPLICATION]['company']) and $_SESSION[APPLICATION]['company'] != null) {
				$strSQL .= " AND userfacilityxr.Company = ".$_SESSION[APPLICATION]['company'];
		}
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
			$this->_arrObjects[$arrRow["intUserID"]] = new User();
			$this->_arrObjects[$arrRow["intUserID"]]->setVarsFromRow($arrRow);
			$this->_arrObjects[$arrRow["intUserID"]]->setTooling(true);
			$this->userCount ++;
		}
		}
	function getUserCount() {
		return $this->userCount;
	}
}
include_once('Role.class.php');
class conditionalRoleArray extends roleArray {
	function loadConditionalRolesByUser($user, $arrRoleIDs) {
		$strSQL = "SELECT distinct tblRole.*
		FROM dbPLM.tblRole
		INNER JOIN dbPLM.tbluserrolexr on
		tblRole.intRoleID = tbluserrolexr.intRoleID
		WHERE tbluserrolexr.intUserID = $user and
		tbluserRolexr.intRoleID in ('".implode("','", $arrRoleIDs)."')";
		$rsResult = $this->getDB()->query($strSQL);
		while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
			$this->_arrObjects[$arrRow["intRoleID"]] = new role();
			$this->_arrObjects[$arrRow["intRoleID"]]->setVarsFromRow($arrRow);
		}
	}
}
?>
