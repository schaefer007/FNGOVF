<?php
# Include the MIME Type Class
include "class.mime.php";
require_once "Mail.php";
require_once "Mail/mime.php";

class email {
	
	protected $recipient;
	
	protected $sender;
	
	protected $cc;
	
	protected $bcc;
	
	protected $message;
	
	protected $subject;
	
	protected $type;
	
	protected $replyto;
	
	protected $attachments;
	
	protected $attachmentName;
	
	function __construct() {
		$this->recipient = null;
		$this->sender = null;
		$this->cc = null;
		$this->bcc = null;
		$this->subject = null;
		$this->message = null;
		$this->type = null;
		$this->replyto = null;
		$this->attachments = null;

	}
	function __destruct() {
		if (isset($mime)) {         
		  unset($mime);        
		}
	}	
	
	function addRecipient($recipient) {
		if (base::check_email_address($recipient)) {
		if ($this->recipient != null) {
				$this->recipient.= ", ".$recipient;
			}	else {
				$this->recipient = $recipient;
			}
		return true;
	} else {
		return false;
	}
	}
	
	function resetRecipient($recipient=null) {
		$this->recipient = $recipient;
	}

		function addCC($CC) {
		if (base::check_email_address($CC)) {
		if ($this->cc != null) {
				$this->cc.= ", ".$CC;
			}	else {
				$this->cc = $CC;
			}
		return true;
	} else {
		return false;
	}
	}
	
	function resetCC() {
		$this->cc = null;
	}
	
		function addBCC($BCC) {
		if (base::check_email_address($BCC)) {
		if ($this->bcc != null) {
				$this->bcc.= ", ".$BCC;
			}	else {
				$this->bcc = $BCC;
			}
		return true;
	} else {
		return false;
	}
	}
	
	function resetBCC() {
		$this->bcc = null;
	}
	
	function addAttachment($Attachment, $Name) {
		if ($Attachment != null and file_exists($Attachment)) {
				$this->attachments[] = $Attachment;
		if ($Name != null) {
			$this->attachmentName[$Attachment] = $Name;
		}
				return true;
			} else {
		return false;
	}	
	}
	
	function setType($type) {
		if (in_array(strtoupper($type), array('HTML', 'TEXT'))) {
			$this->type = strtoupper($type);
			return true;
		} else {
			return false;
		}
	}
	
	function addSubject($subject) {
		if (isset($subject) and $subject != null) {
		if ($this->subject == null) {
			$this->subject = $subject;
		} else {
			$this->subject .= $subject;
		}
		return true;
	} else {
		return false;
	}
	}

	function resetSubject($subject) {
		if (isset($subject) and $subject != null) {
			$this->subject = $subject;
		}
		return true;
	}
	
	function addSender($sender) {
		if ($this->sender != null) {
			return false;
		} else {
		if (base::check_email_address($sender)) {
			$this->sender = $sender;
			return true;
		} else {
			return false;
		}
	}
	}
	
		function addReplyTo($replyto) {
		if ($this->replyto != null) {
			return false;
		} else {
		if (base::check_email_address($replyto)) {
			$this->replyto = $replyto;
			return true;
		} else {
			return false;
		}
	}
	}
	
	function addMessage($message) {
		if (isset($message) and $message != null) {
		if ($this->message == null) {
			$this->message = $message;
		} else {
			$this->message .= $message;
		}
		return true;
	} else {
		return false;
	}
	}	 
	
	function resetMessage($message) {
		if (isset($message) and $message != null) {
			$this->message = $message;
		}
		return true;
	} 
		
	function send() {
		global $config;
		global $errorText;
		$file = null;
		$data = null;
		$crlf = "\n";
		error_log('Executing Mail Send . . .');
	# Define the MIME Type class object
	    if (!isset($mime)) {
    	$mime = new MIMETypes("mime.types.php");
	    }
		// ini_set("SMTP", "172.29.3.102");
		if ($this->sender == null and array_key_exists('from',$config['mail'])) {
		    	$this->sender = $config['mail']['from'];		    
		}
		if ($this->sender == null or ($this->recipient == null and $this->cc == null and $this->bcc == null) or $this->subject == null or $this->message == null) {
		  	return false;
		  } else {
     		$mime = new Mail_mime($crlf);
     		
		   		if ($this->type == null) {
		  			$this->type = 'TEXT';
		  		}
		    	if ($this->type == 'HTML') {
		    		$mime->setHTMLBody($this->message);
		    	} else {
		    		$mime->setTXTBody($this->message);
		    	}

		    if (sizeof($this->attachments) > 0) {
		    	$mimeTypes = new MIMETypes("mime.types.php");
		    	foreach ($this->attachments as $AttFile) {
		    		if ($this->attachmentName[$AttFile] != null) {
		    			$mimeTypeValue = $mimeTypes->getMimeType($AttFile);
		    			$mime->addAttachment($this->attachmentName[$AttFile], $mimeTypeValue);
		    		} else {
		    			$mime->addAttachment($AttFile, 'text/plain');
		    		}
		    	}		    	      
	     	}
		  
		    if ($this->sender == null and isset($config['mail']['from'])) {
		    	$this->sender = $config['mail']['from'];		    }
		    
	    	$headers['From'] = $this->sender;
	    	$headers['To'] = $this->recipient;
	    	$headers['Subject'] = $this->subject;
			$headers['Date'] = date('r', time());

		    if ($this->replyto != null) {
		    	$headers['Reply-To'] = $this->replyto;
		    }
		    if ($this->cc != null) {
		    	$headers['Cc'] = $this->cc;
		     } 
		    if ($this->bcc != null) {
		    	$headers['Bcc'] = $this->bcc;
		    }
        $Mime_Message = $mime->get();
        $headers = $mime->headers($headers);
		$smtp = Mail::factory('smtp', $config['mail']);

		$emailObj = $smtp->send($this->recipient, $headers, $Mime_Message);
		
		error_log('Email Processing Completed . . .');
		if (PEAR::isError($emailObj)) {
			$errorText[] = $emailObj->getMessage();
			return false;
		} else {
  			return true;
 		}
	}
}
}

?> 