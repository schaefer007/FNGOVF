<?php
/* base class for common functions */
include_once 'classes/class.base.php';
/**
 * radioButton class
 *
 * This class generates a set of radio buttons when given an array of option/value pairs,
 *  a group name and optionally the value currently selected and generating options.
 *  At this time the only generating option allowed is "DISPLAY" with a value of "VERTICAL"
 *  which causes the list of radio buttons to be generated one over the other.  The
 *  default behaviour is to generate them in-line.
 *
 * @author George L. Slater (Arbor Solutions, Inc)
 * @copyright 2009 - Arbor Solutions, Inc
 * @license GNU GPL
 * @version 1.0
 * @package Utility
 *
 */
class radioButton {
/**
 * Name of radiobutton group to generate
 *
 * @var string
 */
 protected $name;

 /**
  * Array of Option => Value pairs generated elsewhere
  *
  * @var array
  */
 protected  $ddArray;

 // Instantiate a radioButton object, establish the name, data.

 /**
  * Constructor for radioButton class.
  *
  * Assign radioButton Group name, data.
  *
  * @param string $name
  * @param array $data
  * @return boolean
  */
 function __construct($name, $data) {
   if (isset($name)) {
     $this->name = $name;
   } else {
   // generate an error here.
   }
   if (isset($data)) {
     $this->ddArray = $data;
   } else {
   // generate an error here.
   }

   return true;
}

 // Handle cleanup of any objects created by this class.
/**
 * Destructor
 *
 * Perform cleanup of any objects created by this class.
 */
 function __destruct() {
 }


 // Build a set of radio buttons and return output the results.
 /**
  * Build a set of radio buttons
  *
  * Build a set of radio buttons and output the results. Output is generated directly
  * to standard out.
  *
  * @param string $selectedValue
  * @param boolean $required
  * @param array $options
  */
 function bldRadioButtons($selectedValue, $required=true, $options=array(),$AsAssoc=false) {

	$optionHTML = '';
 	if ($AsAssoc == true or base::is_assoc($this->ddArray)) {
     foreach ($this->ddArray as $Value => $Option) {
       if (trim($selectedValue) == trim($Value)) {
         $selected = " checked";
       } else {
         $selected = "";
       }
       if (isset($options) and array_key_exists('JS', $options)) {
       	$optionJS = $options['JS'];
       } else {
       	$optionJS = '';
       }
       $optionHTML .= "<input type=\"radio\" name=\"$this->name\" value=\"".htmlspecialchars(trim($Value))."\"".$selected." ".$optionJS.">".htmlspecialchars(trim($Option));
	   if (isset($options['DISPLAY']) and $options['DISPLAY'] == 'VERTICAL') {
	   	$optionHTML.="<br>\n";
	   } else {
	   	$optionHTML.="&nbsp;&nbsp;";
	   }
     }
   } else {
     foreach ($this->ddArray as $Option) {
     if (trim($selectedValue) == trim($Option)) {
     	$selected = " checked";
     } else {
     	$selected = "";
     }
     $optionHTML .= "<input type=\"radio\" name=\"$this->name\" value=\"".htmlspecialchars(trim($Option))."\"".$selected.">".htmlspecialchars(trim($Option));
     if (isset($options['DISPLAY']) == 'VERTICAL') {
     	$optionHTML.="<br>\n";
     } else {
     	$optionHTML.="&nbsp;&nbsp;";
     }
     }
   }
   return $optionHTML;
 }
}
?>