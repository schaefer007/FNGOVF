<?php
    require_once('classes/class.tbluserrolexr.php');
    require_once('classes/class.tblrole.php');
    require_once('classes/class.tblrolepermission.php');
    require_once('classes/class.tblsecurityitem.php');
    require_once('classes/class.tblpermission.php');
	/**
	 * Contains miscellaneouse and utility functions commonly used.
	 * 
	 * @author George L. Slater (Arbor Solutions, Inc)
	 * @copyright 2007 - Arbor Solutions, Inc
	 * @license GNU GPL
	 * @version 1.0
	 * @package Utility
	 */
	/**
	 * Base Class used for utility and miscellaneous functions.
	 * 
	 * This class includes static methods used to perform utility functions.
	 *
	 * @author George L. Slater (Arbor Solutions, Inc)
	 * @copyright 2007 - Arbor Solutions, Inc
	 * @license GNU GPL
	 * @version 1.0
	 * @package Utility
	 */
	class base {
		
	private $formsAvailable = array();
	
	/**
	 * Format a phone number for display
	 * 
	 * Determine if the phone number contains 10 digits or 7 digits and then format it as
	 * either (xxx) xxx-xxxx or xxx-xxxx.  If a phone number does not contain 7 or 10 digits
	 * it is returned with no formatting.
	 *
	 * @param string $Phone_Number
	 * @return string
	 */
	static function Format_Phone ($Phone_Number) {
		$Phone_Number = str_replace(array("-"," ","(",")"),array("","","",""),$Phone_Number);
		switch (strlen($Phone_Number)) {

		case 10: $Editted_Phone_Number = "(".substr($Phone_Number,0,3).") ".substr($Phone_Number,3,3)."-".substr($Phone_Number,6,4);
			break;

		case 7: $Editted_Phone_Number = substr($Phone_Number,0,3)."-".substr($Phone_Number,3,4);
			break;
			
		default: $Editted_Phone_Number = $Phone_Number;
			break;
	}
	if ($Editted_Phone_Number == "0") {
		$Editted_Phone_Number = " ";
	}
	return $Editted_Phone_Number;
	}
	
	/**
	 * Handle Exceptions
	 * 
	 * This is a generic Exception handler called when an exception is encountered.  This method
	 * is intended to create a single process for handling exceptions.
	 *
	 * @param exception $Error
	 * @return string
	 */
    static function Error_Handler ($Error) {
		base::write_log($Error->getMessage(),"E",$Error->getFile(),$Error->getLine());
 	error_log($Error->getMessage()." occurred in file ".$Error->getFile()." at line ".$Error->getLine(), 0);
    return "<font color=red><b>An Error occurred while processing file ".$Error->getFile()."</b></font><br>";
    }
    
/**
 * Check an email address for correct formatting. 
 *
 * @param string $email
 * @return boolean
 */
static function check_email_address($email) {  
  $isValid = true;
   $atIndex = strrpos($email, "@");
   if (is_bool($atIndex) && !$atIndex)
   {
      $isValid = false;
   }
   else
   {
      $domain = substr($email, $atIndex+1);
      $local = substr($email, 0, $atIndex);
      $localLen = strlen($local);
      $domainLen = strlen($domain);
      if ($localLen < 1 || $localLen > 64)
      {
         // local part length exceeded
         $isValid = false;
      }
      else if ($domainLen < 1 || $domainLen > 255)
      {
         // domain part length exceeded
         $isValid = false;
      }
      else if ($local[0] == '.' || $local[$localLen-1] == '.')
      {
         // local part starts or ends with '.'
         $isValid = false;
      }
      else if (preg_match('/\\.\\./', $local))
      {
         // local part has two consecutive dots
         $isValid = false;
      }
      else if (!preg_match('/^[A-Za-z0-9\\-\\.]+$/', $domain))
      {
         // character not valid in domain part
         $isValid = false;
      }
      else if (preg_match('/\\.\\./', $domain))
      {
         // domain part has two consecutive dots
         $isValid = false;
      }
      else if
(!preg_match('/^(\\\\.|[A-Za-z0-9!#%&`_=\\/$\'*+?^{}|~.-])+$/',
                 str_replace("\\\\","",$local)))
      {
         // character not valid in local part unless 
         // local part is quoted
         if (!preg_match('/^"(\\\\"|[^"])+"$/',
             str_replace("\\\\","",$local)))
         {
            $isValid = false;
         }
      }
     // if ($isValid && !(checkdnsrr($domain,"MX") || checkdnsrr($domain,"A")))
     // {
         // domain not found in DNS
     //    $isValid = false;
     // }
   }
   return $isValid;
	
}
/**
 * Check an array to see if it is an associative array.
 * 
 *  If the array has keys associated with it then this is an associative array and this
 *  function will return true otherwise it will return false.
 *
 * @param string $email
 * @return boolean
 */
static function is_assoc($_array) {
    if ( !is_array($_array) || empty($_array) ) {
        return -1;
    }
    foreach (array_keys($_array) as $k => $v) {
        if ($k !== $v) {
            return true;
        }
    }
    return false;
}
	/**
 * Issue a page not found message and set the haltProcessing flag.
 * 
 * @param string $pageNotFound
 * @return boolean
 */
static function missingPage($pageNotFound) {
	global $haltProcessing;
	global $module;
	echo "<h2 style=\"text-align:center;\">Page $pageNotFound not found.  Please contact support.</h2>";
	$haltProcessing = true;
}
	/**
 * Get Elapsed Days
 * 
 * Given two strings representing dates in YYYY/MM/DD format, determine the number of days elapsed between the two dates.
 *
 * @param string $Date1
 * @param string $Date2
 * @return int
 */
	static function getDaysDiff($Date1,$Date2) {
		$Y1 = substr($Date1,0,4);
		$M1 = substr($Date1,5,2);
		$D1 = substr($Date1,8,2);
		$Y2 = substr($Date2,0,4);
		$M2 = substr($Date2,5,2);
		$D2 = substr($Date2,8,2);
		$Time1 = mktime(0,0,0,$M1,$D1,$Y1);
		$Time2 = mktime(0,0,0,$M2,$D2,$Y2);
		if ($Time1 > $Time2) {
			return ($Time1 - $Time2)/86400;
		} else {
			return ($Time2 - $Time1)/86400;
		}
	}
/**
 * Convert plain text password to actual encrypted password.
 * 
 * @param string $password
 * @return string 
 */
  static function hashPassword($password) {
	$passwordOrig = md5($password);
	$rotate = hexdec(substr($passwordOrig,0,1));
	$r_Idx = hexdec(substr($passwordOrig,$rotate * 2,1));
	$NewPassword = md5(substr($passwordOrig,$r_Idx * 2).substr($passwordOrig,0,$r_Idx * 2));
	return $NewPassword;
  }
  

  static function isFormAuthorized($intUser=null, $formName=null) {
  	global $availableForms;
  	global $allFormsAvailable;
  	$formsNo = count($allFormsAvailable);
  	if ($formsNo == 0 and $intUser != null and $formName != null) {
	  	$userRolesDb = new tbluserrolexr();
	  	$userRolesDb->select(null,array('WHERE'=>array('intUserID'=>$intUser)));
  		while($userRolesRow = $userRolesDb->getnext()) {
  		  $userRoles[$userRolesRow['intRoleID']] = array($userRolesRow['intRoleID']);
  		}
  		if (isset($userRolesDb)) {
  			unset($userRolesDb);
  		}
  		$roleDb = new tblrole();
  		foreach ($userRoles as $userRole=>$userRoleArray) {
  			$roleDb->select(null,array('WHERE'=>array('intRoleID'=>$userRole)));
  			while($roleRow = $roleDb->getnext()) {
  				$userRoles[$userRole] = array_merge($userRoleArray,$roleRow);
  			}
  		}
  		if (isset($roleDb)) {
  			unset($roleDb);
  		}
  		$rolePermissions = new tblrolepermission();
  		foreach ($userRoles as $userRole=>$userRoleArray) {
            if ($userRoleArray['strStatus'] == 1) {
  				$rolePermissions->select(null,array('WHERE'=>array('intRoleID'=>$userRole)));
  				while($rolePermissionRow = $rolePermissions->getnext()) {
  					if ($rolePermissionRow['intPermissionID'] != null and $rolePermissionRow['intPermissionID'] != '') {
  						$userRoles[$userRole]['intPermissionID'][$rolePermissionRow['intPermissionID']] = $rolePermissionRow['intPermissionID'];
  					}
  				}
  			}
  		}
  		if (isset($rolePermissions)) {
  			unset($rolePermissions);
  		}
  		$securityItems = array();
  		$permission = new tblpermission();
  		foreach ($userRoles as $userRole=>$userRoleArray) {
  			foreach ($userRoleArray['intPermissionID'] as $permissionID=>$permissionarray) {
  				$permission->select(null,array('WHERE'=>array('intPermissionID'=>$permissionID)));
  				while ($permissionRow = $permission->getnext()) {
  					if (!in_array($permissionRow['intSecurityItemID'],$securityItems)) {
  						$securityItems[] = $permissionRow['intSecurityItemID'];
  					}
  				}
  			}
  		}
  		$forms = new tblsecurityitem();
  		foreach ($securityItems as $securityID) {
  			$forms->select(null,array('WHERE'=>array('intSecurityItemID'=>$securityID)));
  			while ($formRow = $forms->getnext()) {
  				if (array_key_exists($formRow['strSecurityItem'],$availableForms)) {
  					$allFormsAvailable[] = $formRow['strSecurityItem'];
  				}
  			}
  		}
  		if (isset($forms)) {
  			unset($forms);
  		}
  	}

  	if ($formName != null and in_array($formName,$allFormsAvailable)) {
  		return true;
  	} else {
  		return false;
  	}
  }
 static function file_exists_inPath($filename) {
  	if (function_exists("get_include_path")) {
  		$include_path = get_include_path();
  	} elseif(false !== ($ip = ini_get("include_path"))) {
  		$include_path = $ip;
  	} else {
  		return false;
  	}
  
  	if (false !== strpos($include_path, PATH_SEPARATOR)) {
  		if (false !== ($temp = explode(PATH_SEPARATOR, $include_path)) and count($temp) > 0) {
  			for($n=0;$n<count($temp);$n++) {
  				if (false !== @file_exists($temp[$n].$filename)) {
  					return true;
  				}
  			}
  			return false;
  		} else {
  			return false;
  		}
  	} elseif(!empty($include_path)) {
  		if (false !== @file_exists($include_path)) {
  			return true;
  		} else {
  			return false;
  		}
  	} else {
  		return false;
  	}
  }
		static function getNextRole($processInstance) {
			$roleArray = $processInstance->getRoleArray()->getArray();
			foreach ($roleArray as $Role) {
				$UserArray = $Role->getUserArray()->getArray();
				foreach ($UserArray as $User) {
					if ($User->getTooling()) {
						return "Tooling";
					} else if ($User->getCommodity()) {
						return "CCC";
					}
				}
			}
			return "BI Team";
		}
		static function getNextRoleID($processInstance) {
			$roleArray = $processInstance->getRoleArray()->getArray();
			foreach ($roleArray as $Role) {
				$UserArray = $Role->getUserArray()->getArray();
				foreach ($UserArray as $User) {
					if ($User->getTooling() || $User->getCommodity()) {
						return 2;
					}
				}
			}
			return 3;
		}
		static function getNextAction($processInstance) {
			$roleArray = $processInstance->getRoleArray()->getArray();
			$targetRole = base::getNextRole($processInstance);
			foreach ($roleArray as $Role) {
				$ProcessTransitionArray = $Role->getProcessTransitionArray()->getArray();
				switch($targetRole) {
					case "CCC":
					case "Tooling":
						if($Role->getRoleName() == "Director") {
							foreach($ProcessTransitionArray as $ProcessTransition) {
								return $ProcessTransition->getTransitionButton();
							}
						}
						break;
					case "BI Team":
						if($Role->getRoleName() == "BI Team") {
							foreach($ProcessTransitionArray as $ProcessTransition) {
								return $ProcessTransition->getTransitionButton();
							}
						}
						break;
					default:
						break;
				}
			}
			return "Submit";
		}
		public static function write_log($msg=null,$type=null,$file=null,$line=null) {
			global $config,$formRow;
			$error = error_get_last();
			if( $error !== NULL && $error['type'] == 1 && $msg == null && $type == null) {
//            echo "<pre>".print_r($error)."</pre>";
				$caller['file'] = substr($error['file'],strlen($config[applicationRoot]));
				$caller['line'] = $error['line'];
				$message['message'] = $error['message'];
				$message['type'] = 'E'; //Log::get_error_type($error['type']);
			} else {
				$bt = debug_backtrace();
				$caller = array_shift($bt);
				$caller['file'] = substr($caller['file'],strlen($config[applicationRoot]));
				$message['message'] = $msg;
				$message['type'] = $type;
			}
			// Make sure that the script ended successfully and return
			// or else we have a log entry to write.
			if($caller['file'] == '') {
				return 0;
			}
			if($file != null) {
				$caller['file'] = $file;
			}
			if($line != null) {
				$caller['line'] = $line;
			}

			$session_id = session_id();
			$session_instance = $_SERVER['REQUEST_TIME'];
			$session_user = $_SESSION['intUser'];
			$current_time = date('Y-m-d-H.i.s.000000', time());//DateTime('NOW');
//			echo "File: {$caller['file']}<br>";
//			echo "Line Number: {$caller['line']}<br>";
//			echo "Session ID: $session_id<br>";
//			echo "Session Instance: $session_instance<br>";
//			echo "User ID: $session_user<br>";
//			echo "Current Timestamp: $current_time<br>";
//			echo "Form ID: $formID<br>";
//			echo "Instance ID: {$_REQUEST['instanceID']}<br>";
//			echo "Message: {$message['message']}<br>";
//			echo "Type: {$message['type']}<br>";
//			echo $view;
			$formID = $formRow['formID'];
			if($formID == "") {
				$formID = $_REQUEST['VFID'];
			}
			$instanceID = $_REQUEST['instanceID'];
			if($instanceID == "") {
				$instanceID = $_POST["intProcessInstanceID"];
			}
//			if($formID = "" and $instanceID != "") {
//				$formrequest = new formrequest();
//				$formrequest->loadForWorkflow($instanceID);
//				$formID = $formrequest->getFormID();
//			}
			$_REQUEST['LogSessionID'] = $session_id;
			$_REQUEST['LogSessionInstance'] = $session_instance;
			$_REQUEST['LogUserID'] = $session_user;
			$_REQUEST['logTimestamp'] = $current_time;
			$_REQUEST['LogFormID'] = $formID;
			$_REQUEST['LogProcessingInstanceID'] = $instanceID;
			$_REQUEST['LogMessage'] = $message['message'];
			$_REQUEST['LogMessageType'] = $message['type'];
			$_REQUEST['LogScriptName'] = $caller['file'];
			$_REQUEST['LogScriptLine'] = $caller['line'];
			// do your logging stuff here.
			$Log = new Log();
			$Log->insert();
//        $db = new db();
//        $db->connect();
//        $db->execute("INSERT INTO \"dbsystem\".\"tblalert\" (\"formID\",\"alertNumber\",\"subject\",\"body\",\"dtmSent\",\"stateID\") VALUES (2225,1,'Form Delayed - Form: 1112, ABC COMPANY TEST:Because','Hello','2015-09-21-10.53.23.000000',2)");
//        $db->close();

		}
}
?>