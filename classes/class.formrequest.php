<?php
require_once('class.table.php');
include_once("ProcessObject.class.php");
include_once('ProcessState.class.php');
include_once('ProcessTransition.class.php');
include_once('classes/class.mail.php');
include_once('classes/class.account.php');
include_once('classes/class.tbluser.php');
include_once('User.class.php');
include_once('../plm/classes/Alert.class.php');
include_once('../plm/classes/StateInstance.class.php');
/**
 * import table Definitions.
 *
 * The import table contains defintions of each RETS import that will be run
 * The import class extends the standard table class.
 *
 * @author George L. Slater (Arbor Solutions, Inc)
 * @copyright 2011 - GRAR and Arbor Solutions, Inc
 * @version 1.0
 * @package Db
 * @see class.table.php
 */
class formrequest extends table {

	private $_objProcessInstance;

	function __construct() {
		global $config;
		// fieldMetaData defines special editting hand handling above and beyond what can be determined
		//  simply based on the field attributes.
		/**
		 * Fields Metadata
		 *
		 * This data identifies special field handling for edits and form display.
		 *
		 *  Valid Edit types are:
		 *
		 *  ALPHA	=	all characters are allowed but embedded single quotes are doubled
		 *  			during the database update.  Mixed case is allowed.
		 *  ALPHA_UC=	all characters are allowed but alphabetic characters will be
		 *  			converted to Upper case during database operations.
		 *  EMAIL   =   only valid email addresses are allowed.  Email addresses are checked
		 *              for validity by format only.  No attempt it made to confirm that the
		 *              email address corresponds to an actual email account.
		 *  FROMTABLE=	This is an associative array.  When FROMTABLE is specified, VALCOLUMN
		 *              must also be specified (SELCOLUMN is optional as is the WHERE Key).
		 *              FROMTABLE identifies a separate table from which values are derived as
		 *              distinct values.  VALCOLUMN (and optionally SELCOLUMN) keys identify
		 *              the columns used to generate an associative array (and subsequently
		 *              a dropdown for selection) of values.  Using a "WHERE" subclause the
		 *              data returned can be filtered to a select set of data based on input
		 *              values.
		 *  INT		=	only digits are allowed.
		 *  JS		=   Defines Javascript to be associated with the field when displayed in
		 *              an input form.
		 *  NUM		=	only digits and a decimal point are allowed.
		 *  VALUES	=	if the edit type is an array, then the value provided must be one of
		 *  			the values stored in the array.  Input generates a dropdown list for
		 *  			selection.
		 *  YN		=	identifies a Yes/No field that behaves like an array edit type except
		 *  			that the values are predefined to only allow "Y" and "N".
		 *  UNIQUE  =   identifies this field as unique in the table based on a set of values.
		 *  UNIQUEBY=	identifies a set of fields in which the field being defined must be
		 *              unique.
		 *  PHONE   =   inserts phone number masking during input.
		 **/
		$this->thisIsQuoted = true;
		$this->schema = '"dbsystem"';
		$this->fieldMetaData = array(
			"facilityName"=>array("JS"=>'onchange="openWait(\'Please wait while your form is being generated . . .\'); this.form.submit();"'),
			"dbName"=>array("FROMTABLE"=>'server',"VALCOLUMN"=>'DBNAME'),
		);
		parent::__construct();
		$this->optionalField('set');
	}

	function reset () {
		$this->__construct();
	}

	function set ($var, $value) {
		if (isset($var) and isset($value)) {
			$this->$var = $value;
		}
	}

//function getFormID() {
//	return $this->_formID;
//}
	function getProcessInstanceID() {
		return $this->_processingInstanceID;
	}
	function setProcessInstanceID($value) {
		if($this->_processingInstanceID !== $value) {
			$this->_processingInstanceID = $value;
			$this->_blnDirty = true;
		}
	}
	function getProcessInstance($blnLoad=false){
		if(!$this->_objProcessInstance) {
			$this->_objProcessInstance = new ProcessInstance();
		}
		if($blnLoad && $this->getProcessInstanceID() && !$this->_objProcessInstance->getProcessInstanceID()) {
			$this->_objProcessInstance->load($this->getProcessInstanceID());
		}
		return $this->_objProcessInstance;
	}

	public function loadForWorkflow($processInstanceID) {
		$formDb = new formrequest();
		$formDb->select(null,array('WHERE'=>array('processingInstanceID'=>$processInstanceID)));
		if ($formRow = $formDb->getnext()) {
			$this->set('formType', $formRow['formType']);
			$this->set('DbName', $formRow['dbName']);
			$this->set('Plant', $formRow['plant']);
			$this->set('Company', $formRow['company']);
			$this->set('formID', $formRow['formID']);
			$this->set('dbSchema', $formRow['dbSchema']);
			$this->set('requestText', $formRow['requestText']);
			$this->set('_processingInstanceID', $processInstanceID);
			$this->set('reqID', $formRow['reqID']);
//		$this->set('_formID', $formRow['formID']);
		}
		if (isset($formDb)) {
			unset($formDb);
		}
	}
	public function getID() {
		return $this->getProcessInstanceID();
	}

	public function getBody($objProcessInstance, $objProcessTransitionInstance) {
		$this->loadForWorkFlow($objProcessInstance->getProcessInstanceID());
		include_once('classes/class.'.trim($this->formType).'.php');
		$formHandler = new $this->formType();
		return $formHandler->getBody($objProcessInstance, $objProcessTransitionInstance, $this->requestText);
	}

	public function getProcessObjectClass() {
		return get_class($this);
	}

	function setVarsFromRow($arrRow) {
		if(isset($arrRow["formID"])) $this->_formID = $arrRow["formID"];
		if(isset($arrRow["reqID"])) $this->_reqID = $arrRow["reqID"];
		if(isset($arrRow["facilityName"])) $this->_facilityName = $arrRow["facilityName"];
		if(isset($arrRow["dbName"])) $this->_dbName = $arrRow["dbName"];
		if(isset($arrRow["dbSchema"])) $this->_dbSchema = $arrRow["dbSchema"];
		if(isset($arrRow["plant"])) $this->_plant = $arrRow["plant"];
		if(isset($arrRow["company"])) $this->_company = $arrRow["company"];
		if(isset($arrRow["reqTimeStamp"])) $this->_reqTimeStamp = $arrRow["reqTimeStamp"];
		if(isset($arrRow["status"])) $this->_status = $arrRow["status"];
		if(isset($arrRow["stsTimeStamp"])) $this->_stsTimeStamp = $arrRow["stsTimeStamp"];
		if(isset($arrRow["stsID"])) $this->_stsID = $arrRow["stsID"];
		if(isset($arrRow["formType"])) $this->_formType = $arrRow["formType"];
		if(isset($arrRow["processingInstanceID"])) $this->_processingInstanceID = $arrRow["processingInstanceID"];
		if(isset($arrRow['requestText'])) $this->_requestText = $arrRow['requestText'];
		if(isset($arrRow['intNextRoleID'])) $this->_intNextRoleID = $arrRow['intNextRoleID'];
		if(isset($arrRow['strNextRole'])) $this->_strNextRole = $arrRow['strNextRole'];
		if(isset($arrRow['strNextAction'])) $this->_strNextAction = $arrRow['strNextAction'];
	}

	public function setProcessVarsFromRow($arrRow) {
		$this->setVarsFromRow($arrRow);
	}

	public function getProcessName() {
		//return $this->getProgram()->getProgramName() . ", EC " . $this->getECNumber();
		return 'Form: '.$this->_processingInstanceID.', '.$this->requestText;
	}

	function getURL() {
		return strSITE_URL."index.php?view=editForm&instanceID=".$this->getProcessInstanceID();
	}

	public function getUserArrayFromRoleIDs($arrRoleIDs, $objProcessInstance, $objProcessTransitionInstance) {
		if($objProcessTransitionInstance->getProcessTransition()->getToProcessStateID() == vf_InitialState) {
			if (!class_exists('ProcessTransitionInstance')) {
				include_once('ProcessTransitionInstance.Class.php');
			}
			$this->loadForWorkflow($objProcessInstance->getProcessInstanceID());
			$objUserArray = new UserArray();
			$objUserArray->loadByUserID($this->reqID);
			//$objProcessTransitionInstance = new ProcessTransitionInstance();
			//$objProcessTransitionInstance->loadLastByProcessInstanceIDAndProcessTransitionID($objProcessInstance->getProcessInstanceID(),vf_InitialTransitionId);
			//$objUserArray = $objProcessTransitionInstance->getConditionInstanceArray()->getUserArrayByProcessTransitionInstanceID($objProcessTransitionInstance->getProcessTransitionInstanceID());
			return $objUserArray;
		} else {
			error_log('getting custom conditions in getUserArrayFromRoleIDs');
			if(!$objProcessTransitionInstance->getProcessTransition()->getToProcessState(true)->getCustomConditions()) {
				$objUserArray = new formUserArray();
				$objUserArray->loadByFormTypeAndFacility($this->formType, $this->dbSchema, $this->Plant, $this->Company, $arrRoleIDs);
				return $objUserArray;
			} else {
				// get users based on custom conditions
				$this->loadForWorkflow($objProcessInstance->getProcessInstanceID());
				if (!class_exists($this->formType)) {
					include_once("classes/class.".$this->formType.".php");
				}
				error_log('this formtype is '.$this->formType);
				$thisForm = new $this->formType();
				$arrUsers = $thisForm->getCustomConditionUsers($this->formID);
				$objUserArray = new formUserArray();
				$objUserArray->loadByFormTypeAndUserList($this->formType, $arrUsers);
				return $objUserArray;
			}
		}
	}
	public function getRoleAndUserArrayFromRoleIDs($arrRoleIDs) {
		//include_once("classes/Role.class.php");
		//$objRoleArray = new RoleArray();
		//$objRoleArray->loadByProgramID($this->getProgramCommon(true)->getTopLevelProductID(), $arrRoleIDs);
		//return $objRoleArray;
		$objRoleArray = new formRoleArray();
		$objRoleArray->loadByFormTypeAndFacility($this->formType, $this->dbSchema, $this->Plant, $this->Company, $arrRoleIDs);
		return $objRoleArray;
	}
	function getProgramCommonID() {
		return true;
	}
	public function validateWorkflowAction($intProcessTransitionID, $intRoleID) {
		try {
			self::loadForWorkflow($this->processingInstanceID);
		} catch (Exception $e) {
			error_log('validateWorkflowAction - loadForWorkflow function did not execute correctly');
		}
		error_log(' -- validation connection information');
		$GLOBALS['thisFormdbName'] = $this->DbName;
		$GLOBALS['thisFormdbSchema'] = $this->dbSchema;
		error_log('validation DbName is '.$this->DbName);
		error_log('validation _dbName is '.$this->_dbName);
		error_log('validation _dbSchema is '.$this->_dbSchema);
		error_log('validation dbSchema is '.$this->dbSchema);
		error_log(' -- End of validation connection information');
		include_once('classes/class.'.$this->formType.'.php');
		$thisForm = new $this->formType();
		return $thisForm->validateWorkflowAction($intProcessTransitionID, $intRoleID, $this->_processingInstanceID, $this->formID);
	}
	public function resendNotices() {
		$objProcessInstance = new processInstance($this->getProcessInstanceID());
		$objProcessTransitionInstance = new ProcessTransitionInstance();
		$objProcessTransitionInstance->loadLastByProcessInstanceID($this->getProcessInstanceID());
		$intProcessTransitionID = $objProcessTransitionInstance->getProcessTransitionID();
		$objProcessTransitionInstance->getProcessTransition()->load($intProcessTransitionID);
		if($objProcessTransitionInstance->getProcessTransition()->getSendEmail()) {
			$strProcessObject = $this->getProcessObjectClass();
			$objProcessObject = new $strProcessObject();
			$objProcessTransitionInstance->getProcessTransition()->getToProcessState(true)->emailForLastProcessTransitions($objProcessObject, $objProcessInstance, $objProcessTransitionInstance);
		}
	}
	public function afterProcessTransition($objProcessInstance, $objProcessTransitionInstance) {
		global $errorText;
		global $config;
		//	switch($objProcessInstance->getCurrentProcessStateID()) {
		//	case intPROCESS_STATE_ID_EC_APPROVED:
		//		$this->approveEC();
		//		break;
		//	case intPROCESS_STATE_ID_EC_REJECTED:
		//		$this->rejectEC();
		//		break;
		//}
		$objProcessInstance->loadForDisplay($objProcessInstance->getProcessInstanceID(),$this);

		$currentStateId = $objProcessInstance->getCurrentProcessStateID();
		$objProcessInstance->getCurrentProcessState()->load($objProcessInstance->getCurrentProcessStateID());
		$currentState = $objProcessInstance->getCurrentProcessState()->getProcessStateName();
		if ($objProcessInstance->getRoleArray()->getArray()) {
			if ($currentStateId == vf_InitialState) {
				$_REQUEST['status'] = 'Re-Submit';
			} else {
				$_REQUEST['status'] = 'Active';
			}
			$finalState = false;
		} else {
			$_REQUEST['status'] = $currentState;
			// $finalState = true;
			$finalState = $objProcessInstance->getCurrentProcessState()->getFinalState();
		}
		$_REQUEST['modified'] = 'N';
		$_REQUEST['processingInstanceID'] = $objProcessInstance->getProcessInstanceID();
		$_REQUEST['stsTimeStamp'] = date('Y-m-d-H.i.s').'.000000';
		$_REQUEST['stsID'] = $_SESSION['intUser'];
		// set request variables for intNextRoleID and strNextRole and strNextAction
		$_REQUEST['intNextRoleID'] = base::getNextRoleID($objProcessInstance);
		$_REQUEST['strNextRole'] = base::getNextRole($objProcessInstance);
		$_REQUEST['strNextAction'] = base::getNextAction($objProcessInstance);
		error_log('intnextroleid: ' . $_REQUEST['intNextRoleID']);
		error_log('strnextrole: ' . $_REQUEST['strNextRole']);
		error_log('strnextaction: ' . $_REQUEST['strNextAction']);
		if (!$this->update()) {
			foreach($errorText as $errorMsg) {
				error_log($errorMsg);
			}
		} else {
// TODO: update and create StateInstance table with processInstanceID and StateID
			// Update previous StateInstance if it exists.
			$StateInstance = new StateInstance();
			if($StateInstance->loadByProcessInstanceID($objProcessInstance->getProcessInstanceID())) {
				$StateInstance->complete();
				$StateInstance->save();
			}
			unset($StateInstance);
			// Insert new StateInstance
			$StateInstance = new StateInstance();
			$StateInstance->setProcessInstanceID($objProcessInstance->getProcessInstanceID());
			$StateInstance->setStateID($objProcessInstance->getCurrentProcessStateID());
			$StateInstance->save();

			$Alert = new Alert($this->formID);
			$Alert->setCompleteStatus();
			if ($finalState == true) {
				try {
					// TODO: post completed for stateinstance
					self::loadForWorkflow($objProcessInstance->getProcessInstanceID());
					$StateInstance = new StateInstance();
					$StateInstance->loadByProcessInstanceID($objProcessInstance->getProcessInstanceID());
					$StateInstance->setStatus("complete");
					$StateInstance->save();
					error_log('loadForWorkflow function completed');
				} catch (Exception $e) {
					error_log('loadForWorkflow function did not execute correctly');
				}
				error_log('-- test record connection information for Post');
				$GLOBALS['thisFormdbName'] = $this->DbName;
				$GLOBALS['thisFormdbSchema'] = $this->dbSchema;
				error_log('$this->DbName = '.$this->DbName);
				error_log('$_SESSION[APPLICATION][\'dbName\'] = '.$_SESSION[APPLICATION]['dbName']);
				error_log('$this->dbSchema = '.$this->dbSchema);
				error_log('$_SESSION[APPLICATION][\'dbSchema\'] = '.$_SESSION[APPLICATION]['dbSchema']);
				error_log('-- end of test record connection information for post');
				include_once('classes/class.'.trim($this->formType).'.php');
				error_log('this schema is '.$this->schema);
				error_log('this database is '.$this->dbName);
				$formPost = new $this->formType();
				$formPost->postForm($this->formID);
				//if(!$formPost->postForm($this->formID)) {
				//  try {
				// 	$failMail = new mail();
				// 	$failMail->setType('HTML');
				//	 	$failMail->addSender('formsProcessor@flexngate-mi.com');
				// 	$failMail->addRecipient('gslater@arbsol.com');
				// 	$failMail->addSubject($this->formID.' failed to post');
				// 	$failMail->addMessage('FormID <b>'.$this->formID.'</b> failed to post properly.');
				// 	$failMail->send();
				//  }
				// catch (Exception $e) {
				// 	 error_log('posting failure email could not be sent');
				//}
				//}
				//if (isset($failMail)) {
				//	unset($failMail);
				//}
			}
		}
		if ($_REQUEST['status'] != 'Active' and $_REQUEST['status'] != 'New') {
			error_log('sending final status email');
			try {
				$requestor = new tbluser();
				$requestor->select(null,array('WHERE'=>array('intUserID'=>$this->reqID)));
				if ($reqRow = $requestor->getnext()) {
					$reqTextArray = explode(":",$this->requestText);
					error_log('sending now');
					$strSubject = 'Request Completed: '.$this->getProcessName();
					$txtBody = '<html><body><p><u><b>Workflow Process Details</b></u></p><p>'.$this->formType.' - Request updated to: '.$_REQUEST['status'].'<br>'.$reqTextArray[0].'<br>'.$reqTextArray[1].'</p></body></html>';
					$objUserArray = new PostUserArray();
					$objUserArray->loadUsersFromArray(array($reqRow));
					$objNotification = new Notification();
					$objNotification->setSubject($strSubject);
					$objNotification->setBody($txtBody);
					$objNotification->setUserArray($objUserArray);
					$objNotification->sendNotification();

					$objProcessTransitionInstance->setNotificationID($objNotification->getNotificationID());
					$objProcessTransitionInstance->save();
					error_log('Final disposition of request '.$this->_processingInstanceID.' set to '.$_REQUEST['status'].' has been emailed to '.$reqRow['strEmail']);
				}
				unset($requestor);
			}
			catch (Exception $e) {
				error_log('Final Email Failed');
			}
		}

	}

	public function setCustomConditions($objProcessInstance, $objProcessTransitionArray) {
		//if (!isset($_SESSION[APPLICATION][$this->formType])) {
		//	$_SESSION[APPLICATION][$this->formType] = new $this->formType();
		include_once('classes/class.'.$this->formType.'.php');
		$thisForm = new $this->formType();
		$arrRoleIDs = array();
		$thisForm->setCustomConditions($objProcessInstance, $objProcessTransitionArray, $arrRoleIDs);
		//}
//	$objProcessInstance->loadPotentialUsersForRoleArray($objProcessTransitionArray->getRoleArray());
		//$_SESSION[APPLICATION][$this->formType]->setCustomConditions($objProcessInstance, $objProcessTransitionArray, $arrRoleIDs);
	}

	public function evaluateProcessTransition($objProcessTransitionInstance) {
		//if (!isset($_SESSION[APPLICATION][$this->formType])) {
		//	$_SESSION[APPLICATION][$this->formType] = new $this->formType();
		include_once('classes/class.'.$this->formType.'.php');
		$thisForm = new $this->formType();
		return $thisForm->evaluateProcessTransition($objProcessTransitionInstance);
		//}
		//	$objProcessInstance->loadPotentialUsersForRoleArray($objProcessTransitionArray->getRoleArray());
		//$_SESSION[APPLICATION][$this->formType]->setCustomConditions($objProcessInstance, $objProcessTransitionArray, $arrRoleIDs);
	}

	public function getChildTerminalStates($fromStateID) {
		if(!$fromStateID)
			return false;
		$terminalStates = array();
		$strSQL = "SELECT distinct tblProcessTransition.intToProcessStateID,
							   tblProcessTransition.strTransitionName,
							   tblProcessTransition.strTransitionButton
	FROM dbSystem.tblProcessTransition
	INNER JOIN dbSystem.tblProcessState
	        ON tblProcessTransition.intToProcessStateID = tblProcessState.intProcessStateID
	WHERE tblProcessTransition.intFromProcessStateID = $fromStateID and
	      tblProcessState.blnFinalState = 1";
		$rsResult = $this->getDB()->query($strSQL);
		while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
			$terminalStates[$arrRow["intProcessTransitionID"]] = new ProcessTransition();
			$terminalStates[$arrRow["intProcessTransitionID"]]->setVarsFromRow($arrRow);
		}
		return $terminalStates;
	}
}
class terminalTransitionArray extends ProcessTransitionArray {
	public function getChildTerminalTransitions($fromStateID) {
		if(!$fromStateID)
			return false;
		$terminalTransitions = array();
		$strSQL = "SELECT tblProcessTransition.*
		FROM dbSystem.tblProcessTransition
		INNER JOIN dbSystem.tblProcessState
		ON tblProcessTransition.intToProcessStateID = tblProcessState.intProcessStateID
		WHERE tblProcessTransition.intFromProcessStateID = $fromStateID and
		tblProcessState.blnFinalState = 1";
		$rsResult = $this->getDB()->query($strSQL);
		while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
			$terminalTransitions[$arrRow["intProcessTransitionID"]] = new ProcessTransition();
			$terminalTransitions[$arrRow["intProcessTransitionID"]]->setVarsFromRow($arrRow);
		}
		return $terminalTransitions;
	}
}
include_once('User.class.php');
class formUserArray extends UserArray {
	function loadByFormTypeAndFacility($formType, $DbName, $Plant, $Company, $arrRoleIDs) {
		if(!$formType)
			return false;

		$strSQL = "SELECT distinct tblUser.*
		FROM dbPLM.tblRole
		INNER JOIN dbPLM.tblUserRoleXR
		ON tblUserRoleXR.intRoleID = tblRole.intRoleID
		INNER JOIN dbPLM.tblUser
		ON tblUser.intUserID = tblUserRoleXR.intUserID
		INNER JOIN dbPLM.tblRolePermission
		ON tblRolePermission.intRoleID = tblRole.intRoleID
		INNER JOIN dbPLM.tblPermission
		ON tblPermission.intPermissionID = tblRolePermission.intPermissionID
		INNER JOIN dbPLM.tblSecurityItem
		ON tblSecurityItem.intSecurityItemID = tblPermission.intSecurityItemID
		INNER JOIN dbPLM.userfacilityxr
		ON tblUser.intUserID = userfacilityxr.userID
		INNER JOIN dbSystem.tblWFProcess
		ON tblSecurityItem.strSecurityItem = tblWFProcess.strWFProcessName
		WHERE tblWFProcess.strProcessClass = '$formType'";
		if(is_array($arrRoleIDs)) {
			$strSQL .= " AND tblRole.intRoleID IN ('".implode("','", $arrRoleIDs)."')";
		}
		if(isset($DbName) and $DbName != null) {
			$strSQL .= " AND userfacilityxr.DBase = '$DbName'";
		}
		if(isset($Plant) and $Plant != null) {
			$strSQL .= " AND userfacilityxr.Plant = '$Plant'";
		}
		if(isset($Company) and $Company != null) {
			$strSQL .= " AND userfacilityxr.Company = $Company";
		}
		$rsResult = $this->getDB()->query($strSQL);
		while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
			$this->_arrObjects[$arrRow["intUserID"]] = new User();
			$this->_arrObjects[$arrRow["intUserID"]]->setVarsFromRow($arrRow);
		}
	}
	function loadByFormTypeAndUserList($formType, $arrUsers) {
		if(!$formType)
			return false;

		$strSQL = "SELECT distinct tblUser.*
		FROM dbPLM.tblRole
		INNER JOIN dbPLM.tblUserRoleXR
		ON tblUserRoleXR.intRoleID = tblRole.intRoleID
		INNER JOIN dbPLM.tblUser
		ON tblUser.intUserID = tblUserRoleXR.intUserID
		INNER JOIN dbPLM.tblRolePermission
		ON tblRolePermission.intRoleID = tblRole.intRoleID
		INNER JOIN dbPLM.tblPermission
		ON tblPermission.intPermissionID = tblRolePermission.intPermissionID
		INNER JOIN dbPLM.tblSecurityItem
		ON tblSecurityItem.intSecurityItemID = tblPermission.intSecurityItemID
		INNER JOIN dbPLM.userfacilityxr
		ON tblUser.intUserID = userfacilityxr.userID
		INNER JOIN dbSystem.tblWFProcess
		ON tblSecurityItem.strSecurityItem = tblWFProcess.strWFProcessName
		WHERE tblWFProcess.strProcessClass = '$formType'";
		if(is_array($arrUsers)) {
			$strSQL .= " AND tblUser.intUserID IN ('".implode("','", $arrUsers)."')";
		}
		$rsResult = $this->getDB()->query($strSQL);
		while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
			$this->_arrObjects[$arrRow["intUserID"]] = new User();
			$this->_arrObjects[$arrRow["intUserID"]]->setVarsFromRow($arrRow);
		}
	}
}
include_once('Role.class.php');
class formRoleArray extends RoleArray {
	function loadByFormTypeAndFacility($formType, $DbName, $Plant, $Company, $arrRoleIDs){
		if(!$formType)
			return false;

		$strSQL = "SELECT distinct tblRole.*, tblUser.*
		FROM dbPLM.tblRole
		INNER JOIN dbPLM.tblUserRoleXR
		ON tblUserRoleXR.intRoleID = tblRole.intRoleID
		INNER JOIN dbPLM.tblUser
		ON tblUser.intUserID = tblUserRoleXR.intUserID
		INNER JOIN dbPLM.tblRolePermission
		ON tblRolePermission.intRoleID = tblRole.intRoleID
		INNER JOIN dbPLM.tblPermission
		ON tblPermission.intPermissionID = tblRolePermission.intPermissionID
		INNER JOIN dbPLM.tblSecurityItem
		ON tblSecurityItem.intSecurityItemID = tblPermission.intSecurityItemID
		INNER JOIN dbSystem.tblWFProcess
		ON tblSecurityItem.strSecurityItem = tblWFProcess.strWFProcessName
		INNER JOIN dbPLM.userfacilityxr
		ON tblUser.intUserID = userfacilityxr.userID
		WHERE tblWFProcess.strProcessClass = '$formType'";
		if(is_array($arrRoleIDs)) {
			$strSQL .= " AND tblRole.intRoleID IN ('".implode("','", $arrRoleIDs)."')";
		}
		if(isset($DbName) and $DbName != null) {
			$strSQL .= " AND userfacilityxr.DBase = '$DbName'";
		}
		if(isset($Plant) and $Plant != null) {
			$strSQL .= " AND userfacilityxr.Plant = '$Plant'";
		}
		if(isset($Company) and $Company != null) {
			$strSQL .= " AND userfacilityxr.Company = $Company";
		}
		$rsResult = $this->getDB()->query($strSQL);
		while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
			if(!isset($this->_arrObjects[$arrRow["intRoleID"]])) {
				$this->_arrObjects[$arrRow["intRoleID"]] = new Role();
				$this->_arrObjects[$arrRow["intRoleID"]]->setVarsFromRow($arrRow);
			}
			$this->_arrObjects[$arrRow["intRoleID"]]->getUserArray()->addFromRow($arrRow, "intUserID");
		}
	}

}
class postUserArray extends UserArray {
	function loadUsersFromArray($userRows){
		foreach($userRows as $arrRow) {
			if(!isset($this->_arrObjects[$arrRow["intUserID"]])) {
				$this->_arrObjects[$arrRow["intUserID"]] = new User();
				$this->_arrObjects[$arrRow["intUserID"]]->setVarsFromRow($arrRow);
			}
		}
	}

}
?>
