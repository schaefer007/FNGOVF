<?php
//if(!class_exists('processInstanceArray')) {
	require_once('ProcessInstance.class.php');
//}
require_once('DataClass.class.php');

class workFlowInstanceArray extends processInstanceArray {
	function loadForUserPage($intUserID,$selectedFacilities = array()) {
		if(!$intUserID)
			return;
	    $userCustomInstances = $this->getCustomUserInstances($intUserID,$selectedFacilities);
		$strSQL = "SELECT distinct formrequest.*, tblWFProcess.*, tblProcessInstance.* 
			FROM dbSystem.tblProcessInstance
			INNER JOIN dbSystem.tblWFProcess
				ON tblWFProcess.intWFProcessID = tblProcessInstance.intWFProcessID
			INNER JOIN dbSystem.tblProcessState
				ON tblProcessState.intProcessStateID = tblProcessInstance.intCurrentProcessStateID
			INNER JOIN dbSystem.tblProcessTransition
				ON tblProcessTransition.intFromProcessStateID = tblProcessState.intProcessStateID
			Left Outer JOIN ( 
				SELECT *
					FROM dbSystem.tblCondition
					LEFT OUTER JOIN dbSystem.tblConditionXR
						ON tblCondition.intConditionID = tblConditionXR.intChildConditionID
				) AS tblCondition
				ON tblCondition.intTopLevelConditionID = tblProcessTransition.intConditionID or
				   tblCondition.intConditionID = tblProcessTransition.intConditionID
			INNER JOIN dbsystem.formrequest
				ON formrequest.processingInstanceID = tblProcessInstance.intProcessInstanceID
			Left Outer JOIN dbPLM.tbluserrolexr
				ON tbluserrolexr.intRoleID = tblCondition.intRoleID
				AND tbluserrolexr.intUserID = $intUserID
		    WHERE trim(formrequest.status) in ('New','Active','Re-Submit') and
		          ((trim(formrequest.facilityName) in ('".implode('\',\'',str_replace('\'','\'\'',$selectedFacilities))."') and
             tblProcessState.blnCustomConditions <> 1 and
		           tbluserrolexr.intUserID = $intUserID)";
		if (count($userCustomInstances) > 0) {
			$strSQL .= " or formrequest.processingInstanceID in (".implode(',',$userCustomInstances)."))";
		} else {
			$strSQL .= ")";
		}
		$strSQL.= " order by reqTimeStamp";
		$rsResult = self::getDB()->query($strSQL);
		while($arrRow = self::getDB()->fetch_assoc($rsResult)) {
			$resultArray[] = $arrRow;
		}
		return $resultArray;
	}
	function loadAllForms() {
		$strSQL = "SELECT Distinct intWFProcessID, strWFProcessName, strProcessClass
				     FROM dbsystem.tblWFProcess
				";
		$rsResult = self::getDB()->query($strSQL);
		while($arrRow = self::getDB()->fetch_assoc($rsResult)) {
			if ($arrRow['intWFProcessID'] != intWFPROCESS_ID_VENDOR_CHANGE) {
			  $resultArray[$arrRow['strWFProcessName']] = $arrRow['strProcessClass'];
			}
		}
		return $resultArray;
	}
	function getCustomUserInstances($intUserID, $selectedFacilities = array()) {
		// initialize instance array
		$instanceArray = array();
		$customArray = array();
		// get all form intances currently in a custom state where the user is 
		//  authorized to the facility and the status is active/new.
		$strSQL = "SELECT formrequest.*, tblWFProcess.*, tblProcessInstance.*
		FROM dbSystem.tblProcessInstance
		INNER JOIN dbSystem.tblWFProcess
		ON tblWFProcess.intWFProcessID = tblProcessInstance.intWFProcessID
		INNER JOIN dbSystem.tblProcessState
		ON tblProcessState.intProcessStateID = tblProcessInstance.intCurrentProcessStateID
		INNER JOIN dbsystem.formrequest
		ON formrequest.processingInstanceID = tblProcessInstance.intProcessInstanceID
		WHERE formrequest.status in ('New','Active') and
		      formrequest.facilityName in ('".implode('\',\'',str_replace('\'','\'\'',$selectedFacilities))."') and
			  tblProcessState.blnCustomConditions = 1"; 
		$rsResult = self::getDB()->query($strSQL);
		while($arrRow = self::getDB()->fetch_assoc($rsResult)) {
			$customArray[$arrRow['processingInstanceID']] = $arrRow;
		}
		// for each valid instance in a custom state get the list of custom users.
		foreach ($customArray as $instanceID=>$arrData) {
			    $_SESSION[APPLICATION]['dbCode'] = $arrData['dbSchema'];
			    if (trim($arrData['dbSchema']) == 'CMSDATMX') {
			    	$_SESSION[APPLICATION]['dbSchema'] = 'CMSDAT';
			    } else {
					$_SESSION[APPLICATION]['dbSchema'] = $arrData['dbSchema'];
			    }
				$_SESSION[APPLICATION]['plant'] = $arrData['plant'];
				$_SESSION[APPLICATION]['company'] = $arrData['company'];
				$formClass[$arrData['formType']] = new $arrData['formType']();
				$userArray = $formClass[$arrData['formType']]->getCustomConditionUsers($arrData['formID']);
				// if the current user is in the list of custom users, then add this instance to the 
				//  instance array.
				if (count($userArray) > 0 and in_array($intUserID,$userArray)) {
					$instanceArray[] = $instanceID;
				}
		}
		return $instanceArray;
	}
}
?>
