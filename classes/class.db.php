<?php
/**
 * Database class for Db2 native access
 *
 * This class contains methods and properties used to access data on the Db2 database
 *
 * @author George L. Slater (Arbor Solutions, inc)
 * @copyright 2007 - Grand Rapids Commercial Alliance of Realtors(r)
 * @version 1.0
 * @package Utility
 *
 */
/**
 * Db2 Access
 *
 * Used to make a connection to the Db2 database and perform SQL funtions
 *
 * @author George L. Slater (Arbor Solutions, inc)
 * @copyright 2007 - Grand Rapids Commercial Alliance of Realtors(r)
 * @version 1.0
 * @package Utility
 *
 */
class db {
	/**
	 * Is connection method Persistent?
	 *
	 * @var boolean
	 */
	protected $Persistent;
	/**
	 * Server name to connect to
	 *
	 * @var string
	 */
	protected $Server;
	/**
	 * Error Message Store
	 *
	 * @var string
	 */
	protected $Error;
	/**
	 * Connection User Id
	 *
	 * @var string
	 */
	protected $User;
	/**
	 * Connection Password
	 *
	 * @var string
	 */
	protected $Pwd;
	/**
	 * Array to store variables for binding into prepared statements
	 *
	 * @var array
	 */
	protected $BindArray = array();
	/**
	 * array to store options for the statement
	 *
	 * @var array
	 */
	protected $Options = array();
	/**
	 * Is statement prepared?
	 *
	 * @var boolean
	 */
	protected $Prepared;
	/**
	 * Statement variable
	 *
	 * @var string
	 */
	protected $Statement;
	/**
	 * Result set Limits
	 *
	 * @var int
	 */
	protected $Limits;
	/**
	 * Was the connection being used instantiated within this Script
	 *
	 * @var boolean
	 */
	protected $Local_Connect;
	/**
	 * Have the options been set
	 *
	 * @var boolean
	 */
	protected $OptionsSet;
	/**
	 * Constructor for Db class
	 *
	 * Set the server name and persistence based on data passed when this object was created.
	 *
	 * @param string $Server
	 * @param boolean $Persistence
	 */
function __construct($Server=null, $Persistence=null) {
	$this->Server = $Server;
	$this->Persistent = $Persistence;
	$this->Error = null;
	$this->Prepared = false;
	$this->i = 0;
	$this->Limits = 0;
	$this->Statement = null;
	$this->Connection = null;
	$this->Local_Connect = null;
	$this->OptionsSet = false;
	// force persistence off
	$this->Persistent = false;
}
/**
 * Destructor
 *
 * Perform Cleanup of objects created by class methods that are no longer needed.
 *
 */
function __destruct() {
	$this->close();
}
/**
 * Gets a new connection to the Db
 *
 * @return boolean
 */
function getConnectionInfo() {
	$this->Error = null;
	$conn1 = db2_connect("","","");
	if(!$conn1) {
		$this->Error = 'error in local connection : '.db2_conn_errormsg();
		$this->close();
	} else {
  	$stmt1 = db2_prepare($conn1, "select Uid, Pwd from Qgpl.SysConn where SysName='$this->Server'");
  	$options = array("i5_fetch_only"=>DB2_I5_FETCH_ON);
  	db2_set_option($stmt1,$options,0);
  	if (db2_execute($stmt1)) {
    	While ($row = db2_fetch_array($stmt1)) {
     		$this->User = trim($row[0]);
     		$this->Pwd = trim($row[1]);
    	}
  	} else {
  		$this->Error = "Error occurred in select: ".db2_stmt_errormsg();
		$this->close();
  	}
  	// Free any resources
  	db2_free_result($stmt1);
  	db2_free_stmt($stmt1);
  	db2_close($conn1);
  	if ($this->Error != null) {
  		$this->close();
  		throw new Exception($this->Error);
  	} else {
  		return true;
  	}
	}
}
/**
 * Establish a connection to the Db
 *
 * Either set the connection if one is passed into this function or get a new connection and set the
 * local connection variable to true.
 *
 * @param connection_resource $Connection
 * @return boolean
 */
function connect($Connection=null) {
	$i5ConnectOptions = array('i5_date_fmt'=>DB2_I5_FMT_ISO, 'i5_date_sep'=>DB2_I5_SEP_DASH);
	$this->Error = null;
	if ($Connection == null) {
		$this->Local_Connect = true;
	if ($this->Persistent == true) {
		if ($this->Server == null) {
			if (!$this->Connection = db2_pconnect('','','',$i5ConnectOptions)) {
				$this->Error = "Connection Failed(1): ".db2_conn_errormsg();
			}
		} else {
			if ($this->getConnectionInfo()) {
			// add functions here to retrieve userid and password for server
		if (!$this->Connection = db2_pconnect($this->Server,$this->User,$this->Pwd,$i5ConnectOptions)) {
			$this->Error = "Connection Failed(2): ".db2_conn_errormsg();
		}
			} else {$this->Error = db2_conn_errormsg(); }
			}
	} else {
		if ($this->Server == null) {
			if (!$this->Connection = db2_connect('','','',$i5ConnectOptions)) {
				$this->Error = "Connection Failed(3): ".db2_conn_errormsg();
				}
		} else {
			if ($this->getConnectionInfo()) {
			// add functions here to retrieve userid and password for server
		if (!$this->Connection = db2_connect($this->Server,$this->User,$this->Pwd,$i5ConnectOptions)) {
			$this->Error = "Connection Failed(4): ".db2_conn_errormsg();
		}
				}	else {$this->Error = db2_conn_errormsg(); }
	}
}
} else {
	if (!isset($this->Connection)) {
		$this->Connection = $Connection;
		$this->Local_Connect = false;
	}
	return true;
}
if ($this->Error != null) {
	$this->Close();
	throw new Exception($this->Error);
}
}
/**
 * Get a new connection to the DB and return it for use.
 *
 * @return resource
 */
function Get_Db_Connection($SysName=null) {
	// Retrieve the login information for the target system from the local one.
	$i5ConnectOptions = array('i5_date_fmt'=>DB2_I5_FMT_ISO, 'i5_date_sep'=>DB2_I5_SEP_DASH);
    if ($SysName == null) {
    	$this->Connection = db2_connect("","","",$i5ConnectOptions);
    	$this->Local_Connection = true;
     	return $this->Connection;
    } else {
    	if (file_exists('includes/txtInfo.txt')) {
    		$encryptionKey = file_get_contents('includes/txtInfo.txt');
    	} else {
    		if (file_exists('../includes/txtInfo.txt')) {
    			$encryptionKey = file_get_contents('../includes/txtInfo.txt');
    		}
    	}
    	$conn = db2_connect("","","");
    	$query = "Select dbname, decrypt_char(dbuser,'$encryptionKey') as DBUSER, decrypt_char(dbpwd,'$encryptionKey') as DBPWD from \"dbsystem\".server where dbname = '$SysName'";
    	if ($connections = db2_exec($conn,$query)) {
    		if ($connRow = db2_fetch_assoc($connections)) {
    		$this->Connection = db2_connect($connRow['DBNAME'],$connRow['DBUSER'],$connRow['DBPWD'],$i5ConnectOptions);
    		}
    	}
    	return $this->Connection;
    }
 }
/**
 * Close the Db objects created by class methods
 *
 * @return boolean
 */
function close() {
	if ($this->Prepared == true) {
		if (isset($this->Statement) and $this->Statement != null) {
		if (!db2_free_result($this->Statement)) {
			if ($this->Error == null) {
			$this->Error = "Result Not Freed: ".db2_stmt_errormsg();
			throw new Exception($this->Error);
			}
		}
		if (!db2_free_stmt($this->Statement)) {
			if ($this->Error == null) {
			$this->Error = "Statement Not Freed: ".db2_stmt_errormsg();
			throw new Exception($this->Error);
			}
			}
		}
		} else {
		if (isset($this->Statement) and $this->Statement != null) {
		if (!db2_free_result($this->Statement)) {
			if ($this->Error == null) {
			$this->Error = "Result Not Freed: ".db2_stmt_errormsg();
			throw new Exception($this->Error);
			}
		}
		}
	}
	if ($this->Connection != null and $this->Local_Connect == true) {
	if (!db2_close($this->Connection)) {
		if ($this->Error == null) {
		$this->Error = "Close Connection Failed: ".db2_conn_errormsg();
		throw new Exception($this->Error);
		} else {
			unset($this->Connection);
		}
	}
	}
	unset($this->Statement);
	if ($this->Error == null) {
	return true;
	}
}
function reset() {
		if (isset($this->Statement) and $this->Statement != null) {
		if (!db2_free_result($this->Statement)) {
			if ($this->Error == null) {
			$this->Error = "Result Not Freed: ".db2_stmt_errormsg();
			throw new Exception($this->Error);
			}
		}
		if (!db2_free_stmt($this->Statement)) {
			if ($this->Error == null) {
			$this->Error = "Statement Not Freed: ".db2_stmt_errormsg();
			throw new Exception($this->Error);
			}
			}
		}
}
/**
 * Prepare an SQL statement
 *
 * Prepares a statement for use and sets the prepared variable to true.
 *
 * @param string $sqlStmt
 * @return boolean
 */
function prepare($sqlStmt) {
	$this->Error = null;
	if ($this->Limits > 0) {
		$Statement.="Fetch for ".$SeekPos + $this->Limits." rows optimize for ".$Limits." rows";
	}
	if (!$this->Statement = db2_prepare($this->Connection,$sqlStmt)) {
		$this->Error = "Prepare Failed: ".db2_stmt_errormsg();
		$this->close();
		throw new Exception($this->Error);
	} else {
		$this->Prepared = true;
		return true;
	}
}
/**
 * Adds a bound parameter for a prepared statement
 *
 * Adds a variable to the list of bound variables associated with a statement.  These variables
 * will be used when the statement is actually run.
 *
 * @param string $Param
 * @param int $Number
 * @param int $Type
 */
function addParam($Param,$Number,$Type) {
	$Index = $Number - 1;
	$this->BindArray[$Index] = $Param;
	if (!db2_bind_param($this->Statement,$Number,"bindval_".$Index,$Type)) {
		$this->Error = "Parameter Binding Failed: ".db2_stmt_errormsg();
		$this->close();
		throw new Exception($this->Error);
	}
	if ($Index > $this->i) {
		$this->i = $Index;
	}
//	$this->i++;
}
/**
 * Sets a bound parameter value
 *
 * This function sets the value of a bound paramenter that was previously added using
 * the addParam function.  This is useful when multiple calls to this statement will
 * be made and the values of bound variables change from call to call.
 *
 * @param string $Param
 * @param int $Number
 */
function setParam($Param,$Number) {
	$this->BindArray[$Number-1] = $Param;
}
/**
 * Execute the SQL statement
 *
 * This function will execute a statement that is passed to it or will run a prepared
 * statement if one exists in the object.
 *
 * @param string $Statement
 * @return boolean
 */
function execute($Statement = null) {
	if ($this->Prepared == true) {
  		if (count($this->Options) > 0 and !$this->OptionsSet) {
   	if (!db2_set_option($this->Statement,$this->Options,0))  {
  			$this->Error = db2_stmt_errormsg();
  			$this->close();
  			throw new Exception($this->Error);
  		} else {
  			$this->OptionsSet = true;
  		}
  	}
		if (count($this->BindArray) > 0) {
			$count = 0;
			foreach ($this->BindArray as $bindVal) {
				$name = "bindval_".$count;
				$$name = $bindVal;
				$count++;
			}
		}
		if (db2_execute($this->Statement)) {
				return true;
			} else {
				$this->Error = "Execution Failed: ".db2_stmt_error().db2_stmt_errormsg();
				$this->close();
				throw new Exception($this->Error);
			}
	} else {
		if (count($this->Options) > 0) {
		if (!db2_set_option($this->Connection,$this->Options,1)) {
			$this->Error = "Options Failed: ".db2_stmt_errormsg();
			$this->Close();
			throw new Exception($this->Error);
		}
	}
		if ($this->Limits > 0) {
			$Statement.="Fetch for ".$SeekPos + $this->Limits." rows optimize for ".$this->Limits." rows";
		}
		if ($this->Statement = db2_exec($this->Connection,$Statement)) {
			return true;
		} else {
			error_log($Statement);
			$this->Error = "Execution Failed: ".db2_stmt_error().db2_stmt_errormsg();
			$this->close();
			throw new Exception($this->Error);
		}
	}
}
/**
 * Get next row from results as an ordered array
 *
 * This function retrieves the next row from a set of results
 *
 * @return array
 */
function getNextRow() {
	  if ($row = db2_fetch_array($this->Statement)) {
	 	return $row;
	 } else {
	 	$this->Error = "Fetch Failed: ".db2_stmt_errormsg();
	 	return false;
	 }
	}
/**
 * Retrieve the next row from the results as an associated array
 *
 * This function retrieves the next row from a set of results
 *
 * @return array
 */
function getNextAssoc() {
	  if ($row = db2_fetch_assoc($this->Statement)) {
	 	return $row;
	 } else {
	 	$this->Error = "Fetch Failed: ".db2_stmt_errormsg();
	 	return false;
	 }
	}
/**
 * Retrieve the next row from the results as both an array and an associated array
 *
 * This function retrieves the next row from a set of results
 *
 * @return array
 */
function getNextBoth() {
	  if ($row = db2_fetch_row($this->Statement)) {
	 	return $row;
	 } else {
	 	$this->Error = "Fetch Failed: ".db2_stmt_errormsg();
	 	return false;
	 }
	}
/**
 * Go to a relative row in a result set
 *
 * This function will position the cursor within a result set based on the row number
 * passed to the function.
 *
 * @param int $RowNum
 * @return boolean
 */
function seekRow($RowNum) {
	if (db2_fetch_row($this->Statement,$RowNum)) {
		$this->SeekPos = $RowNum;
		return true;
	} else {
		$this->Error = "Seek Failed: ".db2_stmt_errormsg();
		return false;
	}
}
/**
 * Sets the limit value for results
 *
 * This function sets the limit property for this class.  This value is used when executing a
 * statement.
 *
 * @param int $Limits
 */
function setLimit($Limits) {
	$this->Limits = $Limits;
}
/**
 * Set options for the running of an SQL statement
 *
 * This function stores the options in an array that is used when the statement is executed to
 * set the appropriate options.
 *
 * @param string $Key
 * @param int $Value
 */
function setOptions($Key,$Value) {
	$this->Options[$Key] = $Value;
	$this->OptionsSet = false;
}
/**
 * Retrieve the number of rows affected
 *
 * Retrieve the number of rows affected by an insert, update, or delete operation.  This
 * will return zero for a select statement.
 *
 * @return boolean
 */
function count() {
 	return db2_num_rows($this->Statement);
}
function getColText($schema,$table) {
	$columnText = array();
	if ($sysColumns = db2_exec($this->Connection,"Select Column_Name, Column_Text from qsys2.syscolumns where (ucase(Table_Name) = ucase('$table') or ucase(System_Table_Name) = ucase('$table')) and (Table_Schema = '$schema' or System_Table_Schema = '$schema')")) {
		while ($row = db2_fetch_assoc($sysColumns)) {
			$columnText[$row['COLUMN_NAME']]['COLUMN_TEXT'] = $row['COLUMN_TEXT'];
		}
	}
	return $columnText;
}
function getColumnMeta($schema,$table) {
	if (isset($this->Statement)) {
		unset($this->Statement);
	}
	$query = "Select COLUMN_NAME, DATA_TYPE as TYPE_NAME, LENGTH as COLUMN_SIZE, NUMERIC_SCALE as SCALE, NUMERIC_SCALE as NUM_SCALE, NUMERIC_PRECISION as LENGTH_PRECISION, CHARACTER_OCTET_LENGTH as CHAR_OCTET_LENGTH, IS_NULLABLE as NULLABLE, IS_IDENTITY from QSYS2.SYSCOLUMNS where (TABLE_SCHEMA = '$schema' or SYSTEM_TABLE_SCHEMA = '$schema') and (ucase(TABLE_NAME) = ucase('$table') or ucase(SYSTEM_TABLE_NAME) = ucase('$table')) order by Ordinal_Position";
	if ($this->Statement = db2_exec($this->Connection,$query)) {
		return $this->Statement;
	} else {
		return false;
	}
}

function getSqlPrimaryKeys($schema,$table) {
	if ($this->Statement = db2_primary_keys($this->Connection, '', $schema, $table)) {
		return $this->Statement;
	} else {
  error_log("error occurred getting primary keys for table $schema/$table: ".db2_stmt_error().db2_stmt_errormsg());
		$this->Error = "Execution Failed for $schema/$table: ".db2_stmt_error().db2_stmt_errormsg();
		throw new Exception($this->Error);
		return false;
	}
}
function getDDSPrimaryKeys($schema,$table) {

		// No SQL primary keys so check Physical file primary keys
		if (substr($table,0,1) == '"') {
			$tableNameArray = explode('"',$table);
			$table = $tableNameArray[1];
		}
		if (substr($schema,0,1) == '"') {
			$schemaArray = explode('"',$schema);
			$schema = $schemaArray[1];
		}
		if ($syskeys = db2_exec($this->Connection,"Select Column_Names from qsys2.syspindex where (Index_Name = Table_Name or Index_Type = 'PRIMARY KEY') and ((ucase(Table_Name) = ucase('$table') or ucase(System_Table_Name) = ucase('$table')) and (Table_Schema = '$schema' or System_Table_Schema = '$schema'))")) {
		   if ($row = db2_fetch_assoc($syskeys)) {
		      $keys = explode(',',$row['COLUMN_NAMES']);
		      return $keys;
		   }
		} else {
			$this->Error = "Execution Failed for $schema/$table: ".db2_stmt_error().db2_stmt_errormsg();
			throw new Exception($this->Error);
		}
}
function getNextSequence($schema,$sequence) {
	$this->execute("select nextval for $schema.$sequence from sysibm.sysdummy1");
	$idRow = $this->getNextRow();
	return $idRow[0];
}
function getLastSequence($schema,$sequence) {
	$this->execute("select prevval for $schema.$sequence from sysibm.sysdummy1");
	$idRow = $this->getNextRow();
	return $idRow[0];
}
}
