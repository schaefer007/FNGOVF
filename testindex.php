<?php
echo php_info();
require_once('includes/initialize.php');
//echo var_dump($_SESSION[APPLICATION]);
if (!is_array($_SESSION[APPLICATION]) or !array_key_exists('FACILITY',$_SESSION[APPLICATION])) {
    $_SESSION[APPLICATION]['FACILITY'] = 'unDefined';
}
$page = $view.".php";
if ($cmd == "Save" or $cmd == "Delete" or ($cmd == "Logon" and (!isset($_SESSION[APPLICATION]['loggedIn']) or $_SESSION[APPLICATION]['loggedIn'] != true)) or ($view == "logoff" and $_SESSION[APPLICATION]['loggedIn'] == true)) {
	if (!isset($_REQUEST['updKey']) or $_REQUEST['updKey'] != $_SESSION[APPLICATION]['lastUpdKey']) {
		$targetPage = "process.".$page;
		if (array_key_exists('module',$_SESSION[APPLICATION]) and file_exists($_SESSION[APPLICATION]['module'].'/includes/'.$targetPage)) {
			include($_SESSION[APPLICATION]['module']."/includes/".$targetPage);
		} else {
			if (file_exists("includes/".$targetPage)) {
				include("includes/".$targetPage);
			} else {
				base::missingPage($targetPage);
			}
		}
		if (isset($_REQUEST['updKey'])) {
			$_SESSION[APPLICATION]['lastUpdKey'] = $_REQUEST['updKey'];
		}
	}
}
if (isset($_REQUEST['updKey']) and $_REQUEST['updKey'] == $_SESSION[APPLICATION]['lastUpdKey']) {
	include ('includes/reDirect.php');
}
if (!$haltProcessing) {
	if (array_key_exists('change',$_SESSION[APPLICATION]) and $_SESSION[APPLICATION]['change'] == 'Y') {
		$view = 'changePWD';
		$page = $view.'.php';
		$targetPage = $page;
		if (file_exists('controllers/'.$targetPage)) {
			include('controllers/'.$targetPage);
		} else {
			base::missingPage($targetPage);
		}
	} else {
		$page = $view.".php";
		$targetPage = $page;
		if (isset($_SESSION[APPLICATION]['loggedIn']) and $_SESSION[APPLICATION]['loggedIn'] == true and !isset($_SESSION[APPLICATION]['targetView']) or !array_key_exists('targetView',$_SESSION[APPLICATION]) or (is_array($_SESSION[APPLICATION]) and array_key_exists('targetView',$_SESSION[APPLICATION]) and $_SESSION[APPLICATION]['targetView'] != $view)) {
			$_SESSION[APPLICATION]['targetView'] = $view;
		}
		if (array_key_exists('module', $_SESSION[APPLICATION]) and file_exists($_SESSION[APPLICATION]['module']."/controllers/".$targetPage)) {
			include($_SESSION[APPLICATION]['module']."/controllers/".$targetPage);
		} else {
			if (file_exists('controllers/'.$targetPage)) {
			  	include('controllers/'.$targetPage);
			} else {
				base::missingPage($targetPage);
			}
		}
	}
}
?>
