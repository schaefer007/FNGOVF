<?php 
if (isset($errorText) and count($errorText) > 0) {
	echo "<div class=\"errorDiv\">";
	foreach($errorText as $message) {
		echo $message."<br>";
	}
	echo "</div>";
} 
?>
<form name="loginForm" method="post" action="<?php echo $_SERVER['PHP_SELF']?>">
<div class="loginContainer">
<div class="loginContent">
<div class="formHeader">Change Password</div>
<input type="hidden" name="view" value="<?php if (isset($_SESSION) and array_key_exists('view', $_SESSION)) { echo $_SESSION['view'];}?>"></input>
<table name="loginTable">
<tr>
<td>Old Password:</td><td><input type="password" name="oldPassword" size="50"></input></td>
</tr>
<tr>
<td>New Password:</td><td><input type="password" name="newPassword1" size="50"></input></td>
</tr>
<tr>
<td>Confirm New Password:</td><td><input type="password" name="newPassword2" size="50"></input></td>
</tr>
</table>
<input type="submit" name="cmd" value="Save" onclick="return (this.form.view.value='changePWD');"></input>
</div>
</div>
</form>
