<?php 
function showErrors() {
	global $errorText;
	if (is_array($errorText) and count($errorText) > 0) {
		echo "<div class=\"errorDiv\">";
		foreach ($errorText as $message) {
			echo "$message<br>";
		}
		echo "</div>";
	}
} 
if (isset($_REQUEST['PASSWORD1'])) {
	$password1 = trim($_REQUEST['PASSWORD1']);
} else {
	$password1 = '';
}
if (isset($_REQUEST['PASSWORD2'])) {
	$password2 = trim($_REQUEST['PASSWORD2']);
} else {
	$password2 = '';
}
?>
<div id="contentLegend"><?php echo $viewTitle?></div>
<div style="clear:both;margin-top:20pt;padding-top:10pt;"><p><?php echo $passwordText?>Passwords must be at least 5 characters long</p></div>
<?php 
showErrors();
?> 
<form name="user" id="user" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
<input type="hidden" name="view" value="<?php echo $_REQUEST['view']?>"></input>
<input type="hidden" name="action" value="<?php echo $action?>"></input>
<input type="hidden" name="updKey" value="<?php echo mkTime()?>"></input>
<table width=100%>
<?php 
if (!isset($editUser)) {
	$editUser = new account();
}
echo $editUser->renderFormLine('EMAIL',$EMAIL,true)."\n";
echo $editUser->renderFormLine('FNAME',$FNAME,false)."\n";
echo $editUser->renderFormLine('LNAME',$LNAME,false)."\n";
echo $editUser->renderFormLine('NICKNAME',$NICKNAME,false)."\n";
echo $editUser->renderFormLine('PHONE',$PHONE,false)."\n";
echo $editUser->renderFormLine('EXT',$EXT,false)."\n";
echo $editUser->renderFormLine('MOBILE',$MOBILE,false)."\n";
echo $editUser->renderFormLine('FAX',$FAX,false)."\n";
echo $editUser->renderFormLine('PASSWORD','',false,array('NAME'=>'PASSWORD1','TEXT'=>"$passwordReqd Password"))."\n";
echo $editUser->renderFormLine('PASSWORD','',false,array('NAME'=>'PASSWORD2','TEXT'=>"$passwordReqd Re-Enter Password"))."\n";
?>
</table>
<div>&nbsp;</div>
<input type="submit" name="cmd" value="Save"></input>
</form>
