<?php
//  special handling for state to change format during transition
$combineState = false;
if (is_array($vendorArray[$FACILITY]['prov']['none']) and strPos($VENDORSTATE,':') === false) {
	foreach($vendorArray[$FACILITY]['prov']['none'] as $vaKey=>$vaValue) {
		if (strPos($vaKey,':') !== false) {
			$combineState = true;
		}
	}
}
if ($combineState) {
	$VENDORSTATE = str_pad($VENDORSTATE,3,' ').':'.trim($VENDORCOUNTRY);
}

// End special handling
?>
<input type="hidden" name="view" value="<?php echo $_REQUEST['view']?>"></input>
<input type="hidden" name="ID#" value="<?php echo $editRow['ID#']?>"></input>
<input type="hidden" name="action" value="<?php echo $action?>"></input>
<input type="hidden" name="updKey" value="<?php echo mktime()?>"></input>
<input type="hidden" name="formType" value="<?php if (isset($formRow)) {echo $formRow['formType'];}?>"></input>
<input type="hidden" name="formSchema" id="formSchema" value="<?php if (isset($formRow)) {echo $formRow['dbSchema'];}?>"></input>
<table width=100%>
<?php
$scrollToField = '';
$cpyButton = '';
function getErrorClass($field) {
	global $errorColumn;
	global $scrollToField;
	if (isset($errorColumn) and in_array($field, $errorColumn)) {
		$errClass = array('CLASS'=>'fieldError');
		if ($scrollToField == '') {
			$scrollToField = $field;
		}
	} else {
		$errClass = array();
	}
	return $errClass;
}

// load appropriate vendor array for this form
if (!isset($vendorArray) or !array_key_exists($_REQUEST['FACILITY'],$vendorArray)) {
	if (isset($vendorArray)) {
		unset($vendorArray);
	}
	$vendorArrayFile = 'va_'.str_replace(array(' ',':'),array('_','-'),$_REQUEST['FACILITY']).'.php';
	if (file_exists('includes/'.$vendorArrayFile)) {
		require_once('includes/'.$vendorArrayFile);
	}
}

// This form is entered in three steps as follows:
// 		1. A facility is entered.  This determines the vendor array to use as well as connection information
//		2. A vendor is selected to update ("New" is a special vendor that indicates a new vendor is to be created).
//		3. The vendor input screen is displayed with all relevant input fields.

// If a facility has been chosen, display a disclaimer for entry of the vendor number.
if (isset($step) and $step == 2) {
	echo "<tr><td colspan=\"2\"><div class=\"notice\">Please Note: One vendor form must be completed for each vendor including new remit to's</div></td></tr>";
}

// Instantiate the vendor form class in the session
if (!isset($_SESSION[APPLICATION]['vendorform'])) {
	$_SESSION[APPLICATION]['vendorform'] = new vendorform();
}

// Store commodity information in the session.
if (isset($COMMODITYCAT)) {
	$_SESSION[APPLICATION]['COMMODITYCAT'] = $COMMODITYCAT;
}
if (isset($COMMODITYCAT2)) {
	$_SESSION[APPLICATION]['COMMODITYCAT2'] = $COMMODITYCAT2;
}
if (isset($COMMODITYCAT3)) {
	$_SESSION[APPLICATION]['COMMODITYCAT3'] = $COMMODITYCAT3;
}
if (isset($COMMODITYCAT4)) {
	$_SESSION[APPLICATION]['COMMODITYCAT4'] = $COMMODITYCAT4;
}

// reset the tax rates columns.  Possible values for these columns are determined by the Tax Group Field
$_SESSION[APPLICATION]['vendorform']->resetColumns('GOODSTAXRATE');
$_SESSION[APPLICATION]['vendorform']->resetColumns('SERVICESTAXRATE');

// initialize the facility data for the facility drop down.  This will be a list of all facilities this user is
//  authorized to.  This list of facilities is stored in the session when the user logs in.
$facilityData = array();
foreach ($_SESSION[APPLICATION]['userFacilities'] as $facilityInfo) {
   $facilityKey = $facilityInfo['DBase'].':'.$facilityInfo['Plant'].':'.$facilityInfo['Company'];
   $facilityData[$facilityKey] = $facilityInfo['Name'];
}
$cpyFacilityData = array();
foreach ($_SESSION[APPLICATION]['allFacilities'] as $facilityInfo) {
	$facilityKey = $facilityInfo['DBase'].':'.$facilityInfo['Plant'].':'.$facilityInfo['Company'];
	$cpyFacilityData[$facilityKey] = $facilityInfo['Name'];
}
// If a Corporate vendor master has already been selected, retrieve it's description for display and setup the autocomplet
//  information.
if (!empty($CORPVENDMAST)) {
	if (trim($CORPVENDMAST) == 'NEW') {
		$CVMValue = 'value="New Corporate Vendor - NEW"';
	} else {
		$crvncd = new vmcrvncd();
		$crvncd->select(array('STRIP(CRVNDESC)||\' - \'||STRIP(CORPVEND) as CRVNDESC',CRVNSTAT),array('WHERE'=>array('CORPVEND'=>$CORPVENDMAST)));
		if ($crvnRow = $crvncd->getnext()) {
			$CVMValue = 'value="'.$crvnRow['CRVNDESC'].'"';
			$CVMStat = $crvnRow['CRVNSTAT'];
		}
		unset($crvncd);
	}
	$CVMSelector = $CORPVENDMAST;
} else {
	$CVMSelector = '';
}
$LASTCORPVENDMAST = $CVMSelector;

// If a remit to vendor has already been selected, retrieve it's description for display and setup the autocomplete
//  information
if (!empty($REMITTOVENDOR)) {
	$Vendor = new vend();
	$Vendor->select(array('STRIP(BTNAME)||\' - \'||STRIP(BTVEND) as BTDESC'),array('WHERE'=>array('BTVEND'=>$REMITTOVENDOR)));
	if ($VendorRow = $Vendor->getnext()) {
		$REMITTOSelector = $VendorRow['BTDESC'];
	}
	unset($Vendor);
} else {
	$REMITTOSelector = '';
}
$LASTREMITTOVENDOR = $REMITTOSelector;

// If an old vendor code has already been selected, retrieve it's description for display and setup the autocomplete
//   information.
if (!empty($REQOLDVENDCD)) {
	if (!isset($Vendor)) {
		$Vendor = new vend();
	}
	$Vendor->select(array('STRIP(BTNAME)||\' - \'||STRIP(BTVEND) as BTDESC'),array('WHERE'=>array('BTVEND'=>$REQOLDVENDCD)));
	if ($VendorRow = $Vendor->getnext()) {
		$OLDVENDORSelector = $VendorRow['BTDESC'];
	}
	unset($Vendor);
} else {
	$OLDVENDORSelector = '';
}
$LASTREQOLDVENDCD = $OLDVENDORSelector;

// Define the template array for replacing data in the form field templates.  This will only be used when the normal
//   render function is not called but the field is rendered manually in this script.
$templateArray = array("%text%","%name%","%value%","%size%","%maxlength%","%Js%","%multiple%","%Id%","%rowId%","%rows%");

// If this is step 1, then only the list of available facilities should be displayed.
if ($step == 1) {
$facilityDD = new dropdown('FACILITY',$facilityData);
$ddTemplate = file_get_contents('views/view.dropdownFormField.php');
$templateData = array('*&nbsp;&nbsp;For Production Facility','FACILITY',$facilityDD->getDropDownOptions($_REQUEST['FACILITY'],true),null,null,'onchange="openWait(\'Loading Vendors . . .\'); this.form.submit();"',null,'FACILITY',null,null);
echo str_replace($templateArray,$templateData,$ddTemplate);
} else {
	// If not step 1 and not up to step 3 yet (so must be step 2), then display the vendor autocomplete field.
	if ($step < 3) {
	$ddTemplate = file_get_contents('views/view.textFormField.php');
	$templateData = array('*&nbsp;For Production Facility','FACILITYNAME',$facilityData[$_REQUEST['FACILITY']],null,null,'onchange="this.form.submit();"',null,'FACILITY',null,null);
	echo "<input type=\"hidden\" name=\"FACILITY\" value=\"{$_REQUEST['FACILITY']}\">";
	echo str_replace($templateArray,$templateData,$ddTemplate);
	} else {
		echo "<tr><td>*&nbsp;For Production Facility</td><td class=\"formOut\">$facilityData[$FACILITY]";
		echo "<input type=\"hidden\" name=\"FACILITY\" value=\"{$_REQUEST['FACILITY']}\"></td></tr>";
	}
}
//  if step is 2, then output the autocomplete field for the vendor number
	if (isset($step) and $step == 2) {
		echo "<tr><td class=\"maintLabel\">*&nbsp;&nbsp;Vendor Code</td>
		<td><input type=\"text\" id=\"VNDSelector\" class=\"maintData\" size=\"60\"></input>
		<br><div class=\"inputnote\">Begin typing to search for a value</div></td></tr>";
	} else {
		// If step is greater than 2, then out put all vendor data for update.
	    if (isset($step) and $step > 2) {
		   // If this is a new vendor then specific information for new vendors must be filled in.
           if ($VENDORACTION == 'Add') {
            	$cpyButton = '&nbsp;&nbsp;<input type="button" name="copyButton" value="Copy Another Vendor" onclick="openCpyDialogue()">';
            	// if this is a new vendor then the vendor number field must be presented so the user can enter a vendor number
            	echo str_replace('</td></tr>',$cpyButton.'</td></tr>',$_SESSION[APPLICATION]['vendorform']->renderFormLine('VENDORCODE',$VENDORCODE,false,array('AsAssoc'=>true)));
				$errClass = getErrorClass('CORPVENDMAST');
				$selectedClass = 'class="';
				foreach($errClass as $selector=>$class) {
					$selectedClass .= ' '.$class;
				}
				$selectedClass .= '"';
				$errClass = array_merge(array('maintData'),$errClass);
            	echo "<tr $selectedClass><td class=\"maintLabel\">*&nbsp;Corporate Vendor Code</td>
            	<td><input type=\"text\" id=\"CVMSelector\" name=\"CVMSelector\" class=\"maintData\" size=\"60\" $CVMValue onblur=\"this.value = document.getElementById('CORPVENDMAST').value+' - '+document.getElementById('LASTCORPVENDMAST').value;this.disabled=true;document.getElementById('CVMSelectorEdit').style.visibility='visible';document.getElementById('CVMSelectorDelete').style.visibility='visible';\"></input>&nbsp;
            	<img id=\"CVMSelectorEdit\" class=\"editImage\" style=\"visibility:hidden;\" src=\"plm/images/edit.png\" onclick=\"document.getElementById('CVMSelector').disabled = false;document.getElementById('CVMSelector').value = '';document.getElementById('CVMSelector').focus();this.style.visibility='hidden';document.getElementById('CVMSelectorDelete').style.visibility='hidden';\">
            	<img id=\"CVMSelectorDelete\" class=\"editImage\" style=\"visibility:hidden;\" src=\"plm/images/delete.png\" onclick=\"document.getElementById('CVMSelector').value = '';document.getElementById('CORPVENDMAST').value = '';document.getElementById('LASTCORPVENDMAST').value = '';\">
            	<br><div class=\"inputnote\">Begin typing to search for a value</div>
            	</td></tr>";
            	// Vendor Type
            	$errClass = getErrorClass('REQVENDTYPE');
            	$errClass = array_merge(array('AsAssoc'=>true),$errClass);
		     	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('REQVENDTYPE',$REQVENDTYPE,false,$errClass);
		 		// Purchase Frequency
		 		$errClass = getErrorClass('REQPURCHFREQ');
		     	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('REQPURCHFREQ',$REQPURCHFREQ,false,$errClass);
				// Is this a first time purchase
				$errClass = getErrorClass('REQFIRSTTIME');
				$errClass = array_merge(array('AsAssoc'=>true),$errClass);
		    	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('REQFIRSTTIME',$REQFIRSTTIME,false,$errClass);
				// Old vendor code if this vendor is to replace an existing one.
		    	if (!empty($OLDVENDORSelector)) {
		    		$OLDVENDORValue = 'value="'.$OLDVENDORSelector.'"';
		    	} else {
		    		$OLDVENDORValue = '';
		    	}
		    	echo "<tr><td class=\"maintLabel\">&nbsp;&nbsp;&nbsp;Old Vendor Code</td>
		    	<td><input type=\"text\" id=\"OLDVENDORSelector\" class=\"maintData\" size=\"60\" $OLDVENDORValue onblur=\"this.value = document.getElementById('REQOLDVENDCD').value+' - '+document.getElementById('LASTREQOLDVENDCD').value;this.disabled=true;document.getElementById('OLDVENDORSelectorEdit').style.visibility='visible';document.getElementById('OLDVENDORSelectorDelete').style.visibility='visible';\"></input>&nbsp;
		    	<img id=\"OLDVENDORSelectorEdit\" class=\"editImage\" style=\"visibility:hidden;\" src=\"plm/images/edit.png\" onclick=\"document.getElementById('OLDVENDORSelector').disabled = false;document.getElementById('OLDVENDORSelector').value = '';document.getElementById('OLDVENDORSelector').focus();this.style.visibility='hidden';document.getElementById('OLDVENDORSelectorDelete').style.visibility='hidden';\">
		    	<img id=\"OLDVENDORSelectorDelete\" class=\"editImage\" style=\"visibility:hidden;\" src=\"plm/images/delete.png\" onclick=\"document.getElementById('OLDVENDORSelector').value = '';document.getElementById('REQOLDVENDCD').value = '';document.getElementById('LASTREQOLDVENDCD').value = '';\">
		    	<br><div class=\"inputnote\">Begin typing to search for a value</div>
		    	</td></tr>";
		    	// Should the old vendor be inactivated when this vendor is created.
		    	$errClass = getErrorClass('REQINACTIVEOLD');
		    	$errClass = array_merge(array('AsAssoc'=>true),$errClass);
		    	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('REQINACTIVEOLD',$REQINACTIVEOLD,false,$errClass);
				// Reason code for inactivation
		    	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('INACTIVEREASON',$INACTIVEREASON,false,array('AsAssoc'=>true));
		    	// Reason for this request

		    	$errClass = getErrorClass('REQREASON');
		    	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('REQREASON',$REQREASON,false,$errClass);
		    	// Vendor Name
		    	$errClass = getErrorClass('VENDORNAME');
		    	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('VENDORNAME',$VENDORNAME,false,$errClass);
		    	// Vendor Address Line 1
		    	$errClass = getErrorClass('VENDORADDRESS1');
		    	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('VENDORADDRESS1',$VENDORADDRESS1,false,$errClass);
		    	// Vendor Address Line 2
		    	$errClass = getErrorClass('VENDORADDRESS2');
		    	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('VENDORADDRESS2',$VENDORADDRESS2,false,$errClass);
		    	// Vendor Address Line 3
		    	$errClass = getErrorClass('VENDORADDRESS3');
		    	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('VENDORADDRESS3',$VENDORADDRESS3,false,$errClass);
		    	// Vendor City
		    	$errClass = getErrorClass('VENDORCITY');
		    	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('VENDORCITY',$VENDORCITY,false,$errClass);
		    	// Vendor State
		    	$errClass = getErrorClass('VENDORSTATE');
		    	$errClass = array_merge(array('AsAssoc'=>true),$errClass);
		    	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('VENDORSTATE',$VENDORSTATE,false,$errClass);
		    	// Vendor Postal Code
		    	$errClass = getErrorClass('VENDORZIPCODE');
		    	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('VENDORZIPCODE',$VENDORZIPCODE,false,$errClass);
		    	// Vendor Country
		    	$errClass = getErrorClass('VENDORCOUNTRY');
		    	$errClass = array_merge(array('AsAssoc'=>true),$errClass);
		    	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('VENDORCOUNTRY',$VENDORCOUNTRY,false,$errClass);
		    	// Vendor Phone Number
		    	$errClass = getErrorClass('VENDORPHONE');
		    	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('VENDORPHONE',$VENDORPHONE,false,$errClass);
		    	// Contact Name for vendor
		    	$errClass = getErrorClass('CONTACTNAME');
		    	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('CONTACTNAME',$CONTACTNAME,false,$errClass);
		    	// Vendor Fax Number
		    	$errClass = getErrorClass('VENDORFAX');
		    	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('VENDORFAX', $VENDORFAX, false,$errClass);
		    	// Vendor Web Address
		    	$errClass = getErrorClass('VENDORWEBADDRESS');
		    	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('VENDORWEBADDRESS', $VENDORWEBADDRESS, false,$errClass);
		    	// Vendor Email Address
		    	$errClass = getErrorClass('EMAILADDRESS');
		    	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('EMAILADDRESS',$EMAILADDRESS, false,$errClass);
		    	// Vendor DUNS number
		    	$errClass = getErrorClass('DUNS');
		    	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('DUNS',$DUNS,false,$errClass);
		    	// Vendor GST License Number (only valid for Canadian Addresses)
		    	$errClass = getErrorClass('GSTLICENSE');
		    	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('GSTLICENSE',$GSTLICENSE, false,$errClass);
		    	// Vendor Freight Charges Selection
		    	$errClass = getErrorClass('FRTCHARGES');
		    	$errClass = array_merge(array('AsAssoc'=>true),$errClass);
		    	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('FRTCHARGES',$FRTCHARGES, false,$errClass);
		    	// Vendor Class
		    	$errClass = getErrorClass('VENDORCLASS');
		    	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('VENDORCLASS',$VENDORCLASS,false,$errClass);
		    	// Remit To Vendor Autocomplete Field
		    	if (!empty($REMITTOSelector)) {
		    		$REMITTOValue = 'value="'.$REMITTOSelector.'"';
		    	} else {
		    		$REMITTOValue = '';
		    	}
		    	echo "<tr><td class=\"maintLabel\">&nbsp;&nbsp;&nbsp;Remit to Vendor</td>
		    	<td><input type=\"text\" id=\"REMITTOSelector\" class=\"maintData\" size=\"60\" $REMITTOValue onblur=\"this.value = document.getElementById('REMITTOVENDOR').value+' - '+document.getElementById('LASTREMITTOVENDOR').value;this.disabled=true;document.getElementById('REMITTOSelectorEdit').style.visibility='visible';document.getElementById('REMITTOSelectorDelete').style.visibility='visible';\"></input>&nbsp;
		    	<img id=\"REMITTOSelectorEdit\" class=\"editImage\" style=\"visibility:hidden;\" src=\"plm/images/edit.png\" onclick=\"document.getElementById('REMITTOSelector').disabled = false;document.getElementById('REMITTOSelector').value = '';document.getElementById('REMITTOSelector').focus();this.style.visibility='hidden';document.getElementById('REMITTOSelectorDelete').style.visibility='hidden';\">
		    	<img id=\"REMITTOSelectorDelete\" class=\"editImage\" style=\"visibility:hidden;\" src=\"plm/images/delete.png\" onclick=\"document.getElementById('REMITTOSelector').value = '';document.getElementById('REMITTOVENDOR').value = '';document.getElementById('LASTREMITTOVENDOR').value = '';\">
		    	<br><div class=\"inputnote\">Begin typing to search for a value</div>
		    	</td></tr>";
		    	// Accounts Payable Bank
		    	$errClass = getErrorClass('APBANK');
		    	$errClass = array_merge(array('AsAssoc'=>true),$errClass);
		    	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('APBANK',$APBANK,false,$errClass);
		    	// Minority Flag
		    	$errClass = getErrorClass('MINORITYFLAG');
		    	$errClass = array_merge(array('AsAssoc'=>true),$errClass);
		    	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('MINORITYFLAG',$MINORITYFLAG,false,$errClass);
		    	// AutoVoucher Flag
		    	$errClass = getErrorClass('AUTOVOUCHER');
		    	$errClass = array_merge(array('AsAssoc'=>true),$errClass);
		    	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('AUTOVOUCHER',$AUTOVOUCHER,false,$errClass);
		    	// Check Voucher Flag
		    	$errClass = getErrorClass('CHEQUEVOUCHER');
		    	$errClass = array_merge(array('AsAssoc'=>true),$errClass);
		    	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('CHEQUEVOUCHER',$CHEQUEVOUCHER,false,$errClass);
		    	// Use 1099 Flag
		    	$errClass = getErrorClass('USE1099');
		    	$errClass = array_merge(array('AsAssoc'=>true),$errClass);
            	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('USE1099',$USE1099,false,$errClass);
		    	// Placeholder for Corporate Terms.  This is filled in with corporate vendor terms when a corporate vendor
		    	//   is selected
		    	echo "<tr id=\"row_CORPTERMS\"><td>&nbsp;</td><td id=\"corpTerms\"></td></tr>";
		    	// Vendor Terms
		    	$errClass = getErrorClass('TERMSCODE');
		    	$errClass = array_merge(array('AsAssoc'=>true),$errClass);
            	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('TERMSCODE',$TERMSCODE,false,$errClass);
		    	// Tax Group
		    	$errClass = getErrorClass('TAXGROUP');
		    	$errClass = array_merge(array('AsAssoc'=>true),$errClass);
		    	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('TAXGROUP',$TAXGROUP, false,$errClass);
		    	// FOB
		    	$errClass = getErrorClass('FOB');
		    	$errClass = array_merge(array('AsAssoc'=>true),$errClass);
		    	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('FOB',$FOB,false,$errClass);
		    	// Goods Tax Rate (recalculated when the tax group is modified)
		    	$errClass = getErrorClass('GOODSTAXRATE');
		    	$errClass = array_merge(array('AsAssoc'=>true),$errClass);
		    	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('GOODSTAXRATE',$GOODSTAXRATE, false,$errClass);
		    	// Services Tax Rate (recalculated when the tax group is modified)
		    	$errClass = getErrorClass('SERVICESTAXRATE');
		    	$errClass = array_merge(array('AsAssoc'=>true),$errClass);
		    	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('SERVICESTAXRATE',$SERVICESTAXRATE, false,$errClass);
		    	// Outside Services stockroom
		    	$errClass = getErrorClass('OSSTOCKROOM');
		    	$errClass = array_merge(array('AsAssoc'=>true),$errClass);
		    	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('OSSTOCKROOM',$OSSTOCKROOM, false,array('AsAssoc'=>true));
		    	// Create Planning Schedule?
		    	$errClass = getErrorClass('CRTPLANSCHED');
		    	$errClass = array_merge(array('AsAssoc'=>true),$errClass);
		    	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('CRTPLANSCHED',$CRTPLANSCHED, false,$errClass);
		    	// Planning Schedule Frequency
		    	$errClass = getErrorClass('PLANSCHEDFREQ');
		    	$errClass = array_merge(array('AsAssoc'=>true),$errClass);
		    	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('PLANSCHEDFREQ',$PLANSCHEDFREQ, false,$errClass);
		    	// Planning Schedule Day
		    	$errClass = getErrorClass('PLANSCHEDDAY');
		    	$errClass = array_merge(array('AsAssoc'=>true),$errClass);
		    	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('PLANSCHEDDAY',$PLANSCHEDDAY, false,$errClass);
		    	// Planning Schedule Date
		    	$errClass = getErrorClass('PLANSCHEDDATE');
		    	$errClass = array_merge(array('AsAssoc'=>true),$errClass);
		    	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('PLANSCHEDDATE',$PLANSCHEDDATE,false,$errClass);
		    	// Send Via EDI
		    	$errClass = getErrorClass('SENDEDI');
		    	$errClass = array_merge(array('AsAssoc'=>true),$errClass);
		    	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('SENDEDI',$SENDEDI, false,$errClass);
		    	// Create Shipping Schedule?
		    	$errClass = getErrorClass('CRTSHIPSCHED');
		    	$errClass = array_merge(array('AsAssoc'=>true),$errClass);
		    	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('CRTSHIPSCHED',$CRTSHIPSCHED,false,$errClass);
		    	// Shipping Schedule Frequency
		    	$errClass = getErrorClass('SHIPSCHEDFREQ');
		    	$errClass = array_merge(array('AsAssoc'=>true),$errClass);
		    	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('SHIPSCHEDFREQ',$SHIPSCHEDFREQ,false,$errClass);
		    	// Shipping Schedule Day
		    	$errClass = getErrorClass('SHIPSHCEDDAY');
		    	$errClass = array_merge(array('AsAssoc'=>true),$errClass);
		    	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('SHIPSCHEDDAY',$SHIPSCHEDDAY,false,$errClass);
		    	// Shipping Schedule Date
		    	$errClass = getErrorClass('SHIPDSCHEDDATE');
		    	$errClass = array_merge(array('AsAssoc'=>true),$errClass);
		    	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('SHIPSCHEDDATE',$SHIPSCHEDDATE,false,$errClass);
		    	// Generate Scorecard
		    	$errClass = getErrorClass('GENSCORECARD');
		    	$errClass = array_merge(array('AsAssoc'=>true),$errClass);
		    	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('GENSCORECARD',$GENSCORECARD,false,$errClass);
		    	// Scan Supplier Serial Numbers
		    	$errClass = getErrorClass('SCANSUPPSER');
		    	$errClass = array_merge(array('AsAssoc'=>true),$errClass);
		    	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('SCANSUPPSER',$SCANSUPPSER,false,$errClass);
		    	// Purchase Order Send Mode
		    	$errClass = getErrorClass('POSENDMODE');
		    	$errClass = array_merge(array('AsAssoc'=>true),$errClass);
		    	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('POSENDMODE',$POSENDMODE,false,$errClass);
		    	// Shipping Lead Time
		    	$errClass = getErrorClass('SHIPLEADTIME');
		    	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('SHIPLEADTIME',$SHIPLEADTIME,false,$errClass);
		    	// Shipping Lead Time Units
		    	$errClass = getErrorClass('SHIPLEADUNIT');
		    	$errClass = array_merge(array('AsAssoc'=>true),$errClass);
		    	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('SHIPLEADUNIT',$SHIPLEADUNIT,false,$errClass);
		    	// Round Hours To Days?
		    	$errClass = getErrorClass('ROUNDTODAYS');
		    	$errClass = array_merge(array('AsAssoc'=>true),$errClass);
		    	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('ROUNDTODAYS',$ROUNDTODAYS,false,$errClass);
		    	// Ship on Weekends?
		    	$errClass = getErrorClass('SHPWEDAYS');
		    	$errClass = array_merge(array('AsAssoc'=>true),$errClass);
		    	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('SHIPWEDAYS',$SHIPWEDAYS,false,$errClass);
		    	// Require EDI Remittance?
		    	$errClass = getErrorClass('REQEDIREMIT');
		    	$errClass = array_merge(array('AsAssoc'=>true),$errClass);
		    	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('REQEDIREMIT',$REQEDIREMIT,false,$errClass);
		    	// Allow EDI Partial Payment?
		    	$errClass = getErrorClass('EDIPARTPAYMNT');
		    	$errClass = array_merge(array('AsAssoc'=>true),$errClass);
		    	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('EDIPARTPAYMNT',$EDIPARTPAYMNT,false,$errClass);
		    	// Voucher Control
		    	$errClass = getErrorClass('VOUCHERCONTROL');
		    	$errClass = array_merge(array('AsAssoc'=>true),$errClass);
		    	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('VOUCHERCONTROL',$VOUCHERCONTROL,false,$errClass);
		    	// Process Serial Numbers on ASN's
		    	$errClass = getErrorClass('PROCSERIASN');
		    	$errClass = array_merge(array('AsAssoc'=>true),$errClass);
		    	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('PROCSERIASN',$PROCSERIASN,false,$errClass);
		    	// Timezone
		    	$errClass = getErrorClass('TIMEZONE');
		    	$errClass = array_merge(array('AsAssoc'=>true),$errClass);
		    	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('TIMEZONE',$TIMEZONE,false,$errClass);
		    	// Federal Tax ID (only valid for US addresses)
		    	$errClass = getErrorClass('FEDERALID');
		    	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('FEDERALID',$FEDERALID,false,$errClass);
		    	// Vendor Type
		    	$errClass = getErrorClass('VENDORTYPE');
		    	$errClass = array_merge(array('AsAssoc'=>true),$errClass);
		    	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('VENDORTYPE',$VENDORTYPE,false,$errClass);
		    	// Dumping Class
		    	$errClass = getErrorClass('DUMPINGCLASS');
		    	$errClass = array_merge(array('AsAssoc'=>true),$errClass);
		    	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('DUMPINGCLASS',$DUMPINGCLASS,false,$errClass);
		    	// Default Carrier
		    	$errClass = getErrorClass('DFTCARRIER');
		    	$errClass = array_merge(array('AsAssoc'=>true),$errClass);
		    	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('DFTCARRIER',$DFTCARRIER,false,$errClass);
		    	// Default Custom Broker
		    	$errClass = getErrorClass('DFTCSTBROKER');
		    	$errClass = array_merge(array('AsAssoc'=>true),$errClass);
		    	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('DFTCSTBROKER',$DFTCSTBROKER,false,$errClass);
		    	// User Verification Template
		    	$errClass = getErrorClass('USRVERTEMPLATE');
		    	$errClass = array_merge(array('AsAssoc'=>true),$errClass);
		    	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('USRVERTEMPLATE',$USRVERTEMPLATE,false,$errClass);
		    	// QC Inspector
		    	$errClass = getErrorClass('QCINSPECTOR');
		    	$errClass = array_merge(array('AsAssoc'=>true),$errClass);
		    	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('QCINSPECTOR',$QCINSPECTOR,false,$errClass);
		    	// Initial Display Scheduled Releases?
		    	$errClass = getErrorClass('INITDSPSCHEDR');
		    	$errClass = array_merge(array('AsAssoc'=>true),$errClass);
		    	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('INITDSPSCHEDR',$INITDSPSCHEDR,false,$errClass);
		    	// Repair Location
		    	$errClass = getErrorClass('REPAIRLOCATION');
		    	$errClass = array_merge(array('AsAssoc'=>true),$errClass);
		    	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('REPAIRLOCATION',$REPAIRLOCATION,false,$errClass);
		    	// MRP Notification Method
		    	$errClass = getErrorClass('MRPNOTIFICATION');
		  	  	$errClass = array_merge(array('AsAssoc'=>true),$errClass);
		    	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('MRPNOTIFICATION',$MRPNOTIFICATION,false,$errClass);
		    	// load drop down field row template
		    	$ddTemplate = file_get_contents('views/view.dropdownFormField.php');
		    	// Set valid information for Commodities already selected so that fields will render with current data
		    	if (isset($COMMODITYCAT)) {
 		    	  $_REQUEST['COMMODITYCAT'] = $COMMODITYCAT;
		    	}
		    	if (isset($COMMODITYCAT2)) {
		    	  $_REQUEST['COMMODITYCAT2'] = $COMMODITYCAT2;
		    	}
		    	if (isset($COMMODITYCAT3)) {
		    	  $_REQUEST['COMMODITYCAT3'] = $COMMODITYCAT3;
		    	}
		    	if (isset($COMMODITYCAT4)) {
		    	  $_REQUEST['COMMODITYCAT4'] = $COMMODITYCAT4;
		    	}
		    	// Render the commodity Drop Down for level 1
		    	$errClass = getErrorClass('COMMODITYCAT');
		    	echo $_SESSION[APPLICATION]['vendorform']->getCommodityDD(1,$errClass);
		    	// Render the commodity Drop Down for level 2
		    	$errClass = getErrorClass('COMMODITYCAT2');
		    	echo $_SESSION[APPLICATION]['vendorform']->getCommodityDD(2,$errClass);
		    	// Render the commodity Drop Down for level 3
		    	$errClass = getErrorClass('COMMODITYCAT3');
		    	echo $_SESSION[APPLICATION]['vendorform']->getCommodityDD(3,$errClass);
		    	// Render the commodity Drop Down for level 4
		    	$errClass = getErrorClass('COMMODITYCAT4');
		    	echo $_SESSION[APPLICATION]['vendorform']->getCommodityDD(4,$errClass);
		    	// If a corporate vendor master has been selected, set value for hidden field.
		    	if (!empty($CVMSelector)) {
		    		$CVMValue = 'value="'.$CVMSelector.'"';
		    	} else {
		    		$CVMValue = '';
		    	}
		    	?>
		    	<div id="addressNote">City, State, Postal Code and Country should be entered in the appropriate fields and not placed in an address line.</div>
          		<?php
          	} else {
           		// Else this is an existing vendor that is being updated so don't allow the vendor code to change
           		echo "<tr><td>*&nbsp;&nbsp;Vendor Code</td><td class=\"formOut\">$VENDORCODE";
           		echo "<input type=\"hidden\" name=\"VENDORCODE\" value=\"$VENDORCODE\"></td></tr>";
           		// Autocomplete field for Corporate Vendor Master
           		echo "<tr><td class=\"maintLabel\">*&nbsp;Corporate Vendor Code</td>
           		<td><input type=\"text\" id=\"CVMSelector\" class=\"maintData\" size=\"60\" $CVMValue onblur=\"this.value = document.getElementById('CORPVENDMAST').value+' - '+document.getElementById('LASTCORPVENDMAST').value;this.disabled=true;document.getElementById('CVMSelectorEdit').style.visibility='visible';document.getElementById('CVMSelectorDelete').style.visibility='visible';\"></input>&nbsp;
           		<img id=\"CVMSelectorEdit\" class=\"editImage\" style=\"visibility:hidden;\" src=\"plm/images/edit.png\" onclick=\"document.getElementById('CVMSelector').disabled = false;document.getElementById('CVMSelector').value = '';document.getElementById('CVMSelector').focus();this.style.visibility='hidden';document.getElementById('CVMSelectorDelete').style.visibility='hidden';\">
           		<img id=\"CVMSelectorDelete\" class=\"editImage\" style=\"visibility:hidden;\" src=\"plm/images/delete.png\" onclick=\"document.getElementById('CVMSelector').value = '';document.getElementById('CORPVENDMAST').value = '';document.getElementById('LASTCORPVENDMAST').value = '';\">
           		<br><div class=\"inputnote\">Begin typing to search for a value</div>
           		</td></tr>";
           		// Vendor Status
           		$errClass = getErrorClass('VENDORSTATUS');
           		echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('VENDORSTATUS',$VENDORSTATUS, false,$errClass);
           		echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('INACTIVEREASON',$INACTIVEREASON,false,array('AsAssoc'=>true));
           		// Vendor Name
           		$errClass = getErrorClass('VENDORNAME');
           		echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('VENDORNAME',$VENDORNAME,false,$errClass);
		    	// Minority Flag
		    	$errClass = getErrorClass('MINORITYFLAG');
		    	$errClass = array_merge(array('AsAssoc'=>true),$errClass);
		    	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('MINORITYFLAG',$MINORITYFLAG,false,$errClass);
		    	// Placeholder for Corporate Terms.  This is filled in with corporate vendor terms when a corporate vendor
		    	//   is selected
		    	echo "<tr id=\"row_CORPTERMS\"><td>&nbsp;</td><td id=\"corpTerms\"></td></tr>";
		    	// Vendor Terms
		    	$errClass = getErrorClass('TERMSCODE');
		    	$errClass = array_merge(array('AsAssoc'=>true),$errClass);
		    	echo $_SESSION[APPLICATION]['vendorform']->renderFormLine('TERMSCODE',$TERMSCODE,false,$errClass);
		    	$ddTemplate = file_get_contents('views/view.dropdownFormField.php');
		    	// Set valid information for Commodities already selected so that fields will render with current data
				if (isset($COMMODITYCAT)) {
		    	   $_REQUEST['COMMODITYCAT'] = $COMMODITYCAT;
		    	}
		    	if (isset($COMMODITYCAT2)) {
		    	   $_REQUEST['COMMODITYCAT2'] = $COMMODITYCAT2;
		    	}
		    	if (isset($COMMODITYCAT3)) {
		    	   $_REQUEST['COMMODITYCAT3'] = $COMMODITYCAT3;
		    	}
		    	if (isset($COMMODITYCAT4)) {
		    	   $_REQUEST['COMMODITYCAT4'] = $COMMODITYCAT4;
		    	}
		    	// Render the commodity Drop Down for level 1
		    	$errClass = getErrorClass('COMMODITYCAT');
		    	echo $_SESSION[APPLICATION]['vendorform']->getCommodityDD(1,$errClass);
		    	// Render the commodity Drop Down for level 2
		    	$errClass = getErrorClass('COMMODITYCAT2');
		    	echo $_SESSION[APPLICATION]['vendorform']->getCommodityDD(2,$errClass);
		    	// Render the commodity Drop Down for level 3
		    	$errClass = getErrorClass('COMMODITYCAT3');
		    	echo $_SESSION[APPLICATION]['vendorform']->getCommodityDD(3,$errClass);
		    	// Render the commodity Drop Down for level 4
		    	$errClass = getErrorClass('COMMODITYCAT4');
		    	echo $_SESSION[APPLICATION]['vendorform']->getCommodityDD(4,$errClass);
		    	// If a corporate vendor master has been selected, set value for hidden field.
		    	if (!empty($CVMSelector)) {
		    		$CVMValue = 'value="'.$CVMSelector.'"';
		    	} else {
		    		$CVMValue = '';
		    	}
           }
		}
	}
?>
</table>
<input type="hidden" name="cmd"></input>
<input type="hidden" name="VENDORACTION" value="<?php echo $VENDORACTION?>"></input>
<input type="hidden" id="CORPVENDMAST" name="CORPVENDMAST" value="<?php if (isset($CORPVENDMAST)) {echo $CORPVENDMAST;}?>"></input>
<input type="hidden" id="LASTCORPVENDMAST" name="LASTCORPVENDMAST" value="<?php if (isset($LASTCORPVENDMAST)) {echo $LASTCORPVENDMAST;}?>"></input>
<input type="hidden" id="REMITTOVENDOR" name="REMITTOVENDOR" value="<?php echo $REMITTOVENDOR?>"></input>
<input type="hidden" id="LASTREMITTOVENDOR" name="LASTREMITTOVENDOR" value="<?php echo $LASTREMITTOVENDOR?>"></input>
<input type="hidden" id="REQOLDVENDCD" name="REQOLDVENDCD" value="<?php echo $REQOLDVENDCD?>"></input>
<input type="hidden" id="LASTREQOLDVENDCD" name="LASTREQOLDVENDCD" value="<?php echo $LASTREQOLDVENDCD?>"></input>
<input type="hidden" id="selectVendor" name="selectVendor" value="<?php echo $selectVendor?>"></input>
<!--<input type="hidden" id="VENDORSTATUS" name="VENDORSTATUS" value="<?//$VENDORSTATUS?>"></input>-->
<?php
if ($step > 2) {
echo "<input type=\"hidden\" id=\"ASNMAPPING\" name=\"ASNMAPPING\" value=\"$ASNMAPPING\"></input>";
//echo "<input type=\"hidden\" name=\"selectVendor\" value=\"$selectVendor\">";
}
if ($step > 2 and $VENDORACTION == 'Add') {
if (isset($_SESSION[APPLICATION]['allFacilities'])) {
	$cpyFacilityDD = new dropdown('CPYFACILITY',$cpyFacilityData);
} else {
	$cpyFacilityDD = new dropdown('CPYFACILITY',$facilityData);
}
?>
		<input type="hidden" name="cpyVendor" id="cpyVendor">
		<div class="popUp" id="cpyVendorPrompt">
		<div class="divHandle" id="cpyVndHandle" onmousedown="dragStart(event,'cpyVendorPrompt')">Click to drag</div>
		<table>
		<tr>
		<td>Facility:</td>
		<td><?php echo $cpyFacilityDD->bldDropDown($_REQUEST['CPYFACILITY'],true)?></td>
		</tr>
		<tr>
		<td>Vendor:</td>
		<td>
		<input type="text" id="cpyVNDSelector" class="maintData" size="60"></input>
				<br><div class="inputnote">Begin typing to search for a value</div>
		</td>
		</tr>
		</table>
		&nbsp;&nbsp;<input type="button" name="copyButton" value="Copy" onclick="this.form.submit();">
		<input type="button" name="closePopUp" value="Cancel" onclick="closeMyPopUp('cpyVendorPrompt')">
		</div>
<?php
}
?>
<script>
// Javascript to run after form loads.

$(window).resize(function() {setAddressNote();});
if (!document.getElementById('TAXGROUP') || !document.getElementById('TAXGROUP').selectedIndex || document.getElementById('TAXGROUP').selectedIndex == 0) {
	if (document.getElementById('GOODSTAXRATE')) {
		document.getElementById('GOODSTAXRATE').disabled = true;
	}
	if (document.getElementById('SERVICESTAXRATE')) {
		document.getElementById('SERVICESTAXRATE').disabled = true;
	}
} else {
	adjustTaxRates(document.getElementById('TAXGROUP')[document.getElementById('TAXGROUP').selectedIndex].value);
}
function setInactiveReason() {
	if (document.getElementById('row_INACTIVEREASON')) {
		var radioObj = document.getElementsByName('REQINACTIVEOLD')[0];
		if (document.getElementById('row_VENDORSTATUS')) {
			var radioObj2 = document.getElementsByName('VENDORSTATUS')[0];
		}
		if ((radioObj && !radioObj.checked) || (radioObj2 && !radioObj2.checked)) {
			document.getElementById('row_INACTIVEREASON').style.display = 'none';
		} else {
			document.getElementById('row_INACTIVEREASON').style.display = '';
			var htmlData = document.getElementById('row_INACTIVEREASON').innerHTML;
			var htmlData = htmlData.replace("&nbsp;&nbsp;&nbsp;Inactive", "*&nbsp;Inactive");
			    htmlData = htmlData.replace("(optional)","");
			//document.getElementById('row_INACTIVEREASON').innerHTML = htmlData;
				$("#row_INACTIVEREASON").html(htmlData);
		}
		setAddressNote();
	}
}

setInactiveReason();
setRequiredByCountry();
setDisableAutoComplete();
getCorpVendorDefaults(formAction);

function blacklisted() {
	goToView('home');
}

$(document).ready(function() {
	$('form:first *:input[type!=hidden]:first').focus();
	<?php if ($CVMStat == "B") { ?>
	openWait('No changes can be made to a vendor that has a blacklisted Corporate Vendor Code. Redirecting to the home page...');
	setTimeout( blacklisted , 2000 );
	<?php } ?>
});

// Autocomplete functions from jQuery ui
$(function(){
	//attach autocomplete for corporate vendor master
	$("#CVMSelector").autocomplete({
		source: function(req, add){
			$.getJSON("runAJAXRequest.PHP?AJAXFunction=getCorpVendors&callback=?",req,function(data) {
				var suggestions = [];
				$.each(data, function(i, val){
				 	if (val.VENDSTAT == ' ') {
					 	var vendorSel = '(A) ' + val.CORPVEND;
				 	} else {
					 	var vendorSel = '(' + val.VENDSTAT + ') ' + val.CORPVEND;
				 	}
					suggestions.push(vendorSel);
				});
				add(suggestions);
			});
		},
		select: function(e, ui) {
			selVal = ui.item.value;
			selBLArray = selVal.split(') ');
			if (selBLArray[0] == '(B') {
				alert(selBLArray[1] + ' is Blacklisted and cannot be selected.');
				$("#CVMSelector").val('');
				$("#CVMSelector").focus();
				return false;
			} else {
				if (selBLArray.length > 1) {
				var newSel = '';
				for (i=1;i<selBLArray.length;i++) {
					if (i+1 < selBLArray.length) {
					 newSel += selBLArray[i] + ') ';
				    } else {
					  newSel += selBLArray[i];
				    }
				}
				selVal = newSel;
				} else {
				selVal = selBLArray[1];
				}
				selArray = selVal.split(' - ');
				$("#CVMSelector").val(selArray[0] + ' - ' + selArray[selArray.length - 1]);
				$("#CORPVENDMAST").val(selArray[selArray.length - 1]);
				$("#LASTCORPVENDMAST").val(selArray[0]);
				// Allows the $(document).ready event to prevent
				// the page from loading and return to the home page
				// if the CVC has a Blacklisted (B) VENDORSTATE.
				$("#VENDORSTATUS").val(selBLArray[0].split("(")[1]);
				this.disabled=true;
				$("#CVMSelectorEdit").css("visibility","visible");
				$("#CVMSelectorDelete").css("visibility","visible");
				getCorpVendorDefaults('change');
			}
			return false;
		},
		change: function(){

		}
	});
	// Attach autocomplete for Remit to vendor
	$("#REMITTOSelector").autocomplete({
		source: function(req, add){
			$.getJSON("runAJAXRequest.PHP?AJAXFunction=getVendorMasters&callback=?",req,function(data) {
				var suggestions = [];
				$.each(data, function(i, val){
				 	if (val.BTSTAT == ' ') {
					 	var vendorSel = '(A) ' + val.BTVEND;
				 	} else {
					 	var vendorSel = '(' + val.BTSTAT + ') ' + val.BTVEND;
				 	}
					suggestions.push(vendorSel);
				});
				add(suggestions);
			});
		},
		select: function(e, ui) {
			selVal = ui.item.value;
			selBLArray = selVal.split(') ');
			selArray = selBLArray[selBLArray.length - 1].split('- ');
			$("#REMITTOVENDOR").val(selArray[selArray.length - 1]);
			$("#LASTREMITTOVENDOR").val(selArray[0]);
			this.disabled=true;
			$("#REMITTOSelectorEdit").css("visibility","visible");
			$("#REMITTOSelectorDelete").css("visibility","visible");
		},
		change: function(){

		}
	});
	// Attach autocomplete for old vendor code
	$("#OLDVENDORSelector").autocomplete({
		source: function(req, add){
			$.getJSON("runAJAXRequest.PHP?AJAXFunction=getVendorMasters&callback=?",req,function(data) {
				var suggestions = [];
				$.each(data, function(i, val){
				 	if (val.BTSTAT == ' ') {
					 	var vendorSel = '(A) ' + val.BTVEND;
				 	} else {
					 	var vendorSel = '(' + val.BTSTAT + ') ' + val.BTVEND;
				 	}
					suggestions.push(vendorSel);
				});
				add(suggestions);
			});
		},
		select: function(e, ui) {
			selVal = ui.item.value;
			selBLArray = selVal.split(') ');
			selArray = selBLArray[selBLArray.length - 1].split('- ');
			$("#REQOLDVENDCD").val(selArray[selArray.length - 1]);
			$("#LASTREQOLDVENDCD").val(selArray[0]);
			this.disabled=true;
			$("#OLDVENDORSelectorEdit").css("visibility","visible");
			$("#OLDVENDORSelectorDelete").css("visibility","visible");
		},
		change: function(){

		}
	});
	// Attach autocomplete for vendor master
	$("#VNDSelector").autocomplete({
		source: function(req, add){
			$.getJSON("runAJAXRequest.PHP?AJAXFunction=getVendorMasters&allowNew=Y&callback=?",req,function(data) {
				var suggestions = [];
				// DEBUG
				console.dir(data);
				$.each(data, function(i, val){
				 	if (val.BTSTAT == ' ') {
					 	var vendorSel = '(A) ' + val.BTVEND;
				 	} else {
					 	var vendorSel = '(' + val.BTSTAT + ') ' + val.BTVEND;
				 	}
					suggestions.push(vendorSel);
				});
				add(suggestions);
			});
		},
		select: function(e, ui) {
			selVal = ui.item.value;
			selBLArray = selVal.split(') ');
			selArray = selBLArray[selBLArray.length - 1].split('- ');
			$("#selectVendor").val(selArray[selArray.length - 1]);
			openWait('Please wait while your form is loading . . .');
			$(this).closest('form').submit();
		},
		change: function(){

		}
	});
	// Attach autocomplete for vendor master
	$("#cpyVNDSelector").autocomplete({
		source: function(req, add){
			$.getJSON("runAJAXRequest.PHP?AJAXFunction=getVendorMasters&facility=" + $("select[name='CPYFACILITY']").val() + "&allowNew=N&callback=?",req,function(data) {
				var suggestions = [];
				// DEBUG
				console.dir(data);
				$.each(data, function(i, val){
				 	if (val.BTSTAT == ' ') {
					 	var vendorSel = '(A) ' + val.BTVEND;
				 	} else {
					 	var vendorSel = '(' + val.BTSTAT + ') ' + val.BTVEND;
				 	}
					suggestions.push(vendorSel);
				});
				add(suggestions);
			});
		},
		select: function(e, ui) {
			selVal = ui.item.value;
			selBLArray = selVal.split(') ');
			selArray = selBLArray[selBLArray.length - 1].split('- ');
			$("#cpyVendor").val(selArray[selArray.length - 1]);
		},
		change: function(){

		}
	});
});
// Patch the autocomplete renderer so that blacklisted vendors will be highlighted.
$(function () {
	   $.ui.autocomplete.prototype._renderItem = function(ul, item) {
		   if (item.label.substring(0,3) == '(B)') {
			    blacklisted = '<span class="blacklisted">' + item.label + '</span>';
		   		return $( '<li></li>' )
		   			.data("item.autocomplete", item)
		   			.append("<a>" + blacklisted + "</a>")
		   			.appendTo(ul);
	   		} else {
		   		if (item.label.substring(0,3) == '(I)') {
				    inactive = '<span class="inactive">' + item.label + '</span>';
			   		return $( '<li></li>' )
			   			.data("item.autocomplete", item)
			   			.append("<a>" + inactive + "</a>")
			   			.appendTo(ul);
		   		} else {
		   		return $( "<li></li>" )
	   			.data("item.autocomplete", item)
	   			.append("<a>" + item.label + "</a>")
	   			.appendTo(ul);
		   		}
	   		}
		   }
});

</script>
<?php
if ($scrollToField != '') {
?>
<script>
var scrollPos = $('#row_<?php echo trim($scrollToField)?>').position().top;
$('#formContents').animate({scrollTop: scrollPos});
</script>
<?php } ?>