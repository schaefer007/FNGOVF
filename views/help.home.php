<div class="pageHeading">Home Page - Help</div>
<div>

<div class="headingLvl1">Flex-N-Gate Forms Request Portal Overview</div>

<p class="indent1">The Flex-N-Gate Forms Request Portal allows users to submit and manage forms requests.  Those requests will be routed and managed as defined in 
specific workflows.  Users that log in will be able to see any forms they submitted as well as any forms that currently require an action from them.  A users 
submissions are shown by default.</p>
<div class="headingLvl1">Filter List By</div>
<p class="indent1">The following options may be selected:
<ul>
<li>My Submissions
<p class="indent1">Lists any forms that you have submitted.</p>
</li>
<li>Requiring My Action
<p class="indent1">Lists any forms currently requiring your action.</p>
</li>
<li>All Forms
<p class="indent1">Lists all forms in the selected status.  This option is only available for administrators.</p>
</li>
</ul></p>
<div class="headingLvl1">Status</div>
<p class="indent1">The following options may be selected:
<ul>
<li>New/Active
<p class="indent1">Lists any forms that are either New (just created) or Active (some action has been taken on them).</p>
</li>
<li>Approved
<p class="indent1">Lists any forms that have been approved and posted.</p>
</li>
<li>Rejected
<p class="indent1">Lists any forms that have been permanently rejected.</p>
</li>
</ul></p>
<div class="headingLvl1">Available Forms</div>
<p class="indent1">This section lists any forms that you are authorized to.</p>