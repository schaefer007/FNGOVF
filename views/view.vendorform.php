<script>
var formAction = 'load';

function setAddressNote() {
	if (document.getElementById('addressNote')) {
		var p = $("#VENDORADDRESS1");
		var offset = p.offset();
		$('#addressNote').css({"top":offset.top-125+'px',
   		 					"left":offset.left+400+'px'});
		$('#addressNote').show();
		}
	}
</script>
<?php
function showErrors() {
	global $errorText;
	if (is_array($errorText) and count($errorText) > 0) {
		echo "<div class=\"errorDiv\">";
		foreach ($errorText as $message) {
			echo "$message<br>";
		}
		echo "</div>";
	}
}
?>
<div class="formHeader"><?php echo $viewTitle?></div>
<?php
showErrors();
?>
<form name="vendorRequest" id="vendorRequest" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
<?php
include('views/view.form.vendorform.php');
if ($step != 2 or empty($VENDORCODE)) {
if ($step > 2) { ?>
<input type="button" name="saveButton" value="Save" onclick="this.form.cmd.value = 'Save';this.form.submit();"></input>
<?php } else { ?>
<input type="button" name="continueButton" value="Continue" onclick="this.form.submit();">
<?php } } ?>
</form>
</div>
</div>