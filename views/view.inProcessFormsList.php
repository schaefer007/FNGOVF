<?php

include_once("classes/Role.class.php");

function showErrors() {
	global $errorText;
	if (is_array($errorText) and count($errorText) > 0) {
		echo "<div class=\"errorDiv\">";
		foreach ($errorText as $message) {
			echo "$message<br>";
		}
		echo "</div>";
	}
}
function showList() {
	global $formsInfo;
	$RoleArray = new RoleArray();
	$RoleArray->load();
	$RoleArrayValues = $RoleArray->getArray();
	unset($RoleArray);
	$idx = 0;
	foreach ($formsInfo as $reportRow) {
			$idx++;
		$tmpFormRequest = new formrequest();
		$tmpFormRequest->loadForWorkflow($reportRow["processingInstanceID"]);
		$processInstance = new ProcessInstance();
		$processInstance->loadForDisplay($reportRow["processingInstanceID"], $tmpFormRequest);

		$class = "";
		if(GetDifferenceInMinutes(GetDateFromResult($reportRow["stsTimeStamp"]),new DateTime('NOW')) > $config['delayTime']) {
			$class = "delayed";
		}

        if ($reportRow['status'] == 'Deleted') {
            echo "<tr id=\"row{$idx}\" onmouseover=\"javascript:qzhighlight('row{$idx}')\" onmouseout=\"javascript:qznormal('row{$idx}')\">\n",
            "<td>".$reportRow['processingInstanceID']."</td>\n",
            "<td>".$reportRow['facilityName']."</td>\n",
            "<td>".$reportRow['formType']."</td>\n",
            "<td>".str_replace(':', '<br>', $reportRow['requestText'])."</td>\n",
            "<td>".substr($reportRow['reqTimeStamp'],0,10)."</td>\n",
            "<td>".$reportRow['requestorName']."</td>\n",
            "<td>".$reportRow['status']."</td>\n",
            "<td>None</td>\n",
            "<td>None</td>\n",
//            "<td>".$reportRow['statusName']."</td>\n",
            "</tr>\n";        
        } else {
	       echo "<tr class=\"$class\" id=\"row{$idx}\" onmouseover=\"javascript:qzhighlight('row{$idx}')\" onmouseout=\"javascript:qznormal('row{$idx}')\">\n",
    	       "<td><a href=\"javascript:goToForm(document.getElementById('formsRequests'),'".$reportRow['processingInstanceID']."')\">".$reportRow['processingInstanceID']."</a></td>\n",
    	       "<td>".$reportRow['facilityName']."</td>\n",
		       "<td>".$reportRow['formType']."</td>\n",
		       "<td>".str_replace(':', '<br>', $reportRow['requestText'])."</td>\n",
		       "<td>".substr($reportRow['reqTimeStamp'],0,10)."</td>\n",
	           "<td>".$reportRow['requestorName']."</td>\n",
               "<td>".$reportRow['status']."</td>\n",
		       "<td>".$reportRow['strNextAction']."</td>\n",
	 	       "<td>".$reportRow['strNextRole']."</td>\n",
               "</tr>\n";
        }
	}
}
?>
<form name="forms" id="formsRequests" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
<input type="hidden" name="view" value="home"></input>
<input type="hidden" name="instanceID" value=""></input>
<?php
showErrors();
$filterFacilityDD = new dropDown('listFacilityFilter', $facilityDDArray);
$listActions = array('sbm'=>'My Submissions','rma'=>'Requiring My Action',);
$listStatus = array('active'=>'New/Active','approve'=>'Approved','reject'=>'Rejected','any'=>'Any Status');
if (isset($_SESSION[APPLICATION]['module']) and $_SESSION[APPLICATION]['module'] == 'admin') {
   $listActions['all'] = 'All Forms';
}
$filterDD = new dropDown('listFilter', $listActions);
$filterStatusDD = new dropDown('listStatusFilter', $listStatus);
?>
<div class="listFilter"><label for="listFilter">Filter List By: </label><select name="listFilter" onchange="this.form.submit();"><?php echo $filterDD->getDropDownOptions($listFilter,false);?></select>&nbsp;&nbsp;<label for="listStatusFilter">Status: </label><select name="listStatusFilter" onchange="this.form.submit();"><?php echo $filterStatusDD->getDropDownOptions($listStatusFilter,false);?></select>&nbsp;&nbsp;<label for="listFacilityFilter">Facility: </label><select name="listFacilityFilter" onchange="this.form.submit();"><?php echo $filterFacilityDD->getDropDownOptions($listFacilityFilter,false);?></select></div>
<table id="formList" width=100%>
<thead>
<tr class="tableHeading">
<th>ID</th>
<th>Facility</th>
<th>Form&nbsp;Type</th>
<th>Form&nbsp;Text</th>
<th>Submission Date</th>
<th>Submitted By</th>
<th>Status</th>
<th>Next Action</th>
<th>Next Role</th>
</tr>
</thead>
<tbody>
<?php
showList();
?>
</tbody>
</table>
</form>
<script>
$("#formList").chromatable({
	width: "940px",
	height: "320px",
	scrolling: "yes"});
function refresh_page() {
	var strloc = window.location.origin + window.location.pathname;
	window.location.href = strloc;
}
setTimeout("refresh_page()",1000 * 5 * 60);
</script>