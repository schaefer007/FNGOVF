<?php 
function showErrors() {
	global $errorText;
	if (is_array($errorText) and count($errorText) > 0) {
		echo "<div class=\"errorDiv\">";
		foreach ($errorText as $message) {
			echo "$message<br>";
		}
		echo "</div>";
	}
} 
?>
<div class="formHeader"><?php echo $viewTitle?></div>
<div style="clear:both;"><p><?php echo $passwordText?>Passwords must be at least 5 characters long</p></div>
<?php 
showErrors();
?> 
<form name="user" id="user" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
<input type="hidden" name="view" value="<?php echo $_REQUEST['view']?>"></input>
<input type="hidden" name="ID#" value="<?php echo $editRow['ID#']?>"></input>
<input type="hidden" name="action" value="<?php echo $action?>"></input>
<table width=100%>
<?php 
if (!isset($editUser)) {
	$editUser = new user();
}
echo $editUser->renderFormLine('EMAIL',trim($editRow['EMAIL']),true)."\n";
echo $editUser->renderFormLine('FNAME',trim($editRow['FNAME']),false)."\n";
echo $editUser->renderFormLine('LNAME',trim($editRow['LNAME']),false)."\n";
echo $editUser->renderFormLine('NICKNAME',trim($editRow['NICKNAME']),false)."\n";
echo $editUser->renderFormLine('PHONE',trim($editRow['PHONE']),false)."\n";
echo $editUser->renderFormLine('EXT',trim($editRow['EXT']),false)."\n";
echo $editUser->renderFormLine('MOBILE',trim($editRow['EXT']),false)."\n";
echo $editUser->renderFormLine('FAX',trim($editRow['FAX']),false)."\n";
echo $editUser->renderFormLine('PASSWORD1',$password1,false,array('TEXT'=>"$passwordReqd Password"))."\n";
echo $editUser->renderFormLine('PASSWORD2',$password2,false,array('TEXT'=>"$passwordReqd Re-Enter Password"))."\n";
?>
</table>
<input type="hidden" name="cmd"></input>
<input type="button" name="saveButton" value="Save" onclick="this.form.cmd.value = 'Save';this.form.submit();"></input>
<input type="button" name="ResetButton" value="Reset Password" onclick="this.form.action.value = 'Reset';this.form.cmd.value = 'Save'; this.form.submit();"></input>
</form>