<div class="pageHeading">Maintain Configuration Options - Help</div>
<div>

<div class="headingLvl1">Content</div>

<p class="indent1">Use this display to maintain configuration settings for the GRAR RETs client application.</p>
<div class="indent1">

<div class="headingLvl1">Security Settings</div>
<p class="indent1">Set Security values for the application.</p>
<div class="headingLvl2">Admin User</div>
<p class="indent1">Enter the user id used to log in with.  There is only one so be very careful when changing this value.</p>
<div class="headingLvl2">Admin Password</div>
<p class="indent1">Leave this field blank to retain the existing password.  If you specify a value in this field, it will be encrypted and stored and you will need to use that new password for any subsequent Logins.</p>

<div class="headingLvl1">Database Settings</div>
<p class="indent1">Set Database values for the application.</p>
<div class="headingLvl2">Schema Name</div>
<p class="indent1">Enter the schema in which to store any local data.  All local tables populated by the import process will be stored in this schema.</p>

<div class="headingLvl1">Email Settings</div>
<p class="indent1">Set Email values for the applications (Email is not currently used by the application).</p>
<div class="headingLvl2">Mail Host</div>
<p class="indent1">Enter the Host Name or address for your email server.</p>
<div class="headingLvl2">Use SSL</div>
<p class="indent1">Let the application know whether your mail server uses an SSL connection or not.</p>
<div class="headingLvl2">Mail Port</div>
<p class="indent1">Let the application know which port to use when connecting to your mail server.</p>
<div class="headingLvl2">Mail Requires Authentication</div>
<p class="indent1">Let the application know if it needs to send authentication information to the mail server.</p>
<div class="headingLvl2">Auth User</div>
<p class="indent1">If your mail server requires authentication, then provide here the authorized user needed to connect to the mail server.</p>
<div class="headingLvl2">Auth Pwd</div>
<p class="indent1">If your mail server requires authentication, then provide the authenticated users password here.</p>
<div class="headingLvl2">Mail From</div>
<p class="indent1">Define who mail should be sent from when the application needs to send out an email.</p>

<div class="headingLvl1">Miscellaneoud Settings</div>
<p class="indent1">Set Miscellaneouse Values for the application.</p>
<div class="headingLvl2">Web URL</div>
<p class="indent1">Provide the direct URL needed to access the application.  This value is used when sending emails to provide a direct link back into the application from outside.</p>
</div> 

<div class="headingLvl1">Actions</div>
<p class="indent1">The following Actions are available on this display:</p>
<div class="headingLvl2">Save</div>
<p class="indent1">Save any changes made.  This is the only option available.  These configuration options cannot be deleted.</p>
</div>