<?php 
if (isset($errorText) and count($errorText) > 0) {
	echo "<div class=\"errorDiv\">";
	foreach($errorText as $message) {
		echo $message."<br>";
	}
	echo "</div>";
} 
?>
<script>
window.onload = function() {document.loginForm.userEmail.focus();}
</script>
<form name="loginForm" method="post" action="<?php echo $_SERVER['PHP_SELF']?>">
<div class="loginContainer">
<div class="loginContent">
<div class="formHeader">Sign in</div>
<table name="loginTable">
<tr>
<td>User&nbsp;Email:</td><td><input type="text" name="userEmail" size="20" maxlength="50"></input></td>
</tr>
<tr>
<td>Password:</td><td><input type="password" name="password" size="20"></input></td>
</tr>
</table>
<div style="margin-top:10pt;">* Note: Email address and passwords are case sensitive!</div>
<input type="submit" name="cmd" value="Logon" class="buttonLeft" onclick="this.form.view.value='login'; return true;"></input>
<input type="button" name="resetButton" value="Reset my Password" class="buttonRight" onclick="document.resetForm.userEmail.value = this.form.userEmail.value; document.resetForm.cmd.value = 'Save'; document.resetForm.view.value='pwdReset';document.resetForm.submit();" style="margin-top:10pt;"></input>
</div>
</div>
<p id="registerBlock"><span>Not registered yet?  Click the button below to register.  You will need to provide some basic information.  You'll be notified as soon as your registration is activated.</span>
<br><input type="button" name="register" value="Register for access" onclick="goToView('registerUser');">
</p>
</form>

<form name="resetForm" method="post" action="<?php echo $_SERVER['PHP_SELF']?>">
<input type="hidden" name="userEmail"></input>
<input type="hidden" name="cmd"></input>
<input type="hidden" name="view"></input>
</form>
