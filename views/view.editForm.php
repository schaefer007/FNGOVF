<script>
var formAction = 'edit';

function setAddressNote() {
    if (document.getElementById('VENDORADDRESS1')) {
	var p = $("#VENDORADDRESS1");
	var position = p.position();
	var scrollTop = document.getElementById('formContents').scrollTop;
	$('#addressNote').css({"top":(position.top+scrollTop)+'px',
   	 					"left":position.left+300+'px'});
	$('#addressNote').show();
	}
}
</script>
<?php
function showErrors() {
	global $errorText;
	if (is_array($errorText) and count($errorText) > 0) {
		echo "<div class=\"errorDiv\">";
		foreach ($errorText as $message) {
			echo "$message<br>";
		}
		echo "</div>";
	}
}
?>
<div class="formHeader"><?php echo $viewTitle?></div>
<?php
showErrors();
?>

<form name="vendorRequest" id="vendorRequest" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
<input type="hidden" name="instanceID" value="<?php echo $_REQUEST['instanceID']?>">
<input type="hidden" name="VFID" value="<?php echo $formRow['formID']?>">
<div class="formContents" id="formContents">
<?php
include("views/view.form.{$formRow['formType']}.php");
?>
</div>
<?php if (in_array($formRow['status'],array('New','Active','Re-Submit'))) { ?>
<input type="button" name="saveButton" id="saveButton" value="Save" onclick="this.form.cmd.value = 'Save';this.form.submit();"></input>
<?php } ?>
<?php if ($formRow['status'] == 'New') { ?>
<input type="button" name="deleteButton" id="deleteButton" value="Delete" onclick="this.form.cmd.value = 'Delete';this.form.action.value='Delete';this.form.submit();"></input>
<?php } ?>
</form>
<?php
include_once('classes/class.workFlowInstance.php');
include_once('smarty.php');

foreach($availableForms as $formName=>$formType) {
	if ($formType == $formRow['formType']) {
		$thisFormType = $formName;
	}
}
$g_strTitle = $thisFormType;
if (!isset($objForm)) {
	$objForm = new formrequest();
}
?>
<style>pre { max-height: 500px; overflow-y: auto; border: 2px solid black; }</style>
<?
$objForm->loadForWorkflow($formRow['processingInstanceID']);
$objForm->getProcessInstance();
//echo "<pre>"; print_r($objForm->getProcessInstance()); echo "</pre>";
$objForm->getProcessInstance()->loadForDisplay($formRow['processingInstanceID'], $objForm);
//echo "<pre>"; print_r($objForm->getProcessInstance()); echo "</pre>";
$objTerminalTransitions = new terminalTransitionArray('ProcessTransition');
$terminalTransitions = $objTerminalTransitions->getChildTerminalTransitions($objForm->getProcessInstance()->getCurrentProcessStateID());

$g_objSmarty->assign("objEC", $objForm);
$g_objSmarty->assign("objProcessInstance", $objForm->getProcessInstance());
?>
<div class="box">
<div class="heading">
<h3><?php echo $thisFormType;?> Process</h3>
<div class="clL"></div>
</div>
<div class="clL"></div>
<?php
$g_objSmarty->display("process_instance.tpl");
$g_objSmarty->display("formRequest.tpl");
?>
<div class="clL"></div>
</div>
<div class="clL"></div>
<script>
var terminalStates = new Array();
<?php
foreach ($terminalTransitions as $objTransition) {
$buttonName = $objTransition->getTransitionButton();
  if (empty($buttonName)) {
  	$buttonName = $objTransition->getTransitionName();
  }
  echo "terminalStates.push('".$buttonName."');";
}
?>

checkCorpVendMast();
checkSaveAllowed();
</script>
