<script>
document.configForm.schema.focus();
</script>
<?php

require_once('classes/class.dropDown.php');
 
if (isset($errorText) and count($errorText) > 0) {
	echo "<div class=\"errorDiv\">";
	foreach($errorText as $message) {
		echo $message."<br>";
	}
	echo "</div>";
} 
?>
<form name="configForm" method="post" action="<?php echo $_SERVER['PHP_SELF']?>">
<div class="formHeader">Configuration Settings</div>
<div class="formHeaderIndent">Security Settings</div>
<table width="100%">
<tr>
<td class="configFormLabel">Admin User:</td>
<td class="formData"><input type="text" name="adminUser" size="20" maxlength="20" value="<?php echo $adminUser?>"></td>
</tr>
<tr>
<td class="configFormLabel">Admin Password:<br>&nbsp;&nbsp;(remains the same unless provided)</td>
<td class="formData"><input type="text" name="adminPassword" size="20" maxlength="20" value=""></td>
</tr>
</table>
<div class="formHeaderIndent">Database Settings</div>
<table width="100%">
<tr>
<td class="configFormLabel">Schema Name:</td>
<td class="fromData"><input type="text" name="schema" size="50" maxlength="50" value="<?php echo $schema?>"></td>
</tr>
</table>
<div class="formHeaderIndent">Email Settings</div>
<table width="100%">
<tr>
<td class="configFormLabel">Mail Host:</td>
<td class="formData"><input type="text" name="mailHost" size="50" maxlength="50" value="<?php echo $mailHost?>"></td>
</tr>
<tr>
<td class="configFormLabel">Use SSL:</td>
<td class="formData">
<?php 
$mailSSlDD = new dropDown('mailSSl', array('N'=>'No', 'Y'=>'Yes'));
echo $mailSSlDD->bldDropDown($mailSSl);
unset($mailSSlDD);
?>
</td>
</tr>
<tr>
<td class="configFormLabel">Mail Port:</td>
<td class="formData"><input type="text" name="mailPort" size="5" maxlength="5" value="<?php echo $mailPort?>"></td>
</tr>
<tr>
<td class="configFormLabel">Mail Requires Authentication:</td>
<td class="formData">
<?php 
$mailAuthDD = new dropDown('mailAuth', array('N'=>'No', 'Y'=>'Yes'));
echo $mailAuthDD->bldDropDown($mailAuth);
unset($mailAuthDD);
?>
</td>
</tr>
<td class="configFormLabel">Auth User:</td>
<td class="formData"><input type="text" name="mailUser" size="50" maxlength="50" value="<?php echo $mailUser?>"></td>
</tr>
<td class="configFormLabel">Auth Pwd:</td>
<td class="formData"><input type="text" name="mailPwd" size="20" maxlength="20" value="<?php echo $mailPwd?>"></td>
</tr>
<td class="configFormLabel">Mail From:</td>
<td class="formData"><input type="text" name="mailFrom" size="50" maxlength="50" value="<?php echo $mailFrom?>"></td>
</tr>
</table>
<div class="formHeaderIndent">Miscellaneous Settings</div>
<table width="100%">
<tr>
<td class="configFormLabel">Web URL:</td>
<td class="formData"><input type="text" name="webURL" size="50" maxlength="50" value="<?php echo $webURL?>"></td>
</tr>
</table>

<input type="submit" name="cmd" value="Save"></input>
<input type="hidden" name="view" value="config"></input>
</form>
</div>

</body>
</html>
