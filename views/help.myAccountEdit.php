<div class="pageHeading">User Edit - Help</div>
<div>

<div class="headingLvl1">Content</div>
<p class="indent1"><img src="media/adminEditUser.jpg"></p>
<p class="indent1">Enter the appropriate settings to change the account.  Items preceeded by an asterisk(*) are required.  All other fields are optional.
<p class="headingLvl2" style="margin-bottom:0px;">Email Address</p>
<p class="indent1" style="margin-top:0px;">Changing the email address will change the email address at which email notices are received and 
will also change the email address used on the Sign In screen.  Email addresses must be unique.</p>
<p class="headingLvl2" style="margin-bottom:0px;">User First Name</p>
<p class="indent1" style="margin-top:0px;">Enter the first name of the user.</p>
<p class="headingLvl2" style="margin-bottom:0px;">User Last Name</p>
<p class="indent1" style="margin-top:0px;">Enter the Last Name of the user.</p>
<p class="headingLvl2" style="margin-bottom:0px;">User Nickname</p>
<p class="indent1" style="margin-top:0px;">Enter the Nickname the user is known by.</p>
<p class="headingLvl2" style="margin-bottom:0px;">Telephone Number</p>
<p class="indent1" style="margin-top:0px;">Enter the primary phone number of the user.  This is the phone number used to search for users by Primary Phone.</p>
<p class="headingLvl2" style="margin-bottom:0px;">Extension</p>
<p class="indent1" style="margin-top:0px;">Enter the users Extension at their primary phone if one exists.</p>
<p class="headingLvl2" style="margin-bottom:0px;">Mobile Phone</p>
<p class="indent1" style="margin-top:0px;">Enter the mobile phone number of the user.</p>
<p class="headingLvl2" style="margin-bottom:0px;">Fax Number</p>
<p class="indent1" style="margin-top:0px;">Enter the Fax Number of the user.</p>
<p class="headingLvl2" style="margin-bottom:0px;">Password</p>
<p class="indent1" style="margin-top:0px;">Entering the password is optional.  If entered it must be entered in both Password fields and both
entries must match.  The password must be at least 5 characters long and has a maximum length of 10 characters.  If entered and valid, the password
will be changed.  If left blank, the password remains the same.</p>
<p class="headingLvl2" style="margin-bottom:0px;">Confirm Password</p>
<p class="indent1" style="margin-top:0px;">If a password is entered it must be re-entered here to confirm it.</p>
<p class="headingLvl2" style="margin-bottom:0px;">Password Change Required</p>
<p class="indent1" style="margin-top:0px;">Setting this to "Yes" will cause the user to be prompted to change their password at their next login.  If the user is 
already logged in, changing this value will have no affect on their current session.</p>

<div class="headingLvl1">Actions</div>
<p class="indent1"><img src="media/adminEditUserActions.jpg"></p>
<p class="headingLvl2" style="margin-bottom:0px;">Save</p>
<p class="indent1" style="margin-top:0px;">Click on the Save button to save any changes made.  If you don't want to make any changes, use the navigation
menu at the top of the content area to go back to the home screen or Sign Out or use the "Back to Users" as described below.</p>
<p class="headingLvl2" style="margin-bottom:0px;">Reset Password</p>
<p class="indent1" style="margin-top:0px;">Clicking on the Reset Password button will cause a random password to be generated and sent to the email 
address association with the accout.  The email will contain the new password.  The user has 24 hours to Sign in with that new password before it becomes
unusable.  After sign in with the new password the user will be prompted to change the password to a new value that only they know.
<p class="indent1" style="margin-top:0px; font-weight:bold;">Important!  If the email address has been changed it must be saved before resetting a password or the new password
will be sent to the old email address.  When the new email password is sent, it is sent to the email address on record at that time.</p>
</p>
</div>
