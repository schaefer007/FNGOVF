<?php if ($_SESSION[APPLICATION]['loggedIn'] == true) {?>
<fieldset>
<legend>Submitted Forms</legend>
<?php include ('views/view.inProcessFormsList.php'); ?>
</fieldset>
<fieldset>
<legend>Available Forms:</legend>
<?php foreach($formsArray as $formName=>$formController) {?>
<div class="menuListItem" onclick="openWait('Please wait while your form loads . . .');goToView('<?php echo $formController;?>');"><?php echo $formName;?></div>
<?php }
if (count($formsArray) == 0) {?>
<div class="menuListItem">No Forms Available</div>
<?php }?>
</fieldset>
<?php }?>