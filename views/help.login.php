<div class="pageHeading">Sign In - Help</div>
<div>

<div class="headingLvl1">Content</div>

<p class="indent1">In order to use the Flex-N-Gate forms client you must first Sign Into the site using a valid User Email and Password.</p>

<p class="indent1">Simply enter the established email address and password then press Enter or click the Logon button to sign in.  The Email address and password are case sensitive and must be entered correctly.  
</p>

<p class="indent1">If you've forgotten your password, you may enter your email address in the field provided and press the "Reset Password" button.  You will receive a temporary password in your email that will be good for 24 hours.  Please log in within that time or you will have to go through this process again.  When you login with your temporary password, you will be required to change it.  Please remember the new password you provide.</p>
<p class="indent1">If you are new to this site and have not registered, you may do so by clicking on the "Register For Access" button.  Please fill in the required information.  Your registration will be pending until final authorization is given.  When that happens you will receive an email letting you know that you can use the website.</p>

</div>