<?php

include_once("constants.php");

define("strSITE_NAME", "Forms Management");
define("strSITE_TITLE", strSITE_NAME);
define("strSITE_KEYWORDS", "");

// Pages
define("strREGISTRATION_PAGE", "index.php"); // Page where users can sign up to the site
define("strNOT_LOGGED_IN_PAGE", "index.php"); // Page to redirect to if not logged in

// Database
define("strENCRYPTION_KEY", "klwef98*@#Fsds");

define("strUSER_TYPE_NORMAL", "Normal");
define("strUSER_TYPE_ADMIN", "Admin");

?>