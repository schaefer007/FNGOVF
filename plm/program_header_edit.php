<?php
	include_once("classes/Product.class.php");
	include_once("common.php");
	$intSecurityItemID = intSECURITY_ITEM_ID_PROGRAM;
	include_once("authenticate.php");
	$arrCSS = array("table.css.php", "program_header_full.css.php", "program_common.css.php", "program_edit.css.php");
	$arrJS = array("program_common.js.php", "program_header_edit.js.php", "program_edit.js.php");
	$g_strTitle = "Edit Program";
	include_once("smarty.php");
	include_once("header.php");
	include_once("classes/User.class.php");
	include_once("classes/Role.class.php");
	include_once("classes/Product.class.php");
	include_once("classes/EngineeringChange.class.php");

	$objProgram = $objProduct = isset($_SESSION["arrFormData"]["objProgram"])?$_SESSION["arrFormData"]["objProgram"]:null;
	if($objProgram) { // Failed save
	} else {
		$intProgramID = isset($_GET["intProgramID"]) ? $_GET["intProgramID"] : false;
		$objProgram = $objProduct = new Product(); // CLEAN CODE: This splitting of $objProgram and $objProduct will help splitting tblProgram and tblProduct (at the db level)
		//$objProgram->loadForProgramHeaderEdit($intProgramID);
		$objProgram->load($intProgramID);
		$objProgram->getProgramCommon(true);
		if($objProgram->getProgramID()) { // Existing Program
			$objProgram->getMemberArray()->loadByProgramID($objProgram->getProgramID());
			$objProgram->getProgramData()->loadByProgramID($objProgram->getProgramID());
		}
	}

	$objSearch->setProgramID($objProgram->getProgramID());

	$objUserArray = new UserArray();
	$objUserArray->loadForMembers();
	$objRoleArray = new RoleArray();
	$objRoleArray->loadActiveRoles();

	// TODO: Allow deleting of MemberRole, with the click of a delete button
	$blnProgramWrite = Security::hasPermission(intSECURITY_ITEM_ID_PROGRAM, intOPERATION_ID_WRITE, $objProgram->getProgramID());
	$blnProgramCreator = $objSessionUser->getUserID() == $objProgram->getUserID();
	$g_objSmarty->assign("blnProgramWrite",  $blnProgramWrite || $blnProgramCreator);
	$g_objSmarty->assign("blnProgramWriteECFields", $objProgram->canWriteECFields());
	$g_objSmarty->assign("blnFinancialWrite", Security::hasPermission(intSECURITY_ITEM_ID_FINANCIAL, intOPERATION_ID_WRITE));
	$g_objSmarty->assign("blnEngineeringChangeWrite", Security::hasPermission(intSECURITY_ITEM_ID_ENGINEERING_CHANGE, intOPERATION_ID_WRITE));

	$g_objSmarty->assign("objProgram", $objProgram);
	$g_objSmarty->assign("objProduct", $objProduct);
	$g_objSmarty->assign("objUserArray", $objUserArray);
	$g_objSmarty->assign("objRoleArray", $objRoleArray);

	$g_objSmarty->display("program_header_edit.tpl");
?>