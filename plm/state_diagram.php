<?php

include_once("common.php");
include_once("classes/WFProcess.class.php");

$intWFProcessID = isset($_GET["intWFProcessID"])?$_GET["intWFProcessID"]:null;
$intProcessPathID = isset($_GET["intProcessPathID"])?$_GET["intProcessPathID"]:null;

$objWFProcess = new WFProcess($intWFProcessID);
$objProcessTransitionArray = $objWFProcess->getProcessStateArray()->loadForStateDiagram($objWFProcess->getWFProcessID(), $intProcessPathID);

header("Content-type: image/png");

$intWidth = $objWFProcess->getProcessStateArray()->getDiagramWidth() + (2 * intPROCESS_STATE_DIAGRAM_PADDING);
$intHeight = $objWFProcess->getProcessStateArray()->getDiagramHeight() + (2 * intPROCESS_STATE_DIAGRAM_PADDING);

$imgDiagram = imagecreate($intWidth, $intHeight);
$white = imagecolorallocate($imgDiagram, 255, 255, 255);
$objBlack = imagecolorallocate($imgDiagram, 0, 0, 0);
$objGrey = imagecolorallocate($imgDiagram, 180, 180, 180);
$objCustomColor = imagecolorallocate($imgDiagram, 255, 0, 0);
$font = 'arial.ttf';
foreach($objWFProcess->getProcessStateArray()->getArray() as $objProcessState) {
	$x = $objProcessState->getLeftX();
	$y = $objProcessState->getTopY();
	$strProcessStateName = insertNewlines($objProcessState->getProcessStateName());
	imagettftext($imgDiagram, 12, 0, $x + 10, $y + 20, $objBlack, $font, $strProcessStateName);
	imagerectangle($imgDiagram, $x, $y, $x + intPROCESS_STATE_WIDTH, $y + intPROCESS_STATE_HEIGHT, $objBlack);
}

$arrOffsets = array(
	"Left" => array(),
	"Right" => array()
);
$intOffset = 8;
$arrRoads = array();
foreach($objProcessTransitionArray->getArray() as $objProcessTransition) {
	if(!$objWFProcess->getProcessStateArray()->objectIsSet($objProcessTransition->getFromProcessStateID()) || !$objWFProcess->getProcessStateArray()->objectIsSet($objProcessTransition->getToProcessStateID()))
		continue;

	$objTransitionColor = $objBlack;
	if($intProcessPathID) {
		if($objProcessTransition->getProcessPathTransition()->getProcessPathTransitionID()) {
			$objTransitionColor = $objCustomColor;
		} else {
			$objTransitionColor = $objGrey;
		}
	}

	$arrLines = array();

	$objFromProcessState = $objWFProcess->getProcessStateArray()->getObject($objProcessTransition->getFromProcessStateID());
	$objToProcessState = $objWFProcess->getProcessStateArray()->getObject($objProcessTransition->getToProcessStateID());

	$blnLeftIsFrom = true;
	if($objFromProcessState->getDiagramXCord() <= $objToProcessState->getDiagramXCord()) {
		$objLeft = $objFromProcessState;
		$objRight = $objToProcessState;
	} elseif($objFromProcessState->getDiagramXCord() > $objToProcessState->getDiagramXCord()) {
		$objLeft = $objToProcessState;
		$objRight = $objFromProcessState;
		$blnLeftIsFrom = false;
	}

	$intRightXOffset = $intLeftYOffset = 0;
	if(!isset($arrOffsets["RightX"][$objRight->getProcessStateID()])) $arrOffsets["RightX"][$objRight->getProcessStateID()] = 0;
	if(!isset($arrOffsets["LeftY"][$objLeft->getProcessStateID()])) $arrOffsets["LeftY"][$objLeft->getProcessStateID()] = 0;

	if(!isset($arrRoads["X".$objLeft->getDiagramXCord()])) $arrRoads["X".$objLeft->getDiagramXCord()] = 0;
	if(!isset($arrRoads["Y".$objRight->getDiagramYCord()])) $arrRoads["Y".$objRight->getDiagramYCord()] = 0;

	$intRightXOffset = $arrOffsets["RightX"][$objRight->getProcessStateID()] * $intOffset;
	$intLeftYOffset = $arrOffsets["LeftY"][$objLeft->getProcessStateID()] * $intOffset;

	$arrPoints = array();
	$arrPoints[] = array($objLeft->getRightX(), $objLeft->getTopY() + $intLeftYOffset + $intOffset);
	$arrPoints[] = array($objLeft->getRightX() + $intOffset + $arrRoads["X".$objLeft->getDiagramXCord()] * $intOffset, $objLeft->getTopY() + $intLeftYOffset + $intOffset);
	$arrPoints[] = array($objLeft->getRightX() + $intOffset + $arrRoads["X".$objLeft->getDiagramXCord()] * $intOffset, $objRight->getTopY() - $intOffset - $arrRoads["Y".$objRight->getDiagramYCord()] * $intOffset/*+ $intRightYOffset + $intOffset */);
	$arrPoints[] = array($objRight->getLeftX() + $intOffset + $intRightXOffset, $objRight->getTopY() - $intOffset - $arrRoads["Y".$objRight->getDiagramYCord()] * $intOffset);
	$arrPoints[] = array($objRight->getLeftX() + $intOffset + $intRightXOffset, $objRight->getTopY());

	foreach($arrPoints as $intKey => $arrPoint) {
		if(!isset($arrPoints[$intKey+1]))
			break;
		$arrPoint2 = $arrPoints[$intKey+1];
		$arrLines[] = array($arrPoint[0], $arrPoint[1], $arrPoint2[0], $arrPoint2[1]);
	}

	$arrOffsets["RightX"][$objRight->getProcessStateID()]++;
	$arrOffsets["LeftY"][$objLeft->getProcessStateID()]++;

	$arrRoads["X".$objLeft->getDiagramXCord()]++;
	$arrRoads["Y".$objRight->getDiagramYCord()]++;

	foreach($arrLines as $arrLine) {
		imageline($imgDiagram, $arrLine[0], $arrLine[1], $arrLine[2], $arrLine[3], $objTransitionColor);
	}

	$arrPoints2 = array();
	if($blnLeftIsFrom) {
		$intLastIndex = count($arrPoints) - 1;
		$arrPoint = $arrPoints[$intLastIndex];
	} else {
		$arrPoint = $arrPoints[0];
	}
	$arrPoints2[] = $arrPoint[0];
	$arrPoints2[] = $arrPoint[1];
	$arrPoints2[] = $arrPoint[0] + ($blnLeftIsFrom?-4:6);
	$arrPoints2[] = $arrPoint[1] + ($blnLeftIsFrom?-6:-4);
	$arrPoints2[] = $arrPoint[0] + ($blnLeftIsFrom?4:6);
	$arrPoints2[] = $arrPoint[1] + ($blnLeftIsFrom?-6:4);
	imagefilledpolygon($imgDiagram, $arrPoints2, 3, $objTransitionColor);


	if($objFromProcessState->getDiagramXCord() == $objToProcessState->getDiagramXCord()) {
		$x1 = $x2 = intPROCESS_STATE_DIAGRAM_PADDING + ($objFromProcessState->getDiagramXCord() - 1) * intPROCESS_STATE_WIDTH + ($objFromProcessState->getDiagramXCord() - 1) * intPROCESS_STATE_SPACE + (intPROCESS_STATE_WIDTH / 2);
	} elseif ($objFromProcessState->getDiagramXCord() > $objToProcessState->getDiagramXCord()) {
		$x1 = $objToProcessState->getRightX();
		$x2 = $objFromProcessState->getLeftX();
	} else {
		$x1 = $objToProcessState->getLeftX();
		$x2 = $objFromProcessState->getRightX();
	}
	if($objFromProcessState->getDiagramYCord() == $objToProcessState->getDiagramYCord()) {
		$y1 = $y2 = intPROCESS_STATE_DIAGRAM_PADDING + ($objFromProcessState->getDiagramYCord() - 1) * intPROCESS_STATE_HEIGHT + ($objFromProcessState->getDiagramYCord() - 1) * intPROCESS_STATE_SPACE + (intPROCESS_STATE_HEIGHT / 2);
	} elseif ($objFromProcessState->getDiagramYCord() > $objToProcessState->getDiagramYCord()) {
		$y1 = $objToProcessState->getBottomY();
		$y2 = $objFromProcessState->getTopY();
	} else {
		$y1 = $objToProcessState->getTopY();
		$y2 = $objFromProcessState->getBottomY();
	}
	//imageline($imgDiagram, $x1, $y1, $x2, $y2, $objBlack);

	/*
	$arrPoints = array($x2, $y2);
	if($x1 < $x2) {
		$xp2 = $x2 - 8;
		$xp3 = $x2 - 8;
	} else {
		$xp2 = $x2 + 8;
		$xp3 = $x2 + 8;
	}
	$yp2 = $y2 - 4;
	$yp3 = $y2 + 4;
	$arrPoints[] = $xp2;
	$arrPoints[] = $yp2;
	$arrPoints[] = $xp3;
	$arrPoints[] = $yp3;
	//imagefilledpolygon($imgDiagram, $arrPoints, 3, $objBlack);
	*/
}



imagepng($imgDiagram);
imagedestroy($imgDiagram);

function insertNewLines($strText) {
	$intWidth = 0;
	$strNewText = "";
	$strCurrentWord = "";
	$intLength = strlen($strText);
	for($i=0;$i<$intLength;$i++) {
		$strChar = $strText[$i];
		$intWidth += 12;
		$strCurrentWord .= $strChar;
		if($strChar == " ") {
			$strNewText .= $strCurrentWord . " ";
			$strCurrentWord = "";
		}
		if($intWidth > intPROCESS_STATE_WIDTH) {
			$strNewText .= "\n";
			$intWidth = 0;
		}
	}
	$strNewText .= $strCurrentWord;
	return $strNewText;
}

?>