<?php /* Smarty version Smarty-3.0.9, created on 2015-08-31 12:59:10
         compiled from "templates/process_transition_line.tpl" */ ?>
<?php /*%%SmartyHeaderCode:17296343255e44f9e53ffd0-01837314%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0f33c5802be6ebb46264c8532852812687f61c8d' => 
    array (
      0 => 'templates/process_transition_line.tpl',
      1 => 1353426548,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '17296343255e44f9e53ffd0-01837314',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_function_cycle')) include '/www/zendsvr6/htdocs/Development/FNGForms/plm/Smarty/libs/plugins/function.cycle.php';
if (!is_callable('smarty_modifier_nl2br')) include '/www/zendsvr6/htdocs/Development/FNGForms/plm/Smarty/libs/plugins/modifier.nl2br.php';
?><?php if (!isset($_smarty_tpl->getVariable('blnEditable',null,true,false)->value)){?>
	<?php $_smarty_tpl->tpl_vars['blnEditable'] = new Smarty_variable(0, null, null);?>
<?php }?>
<?php echo smarty_function_cycle(array('values'=>"altBG0_0,altBG0_1",'assign'=>"strBGClass"),$_smarty_tpl);?>

<tbody id="process_transition_<?php echo $_smarty_tpl->getVariable('objProcessTransition')->value->getProcessTransitionID();?>
">
	<tr class="process_transition <?php echo $_smarty_tpl->getVariable('strBGClass')->value;?>
">
		<td class="output transition_name"><?php echo $_smarty_tpl->getVariable('objProcessTransition')->value->getTransitionName();?>
</td>
		<td class="output condition"><div class="condition_text"><?php echo $_smarty_tpl->getVariable('objProcessTransition')->value->getConditionText();?>
</div></td>
		<td class="output transition_button"><?php echo $_smarty_tpl->getVariable('objProcessTransition')->value->getTransitionButton();?>
</td>
		<td class="output process_state"><?php echo $_smarty_tpl->getVariable('objFromProcessState')->value->getProcessStateName();?>
</td>
		<td class="output process_state"><?php echo $_smarty_tpl->getVariable('objToProcessState')->value->getProcessStateName();?>
</td>
		<?php if ($_smarty_tpl->getVariable('blnEditable')->value){?>
			<td class="actions"><a href="javascript:loadProcessTransition('<?php echo $_smarty_tpl->getVariable('objProcessTransition')->value->getProcessTransitionID();?>
', '<?php echo $_smarty_tpl->getVariable('intWFProcessID')->value;?>
')"><img src="images/edit.png" alt="Edit Transition" /></a></td>
			<td class="actions"><a href="javascript:deleteProcessTransition('<?php echo $_smarty_tpl->getVariable('objProcessTransition')->value->getProcessTransitionID();?>
')"><img src="images/delete.png" alt="Delete Transition" /></a></td>
		<?php }?>
	</tr>
	<?php if ($_smarty_tpl->getVariable('objProcessTransition')->value->getDescription()){?>
		<tr class="<?php echo $_smarty_tpl->getVariable('strBGClass')->value;?>
">
			<td colspan="7" class="output"><b>Description:</b> <?php echo smarty_modifier_nl2br(htmlentities($_smarty_tpl->getVariable('objProcessTransition')->value->getDescription()));?>
</td>
		</tr>
	<?php }?>
</tbody>