<?php /* Smarty version Smarty-3.0.9, created on 2015-08-14 18:10:49
         compiled from "templates/wf_process_edit.tpl" */ ?>
<?php /*%%SmartyHeaderCode:53258724655ce2f296ecc00-56412339%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '918113186d7c77c46b91845d5a93fe7255cd261c' => 
    array (
      0 => 'templates/wf_process_edit.tpl',
      1 => 1348590628,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '53258724655ce2f296ecc00-56412339',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<form id="frmPopup" method="post">
	<div class="heading">
		<h3>Add / Edit Workflow Process</h3>
		<div class="clL"></div>
	</div>

	<input type="hidden" name="arrWFProcess[intWFProcessID]" value="<?php echo $_smarty_tpl->getVariable('objWFProcess')->value->getWFProcessID();?>
" />

	<div class="input_table input_table_wf_process">
		<div class="row">
			<div class="cell">
				<div class="label label_horizontal">Process Name:</div>
				<div class="input input_horizontal">
					<input type="text" name="arrWFProcess[strWFProcessName]" value="<?php echo $_smarty_tpl->getVariable('objWFProcess')->value->getWFProcessName();?>
" />
				</div>
			</div>
			<div class="clL"></div>
		</div>
		<div class="clL"></div>

		<div class="row">
			<div class="label label_horizontal">Starting Process:</div>
			<div class="input input_horizontal" id="process_tools">
				<input type="hidden" name="arrWFProcess[intStartingProcessStateID]" value="<?php echo $_smarty_tpl->getVariable('objWFProcess')->value->getStartingProcessStateID();?>
" />
				<input type="text" name="arrWFProcess[strStartingProcessState]" value="<?php echo $_smarty_tpl->getVariable('objWFProcess')->value->getStartingProcessState()->getProcessStateName();?>
" />
			</div>
			<div class="clL"></div>
		</div>
		<div class="clL"></div>

		<div class="row">
			<input type="button" value="Save" onclick="popupOkSubmit()" />
			<input type="button" value="Cancel" onclick="popupCancel()" />
		</div>
	</div>
</form>