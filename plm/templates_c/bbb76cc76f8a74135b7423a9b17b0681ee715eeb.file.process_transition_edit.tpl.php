<?php /* Smarty version Smarty-3.0.9, created on 2015-08-14 18:10:04
         compiled from "templates/process_transition_edit.tpl" */ ?>
<?php /*%%SmartyHeaderCode:21033453755ce2efc5dd5d4-31711545%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'bbb76cc76f8a74135b7423a9b17b0681ee715eeb' => 
    array (
      0 => 'templates/process_transition_edit.tpl',
      1 => 1350408662,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '21033453755ce2efc5dd5d4-31711545',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<form id="frmPopup" class="edit_tool">
	<div class="heading">
		<h3>Add / Edit Process Transition</h3>
		<div class="clL"></div>
	</div>

	<input type="hidden" name="arrProcessTransition[intProcessTransitionID]" value="<?php echo $_smarty_tpl->getVariable('objProcessTransition')->value->getProcessTransitionID();?>
" />

	<div class="input_table input_table_process_transition">
		<div class="row">
			<div class="cell transition_name">
				<div class="label label_horizontal">Transition Name:</div>
				<div class="input input_horizontal">
					<input type="text" name="arrProcessTransition[strTransitionName]" value="<?php echo htmlentities($_smarty_tpl->getVariable('objProcessTransition')->value->getTransitionName());?>
" />
				</div>
			</div>

			<div class="cell transition_button">
				<div class="label label_horizontal">Transition Button:</div>
				<div class="input input_horizontal">
					<input type="text" name="arrProcessTransition[strTransitionButton]" value="<?php echo htmlentities($_smarty_tpl->getVariable('objProcessTransition')->value->getTransitionButton());?>
" />
				</div>
			</div>
			<div class="clL"></div>
		</div>
		<div class="clL"></div>

		<div class="row">
			<div class="cell state">
				<div class="label label_horizontal">From State:</div>
				<div class="input input_horizontal">
					<select name="arrProcessTransition[intFromProcessStateID]">
						<?php  $_smarty_tpl->tpl_vars['objProcessState'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('objProcessStateArray')->value->getArray(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['objProcessState']->key => $_smarty_tpl->tpl_vars['objProcessState']->value){
?>
							<option value="<?php echo $_smarty_tpl->getVariable('objProcessState')->value->getProcessStateID();?>
" <?php if ($_smarty_tpl->getVariable('objProcessState')->value->getProcessStateID()==$_smarty_tpl->getVariable('objProcessTransition')->value->getFromProcessStateID()){?>selected<?php }?>>
								<?php echo htmlentities($_smarty_tpl->getVariable('objProcessState')->value->getProcessStateName());?>

							</option>
						<?php }} ?>
					</select>
				</div>
			</div>

			<div class="cell state">
				<div class="label label_horizontal">To State:</div>
				<div class="input input_horizontal">
					<select name="arrProcessTransition[intToProcessStateID]">
						<?php  $_smarty_tpl->tpl_vars['objProcessState'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('objProcessStateArray')->value->getArray(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['objProcessState']->key => $_smarty_tpl->tpl_vars['objProcessState']->value){
?>
							<option value="<?php echo $_smarty_tpl->getVariable('objProcessState')->value->getProcessStateID();?>
" <?php if ($_smarty_tpl->getVariable('objProcessState')->value->getProcessStateID()==$_smarty_tpl->getVariable('objProcessTransition')->value->getToProcessStateID()){?>selected<?php }?>>
								<?php echo htmlentities($_smarty_tpl->getVariable('objProcessState')->value->getProcessStateName());?>

							</option>
						<?php }} ?>
					</select>
				</div>
			</div>
			<div class="clL"></div>
		</div>
		<div class="clL"></div>

		<div class="row">
			<div class="cell state">
				<div class="label label_horizontal">Display Order:</div>
				<div class="input input_horizontal">
					<input type="text" name="arrProcessTransition[intDisplayOrder]" value="<?php echo $_smarty_tpl->getVariable('objProcessTransition')->value->getDisplayOrder();?>
" />
				</div>
			</div>
		</div>
		<div class="clL"></div>
		<br />

		<div class="heading">
			<h4>Conditions</h4>
			<div class="clL"></div>
		</div>
		<div class="clL"></div>

		<input type="hidden" name="intTopLevelConditionID" value="<?php echo $_smarty_tpl->getVariable('objProcessTransition')->value->getConditionID();?>
" />
		<table class="table table_conditions">
			<?php $_smarty_tpl->tpl_vars['intConditionLevel'] = new Smarty_variable(0, null, null);?>
			<?php $_smarty_tpl->tpl_vars['intConditionIndex'] = new Smarty_variable(0, null, null);?>
			<?php $_smarty_tpl->tpl_vars['objConditionArray'] = new Smarty_variable($_smarty_tpl->getVariable('objProcessTransition')->value->getConditionArray(), null, null);?>
			<?php $_smarty_tpl->tpl_vars['objCondition'] = new Smarty_variable($_smarty_tpl->getVariable('objConditionArray')->value->getObject($_smarty_tpl->getVariable('objProcessTransition')->value->getConditionID()), null, null);?>
			<?php $_template = new Smarty_Internal_Template("condition_children.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
		</table>
		<br />

		<div class="row">
			<input type="button" value="Save" onclick="popupOk()" />
			<input type="button" value="Cancel" onclick="popupCancel()" />
		</div>
	</div>
</form>