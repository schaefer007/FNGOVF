<?php /* Smarty version Smarty-3.0.9, created on 2015-09-10 14:09:57
         compiled from "templates/process_state_line.tpl" */ ?>
<?php /*%%SmartyHeaderCode:186944392755f18f35eeabf6-34216894%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1558ec338c82e84d9c4a2ee79871294d9a46b287' => 
    array (
      0 => 'templates/process_state_line.tpl',
      1 => 1348667136,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '186944392755f18f35eeabf6-34216894',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_function_cycle')) include '/www/zendsvr6/htdocs/Development/FNGForms/plm/Smarty/libs/plugins/function.cycle.php';
?><tr class="<?php echo smarty_function_cycle(array('values'=>"altBG0_0,altBG0_1"),$_smarty_tpl);?>
" id="process_state_<?php echo $_smarty_tpl->getVariable('objProcessState')->value->getProcessStateID();?>
">
	<td class="output process_state_name"><?php echo $_smarty_tpl->getVariable('objProcessState')->value->getProcessStateName();?>
</td>
	<td class="output description"><?php echo $_smarty_tpl->getVariable('objProcessState')->value->getDescription();?>
</td>
	<?php if ($_smarty_tpl->getVariable('blnEditable')->value){?>
		<td class="actions"><a href="javascript:loadProcessState('<?php echo $_smarty_tpl->getVariable('objProcessState')->value->getProcessStateID();?>
')"><img src="images/edit.png" alt="Edit State" /></a></td>
		<td class="actions"><a href="javascript:deleteProcessState('<?php echo $_smarty_tpl->getVariable('objProcessState')->value->getProcessStateID();?>
')"><img src="images/delete.png" alt="Delete State" /></a></td>
	<?php }?>
</tr>