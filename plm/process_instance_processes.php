<?php
	include_once("common.php");
	include_once("classes/AjaxReturn.class.php");
	include_once("classes/formrequest.class.php");
	include_once("authenticate.php");
	error_log('Processing PLM Action . . .');
	$strAction = isset($_GET["strAction"])?$_GET["strAction"]:(isset($_POST["strAction"])?$_POST["strAction"]:null);
	switch($strAction){
		case "Fulfill Requirement":
			// form id
			$intProcessInstanceID = isset($_POST["intProcessInstanceID"])?$_POST["intProcessInstanceID"]:null;
			// transition id
			$intProcessTransitionID = isset($_POST["intProcessTransitionID"])?$_POST["intProcessTransitionID"]:null;

			$processTransition = new ProcessTransition($intProcessTransitionID);
			$transitionName = ($processTransition->getTransitionName() == "") ? "Submit Form" : $processTransition->getTransitionName();
			base::write_log("Beginning Workflow action processing [$transitionName]","S");
			unset($processTransition);

			$intRoleID = isset($_POST["intRoleID"])?$_POST["intRoleID"]:null;
			$txtComment = isset($_POST["txtComment"])?$_POST["txtComment"]:"";

			$strProcessObjectClass = isset($_POST["strProcessObjectClass"])?$_POST["strProcessObjectClass"]:null;
			$intID = isset($_POST["intID"])?$_POST["intID"]:null;
			if(!$intProcessInstanceID || !$intProcessTransitionID || !$intRoleID || !$strProcessObjectClass || !$intID) {
				exit();
			}

			include_once("classes/$strProcessObjectClass.class.php");

			$objProcessObject = new $strProcessObjectClass();
			$objProcessObject->loadForWorkflow($intID);
//			die();
$arrErrors = $objProcessObject->validateWorkflowAction($intProcessTransitionID, $intRoleID);
if($arrErrors) { // Custom validation for ProcessObject (EC, OC, Time Tracking workflow, etc.)
$objAjaxReturn = new AjaxReturn();
$objAjaxReturn->setStatus("Failure");
$objAjaxReturn->setError(implode("\n", $arrErrors));
if (isset($errorColumn) and count($errorColumn) > 1) {
$objAjaxReturn->setColumns($errorColumn);
}
	base::write_log("Failed Workflow Action [$transitionName]","E");
	unset($processTransition);
header('Content-type: application/json');
echo $objAjaxReturn->toJSON();
exit();
}
include_once("classes/ProcessInstance.class.php");
$objProcessInstance = new ProcessInstance($intProcessInstanceID);
$objProcessInstance->workflowAction($objProcessObject, $intProcessTransitionID, $intRoleID, $txtComment);

base::write_log("Completed Workflow action [$transitionName]","S");

// Return new HTML
include("smarty.php");

$objProcessInstance->loadForDisplay($intProcessInstanceID, $objProcessObject);
$g_objSmarty->assign("objProcessInstance", $objProcessInstance);
error_log(intSECURITY_ITEM_ID_ENGINEERING_CHANGE);
error_log(intOPERATION_ID_OVERRIDE_APPROVAL);
$override = Security::hasPermission(intSECURITY_ITEM_ID_ENGINEERING_CHANGE,intOPERATION_ID_OVERRIDE_APPROVAL);
$g_objSmarty->assign("blnOverrideWorkflow", Security::hasPermission(intSECURITY_ITEM_ID_ENGINEERING_CHANGE, intOPERATION_ID_OVERRIDE_APPROVAL));

$objAjaxReturn = new AjaxReturn();
$objAjaxReturn->setStatus("Success");
$arrChanges[0]["strSelector"] = "#process_instance";
$arrChanges[0]["strChangeType"] = "Modification";
$arrChanges[0]["txtHTML"] = $g_objSmarty->fetch("process_instance.tpl");
$objAjaxReturn->setChanges($arrChanges);
header('Content-type: application/json');
echo $objAjaxReturn->toJSON();
break;
}
error_log('PLM Action Processing Complete . . .');
?>