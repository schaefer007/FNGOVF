<?php
	include_once("common.php");
	$intSecurityItemID = intSECURITY_ITEM_ID_PROGRAM;
	include_once("authenticate.php");
	$arrCSS = array("table.css.php");
	$arrJS = array();
	$g_strTitle = "Engineering Change Management";
	include_once("smarty.php");
	include_once("header.php");
	include_once("classes/EngineeringChange.class.php");

	$arrDefaultSort = array(new Sort("tblEngineeringChange.intEngineeringChangeID", "DESC"));
	$objSearch->getECListPage()->setDefaultSort($arrDefaultSort);
	if(isset($_GET["strSortBy"])) {
		$objSearch->getECListPage()->addSort($_GET["strSortBy"]);
	}
	if(!$objSearch->getECListPage()->getSorts()) { // No sort defined
		$objSearch->getECListPage()->useDefaultSort();
	}
	//$objSearch->setProgramID(isset($_GET["intProgramID"])?$_GET["intProgramID"]:$objSearch->getProgramID());
	//$objSearch->setProductTypeID(isset($_GET["intProductTypeID"])?$_GET["intProductTypeID"]:$objSearch->getProductTypeID());
	//$objSearch->setProductName(isset($_GET["strProductName"])?$_GET["strProductName"]:$objSearch->getProductName());

	if(isset($_GET["btnShowAll"]) || isset($_GET["btnResetDefaults"]))
		$objSearch->clearFilters("EC");
	if(isset($_GET["btnResetDefaults"]))
		$objSearch->getECListPage()->useDefaultSort();

	$objECArray = new EngineeringChangeArray();
	$objECArray->loadForECManagement();

	$g_objSmarty->assign("objECArray", $objECArray);
	$g_objSmarty->assign("objSearch", $objSearch);
	$g_objSmarty->display("ec_manage.tpl");
?>
