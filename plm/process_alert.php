<?php
/**
 * Created by PhpStorm.
 * User: chris
 * Date: 9/8/15
 * Time: 10:34 AM
 */
chdir('/www/zendsvr6/htdocs/Development/FNGForms/');
set_include_path(get_include_path().PATH_SEPARATOR.$config['applicationRoot'].'/plm/classes/'.PATH_SEPARATOR.$config['applicationRoot'].'/plm/');
//include_once("common.php");
include_once("classes/formrequest.class.php");
include_once("classes/vendorform.class.php");
include_once("classes/ProcessInstance.class.php");
include_once("classes/ProcessTransition.class.php");
include_once("classes/Alert.class.php");
include_once("classes/AlertNotification.php");
include_once("classes/Role.class.php");
include_once("classes/StateInstance.class.php");
require ('includes/c_config.php');
$RoleArray = new RoleArray();
$RoleArray->load();
$RoleArrayValues = $RoleArray->getArray();
unset($RoleArray);

//echo "<pre>"; print_r($config['email']); echo "</pre>";
//die();
function buildMessageBody($ProcessTransitionArray, $processInstance, $tmpFormRequest, $UserArray, $strRole) {
    global $config;
    $arrActions = array();
    foreach($ProcessTransitionArray as $ProcessTransition) {
        $arrActions[] = $ProcessTransition->getTransitionName();
    }
    $strActions = implode(", ", $arrActions);
    $arrUsers = array();
    foreach($UserArray->getArray() as $User) {
        $arrUsers[] = $User->getName();
    }
    $strUsers = implode(", ", $arrUsers);

    $strBody = "The <b><i>" . $tmpFormRequest->getProcessName() . "</i></b> is delayed.";
    $strBody .= "<br><br><br>";
    $strBody .= "Now you are able to take one of the following action(s):<br>";
    $strBody .= "<b><i>$strActions</i></b><br><br>";
    $strBody .= "These people also have an action to take during this next step in the workflow.<br>";
    $strBody .= "<b><i>$strRole: $strUsers</i></b><br><br>";
    $strBody .= "-----------------------------------------<br><br>";
    $strBody .= "<b><i><u>" . $tmpFormRequest->getProcessName() . " Workflow Action Details</u></i></b><br><br>";
    $strBody .= "<b>Vendor Maintenance - Action Required</b><br>";

    $requestArray = explode(':',$tmpFormRequest->getProcessName());
    if (count($requestArray) > 1) {
        $vendor = trim($requestArray[1]);
        $reason = trim($requestArray[2]);
    }
    $strRequestURL = $config['webURL']."/index.php?view=editForm&instanceID=".$processInstance->getProcessInstanceID();
    $strMyRequestsURL = $config['webURL']."/index.php";
    $strBody .= "Vendor: $vendor<br>";
    $strBody .= "Request Reason: $reason<br><br>";
    $strBody .= "<a href=\"".$strRequestURL."\">Take action on this request</a><br />";
    $strBody .= "<br />";
    $strBody .= "<a href=\"$strMyRequestsURL\">View all submitted or actionable requests</a><br />";
    return $strBody;
}
$formrequest = new formrequest();
$formrequest->select(null,array("WHERE"=>array("status"=>array("Active","New"))));
while($formrow = $formrequest->getnext()) {
//    if(!in_array(trim($formrow["status"]),array("Posted", "Rejected", "Deleted"))) {
//    error_log('CHECKING FORM #' . $formrow["formID"]);
    if($formrow["status"] == "New" and $formrow["strNextAction"] != "Post") {
        continue;
    }
        $Alert = new Alert();
        $Alert->load($formrow['formID']);
        if($Alert->getAlertNumber() >= 3) {
            continue;
        }
        $form_date = GetDateFromResult($formrow["stsTimeStamp"]);
        $alert_date = GetDateFromResult($Alert->getDTMSent() . ".000000","Y-m-d H:i:s");
        if($alert_date != null) {
            if($form_date > $alert_date) {
                $last_date = $form_date;
            } else {
                $last_date = $alert_date;
            }
        } else {
            $last_date = $form_date;
        }
        $since_start = $form_date->diff(new DateTime('NOW'));

        if(GetDifferenceInMinutes($last_date,new DateTime('NOW')) > $config['delayTime']) {
//            echo "[" . $formrow["formID"] . "] Greater than 5 hours<br><br>";
            $Alert->setFormID($formrow['formID']);
            $Alert->setAlertNumber(null);
            $Alert->setDTMSent(now());
        } else {
            continue;
        }
    $strBody = "";
    $strSubject = "";
//    error_log("Processing FORM #" . $formrow["formID"]);
        $tmpFormRequest = new formrequest();
        $tmpFormRequest->loadForWorkflow($formrow["processingInstanceID"]);

        $strSubject = "Next Step Delayed - " . $tmpFormRequest->getProcessName();
        $Alert->setSubject($strSubject);

        $processInstance = new ProcessInstance();
        $processInstance->loadForDisplay($formrow["processingInstanceID"], $tmpFormRequest);
        $Alert->setStateID($processInstance->getCurrentProcessStateID());

        $nextRole = base::getNextRole($processInstance);
//        $_REQUEST['processingInstanceID'] = $formrow["processingInstanceID"];
//        $_REQUEST['intNextRoleID'] = base::getNextRoleID($processInstance);
//        $_REQUEST['strNextRole'] = base::getNextRole($processInstance);
//        $_REQUEST['strNextAction'] = base::getNextAction($processInstance);
//        $tmpFormRequest->update();
//        echo "Updated Form with processingInstanceID: " . $formrow["processingInstanceID"] . "<br><br>";
        $roleArray = $processInstance->getRoleArray()->getArray();
        $usersInRole = "BI Team";
        if($nextRole != "BI Team") {
            foreach ($roleArray as $Role) {
                if ($nextRole == "CCC" && $Role->getRoleName() == "Director") {
                    $usersInRole = "Director";
                    break;
                } else if ($nextRole == "Tooling" && $Role->getRoleName() == "Tooling Accountant") {
                    $usersInRole = "Tooling Accountant";
                    break;
                }
            }
        }
//        echo "Send to $usersInRole<br>";
        foreach($roleArray as $Role) {
            if ($Role->getRoleName() == $usersInRole) {
                $UserArray = $Role->getUserArray();

                $strRole = $Role->getRoleName();
                $ProcessTransitionArray = $Role->getProcessTransitionArray()->getArray();

                $strBody = buildMessageBody($ProcessTransitionArray,$processInstance,$tmpFormRequest,$UserArray,$strRole);

                $Alert->setBody($strBody);
                $StateInstance = new StateInstance();
                $StateInstance->loadByProcessInstanceID($formrow["processingInstanceID"]);
                $Alert->setStateInstanceID($StateInstance->getStateInstanceID());
                $Alert->save();

                $AlertNotification = new AlertNotification($formrow['formID']);
                $AlertNotification->setAlertID($Alert->getAlertID());
                // Set alert class user array with UserArray from the matching Role
                $AlertNotification->setAlertNotificationArrayFromUserArray($UserArray);

//                echo "User Alert Array contains [" . count($AlertNotification->getAlertNotificationArray()->getArray()) . "] users";

                $AlertNotificationArray = $AlertNotification->getAlertNotificationArray()->getArray();
                foreach($AlertNotificationArray as $ANotification) {
                    $ANotification->save();
                }


                $AlertNotification->getAlertNotificationArray()->sendNotification();

                foreach($config['email'] as $key => $value) {
                    if(trim($value) != "") {
                        $AlertNotification->sendIndividualNotification($value);
                    }
                }
                break;
            }
//            echo "<br><br>" . $Role->getRoleName() . "<br>";
//            $ProcessTransitionArray = $Role->getProcessTransitionArray()->getArray();
//            foreach($ProcessTransitionArray as $ProcessTransition) {
//                echo $ProcessTransition->getTransitionName() . ' - ' . $ProcessTransition->getDisplayOrder() . '<br>';
//            }
        }
//    }
}
?>

