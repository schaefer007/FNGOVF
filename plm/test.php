<?php
/**
 * Created by PhpStorm.
 * User: chris
 * Date: 10/7/15
 * Time: 8:42 AM
 */

chdir('/www/zendsvr6/htdocs/Development/FNGForms/');
set_include_path(get_include_path().PATH_SEPARATOR.$config['applicationRoot'].'/plm/classes/'.PATH_SEPARATOR.$config['applicationRoot'].'/plm/');
//include_once("common.php");
include_once("classes/formrequest.class.php");
include_once("classes/vendorform.class.php");
include_once("classes/ProcessInstance.class.php");
include_once("classes/ProcessTransition.class.php");
include_once("classes/Alert.class.php");
include_once("classes/AlertNotification.php");
include_once("classes/Role.class.php");
include_once("classes/StateInstance.class.php");
require ('includes/c_config.php');

$StateInstance = new StateInstance();
if($StateInstance->loadByProcessInstanceID(1139)) {
    $StateInstance->complete();
    $StateInstance->save();
}
?>
<pre>
<?php print_r($StateInstance); ?>
</pre>
