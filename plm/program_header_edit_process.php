<?php
include_once("common.php");
include_once("reporting.php");
include_once("classes/ProgramCommon.class.php");
include_once("classes/Member.class.php");
include_once("classes/MemberRole.class.php");

include_once("authenticate.php");

// TODO: Remove bad characters from program name so that it doesn't crash from trying to create a bad folder name (for documents)

$intProgramID = isset($_POST["intProgramID"]) ? $_POST["intProgramID"] : null;
$blnIsChild = isset($_POST["blnIsChild"]) ? $_POST["blnIsChild"] : false;
$blnNewProgram = false;

// Load Program Data
$objProgram = $objTopLevelProduct = new Product(); // CODE CLEAN: objTopLevelProduct will make splitting of Program and Product easier in the end
$objProgram->setIsChild($blnIsChild);
$objProgram->load($intProgramID);
$objProgram->getProgramCommon(true);
$objProgram->getMemberArray()->loadByProgramID($objProgram->getProgramID());

$strOriginalProductName = $objProgram->getProgramName();
$intOriginalPlatformID = $objProgram->getProgramCommon()->getPlatformID();

// Members
if(isset($_POST["arrMembers"])) {
	foreach($_POST["arrMembers"] as $intMemberID => $arrMember) {
		$objMember = new Member($intMemberID);
		list($strKey, $objExistingMember) = $objProgram->getMemberArray()->getMemberByUserID($arrMember["intUserID"]);
		if($objExistingMember) {
			$objMember = $objExistingMember;
			$intMemberID = $strKey;
		} elseif(!$objMember->getMemberID()) {
			$objMember->loadByProgramIDAndUserID($objProgram->getProgramID(), $arrMember["intUserID"]);
		}

		if(isset($arrMember["arrMemberRoles"])) {
			foreach($arrMember["arrMemberRoles"] as $intMemberRoleID => $intRoleID) {
				$objMemberRole = new MemberRole($intMemberRoleID);
				list($strKey, $objExistingMemberRole) = $objMember->getMemberRoleArray()->getMemberRoleByRoleID($intRoleID);
				if($objExistingMemberRole) {
					$objMemberRole = $objExistingMemberRole;
					$intMemberRoleID = $strKey;
				} elseif(!$objMemberRole->getMemberRoleID()) {
					$objMemberRole->loadByMemberIDAndRoleID($objMember->getMemberID(), $intRoleID);
				}

				if($intRoleID) {
					$objMemberRole->setRoleID($intRoleID);
					$objMember->getMemberRoleArray()->setObject($intMemberRoleID, $objMemberRole);
				} elseif($objMemberRole->getMemberRoleID()) {
					$objMemberRole->delete();
				}
			}
		}

		if(isset($arrMember["blnDeleted"]) && $arrMember["blnDeleted"]) {
			$objMember->delete();
		} elseif($arrMember["intUserID"]) {
			$objMember->setUserID($arrMember["intUserID"]);
			$objProgram->getMemberArray()->setObject($intMemberID, $objMember);
		}
	}
}

// Members
if($objProgram->getMemberArray()->getArray()) {
	foreach($objProgram->getMemberArray()->getArray() as $objMember) {
		$objMember->setProgramID($objProgram->getProgramID());
		$objMember->save();
		if($objMember->getMemberRoleArray()->getArray()) {
			foreach($objMember->getMemberRoleArray()->getArray() as $objMemberRole) {
				$objMemberRole->setMemberID($objMember->getMemberID());
				$objMemberRole->save();
			}
		}
	}
}

triggerSuccess("Program members successfully saved.");
header("location:program_header_edit.php?intProgramID=".$objProgram->getProgramID());
?>