<?php
	include_once("common.php");
	include_once("classes/ActivityLog.class.php");
	ActivityLog::log("Logout");
	unset($_SESSION["objUser"]);
	unset($_SESSION["objSearch"]);
	unset($_SESSION["arrWorkingData"]);

	$strReferer = $_SERVER["HTTP_REFERER"]?$_SERVER["HTTP_REFERER"]:null;
	header("location:".strNOT_LOGGED_IN_PAGE."?strRequestURI=$strReferer");
?>