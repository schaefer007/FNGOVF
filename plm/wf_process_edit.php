<?php
	include_once("common.php");
	$arrCSS = array();
	$arrJS = array();
	include_once("smarty.php");
	include_once("header.php");
	include_once("classes/WFProcess.class.php");

	$intWFProcessID = isset($_POST["intWFProcessID"])?$_POST["intWFProcessID"]:null;
	$objWFProcess = new WFProcess();
	if($intWFProcessID) {
		$objWFProcess->loadForWFProcessEdit($intWFProcessID);
	//} else {
	}
	$g_objSmarty->assign("objWFProcess", $objWFProcess);

	$g_objSmarty->display("wf_process_edit.tpl");
?>
