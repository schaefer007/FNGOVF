<?php
	include_once("constants_allsites.php");

	// Cross PHP Version clone (works in 4 and 5)
	if (version_compare(phpversion(), '5.0') < 0) {
		eval('function clone($object) { return $object; }');
	}

	/* Error reporting */
	function triggerFeedback($strFeedbackType, $strMessage) {
		if(!isset($_SESSION["arrFeedback"])) {
			$_SESSION["arrFeedback"] = array();
		}
		if(!isset($_SESSION["arrFeedback"][$strFeedbackType])) $_SESSION["arrFeedback"][$strFeedbackType] = array();
		if(!in_array($strMessage, $_SESSION["arrFeedback"][$strFeedbackType])) {
			$_SESSION["arrFeedback"][$strFeedbackType][] = $strMessage;
		}
	}

	function triggerError($strMessage) {
		triggerFeedback("error", $strMessage);
	}

	function triggerWarning($strMessage) {
		triggerFeedback("warning", $strMessage);
	}

	function triggerSuccess($strMessage) {
		triggerFeedback("success", $strMessage);
	}

	function getMessages($strFeedbackType) {
		$strReturn = "";
		$arrFeedbackTypeMap = array("error" => "Error(s)", "warning" => "Warning(s)", "success" => "Confirmation(s)");
		$strStyle = "display:none;";
		if(isset($_SESSION["arrFeedback"][$strFeedbackType]))
			$strStyle = "display:block;";
		$strReturn .= "<div class=\"".$strFeedbackType."_messages messages\" style=\"$strStyle\" id=\"".$strFeedbackType."_message_main\">";
		if(isset($arrFeedbackTypeMap[$strFeedbackType])) {
			$strReturn .= "<div class=\"".$strFeedbackType."_messages_header\">".$arrFeedbackTypeMap[$strFeedbackType].":</div>";
		}
		$strReturn .= "<div id=\"".$strFeedbackType."_messages\">";
		if(isset($_SESSION["arrFeedback"][$strFeedbackType])) {
			foreach($_SESSION["arrFeedback"][$strFeedbackType] as $strMessage) {
				$strReturn .= "<div class=\"".$strFeedbackType."_message\">" . $strMessage . "</div>";
			}
		}
		$strReturn .= "</div>";
		$strReturn .= "</div>";
		return $strReturn;
	}

	function showFeedback() {
		echo "<div id=\"feedback\">";
		echo getMessages("error");
		echo getMessages("warning");
		echo getMessages("success");
		echo "</div>";
	}

	function clearFeedback() {
		unset($_SESSION["arrFeedback"]);
	}
	/* End Error Reporting */

	/* JSON */
	function toJSON($varValue) {
		switch(gettype($varValue)) {
		    case 'double':
		    case 'integer':
		      return $varValue;
		    case 'bool':
		      return $varValue?'true':'false';
		    case 'string':
		      return '"'.str_replace(array("\n","\r","\t","\\'"),array("\\n","\\r","\\t","'"),addslashes($varValue)).'"';
			  //return '\''.addslashes($varValue).'\'';
		    case 'NULL':
		      return 'null';
		    case 'object':
		      //return '\'Object '.addslashes(get_class($varValue)).'\'';
			  return toJSON(get_object_vars($varValue));
		    case 'array':
		      if (isVector($varValue))
		        return '['.implode(',', array_map('toJSON', $varValue)).']';
		      else {
		        $strResult = '{';
		        foreach ($varValue as $k=>$v) {
		          if ($strResult != '{') $strResult .= ',';
		          $strResult .= '"'.$k.'"'.':'.toJSON($v);
		        }
		        return $strResult.'}';
		      }
		    default:
		      return '\''.addslashes(gettype($varValue)).'\'';
		}
	}

	function isVector(&$arrArray) {
	  $intNext = 0;
	  foreach ($arrArray as $k=>$v) {
	    if ($k !== $intNext)
	      return false;
	    $intNext++;
	  }
	  return true;
	}
	/* End JSON */

	function printCSSFiles($arrParams) {
		if(!$arrParams)
			return;

		foreach($arrParams as $strFile) {
			include("css/".$strFile);
			echo "\r\n";
		}
	}

	function printJSFiles($arrParams) {
		if(!$arrParams)
			return;

		foreach($arrParams as $strFile) {
			include("javascript/".$strFile);
			echo "\r\n";
		}
	}

	function isNumber($strNumber){
		if($strNumber[0] == "$")
			$strNumber = substr($strNumber, 1);

		$arrThousands = explode(",", $strNumber);
		$intLastThousand = count($arrThousands) - 1;
		$blnCommaExists = count($arrThousands) > 1;
		$i = 0;
		foreach($arrThousands as $strThousand) {
			if($i == $intLastThousand) {
				$arrDecimal = explode(".", $strThousand);
				if((isset($arrDecimal[0]) && !is_numeric($arrDecimal[0])) || (isset($arrDecimal[1]) && !is_numeric($arrDecimal[1]))) {
					return false;
				}
				if($blnCommaExists && (strlen($strThousand) != 3 || !is_numeric($strThousand))) {
					return false;
				}
				if(count($arrDecimal) > 2 || (isset($arrDecimal[1]) && !is_numeric($arrDecimal[1]))) {
					return false;
				}
			} elseif($i == 0) {
				if(!is_numeric($strThousand)) {
					return false;
				}
			} elseif(strlen($strThousand) != 3 || !is_numeric($strThousand)) {
				return false;
			}
			$i++;
		}
		return true;
	}

	function unformatNumber($strNumber){
		if($strNumber[0] == "$")
			$strNumber = substr($strNumber, 1);

		$strNumber = str_replace(",", "", $strNumber);
		return $strNumber;
	}

	/* Has JS counterpart in common_allsites.js.php */
	function budgetNumberFormat($strNumber) {
		if(is_numeric($strNumber)) {
			$strAbsNumber = abs($strNumber);
			return ($strNumber < 0?"-":"")."$".number_format($strAbsNumber);
		}
		return "";
	}

	function piecePriceNumberFieldFormat($strNumber){
		if(is_numeric($strNumber)) {
			return number_format($strNumber, 4);
		}
		return "";
	}
	function piecePriceNumberFormat($strNumber){
		if(is_numeric($strNumber)) {
			return ($strNumber < 0?"-":"")."$".piecePriceNumberFieldFormat(abs($strNumber));
		}
		return "";
	}

	function volumeNumberFieldFormat($strNumber){
		return volumeNumberFormat($strNumber);
	}
	function volumeNumberFormat($strNumber){
		if(trim($strNumber) === "")
			return "";
		return number_format($strNumber);
	}

	function quantityNumberFormat($strNumber) {
		if(strpos($strNumber, ".") !== false) $strNumber = rtrim($strNumber, "0");
		return rtrim($strNumber,".");
	}

	function capacityHoursFormat($dblCapacityHours) {
		return round($dblCapacityHours, 1);
	}
	function percentUtilizationFormat($dblPercentUtilization) {
		return round($dblPercentUtilization, 1);
	}

	function timeTrackingHourFormat($dblHours) {
		return round($dblHours, 1);
	}

	// TODO: Replace with a solution that check if a number really is a number

	function cleanNumber($strNumber){
		if($strNumber === null || $strNumber === false)
			return $strNumber;
		return trim(str_replace(array(",","$"), array("",""), $strNumber));
	}

	function formatModelYear($dblModelYear) {
		if(!$dblModelYear || $dblModelYear == 0)
			return "";
		return round($dblModelYear, 1);
	}

	/* Date and time */
	function formatDate($dtmDate, $blnDateOnly=false) {
		if($dtmDate == "0000-00-00" || $dtmDate == "0000-00-00 00:00:00" || !$dtmDate)
			return "";

		$intDatePosted = strtotime($dtmDate);
		$strFormat = "d-M-y g:iA";
		if($blnDateOnly) {
			$strFormat = "d-M-y";
		}
		$strDate = date($strFormat, $intDatePosted);
		return $strDate;
	}

	function dateFieldFormat($dtmDate) {
		if($dtmDate == "0000-00-00" || $dtmDate == "0000-00-00 00:00:00" || !$dtmDate)
			return "";
		$dtmDate = date("m/d/Y", strtotime($dtmDate));
		//if(!$blnIncludeTime)
		//	$dtmDate = substr($dtmDate, 0, 10);
		return $dtmDate;
	}

	function formatDateForDB($dtmDate) {
		$dtmDate = strtotime($dtmDate);
		if($dtmDate) {
			return date("Y-m-d H:i:s", $dtmDate);
		}
		return null;
	}

	function formatDateForExcel($dtmDate) {
		$dtmDate = strtotime($dtmDate);
		if($dtmDate) {
			return date("m/d/Y", $dtmDate);
		}
		return null;
	}

	function now(){
		return date("Y-m-d G:i:s");
	}
	/* End Date and time */

	/* Photo */
	function upload($strFormName, $strFileName, $strFolder) {
		if(isset($_FILES[$strFormName]["tmp_name"]) && $_FILES[$strFormName]["tmp_name"]) {
			$strTempName = $_FILES[$strFormName]["tmp_name"];
			$strName = $_FILES[$strFormName]["name"];
			$strExtension = substr($strName, strrpos($strName, ".")+1);
			$strURL = $strFileName.".".$strExtension;
			$strFullFileName = $strFolder.$strURL;

			$blnMoveSuccess = move_uploaded_file($strTempName, $strFullFileName);
			if($blnMoveSuccess) {
				return $strExtension;
			}
		}
		return false;
	}
	/* End Photo */

	function getFilesFromFolder($strDirectory) {
		$arrFiles = scandir($strDirectory);
		if(!$arrFiles)
			return;

		$i = 0;
		$arrFiles2 = array();
		foreach($arrFiles as $strFile) {
			if ($strFile == "." || $strFile == "..") continue;

			if(is_dir($strDirectory."/".$strFile)) {
				$arrFiles2[$i]["Type"] = "Directory";

				$arrFiles2[$i]["Folder"] = $strDirectory."/".$strFile;
			} else {
				$arrFiles2[$i]["Type"] = "File";
			}
			$arrFiles2[$i]["Name"] = $strFile;
			$i++;
		}

		return $arrFiles2;
	}

	function getFolders($strDirectory = null) {
		if ($strDirectory == null) {
			$strDirectory = getcwd();
		}

		$arrReturn = array();

		if (is_dir($strDirectory)) {
			foreach(scandir($strDirectory) as $strFile) {
				if ($strFile == "." || $strFile == "..") continue;

				if (is_dir($strDirectory."/".$strFile))
					$arrReturn[$strFile] = getFolders($strDirectory."/".$strFile);
				else
					$arrReturn["arrFiles"][] = $strFile;
			}
		} else {
			$arrReturn[] = $strDirectory;
		}

		return $arrReturn;
	}

	function getExtension($strFileName){
		return substr($strFileName, strrpos($strFileName, ".")+1);
	}

	// DEPRECATED: Use Array->getAllFields()
	// TODO: Remove this, replace with ArrayClass->getAllFields()
	function getArrayFromObjectField($objObjectArray, $strFieldFunction, $strKeyFunction=null) {
		$arrArray = array();
		if($objObjectArray && $objObjectArray->getArray()) {
			foreach($objObjectArray->getArray() as $objObject) {
				$strFieldValue = $objObject->$strFieldFunction();
				if($strKeyFunction) {
					$strKey = $objObject->$strKeyFunction();
					$arrArray[$strKey] = $strFieldValue;
				} else {
					$arrArray[] = $strFieldValue; // Value aren't unique, is this a problem?
				}
			}
		}
		return $arrArray;
	}

	function explodeCSVLine($strLine) {
	$intCount = strlen($strLine);
	$strWord = "";
	$blnInQuotes = false;
	$j = 0;
	for($i=0;$i<$intCount;$i++) {
		if($strLine[$i] == "\"") {
			if(!$blnInQuotes) {
				$blnInQuotes = true;
				$i++;
			} elseif($blnInQuotes && (!isset($strLine[$i+1]) || $strLine[$i+1] != "\"")) {
				$blnInQuotes = false;
				$i++;
			}
		}

		if($strLine[$i] == "," && !$blnInQuotes) {
			$arrLines[$j++] = $strWord;
			$strWord = "";
		} else {
			$strWord .= $strLine[$i];
		}
	}
	$arrLines[$j] = $strWord;
	return $arrLines;
}

	function firstSunday($intYear=null) {
		if(!$intYear) {
			$intYear = date("Y");
		}

		$intDate = strtotime($intYear."-01-01");
		while(date("D", $intDate) != "Sun") {
			$intDate = strtotime(date("Y-m-d", $intDate) . " +1 day");
		}
		return $intDate;
	}
	function previousSunday($dtmDate=null) {
		if(!$dtmDate) {
			$dtmDate = date("Y-m-d");
		}

		$intDate = strtotime($dtmDate);
		while(date("D", $intDate) != "Sun") {
			$intDate = strtotime(date("Y-m-d", $intDate) . " -1 day");
		}
		return $intDate;
	}
?>