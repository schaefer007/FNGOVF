<?php
require_once("ArrayClass.class.php");

class ActivityLogArray extends ArrayClass {
	function __construct(){
	}

	function load() {
		$strSQL = " SELECT * FROM dbSystem.tblActivityLog";
		$rsResult = $this->getDB()->query($strSQL);
		while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
			$this->_arrObjects[$arrRow["intActivityLogID"]] = new ActivityLog();
			$this->_arrObjects[$arrRow["intActivityLogID"]]->setVarsFromRow($arrRow);
		}
	}
}

require_once("DataClass.class.php");

class ActivityLogBase extends DataClass {
	protected $_intActivityLogID;
	protected $_intUserID;
	protected $_strAction;
	protected $_txtDetails;
	protected $_dtmCreatedOn;

	function __construct($intActivityLogID=null) {
		$this->DataClass();
		if($intActivityLogID) {
			$this->load($intActivityLogID);
		}
	}

	protected function insert() {
		base::write_log("Activity log created","S");
		$strSQL = "INSERT INTO dbSystem.tblActivityLog SET ";
		$strConnector = "";
		$strSQL .= $strConnector . "intActivityLogID = ".$this->getDB()->sanitize(self::getActivityLogID());
		$strConnector = ",";
		if(isset($this->_intUserID)) {
			$strSQL .= $strConnector . "intUserID = ".$this->getDB()->sanitize(self::getUserID());
			$strConnector = ",";
		}
		if(isset($this->_strAction)) {
			$strSQL .= $strConnector . "strAction = ".$this->getDB()->sanitize(self::getAction());
			$strConnector = ",";
		}
		if(isset($this->_txtDetails)) {
			$strSQL .= $strConnector . "txtDetails = ".$this->getDB()->sanitize(self::getDetails());
			$strConnector = ",";
		}
		if(isset($this->_dtmCreatedOn)) {
			$strSQL .= $strConnector . "dtmCreatedOn = ".$this->getDB()->sanitize(self::getCreatedOn());
			$strConnector = ",";
		}
		//echo $strSQL;
		$this->getDB()->query($strSQL);
		$this->setActivityLogID($this->getDB()->insert_id());
		return $this->getActivityLogID();
	}

	protected function update() {
		base::write_log("Activity log update","S");
		$strSQL = "UPDATE dbSystem.tblActivityLog SET ";
		$strConnector = "";
		$strSQL .= $strConnector . "intActivityLogID = ".$this->getDB()->sanitize(self::getActivityLogID());
		$strConnector = ",";
		if(isset($this->_intUserID)) {
			$strSQL .= $strConnector . "intUserID = ".$this->getDB()->sanitize(self::getUserID());
			$strConnector = ",";
		}
		if(isset($this->_strAction)) {
			$strSQL .= $strConnector . "strAction = ".$this->getDB()->sanitize(self::getAction());
			$strConnector = ",";
		}
		if(isset($this->_txtDetails)) {
			$strSQL .= $strConnector . "txtDetails = ".$this->getDB()->sanitize(self::getDetails());
			$strConnector = ",";
		}
		if(isset($this->_dtmCreatedOn)) {
			$strSQL .= $strConnector . "dtmCreatedOn = ".$this->getDB()->sanitize(self::getCreatedOn());
			$strConnector = ",";
		}
		$strSQL .= " WHERE intActivityLogID = ".$this->getDB()->sanitize(self::getActivityLogID())."";
		//echo $strSQL;
		return $this->getDB()->query($strSQL);
	}

	public function save() {
		if($this->_intActivityLogID) {
			return $this->update();
		} else {
			return $this->insert();
		}
	}

	public function delete() {
		if($this->_intActivityLogID) {
			$strSQL = "DELETE FROM dbSystem.tblActivityLog
				WHERE intActivityLogID = '$this->_intActivityLogID'
				";
			return $this->getDB()->query($strSQL);
		}
	}

	public function load($intActivityLogID) {
		if(!$intActivityLogID) {
			return false;
		}

		$strSQL = "SELECT *
				FROM dbSystem.tblActivityLog
				WHERE intActivityLogID = '$intActivityLogID'
				LIMIT 1
			";
		$rsActivityLog = $this->getDB()->query($strSQL);
		$arrActivityLog = $this->getDB()->fetch_assoc($rsActivityLog);
		$this->setVarsFromRow($arrActivityLog);
	}

	function setVarsFromRow($arrActivityLog) {
		if(isset($arrActivityLog["intActivityLogID"])) $this->_intActivityLogID = $arrActivityLog["intActivityLogID"];
		if(isset($arrActivityLog["intUserID"])) $this->_intUserID = $arrActivityLog["intUserID"];
		if(isset($arrActivityLog["strAction"])) $this->_strAction = $arrActivityLog["strAction"];
		if(isset($arrActivityLog["txtDetails"])) $this->_txtDetails = $arrActivityLog["txtDetails"];
		if(isset($arrActivityLog["dtmCreatedOn"])) $this->_dtmCreatedOn = $arrActivityLog["dtmCreatedOn"];
	}

	function getActivityLogID() {
		return $this->_intActivityLogID;
	}
	function setActivityLogID($value) {
		if($this->_intActivityLogID !== $value) {
			$this->_intActivityLogID = $value;
			$this->_blnDirty = true;
		}
	}

	function getUserID() {
		return $this->_intUserID;
	}
	function setUserID($value) {
		if($this->_intUserID !== $value) {
			$this->_intUserID = $value;
			$this->_blnDirty = true;
		}
	}

	function getAction() {
		return $this->_strAction;
	}
	function setAction($value) {
		if($this->_strAction !== $value) {
			$this->_strAction = $value;
			$this->_blnDirty = true;
		}
	}

	function getDetails() {
		return $this->_txtDetails;
	}
	function setDetails($value) {
		if($this->_txtDetails !== $value) {
			$this->_txtDetails = $value;
			$this->_blnDirty = true;
		}
	}

	function getCreatedOn() {
		return $this->_dtmCreatedOn;
	}
	function setCreatedOn($value) {
		if($this->_dtmCreatedOn !== $value) {
			$this->_dtmCreatedOn = $value;
			$this->_blnDirty = true;
		}
	}

}

class ActivityLog extends ActivityLogBase {
	function __construct($intActivityLogID=null) {
		parent::__construct($intActivityLogID);
	}

	static function log($strAction, $txtDetails=null, $intUserID=null) {
		if(!$intUserID && isset($_SESSION["objUser"]))
			$intUserID = $_SESSION["objUser"]->getUserID();

		if(!$intUserID)
			return false;
		base::write_log("Activity log created","S");
		$strSQL = "INSERT INTO dbSystem.tblActivityLog SET
			intUserID = ".self::getDB()->sanitize($intUserID).",
			strAction = ".self::getDB()->sanitize($strAction).",
			txtDetails = ".self::getDB()->sanitize($txtDetails).",
			dtmCreatedOn = ".self::getDB()->sanitize(now());
		return self::getDB()->query($strSQL);
	}
}
?>
