<?php
/**
 * Created by PhpStorm.
 * User: chris
 * Date: 10/7/15
 * Time: 8:01 AM
 */

require_once("ArrayClass.class.php");

class StateInstanceArray extends ArrayClass {
    function __construct(){
    }

    function load() {
        $strSQL = " SELECT * FROM dbSystem.tblStateInstance";
        $rsResult = $this->getDB()->query($strSQL);
        while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
            $this->_arrObjects[$arrRow["intStateInstanceID"]] = new StateInstance();
            $this->_arrObjects[$arrRow["intStateInstanceID"]]->setVarsFromRow($arrRow);
        }
    }
}

require_once("DataClass.class.php");

class StateInstanceBase extends DataClass {
    protected $_intStateInstanceID;
    protected $_intProcessInstanceID;
    protected $_intStateID;
    protected $_dtmStateBegan;
    protected $_dtmStateComplete;
    protected $_strStatus;

    function __construct($intStateInstanceID=null) {
        $this->DataClass();
        if($intStateInstanceID) {
            $this->load($intStateInstanceID);
        }
    }

    protected function insert() {
        $this->setStatus("started");
        base::write_log("State Instance Created","S");
        $strSQL = "INSERT INTO dbSystem.tblStateInstance SET ";
        $strConnector = "";
        $strSQL .= $strConnector . "intStateInstanceID = ".$this->getDB()->sanitize(self::getStateInstanceID());
        $strConnector = ",";
        if(isset($this->_intProcessInstanceID)) {
            $strSQL .= $strConnector . "intProcessInstanceID = ".$this->getDB()->sanitize(self::getProcessInstanceID());
            $strConnector = ",";
        }
        if(isset($this->_intStateID)) {
            $strSQL .= $strConnector . "intStateID = ".$this->getDB()->sanitize(self::getStateID());
            $strConnector = ",";
        }
//        if(isset($this->_dtmStateBegan)) {
            $strSQL .= $strConnector . "dtmStateBegan = ".$this->getDB()->sanitize(date("Y-m-d H:i:s"));
            $strConnector = ",";
//        }
//        if(isset($this->_dtmStateComplete)) {
            $strSQL .= $strConnector . "dtmStateComplete = ".$this->getDB()->sanitize(date("Y-m-d H:i:s"));
            $strConnector = ",";
//        }
        if(isset($this->_strStatus)) {
            $strSQL .= $strConnector . "strStatus = ".$this->getDB()->sanitize(self::getStatus());
            $strConnector = ",";
        }
        $this->getDB()->query($strSQL);
        $this->setStateInstanceID($this->getDB()->insert_id());
        return $this->getStateInstanceID();
    }

    protected function update() {
        base::write_log("State Instance Completed","S");
        $strSQL = "UPDATE dbSystem.tblStateInstance SET ";
        $strConnector = "";
        $strSQL .= $strConnector . "intStateInstanceID = ".$this->getDB()->sanitize(self::getStateInstanceID());
        $strConnector = ",";
        if(isset($this->_intProcessInstanceID)) {
            $strSQL .= $strConnector . "intProcessInstanceID = ".$this->getDB()->sanitize(self::getProcessInstanceID());
            $strConnector = ",";
        }
        if(isset($this->_intStateID)) {
            $strSQL .= $strConnector . "intStateID = ".$this->getDB()->sanitize(self::getStateID());
            $strConnector = ",";
        }
        if(isset($this->_dtmStateBegan)) {
            $strSQL .= $strConnector . "dtmStateBegan = ".$this->getDB()->sanitize(self::getStateBegan());
            $strConnector = ",";
        }
        if(isset($this->_dtmStateComplete)) {
            $strSQL .= $strConnector . "dtmStateComplete = ".$this->getDB()->sanitize(self::getStateComplete());
            $strConnector = ",";
        }
        if(isset($this->_strStatus)) {
            $strSQL .= $strConnector . "strStatus = ".$this->getDB()->sanitize(self::getStatus());
            $strConnector = ",";
        }
        $strSQL .= " WHERE intStateInstanceID = ".$this->getDB()->sanitize(self::getStateInstanceID())."";
        return $this->getDB()->query($strSQL);
    }

    public function save() {
        if($this->_intStateInstanceID) {
            return $this->update();
        } else {
            return $this->insert();
        }
    }

    public function delete() {
        if($this->_intStateInstanceID) {
            $strSQL = "DELETE FROM dbSystem.tblStateInstance
				WHERE intStateInstanceID = '$this->_intStateInstanceID'
				";
            return $this->getDB()->query($strSQL);
        }
    }

    public function load($intStateInstanceID) {
        if(!$intStateInstanceID) {
            return false;
        }
        $strSQL = "SELECT *
				FROM dbSystem.tblStateInstance
				WHERE intStateInstanceID = '$intStateInstanceID'
			";
        $rsStateInstance = $this->getDB()->query($strSQL);
        $arrStateInstance = $this->getDB()->fetch_assoc($rsStateInstance);
        $this->setVarsFromRow($arrStateInstance);
    }

    function setVarsFromRow($arrStateInstance) {
        parent::setVarsFromRow($arrStateInstance);
        if(isset($arrStateInstance["intStateInstanceID"])) $this->_intStateInstanceID = $arrStateInstance["intStateInstanceID"];
        if(isset($arrStateInstance["intProcessInstanceID"])) $this->_intProcessInstanceID = $arrStateInstance["intProcessInstanceID"];
        if(isset($arrStateInstance["intStateID"])) $this->_intStateID = $arrStateInstance["intStateID"];
        if(isset($arrStateInstance["dtmStateBegan"])) $this->_dtmStateBegan = $arrStateInstance["dtmStateBegan"];
        if(isset($arrStateInstance["dtmStateComplete"])) $this->_dtmStateComplete = $arrStateInstance["dtmStateComplete"];
        if(isset($arrStateInstance["strStatus"])) $this->_strStatus = $arrStateInstance["strStatus"];
    }

    function getStateInstanceID() {
        return $this->_intStateInstanceID;
    }
    function setStateInstanceID($value) {
        if($this->_intStateInstanceID !== $value) {
            $this->_intStateInstanceID = $value;
            $this->_blnDirty = true;
        }
    }

    function getProcessInstanceID() {
        return $this->_intProcessInstanceID;
    }
    function setProcessInstanceID($value) {
        if($this->_intProcessInstanceID !== $value) {
            $this->_intProcessInstanceID = $value;
            $this->_blnDirty = true;
        }
    }

    function getStateID() {
        return $this->_intStateID;
    }
    function setStateID($value) {
        if($this->_intStateID !== $value) {
            $this->_intStateID = $value;
            $this->_blnDirty = true;
        }
    }

    function getStateBegan() {
        return $this->_dtmStateBegan;
    }
    function setStateBegan($value) {
        if($this->_dtmStateBegan !== $value) {
            $this->_dtmStateBegan = $value;
            $this->_blnDirty = true;
        }
    }

    function getStateComplete() {
        return $this->_dtmStateComplete;
    }
    function setStateComplete($value) {
        if($this->_dtmStateComplete !== $value) {
            $this->_dtmStateComplete = $value;
            $this->_blnDirty = true;
        }
    }

    function getStatus() {
        return $this->_strStatus;
    }
    function setStatus($value) {
        if($this->_strStatus !== $value) {
            $this->_strStatus = $value;
            $this->_blnDirty = true;
        }
    }
}

class StateInstance extends StateInstanceBase {
    function complete() {
        $this->setStateComplete(date("Y-m-d H:i:s"));
        $this->setStatus("complete");
    }

    function loadByFormID($formID) {
        $formrequest = new formrequest();
        $formrequest->select(null,array("WHERE"=>array("formID"=>$formID)));
        if($formrow = $formrequest->getnext()) {
            $this->setProcessInstanceID($formrow["processingInstanceID"]);
            $this->loadByProcessInstanceID($this->getProcessInstanceID());
        }
    }

    function loadByProcessInstanceID($processInstanceID) {
        if(!$processInstanceID) {
            return false;
        }
        $strSQL = "SELECT *
				FROM dbSystem.tblStateInstance
				WHERE intProcessInstanceID = '$processInstanceID'
				ORDER BY intStateInstanceID DESC
				LIMIT 1
			";
        $rsStateInstance = $this->getDB()->query($strSQL);
        $arrStateInstance = $this->getDB()->fetch_assoc($rsStateInstance);
        $this->setVarsFromRow($arrStateInstance);
        return isset($arrStateInstance["strStatus"]);
    }
}
?>