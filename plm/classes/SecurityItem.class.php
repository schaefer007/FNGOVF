<?php
	require_once("ArrayClass.class.php");
	include_once("Permission.class.php");

	class SecurityItemArray extends ArrayClass {
		function __construct(){
		}

		function load() {
			$strSQL = " SELECT *
				FROM dbPLM.tblSecurityItem
				ORDER BY strSecurityItem";
			//echo $strSQL;
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				$this->_arrObjects[$arrRow["intSecurityItemID"]] = new SecurityItem();
				$this->_arrObjects[$arrRow["intSecurityItemID"]]->setVarsFromRow($arrRow);
			}
		}

		function loadForSecurityItemManagement() {
			$objSearch = $_SESSION["objSearch"];

			$strSQL = " SELECT tblOperation.*, tblPermission.*, tblSecurityItem.*
				FROM dbPLM.tblSecurityItem
				LEFT JOIN dbPLM.tblPermission
					ON tblSecurityItem.intSecurityItemID = tblPermission.intSecurityItemID
				LEFT JOIN dbPLM.tblOperation
					ON tblOperation.intOperationID = tblPermission.intOperationID";
			$strSQL .= $objSearch->getSecurityItemListPage()->getSortQuery();
			//echo $strSQL;
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				if(!isset($this->_arrObjects[$arrRow["intSecurityItemID"]])) {
					$this->_arrObjects[$arrRow["intSecurityItemID"]] = new SecurityItem();
					$this->_arrObjects[$arrRow["intSecurityItemID"]]->setVarsFromRow($arrRow);
				}
				$objPermission = new Permission();
				$objPermission->setVarsFromRow($arrRow);
				$objPermission->getOperation()->setVarsFromRow($arrRow);
				$this->_arrObjects[$arrRow["intSecurityItemID"]]->getPermissionArray()->setObject($arrRow["intPermissionID"], $objPermission);
			}
		}

		function loadForPermissionManagement() {
			$objSearch = $_SESSION["objSearch"];
			$strSQL = " SELECT tblSecurityItem.*, tblRolePermission.*, tblPermission.*
				FROM dbPLM.tblPermission
				INNER JOIN dbPLM.tblSecurityItem
					ON tblPermission.intSecurityItemID = tblSecurityItem.intSecurityItemID
				LEFT JOIN dbPLM.tblRolePermission
					ON tblPermission.intPermissionID = tblRolePermission.intPermissionID
					AND tblRolePermission.intRoleID = ".$this->getDB()->sanitize($objSearch->getRoleID());
			$strSQL .= $objSearch->getPermissionListPage()->getSortQuery();
			//echo $strSQL;
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				if(!isset($this->_arrObjects[$arrRow["intSecurityItemID"]])) {
					$this->_arrObjects[$arrRow["intSecurityItemID"]] = new SecurityItem();
					$this->_arrObjects[$arrRow["intSecurityItemID"]]->setVarsFromRow($arrRow);
				}
				$objPermission = new Permission();
				$objPermission->setVarsFromRow($arrRow);
				$objPermission->getRolePermission()->setVarsFromRow($arrRow);
				$this->_arrObjects[$arrRow["intSecurityItemID"]]->getPermissionArray()->setObject($arrRow["intPermissionID"], $objPermission);
			}
		}
	}

	require_once("DataClass.class.php");

	class SecurityItemBase extends DataClass {
		protected $_intSecurityItemID;
		protected $_strSecurityItem;
		protected $_intCreatedByUserID;
		protected $_dtmCreatedOn;

		function __construct($intSecurityItemID=null) {
			$this->DataClass();
			if($intSecurityItemID) {
				$this->load($intSecurityItemID);
			}
		}

		protected function insert() {
			base::write_log("Security Item created","S");
			$strSQL = "INSERT INTO dbPLM.tblSecurityItem SET ";
			$strConnector = "";
			$strSQL .= $strConnector . "intSecurityItemID = ".$this->getDB()->sanitize(self::getSecurityItemID());
			$strConnector = ",";
			if(isset($this->_strSecurityItem)) {
				$strSQL .= $strConnector . "strSecurityItem = ".$this->getDB()->sanitize(self::getSecurityItem());
				$strConnector = ",";
			}
			if(isset($this->_intCreatedByUserID)) {
				$strSQL .= $strConnector . "intCreatedByUserID = ".$this->getDB()->sanitize(self::getCreatedByUserID());
				$strConnector = ",";
			}
			if(isset($this->_dtmCreatedOn)) {
				$strSQL .= $strConnector . "dtmCreatedOn = ".$this->getDB()->sanitize(self::getCreatedOn());
				$strConnector = ",";
			}
			//echo $strSQL;
			$this->getDB()->query($strSQL);
			$this->setSecurityItemID($this->getDB()->insert_id());
			return $this->getSecurityItemID();
		}

		protected function update() {
			base::write_log("Security Item Update","S");
			$strSQL = "UPDATE dbPLM.tblSecurityItem SET ";
			$strConnector = "";
			$strSQL .= $strConnector . "intSecurityItemID = ".$this->getDB()->sanitize(self::getSecurityItemID());
			$strConnector = ",";
			if(isset($this->_strSecurityItem)) {
				$strSQL .= $strConnector . "strSecurityItem = ".$this->getDB()->sanitize(self::getSecurityItem());
				$strConnector = ",";
			}
			if(isset($this->_intCreatedByUserID)) {
				$strSQL .= $strConnector . "intCreatedByUserID = ".$this->getDB()->sanitize(self::getCreatedByUserID());
				$strConnector = ",";
			}
			if(isset($this->_dtmCreatedOn)) {
				$strSQL .= $strConnector . "dtmCreatedOn = ".$this->getDB()->sanitize(self::getCreatedOn());
				$strConnector = ",";
			}
			$strSQL .= " WHERE intSecurityItemID = ".$this->getDB()->sanitize(self::getSecurityItemID())."";
			//echo $strSQL;
			return $this->getDB()->query($strSQL);
		}

		public function save() {
			if($this->_intSecurityItemID) {
				return $this->update();
			} else {
				return $this->insert();
			}
		}

		public function delete() {
			if($this->_intSecurityItemID) {
				base::write_log("Security Item Delete","S");
				$strSQL = "DELETE FROM dbPLM.tblSecurityItem
				WHERE intSecurityItemID = '$this->_intSecurityItemID'
				";
				return $this->getDB()->query($strSQL);
			}
		}

		public function load($intSecurityItemID) {
			if(!$intSecurityItemID) {
				return false;
			}

			$strSQL = "SELECT *
				FROM dbPLM.tblSecurityItem
				WHERE intSecurityItemID = '$intSecurityItemID'
				LIMIT 1
			";
			$rsSecurityItem = $this->getDB()->query($strSQL);
			$arrSecurityItem = $this->getDB()->fetch_assoc($rsSecurityItem);
			$this->setVarsFromRow($arrSecurityItem);
		}

		function setVarsFromRow($arrSecurityItem) {
			if(isset($arrSecurityItem["intSecurityItemID"])) $this->_intSecurityItemID = $arrSecurityItem["intSecurityItemID"];
			if(isset($arrSecurityItem["strSecurityItem"])) $this->_strSecurityItem = $arrSecurityItem["strSecurityItem"];
			if(isset($arrSecurityItem["intCreatedByUserID"])) $this->_intCreatedByUserID = $arrSecurityItem["intCreatedByUserID"];
			if(isset($arrSecurityItem["dtmCreatedOn"])) $this->_dtmCreatedOn = $arrSecurityItem["dtmCreatedOn"];
		}

		function getSecurityItemID() {
			return $this->_intSecurityItemID;
		}
		function setSecurityItemID($value) {
			if($this->_intSecurityItemID !== $value) {
				$this->_intSecurityItemID = $value;
				$this->_blnDirty = true;
			}
		}

		function getSecurityItem() {
			return $this->_strSecurityItem;
		}
		function setSecurityItem($value) {
			if($this->_strSecurityItem !== $value) {
				$this->_strSecurityItem = $value;
				$this->_blnDirty = true;
			}
		}

		function getCreatedByUserID() {
			return $this->_intCreatedByUserID;
		}
		function setCreatedByUserID($value) {
			if($this->_intCreatedByUserID !== $value) {
				$this->_intCreatedByUserID = $value;
				$this->_blnDirty = true;
			}
		}

		function getCreatedOn() {
			return $this->_dtmCreatedOn;
		}
		function setCreatedOn($value) {
			if($this->_dtmCreatedOn !== $value) {
				$this->_dtmCreatedOn = $value;
				$this->_blnDirty = true;
			}
		}

	}

	include_once("Permission.class.php");

	class SecurityItem extends SecurityItemBase {
		private $_objPermissionArray;

		function __construct($intSecurityItemID=null) {
			parent::__construct($intSecurityItemID);
		}

		function loadBySecurityItem($strSecurityItem){
			if(!$strSecurityItem) {
				return false;
			}

			$strSQL = "SELECT *
				FROM dbPLM.tblSecurityItem
				WHERE strSecurityItem = '".$this->getDB()->sanitize($strSecurityItem)."
				LIMIT 1
			";
			$rsSecurityItem = $this->getDB()->query($strSQL);
			$arrSecurityItem = $this->getDB()->fetch_assoc($rsSecurityItem);
			$this->setVarsFromRow($arrSecurityItem);
		}

		function getPermissionArray(){
			if(!isset($this->_objPermissionArray)) {
				$this->_objPermissionArray = new PermissionArray();
			}
			return $this->_objPermissionArray;
		}

		function getID() {
			return $this->getSecurityItemID();
		}
		function getName() {
			return $this->getSecurityItem();
		}
	}
?>
