<?php

require_once("ArrayClass.class.php");
class ProcessInstanceArray extends ArrayClass {
	function __construct(){
		parent::__construct("ProcessInstance");
	}

	function load() {
		$strSQL = " SELECT * FROM dbSystem.tblProcessInstance";
		$rsResult = $this->getDB()->query($strSQL);
		while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
			$this->_arrObjects[$arrRow["intProcessInstanceID"]] = new ProcessInstance();
			$this->_arrObjects[$arrRow["intProcessInstanceID"]]->setVarsFromRow($arrRow);
		}
	}

	function loadForUserPage($intUserID) {
		if(!$intUserID)
			return;

		$strSQL = "SELECT tblProduct.*, tblPlatform.*, tblProgramCommon.*, tblEngineeringChange.*, tblWFProcess.*, tblProcessInstance.*
			FROM dbSystem.tblProcessInstance
			INNER JOIN dbSystem.tblWFProcess
				ON tblWFProcess.intWFProcessID = tblProcessInstance.intWFProcessID
			INNER JOIN dbSystem.tblProcessState
				ON tblProcessState.intProcessStateID = tblProcessInstance.intCurrentProcessStateID
			INNER JOIN dbSystem.tblProcessTransition
				ON tblProcessTransition.intFromProcessStateID = tblProcessState.intProcessStateID
			INNER JOIN (
				SELECT *
					FROM dbSystem.tblCondition
					INNER JOIN dbSystem.tblConditionXR
						ON tblCondition.intConditionID = tblConditionXR.intChildConditionID
				) AS tblCondition
				ON tblCondition.intTopLevelConditionID = tblProcessTransition.intConditionID
			INNER JOIN dbEngineeringChange.tblEngineeringChange
				ON tblEngineeringChange.intProcessInstanceID = tblProcessInstance.intProcessInstanceID
			INNER JOIN dbPLM.tblProgramCommon
				ON tblProgramCommon.intProgramCommonID = tblEngineeringChange.intProgramCommonID
			INNER JOIN dbPLM.tblPlatform
				ON tblPlatform.intPlatformID = tblProgramCommon.intPlatformID
			INNER JOIN dbPLM.tblProduct
				ON tblProduct.intProgramID = tblProgramCommon.intTopLevelProductID
			INNER JOIN dbPLM.tblMember
				ON tblMember.intProgramID = tblProduct.intProgramID
				AND tblMember.intUserID = ".self::getDB()->sanitize($intUserID)."
			INNER JOIN dbPLM.tblMemberRole
				ON tblMemberRole.intMemberID = tblMember.intMemberID
				AND tblMemberRole.intRoleID = tblCondition.intRoleID
			LEFT JOIN dbSystem.tblProcessTransitionInstance
				ON tblProcessTransitionInstance.intProcessInstanceID = tblProcessInstance.intProcessInstanceID
				AND tblProcessTransitionInstance.dtmTransitionCompleted IS NULL
			LEFT JOIN dbSystem.tblConditionInstance
				ON tblConditionInstance.intProcessTransitionInstanceID = tblProcessTransitionInstance.intProcessTransitionInstanceID
				AND tblConditionInstance.intUserID = tblMember.intUserID
			WHERE intConditionInstanceID IS NULL
			"; // Remove waiting action if you've taken any action (condition instance) on any process transitions of the process instance that is incomplete... this feels sketchy, but I think it is sound
		//echo $strSQL;
		$rsResult = self::getDB()->query($strSQL);
		while($arrRow = self::getDB()->fetch_assoc($rsResult)) {
			if(!$this->objectIsSet($arrRow["intProcessInstanceID"])) {
				$this->addFromRow($arrRow, "intProcessInstanceID");
			}

			if($arrRow["strProcessClass"]) {
				$this->getObject($arrRow["intProcessInstanceID"])->getWFProcess()->setVarsFromRow($arrRow);
				$this->getObject($arrRow["intProcessInstanceID"])->getProcessObject($arrRow["strProcessClass"])->setProcessVarsFromRow($arrRow);
			}
		}
	}
}

require_once("DataClass.class.php");

class ProcessInstanceBase extends DataClass {
	protected $_intProcessInstanceID;
	protected $_intWFProcessID;
	protected $_intCurrentProcessStateID;

	function __construct($intProcessInstanceID=null) {
		$this->DataClass();
		if($intProcessInstanceID) {
			$this->load($intProcessInstanceID);
		}
	}

	protected function insert() {
		base::write_log("Process instance created","S");
		$strSQL = "INSERT INTO dbSystem.tblProcessInstance SET ";
		$strConnector = "";
		$strSQL .= $strConnector . "intProcessInstanceID = ".$this->getDB()->sanitize(self::getProcessInstanceID());
		$strConnector = ",";
		if(isset($this->_intWFProcessID)) {
			$strSQL .= $strConnector . "intWFProcessID = ".$this->getDB()->sanitize(self::getWFProcessID());
			$strConnector = ",";
		}
		if(isset($this->_intCurrentProcessStateID)) {
			$strSQL .= $strConnector . "intCurrentProcessStateID = ".$this->getDB()->sanitize(self::getCurrentProcessStateID());
			$strConnector = ",";
		}
		//echo $strSQL;
		$this->getDB()->query($strSQL);
		$this->setProcessInstanceID($this->getDB()->insert_id());
		return $this->getProcessInstanceID();
	}

	protected function update() {
		base::write_log("Process instance updated","S");
		$strSQL = "UPDATE dbSystem.tblProcessInstance SET ";
		$strConnector = "";
		$strSQL .= $strConnector . "intProcessInstanceID = ".$this->getDB()->sanitize(self::getProcessInstanceID());
		$strConnector = ",";
		if(isset($this->_intWFProcessID)) {
			$strSQL .= $strConnector . "intWFProcessID = ".$this->getDB()->sanitize(self::getWFProcessID());
			$strConnector = ",";
		}
		if(isset($this->_intCurrentProcessStateID)) {
			$strSQL .= $strConnector . "intCurrentProcessStateID = ".$this->getDB()->sanitize(self::getCurrentProcessStateID());
			$strConnector = ",";
		}
		$strSQL .= " WHERE intProcessInstanceID = ".$this->getDB()->sanitize(self::getProcessInstanceID())."";
		//echo $strSQL;
		return $this->getDB()->query($strSQL);
	}

	public function save() {
		if($this->_intProcessInstanceID) {
			return self::update();
		} else {
			return self::insert();
		}
	}

	public function delete() {
		if($this->_intProcessInstanceID) {
			base::write_log("Process instance deleted","S");
			$strSQL = "DELETE FROM dbSystem.tblProcessInstance
				WHERE intProcessInstanceID = '$this->_intProcessInstanceID'
				";
			return $this->getDB()->query($strSQL);
		}
	}

	public function load($intProcessInstanceID) {
		if(!$intProcessInstanceID) {
			return false;
		}

		$strSQL = "SELECT *
				FROM dbSystem.tblProcessInstance
				WHERE intProcessInstanceID = ".self::getDB()->sanitize($intProcessInstanceID)."
				LIMIT 1
			";
		$rsResult = $this->getDB()->query($strSQL);
		$arrRow = $this->getDB()->fetch_assoc($rsResult);
		$this->setVarsFromRow($arrRow);
	}

	function setVarsFromRow($arrRow) {
		if(isset($arrRow["intProcessInstanceID"])) $this->_intProcessInstanceID = $arrRow["intProcessInstanceID"];
		if(isset($arrRow["intWFProcessID"])) $this->_intWFProcessID = $arrRow["intWFProcessID"];
		if(isset($arrRow["intCurrentProcessStateID"])) $this->_intCurrentProcessStateID = $arrRow["intCurrentProcessStateID"];
	}

	function getProcessInstanceID() {
		return $this->_intProcessInstanceID;
	}
	function setProcessInstanceID($value) {
		if($this->_intProcessInstanceID !== $value) {
			$this->_intProcessInstanceID = $value;
			$this->_blnDirty = true;
		}
	}

	function getWFProcessID() {
		return $this->_intWFProcessID;
	}
	function setWFProcessID($value) {
		if($this->_intWFProcessID !== $value) {
			$this->_intWFProcessID = $value;
			$this->_blnDirty = true;
		}
	}

	function getCurrentProcessStateID() {
		return $this->_intCurrentProcessStateID;
	}
	function setCurrentProcessStateID($value) {
		if($this->_intCurrentProcessStateID !== $value) {
			$this->_intCurrentProcessStateID = $value;
			$this->_blnDirty = true;
		}
	}
}

require_once("WFProcess.class.php");
require_once("ProcessState.class.php");
require_once("ProcessTransition.class.php");
require_once("ProcessTransitionInstance.class.php");
require_once("Condition.class.php");
require_once("ConditionInstance.class.php");
require_once("Role.class.php");

class ProcessInstance extends ProcessInstanceBase {
	protected $_objWFProcess;
	protected $_objCurrentProcessState;
	protected $_objProcessTransitionInstanceArray;
	protected $_objRoleArray;
	protected $_objProcessTransitionArray;
	protected $_objProcessObject;

	function __construct($intProcessInstanceID=null) {
		parent::__construct($intProcessInstanceID);
	}

	function createNewInstance($intWFProcessID) {
		$this->setWFProcessID($intWFProcessID);
		$this->getWFProcess()->load($intWFProcessID);
		$this->setCurrentProcessStateID($this->getWFProcess()->getStartingProcessStateID());
	}

	function loadForDisplay($intProcessInstanceID, $objProcessObject) {
		if(!$intProcessInstanceID)
			return;

		$this->setProcessObject($objProcessObject);
		$this->load($intProcessInstanceID);
		$this->getWFProcess()->load($this->getWFProcessID());
		$this->getCurrentProcessState()->load($this->getCurrentProcessStateID());

		// Load Past Transitions
		$this->loadPastTransitions($this->getProcessInstanceID());

		// Load Potential Transitions
		include_once("ProcessTransition.class.php");
		$objProcessTransitionArray = new ProcessTransitionArray();
		// Pass the Form ID to retrieve Vendor Form array;
		$objProcessObject->select(null,array('WHERE'=>array('processingInstanceID'=>$intProcessInstanceID)));
		$formRow = $objProcessObject->getnext();
		// Have to set $_SESSION[APPLICATION]["dbSchema"] for the setCustomConditions() function
		// so that we can load tooling.
		$_SESSION[APPLICATION]["dbSchema"] = $formRow["dbSchema"];
		$objProcessTransitionArray->loadByProcessStateID($this->getCurrentProcessStateID(),$formRow);
		$objProcessTransitionArray->loadConditions(); // Used in both setRoleArrayFromProcessTransitionArray() and loadUsersForRoleArray()

		if($this->getCurrentProcessState()->getCustomConditions()) {
			$this->getProcessObject()->setCustomConditions($this, $objProcessTransitionArray);
		} else {
			$this->setRoleArrayFromProcessTransitionArray($objProcessTransitionArray);
		}

		$this->loadExistingTransitions($intProcessInstanceID);

		if(!$this->getCurrentProcessState()->getCustomConditions()) {
			$this->loadPotentialUsersForRoleArray($objProcessTransitionArray->getRoleArray());
		}
	}

	public function loadPastTransitions($intProcessInstanceID) {
		if(!$intProcessInstanceID)
			return;

		$strSQL = "SELECT tblRole.*, tblUser.*, tblProcessTransition.*, tblConditionInstance.*, tblProcessState.*, tblProcessTransitionInstance.*
				FROM dbSystem.tblProcessTransitionInstance
				INNER JOIN dbSystem.tblProcessTransition
					ON tblProcessTransitionInstance.intProcessTransitionID = tblProcessTransition.intProcessTransitionID
				INNER JOIN dbSystem.tblProcessState
					ON tblProcessState.intProcessStateID = tblProcessTransition.intFromProcessStateID
				LEFT JOIN dbSystem.tblConditionInstance
					ON tblConditionInstance.intProcessTransitionInstanceID = tblProcessTransitionInstance.intProcessTransitionInstanceID
				LEFT JOIN dbPLM.tblUser
					ON tblUser.intUserID = tblConditionInstance.intUserID
				LEFT JOIN dbPLM.tblRole
					ON tblRole.intRoleID = tblConditionInstance.intRoleID
				WHERE tblProcessTransitionInstance.intProcessInstanceID = ".self::getDB()->sanitize($intProcessInstanceID)."
				AND dtmTransitionCompleted
				ORDER BY dtmTransitionCompleted ASC
			";
		//echo $strSQL;
		$rsResult = self::getDB()->query($strSQL);
		while($arrRow = self::getDB()->fetch_assoc($rsResult)) {
			if(!$this->getProcessTransitionInstanceArray()->objectIsSet($arrRow["intProcessTransitionInstanceID"])) {
				$this->getProcessTransitionInstanceArray()->addFromRow($arrRow, "intProcessTransitionInstanceID");
				$this->getProcessTransitionInstanceArray()->getObject($arrRow["intProcessTransitionInstanceID"])->getProcessTransition()->setVarsFromRow($arrRow);
				$this->getProcessTransitionInstanceArray()->getObject($arrRow["intProcessTransitionInstanceID"])->getProcessTransition()->getFromProcessState()->setVarsFromRow($arrRow);
			}
			if($arrRow["intConditionInstanceID"]) {
				if(!$this->getProcessTransitionInstanceArray()->getObject($arrRow["intProcessTransitionInstanceID"])->getConditionInstanceArray()->objectIsSet($arrRow["intConditionInstanceID"])) {
					$this->getProcessTransitionInstanceArray()->getObject($arrRow["intProcessTransitionInstanceID"])->getConditionInstanceArray()->addFromRow($arrRow, "intConditionInstanceID");
				}
				if($arrRow["intRoleID"] && !$this->getProcessTransitionInstanceArray()->getObject($arrRow["intProcessTransitionInstanceID"])->getConditionInstanceArray()->getObject($arrRow["intConditionInstanceID"])->getRole()->getRoleID()) {
					$this->getProcessTransitionInstanceArray()->getObject($arrRow["intProcessTransitionInstanceID"])->getConditionInstanceArray()->getObject($arrRow["intConditionInstanceID"])->getRole()->setVarsFromRow($arrRow);
				}
				if($arrRow["intUserID"]) {
					$this->getProcessTransitionInstanceArray()->getObject($arrRow["intProcessTransitionInstanceID"])->getConditionInstanceArray()->getObject($arrRow["intConditionInstanceID"])->getUser()->setVarsFromRow($arrRow);
				}
			}
		}
	}

	function setRoleArrayFromProcessTransitionArray($objProcessTransitionArray) {
		foreach($objProcessTransitionArray->getArray() as $objProcessTransition) {
			$objRoleArray = $objProcessTransition->getConditionArray()->getRoleArray();
			if(!$objRoleArray->getArray())
				continue;

			foreach($objRoleArray->getArray() as $objRole) {
				if(!$this->getRoleArray()->objectIsSet($objRole->getRoleID())) {
					$this->getRoleArray()->setObject($objRole->getRoleID(), $objRole);
				}
				if(!$this->getRoleArray()->getObject($objRole->getRoleID())->getProcessTransitionArray()->objectIsSet($objProcessTransition->getProcessTransitionID())) {
					$this->getRoleArray()->getObject($objRole->getRoleID())->getProcessTransitionArray()->setObject($objProcessTransition->getProcessTransitionID(), $objProcessTransition);
				}
			}
		}
	}

	function loadExistingTransitions($intProcessInstanceID) {
		// Load those who have taken action
		$strSQL = "SELECT tblUser.*, tblRole.*, tblConditionInstance.*, tblProcessTransitionInstance.*
			FROM dbSystem.tblProcessTransitionInstance
			LEFT JOIN dbSystem.tblConditionInstance
				ON tblConditionInstance.intProcessTransitionInstanceID = tblProcessTransitionInstance.intProcessTransitionInstanceID
			LEFT JOIN dbPLM.tblRole
				ON tblConditionInstance.intRoleID = tblRole.intRoleID
			LEFT JOIN dbPLM.tblUser
				ON tblConditionInstance.intUserID = tblUser.intUserID
			WHERE tblProcessTransitionInstance.intProcessInstanceID = ".self::getDB()->sanitize($intProcessInstanceID)."
			AND dtmTransitionCompleted IS NULL
		";
		//echo $strSQL;
		$rsResult = self::getDB()->query($strSQL);
		while($arrRow = self::getDB()->fetch_assoc($rsResult)) {
			if($arrRow["intConditionInstanceID"]) { // Roles who have taken action
				if($this->getRoleArray()->objectIsSet($arrRow["intRoleID"]) && $arrRow["intUserID"]) {
					if(!$this->getRoleArray()->getObject($arrRow["intRoleID"])->getUserArray()->objectIsSet($arrRow["intUserID"])) {
						$this->getRoleArray()->getObject($arrRow["intRoleID"])->getUserArray()->addFromRow($arrRow, "intUserID");
					}
					$this->getRoleArray()->getObject($arrRow["intRoleID"])->getUserArray()->getObject($arrRow["intUserID"])->getConditionInstance()->setVarsFromRow($arrRow);
					$this->getRoleArray()->getObject($arrRow["intRoleID"])->getUserArray()->getObject($arrRow["intUserID"])->getConditionInstance()->getProcessTransitionInstance()->setVarsFromRow($arrRow);
					$this->getRoleArray()->getObject($arrRow["intRoleID"])->getUserArray()->getObject($arrRow["intUserID"])->getConditionInstance()->getProcessTransitionInstance()->getProcessTransition()->setVarsFromRow($arrRow);
				}
			}
		}
	}

	function loadPotentialUsersForRoleArray($objProcessStateRoleArray) {
		$arrRoleIDs = $objProcessStateRoleArray->getAllFields("getRoleID", true);
		$objRoleArray = $this->getProcessObject()->getRoleAndUserArrayFromRoleIDs($arrRoleIDs);

		foreach($this->getRoleArray()->getArray() as $objRole) {
			if(!$objRoleArray->objectIsSet($objRole->getRoleID()))
				continue;

			foreach($objRoleArray->getObject($objRole->getRoleID())->getUserArray()->getArray() as $objUser) {
				if(!$objRole->getUserArray()->objectIsSet($objUser->getUserID())) {
					$objRole->getUserArray()->setObject($objUser->getUserID(), $objUser);
				}
			}
		}
	}

	function workflowAction($objProcessObject, $intProcessTransitionID, $intRoleID, $txtComment, $chkConditions=false) {
		$objSessionUser = isset($_SESSION["objUser"])?$_SESSION["objUser"]:null;
		if(!$objSessionUser)
			return;

		$objProcessTransitionInstance = new ProcessTransitionInstance();
		$objProcessTransitionInstance->loadCurrentByProcessInstanceIDAndProcessTransitionID($this->getProcessInstanceID(), $intProcessTransitionID);
		$objProcessTransitionInstance->getProcessTransition()->load($intProcessTransitionID);
		if(!$objProcessTransitionInstance->getProcessTransitionInstanceID()) {
			$objProcessTransitionInstance->setProcessInstanceID($this->getProcessInstanceID());
			$objProcessTransitionInstance->setProcessTransitionID($intProcessTransitionID);
			$objProcessTransitionInstance->save();
		}

		$objProcessTransitionInstance->getProcessTransition()->getFromProcessState()->load($objProcessTransitionInstance->getProcessTransition()->getFromProcessStateID());

		$objConditionInstance = new ConditionInstance();
		$objConditionInstance->loadByProcessTransitionInstanceIDAndRoleIDAndUserID($objProcessTransitionInstance->getProcessTransitionInstanceID(), $intRoleID, $objSessionUser->getUserID());;
		if(!$objConditionInstance->getConditionInstanceID()) {
			$objConditionInstance->setProcessTransitionInstanceID($objProcessTransitionInstance->getProcessTransitionInstanceID());
			if(!$objProcessTransitionInstance->getProcessTransition()->getFromProcessState()->getCustomConditions()) {
				$objCondition = new Condition();
				$objCondition->loadByRoleID($intRoleID);
				$objConditionInstance->setConditionID($objCondition->getConditionID());
			}
			$objConditionInstance->setRoleID($intRoleID);
			$objConditionInstance->setUserID($objSessionUser->getUserID());
			$objConditionInstance->setCompletedDate(now());
			$objConditionInstance->setComment($txtComment);
			$objConditionInstance->save();
		}
//todo: this is where I feel like I should check to see if
//the transition instance id passes our transition condition test.
        if ($chkCondtions) {
		$blnConditionsMet = false;
		if($objProcessTransitionInstance->getProcessTransition()->getFromProcessState()->getCustomConditions()) {
			$objProcessTransitionInstance->getConditionInstanceArray()->loadByProcessTransitionInstanceID($objProcessTransitionInstance->getProcessTransitionInstanceID());
			$blnConditionsMet = $objProcessObject->evaluateProcessTransition($objProcessTransitionInstance);
			error_log("here");
		} else {
			$objRoleArray = new RoleArray();
			$objRoleArray->loadByProcessTransitionInstanceID($objProcessTransitionInstance->getProcessTransitionInstanceID());
			$objProcessTransitionInstance->getProcessTransition()->getConditionArray()->loadByConditionID($objProcessTransitionInstance->getProcessTransition()->getConditionID());

			$blnConditionsMet = $objProcessTransitionInstance->getProcessTransition()->getConditionArray()->getObject($objProcessTransitionInstance->getProcessTransition()->getConditionID())->evaluate($objProcessTransitionInstance->getProcessTransition()->getConditionArray(), $objRoleArray);
			error_log("there");
		}
        } else {
            $blnConditionsMet = true;
        }
error_log('contitions met is '.$blnConditionsMet);
		if($blnConditionsMet) {
			$this->setCurrentProcessStateID($objProcessTransitionInstance->getProcessTransition()->getToProcessStateID());
			$this->save();

			$objProcessTransitionInstance->setTransitionCompleted(now());
			$objProcessTransitionInstance->save();
			ProcessTransitionInstance::clearIncompleteProcessTransitions($objProcessTransitionInstance->getProcessInstanceID());

			 // Load some data for afterProcessTransition and emailing next ProcessTransitions
			$objProcessTransitionInstance->getProcessTransition()->getFromProcessState(true);
			$objProcessTransitionInstance->getProcessTransition()->getToProcessState(true);
			$objProcessTransitionInstance->getConditionInstanceArray()->loadByProcessTransitionInstanceID($objProcessTransitionInstance->getProcessTransitionInstanceID());
			$objProcessObject->afterProcessTransition($this, $objProcessTransitionInstance);

			if($objProcessTransitionInstance->getProcessTransition()->getSendEmail()) {
				$objProcessTransitionInstance->getProcessTransition()->getToProcessState()->emailForNextProcessTransitions($objProcessObject, $this, $objProcessTransitionInstance);
			}
		}
	}

	function canTakeAction($intUserID, $intProcessTransitionID, $blnOverrideWorkflow=false) {
		if(!$this->getRoleArray()->getArray())
			return false;

		$arrProcessTransitionIDs = $this->getCurrentProcessState()->getProcessTransitionArray()->getAllFields("getID");
		if(!in_array($intProcessTransitionID, $arrProcessTransitionIDs)) {
			return false;
		}

		foreach($this->getRoleArray()->getArray() as $objRole) {
			if(!$objRole->getUserArray()->getArray() || !$objRole->getProcessTransitionArray()->getArray())
				continue;

			$intRoleID = false;
			$arrUserRoleIDs = array();
			foreach($objRole->getUserArray()->getArray() as $objUser) {
				if($objUser->getUserID() == $intUserID) {
					$arrUserRoleIDs[$objRole->getRoleID()] = $objRole->getRoleID();
				}
			}
			foreach($objRole->getProcessTransitionArray()->getArray() as $objProcessTransition) {
				if($objProcessTransition->getProcessTransitionID() == $intProcessTransitionID) {
					if(in_array($objRole->getRoleID(), $arrUserRoleIDs) || $blnOverrideWorkflow) {
						$intRoleID = $objRole->getRoleID();
						return $intRoleID;
					}
				}
			}
		}
	}

	function getWFProcess() {
		if(!isset($this->_objWFProcess)) {
			$this->_objWFProcess = new WFProcess();
		}
		return $this->_objWFProcess;
	}
	function getCurrentProcessState($blnLoad=false) {
		if(!isset($this->_objCurrentProcessState)) {
			$this->_objCurrentProcessState = new ProcessState();
		}
		if($blnLoad && $this->getCurrentProcessStateID() && !$this->_objCurrentProcessState->getProcessStateID()) {
			$this->_objCurrentProcessState->load($this->getCurrentProcessStateID());
		}
		return $this->_objCurrentProcessState;
	}
	function getProcessTransitionInstanceArray() {
		if(!isset($this->_objProcessTransitionInstanceArray)) {
			$this->_objProcessTransitionInstanceArray = new ProcessTransitionInstanceArray();
		}
		return $this->_objProcessTransitionInstanceArray;
	}
	function getRoleArray() {
		if(!isset($this->_objRoleArray)) {
			$this->_objRoleArray = new RoleArray();
		}
		return $this->_objRoleArray;
	}
	function getProcessTransitionArray() {
		if(!isset($this->_objProcessTransitionArray)) {
			$this->_objProcessTransitionArray = new ProcessTransitionArray();
		}
		return $this->_objProcessTransitionArray;
	}
	function getProcessObject($strClass=null) {
		if(!$this->_objProcessObject && $strClass) {
			include_once($strClass.".class.php");
			$this->_objProcessObject = new $strClass();
		}
		return $this->_objProcessObject;
	}
	function setProcessObject($value) {
		$this->_objProcessObject = $value;
	}
}
?>