<?php
require_once("ArrayClass.class.php");

class ECNotificationArray extends ArrayClass {
	function __construct(){
		parent::__construct("ECNotification");
	}

	function load() {
		$strSQL = " SELECT * FROM dbSystem.tblECNotification";
		$rsResult = $this->getDB()->query($strSQL);
		while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
			$this->_arrObjects[$arrRow["intECNotificationID"]] = new ECNotification();
			$this->_arrObjects[$arrRow["intECNotificationID"]]->setVarsFromRow($arrRow);
		}
	}
}

require_once("Notification.class.php");

class ECNotificationBase extends Notification {
	protected $_intECNotificationID;
	protected $_intEngineeringChangeID;

	function __construct($intECNotificationID=null) {
		$this->DataClass();
		if($intECNotificationID) {
			$this->load($intECNotificationID);
		}
	}

	protected function insert() {
		base::write_log("ECNotification created","S");
		$strSQL = "INSERT INTO dbSystem.tblECNotification SET ";
		$strConnector = "";
		$strSQL .= $strConnector . "intECNotificationID = ".$this->getDB()->sanitize(self::getECNotificationID());
		$strConnector = ",";
		if(isset($this->_intNotificationID)) {
			$strSQL .= $strConnector . "intNotificationID = ".$this->getDB()->sanitize(self::getNotificationID());
			$strConnector = ",";
		}
		if(isset($this->_intEngineeringChangeID)) {
			$strSQL .= $strConnector . "intEngineeringChangeID = ".$this->getDB()->sanitize(self::getEngineeringChangeID());
			$strConnector = ",";
		}
		//echo $strSQL;
		$this->getDB()->query($strSQL);
		$this->setECNotificationID($this->getDB()->insert_id());
		return $this->getECNotificationID();
	}

	protected function update() {
		base::write_log("ECNotification updated","S");
		$strSQL = "UPDATE dbSystem.tblECNotification SET ";
		$strConnector = "";
		$strSQL .= $strConnector . "intECNotificationID = ".$this->getDB()->sanitize(self::getECNotificationID());
		$strConnector = ",";
		if(isset($this->_intNotificationID)) {
			$strSQL .= $strConnector . "intNotificationID = ".$this->getDB()->sanitize(self::getNotificationID());
			$strConnector = ",";
		}
		if(isset($this->_intEngineeringChangeID)) {
			$strSQL .= $strConnector . "intEngineeringChangeID = ".$this->getDB()->sanitize(self::getEngineeringChangeID());
			$strConnector = ",";
		}
		$strSQL .= " WHERE intECNotificationID = ".$this->getDB()->sanitize(self::getECNotificationID())."";
		//echo $strSQL;
		return $this->getDB()->query($strSQL);
	}

	public function save() {
		parent::save();
		if($this->_intECNotificationID) {
			return $this->update();
		} else {
			return $this->insert();
		}
	}

	public function delete() {
		base::write_log("ECNotification deleted");
		if($this->_intECNotificationID) {
			$strSQL = "DELETE FROM dbSystem.tblECNotification
				WHERE intECNotificationID = '$this->_intECNotificationID'
				";
			return $this->getDB()->query($strSQL);
		}
	}

	public function load($intECNotificationID) {
		if(!$intECNotificationID) {
			return false;
		}

		$strSQL = "SELECT *
				FROM dbSystem.tblECNotification
				WHERE intECNotificationID = ".self::getDB()->sanitize($intECNotificationID)."
				LIMIT 1
			";
		$rsResult = $this->getDB()->query($strSQL);
		$arrRow = $this->getDB()->fetch_assoc($rsResult);
		$this->setVarsFromRow($arrRow);
	}

	function setVarsFromRow($arrRow) {
		parent::setVarsFromRow($arrRow);

		if(isset($arrRow["intECNotificationID"])) $this->_intECNotificationID = $arrRow["intECNotificationID"];
		if(isset($arrRow["intNotificationID"])) $this->_intNotificationID = $arrRow["intNotificationID"];
		if(isset($arrRow["intEngineeringChangeID"])) $this->_intEngineeringChangeID = $arrRow["intEngineeringChangeID"];
	}

	function getECNotificationID() {
		return $this->_intECNotificationID;
	}
	function setECNotificationID($value) {
		if($this->_intECNotificationID !== $value) {
			$this->_intECNotificationID = $value;
			$this->_blnDirty = true;
		}
	}

	function getEngineeringChangeID() {
		return $this->_intEngineeringChangeID;
	}
	function setEngineeringChangeID($value) {
		if($this->_intEngineeringChangeID !== $value) {
			$this->_intEngineeringChangeID = $value;
			$this->_blnDirty = true;
		}
	}

}

include_once("EngineeringChange.class.php");

class ECNotification extends ECNotificationBase {
	private $_objEngineeringChange;

	function __construct($intECNotificationID=null) {
		parent::__construct($intECNotificationID);
	}

	function loadByID($intID){
		if(!$intID) {
			return false;
		}

		$strSQL = "SELECT *
			FROM dbSystem.tblNotification
			INNER JOIN dbSystem.tblECNotification
				ON tblNotification.intNotificationID = tblECNotification.intNotificationID
			WHERE intEngineeringChangeID = ".self::getDB()->sanitize($intID)."
			LIMIT 1
		";
		$rsResult = $this->getDB()->query($strSQL);
		$arrRow = $this->getDB()->fetch_assoc($rsResult);
		$this->setVarsFromRow($arrRow);
	}

	function notify($objEC){
		$this->setEngineeringChange($objEC);
		if(!$this->getEngineeringChange()->getEngineeringChangeID())
			return false;

		$this->getEngineeringChange()->loadForApprovalNotification($this->getEngineeringChange()->getEngineeringChangeID());
		$strSubject = $this->getEngineeringChange()->getProgram()->getProgramCommon()->getPlatform()->getPlatform() . " " . $this->getEngineeringChange()->getProgram()->getProgramName() . ": EC ".$this->getEngineeringChange()->getECNumber()." has been approved.";

		include("smarty.php");
		$g_objSmarty->assign("objEC", $this->getEngineeringChange());
		$txtBody = $g_objSmarty->fetch("not_ec.tpl");

		// Save notification
		$this->loadByID($this->getEngineeringChange()->getEngineeringChangeID());
		$this->setEngineeringChangeID($this->getEngineeringChange()->getEngineeringChangeID());
		$this->setNotificationTypeID(intNOTIFICATION_TYPE_ID_ENGINEERING_CHANGE);
		$this->setSubject($strSubject);
		$this->setBody($txtBody);

		$this->loadUsersToNotify();
		$this->sendNotification();
	}

	function loadUsersToNotify() {
		$this->loadNonProgramMembers();
		$this->loadProgramMembers();
	}

	function loadNonProgramMembers(){
		if(!$this->getNotificationTypeID())
			return false;

		$strSQL = "SELECT tblUser.*
			FROM dbSystem.tblRoleNotificationType
			INNER JOIN dbPLM.tblUserRoleXR
				ON tblUserRoleXR.intRoleID = tblRoleNotificationType.intRoleID
			INNER JOIN dbPLM.tblUser
				ON tblUser.intUserID = tblUserRoleXR.intUserID
			INNER JOIN dbSystem.tblUserCustomer
				ON tblUserCustomer.intUserID = tblUser.intUserID
				AND tblUserCustomer.intCustomerID = ".self::getDB()->sanitize($this->getEngineeringChange()->getProgram()->getProgramCommon()->getCustomerID())."
			INNER JOIN dbSystem.tblUserProgramType
				ON tblUserProgramType.intUserID = tblUser.intUserID
				AND tblUserProgramType.intProgramTypeID = ".self::getDB()->sanitize($this->getEngineeringChange()->getProgram()->getProgramCommon()->getProgramTypeID())."
			LEFT JOIN (
				SELECT tblUserNotification.intUserNotificationID, tblUserNotification.intUserID, tblECNotification.intEngineeringChangeID
				FROM dbSystem.tblNotification
				INNER JOIN dbSystem.tblECNotification
					ON tblECNotification.intNotificationID = tblNotification.intNotificationID
				INNER JOIN dbSystem.tblUserNotification
					ON tblUserNotification.intNotificationID = tblNotification.intNotificationID
			) AS tblNotification2
				ON tblNotification2.intUserID = tblUser.intUserID
				AND tblNotification2.intEngineeringChangeID = ".self::getDB()->sanitize($this->getEngineeringChange()->getProgram()->getProgramID())."
			WHERE tblRoleNotificationType.intNotificationTypeID = ".self::getDB()->sanitize($this->getNotificationTypeID())."
			AND tblNotification2.intUserNotificationID IS NULL";
		//echo $strSQL . "<br />";
		$rsResult = $this->getDB()->query($strSQL);
		while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
			$this->getUserArray()->addFromRow($arrRow, "intUserID");
		}
	}

	function loadProgramMembers() {
		$objUserArray = new UserArray();
		$objUserArray->loadProgramMembers($this->getEngineeringChange()->getProgram()->getProgramID());
		$arrUserIDs = getArrayFromObjectField($objUserArray, "getUserID");

		$strSQL = "SELECT tblUser.*
			FROM dbPLM.tblUser
			LEFT JOIN (
				SELECT intUserNotificationID, intUserID
				FROM dbSystem.tblUserNotification
				INNER JOIN dbSystem.tblNotification
					ON tblUserNotification.intNotificationID = tblNotification.intNotificationID
				INNER JOIN dbSystem.tblECNotification
					ON tblECNotification.intNotificationID = tblNotification.intNotificationID
					AND tblECNotification.intEngineeringChangeID = ".self::getDB()->sanitize($this->getEngineeringChangeID())."
			) AS tblUserNotification2
				ON tblUserNotification2.intUserID = tblUser.intUserID
			WHERE tblUser.intUserID IN ('".implode("','", $arrUserIDs)."')
			AND tblUserNotification2.intUserNotificationID IS NULL";
		//echo $strSQL . "<br />";
		$rsResult = $this->getDB()->query($strSQL);
		while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
			$this->getUserArray()->addFromRow($arrRow, "intUserID");
		}
		//return $objUserArray;
	}

	function getEngineeringChange(){
		if(!$this->_objEngineeringChange)
			$this->_objEngineeringChange = new EngineeringChange();
		return $this->_objEngineeringChange;
	}
	function setEngineeringChange($value){
		$this->_objEngineeringChange = $value;
	}
}
?>