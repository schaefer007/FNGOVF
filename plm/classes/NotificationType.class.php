<?php
require_once("ArrayClass.class.php");

class NotificationTypeArray extends ArrayClass {
	function __construct(){
		parent::__construct("NotificationType");
	}

	function load() {
		$strSQL = " SELECT * FROM dbSystem.tblNotificationType";
		$rsResult = $this->getDB()->query($strSQL);
		while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
			$this->_arrObjects[$arrRow["intNotificationTypeID"]] = new NotificationType();
			$this->_arrObjects[$arrRow["intNotificationTypeID"]]->setVarsFromRow($arrRow);
		}
	}

	function loadForNotificationSubscription() {
		$strSQL = " SELECT tblRole.*, tblRoleNotificationType.*, tblNotificationType.* FROM dbSystem.tblNotificationType
			LEFT JOIN dbSystem.tblRoleNotificationType
				ON tblNotificationType.intNotificationTypeID = tblRoleNotificationType.intNotificationTypeID
			LEfT JOIN dbPLM.tblRole
				ON tblRole.intRoleID = tblRoleNotificationType.intRoleID ";
		//echo $strSQL;
		$rsResult = $this->getDB()->query($strSQL);
		while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
			if(!isset($this->_arrObjects[$arrRow["intNotificationTypeID"]])) {
				$this->_arrObjects[$arrRow["intNotificationTypeID"]] = new NotificationType();
				$this->_arrObjects[$arrRow["intNotificationTypeID"]]->setVarsFromRow($arrRow);
			}
			if($arrRow["intRoleNotificationTypeID"]) {
				$this->_arrObjects[$arrRow["intNotificationTypeID"]]->getRoleNotificationTypeArray()->addFromRow($arrRow, "intRoleNotificationTypeID");
				$this->_arrObjects[$arrRow["intNotificationTypeID"]]->getRoleNotificationTypeArray()->getObject($arrRow["intRoleNotificationTypeID"])->getRole()->setVarsFromRow($arrRow);
			}
		}
	}
}

require_once("DataClass.class.php");

class NotificationTypeBase extends DataClass {
	protected $_intNotificationTypeID;
	protected $_strNotificationType;
	protected $_strNotificationTypeDescription;
	protected $_strNotificationTypeObject;
	protected $_dtmCreatedOn;

	function __construct($intNotificationTypeID=null) {
		$this->DataClass();
		if($intNotificationTypeID) {
			$this->load($intNotificationTypeID);
		}
	}

	protected function insert() {
		base::write_log("Notification type created","S");
		$strSQL = "INSERT INTO dbSystem.tblNotificationType SET ";
		$strConnector = "";
		$strSQL .= $strConnector . "intNotificationTypeID = ".$this->getDB()->sanitize(self::getNotificationTypeID());
		$strConnector = ",";
		if(isset($this->_strNotificationType)) {
			$strSQL .= $strConnector . "strNotificationType = ".$this->getDB()->sanitize(self::getNotificationType());
			$strConnector = ",";
		}
		if(isset($this->_strNotificationTypeDescription)) {
			$strSQL .= $strConnector . "strNotificationTypeDescription = ".$this->getDB()->sanitize(self::getNotificationTypeDescription());
			$strConnector = ",";
		}
		if(isset($this->_strNotificationTypeObject)) {
			$strSQL .= $strConnector . "strNotificationTypeObject = ".$this->getDB()->sanitize(self::getNotificationTypeObject());
			$strConnector = ",";
		}
		if(isset($this->_dtmCreatedOn)) {
			$strSQL .= $strConnector . "dtmCreatedOn = ".$this->getDB()->sanitize(self::getCreatedOn());
			$strConnector = ",";
		}
		//echo $strSQL;
		$this->getDB()->query($strSQL);
		$this->setNotificationTypeID($this->getDB()->insert_id());
		return $this->getNotificationTypeID();
	}

	protected function update() {
		base::write_log("Notification Type Updated","S");
		$strSQL = "UPDATE dbSystem.tblNotificationType SET ";
		$strConnector = "";
		$strSQL .= $strConnector . "intNotificationTypeID = ".$this->getDB()->sanitize(self::getNotificationTypeID());
		$strConnector = ",";
		if(isset($this->_strNotificationType)) {
			$strSQL .= $strConnector . "strNotificationType = ".$this->getDB()->sanitize(self::getNotificationType());
			$strConnector = ",";
		}
		if(isset($this->_strNotificationTypeDescription)) {
			$strSQL .= $strConnector . "strNotificationTypeDescription = ".$this->getDB()->sanitize(self::getNotificationTypeDescription());
			$strConnector = ",";
		}
		if(isset($this->_strNotificationTypeObject)) {
			$strSQL .= $strConnector . "strNotificationTypeObject = ".$this->getDB()->sanitize(self::getNotificationTypeObject());

			$strConnector = ",";
		}
		if(isset($this->_dtmCreatedOn)) {
			$strSQL .= $strConnector . "dtmCreatedOn = ".$this->getDB()->sanitize(self::getCreatedOn());
			$strConnector = ",";
		}
		$strSQL .= " WHERE intNotificationTypeID = ".$this->getDB()->sanitize(self::getNotificationTypeID())."";
		//echo $strSQL;
		return $this->getDB()->query($strSQL);
	}

	public function save() {
		if($this->_intNotificationTypeID) {
			return $this->update();
		} else {
			return $this->insert();
		}
	}

	public function delete() {
		if($this->_intNotificationTypeID) {
			$strSQL = "DELETE FROM dbSystem.tblNotificationType
				WHERE intNotificationTypeID = '$this->_intNotificationTypeID'
				";
			return $this->getDB()->query($strSQL);
		}
	}

	public function load($intNotificationTypeID) {
		if(!$intNotificationTypeID) {
			return false;
		}

		$strSQL = "SELECT *
				FROM dbSystem.tblNotificationType
				WHERE intNotificationTypeID = ".self::getDB()->sanitize($intNotificationTypeID)."
				LIMIT 1
			";
		$rsResult = $this->getDB()->query($strSQL);
		$arrRow = $this->getDB()->fetch_assoc($rsResult);
		$this->setVarsFromRow($arrRow);
	}

	function setVarsFromRow($arrRow) {
		if(isset($arrRow["intNotificationTypeID"])) $this->_intNotificationTypeID = $arrRow["intNotificationTypeID"];
		if(isset($arrRow["strNotificationType"])) $this->_strNotificationType = $arrRow["strNotificationType"];
		if(isset($arrRow["strNotificationTypeDescription"])) $this->_strNotificationTypeDescription = $arrRow["strNotificationTypeDescription"];
		if(isset($arrRow["strNotificationTypeObject"])) $this->_strNotificationTypeObject = $arrRow["strNotificationTypeObject"];
		if(isset($arrRow["dtmCreatedOn"])) $this->_dtmCreatedOn = $arrRow["dtmCreatedOn"];
	}

	function getNotificationTypeID() {
		return $this->_intNotificationTypeID;
	}
	function setNotificationTypeID($value) {
		if($this->_intNotificationTypeID !== $value) {
			$this->_intNotificationTypeID = $value;
			$this->_blnDirty = true;
		}
	}

	function getNotificationType() {
		return $this->_strNotificationType;
	}
	function setNotificationType($value) {
		if($this->_strNotificationType !== $value) {
			$this->_strNotificationType = $value;
			$this->_blnDirty = true;
		}
	}

	function getNotificationTypeDescription() {
		return $this->_strNotificationTypeDescription;
	}
	function setNotificationTypeDescription($value) {
		if($this->_strNotificationTypeDescription !== $value) {
			$this->_strNotificationTypeDescription = $value;
			$this->_blnDirty = true;
		}
	}

	function getNotificationTypeObject() {
		return $this->_strNotificationTypeObject;
	}
	function setNotificationTypeObject($value) {
		if($this->_strNotificationTypeObject !== $value) {
			$this->_strNotificationTypeObject = $value;
			$this->_blnDirty = true;
		}
	}

	function getCreatedOn() {
		return $this->_dtmCreatedOn;
	}
	function setCreatedOn($value) {
		if($this->_dtmCreatedOn !== $value) {
			$this->_dtmCreatedOn = $value;
			$this->_blnDirty = true;
		}
	}
}

include_once("Product.class.php");
include_once("RoleNotificationType.class.php");
include_once("Notification.class.php");
include_once("User.class.php");

class NotificationType extends NotificationTypeBase {
	private $_objRoleNotificationTypeArray;

	function __construct($intNotificationTypeID=null) {
		parent::__construct($intNotificationTypeID);
	}

	function getRoleNotificationTypeArray(){
		if(!$this->_objRoleNotificationTypeArray)
			$this->_objRoleNotificationTypeArray = new RoleNotificationTypeArray();
		return $this->_objRoleNotificationTypeArray;
	}
}
?>
