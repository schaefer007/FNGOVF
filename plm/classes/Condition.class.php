<?php
require_once("ArrayClass.class.php");

class ConditionArray extends ArrayClass {
	function __construct(){
		parent::__construct("Condition");
	}

	function load() {
		$strSQL = " SELECT * FROM dbSystem.tblCondition";
		$rsResult = $this->getDB()->query($strSQL);
		while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
			$this->_arrObjects[$arrRow["intConditionID"]] = new Condition();
			$this->_arrObjects[$arrRow["intConditionID"]]->setVarsFromRow($arrRow);
		}
	}

	function loadByConditionID($intConditionID) {
		if(!$intConditionID)
			return;

		$strSQL = " SELECT tblRole.*, tblConditionXR.*, tblCondition.*
			FROM dbSystem.tblCondition
			LEFT JOIN dbSystem.tblConditionXR
				ON tblConditionXR.intParentConditionID = tblCondition.intConditionID
			LEFT JOIN dbPLM.tblRole
				ON tblRole.intRoleID = tblCondition.intRoleID
			WHERE tblCondition.intConditionID = ".self::getDB()->sanitize($intConditionID);
		//echo $strSQL . "<br />";
		$rsResult = $this->getDB()->query($strSQL);
		while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
			if(!isset($this->_arrObjects[$arrRow["intConditionID"]])) {
				$this->_arrObjects[$arrRow["intConditionID"]] = new Condition();
				$this->_arrObjects[$arrRow["intConditionID"]]->setVarsFromRow($arrRow);
				if($arrRow["intRoleID"]) {
					$this->_arrObjects[$arrRow["intConditionID"]]->getRole()->setVarsFromRow($arrRow);
					if(!$arrRow["blnHideByDefault"]) { // If a single role (from any condition) doesn't have blnHideByDefault, then show the role.
						$this->_arrObjects[$arrRow["intConditionID"]]->getRole()->setHideByDefault(false);
					}
				}
			}
			if($arrRow["intConditionXRID"]) {
				$this->_arrObjects[$arrRow["intConditionID"]]->getChildConditionArray()->createNew($arrRow["intChildConditionID"]);
				if(!$this->objectIsSet($arrRow["intChildConditionID"])) $this->loadByConditionID($arrRow["intChildConditionID"]);
			}
		}
	}

	function loadByTopLevelConditionID($intTopLevelConditionID) {
		if(!$intTopLevelConditionID)
			return;

		$strSQL = " SELECT tblRole.*, tblCondition.*, tblChildConditionXR.*
			FROM dbSystem.tblConditionXR tblParentConditionXR
			INNER JOIN dbSystem.tblCondition
				ON tblCondition.intConditionID = tblParentConditionXR.intChildConditionID
			LEFT JOIN dbSystem.tblConditionXR tblChildConditionXR
				ON tblChildConditionXR.intParentConditionID = tblCondition.intConditionID
				AND tblChildConditionXR.intTopLevelConditionID = tblParentConditionXR.intTopLevelConditionID
			LEFT JOIN dbPLM.tblRole
				ON tblRole.intRoleID = tblCondition.intRoleID
			WHERE tblParentConditionXR.intTopLevelConditionID = ".self::getDB()->sanitize($intTopLevelConditionID)."
		";
		//echo $strSQL . "<br />";
		$rsResult = $this->getDB()->query($strSQL);
		while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
			if(!isset($this->_arrObjects[$arrRow["intConditionID"]])) {
				$this->_arrObjects[$arrRow["intConditionID"]] = new Condition();
				$this->_arrObjects[$arrRow["intConditionID"]]->setVarsFromRow($arrRow);
				if($arrRow["intRoleID"]) {
					$this->_arrObjects[$arrRow["intConditionID"]]->getRole()->setVarsFromRow($arrRow);
				}
			}
			if($arrRow["intChildConditionID"]) {
				$objChildConditionXR = $this->_arrObjects[$arrRow["intConditionID"]]->getChildConditionXRArray()->addFromRow($arrRow, "intConditionXRID");
			}
			//$objChildCondition->setConditionID($arrRow["intChildConditionID"]);
		}
	}

	function getRoleArray() {
		include_once("Role.class.php");
		$objRoleArray = new RoleArray();
		if(!$this->getArray())
			return $objRoleArray;

		foreach($this->getArray() as $objCondition) {
			if($objCondition->getRoleID() && !$objRoleArray->objectIsSet($objCondition->getRoleID())) {
				$objRoleArray->setObject($objCondition->getRoleID(), $objCondition->getRole());
			}
			if($objCondition->getChildConditionArray()->getArray()) {
				$objRoleArray->setArray(array_merge($objRoleArray->getArray(), $objCondition->getChildConditionArray()->getRoleArray()->getArray()));
			}
		}
		return $objRoleArray;
	}

	function loadConditionRoles() {
		$strSQL = " SELECT tblRole.*, tblCondition.*
			FROM dbSystem.tblCondition
			INNER JOIN dbPLM.tblRole
				ON tblRole.intRoleID = tblCondition.intRoleID
			ORDER BY strRoleName
		";
		//echo $strSQL . "<br />";
		$rsResult = $this->getDB()->query($strSQL);
		while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
			$this->_arrObjects[$arrRow["intConditionID"]] = new Condition();
			$this->_arrObjects[$arrRow["intConditionID"]]->setVarsFromRow($arrRow);
			$this->_arrObjects[$arrRow["intConditionID"]]->getRole()->setVarsFromRow($arrRow);
		}
	}
}

require_once("DataClass.class.php");

class ConditionBase extends DataClass {
	protected $_intConditionID;
	protected $_intRoleID;
	protected $_strOperation;
	protected $_blnHideByDefault;
	protected $_strDescription;
	protected $_blnPrimaryCondition;

	function __construct($intConditionID=null) {
		$this->DataClass();
		if($intConditionID) {
			$this->load($intConditionID);
		}
	}

	protected function insert() {
		base::write_log("Condition created","S");
		$strSQL = "INSERT INTO dbSystem.tblCondition SET ";
		$strConnector = "";
		$strSQL .= $strConnector . "intConditionID = ".$this->getDB()->sanitize(self::getConditionID());
		$strConnector = ",";
		if(isset($this->_intRoleID)) {
			$strSQL .= $strConnector . "intRoleID = ".$this->getDB()->sanitize(self::getRoleID());
			$strConnector = ",";
		}
		if(isset($this->_strOperation)) {
			$strSQL .= $strConnector . "strOperation = ".$this->getDB()->sanitize(self::getOperation());
			$strConnector = ",";
		}
		if(isset($this->_blnHideByDefault)) {
			$strSQL .= $strConnector . "blnHideByDefault = ".$this->getDB()->sanitize(self::getHideByDefault());
			$strConnector = ",";
		}
		if(isset($this->_strDescription)) {
			$strSQL .= $strConnector . "strDescription = ".$this->getDB()->sanitize(self::getDescription());
			$strConnector = ",";
		}
		if(isset($this->_blnPrimaryCondition)) {
			$strSQL .= $strConnector . "blnPrimaryCondition = ".$this->getDB()->sanitize(self::getPrimaryCondition());
			$strConnector = ",";
		}
		//echo $strSQL;
		$this->getDB()->query($strSQL);
		$this->setConditionID($this->getDB()->insert_id());
		return $this->getConditionID();
	}

	protected function update() {
		base::write_log("Condition updated","S");
		$strSQL = "UPDATE dbSystem.tblCondition SET ";
		$strConnector = "";
		$strSQL .= $strConnector . "intConditionID = ".$this->getDB()->sanitize(self::getConditionID());
		$strConnector = ",";
		if(isset($this->_intRoleID)) {
			$strSQL .= $strConnector . "intRoleID = ".$this->getDB()->sanitize(self::getRoleID());
			$strConnector = ",";
		}
		if(isset($this->_strOperation)) {
			$strSQL .= $strConnector . "strOperation = ".$this->getDB()->sanitize(self::getOperation());
			$strConnector = ",";
		}
		if(isset($this->_blnHideByDefault)) {
			$strSQL .= $strConnector . "blnHideByDefault = ".$this->getDB()->sanitize(self::getHideByDefault());
			$strConnector = ",";
		}
		if(isset($this->_strDescription)) {
			$strSQL .= $strConnector . "strDescription = ".$this->getDB()->sanitize(self::getDescription());
			$strConnector = ",";
		}
		if(isset($this->_blnPrimaryCondition)) {
			$strSQL .= $strConnector . "blnPrimaryCondition = ".$this->getDB()->sanitize(self::getPrimaryCondition());
			$strConnector = ",";
		}
		$strSQL .= " WHERE intConditionID = ".$this->getDB()->sanitize(self::getConditionID())."";
		//echo $strSQL;
		return $this->getDB()->query($strSQL);
	}

	public function save() {
		if($this->_intConditionID) {
			return self::update();
		} else {
			return self::insert();
		}
	}

	public function delete() {
		base::write_log("Condition deleted","S");
		if($this->_intConditionID) {
			$strSQL = "DELETE FROM dbSystem.tblCondition
				WHERE intConditionID = '$this->_intConditionID'
				";
			return $this->getDB()->query($strSQL);
		}
	}

	public function load($intConditionID) {
		if(!$intConditionID) {
			return false;
		}

		$strSQL = "SELECT *
				FROM dbSystem.tblCondition
				WHERE intConditionID = ".self::getDB()->sanitize($intConditionID)."
				LIMIT 1
			";
		$rsResult = $this->getDB()->query($strSQL);
		$arrRow = $this->getDB()->fetch_assoc($rsResult);
		$this->setVarsFromRow($arrRow);
	}

	function setVarsFromRow($arrRow) {
		if(isset($arrRow["intConditionID"])) $this->_intConditionID = $arrRow["intConditionID"];
		if(isset($arrRow["intRoleID"])) $this->_intRoleID = $arrRow["intRoleID"];
		if(isset($arrRow["strOperation"])) $this->_strOperation = $arrRow["strOperation"];
		if(isset($arrRow["blnHideByDefault"])) $this->_blnHideByDefault = $arrRow["blnHideByDefault"];
		if(isset($arrRow["strDescription"])) $this->_strDescription = $arrRow["strDescription"];
		if(isset($arrRow["blnPrimaryCondition"])) $this->_blnPrimaryCondition = $arrRow["blnPrimaryCondition"];
	}

	function getConditionID() {
		return $this->_intConditionID;
	}
	function setConditionID($value) {
		if($this->_intConditionID !== $value) {
			$this->_intConditionID = $value;
			$this->_blnDirty = true;
		}
	}

	function getRoleID() {
		return $this->_intRoleID;
	}
	function setRoleID($value) {
		if($this->_intRoleID !== $value) {
			$this->_intRoleID = $value;
			$this->_blnDirty = true;
		}
	}

	function getOperation() {
		return $this->_strOperation;
	}
	function setOperation($value) {
		if($this->_strOperation !== $value) {
			$this->_strOperation = $value;
			$this->_blnDirty = true;
		}
	}

	function getHideByDefault() {
		return $this->_blnHideByDefault;
	}
	function setHideByDefault($value) {
		if($this->_blnHideByDefault !== $value) {
			$this->_blnHideByDefault = $value;
			$this->_blnDirty = true;
		}
	}

	function getDescription() {
		return $this->_strDescription;
	}
	function setDescription($value) {
		if($this->_strDescription !== $value) {
			$this->_strDescription = $value;
			$this->_blnDirty = true;
		}
	}

	function getPrimaryCondition() {
		return $this->_blnPrimaryCondition;
	}
	function setPrimaryCondition($value) {
		if($this->_blnPrimaryCondition !== $value) {
			$this->_blnPrimaryCondition = $value;
			$this->_blnDirty = true;
		}
	}
}

include_once("Role.class.php");
include_once("ConditionInstance.class.php");

class Condition extends ConditionBase {
	protected $_objRole;
	protected $_objChildConditionArray; // Considering deprecating this, and only using $_objChildConditionXRArray
	protected $_objChildConditionXRArray;
	protected $_objParentConditionXRArray;
	protected $_objConditionInstanceArray;

	function __construct($intConditionID=null) {
		parent::__construct($intConditionID);
	}

	function loadByRoleID($intRoleID) {
		if(!$intRoleID) {
			return false;
		}

		$strSQL = "SELECT *
				FROM dbSystem.tblCondition
				WHERE intRoleID = ".self::getDB()->sanitize($intRoleID)."
				LIMIT 1

			";
		$rsResult = $this->getDB()->query($strSQL);
		$arrRow = $this->getDB()->fetch_assoc($rsResult);
		$this->setVarsFromRow($arrRow);
	}

	function loadChildConditions() {
		if(!$this->getConditionID())
			return;

		$strSQL = "SELECT tblRole.*, tblConditionXR.*, tblCondition.*
			FROM dbSystem.tblCondition
			INNER JOIN dbSystem.tblConditionXR
				ON tblConditionXR.intChildConditionID = tblCondition.intConditionID
			LEFT JOIN dbPLM.tblRole
				ON tblRole.intRoleID = tblCondition.intRoleID
			WHERE intParentConditionID = ".self::getDB()->sanitize($this->getConditionID())."
		";
		$rsResult = $this->getDB()->query($strSQL);
		while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
			$this->getChildConditionArray()->addFromRow($arrRow, "intConditionID");
			$this->getChildConditionArray()->getObject($arrRow["intConditionID"])->getRole()->setVarsFromRow($arrRow);
			$this->getChildConditionArray()->getObject($arrRow["intConditionID"])->loadChildConditions();
		}
	}

	function evaluate($objConditionArray, $objRoleArray) {
		if($this->getRoleID()) {
			if($objRoleArray->objectIsSet($this->getRoleID())) {
				return true;
			}
		} elseif($this->getChildConditionArray()->getArray()) {
			$blnReturn = false;
			if($this->getOperation() == "Or") {
				$blnReturn = false;
				foreach($this->getChildConditionArray()->getArray() as $intChildConditionID => $objChildCondition) {
					$blnReturn = $blnReturn || $objConditionArray->getObject($intChildConditionID)->evaluate($objConditionArray, $objRoleArray);

				}
			} elseif($this->getOperation() == "And") {
				$blnReturn = true;
				foreach($this->getChildConditionArray()->getArray() as $intChildConditionID => $objChildCondition) {
					$blnReturn = $blnReturn && $objConditionArray->getObject($intChildConditionID)->evaluate($objConditionArray, $objRoleArray);
				}
			}
			return $blnReturn;
		}
		return false;
	}

	function output($objConditionArray, $objRoleArray=null) {
		if(!$objRoleArray) {
			$arrRoleIDs = $objConditionArray->getAllFields("getRoleID");
			$objRoleArray = new RoleArray();
			$objRoleArray->loadByIDs($arrRoleIDs);
		}

		$strReturn = "";
		if($this->getRoleID()) {
			//if($objRoleArray->objectIsSet($this->getRoleID())) {
				$strReturn = $objRoleArray->getObject($this->getRoleID())->getRoleName();
			//}
		} elseif($this->getChildConditionArray()->getArray()) {
			if($this->getOperation() == "Or") {
				$strConnector = "(";
				foreach($this->getChildConditionArray()->getArray() as $intChildConditionID => $objChildCondition) {
					$strReturn .= $strConnector . $objConditionArray->getObject($intChildConditionID)->output($objConditionArray, $objRoleArray);
					$strConnector = " or ";
				}
			} elseif($this->getOperation() == "And") {
				$strConnector = "(";
				foreach($this->getChildConditionArray()->getArray() as $intChildConditionID => $objChildCondition) {
					$strReturn .= $strConnector . $objConditionArray->getObject($intChildConditionID)->output($objConditionArray, $objRoleArray);
					$strConnector = " and ";
				}
			}
			if($strReturn)
				$strReturn .= ")";
		}
		return $strReturn;
	}

	function getRole() {
		if(!$this->_objRole) {
			$this->_objRole = new Role();
		}
		return $this->_objRole;
	}
	function getChildConditionArray() {
		if(!$this->_objChildConditionArray) {
			$this->_objChildConditionArray = new ConditionArray();
		}
		return $this->_objChildConditionArray;
	}
	function getChildConditionXRArray() {
		include_once("ConditionXR.class.php");
		if(!$this->_objChildConditionXRArray) {
			$this->_objChildConditionXRArray = new ConditionXRArray();
		}
		return $this->_objChildConditionXRArray;
	}
	function getParentConditionXRArray() {
		include_once("ConditionXR.class.php");
		if(!$this->_objParentConditionXRArray) {
			$this->_objParentConditionXRArray = new ConditionXRArray();
		}
		return $this->_objParentConditionXRArray;
	}
	function getConditionInstanceArray() {
		if(!$this->_objConditionInstanceArray) {
			$this->_objConditionInstanceArray = new ConditionInstanceArray();
		}
		return $this->_objConditionInstanceArray;
	}
}
?>