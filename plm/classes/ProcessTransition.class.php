<?php
require_once("ArrayClass.class.php");

class ProcessTransitionArray extends ArrayClass {
	function __construct(){
		parent::__construct("ProcessTransition");
	}

	function load() {
		$strSQL = " SELECT * FROM dbSystem.tblProcessTransition";
		$rsResult = $this->getDB()->query($strSQL);
		while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
			$this->_arrObjects[$arrRow["intProcessTransitionID"]] = new ProcessTransition();
			$this->_arrObjects[$arrRow["intProcessTransitionID"]]->setVarsFromRow($arrRow);
		}
	}

	function loadByProcessStateIDs($arrProcessStateIDs) {
		if(!$arrProcessStateIDs)
			return;
		$strSQL = " SELECT *
			FROM dbSystem.tblProcessTransition
			WHERE intFromProcessStateID IN ('".implode("','", $arrProcessStateIDs)."')
			OR intToProcessStateID IN ('".implode("','", $arrProcessStateIDs)."')";
//		error_log($strSQL);
		$rsResult = $this->getDB()->query($strSQL);
		while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
			$this->_arrObjects[$arrRow["intProcessTransitionID"]] = new ProcessTransition();
			$this->_arrObjects[$arrRow["intProcessTransitionID"]]->setVarsFromRow($arrRow);
		}
	}

	/**
	 * @param $intProcessStateID
	 * @param $formID
	 * todo find how to view _arrObjects[$arrRow["intProcessTransitionID"]]
	 */
	function loadByProcessStateID($intProcessStateID, $formRow) {
		$this->clear();
		if(!$intProcessStateID)
			return;
		$strSQL = " SELECT tblCondition.*, tblProcessTransition.*
			FROM dbSystem.tblProcessTransition
			LEFT JOIN dbSystem.tblCondition
				ON tblCondition.intConditionID = tblProcessTransition.intConditionID
			WHERE intFromProcessStateID = ".self::getDB()->sanitize($intProcessStateID)."
			ORDER BY tblProcessTransition.intDisplayOrder ";
//		echo $strSQL;
		$rsResult = $this->getDB()->query($strSQL);
//		$intProcessTransitionIDs = array();
		include_once("classes/vendorform.class.php");
		$vendorForm = new vendorform();
		$vendorForm->select(null,array('WHERE'=>array('VFID'=>$formRow['formID'])));
		if($vendorRow = $vendorForm->getnext()) {
            $vendorRow = array_merge($vendorRow, $formRow);
		}
		while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
//			$intProcessTransitionIDs[] = $arrRow["intProcessTransitionID"];
			//todo do it here
            include_once("classes/TransitionCondition.php");
            $objTransitionCondition = new TransitionConditionArray($arrRow["intProcessTransitionID"]);
            error_log('Testing Transition Condition for Process Transition ID: ' . $arrRow["intProcessTransitionID"]);
            if (!$objTransitionCondition->evaluateTransitionConditions($vendorRow)) {
                error_log('Failed');
                continue;
            }
            error_log('Passed');
			$this->_arrObjects[$arrRow["intProcessTransitionID"]] = new ProcessTransition();
			$this->_arrObjects[$arrRow["intProcessTransitionID"]]->setVarsFromRow($arrRow);
			$this->_arrObjects[$arrRow["intProcessTransitionID"]]->getCondition()->setVarsFromRow($arrRow);
		}
//		foreach($intProcessTransitionIDs as $procTransID) {
//			echo '<h2>$this->_arrObjects[' . $procTransID . ']</h2><pre>';
//			var_dump($this->_arrObjects[$procTransID]);
//			echo "</pre>";
//		}
	}

	function loadConditions() {
		if(!$this->getArray())
			return;

		foreach($this->getArray() as $objObject) {
			$objObject->getConditionArray()->loadByConditionID($objObject->getConditionID());
		}
	}

	function getRoleArray() {
		include_once("Role.class.php");
		$objRoleArray = new RoleArray();
		if(!$this->getArray())
			return $objRoleArray;

		foreach($this->getArray() as $objProcessTransition) {
			$objRoleArray->setArray(array_merge($objRoleArray->getArray(), $objProcessTransition->getConditionArray()->getRoleArray()->getArray()));
		}
		return $objRoleArray;
	}
}

require_once("DataClass.class.php");

class ProcessTransitionBase extends DataClass {
	protected $_intProcessTransitionID;
	protected $_intFromProcessStateID;
	protected $_intToProcessStateID;
	protected $_intConditionID;
	protected $_strTransitionName;
	protected $_strTransitionButton;
	protected $_txtDescription;
	protected $_strJSCallbackFunction;
	protected $_blnCommentRequired;
	protected $_blnSendEmail;
	protected $_intDisplayOrder;

	function __construct($intProcessTransitionID=null) {
		$this->DataClass();
		if($intProcessTransitionID) {
			$this->load($intProcessTransitionID);
		}
	}

	protected function insert() {
		base::write_log("Process transition created","S");
		$strSQL = "INSERT INTO dbSystem.tblProcessTransition SET ";
		$strConnector = "";
		$strSQL .= $strConnector . "intProcessTransitionID = ".$this->getDB()->sanitize(self::getProcessTransitionID());
		$strConnector = ",";
		if(isset($this->_intFromProcessStateID)) {
			$strSQL .= $strConnector . "intFromProcessStateID = ".$this->getDB()->sanitize(self::getFromProcessStateID());
			$strConnector = ",";
		}
		if(isset($this->_intToProcessStateID)) {
			$strSQL .= $strConnector . "intToProcessStateID = ".$this->getDB()->sanitize(self::getToProcessStateID());
			$strConnector = ",";
		}
		if(isset($this->_intConditionID)) {
			$strSQL .= $strConnector . "intConditionID = ".$this->getDB()->sanitize(self::getConditionID());
			$strConnector = ",";
		}
		if(isset($this->_strTransitionName)) {
			$strSQL .= $strConnector . "strTransitionName = ".$this->getDB()->sanitize(self::getTransitionName());
			$strConnector = ",";
		}
		if(isset($this->_strTransitionButton)) {
			$strSQL .= $strConnector . "strTransitionButton = ".$this->getDB()->sanitize(self::getTransitionButton());
			$strConnector = ",";
		}
		if(isset($this->_txtDescription)) {
			$strSQL .= $strConnector . "txtDescription = ".$this->getDB()->sanitize(self::getDescription());
			$strConnector = ",";
		}
		if(isset($this->_strJSCallbackFunction)) {
			$strSQL .= $strConnector . "strJSCallbackFunction = ".$this->getDB()->sanitize(self::getJSCallbackFunction());
			$strConnector = ",";
		}
		if(isset($this->_blnCommentRequired)) {
			$strSQL .= $strConnector . "blnCommentRequired = ".$this->getDB()->sanitize(self::getCommentRequired());
			$strConnector = ",";
		}
		if(isset($this->_blnSendEmail)) {
			$strSQL .= $strConnector . "blnSendEmail = ".$this->getDB()->sanitize(self::getSendEmail());
			$strConnector = ",";
		}
		if(isset($this->_intDisplayOrder)) {
			$strSQL .= $strConnector . "intDisplayOrder = ".$this->getDB()->sanitize(self::getDisplayOrder());
			$strConnector = ",";
		}
		//echo $strSQL;
		$this->getDB()->query($strSQL);

		$this->setProcessTransitionID($this->getDB()->insert_id());
		return $this->getProcessTransitionID();
	}

	protected function update() {
		base::write_log("Process transition update","S");
		$strSQL = "UPDATE dbSystem.tblProcessTransition SET ";
		$strConnector = "";
		$strSQL .= $strConnector . "intProcessTransitionID = ".$this->getDB()->sanitize(self::getProcessTransitionID());
		$strConnector = ",";
		if(isset($this->_intFromProcessStateID)) {
			$strSQL .= $strConnector . "intFromProcessStateID = ".$this->getDB()->sanitize(self::getFromProcessStateID());
			$strConnector = ",";
		}
		if(isset($this->_intToProcessStateID)) {
			$strSQL .= $strConnector . "intToProcessStateID = ".$this->getDB()->sanitize(self::getToProcessStateID());
			$strConnector = ",";
		}
		if(isset($this->_intConditionID)) {
			$strSQL .= $strConnector . "intConditionID = ".$this->getDB()->sanitize(self::getConditionID());
			$strConnector = ",";
		}
		if(isset($this->_strTransitionName)) {
			$strSQL .= $strConnector . "strTransitionName = ".$this->getDB()->sanitize(self::getTransitionName());
			$strConnector = ",";
		}
		if(isset($this->_strTransitionButton)) {
			$strSQL .= $strConnector . "strTransitionButton = ".$this->getDB()->sanitize(self::getTransitionButton());
			$strConnector = ",";
		}
		if(isset($this->_txtDescription)) {
			$strSQL .= $strConnector . "txtDescription = ".$this->getDB()->sanitize(self::getDescription());
			$strConnector = ",";
		}
		if(isset($this->_strJSCallbackFunction)) {
			$strSQL .= $strConnector . "strJSCallbackFunction = ".$this->getDB()->sanitize(self::getJSCallbackFunction());
			$strConnector = ",";
		}
		if(isset($this->_blnCommentRequired)) {
			$strSQL .= $strConnector . "blnCommentRequired = ".$this->getDB()->sanitize(self::getCommentRequired());
			$strConnector = ",";
		}
		if(isset($this->_blnSendEmail)) {
			$strSQL .= $strConnector . "blnSendEmail = ".$this->getDB()->sanitize(self::getSendEmail());
			$strConnector = ",";
		}
		if(isset($this->_intDisplayOrder)) {
			$strSQL .= $strConnector . "intDisplayOrder = ".$this->getDB()->sanitize(self::getDisplayOrder());
			$strConnector = ",";
		}
		$strSQL .= " WHERE intProcessTransitionID = ".$this->getDB()->sanitize(self::getProcessTransitionID())."";
		//echo $strSQL;
		return $this->getDB()->query($strSQL);
	}

	public function save() {
		if($this->_intProcessTransitionID) {
			return self::update();
		} else {
			return self::insert();
		}
	}

	public function delete() {
		if($this->_intProcessTransitionID) {
			base::write_log("Process transition deleted","S");
			$strSQL = "DELETE FROM dbSystem.tblProcessTransition
				WHERE intProcessTransitionID = '$this->_intProcessTransitionID'
				";
			return $this->getDB()->query($strSQL);
		}
	}

	public function load($intProcessTransitionID) {
		if(!$intProcessTransitionID) {
			return false;
		}

		$strSQL = "SELECT *
				FROM dbSystem.tblProcessTransition
				WHERE intProcessTransitionID = ".self::getDB()->sanitize($intProcessTransitionID)."
				LIMIT 1
			";
		$rsResult = $this->getDB()->query($strSQL);
		$arrRow = $this->getDB()->fetch_assoc($rsResult);
		$this->setVarsFromRow($arrRow);
	}

	function setVarsFromRow($arrRow) {
		if(isset($arrRow["intProcessTransitionID"])) $this->_intProcessTransitionID = $arrRow["intProcessTransitionID"];
		if(isset($arrRow["intFromProcessStateID"])) $this->_intFromProcessStateID = $arrRow["intFromProcessStateID"];
		if(isset($arrRow["intToProcessStateID"])) $this->_intToProcessStateID = $arrRow["intToProcessStateID"];
		if(isset($arrRow["intConditionID"])) $this->_intConditionID = $arrRow["intConditionID"];
		if(isset($arrRow["strTransitionName"])) $this->_strTransitionName = $arrRow["strTransitionName"];
		if(isset($arrRow["strTransitionButton"])) $this->_strTransitionButton = $arrRow["strTransitionButton"];
		if(isset($arrRow["txtDescription"])) $this->_txtDescription = $arrRow["txtDescription"];
		if(isset($arrRow["strJSCallbackFunction"])) $this->_strJSCallbackFunction = $arrRow["strJSCallbackFunction"];
		if(isset($arrRow["blnCommentRequired"])) $this->_blnCommentRequired = $arrRow["blnCommentRequired"];
		if(isset($arrRow["blnSendEmail"])) $this->_blnSendEmail = $arrRow["blnSendEmail"];
		if(isset($arrRow["intDisplayOrder"])) $this->_intDisplayOrder = $arrRow["intDisplayOrder"];
	}

	function getProcessTransitionID() {
		return $this->_intProcessTransitionID;
	}
	function setProcessTransitionID($value) {
		if($this->_intProcessTransitionID !== $value) {
			$this->_intProcessTransitionID = $value;
			$this->_blnDirty = true;
		}
	}

	function getFromProcessStateID() {
		return $this->_intFromProcessStateID;
	}
	function setFromProcessStateID($value) {
		if($this->_intFromProcessStateID !== $value) {
			$this->_intFromProcessStateID = $value;
			$this->_blnDirty = true;
		}
	}

	function getConditionID() {
		return $this->_intConditionID;



	}
	function setConditionID($value) {
		if($this->_intConditionID !== $value) {
			$this->_intConditionID = $value;
			$this->_blnDirty = true;
		}
	}

	function getToProcessStateID() {
		return $this->_intToProcessStateID;
	}
	function setToProcessStateID($value) {
		if($this->_intToProcessStateID !== $value) {
			$this->_intToProcessStateID = $value;
			$this->_blnDirty = true;
		}
	}

	function getTransitionName() {
		return $this->_strTransitionName;
	}
	function setTransitionName($value) {
		if($this->_strTransitionName !== $value) {
			$this->_strTransitionName = $value;
			$this->_blnDirty = true;
		}
	}

	function getTransitionButton() {
		return $this->_strTransitionButton;
	}
	function setTransitionButton($value) {
		if($this->_strTransitionButton !== $value) {
			$this->_strTransitionButton = $value;
			$this->_blnDirty = true;

		}
	}

	function getDescription() {
		return $this->_txtDescription;
	}
	function setDescription($value) {
		if($this->_txtDescription !== $value) {
			$this->_txtDescription = $value;
			$this->_blnDirty = true;
		}
	}

	function getJSCallbackFunction() {
		return $this->_strJSCallbackFunction;
	}
	function setJSCallbackFunction($value) {
		if($this->_strJSCallbackFunction !== $value) {
			$this->_strJSCallbackFunction = $value;
			$this->_blnDirty = true;
		}
	}

	function getCommentRequired() {
		return $this->_blnCommentRequired;
	}
	function setCommentRequired($value) {
		if($this->_blnCommentRequired !== $value) {
			$this->_blnCommentRequired = $value;
			$this->_blnDirty = true;
		}
	}

	function getSendEmail() {
		return $this->_blnSendEmail;
	}
	function setSendEmail($value) {
		if($this->_blnSendEmail !== $value) {
			$this->_blnSendEmail = $value;
			$this->_blnDirty = true;
		}
	}

	function getDisplayOrder() {
		return $this->_intDisplayOrder;
	}
	function setDisplayOrder($value) {
		if($this->_intDisplayOrder !== $value) {
			$this->_intDisplayOrder = $value;
			$this->_blnDirty = true;
		}
	}
}

class ProcessTransition extends ProcessTransitionBase {
	protected $_objFromProcessState;
	protected $_objToProcessState;
	protected $_objCondition;
	protected $_objConditionArray;
	protected $_objProcessTransitionInstance;
	protected $_objProcessPathTransition;

	function __construct($intProcessTransitionID=null) {
		parent::__construct($intProcessTransitionID);
	}

	/*function loadConditions($intConditionID) {
		if(!$intConditionID)
			return;

		$strSQL = "SELECT tblCondition.*
			FROM dbSystem.tblConditionXR
			INNER JOIN dbSystem.tblCondition
				ON tblCondition.intConditionID = tblConditionXR.intChildConditionID
			WHERE tblConditionXR.intParentConditionID = ".self::getDB()->sanitize($intConditionID);
		$rsResult = $this->getDB()->query($strSQL);

		while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
			$this->getConditionArray()->addFromRow($arrRow, "intConditionID");
			$this->getConditionArray()->getChildConditionArray()->addFromRow($arrRow, "")
		}
	}*/

	function validate() {
		$arrErrors = array();

		if(strlen($this->getTransitionName()) < 2) {
			$arrErrors[] = "Transition Name must be at least 2 characters long.";
		}
		if(strlen($this->getTransitionButton()) < 2) {
			$arrErrors[] = "Transition Button must be at least 2 characters long.";
		}
		if(!$this->getFromProcessStateID()) {
			$arrErrors[] = "From Process State must be specified.";
		}
		if(!$this->getToProcessStateID()) {
			$arrErrors[] = "To Process State must be specified.";
		}

		return $arrErrors;
	}

	function getConditionText() {
		if(!$this->getConditionArray()->getArray() || !$this->getConditionID())
			return;

		return $this->getConditionArray()->getObject($this->getConditionID())->output($this->getConditionArray());
	}

	function getID() {
		return $this->getProcessTransitionID();
	}

	function getFromProcessState($blnLoad=false) {
		include_once("ProcessState.class.php");
		if(!isset($this->_objFromProcessState)) {
			$this->_objFromProcessState = new ProcessState();
		}
		if($blnLoad && $this->getFromProcessStateID() && !$this->_objFromProcessState->getProcessStateID()) {
			$this->_objFromProcessState->load($this->getFromProcessStateID());
		}
		return $this->_objFromProcessState;
	}
	function getToProcessState($blnLoad=false) {
		include_once("ProcessState.class.php");
		if(!isset($this->_objToProcessState)) {
			$this->_objToProcessState = new ProcessState();
		}
		if($blnLoad && $this->getToProcessStateID() && !$this->_objToProcessState->getProcessStateID()) {
			$this->_objToProcessState->load($this->getToProcessStateID());
		}
		return $this->_objToProcessState;
	}
	function getCondition() {
		include_once("Condition.class.php");
		if(!isset($this->_objCondition)) {
			$this->_objCondition = new Condition();
		}
		return $this->_objCondition;
	}
	function getConditionArray() {
		include_once("Condition.class.php");
		if(!isset($this->_objConditionArray)) {
			$this->_objConditionArray = new ConditionArray();
		}
		return $this->_objConditionArray;
	}
	function getProcessTransitionInstance() {
		include_once("ProcessTransitionInstance.class.php");
		if(!isset($this->_objProcessTransitionInstance)) {
			$this->_objProcessTransitionInstance = new ProcessTransitionInstance();
		}
		return $this->_objProcessTransitionInstance;
	}
	function getProcessPathTransition() {
		include_once("ProcessPathTransition.class.php");
		if(!isset($this->_objProcessPathTransition)) {
			$this->_objProcessPathTransition = new ProcessPathTransition();
		}
		return $this->_objProcessPathTransition;
	}
}
?>
