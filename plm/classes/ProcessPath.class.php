<?php
require_once("ArrayClass.class.php");

class ProcessPathArray extends ArrayClass {
	function __construct(){
		parent::__construct("ProcessPath");
	}

	function load() {
		$strSQL = " SELECT * FROM dbSystem.tblProcessPath";
		$rsResult = $this->getDB()->query($strSQL);
		while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
			$this->_arrObjects[$arrRow["intProcessPathID"]] = new ProcessPath();
			$this->_arrObjects[$arrRow["intProcessPathID"]]->setVarsFromRow($arrRow);
		}
	}

	function loadByWFProcessID($intWFProcessID) {
		$strSQL = " SELECT tblProcessPath.*, tblProcessPathTransition.*
			FROM dbSystem.tblProcessPath
			INNER JOIN dbSystem.tblProcessPathTransition
				ON tblProcessPath.intProcessPathID = tblProcessPathTransition.intProcessPathID
			WHERE tblProcessPath.intWFProcessID = ".self::getDB()->sanitize($intWFProcessID);
		//echo $strSQL;
		$rsResult = $this->getDB()->query($strSQL);
		while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
			if(!isset($this->_arrObjects[$arrRow["intProcessPathID"]])) {
				$this->_arrObjects[$arrRow["intProcessPathID"]] = new ProcessPath();
				$this->_arrObjects[$arrRow["intProcessPathID"]]->setVarsFromRow($arrRow);
			}
			$this->_arrObjects[$arrRow["intProcessPathID"]]->getProcessPathTransitionArray()->addFromRow($arrRow, "intProcessPathTransitionID");
		}
	}
}

require_once("DataClass.class.php");

class ProcessPathBase extends DataClass {
	protected $_intProcessPathID;
	protected $_intWFProcessID;
	protected $_strPathName;
	protected $_strColorCode;

	function __construct($intProcessPathID=null) {
		$this->DataClass();
		if($intProcessPathID) {
			$this->load($intProcessPathID);
		}
	}

	protected function insert() {
		base::write_log("Process path created","S");
		$strSQL = "INSERT INTO dbSystem.tblProcessPath SET ";
		$strConnector = "";
		$strSQL .= $strConnector . "intProcessPathID = ".$this->getDB()->sanitize(self::getProcessPathID());
		$strConnector = ",";
		if(isset($this->_intWFProcessID)) {
			$strSQL .= $strConnector . "intWFProcessID = ".$this->getDB()->sanitize(self::getWFProcessID());
			$strConnector = ",";
		}
		if(isset($this->_strPathName)) {
			$strSQL .= $strConnector . "strPathName = ".$this->getDB()->sanitize(self::getPathName());
			$strConnector = ",";
		}
		if(isset($this->_strColorCode)) {
			$strSQL .= $strConnector . "strColorCode = ".$this->getDB()->sanitize(self::getColorCode());
			$strConnector = ",";
		}
		//echo $strSQL;
		$this->getDB()->query($strSQL);
		$this->setProcessPathID($this->getDB()->insert_id());
		return $this->getProcessPathID();
	}

	protected function update() {
		base::write_log("Process path updated","S");
		$strSQL = "UPDATE dbSystem.tblProcessPath SET ";
		$strConnector = "";
		$strSQL .= $strConnector . "intProcessPathID = ".$this->getDB()->sanitize(self::getProcessPathID());
		$strConnector = ",";
		if(isset($this->_intWFProcessID)) {
			$strSQL .= $strConnector . "intWFProcessID = ".$this->getDB()->sanitize(self::getWFProcessID());
			$strConnector = ",";
		}
		if(isset($this->_strPathName)) {
			$strSQL .= $strConnector . "strPathName = ".$this->getDB()->sanitize(self::getPathName());
			$strConnector = ",";
		}
		if(isset($this->_strColorCode)) {
			$strSQL .= $strConnector . "strColorCode = ".$this->getDB()->sanitize(self::getColorCode());
			$strConnector = ",";
		}
		$strSQL .= " WHERE intProcessPathID = ".$this->getDB()->sanitize(self::getProcessPathID())."";
		//echo $strSQL;
		return $this->getDB()->query($strSQL);
	}

	public function save() {
		if($this->_intProcessPathID) {
			return self::update();
		} else {
			return self::insert();
		}
	}

	public function delete() {
		if($this->_intProcessPathID) {
			base::write_log("Process Path Deleted","S");
			$strSQL = "DELETE FROM dbSystem.tblProcessPath
				WHERE intProcessPathID = '$this->_intProcessPathID'
				";
			return $this->getDB()->query($strSQL);
		}
	}

	public function load($intProcessPathID) {
		if(!$intProcessPathID) {
			return false;
		}

		$strSQL = "SELECT *
				FROM dbSystem.tblProcessPath
				WHERE intProcessPathID = ".self::getDB()->sanitize($intProcessPathID)."
				LIMIT 1
			";
		$rsResult = $this->getDB()->query($strSQL);
		$arrRow = $this->getDB()->fetch_assoc($rsResult);
		$this->setVarsFromRow($arrRow);
	}

	function setVarsFromRow($arrRow) {
		if(isset($arrRow["intProcessPathID"])) $this->_intProcessPathID = $arrRow["intProcessPathID"];
		if(isset($arrRow["intWFProcessID"])) $this->_intWFProcessID = $arrRow["intWFProcessID"];
		if(isset($arrRow["strPathName"])) $this->_strPathName = $arrRow["strPathName"];
		if(isset($arrRow["strColorCode"])) $this->_strColorCode = $arrRow["strColorCode"];
	}

	function getProcessPathID() {
		return $this->_intProcessPathID;
	}
	function setProcessPathID($value) {
		if($this->_intProcessPathID !== $value) {
			$this->_intProcessPathID = $value;
			$this->_blnDirty = true;
		}
	}
	function getWFProcessID() {
		return $this->_intWFProcessID;
	}
	function setWFProcessID($value) {
		if($this->_intWFProcessID !== $value) {
			$this->_intWFProcessID = $value;
			$this->_blnDirty = true;
		}
	}
	function getPathName() {
		return $this->_strPathName;
	}
	function setPathName($value) {
		if($this->_strPathName !== $value) {
			$this->_strPathName = $value;
			$this->_blnDirty = true;
		}
	}
	function getColorCode() {
		return $this->_strColorCode;
	}
	function setColorCode($value) {
		if($this->_strColorCode !== $value) {
			$this->_strColorCode = $value;
			$this->_blnDirty = true;
		}
	}
}

class ProcessPath extends ProcessPathBase {
	protected $_objProcessPathTransitionArray;

	function __construct($intProcessPathID=null) {
		parent::__construct($intProcessPathID);
	}

	function getProcessPathTransitionArray() {
		include_once("ProcessPathTransition.class.php");
		if(!$this->_objProcessPathTransitionArray) {
			$this->_objProcessPathTransitionArray = new ProcessPathTransitionArray();
		}
		return $this->_objProcessPathTransitionArray;
	}
}
?>