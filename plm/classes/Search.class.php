<?php
	class Search {
		private $_objProgramListPage;
		private $_objProgramTimelineListPage;
		private $_objIssueListPage;
		private $_objProgramMilestoneListPage;
		private $_objToolListPage;
		private $_objContainerListPage;
		private $_objPurchasedProductListPage;
		private $_objManufacturedProductListPage;
		private $_objNotificationListPage;
		private $_objUserListPage;
		private $_objRoleListPage;
		private $_objSecurityItemListPage;
		private $_objPermissionListPage;
		private $_objDocumentListPage;
		private $_objECListPage;
		private $_objOCListPage;
		private $_objPurchaseOrderListPage;
		private $_objPressListPage;
		private $_objPlatingLineListPage;
		private $_objPaintingLineListPage;
		private $_objCPByPressListPage;
		private $_objCPByPlatingLineListPage;
		private $_objCPByPaintingLineListPage;
		private $_objPartListPage;
		private $_objPlatformListPage;
		private $_objAssemblyListPage;

		// Global Filters
		private $_intProgramCommonID;
		private $_intProgramID;
		private $_intProgramTypeID;
		private $_intProgramSubTypeID;
		private $_intCustomerID;
		private $_intFacilityID;
		private $_intMemberUserID;
		private $_intRoleID;
		private $_intSecurityItemID;
		private $_intPlatformID;
		private $_intMilestoneID;
		private $_strToolType;
		private $_intProductTypeID;
		private $_intMachineID;
		private $_intMachineTypeID;

		// Program List Page Specific
		private $_strProgramStatus;
		private $_strProgramRisk;
		private $_intWaitingApprovalUserID;
		private $_blnShowPlants;
		private $_blnShowMinorMilestones;
		private $_blnDesignResponsible;

		// Program Timeline List Page Specific
		private $_dtmStartYear;
		private $_dtmStartWeek;
		private $_dtmEndYear;
		private $_dtmEndWeek;

		// Issue List Page Specific
		private $_intIssueResponsibilityUserID;
		private $_dtmIssueDueDate;
		private $_strIssueStatus;
		private $_strIssueHealth;
		private $_strMilestones;
		private $_strInternalIssue;
		private $_intIssueTypeID;
		private $_intPersonResponsibleUserID;

		// Milestone List Page Specific
		private $_strMeetingStatus;

		// Tool List Page Specific
		private $_intToolCategoryID;
		private $_intToolSupplierID;
		private $_intToolingEngineerUserID;
		private $_strShot;

		// Container List Page Specific
		private $_strContainerName;
		private $_intContainerTypeID;
		private $_intContainerUseID;

		// Document List Page Specific
		private $_intDocumentTypeID;
		private $_strDocumentName;
		private $_intDocumentCreatedByUserID;
		private $_blnShowRevisions;

		// Product List Page Specific
		private $_strProductName;

		// PO Management Specific
		private $_strPOStatus;

		// Time Tracking Specific
		private $_dtmYear;
		private $_dtmWeek;

		// Capacity Planning Specific
		private $_intFirstYear;
		private $_intLastYear;
		private $_intTonnage;
		private $_strPressSubType;
		private $_intBedSizeLR;
		private $_intBedSizeFB;
		private $_intMaxCoilWidth;
		private $_intWindowSizeLR;
		private $_intStrokeLength;
		private $_blnShowMachines;
		private $_blnShowParts;
		private $_blnShowMonths;
		private $_blnShowAdditionalFields;
		private $_strOutsourced;

		// Part Management Specific
		private $_strPartNumber;
		private $_strAssemblyNumber;
		private $_strPlatformStatus;

		function clearFilters($strListPage) {
			switch($strListPage) {
				case "Program":
					$this->setPlatformID(null);
					$this->setProgramTypeID(null);
					$this->setProgramSubTypeID(null);
					$this->setCustomerID(null);
					$this->setFacilityID(null);
					$this->setMemberUserID(null);
					$this->setProgramStatus(null);
					$this->setProgramRisk(null);
					$this->setWaitingApprovalUserID(null);
					$this->setDesignResponsible(null);
					break;
				case "Program Timeline":
					$this->setStartWeek(null);
					$this->setEndWeek(null);
					break;
				case "Issue":
					$this->setPlatformID(null);
					$this->setCustomerID(null);
					$this->setProgramID(null);
					$this->setIssueStatus(null);
					$this->setIssueHealth(null);
					$this->setMilestones(null);
					$this->setInternalIssue(null);
					$this->setIssueTypeID(null);
					$this->setMilestoneID(null);
					$this->setPersonResponsibleUserID(null);
					break;
				case "Program Milestone":
					$this->setCustomerID(null);
					$this->setProgramTypeID(null);
					$this->setProgramID(null);
					$this->setMilestoneID(null);
					$this->setMeetingStatus(null);
					break;
				case "Tool":
					$this->setToolType(null);
					$this->setToolCategoryID(null);
					$this->setToolSupplierID(null);
					$this->setToolingEngineerUserID(null);
					$this->setCustomerID(null);
					$this->setProgramTypeID(null);
					$this->setFacilityID(null);
					$this->setProgramID(null);
					$this->setShot(null);
					break;
				case "Container":
					$this->setContainerName(null);
					$this->setFacilityID(null);
					$this->setContainerTypeID(null);
					$this->setContainerUseID(null);
					break;
				case "Purchased Product":
					$this->setProductTypeID(null);
					$this->setProductName(null);
					break;
				case "Manufactured Product":
					$this->setProductTypeID(null);
					$this->setProductName(null);
					break;
				case "Notification":
					break;
				case "Document":
					$this->setProgramCommonID(null);
					$this->setDocumentTypeID(null);
					$this->setDocumentName(null);
					$this->setDocumentCreatedByUserID(null);
					$this->setShowRevisions(null);
					break;
				case "EC":
					break;
				case "OC":
					break;
				case "Purchase Order":
					$this->getPOStatus(null);
					break;
				case "Capacity Planning By Press":
					$this->setFacilityID(null);
					$this->setFirstYear(null);
					$this->setLastYear(null);
					$this->setTonnage(null);
					$this->setPressSubType(null);
					$this->setBedSizeLR(null);
					$this->setBedSizeFB(null);
					$this->setMaxCoilWidth(null);
					$this->setWindowSizeLR(null);
					$this->setStrokeLength(null);
					$this->setShowMachines(null);
					$this->setShowParts(null);
					$this->setShowMonths(null);
					$this->setShowAdditionalFields(null);
					$this->setOutsourced(null);
					break;
				case "Capacity Planning By Plating Line":
				case "Capacity Planning By Painting Line":
					$this->setFacilityID(null);

					$this->setFirstYear(null);
					$this->setLastYear(null);
					$this->setShowMachines(null);
					$this->setShowParts(null);
					$this->setShowMonths(null);
					$this->setShowAdditionalFields(null);
					$this->setOutsourced(null);
					break;
				case "Press":
					$this->setFacilityID(null);
					$this->setTonnage(null);
					$this->setPressSubType(null);
					break;
				case "Plating Line":
				case "Painting Line":
					$this->setFacilityID(null);
					break;
				case "Part":
					$this->setPartNumber(null);
					$this->setAssemblyNumber(null);
					$this->setFacilityID(null);
					$this->setMachineTypeID(null);
					$this->setMachineID(null);
					$this->setPlatformID(null);
					$this->setPlatformStatus(null);
					break;
				case "Platform":
					break;
				case "Assembly":
					$this->setFacilityID(null);
					$this->setAssemblyNumber(null);
					break;
			}
		}

		function getProgramListPage() {
			if(!$this->_objProgramListPage)
				$this->_objProgramListPage = new ListPage();
			return $this->_objProgramListPage;
		}
		function getIssueListPage() {
			if(!$this->_objIssueListPage)
				$this->_objIssueListPage = new ListPage();
			return $this->_objIssueListPage;
		}
		function getProgramMilestoneListPage() {
			if(!$this->_objProgramMilestoneListPage)
				$this->_objProgramMilestoneListPage = new ListPage();
			return $this->_objProgramMilestoneListPage;
		}
		function getToolListPage() {
			if(!$this->_objToolListPage)
				$this->_objToolListPage = new ListPage();
			return $this->_objToolListPage;
		}
		function getContainerListPage() {
			if(!$this->_objContainerListPage)
				$this->_objContainerListPage = new ListPage();
			return $this->_objContainerListPage;
		}
		function getPurchasedProductListPage() {
			if(!$this->_objPurchasedProductListPage)
				$this->_objPurchasedProductListPage = new ListPage();
			return $this->_objPurchasedProductListPage;
		}
		function getManufacturedProductListPage() {
			if(!$this->_objManufacturedProductListPage)
				$this->_objManufacturedProductListPage = new ListPage();
			return $this->_objManufacturedProductListPage;
		}
		function getNotificationListPage() {
			if(!$this->_objNotificationListPage)
				$this->_objNotificationListPage = new ListPage();
			return $this->_objNotificationListPage;
		}
		function getUserListPage() {
			if(!$this->_objUserListPage)
				$this->_objUserListPage = new ListPage();
			return $this->_objUserListPage;
		}
		function getRoleListPage() {
			if(!$this->_objRoleListPage)
				$this->_objRoleListPage = new ListPage();
			return $this->_objRoleListPage;
		}
		function getSecurityItemListPage() {
			if(!$this->_objSecurityItemListPage)
				$this->_objSecurityItemListPage = new ListPage();
			return $this->_objSecurityItemListPage;
		}
		function getPermissionListPage() {
			if(!$this->_objPermissionListPage)
				$this->_objPermissionListPage = new ListPage();
			return $this->_objPermissionListPage;
		}
		function getDocumentListPage() {
			if(!$this->_objDocumentListPage)
				$this->_objDocumentListPage = new ListPage();
			return $this->_objDocumentListPage;
		}
		function getECListPage() {
			if(!$this->_objECListPage)
				$this->_objECListPage = new ListPage();
			return $this->_objECListPage;
		}
		function getOCListPage() {
			if(!$this->_objOCListPage)
				$this->_objOCListPage = new ListPage();
			return $this->_objOCListPage;
		}
		function getPurchaseOrderListPage() {
			if(!$this->_objPurchaseOrderListPage)
				$this->_objPurchaseOrderListPage = new ListPage();
			return $this->_objPurchaseOrderListPage;
		}
		function getPressListPage() {
			if(!$this->_objPressListPage)
				$this->_objPressListPage = new ListPage();
			return $this->_objPressListPage;
		}
		function getPlatingLineListPage() {
			if(!$this->_objPlatingLineListPage)
				$this->_objPlatingLineListPage = new ListPage();
			return $this->_objPlatingLineListPage;
		}
		function getPaintingLineListPage() {
			if(!$this->_objPaintingLineListPage)
				$this->_objPaintingLineListPage = new ListPage();
			return $this->_objPaintingLineListPage;
		}
		function getCPByPressListPage() {
			if(!$this->_objCPByPressListPage)
				$this->_objCPByPressListPage = new ListPage();
			return $this->_objCPByPressListPage;
		}
		function getCPByPlatingLineListPage() {
			if(!$this->_objCPByPlatingLineListPage)
				$this->_objCPByPlatingLineListPage = new ListPage();
			return $this->_objCPByPlatingLineListPage;
		}
		function getCPByPaintingLineListPage() {
			if(!$this->_objCPByPaintingLineListPage)
				$this->_objCPByPaintingLineListPage = new ListPage();
			return $this->_objCPByPaintingLineListPage;
		}
		function getPartListPage() {
			if(!$this->_objPartListPage)
				$this->_objPartListPage = new ListPage();
			return $this->_objPartListPage;
		}
		function getPlatformListPage() {
			if(!$this->_objPlatformListPage)
				$this->_objPlatformListPage = new ListPage();
			return $this->_objPlatformListPage;
		}
		function getAssemblyListPage() {
			if(!$this->_objAssemblyListPage)
				$this->_objAssemblyListPage = new ListPage();
			return $this->_objAssemblyListPage;
		}

		function getProgramCommonID() {
			return $this->_intProgramCommonID;
		}
		function setProgramCommonID($value) {
			if($this->_intProgramCommonID !== $value) {
				$this->_intProgramCommonID = $value;
				$this->_blnDirty = true;
			}
		}
		function getProgramID() {
			return $this->_intProgramID;
		}
		function setProgramID($value) {
			if($this->_intProgramID !== $value) {
				$this->_intProgramID = $value;
				$this->_blnDirty = true;
			}
		}
		function getProgramTypeID() {
			return $this->_intProgramTypeID;
		}
		function setProgramTypeID($value) {
			if($this->_intProgramTypeID !== $value) {
				$this->_intProgramTypeID = $value;
				$this->_blnDirty = true;
			}
		}
		function getProgramSubTypeID() {
			return $this->_intProgramSubTypeID;
		}
		function setProgramSubTypeID($value) {
			if($this->_intProgramSubTypeID !== $value) {
				$this->_intProgramSubTypeID = $value;
				$this->_blnDirty = true;
			}
		}
		function getCustomerID() {
			return $this->_intCustomerID;
		}
		function setCustomerID($value) {
			if($this->_intCustomerID !== $value) {
				$this->_intCustomerID = $value;
				$this->_blnDirty = true;
			}
		}
		function getFacilityID() {
			return $this->_intFacilityID;
		}
		function setFacilityID($value) {
			if($this->_intFacilityID !== $value) {
				$this->_intFacilityID = $value;
				$this->_blnDirty = true;
			}
		}
		function getMemberUserID() {
			return $this->_intMemberUserID;
		}
		function setMemberUserID($value) {
			if($this->_intMemberUserID !== $value) {
				$this->_intMemberUserID = $value;
				$this->_blnDirty = true;
			}
		}

		function getStartYear() {
			return $this->_dtmStartYear;
		}
		function setStartYear($value) {
			if($this->_dtmStartYear !== $value) {
				$this->_dtmStartYear = $value;
				$this->_blnDirty = true;
			}
		}
		function getStartWeek() {
			return $this->_dtmStartWeek;
		}
		function setStartWeek($value) {
			if($this->_dtmStartWeek !== $value) {
				$this->_dtmStartWeek = $value;
				$this->_blnDirty = true;
			}
		}
		function getEndYear() {
			return $this->_dtmEndYear;
		}
		function setEndYear($value) {
			if($this->_dtmEndYear !== $value) {
				$this->_dtmEndYear = $value;
				$this->_blnDirty = true;
			}
		}
		function getEndWeek() {
			return $this->_dtmEndWeek;
		}
		function setEndWeek($value) {
			if($this->_dtmEndWeek !== $value) {
				$this->_dtmEndWeek = $value;
				$this->_blnDirty = true;
			}
		}

		function getRoleID() {
			return $this->_intRoleID;
		}
		function setRoleID($value) {
			if($this->_intRoleID !== $value) {
				$this->_intRoleID = $value;
				$this->_blnDirty = true;
			}
		}
		function getSecurityItemID() {
			return $this->_intSecurityItemID;
		}
		function setSecurityItemID($value) {
			if($this->_intSecurityItemID !== $value) {
				$this->_intSecurityItemID = $value;
				$this->_blnDirty = true;
			}
		}
		function getPlatformID() {
			return $this->_intPlatformID;
		}
		function setPlatformID($value) {
			if($this->_intPlatformID !== $value) {
				$this->_intPlatformID = $value;
				$this->_blnDirty = true;
			}
		}
		function getMilestoneID() {
			return $this->_intMilestoneID;
		}
		function setMilestoneID($value) {
			if($this->_intMilestoneID !== $value) {
				$this->_intMilestoneID = $value;
				$this->_blnDirty = true;
			}
		}
		function getToolType() {
			return $this->_strToolType;
		}
		function setToolType($value) {
			if($this->_strToolType !== $value) {
				$this->_strToolType = $value;
				$this->_blnDirty = true;
			}
		}
		function getProductTypeID() {
			return $this->_intProductTypeID;
		}
		function setProductTypeID($value) {
			if($this->_intProductTypeID !== $value) {
				$this->_intProductTypeID = $value;
				$this->_blnDirty = true;
			}
		}
		function getMachineID() {
			return $this->_intMachineID;
		}
		function setMachineID($value) {
			if($this->_intMachineID !== $value) {
				$this->_intMachineID = $value;
				$this->_blnDirty = true;
			}
		}
		function getMachineTypeID() {
			return $this->_intMachineTypeID;
		}
		function setMachineTypeID($value) {
			if($this->_intMachineTypeID !== $value) {
				$this->_intMachineTypeID = $value;
				$this->_blnDirty = true;
			}
		}

		function getProgramStatus() {
			return $this->_strProgramStatus;
		}
		function setProgramStatus($value) {
			if($this->_strProgramStatus !== $value) {
				$this->_strProgramStatus = $value;
				$this->_blnDirty = true;
			}
		}
		function getProgramRisk() {
			return $this->_strProgramRisk;
		}
		function setProgramRisk($value) {
			if($this->_strProgramRisk !== $value) {
				$this->_strProgramRisk = $value;
				$this->_blnDirty = true;
			}
		}
		function getWaitingApprovalUserID() {
			return $this->_intWaitingApprovalUserID;
		}
		function setWaitingApprovalUserID($value) {
			if($this->_intWaitingApprovalUserID !== $value) {
				$this->_intWaitingApprovalUserID = $value;
				$this->_blnDirty = true;
			}
		}
		function getShowPlants() {
			return $this->_blnShowPlants;
		}
		function setShowPlants($value) {
			if($this->_blnShowPlants !== $value) {
				$this->_blnShowPlants = $value;
				$this->_blnDirty = true;
			}
		}
		function getShowMinorMilestones() {
			return $this->_blnShowMinorMilestones;
		}
		function setShowMinorMilestones($value) {
			if($this->_blnShowMinorMilestones !== $value) {
				$this->_blnShowMinorMilestones = $value;
				$this->_blnDirty = true;
			}
		}
		function getDesignResponsible() {
			return $this->_blnDesignResponsible;
		}
		function setDesignResponsible($value) {
			if($this->_blnDesignResponsible !== $value) {
				$this->_blnDesignResponsible = $value;
				$this->_blnDirty = true;
			}
		}

		function getIssueResponsibilityUserID() {
			return $this->_intIssueResponsibilityUserID;
		}
		function setIssueResponsibilityUserID($value) {
			if($this->_intIssueResponsibilityUserID !== $value) {
				$this->_intIssueResponsibilityUserID = $value;
				$this->_blnDirty = true;
			}
		}
		function getIssueDueDate() {
			return $this->_dtmIssueDueDate;
		}
		function setIssueDueDate($value) {
			if($this->_dtmIssueDueDate !== $value) {

				$this->_dtmIssueDueDate = $value;
				$this->_blnDirty = true;
			}
		}
		function getIssueStatus() {
			return $this->_strIssueStatus;
		}

		function setIssueStatus($value) {
			if($this->_strIssueStatus !== $value) {
				$this->_strIssueStatus = $value;
				$this->_blnDirty = true;
			}
		}
		function getIssueHealth() {
			return $this->_strIssueHealth;
		}
		function setIssueHealth($value) {
			if($this->_strIssueHealth !== $value) {
				$this->_strIssueHealth = $value;
				$this->_blnDirty = true;
			}
		}
		function getMilestones() {
			return $this->_strMilestones;
		}
		function setMilestones($value) {
			if($this->_strMilestones !== $value) {
				$this->_strMilestones = $value;
				$this->_blnDirty = true;
			}
		}
		function getInternalIssue() {
			return $this->_strInternalIssue;
		}
		function setInternalIssue($value) {

			if($this->_strInternalIssue !== $value) {
				$this->_strInternalIssue = $value;
				$this->_blnDirty = true;
			}
		}
		function getIssueTypeID() {
			return $this->_intIssueTypeID;
		}
		function setIssueTypeID($value) {
			if($this->_intIssueTypeID !== $value) {
				$this->_intIssueTypeID = $value;
				$this->_blnDirty = true;
			}
		}
		function getPersonResponsibleUserID() {
			return $this->_intPersonResponsibleUserID;
		}
		function setPersonResponsibleUserID($value) {
			if($this->_intPersonResponsibleUserID !== $value) {
				$this->_intPersonResponsibleUserID = $value;
				$this->_blnDirty = true;
			}
		}

		function getMeetingStatus() {
			return $this->_strMeetingStatus;
		}
		function setMeetingStatus($value) {
			if($this->_strMeetingStatus !== $value) {
				$this->_strMeetingStatus = $value;
				$this->_blnDirty = true;
			}
		}

		function getToolCategoryID() {
			return $this->_intToolCategoryID;
		}
		function setToolCategoryID($value) {
			if($this->_intToolCategoryID !== $value) {
				$this->_intToolCategoryID = $value;
				$this->_blnDirty = true;
			}
		}
		function getToolSupplierID() {
			return $this->_intToolSupplierID;
		}
		function setToolSupplierID($value) {
			if($this->_intToolSupplierID !== $value) {
				$this->_intToolSupplierID = $value;
				$this->_blnDirty = true;
			}
		}
		function getToolingEngineerUserID() {
			return $this->_intToolingEngineerUserID;
		}
		function setToolingEngineerUserID($value) {
			if($this->_intToolingEngineerUserID !== $value) {
				$this->_intToolingEngineerUserID = $value;
				$this->_blnDirty = true;
			}
		}
		function getShot() {

			return $this->_strShot;
		}
		function setShot($value) {
			if($this->_strShot !== $value) {
				$this->_strShot = $value;
				$this->_blnDirty = true;
			}
		}

		function getContainerName() {
			return $this->_strContainerName;
		}
		function setContainerName($value) {
			if($this->_strContainerName !== $value) {
				$this->_strContainerName = $value;
				$this->_blnDirty = true;
			}
		}
		function getContainerTypeID() {
			return $this->_intContainerTypeID;
		}
		function setContainerTypeID($value) {
			if($this->_intContainerTypeID !== $value) {
				$this->_intContainerTypeID = $value;
				$this->_blnDirty = true;
			}
		}
		function getContainerUseID() {
			return $this->_intContainerUseID;
		}
		function setContainerUseID($value) {
			if($this->_intContainerUseID !== $value) {
				$this->_intContainerUseID = $value;
				$this->_blnDirty = true;
			}
		}

		function getDocumentTypeID() {
			return $this->_intDocumentTypeID;
		}
		function setDocumentTypeID($value) {
			if($this->_intDocumentTypeID !== $value) {
				$this->_intDocumentTypeID = $value;
				$this->_blnDirty = true;
			}
		}
		function getDocumentName() {
			return $this->_strDocumentName;
		}
		function setDocumentName($value) {
			if($this->_strDocumentName !== $value) {
				$this->_strDocumentName = $value;
				$this->_blnDirty = true;
			}
		}
		function getDocumentCreatedByUserID() {
			return $this->_intDocumentCreatedByUserID;
		}
		function setDocumentCreatedByUserID($value) {
			if($this->_intDocumentCreatedByUserID !== $value) {
				$this->_intDocumentCreatedByUserID = $value;
				$this->_blnDirty = true;
			}
		}
		function getShowRevisions() {
			return $this->_blnShowRevisions;
		}
		function setShowRevisions($value) {
			if($this->_blnShowRevisions !== $value) {
				$this->_blnShowRevisions = $value;
				$this->_blnDirty = true;
			}
		}

		function getProductName() {
			return $this->_strProductName;
		}
		function setProductName($value) {
			if($this->_strProductName !== $value) {
				$this->_strProductName = $value;
				$this->_blnDirty = true;
			}
		}

		function getPOStatus() {
			return $this->_strPOStatus;
		}
		function setPOStatus($value) {
			if($this->_strPOStatus !== $value) {
				$this->_strPOStatus = $value;
				$this->_blnDirty = true;
			}
		}

		function getYear() {
			return $this->_dtmYear;
		}
		function setYear($value) {
			if($this->_dtmYear !== $value) {
				$this->_dtmYear = $value;
				$this->_blnDirty = true;
			}
		}
		function getWeek() {
			return $this->_dtmWeek;
		}
		function setWeek($value) {
			if($this->_dtmWeek !== $value) {
				$this->_dtmWeek = $value;
				$this->_blnDirty = true;
			}
		}

		function getFirstYear() {
			return $this->_intFirstYear;
		}
		function setFirstYear($value) {
			if($this->_intFirstYear !== $value) {
				$this->_intFirstYear = $value;
				$this->_blnDirty = true;
			}
		}
		function getLastYear() {
			return $this->_intLastYear;
		}
		function setLastYear($value) {
			if($this->_intLastYear !== $value) {
				$this->_intLastYear = $value;
				$this->_blnDirty = true;
			}
		}
		function getTonnage() {
			return $this->_intTonnage;
		}
		function setTonnage($value) {
			if($this->_intTonnage !== $value) {

				$this->_intTonnage = $value;
				$this->_blnDirty = true;
			}
		}
		function getPressSubType() {
			return $this->_strPressSubType;
		}
		function setPressSubType($value) {
			if($this->_strPressSubType !== $value) {
				$this->_strPressSubType = $value;
				$this->_blnDirty = true;
			}
		}
		function getBedSizeLR() {
			return $this->_intBedSizeLR;
		}
		function setBedSizeLR($value) {
			if($this->_intBedSizeLR !== $value) {
				$this->_intBedSizeLR = $value;
				$this->_blnDirty = true;
			}
		}
		function getBedSizeFB() {
			return $this->_intBedSizeFB;
		}
		function setBedSizeFB($value) {
			if($this->_intBedSizeFB !== $value) {
				$this->_intBedSizeFB = $value;
				$this->_blnDirty = true;
			}
		}
		function getMaxCoilWidth() {
			return $this->_intMaxCoilWidth;
		}
		function setMaxCoilWidth($value) {
			if($this->_intMaxCoilWidth !== $value) {

				$this->_intMaxCoilWidth = $value;
				$this->_blnDirty = true;
			}
		}
		function getWindowSizeLR() {
			return $this->_intWindowSizeLR;
		}
		function setWindowSizeLR($value) {
			if($this->_intWindowSizeLR !== $value) {
				$this->_intWindowSizeLR = $value;
				$this->_blnDirty = true;
			}
		}
		function getStrokeLength() {
			return $this->_intStrokeLength;
		}
		function setStrokeLength($value) {
			if($this->_intStrokeLength !== $value) {
				$this->_intStrokeLength = $value;
				$this->_blnDirty = true;
			}
		}
		function getShowMachines() {
			return $this->_blnShowMachines;
		}
		function setShowMachines($value) {
			if($this->_blnShowMachines !== $value) {
				$this->_blnShowMachines = $value;
				$this->_blnDirty = true;
			}
		}
		function getShowParts() {
			return $this->_blnShowParts;
		}
		function setShowParts($value) {
			if($this->_blnShowParts !== $value) {
				$this->_blnShowParts = $value;
				$this->_blnDirty = true;
			}
		}
		function getShowMonths() {
			return $this->_blnShowMonths;
		}
		function setShowMonths($value) {
			if($this->_blnShowMonths !== $value) {
				$this->_blnShowMonths = $value;
				$this->_blnDirty = true;
			}
		}
		function getShowAdditionalFields() {
			return $this->_blnShowAdditionalFields;
		}
		function setShowAdditionalFields($value) {
			if($this->_blnShowAdditionalFields !== $value) {
				$this->_blnShowAdditionalFields = $value;
				$this->_blnDirty = true;
			}
		}
		function getOutsourced() {
			return $this->_strOutsourced;
		}
		function setOutsourced($value) {
			if($this->_strOutsourced !== $value) {
				$this->_strOutsourced = $value;
				$this->_blnDirty = true;
			}
		}

		function getPartNumber() {
			return $this->_strPartNumber;
		}
		function setPartNumber($value) {
			if($this->_strPartNumber !== $value) {
				$this->_strPartNumber = $value;
				$this->_blnDirty = true;
			}
		}
		function getAssemblyNumber() {
			return $this->_strAssemblyNumber;
		}
		function setAssemblyNumber($value) {
			if($this->_strAssemblyNumber !== $value) {
				$this->_strAssemblyNumber = $value;
				$this->_blnDirty = true;
			}
		}
		function getPlatformStatus() {
			return $this->_strPlatformStatus;
		}
		function setPlatformStatus($value) {
			if($this->_strPlatformStatus !== $value) {
				$this->_strPlatformStatus = $value;
				$this->_blnDirty = true;
			}
		}
	}

	class ListPage {

		private $_arrSorts;
		private $_arrDefaultSort;
		private $_blnDefaultSet = false; // True if defaults have been set for the List Page (including filtering as well as sorting0

		function getSorts() {
			return $this->_arrSorts;
		}

		function addSort($strSortBy, $strSortOrder=null) {
			if(!$strSortBy)
				return false;

			if($this->_arrSorts) {
				foreach($this->_arrSorts as $intIndex => $objSort) {
					if($objSort->getSortBy() == $strSortBy) {
						$strFlippedSortOrder = $objSort->getSortOrder()=="ASC"?"DESC":"ASC";
						$strSortOrder = ($strSortOrder?$strSortOrder:$strFlippedSortOrder);
						array_splice($this->_arrSorts, $intIndex, 1);
					}
				}
			}

			$strSortOrder = $strSortOrder?$strSortOrder:"ASC";

			$this->_arrSorts[] = new Sort($strSortBy, $strSortOrder);
		}

		function getDefaultSort() {
			return $this->_arrDefaultSort;
		}
		function setDefaultSort($arrDefaultSort) {
			$this->_arrDefaultSort = $arrDefaultSort;
		}
		function useDefaultSort(){
			$this->_arrSorts = $this->_arrDefaultSort;
		}

		function getDefaultSet() {

			return $this->_blnDefaultSet;
		}
		function setDefaultSet($value) {
			$this->_blnDefaultSet = $value;
		}

		function getSortQuery() {
			// TODO: Return in reverse order (this is forward order right now)
			$strSQL = "";
			$strConnector = " ORDER BY ";
			if($this->getSorts()) {
				$arrSorts = $this->getSorts();
				$intSortsCount = count($arrSorts);
				for($i=$intSortsCount-1; $i>=0; $i--) {
					$strSQL .= $strConnector . $arrSorts[$i]->getSortBy() . " " . $arrSorts[$i]->getSortOrder();
					$strConnector = ", ";
				}
			}
			return $strSQL;
		}
	}

	class Sort {
		private $_strSortBy;
		private $_strSortOrder;

		function Sort($strSortBy, $strSortOrder=null) {
			$this->setSortBy($strSortBy);
			$this->setSortOrder($strSortOrder);
		}

		function getSortBy(){
			return $this->_strSortBy;
		}
		function setSortBy($value){
			$this->_strSortBy = $value;
		}

		function getSortOrder(){

			return $this->_strSortOrder;
		}
		function setSortOrder($value=null){
			$this->_strSortOrder = $value;
		}
	}
?>
