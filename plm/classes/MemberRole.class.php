<?php
	require_once("ArrayClass.class.php");

	class MemberRoleArray extends ArrayClass {
		function __construct(){
		}

		function load() {
			$strSQL = " SELECT * FROM tblMemberRole";
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				$this->_arrObjects[$arrRow["intMemberRoleID"]] = new MemberRole();
				$this->_arrObjects[$arrRow["intMemberRoleID"]]->setVarsFromRow($arrRow);
			}
		}

		function getMemberRoleByRoleID($intRoleID){
			if($this->getArray()) {
				foreach($this->getArray() as $strKey => $objObject) {
					if($objObject->getRoleID() == $intRoleID)
						return array($strKey, $objObject);
				}
			}
			return array(false,false);
		}
	}

	require_once("DataClass.class.php");

	class MemberRoleBase extends DataClass {
		protected $_intMemberRoleID;
		protected $_intMemberID;
		protected $_intRoleID;

		function __construct($intMemberRoleID=null) {
			$this->DataClass();
			if($intMemberRoleID) {
				$this->load($intMemberRoleID);
			}
		}

		protected function insert() {
			base::write_log("Member Role created","S");
			$strSQL = "INSERT INTO tblMemberRole SET ";
			$strConnector = "";
			$strSQL .= $strConnector . "intMemberRoleID = ".$this->getDB()->sanitize(self::getMemberRoleID());
			$strConnector = ",";
			if(isset($this->_intMemberID)) {
				$strSQL .= $strConnector . "intMemberID = ".$this->getDB()->sanitize(self::getMemberID());
				$strConnector = ",";
			}
			if(isset($this->_intRoleID)) {
				$strSQL .= $strConnector . "intRoleID = ".$this->getDB()->sanitize(self::getRoleID());
				$strConnector = ",";
			}
			//echo $strSQL;
			$this->getDB()->query($strSQL);
			$this->setMemberRoleID($this->getDB()->insert_id());
			return $this->getMemberRoleID();
		}

		protected function update() {
			base::write_log("Member Role Updated","S");
			$strSQL = "UPDATE tblMemberRole SET ";
			$strConnector = "";
			$strSQL .= $strConnector . "intMemberRoleID = ".$this->getDB()->sanitize(self::getMemberRoleID());
			$strConnector = ",";
			if(isset($this->_intMemberID)) {
				$strSQL .= $strConnector . "intMemberID = ".$this->getDB()->sanitize(self::getMemberID());
				$strConnector = ",";
			}
			if(isset($this->_intRoleID)) {
				$strSQL .= $strConnector . "intRoleID = ".$this->getDB()->sanitize(self::getRoleID());
				$strConnector = ",";
			}
			$strSQL .= " WHERE intMemberRoleID = ".$this->getDB()->sanitize(self::getMemberRoleID())."";
			//echo $strSQL;
			return $this->getDB()->query($strSQL);
		}

		public function save() {
			if($this->_intMemberRoleID) {
				return $this->update();
			} else {
				return $this->insert();
			}
		}

		public function delete() {
			base::write("Member Role Deleted","S");
			if($this->_intMemberRoleID) {
				$strSQL = "DELETE FROM tblMemberRole
					WHERE intMemberRoleID = '$this->_intMemberRoleID'
					";
				return $this->getDB()->query($strSQL);
			}
		}

		public function load($intMemberRoleID) {
			if($intMemberRoleID) {
				$strSQL = "SELECT intMemberRoleID, intMemberID, intRoleID
						FROM tblMemberRole
						WHERE intMemberRoleID = '$intMemberRoleID'
						LIMIT 1
					";
				$rsMemberRole = $this->getDB()->query($strSQL);
				$arrMemberRole = $this->getDB()->fetch_assoc($rsMemberRole);
				$this->setVarsFromRow($arrMemberRole);
			}
		}

		function setVarsFromRow($arrMemberRole) {
			if(isset($arrMemberRole["intMemberRoleID"])) $this->_intMemberRoleID = $arrMemberRole["intMemberRoleID"];
			if(isset($arrMemberRole["intMemberID"])) $this->_intMemberID = $arrMemberRole["intMemberID"];
			if(isset($arrMemberRole["intRoleID"])) $this->_intRoleID = $arrMemberRole["intRoleID"];
		}

		function getMemberRoleID() {
			return $this->_intMemberRoleID;
		}
		function setMemberRoleID($value) {
			if($this->_intMemberRoleID !== $value) {
				$this->_intMemberRoleID = $value;
				$this->_blnDirty = true;
			}
		}

		function getMemberID() {
			return $this->_intMemberID;
		}
		function setMemberID($value) {
			if($this->_intMemberID !== $value) {
				$this->_intMemberID = $value;
				$this->_blnDirty = true;
			}
		}

		function getRoleID() {
			return $this->_intRoleID;
		}
		function setRoleID($value) {
			if($this->_intRoleID !== $value) {
				$this->_intRoleID = $value;
				$this->_blnDirty = true;
			}
		}

	}

	include_once("Role.class.php");

	class MemberRole extends MemberRoleBase {
		private $_objRole;

		function __construct($intMemberRoleID=null) {
			parent::__construct($intMemberRoleID);
		}


		public function loadByMemberIDAndRoleID($intMemberID, $intRoleID) {
			if(!$intMemberID || !$intRoleID)
				return false;

			$strSQL = "SELECT *
					FROM tblMemberRole
					WHERE intMemberID = ".self::getDB()->sanitize($intMemberID)."
					AND intRoleID = ".self::getDB()->sanitize($intRoleID)."
					LIMIT 1
				";
			$rsMemberRole = $this->getDB()->query($strSQL);
			$arrMemberRole = $this->getDB()->fetch_assoc($rsMemberRole);
			$this->setVarsFromRow($arrMemberRole);
		}

		function getRole(){
			if(!$this->_objRole)
				$this->_objRole = new Role();
			return $this->_objRole;
		}
	}
?>
