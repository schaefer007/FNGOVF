<?php
require_once("ArrayClass.class.php");
require_once("MemberRole.class.php");

class MemberArray extends ArrayClass {
	function __construct(){
	}

	function load() {
		$strSQL = " SELECT * FROM dbPLM.tblMember";
		$rsResult = $this->getDB()->query($strSQL);
		while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
			$this->_arrObjects[$arrRow["intMemberID"]] = new Member();
			$this->_arrObjects[$arrRow["intMemberID"]]->setVarsFromRow($arrRow);
		}
	}

	function loadByProgramID($intProgramID, $objSearch=null){
		if(!$intProgramID)
			return false;

		$strSQL = " SELECT tblRole.*, tblMemberRole.*, tblUser.*, tblMember.*
			FROM dbPLM.tblMember
			INNER JOIN dbPLM.tblProduct
				ON tblMember.intProgramID = tblProduct.intProgramID
			INNER JOIN dbPLM.tblUser
				ON tblUser.intUserID = tblMember.intUserID
			LEFT JOIN dbPLM.tblMemberRole
				ON tblMemberRole.intMemberID = tblMember.intMemberID
			LEFT JOIN dbPLM.tblRole
				ON tblRole.intRoleID = tblMemberRole.intRoleID
			WHERE tblMember.intProgramID = ".$this->getDB()->sanitize($intProgramID);
		$strSQL .= " ORDER BY tblUser.strDisplayName ";
		//echo $strSQL;
		$rsResult = $this->getDB()->query($strSQL);
		while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
			if(!isset($this->_arrObjects[$arrRow["intMemberID"]])) {
				$this->_arrObjects[$arrRow["intMemberID"]] = new Member();
				$this->_arrObjects[$arrRow["intMemberID"]]->setVarsFromRow($arrRow);
				$this->_arrObjects[$arrRow["intMemberID"]]->getUser()->setVarsFromRow($arrRow);
			}
			if($arrRow["intMemberRoleID"]) {
				$objMemberRole = new MemberRole();
				$objMemberRole->setVarsFromRow($arrRow);
				$objMemberRole->getRole()->setVarsFromRow($arrRow);
				$this->_arrObjects[$arrRow["intMemberID"]]->getMemberRoleArray()->setObject($objMemberRole->getMemberRoleID(), $objMemberRole);
			}
		}
	}

	function getMemberByUserID($intUserID){
		if($this->getArray()) {
			foreach($this->getArray() as $strKey => $objObject) {
				if($objObject->getUserID() == $intUserID)
					return array($strKey, $objObject);
			}
		}
		return array(false,false);
	}
}

require_once("DataClass.class.php");

class MemberBase extends DataClass {
	protected $_intMemberID;
	protected $_intProgramID;
	protected $_intUserID;

	function __construct($intMemberID=null) {
		$this->DataClass();
		if($intMemberID) {
			$this->load($intMemberID);
		}
	}

	protected function insert() {
		base::write_log("Member created","S");
		$strSQL = "INSERT INTO tblMember SET ";
		$strConnector = "";
		$strSQL .= $strConnector . "intMemberID = ".$this->getDB()->sanitize(self::getMemberID());
		$strConnector = ",";
		if(isset($this->_intProgramID)) {
			$strSQL .= $strConnector . "intProgramID = ".$this->getDB()->sanitize(self::getProgramID());
			$strConnector = ",";
		}
		if(isset($this->_intUserID)) {
			$strSQL .= $strConnector . "intUserID = ".$this->getDB()->sanitize(self::getUserID());
			$strConnector = ",";
		}
		//echo $strSQL;
		$this->getDB()->query($strSQL);
		$this->setMemberID($this->getDB()->insert_id());
		return $this->getMemberID();
	}

	protected function update() {
		base::write_log("Member Update","S");
		$strSQL = "UPDATE tblMember SET ";
		$strConnector = "";
		$strSQL .= $strConnector . "intMemberID = ".$this->getDB()->sanitize(self::getMemberID());
		$strConnector = ",";
		if(isset($this->_intProgramID)) {
			$strSQL .= $strConnector . "intProgramID = ".$this->getDB()->sanitize(self::getProgramID());
			$strConnector = ",";
		}
		if(isset($this->_intUserID)) {
			$strSQL .= $strConnector . "intUserID = ".$this->getDB()->sanitize(self::getUserID());
			$strConnector = ",";
		}
		$strSQL .= " WHERE intMemberID = ".$this->getDB()->sanitize(self::getMemberID())."";
		//echo $strSQL;
		return $this->getDB()->query($strSQL);
	}

	public function save() {
		if($this->_intMemberID) {
			return $this->update();
		} else {
			return $this->insert();
		}
	}

	public function delete() {
		base::write_log("Member deleted","S");
		if($this->_intMemberID) {
			$strSQL = "DELETE FROM tblMember
				WHERE intMemberID = '$this->_intMemberID'
				";
			return $this->getDB()->query($strSQL);
		}
	}

	public function load($intMemberID) {
		if($intMemberID) {
			$strSQL = "SELECT intMemberID, intProgramID, intUserID
					FROM tblMember
					WHERE intMemberID = '$intMemberID'
					LIMIT 1
				";
			$rsMember = $this->getDB()->query($strSQL);
			$arrMember = $this->getDB()->fetch_assoc($rsMember);
			$this->setVarsFromRow($arrMember);
		}
	}

	function setVarsFromRow($arrMember) {
		if(isset($arrMember["intMemberID"])) $this->_intMemberID = $arrMember["intMemberID"];
		if(isset($arrMember["intProgramID"])) $this->_intProgramID = $arrMember["intProgramID"];
		if(isset($arrMember["intUserID"])) $this->_intUserID = $arrMember["intUserID"];
	}

	function getMemberID() {
		return $this->_intMemberID;
	}
	function setMemberID($value) {
		if($this->_intMemberID !== $value) {
			$this->_intMemberID = $value;
			$this->_blnDirty = true;
		}
	}

	function getProgramID() {
		return $this->_intProgramID;
	}
	function setProgramID($value) {
		if($this->_intProgramID !== $value) {
			$this->_intProgramID = $value;
			$this->_blnDirty = true;
		}
	}

	function getUserID() {
		return $this->_intUserID;
	}
	function setUserID($value) {
		if($this->_intUserID !== $value) {
			$this->_intUserID = $value;
			$this->_blnDirty = true;
		}
	}

}

include_once("User.class.php");
include_once("MemberRole.class.php");

class Member extends MemberBase {
	private $_objUser;
	private $_objMemberRoleArray;

	function __construct($intMemberID=null) {
		parent::__construct($intMemberID);
	}

	function insert(){
		$objMember = new Member();
		$objMember->loadByProgramIDAndUserID($this->getProgramID(), $this->getUserID());
		if($objMember->getMemberID()) {
			$this->setMemberID($objMember->getMemberID());
		} else {
			parent::insert();
		}
	}

	function getUser(){
		if(!$this->_objUser) {
			$this->_objUser = new User();
		}
		return $this->_objUser;
	}
	function getMemberRoleArray(){
		if(!$this->_objMemberRoleArray) {
			$this->_objMemberRoleArray = new MemberRoleArray();
		}
		return $this->_objMemberRoleArray;
	}

	function loadByProgramIDAndUserID($intProgramID, $intUserID){
		if(!$intProgramID || !$intUserID)
			return false;

		$strSQL = "SELECT *
			FROM tblMember
			WHERE intProgramID = ".$this->getDB()->sanitize($intProgramID)."
			AND intUserID = ".$this->getDB()->sanitize($intUserID)."
			LIMIT 1
		";
		$rsMember = $this->getDB()->query($strSQL);
		$arrMember = $this->getDB()->fetch_assoc($rsMember);
		$this->setVarsFromRow($arrMember);
	}
}
?>
