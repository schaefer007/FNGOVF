<?php
require_once("ArrayClass.class.php");

class NotificationArray extends ArrayClass {
	function __construct(){
	}

	function load() {
		$strSQL = " SELECT * FROM dbSystem.tblNotification";
		$rsResult = $this->getDB()->query($strSQL);
		while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
			$this->_arrObjects[$arrRow["intNotificationID"]] = new Notification();
			$this->_arrObjects[$arrRow["intNotificationID"]]->setVarsFromRow($arrRow);
		}
	}
}

require_once("DataClass.class.php");

class NotificationBase extends DataClass {
	protected $_intNotificationID;
	protected $_strSubject;
	protected $_txtBody;
	protected $_intNotificationTypeID;
	protected $_intCreatedByUserID;

	function __construct($intNotificationID=null) {
		$this->DataClass();
		if($intNotificationID) {
			$this->load($intNotificationID);
		}
	}

	protected function insert() {
		base::write_log("Notification created","S");
		$strSQL = "INSERT INTO dbSystem.tblNotification SET ";
		$strConnector = "";
		$strSQL .= $strConnector . "intNotificationID = ".$this->getDB()->sanitize(self::getNotificationID());
		$strConnector = ",";
		if(isset($this->_strSubject)) {
			$strSQL .= $strConnector . "strSubject = ".$this->getDB()->sanitize(self::getSubject());
			$strConnector = ",";
		}
		if(isset($this->_txtBody)) {
			$strSQL .= $strConnector . "txtBody = ".$this->getDB()->sanitize(self::getBody());
			$strConnector = ",";
		}
		if(isset($this->_intNotificationTypeID)) {
			$strSQL .= $strConnector . "intNotificationTypeID = ".$this->getDB()->sanitize(self::getNotificationTypeID());
			$strConnector = ",";
		}
		if(isset($this->_intCreatedByUserID)) {
			$strSQL .= $strConnector . "intCreatedByUserID = ".$this->getDB()->sanitize(self::getCreatedByUserID());
			$strConnector = ",";
		}
		//echo $strSQL . "<br /><br />";
		$this->getDB()->query($strSQL);
		$this->setNotificationID($this->getDB()->insert_id());
		return $this->getNotificationID();
	}

	protected function update() {
		base::write_log("Notification updated","S");
		$strSQL = "UPDATE dbSystem.tblNotification SET ";
		$strConnector = "";
		$strSQL .= $strConnector . "intNotificationID = ".$this->getDB()->sanitize(self::getNotificationID());
		$strConnector = ",";
		if(isset($this->_strSubject)) {
			$strSQL .= $strConnector . "strSubject = ".$this->getDB()->sanitize(self::getSubject());
			$strConnector = ",";
		}
		if(isset($this->_txtBody)) {
			$strSQL .= $strConnector . "txtBody = ".$this->getDB()->sanitize(self::getBody());
			$strConnector = ",";
		}
		if(isset($this->_intNotificationTypeID)) {
			$strSQL .= $strConnector . "intNotificationTypeID = ".$this->getDB()->sanitize(self::getNotificationTypeID());
			$strConnector = ",";
		}
		if(isset($this->_intCreatedByUserID)) {
			$strSQL .= $strConnector . "intCreatedByUserID = ".$this->getDB()->sanitize(self::getCreatedByUserID());
			$strConnector = ",";
		}
		$strSQL .= " WHERE intNotificationID = ".$this->getDB()->sanitize(self::getNotificationID())."";
		//echo $strSQL;
		return $this->getDB()->query($strSQL);
	}

	public function save() {
		if($this->_intNotificationID) {
			return self::update();
		} else {
			return self::insert();
		}
	}

	public function delete() {
		if($this->_intNotificationID) {
			base::write_log("Notification deleted");
			$strSQL = "DELETE FROM dbSystem.tblNotification
				WHERE intNotificationID = '$this->_intNotificationID'
				";
			return $this->getDB()->query($strSQL);
		}
	}

	public function load($intNotificationID) {
		if(!$intNotificationID) {
			return false;
		}

		$strSQL = "SELECT *
				FROM dbSystem.tblNotification
				WHERE intNotificationID = '$intNotificationID'
				LIMIT 1
			";
		$rsNotification = $this->getDB()->query($strSQL);
		$arrNotification = $this->getDB()->fetch_assoc($rsNotification);
		$this->setVarsFromRow($arrNotification);
	}

	function setVarsFromRow($arrNotification) {
		if(isset($arrNotification["intNotificationID"])) $this->_intNotificationID = $arrNotification["intNotificationID"];
		if(isset($arrNotification["strSubject"])) $this->_strSubject = $arrNotification["strSubject"];
		if(isset($arrNotification["txtBody"])) $this->_txtBody = $arrNotification["txtBody"];
		if(isset($arrNotification["intNotificationTypeID"])) $this->_intNotificationTypeID = $arrNotification["intNotificationTypeID"];
		if(isset($arrNotification["intCreatedByUserID"])) $this->_intCreatedByUserID = $arrNotification["intCreatedByUserID"];
	}

	function getNotificationID() {
		return $this->_intNotificationID;
	}
	function setNotificationID($value) {
		if($this->_intNotificationID !== $value) {
			$this->_intNotificationID = $value;
			$this->_blnDirty = true;
		}
	}

	function getSubject() {
		return $this->_strSubject;
	}
	function setSubject($value) {
		if($this->_strSubject !== $value) {
			$this->_strSubject = $value;
			$this->_blnDirty = true;
		}
	}

	function getBody() {
		return $this->_txtBody;
	}
	function setBody($value) {
		if($this->_txtBody !== $value) {
			$this->_txtBody = $value;
			$this->_blnDirty = true;
		}
	}

	function getNotificationTypeID() {
		return $this->_intNotificationTypeID;
	}
	function setNotificationTypeID($value) {
		if($this->_intNotificationTypeID !== $value) {
			$this->_intNotificationTypeID = $value;
			$this->_blnDirty = true;
		}
	}

	function getCreatedByUserID() {
		return $this->_intCreatedByUserID;
	}
	function setCreatedByUserID($value) {
		if($this->_intCreatedByUserID !== $value) {
			$this->_intCreatedByUserID = $value;
			$this->_blnDirty = true;
		}
	}
}

include_once("UserNotification.class.php");
include_once("NotificationType.class.php");
include_once("Product.class.php");
include_once("User.class.php");

class Notification extends NotificationBase {
	private $_objUserNotificationArray;
	private $_objNotificationType;
	private $_objProgram;
	private $_objUserArray;

	function __construct($intNotificationID=null) {
		parent::__construct($intNotificationID);
	}
    function sendNotificationChanges($objUserArray) {
		// Only process if a list of user objects was passed into this function
    	if (isset($objUserArray)) {
    		// load up all the user id's that have been notified already
    		$this->setUserNotificationArrayFromUserArray($this->getUserArray());
    		foreach($this->getUserNotificationArray()->getArray() as $objUserNotification) {
    			$notifiedUsers[] = $objUserNotification->getUserID();
    		}
    		// load up all the user id's to be notified at this time
    		if($objUserArray->getArray()) {
    			foreach($objUserArray->getArray() as $objUser) {
    				$newUsers[] = $objUser->getUserID();
    			}
    		}
    		foreach ($userUsers as $userID) {
    			// select only users to notify that have not already been notified
    			if (!in_array($userID,$notifiedUsers)) {
    				$usersToNotify[] = $userID;
    			}
    		}
    		// Merge all new users to be notified into the existing notification array
    		$this->setUserNotificationArrayFromUserArray(array_merge($this->getUserArray(),$objUserArray));
			// for each existing userNotification, process it If the user is in the array of
			//  new users to be notified.  These users have not been notified previously.
    		foreach($this->getUserNotificationArray()->getArray() as $objUserNotification) {
    			if (!in_array($objUserNotification->getUserID(),$usersToNotify)) {
    				$objUserNotification->setNotificationID($this->getNotificationID());
    				$objUserNotification->setSent(now());
    				$objUserNotification->setSentByUserID($_SESSION["objUser"]->getUserID());
    				$objUserNotification->save();    			
    				$this->sendEmailNotification($objUserNotification->getUser()->getEmail());
    			}
    		}    		
    	}
    }
	function sendNotification() {
		if(!$this->getUserArray()->getArray())
			return false;

		$this->setCreatedByUserID($_SESSION["objUser"]->getUserID());
		$this->save();

		$this->setUserNotificationArrayFromUserArray($this->getUserArray());
		foreach($this->getUserNotificationArray()->getArray() as $objUserNotification) {
			$objUserNotification->setNotificationID($this->getNotificationID());
			$objUserNotification->setSent(now());
			$objUserNotification->setSentByUserID($_SESSION["objUser"]->getUserID());
			$objUserNotification->save();

			$this->sendEmailNotification($objUserNotification->getUser()->getEmail());
		}
	}

	function sendEmailNotification($strEmail){
		//echo "SENDING TO $strEmail <br />";
		sendEmailNotification($strEmail, $this->getSubject(), $this->getBody());
	}

	function getUserNotificationArray(){
		if(!$this->_objUserNotificationArray)
			$this->_objUserNotificationArray = new UserNotificationArray();
		return $this->_objUserNotificationArray;
	}
	function setUserNotificationArrayFromUserArray($objUserArray) {
		$this->getUserNotificationArray()->clear();
		if($objUserArray->getArray()) {
			foreach($objUserArray->getArray() as $objUser) {
				$objUserNotification = new UserNotification();
				$objUserNotification->setUserID($objUser->getUserID());
				$objUserNotification->setUser($objUser);
				$this->getUserNotificationArray()->setObject($objUser->getUserID(), $objUserNotification);
			}
		}
	}
	function getNotificationType(){
		if(!$this->_objNotificationType)
			$this->_objNotificationType = new NotificationType();
		return $this->_objNotificationType;
	}
	function setNotificationType($value){
		$this->_objNotificationType = $value;
	}
	function getProgram(){
		if(!$this->_objProgram)
			$this->_objProgram = new Product();
		return $this->_objProgram;
	}
	function setProgram($value){
		$this->_objProgram = $value;
	}
	function getUserArray(){
		if(!$this->_objUserArray)
			$this->_objUserArray = new UserArray();
		return $this->_objUserArray;
	}
	function setUserArray($value){
		$this->_objUserArray = $value;
	}
}
?>
