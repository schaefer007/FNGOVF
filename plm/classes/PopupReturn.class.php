<?php
include_once("common.php");

class PopupReturn {
	public $strStatus;
	public $arrChanges;
	public $arrData;
	public $strRedirect;

	function __construct(){
	}

	function toJSON() {
		return toJSON($this);
	}

	function getStatus() {
		return $this->strStatus;
	}
	function setStatus($value) {
		$this->strStatus = $value;
	}

	function getChanges() {
		return $this->arrChanges;
	}
	function setChanges($value) {
		$this->arrChanges = $value;
	}

	function getData() {
		return $this->arrData;
	}
	function setData($value) {
		$this->arrData = $value;
	}

	function getRedirect() {
		return $this->strRedirect;
	}
	function setRedirect($value) {
		$this->strRedirect = $value;
	}

	function getError() {
		return $this->strError;
	}
	function setError($value) {
		$this->strError = $value;
	}
}

?>