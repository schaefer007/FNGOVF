<?php
require_once("ArrayClass.class.php");

class AlertNotificationArray extends ArrayClass {
	function __construct(){
	}

	function load() {
		$strSQL = " SELECT * FROM dbSystem.tblAlertNotification";
		$rsResult = $this->getDB()->query($strSQL);
		while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
			$this->_arrObjects[$arrRow["alertNotificationID"]] = new AlertNotification();
			$this->_arrObjects[$arrRow["alertNotificationID"]]->setVarsFromRow($arrRow);
		}
	}

	function loadByNotificationID($intAlertNotificationID) {
		if (!$intAlertNotificationID) {
			return false;
		}
		$strSQL = " SELECT * FROM dbSystem.tblAlertNotification WHERE tblAlertNotification.alertNotificationID = ".self::getDB()->sanitize($intAlertNotificationID);
		$rsResult = $this->getDB()->query($strSQL);
		while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
			$this->_arrObjects[$arrRow["alertNotificationID"]] = new AlertNotification();
			$this->_arrObjects[$arrRow["alertNotificationID"]]->setVarsFromRow($arrRow);
		}
	}
	
//	function loadForNotificationManagement($intUserID) {
//		if(!$intUserID)
//			return false;
//
//		$objSearch = $_SESSION["objSearch"];
//
//		$strSQL = " SELECT SQL_CALC_FOUND_ROWS tblUser.*, tblNotification.*, tblUserNotification.* FROM dbSystem.tblUserNotification
//			INNER JOIN dbSystem.tblNotification
//				ON tblUserNotification.intNotificationID = tblNotification.intNotificationID
//			LEFT JOIN dbPLM.tblUser
//				ON tblUser.intUserID = tblUserNotification.intSentByUserID
//			WHERE dtmDeleted IS NULL
//			AND tblUserNotification.intUserID = ".self::getDB()->sanitize($intUserID);
//		$strSQL .= $objSearch->getNotificationListPage()->getSortQuery();
//		//echo $strSQL . "<br />";
//		$rsResult = $this->getDB()->query($strSQL);
//		while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
//			$this->_arrObjects[$arrRow["intUserNotificationID"]] = new UserNotification();
//			$this->_arrObjects[$arrRow["intUserNotificationID"]]->setVarsFromRow($arrRow);
//			$this->_arrObjects[$arrRow["intUserNotificationID"]]->getSentByUser()->setVarsFromRow($arrRow);
//		}
//
//		$this->setFoundRows(self::getDB()->found_rows());
//	}

	function sendNotification() {
		if(!$this->_arrObjects)
			return false;
		$this->save();
		foreach($this->_arrObjects as $objAlertNotification) {
			$objAlertNotification->setDTMSent(now());
			$objAlertNotification->sendAlertNotification($objAlertNotification->getUser()->getEmail());
			$objAlertNotification->save();
		}
	}
}

require_once("DataClass.class.php");
require_once("Alert.class.php");

class AlertNotificationBase extends Alert {
	protected $_intAlertNotificationID;
	protected $_intUserID;

	function __construct($intFormID=null) {
		$this->DataClass();
		if($intFormID) {
			$this->load($intFormID);
		}
	}

	protected function insert() {
//		$this->setDTMSent(now());
		base::write_log("Alert notification created","S");
		$strSQL = "INSERT INTO dbSystem.tblAlertNotification SET ";
		$strConnector = "";
		$strSQL .= $strConnector . "alertNotificationID = ".$this->getDB()->sanitize(self::getAlertNotificationID());
		$strConnector = ",";
		if(isset($this->_alertID)) {
			$strSQL .= $strConnector . "alertID = ".$this->getDB()->sanitize(self::getAlertID());
			$strConnector = ",";
		}
		if(isset($this->_intUserID)) {
			$strSQL .= $strConnector . "userID = ".$this->getDB()->sanitize(self::getUserID());
			$strConnector = ",";
		}
//		if(isset($this->_dtmSent)) {
//			$strSQL .= $strConnector . "dtmSent = ".$this->getDB()->sanitize(self::getSent());
//			$strConnector = ",";
//		}
//		echo $strSQL;
		$this->getDB()->query($strSQL);
		$this->setAlertNotificationID($this->getDB()->insert_id());
		return $this->getAlertNotificationID();
	}

	protected function update() {
//		$this->setDTMSent(now());
		base::write_log("Alert notification updated","S");
		$strSQL = "UPDATE dbSystem.tblAlertNotification SET ";
		$strConnector = "";
		$strSQL .= $strConnector . "alertNotificationID = ".$this->getDB()->sanitize(self::getAlertNotificationID());
		$strConnector = ",";
		if(isset($this->_alertID)) {
			$strSQL .= $strConnector . "alertID = ".$this->getDB()->sanitize(self::getAlertID());
			$strConnector = ",";
		}
		if(isset($this->_intUserID)) {
			$strSQL .= $strConnector . "userID = ".$this->getDB()->sanitize(self::getUserID());
			$strConnector = ",";
		}
		if(isset($this->_dtmSent)) {
			$strSQL .= $strConnector . "dtmSent = ".$this->getDB()->sanitize(self::getDTMSent());
			$strConnector = ",";
		}
		$strSQL .= " WHERE alertNotificationID = ".$this->getDB()->sanitize(self::getAlertNotificationID())."";
//		echo $strSQL;
		base::write_log("Alert notification sent","S");
		return $this->getDB()->query($strSQL);
	}

	public function save() {
		if($this->_intAlertNotificationID) {
			return $this->update();
		} else {
			return $this->insert();
		}
	}

	public function delete() {
		base::write_log("Alert notification deleted");
		if($this->_intAlertNotificationID) {
			$strSQL = "DELETE FROM dbSystem.tblAlertNotification
				WHERE alertNotificationID = '$this->_intAlertNotificationID'
				";
			return $this->getDB()->query($strSQL);
		}
	}

//	public function load($intFormID) {
//		if(!$intFormID) {
//			return false;
//		}
//
//		$strSQL = "SELECT tblAlert.*, tblAlertNotification.*
//				FROM dbSystem.tblAlert
//				INNER JOIN dbSystem.tblAlertNotification
//					ON tblAlertNotification.alertID = tblAlert.alertID
//				WHERE formID = '$intFormID'
//				ORDER BY alertID, alertNumber DESC
//				LIMIT 1
//			";
//		echo $strSQL;
//		$rsUserNotification = $this->getDB()->query($strSQL);
//		$arrUserNotification = $this->getDB()->fetch_assoc($rsUserNotification);
//		$this->setVarsFromRow($arrUserNotification);
//	}

	function setVarsFromRow($arrUserNotification) {
		parent::setVarsFromRow($arrUserNotification);
		if(isset($arrUserNotification["alertNotificationID"])) $this->_intAlertNotificationID = $arrUserNotification["alertNotificationID"];
		if(isset($arrUserNotification["alertID"])) $this->_alertID = $arrUserNotification["alertID"];
		if(isset($arrUserNotification["userID"])) $this->_userID = $arrUserNotification["userID"];
		if(isset($arrUserNotification["dtmSent"])) $this->_dtmSent = $arrUserNotification["dtmSent"];
		if(isset($arrUserNotification["formID"])) $this->_formID = $arrUserNotification["formID"];
		if(isset($arrUserNotification["alertNumber"])) $this->_alertNumber = $arrUserNotification["alertNumber"];
	}

	function getAlertNotificationID() {
		return $this->_intAlertNotificationID;
	}
	function setAlertNotificationID($value) {
		if($this->_intAlertNotificationID !== $value) {
			$this->_intAlertNotificationID = $value;
			$this->_blnDirty = true;
		}
	}

	function getUserID() {
		return $this->_intUserID;
	}
	function setUserID($value) {
		if($this->_intUserID !== $value) {
			$this->_intUserID = $value;
			$this->_blnDirty = true;
		}
	}

}

include_once("User.class.php");

class AlertNotification extends AlertNotificationBase {
	private $_objUser;
	private $_objSentByUser;

	function __construct($intFormID=null) {
		parent::__construct($intFormID);
	}

	function delete(){
		if(!$this->_intAlertNotificationID)
			return;

		$this->setDeleted(NOW());
		$this->save();
	}

	function getUser(){
		if(!$this->_objUser)
			$this->_objUser = new User();
		return $this->_objUser;
	}
	function setUser($value){
		$this->_objUser = $value;
	}

	function sendIndividualNotification($strEmail) {

		$this->_intAlertNotificationID = null;
		$this->setUserID($strEmail);
		$this->save();

		$this->setDTMSent(now());
		$this->sendAlertNotification($strEmail);
		$this->save();
//		foreach($this->_arrObjects as $objAlertNotification) {
//			$objAlertNotification->setDTMSent(now());
//			$objAlertNotification->sendAlertNotification($objAlertNotification->getUser()->getEmail());
//			$objAlertNotification->save();
//		}
	}
}
?>