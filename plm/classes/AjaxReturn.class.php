<?php
include_once("common.php");

class AjaxReturn {
	public $strStatus;
	public $arrChanges;
	public $arrData;
	public $strError;
	public $arrColumns;

	function __construct(){
	}

	function toJSON() {
		return toJSON($this);
	}

	function getStatus() {
		return $this->strStatus;
	}
	function setStatus($value) {
		$this->strStatus = $value;
	}
    function getColumns() {
    	return $this->arrColumns;
    }
    function setColumns($value) {
    	$this->arrColumns = $value;
    }
	function getChanges() {
		return $this->arrChanges;
	}
	function setChanges($value) {
		$this->arrChanges = $value;
	}

	function getData() {
		return $this->arrData;
	}
	function setData($value) {
		$this->arrData = $value;
	}

	function getError() {
		return $this->strError;
	}
	function setError($value) {
		$this->strError = $value;
	}
}
?>