<?php
require_once("ArrayClass.class.php");

class WFProcessArray extends ArrayClass {
	function __construct(){
		parent::__construct("WFProcess");
	}

	function load() {
		$strSQL = " SELECT * FROM dbSystem.tblWFProcess";
		$rsResult = $this->getDB()->query($strSQL);
		while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
			$this->_arrObjects[$arrRow["intWFProcessID"]] = new WFProcess();
			$this->_arrObjects[$arrRow["intWFProcessID"]]->setVarsFromRow($arrRow);
		}
	}
}

require_once("DataClass.class.php");

class WFProcessBase extends DataClass {
	protected $_intWFProcessID;
	protected $_intStartingProcessStateID;
	protected $_strWFProcessName;

	function __construct($intWFProcessID=null) {
		$this->DataClass();
		if($intWFProcessID) {
			$this->load($intWFProcessID);
		}
	}

	protected function insert() {
		base::write_log("Workflow Process created","S");
		$strSQL = "INSERT INTO dbSystem.tblWFProcess SET ";
		$strConnector = "";
		$strSQL .= $strConnector . "intWFProcessID = ".$this->getDB()->sanitize(self::getWFProcessID());
		$strConnector = ",";
		if(isset($this->_intStartingProcessStateID)) {
			$strSQL .= $strConnector . "intStartingProcessStateID = ".$this->getDB()->sanitize(self::getStartingProcessStateID());
			$strConnector = ",";
		}
		if(isset($this->_strWFProcessName)) {
			$strSQL .= $strConnector . "strWFProcessName = ".$this->getDB()->sanitize(self::getWFProcessName());
			$strConnector = ",";
		}
		//echo $strSQL . "<br />";
		$this->getDB()->query($strSQL);
		$this->setWFProcessID($this->getDB()->insert_id());
		return $this->getWFProcessID();
	}

	protected function update() {
		base::write_log("Workflow Process Update","S");
		$strSQL = "UPDATE dbSystem.tblWFProcess SET ";
		$strConnector = "";
		$strSQL .= $strConnector . "intWFProcessID = ".$this->getDB()->sanitize(self::getWFProcessID());
		$strConnector = ",";
		if(isset($this->_intStartingProcessStateID)) {
			$strSQL .= $strConnector . "intStartingProcessStateID = ".$this->getDB()->sanitize(self::getStartingProcessStateID());
			$strConnector = ",";
		}
		if(isset($this->_strWFProcessName)) {
			$strSQL .= $strConnector . "strWFProcessName = ".$this->getDB()->sanitize(self::getWFProcessName());
			$strConnector = ",";
		}
		$strSQL .= " WHERE intWFProcessID = ".$this->getDB()->sanitize(self::getWFProcessID())."";
		//echo $strSQL;
		return $this->getDB()->query($strSQL);
	}

	public function save() {
		if($this->_intWFProcessID) {
			return self::update();
		} else {
			return self::insert();
		}
	}

	public function delete() {
		if($this->_intWFProcessID) {
			base::write_log("Workflow Process delete","S");
			$strSQL = "DELETE FROM dbSystem.tblWFProcess
				WHERE intWFProcessID = '$this->_intWFProcessID'
				";
			return $this->getDB()->query($strSQL);
		}
	}

	public function load($intWFProcessID) {
		if(!$intWFProcessID) {
			return false;
		}

		$strSQL = "SELECT *
			FROM dbSystem.tblWFProcess
			WHERE intWFProcessID = ".self::getDB()->sanitize($intWFProcessID)."
			LIMIT 1
		";
		$rsResult = $this->getDB()->query($strSQL);
		$arrRow = $this->getDB()->fetch_assoc($rsResult);
		$this->setVarsFromRow($arrRow);
	}

	function setVarsFromRow($arrRow) {
		if(isset($arrRow["intWFProcessID"])) $this->_intWFProcessID = $arrRow["intWFProcessID"];
		if(isset($arrRow["intStartingProcessStateID"])) $this->_intStartingProcessStateID = $arrRow["intStartingProcessStateID"];
		if(isset($arrRow["strWFProcessName"])) $this->_strWFProcessName = $arrRow["strWFProcessName"];
	}

	function getWFProcessID() {
		return $this->_intWFProcessID;
	}
	function setWFProcessID($value) {
		if($this->_intWFProcessID !== $value) {
			$this->_intWFProcessID = $value;
			$this->_blnDirty = true;
		}
	}

	function getStartingProcessStateID() {
		return $this->_intStartingProcessStateID;
	}
	function setStartingProcessStateID($value) {
		if($this->_intStartingProcessStateID !== $value) {
			$this->_intStartingProcessStateID = $value;
			$this->_blnDirty = true;
		}
	}

	function getWFProcessName() {
		return $this->_strWFProcessName;
	}
	function setWFProcessName($value) {
		if($this->_strWFProcessName !== $value) {
			$this->_strWFProcessName = $value;
			$this->_blnDirty = true;
		}
	}
}

class WFProcess extends WFProcessBase {
	protected $_objStartingProcessState;

	protected $_objProcessStateArray;
	protected $_objProcessTransitionArray;
	protected $_objProcessPathArray;

	function __construct($intWFProcessID=null) {
		parent::__construct($intWFProcessID);
	}

	function loadForWFProcessEdit($intWFProcessID) {
		if(!$intWFProcessID) {
			return false;
		}

		$strSQL = "SELECT tblProcessState.*, tblWFProcess.*
			FROM dbSystem.tblWFProcess
			LEFT JOIN dbSystem.tblProcessState
				ON tblProcessState.intProcessStateID = tblWFProcess.intStartingProcessStateID
			WHERE tblWFProcess.intWFProcessID = ".self::getDB()->sanitize($intWFProcessID)."
			LIMIT 1
		";
		//echo $strSQL;
		$rsResult = $this->getDB()->query($strSQL);
		$arrRow = $this->getDB()->fetch_assoc($rsResult);
		$this->setVarsFromRow($arrRow);
		$this->getStartingProcessState()->setVarsFromRow($arrRow);
	}

	function loadByWFProcessName($strWFProcessName) {
		if(!$strWFProcessName) {
			return false;
		}

		$strSQL = "SELECT *
			FROM dbSystem.tblWFProcess
			WHERE strWFProcessName = ".self::getDB()->sanitize($strWFProcessName)."
			LIMIT 1
		";
		$rsResult = $this->getDB()->query($strSQL);
		$arrRow = $this->getDB()->fetch_assoc($rsResult);
		$this->setVarsFromRow($arrRow);
	}

	function loadForWorkflowManagement($intWFProcessID) {
		if(!$intWFProcessID) {
			return false;
		}

		$strSQL = "SELECT tblProcessState.*, tblWFProcess.*
			FROM dbSystem.tblWFProcess
			LEFT JOIN dbSystem.tblProcessState
				ON tblProcessState.intWFProcessID = tblWFProcess.intWFProcessID
			WHERE tblWFProcess.intWFProcessID = ".self::getDB()->sanitize($intWFProcessID)."
			ORDER BY tblProcessState.intDisplayOrder
		";
		//echo $strSQL . "<br />";
		$rsResult = $this->getDB()->query($strSQL);
		while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
			if(!isset($this->_arrObjects[$arrRow["intWFProcessID"]])) {
				$this->setVarsFromRow($arrRow);
			}
			if($arrRow["intProcessStateID"]) {
				$this->getProcessStateArray()->addFromRow($arrRow, "intProcessStateID");
			}
		}

		$this->loadProcessTransitions();
		$this->getProcessTransitionArray()->loadConditions();
		$this->getProcessPathArray()->loadByWFProcessID($this->getWFProcessID());
	}

	function loadProcessTransitions() {
		if(!$this->getProcessStateArray()->getArray())
			return;

		$arrProcessStateIDs = $this->getProcessStateArray()->getAllFields("getProcessStateID");
		$this->getProcessTransitionArray()->loadByProcessStateIDs($arrProcessStateIDs);
	}

	function validate() {
		$arrErrors = array();

		if(!$this->getWFProcessName()) {
			$arrErrors[] = "Process Name must be specified.";
		}

		return $arrErrors;
	}

	function getStartingProcessState() {
		include_once("ProcessState.class.php");
		if(!$this->_objStartingProcessState) {
			$this->_objStartingProcessState = new ProcessState();
		}
		return $this->_objStartingProcessState;
	}
	function getProcessStateArray() {
		include_once("ProcessState.class.php");
		if(!$this->_objProcessStateArray) {
			$this->_objProcessStateArray = new ProcessStateArray();
		}
		return $this->_objProcessStateArray;
	}
	function getProcessTransitionArray() {
		include_once("ProcessTransition.class.php");
		if(!$this->_objProcessTransitionArray) {
			$this->_objProcessTransitionArray = new ProcessTransitionArray();
		}
		return $this->_objProcessTransitionArray;
	}
	function getProcessPathArray() {
		include_once("ProcessPath.class.php");
		if(!$this->_objProcessPathArray) {
			$this->_objProcessPathArray = new ProcessPathArray();
		}
		return $this->_objProcessPathArray;
	}
}
?>