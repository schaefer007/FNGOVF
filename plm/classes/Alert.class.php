<?php
require_once("ArrayClass.class.php");

class AlertArray extends ArrayClass {
    function __construct(){
    }

    function load() {
        $strSQL = " SELECT * FROM dbSystem.tblAlert";
        $rsResult = $this->getDB()->query($strSQL);
        while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
            $this->_arrObjects[$arrRow["alertID"]] = new Alert();
            $this->_arrObjects[$arrRow["alertID"]]->setVarsFromRow($arrRow);
        }
    }
}

require_once("DataClass.class.php");

class AlertBase extends DataClass {
    protected $_alertID;
    protected $_formID;
    protected $_alertNumber;
    protected $_userID;
    protected $_dtmSent;
    protected $_strSubject;
    protected $_txtBody;
    protected $_stateID;
    protected $_stateInstanceID;

    function __construct($formID=null) {
        $this->DataClass();
        if($formID) {
            $this->load($formID);
        }
    }

    protected function insert() {
        if(!isset($this->_alertNumber)) {
            $tempAlertBase = new AlertBase($this->_formID);
            $this->_alertNumber = $tempAlertBase->getAlertNumber() + 1;
        }
        base::write_log("Alert created","S");
        $strSQL = "INSERT INTO dbSystem.tblAlert SET ";
        $strConnector = "";
        $strSQL .= $strConnector . "formID = ".$this->getDB()->sanitize(self::getFormID());
        $strConnector = ",";
        if(isset($this->_alertNumber)) {
            $strSQL .= $strConnector . "alertNumber = ".$this->getDB()->sanitize(self::getAlertNumber());
            $strConnector = ",";
        }
        if(isset($this->_strSubject)) {
            $strSQL .= $strConnector . "subject = ".$this->getDB()->sanitize(self::getSubject());
            $strConnector = ",";
        }
        if(isset($this->_txtBody)) {
            $strSQL .= $strConnector . "body = ".$this->getDB()->sanitize(self::getBody());
            $strConnector = ",";
        }
//        if(isset($this->_userID)) {
//            $strSQL .= $strConnector . "userID = ".$this->getDB()->sanitize(self::getUserID());
//            $strConnector = ",";
//        }
        if(isset($this->_dtmSent)) {
            $strSQL .= $strConnector . "dtmSent = ".$this->getDB()->sanitize(self::getDTMSent());
            $strConnector = ",";
        }
        if(isset($this->_stateID)) {
            $strSQL .= $strConnector . "stateID = ".$this->getDB()->sanitize(self::getStateID());
            $strConnector = ",";
        }
        if(isset($this->_stateInstanceID)) {
            $strSQL .= $strConnector . "intStateInstanceID = ".$this->getDB()->sanitize(self::getStateInstanceID());
            $strConnector = ",";
        }
//        echo $strSQL . "<br /><br />";
        $this->getDB()->query($strSQL);
        $this->setAlertID($this->getDB()->insert_id());
        base::write_log($strSQL,"tblAlert");
        return $this->getAlertID();
    }

    protected function update() {
        base::write_log("Alert Update","S");
        $strSQL = "UPDATE dbSystem.tblAlert SET ";
        $strConnector = "";
        $strSQL .= $strConnector . "formID = ".$this->getDB()->sanitize(self::getFormID());
        $strConnector = ",";
        if(isset($this->_alertNumber)) {
            $strSQL .= $strConnector . "alertNumber = ".$this->getDB()->sanitize(self::getAlertNumber());
            $strConnector = ",";
        }
//        if(isset($this->_userID)) {
//            $strSQL .= $strConnector . "userID = ".$this->getDB()->sanitize(self::getUserID());
//            $strConnector = ",";
//        }
        if(isset($this->_strSubject)) {
            $strSQL .= $strConnector . "subject = ".$this->getDB()->sanitize(self::getSubject());
            $strConnector = ",";
        }
        if(isset($this->_txtBody)) {
            $strSQL .= $strConnector . "body = ".$this->getDB()->sanitize(self::getBody());
            $strConnector = ",";
        }
        if(isset($this->_dtmSent)) {
            $strSQL .= $strConnector . "dtmSent = ".$this->getDB()->sanitize(self::getDTMSent());
            $strConnector = ",";
        }
        if(isset($this->_stateID)) {
            $strSQL .= $strConnector . "stateID = ".$this->getDB()->sanitize(self::getStateID());
            $strConnector = ",";
        }
        if(isset($this->_stateInstanceID)) {
            $strSQL .= $strConnector . "intStateInstanceID = ".$this->getDB()->sanitize(self::getStateInstanceID());
            $strConnector = ",";
        }
        $strSQL .= " WHERE alertID = ".$this->getDB()->sanitize(self::getAlertID())."";
//        echo $strSQL;
        return $this->getDB()->query($strSQL);
    }

    public function save() {
        if($this->_alertNumber) {
            return self::update();
        } else {
            return self::insert();
        }
    }

    public function delete() {
        if($this->_alertID) {
            $strSQL = "DELETE FROM dbSystem.tblAlert
				WHERE formID = '$this->_formID'
				";
            base::write_log("Alert deleted","S");
            return $this->getDB()->query($strSQL);
        }
    }

    public function load($formID) {
        if(!$formID) {
            return false;
        }
//        $strSQL = "SELECT *
//				FROM dbSystem.tblAlert
//				WHERE formID = '$formID'
//				ORDER BY alertNumber DESC
//				LIMIT 1
//			";
        $strSQL = "SELECT *
            FROM dbSystem.tblAlert
            WHERE formID = '$formID' AND alertStatus <> 'C'
            ORDER BY alertNumber DESC
            LIMIT 1;
          ";
        $rsNotification = $this->getDB()->query($strSQL);
        $arrNotification = $this->getDB()->fetch_assoc($rsNotification);
        $this->setVarsFromRow($arrNotification);
        $this->_formID = $formID;
    }

    function setVarsFromRow($arrNotification)
    {
        if (isset($arrNotification["alertID"])) $this->_alertID = $arrNotification["alertID"];
        if (isset($arrNotification["formID"])) $this->_formID = $arrNotification["formID"];
        if (isset($arrNotification["alertNumber"])) $this->_alertNumber = $arrNotification["alertNumber"];
        if (isset($arrNotification["userID"])) $this->_userID = $arrNotification["userID"];
        if (isset($arrNotification["dtmSent"])) $this->_dtmSent = $arrNotification["dtmSent"];
        if (isset($arrNotification["subject"])) $this->_strSubject = $arrNotification["subject"];
        if (isset($arrNotification["body"])) $this->_txtBody = $arrNotification["body"];
        if (isset($arrNotification["stateID"])) $this->_stateID = $arrNotification["stateID"];
        if (isset($arrNotification["intStateInstanceID"])) $this->_stateInstanceID = $arrNotification["intStateInstanceID"];
    }

    function getAlertID() {
        return $this->_alertID;
    }
    function setAlertID($value) {
        if($this->_alertID !== $value) {
            $this->_alertID = $value;
            $this->_blnDirty = true;
        }
    }

    function getFormID() {
        return $this->_formID;
    }
    function setFormID($value) {
        if($this->_formID !== $value) {
            $this->_formID = $value;
            $this->_blnDirty = true;
        }
    }

    function getAlertNumber() {
        if(isset($this->_alertNumber)) {
            return $this->_alertNumber;
        } else {
            return 0;
        }
    }
    function setAlertNumber($value) {
        if($this->_alertNumber !== $value) {
            $this->_alertNumber = $value;
            $this->_blnDirty = true;
        }
    }

    function getUserID() {
        return $this->_userID;
    }
    function setUserID($value) {
        if($this->_userID !== $value) {
            $this->_userID = $value;
            $this->_blnDirty = true;
        }
    }

    function getDTMSent() {
        return $this->_dtmSent;
    }
    function setDTMSent($value) {
        if($this->_dtmSent !== $value) {
            $this->_dtmSent = $value;
            $this->_blnDirty = true;
        }
    }

    function getSubject() {
        return $this->_strSubject;
    }
    function setSubject($value) {
        if($this->_strSubject !== $value) {
            $this->_strSubject = $value;
            $this->_blnDirty = true;
        }
    }

    function getBody() {
        return $this->_txtBody;
    }
    function setBody($value) {
        if($this->_txtBody !== $value) {
            $this->_txtBody = $value;
            $this->_blnDirty = true;
        }
    }

    function getStateID() {
        return $this->_stateID;
    }
    function setStateID($value) {
        if($this->_stateID !== $value) {
            $this->_stateID = $value;
            $this->_blnDirty = true;
        }
    }

    function getStateInstanceID() {
        return $this->_stateInstanceID;
    }
    function setStateInstanceID($value) {
        if($this->_stateInstanceID !== $value) {
            $this->_stateInstanceID = $value;
            $this->_blnDirty = true;
        }
    }
}

include_once("UserAlert.class.php");
include_once("NotificationType.class.php");
include_once("Product.class.php");
include_once("User.class.php");

class Alert extends AlertBase {
    private $_objUserNotificationArray;
    private $_objNotificationType;
    private $_objProgram;
    private $_objUserArray;

    function __construct($formID=null) {
        parent::__construct($formID);
    }
    function setCompleteStatus() {
        base::write_log("Alert status set to C","S");
        $strSQL = "UPDATE dbSystem.tblAlert SET alertStatus = 'C' WHERE formID = " .$this->getFormID();
        return $this->getDB()->query($strSQL);
    }
    function sendNotificationChanges($objUserArray) {
        // Only process if a list of user objects was passed into this function
        if (isset($objUserArray)) {
            // load up all the user id's that have been notified already
            $this->setUserAlertArray($this->getUserArray());
            foreach($this->getUserAlertArray()->getArray() as $objUserNotification) {
                $notifiedUsers[] = $objUserNotification->getUserID();
            }
            // load up all the user id's to be notified at this time
            if($objUserArray->getArray()) {
                foreach($objUserArray->getArray() as $objUser) {
                    $newUsers[] = $objUser->getUserID();
                }
            }
            foreach ($userUsers as $userID) {
                // select only users to notify that have not already been notified
                if (!in_array($userID,$notifiedUsers)) {
                    $usersToNotify[] = $userID;
                }
            }
            // Merge all new users to be notified into the existing notification array
            $this->setUserNotificationArrayFromUserArray(array_merge($this->getUserArray(),$objUserArray));
            // for each existing userNotification, process it If the user is in the array of
            //  new users to be notified.  These users have not been notified previously.
            foreach($this->getUserAlertArray()->getArray() as $objUserNotification) {
                if (!in_array($objUserNotification->getUserID(),$usersToNotify)) {
                    $objUserNotification->setNotificationID($this->getNotificationID());
                    $objUserNotification->setDTMSent(now());
                    $objUserNotification->setSentByUserID($_SESSION["objUser"]->getUserID());
                    $objUserNotification->save();
                    $this->sendEmailNotification($objUserNotification->getUser()->getEmail());
                }
            }
        }
    }

    function sendAlertNotification($strEmail){
//        echo "SENDING TO $strEmail <br />";
        sendAlertNotification($strEmail, $this->getSubject(), $this->getBody());
    }

    function getAlertNotificationArray(){
        if(!$this->_objUserNotificationArray)
            $this->_objUserNotificationArray = new AlertNotificationArray();
        return $this->_objUserNotificationArray;
    }
    function setUserNotificationArrayFromUserArray($objUserArray) {
        $this->getUserNotificationArray()->clear();
        if($objUserArray->getArray()) {
            foreach($objUserArray->getArray() as $objUser) {
                $objUserNotification = new UserNotification();
                $objUserNotification->setUserID($objUser->getUserID());
                $objUserNotification->setUser($objUser);
                $this->getUserNotificationArray()->setObject($objUser->getUserID(), $objUserNotification);
            }
        }
    }

    function setAlertNotificationArrayFromUserArray($objUserArray) {
        $this->getAlertNotificationArray()->clear();
        if($objUserArray->getArray()) {
            foreach($objUserArray->getArray() as $objUser) {
                $objAlertNotification = new AlertNotification();
                $objAlertNotification->setUserID($objUser->getEmail());
                $objAlertNotification->setAlertID($this->_alertID);
                $objAlertNotification->setSubject($this->_strSubject);
                $objAlertNotification->setBody($this->_txtBody);
                $objAlertNotification->setUser($objUser);
                $this->getAlertNotificationArray()->setObject($objUser->getUserID(), $objAlertNotification);
            }
        }
    }
    function getNotificationType(){
        if(!$this->_objNotificationType)
            $this->_objNotificationType = new NotificationType();
        return $this->_objNotificationType;
    }
    function setNotificationType($value){
        $this->_objNotificationType = $value;
    }
    function getProgram(){
        if(!$this->_objProgram)
            $this->_objProgram = new Product();
        return $this->_objProgram;
    }
    function setProgram($value){
        $this->_objProgram = $value;
    }
    function getUserArray(){
        if(!$this->_objUserArray)
            $this->_objUserArray = new UserArray();
        return $this->_objUserArray;
    }
    function setUserArray($value){
        $this->_objUserArray = $value;
    }
}
?>
