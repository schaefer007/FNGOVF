<?php
	include_once("DataClass.class.php");
	include_once("Product.class.php");

	class Security extends DataClass {
		public function __construct() {
		}

		public static function isMemberRole($intProgramID, $intRoleID, $intUserID=null) {
			if($intUserID === null)
				$intUserID = (isset($_SESSION["objUser"]) && $_SESSION["objUser"]->getUserID())?$_SESSION["objUser"]->getUserID():null;

			if(!$intProgramID || !$intRoleID || !$intUserID)
				return false;

			$intProgramID = Product::getTopLevelProgramIDFromProgramID($intProgramID);

			$strSQL = "SELECT 1
				FROM dbPLM.tblMember
				INNER JOIN dbPLM.tblMemberRole
					ON tblMemberRole.intMemberID = tblMember.intMemberID
				WHERE intProgramID = ".self::getDB()->sanitize($intProgramID)."
				AND intUserID = ".self::getDB()->sanitize($intUserID)."
				AND intRoleID = ".self::getDB()->sanitize($intRoleID);
			$rsResult = self::getDB()->query($strSQL);
			return self::getDB()->num_rows($rsResult);
		}

		// TODO: Load all securities into the session, and read that instead of the database
		public static function hasPermission($intSecurityItemID, $intOperationID=null, $intProgramID=null, $blnLevel=null) {
			if(!isset($_SESSION["objUser"]) || !$_SESSION["objUser"]->getUserID() || !$intSecurityItemID)
				return false;

			$objUser = $_SESSION["objUser"];
			if($intProgramID) {
				$intProgramID = Product::getTopLevelProgramIDFromProgramID($intProgramID);

				$strSQL = "SELECT 1
					FROM dbPLM.tblPermission
					INNER JOIN dbPLM.tblRolePermission
						ON tblPermission.intPermissionID = tblRolePermission.intPermissionID
					LEFT JOIN (
						SELECT intUserID, intRoleID FROM dbPLM.tblMemberRole
						INNER JOIN dbPLM.tblMember
							ON tblMember.intMemberID = tblMemberRole.intMemberID
							AND tblMember.intProgramID = ".self::getDB()->sanitize($intProgramID)."
					) AS tblMember2
						ON tblMember2.intRoleID = tblRolePermission.intRoleID
					LEFT JOIN (
						SELECT intUserID, tblRole.intRoleID FROM tblUserRoleXR
						INNER JOIN dbPLM.tblRole
							ON tblRole.intRoleID = tblUserRoleXR.intRoleID
						WHERE tblRole.blnOverrideProgramSecurity
					) AS tblUserRoleXR2
						ON tblUserRoleXR2.intRoleID = tblRolePermission.intRoleID
					WHERE tblPermission.intSecurityItemID = ".self::getDB()->sanitize($intSecurityItemID)."
					AND (tblMember2.intUserID = ".self::getDB()->sanitize($objUser->getUserID()). " OR tblUserRoleXR2.intUserID = ".self::getDB()->sanitize($objUser->getUserID()).")";
				if($intOperationID) {
					if(is_array($intOperationID)) {
						$strSQL .= " AND intOperationID IN ('".implode("','", $intOperationID)."')";
					} else {
						$strSQL .= " AND intOperationID = ".self::getDB()->sanitize($intOperationID);
					}
				}
				$rsResult = self::getDB()->query($strSQL);
				return self::getDB()->num_rows($rsResult);
			} else {
				if(is_array($intOperationID)) {
					foreach($intOperationID as $intOpID) {
						if($objUser->getPermissionArray()->objectIsSet($intSecurityItemID."_".$intOpID)) {
							$objPermission = $objUser->getPermissionArray()->getObject($intSecurityItemID."_".$intOpID); // TODO: This doesn't work if $intOperationID is null
							if($blnLevel == intCORPORATE_LEVEL_ONLY) {
								return ($objPermission->getCorporateLevel())?$objPermission:null;
							} elseif($blnLevel == intPLANT_LEVEL_ONLY) {
								return ($objPermission->getPlantLevel())?$objPermission:null;
							} else {
								return $objPermission;
							}
						}
					}
				} else {
					$objPermission = $objUser->getPermissionArray()->getObject($intSecurityItemID."_".$intOperationID); // TODO: This doesn't work if $intOperationID is null
					if($objPermission && $blnLevel == intCORPORATE_LEVEL_ONLY) {
						return ($objPermission->getCorporateLevel())?$objPermission:null;
					} elseif($objPermission && $blnLevel == intPLANT_LEVEL_ONLY) {
						return ($objPermission->getPlantLevel())?$objPermission:null;
					} else {
						return $objPermission;
					}
				}
			}
			//echo $strSQL . "<br />";
		}

		public static function hasPermissions($arrPermissions, $strRedirectURL="index.php") {
			if(!isset($arrPermissions) || count($arrPermissions) == 0) {
				return false;
			}

			foreach($arrPermissions as $arrPermission) {
				$intSecurityItemID = isset($arrPermission[0])?$arrPermission[0]:null;
				$intOperationID = isset($arrPermission[1])?$arrPermission[1]:null;
				$intProgramID = isset($arrPermission[2])?$arrPermission[2]:null;
				if(!Security::hasPermission($intSecurityItemID, $intOperationID, $intProgramID)) {
					triggerError("You do not have access to this page.");
					if($strRedirectURL) {
						header("location:$strRedirectURL?strRequestURI=".$_SERVER["REQUEST_URI"]);
						exit();
					}
				}
			}
		}

		public static function validateLogin($strRedirectURL="index.php") {
			$objSessionUser = isset($_SESSION["objUser"])?$_SESSION["objUser"]:new User();
			if(!$objSessionUser->getUserID()) {
				triggerError("You must be logged in to access this page."); // TODO: Throw this error instead, this will fix how this is used in program_add_process.php
				if($strRedirectURL) {
					header("location:$strRedirectURL?strRequestURI=".$_SERVER["REQUEST_URI"]);
					exit();
				}
			}
			return $objSessionUser;
		}

		function setVarsFromRow($arrRow){
		}
	}
?>