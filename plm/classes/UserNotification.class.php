<?php
require_once("ArrayClass.class.php");

class UserNotificationArray extends ArrayClass {
	function __construct(){
	}

	function load() {
		$strSQL = " SELECT * FROM dbSystem.tblUserNotification";
		$rsResult = $this->getDB()->query($strSQL);
		while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
			$this->_arrObjects[$arrRow["intUserNotificationID"]] = new UserNotification();
			$this->_arrObjects[$arrRow["intUserNotificationID"]]->setVarsFromRow($arrRow);
		}
	}

	function loadByNotificationID($intNotificationID) {
		if (!$intNotificationID) {
			return false;
		}
		$strSQL = " SELECT * FROM dbSystem.tblUserNotification WHERE tblUserNotification.intNotificationID = ".self::getDB()->sanitize($intNotificationID);
		$rsResult = $this->getDB()->query($strSQL);
		while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
			$this->_arrObjects[$arrRow["intUserNotificationID"]] = new UserNotification();
			$this->_arrObjects[$arrRow["intUserNotificationID"]]->setVarsFromRow($arrRow);
		}
	}
	
	function loadForNotificationManagement($intUserID) {
		if(!$intUserID)
			return false;

		$objSearch = $_SESSION["objSearch"];

		$strSQL = " SELECT SQL_CALC_FOUND_ROWS tblUser.*, tblNotification.*, tblUserNotification.* FROM dbSystem.tblUserNotification
			INNER JOIN dbSystem.tblNotification
				ON tblUserNotification.intNotificationID = tblNotification.intNotificationID
			LEFT JOIN dbPLM.tblUser
				ON tblUser.intUserID = tblUserNotification.intSentByUserID
			WHERE dtmDeleted IS NULL
			AND tblUserNotification.intUserID = ".self::getDB()->sanitize($intUserID);
		$strSQL .= $objSearch->getNotificationListPage()->getSortQuery();
		//echo $strSQL . "<br />";
		$rsResult = $this->getDB()->query($strSQL);
		while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
			$this->_arrObjects[$arrRow["intUserNotificationID"]] = new UserNotification();
			$this->_arrObjects[$arrRow["intUserNotificationID"]]->setVarsFromRow($arrRow);
			$this->_arrObjects[$arrRow["intUserNotificationID"]]->getSentByUser()->setVarsFromRow($arrRow);
		}

		$this->setFoundRows(self::getDB()->found_rows());
	}
}

require_once("DataClass.class.php");
require_once("Notification.class.php");

class UserNotificationBase extends Notification {
	protected $_intUserNotificationID;
	protected $_intNotificationID;
	protected $_intUserID;
	protected $_intSentByUserID;
	protected $_dtmSent;
	protected $_dtmRead;
	protected $_dtmDeleted;

	function __construct($intUserNotificationID=null) {
		$this->DataClass();
		if($intUserNotificationID) {
			$this->load($intUserNotificationID);
		}
	}

	protected function insert() {
		base::write_log("User Notification created","S");
		$strSQL = "INSERT INTO dbSystem.tblUserNotification SET ";
		$strConnector = "";
		$strSQL .= $strConnector . "intUserNotificationID = ".$this->getDB()->sanitize(self::getUserNotificationID());
		$strConnector = ",";
		if(isset($this->_intNotificationID)) {
			$strSQL .= $strConnector . "intNotificationID = ".$this->getDB()->sanitize(self::getNotificationID());
			$strConnector = ",";
		}
		if(isset($this->_intUserID)) {
			$strSQL .= $strConnector . "intUserID = ".$this->getDB()->sanitize(self::getUserID());
			$strConnector = ",";
		}
		if(isset($this->_intSentByUserID)) {
			$strSQL .= $strConnector . "intSentByUserID = ".$this->getDB()->sanitize(self::getSentByUserID());
			$strConnector = ",";
		}
		if(isset($this->_dtmSent)) {
			$strSQL .= $strConnector . "dtmSent = ".$this->getDB()->sanitize(self::getSent());
			$strConnector = ",";
		}
		if(isset($this->_dtmRead)) {
			$strSQL .= $strConnector . "dtmRead = ".$this->getDB()->sanitize(self::getRead());
			$strConnector = ",";
		}
		if(isset($this->_dtmDeleted)) {
			$strSQL .= $strConnector . "dtmDeleted = ".$this->getDB()->sanitize(self::getDeleted());
			$strConnector = ",";
		}
		//echo $strSQL;
		$this->getDB()->query($strSQL);
		$this->setUserNotificationID($this->getDB()->insert_id());
		return $this->getUserNotificationID();
	}

	protected function update() {
		base::write_log("User Notification Update","S");
		$strSQL = "UPDATE dbSystem.tblUserNotification SET ";
		$strConnector = "";
		$strSQL .= $strConnector . "intUserNotificationID = ".$this->getDB()->sanitize(self::getUserNotificationID());
		$strConnector = ",";
		if(isset($this->_intNotificationID)) {
			$strSQL .= $strConnector . "intNotificationID = ".$this->getDB()->sanitize(self::getNotificationID());
			$strConnector = ",";
		}
		if(isset($this->_intUserID)) {
			$strSQL .= $strConnector . "intUserID = ".$this->getDB()->sanitize(self::getUserID());
			$strConnector = ",";
		}
		if(isset($this->_intSentByUserID)) {
			$strSQL .= $strConnector . "intSentByUserID = ".$this->getDB()->sanitize(self::getSentByUserID());
			$strConnector = ",";
		}
		if(isset($this->_dtmSent)) {
			$strSQL .= $strConnector . "dtmSent = ".$this->getDB()->sanitize(self::getSent());
			$strConnector = ",";
		}
		if(isset($this->_dtmRead)) {
			$strSQL .= $strConnector . "dtmRead = ".$this->getDB()->sanitize(self::getRead());
			$strConnector = ",";
		}
		if(isset($this->_dtmDeleted)) {
			$strSQL .= $strConnector . "dtmDeleted = ".$this->getDB()->sanitize(self::getDeleted());
			$strConnector = ",";
		}
		$strSQL .= " WHERE intUserNotificationID = ".$this->getDB()->sanitize(self::getUserNotificationID())."";
		//echo $strSQL;
		return $this->getDB()->query($strSQL);
	}

	public function save() {
		if($this->_intUserNotificationID) {
			return $this->update();
		} else {
			return $this->insert();
		}
	}

	public function delete() {
		if($this->_intUserNotificationID) {
			$strSQL = "DELETE FROM dbSystem.tblUserNotification
				WHERE intUserNotificationID = '$this->_intUserNotificationID'
				";
			return $this->getDB()->query($strSQL);
		}
	}

	public function load($intUserNotificationID) {
		if(!$intUserNotificationID) {
			return false;
		}

		$strSQL = "SELECT tblNotification.*, tblUserNotification.*
				FROM dbSystem.tblUserNotification
				INNER JOIN dbSystem.tblNotification
					ON tblUserNotification.intNotificationID = tblNotification.intNotificationID
				WHERE intUserNotificationID = '$intUserNotificationID'
				LIMIT 1
			";
		$rsUserNotification = $this->getDB()->query($strSQL);
		$arrUserNotification = $this->getDB()->fetch_assoc($rsUserNotification);
		$this->setVarsFromRow($arrUserNotification);
	}

	function setVarsFromRow($arrUserNotification) {
		parent::setVarsFromRow($arrUserNotification);
		if(isset($arrUserNotification["intUserNotificationID"])) $this->_intUserNotificationID = $arrUserNotification["intUserNotificationID"];
		if(isset($arrUserNotification["intNotificationID"])) $this->_intNotificationID = $arrUserNotification["intNotificationID"];
		if(isset($arrUserNotification["intUserID"])) $this->_intUserID = $arrUserNotification["intUserID"];
		if(isset($arrUserNotification["intSentByUserID"])) $this->_intSentByUserID = $arrUserNotification["intSentByUserID"];
		if(isset($arrUserNotification["dtmSent"])) $this->_dtmSent = $arrUserNotification["dtmSent"];
		if(isset($arrUserNotification["dtmRead"])) $this->_dtmRead = $arrUserNotification["dtmRead"];
		if(isset($arrUserNotification["dtmDeleted"])) $this->_dtmDeleted = $arrUserNotification["dtmDeleted"];
	}

	function getUserNotificationID() {
		return $this->_intUserNotificationID;
	}
	function setUserNotificationID($value) {
		if($this->_intUserNotificationID !== $value) {
			$this->_intUserNotificationID = $value;
			$this->_blnDirty = true;
		}
	}

	function getNotificationID() {
		return $this->_intNotificationID;
	}
	function setNotificationID($value) {
		if($this->_intNotificationID !== $value) {
			$this->_intNotificationID = $value;
			$this->_blnDirty = true;
		}
	}

	function getUserID() {
		return $this->_intUserID;
	}
	function setUserID($value) {
		if($this->_intUserID !== $value) {
			$this->_intUserID = $value;
			$this->_blnDirty = true;
		}
	}

	function getSentByUserID() {
		return $this->_intSentByUserID;
	}
	function setSentByUserID($value) {
		if($this->_intSentByUserID !== $value) {
			$this->_intSentByUserID = $value;
			$this->_blnDirty = true;
		}
	}

	function getSent() {
		return $this->_dtmSent;
	}
	function setSent($value) {
		if($this->_dtmSent !== $value) {
			$this->_dtmSent = $value;
			$this->_blnDirty = true;
		}
	}

	function getRead() {
		return $this->_dtmRead;
	}
	function setRead($value) {
		if($this->_dtmRead !== $value) {
			$this->_dtmRead = $value;
			$this->_blnDirty = true;
		}
	}

	function getDeleted() {
		return $this->_dtmDeleted;
	}
	function setDeleted($value) {
		if($this->_dtmDeleted !== $value) {
			$this->_dtmDeleted = $value;
			$this->_blnDirty = true;
		}
	}

}

include_once("User.class.php");

class UserNotification extends UserNotificationBase {
	private $_objUser;
	private $_objSentByUser;

	function __construct($intUserNotificationID=null) {
		parent::__construct($intUserNotificationID);
	}

	function delete(){
		if(!$this->_intUserNotificationID)
			return;

		$this->setDeleted(NOW());
		$this->save();
	}

	function getUser(){
		if(!$this->_objUser)
			$this->_objUser = new User();
		return $this->_objUser;
	}
	function setUser($value){
		$this->_objUser = $value;
	}
	function getSentByUser(){
		if(!$this->_objSentByUser)
			$this->_objSentByUser = new User();
		return $this->_objSentByUser;
	}
}
?>