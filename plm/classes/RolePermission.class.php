<?php
	require_once("ArrayClass.class.php");

	class RolePermissionArray extends ArrayClass {
		function __construct(){
		}

		function load() {
			$strSQL = " SELECT * FROM dbPLM.tblRolePermission";
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				$this->_arrObjects[$arrRow["intRolePermissionID"]] = new RolePermission();
				$this->_arrObjects[$arrRow["intRolePermissionID"]]->setVarsFromRow($arrRow);
			}
		}

	}

	require_once("DataClass.class.php");

	class RolePermissionBase extends DataClass {
		protected $_intRolePermissionID;
		protected $_intRoleID;
		protected $_intPermissionID;
		protected $_intCreatedByUserID;
		protected $_dtmCreatedOn;

		function __construct($intRolePermissionID=null) {
			$this->DataClass();
			if($intRolePermissionID) {
				$this->load($intRolePermissionID);
			}
		}

		protected function insert() {
			base::write_log("Role permission created","S");
			$strSQL = "INSERT INTO dbPLM.tblRolePermission SET ";
			$strConnector = "";
			$strSQL .= $strConnector . "intRolePermissionID = ".$this->getDB()->sanitize(self::getRolePermissionID());
			$strConnector = ",";
			if(isset($this->_intRoleID)) {
				$strSQL .= $strConnector . "intRoleID = ".$this->getDB()->sanitize(self::getRoleID());
				$strConnector = ",";
			}
			if(isset($this->_intPermissionID)) {
				$strSQL .= $strConnector . "intPermissionID = ".$this->getDB()->sanitize(self::getPermissionID());
				$strConnector = ",";
			}
			if(isset($this->_intCreatedByUserID)) {
				$strSQL .= $strConnector . "intCreatedByUserID = ".$this->getDB()->sanitize(self::getCreatedByUserID());
				$strConnector = ",";
			}
			if(isset($this->_dtmCreatedOn)) {
				$strSQL .= $strConnector . "dtmCreatedOn = ".$this->getDB()->sanitize(self::getCreatedOn());
				$strConnector = ",";
			}
			//echo $strSQL;
			$this->getDB()->query($strSQL);
			$this->setRolePermissionID($this->getDB()->insert_id());
			return $this->getRolePermissionID();
		}

		protected function update() {
			base::write_log("Role permission Update","S");
			$strSQL = "UPDATE dbPLM.tblRolePermission SET ";
			$strConnector = "";
			$strSQL .= $strConnector . "intRolePermissionID = ".$this->getDB()->sanitize(self::getRolePermissionID());
			$strConnector = ",";
			if(isset($this->_intRoleID)) {
				$strSQL .= $strConnector . "intRoleID = ".$this->getDB()->sanitize(self::getRoleID());
				$strConnector = ",";
			}
			if(isset($this->_intPermissionID)) {
				$strSQL .= $strConnector . "intPermissionID = ".$this->getDB()->sanitize(self::getPermissionID());
				$strConnector = ",";
			}
			if(isset($this->_intCreatedByUserID)) {
				$strSQL .= $strConnector . "intCreatedByUserID = ".$this->getDB()->sanitize(self::getCreatedByUserID());
				$strConnector = ",";
			}
			if(isset($this->_dtmCreatedOn)) {
				$strSQL .= $strConnector . "dtmCreatedOn = ".$this->getDB()->sanitize(self::getCreatedOn());
				$strConnector = ",";
			}
			$strSQL .= " WHERE intRolePermissionID = ".$this->getDB()->sanitize(self::getRolePermissionID())."";
			//echo $strSQL;
			return $this->getDB()->query($strSQL);
		}

		public function save() {
			if($this->_intRolePermissionID) {
				return $this->update();
			} else {
				return $this->insert();
			}
		}

		public function delete() {
			if($this->_intRolePermissionID) {
				base::write_log("Role permission delete","S");
				$strSQL = "DELETE FROM dbPLM.tblRolePermission
				WHERE intRolePermissionID = '$this->_intRolePermissionID'
				";
				return $this->getDB()->query($strSQL);
			}
		}

		public function load($intRolePermissionID) {
			if(!$intRolePermissionID) {
				return false;
			}

			$strSQL = "SELECT *
				FROM dbPLM.tblRolePermission
				WHERE intRolePermissionID = '$intRolePermissionID'
				LIMIT 1
			";
			$rsRolePermission = $this->getDB()->query($strSQL);
			$arrRolePermission = $this->getDB()->fetch_assoc($rsRolePermission);
			$this->setVarsFromRow($arrRolePermission);
		}

		function setVarsFromRow($arrRolePermission) {
			if(isset($arrRolePermission["intRolePermissionID"])) $this->_intRolePermissionID = $arrRolePermission["intRolePermissionID"];
			if(isset($arrRolePermission["intRoleID"])) $this->_intRoleID = $arrRolePermission["intRoleID"];
			if(isset($arrRolePermission["intPermissionID"])) $this->_intPermissionID = $arrRolePermission["intPermissionID"];
			if(isset($arrRolePermission["intCreatedByUserID"])) $this->_intCreatedByUserID = $arrRolePermission["intCreatedByUserID"];
			if(isset($arrRolePermission["dtmCreatedOn"])) $this->_dtmCreatedOn = $arrRolePermission["dtmCreatedOn"];
		}

		function getRolePermissionID() {
			return $this->_intRolePermissionID;
		}
		function setRolePermissionID($value) {
			if($this->_intRolePermissionID !== $value) {
				$this->_intRolePermissionID = $value;
				$this->_blnDirty = true;
			}
		}

		function getRoleID() {
			return $this->_intRoleID;
		}
		function setRoleID($value) {
			if($this->_intRoleID !== $value) {
				$this->_intRoleID = $value;
				$this->_blnDirty = true;
			}
		}

		function getPermissionID() {
			return $this->_intPermissionID;
		}
		function setPermissionID($value) {
			if($this->_intPermissionID !== $value) {
				$this->_intPermissionID = $value;
				$this->_blnDirty = true;
			}
		}

		function getCreatedByUserID() {
			return $this->_intCreatedByUserID;
		}
		function setCreatedByUserID($value) {
			if($this->_intCreatedByUserID !== $value) {
				$this->_intCreatedByUserID = $value;
				$this->_blnDirty = true;
			}
		}

		function getCreatedOn() {
			return $this->_dtmCreatedOn;
		}
		function setCreatedOn($value) {
			if($this->_dtmCreatedOn !== $value) {
				$this->_dtmCreatedOn = $value;
				$this->_blnDirty = true;
			}
		}

	}

	class RolePermission extends RolePermissionBase {
		function __construct($intRolePermissionID=null) {
			parent::__construct($intRolePermissionID);
		}

		function loadByRoleIDAndPermissionID($intRoleID, $intPermissionID){
			if(!$intRoleID || !$intPermissionID) {
				return false;
			}

			$strSQL = "SELECT *
				FROM dbPLM.tblRolePermission
				WHERE intRoleID = ".$this->getDB()->sanitize($intRoleID)."
				AND intPermissionID = ".$this->getDB()->sanitize($intPermissionID)."
				LIMIT 1
			";
			//echo $strSQL;
			$rsResult = $this->getDB()->query($strSQL);
			$arrRow = $this->getDB()->fetch_assoc($rsResult);
			$this->setVarsFromRow($arrRow);
		}

		function deleteByRoleIDAndPermissionID($intRoleID, $intPermissionID){
			if(!$intRoleID || !$intPermissionID)
				return false;

			$strSQL = "DELETE FROM dbPLM.tblRolePermission
				WHERE intRoleID = ".self::getDB()->sanitize($intRoleID)."
				AND intPermissionID = ".self::getDB()->sanitize($intPermissionID);
			return self::getDB()->query($strSQL);
		}
	}
?>
