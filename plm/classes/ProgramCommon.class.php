<?php
require_once("ArrayClass.class.php");

class ProgramCommonArray extends ArrayClass {
	function __construct(){
		parent::__construct("ProgramCommon");
	}

	function load() {
		$strSQL = " SELECT tblPlatform.*, tblProduct.*, tblProgramCommon.*
			FROM tblProgramCommon
			INNER JOIN dbPLM.tblProduct
				ON tblProduct.intProgramID = tblProgramCommon.intTopLevelProductID
			INNER JOIN dbPLM.tblPlatform
				ON tblPlatform.intPlatformID = tblProgramCommon.intPlatformID
			ORDER BY strPlatform, strProgramName ";
		$rsResult = $this->getDB()->query($strSQL);
		while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
			$this->_arrObjects[$arrRow["intProgramCommonID"]] = new ProgramCommon();
			$this->_arrObjects[$arrRow["intProgramCommonID"]]->setVarsFromRow($arrRow);
			$this->_arrObjects[$arrRow["intProgramCommonID"]]->getTopLevelProduct()->setVarsFromRow($arrRow);
			$this->_arrObjects[$arrRow["intProgramCommonID"]]->getPlatform()->setVarsFromRow($arrRow);
		}
	}

	function loadByCustomerID($intCustomerID) {
		$strSQL = " SELECT tblPlatform.*, tblProduct.*, tblProgramCommon.*
			FROM tblProgramCommon
			INNER JOIN dbPLM.tblProduct
				ON tblProduct.intProgramID = tblProgramCommon.intTopLevelProductID
			INNER JOIN dbPLM.tblPlatform
				ON tblPlatform.intPlatformID = tblProgramCommon.intPlatformID
			WHERE tblProgramCommon.intCustomerID = ".self::getDB()->sanitize($intCustomerID)."
			ORDER BY strPlatform, strProgramName ";
		$rsResult = $this->getDB()->query($strSQL);
		while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
			$this->_arrObjects[$arrRow["intProgramCommonID"]] = new ProgramCommon();
			$this->_arrObjects[$arrRow["intProgramCommonID"]]->setVarsFromRow($arrRow);
			$this->_arrObjects[$arrRow["intProgramCommonID"]]->getTopLevelProduct()->setVarsFromRow($arrRow);
			$this->_arrObjects[$arrRow["intProgramCommonID"]]->getPlatform()->setVarsFromRow($arrRow);
		}
	}

	function loadForDocumentListPage() {
		$objSearch = $_SESSION["objSearch"];
		$strSQL = DocumentArray::getDocumentListPageQuery($objSearch, "Program Common");

		$rsResult = $this->getDB()->query($strSQL);
		while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
			$this->_arrObjects[$arrRow["intProgramCommonID"]] = new ProgramCommon();
			$this->_arrObjects[$arrRow["intProgramCommonID"]]->setVarsFromRow($arrRow);
			$this->_arrObjects[$arrRow["intProgramCommonID"]]->getTopLevelProduct()->setVarsFromRow($arrRow);
			$this->_arrObjects[$arrRow["intProgramCommonID"]]->getPlatform()->setVarsFromRow($arrRow);
		}

		$strSearchValue = $objSearch->getProgramCommonID();
		if($strSearchValue && !isset($this->_arrObjects[$strSearchValue])) { // Search record not in options, add search record
			$this->createNew($strSearchValue)->load($strSearchValue);
			$this->getObject($strSearchValue)->getTopLevelProduct(true);
			$this->getObject($strSearchValue)->getPlatform(true); // TODO CODE CLEAN: Can make this more efficient by putting into a single function
		}
	}

	function loadByProgramCommonIDs($arrProgramCommonIDs) {
		if(!$arrProgramCommonIDs)
			return;

		$strSQL = " SELECT tblPlatform.*, tblProduct.*, tblProgramCommon.*
			FROM tblProgramCommon
			INNER JOIN dbPLM.tblProduct
				ON tblProduct.intProgramID = tblProgramCommon.intTopLevelProductID
			INNER JOIN dbPLM.tblPlatform
				ON tblPlatform.intPlatformID = tblProgramCommon.intPlatformID
			WHERE tblProgramCommon.intProgramCommonID IN ('".implode("','", $arrProgramCommonIDs)."')
			ORDER BY strPlatform, strProgramName ";
		$rsResult = $this->getDB()->query($strSQL);
		while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
			$this->_arrObjects[$arrRow["intProgramCommonID"]] = new ProgramCommon();
			$this->_arrObjects[$arrRow["intProgramCommonID"]]->setVarsFromRow($arrRow);
			$this->_arrObjects[$arrRow["intProgramCommonID"]]->getTopLevelProduct()->setVarsFromRow($arrRow);
			$this->_arrObjects[$arrRow["intProgramCommonID"]]->getPlatform()->setVarsFromRow($arrRow);
		}
	}

	function getProgramTimelineListPageQuery($objSearch) {
		$strSQL = "SELECT tblProduct.*, tblPlatform.*, tblProgramCommon.*
			FROM dbPLM.tblProgramCommon
			INNER JOIN dbPLM.tblPlatform
				ON tblPlatform.intPlatformID = tblProgramCommon.intPlatformID
			INNER JOIN dbPLM.tblProduct
				ON tblProduct.intProgramID = tblProgramCommon.intTopLevelProductID
		";
		return $strSQL;
	}

	function loadForProgramTimeline() {
		$objSearch = $_SESSION["objSearch"];
		$strSQL = ProgramCommonArray::getProgramTimelineListPageQuery($objSearch, "Program Timeline");

		$rsResult = $this->getDB()->query($strSQL);
		while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
			$this->_arrObjects[$arrRow["intProgramCommonID"]] = new ProgramCommon();
			$this->_arrObjects[$arrRow["intProgramCommonID"]]->setVarsFromRow($arrRow);
			$this->_arrObjects[$arrRow["intProgramCommonID"]]->getTopLevelProduct()->setVarsFromRow($arrRow);
			$this->_arrObjects[$arrRow["intProgramCommonID"]]->getPlatform()->setVarsFromRow($arrRow);
		}

		$arrProgramCommonIDs = array_keys($this->_arrObjects);
		$this->loadProgramMilestoneArrayByProgramCommonIDs($arrProgramCommonIDs);
	}

	function loadProgramMilestoneArrayByProgramCommonIDs($arrProgramCommonIDs) {
		if(!$arrProgramCommonIDs || !is_array($arrProgramCommonIDs)) {
			return;
		}

		$strSQL = "SELECT tblProgramCommon.*, tblMilestone.*, tblProgramMilestone.*
			FROM dbPLM.tblProgramMilestone
			INNER JOIN dbPLM.tblMilestone
				ON tblMilestone.intMilestoneID = tblProgramMilestone.intMilestoneID
			INNER JOIN dbPLM.tblProduct
				ON tblProduct.intProgramID = tblProgramMilestone.intProgramID
			INNER JOIN dbPLM.tblProgramCommon
				ON tblProgramCommon.intTopLevelProductID = tblProduct.intProgramID
			WHERE tblProgramCommon.intProgramCommonID IN ('".implode("','", $arrProgramCommonIDs)."')";
		$rsResult = $this->getDB()->query($strSQL);
		while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
			$intWeek = previousSunday(date("Y-m-d", strtotime($arrRow["dtmDueDate"])));
			$this->_arrObjects[$arrRow["intProgramCommonID"]]->getTopLevelProduct()->getProgramMilestoneArray()->createNew($intWeek);
			$this->_arrObjects[$arrRow["intProgramCommonID"]]->getTopLevelProduct()->getProgramMilestoneArray()->getObject($intWeek)->setVarsFromRow($arrRow);
			$this->_arrObjects[$arrRow["intProgramCommonID"]]->getTopLevelProduct()->getProgramMilestoneArray()->getObject($intWeek)->getMilestone()->setVarsFromRow($arrRow);
		}
	}
}
require_once("DataClass.class.php");

class ProgramCommonBase extends DataClass {
	protected $_intProgramCommonID;
	protected $_intTopLevelProductID;
	protected $_intTopLevelFolderID;
	protected $_intCustomerID;
	protected $_intPlatformID;
	protected $_intProgramTypeID;
	protected $_intProgramSubTypeID;
	protected $_strStatus;
	protected $_strRisk;
	protected $_blnDesignResponsible;
	protected $_blnTakeOver;
	protected $_blnDesignReleased;
	protected $_dblModelYear;
	protected $_dblLastModelYear;
	protected $_strQuoteNumber;
	protected $_strBuyer;
	protected $_intEngineeringBudget;
	protected $_intCapitalEquipmentBudget;
	protected $_intCapitalPackagingBudget;
	protected $_intToolingBudget;
	protected $_intCurrencyID;
	protected $_txtNotes;

	function __construct($intProgramCommonID=null) {
		$this->DataClass(intTABLE_ID_PROGRAM_COMMON);
		if($intProgramCommonID) {
			$this->load($intProgramCommonID);
		}
	}

	protected function insert() {
		base::write_log("Program Common created","S");
		$strSQL = "INSERT INTO tblProgramCommon SET ";
		$strConnector = "";
		$strSQL .= $strConnector . "intProgramCommonID = ".$this->getDB()->sanitize(self::getProgramCommonID());
		$strConnector = ",";
		if(isset($this->_intTopLevelProductID)) {
			$strSQL .= $strConnector . "intTopLevelProductID = ".$this->getDB()->sanitize(self::getTopLevelProductID());
			$strConnector = ",";
		}
		if(isset($this->_intTopLevelFolderID)) {
			$strSQL .= $strConnector . "intTopLevelFolderID = ".$this->getDB()->sanitize(self::getTopLevelFolderID());
			$strConnector = ",";
		}
		if(isset($this->_intCustomerID)) {
			$strSQL .= $strConnector . "intCustomerID = ".$this->getDB()->sanitize(self::getCustomerID());
			$strConnector = ",";
		}
		if(isset($this->_intPlatformID)) {
			$strSQL .= $strConnector . "intPlatformID = ".$this->getDB()->sanitize(self::getPlatformID());
			$strConnector = ",";
		}
		if(isset($this->_intProgramTypeID)) {
			$strSQL .= $strConnector . "intProgramTypeID = ".$this->getDB()->sanitize(self::getProgramTypeID());
			$strConnector = ",";
		}
		if(isset($this->_intProgramSubTypeID)) {
			$strSQL .= $strConnector . "intProgramSubTypeID = ".$this->getDB()->sanitize(self::getProgramSubTypeID());
			$strConnector = ",";
		}
		if(isset($this->_strStatus)) {
			$strSQL .= $strConnector . "strStatus = ".$this->getDB()->sanitize(self::getStatus());
			$strConnector = ",";
		}
		if(isset($this->_strRisk)) {
			$strSQL .= $strConnector . "strRisk = ".$this->getDB()->sanitize(self::getRisk());
			$strConnector = ",";
		}
		if(isset($this->_blnDesignResponsible) || @is_null($this->_blnDesignResponsible)) {
			$strSQL .= $strConnector . "blnDesignResponsible = ".$this->getDB()->sanitize(self::getDesignResponsible());
			$strConnector = ",";
		}
		if(isset($this->_blnTakeOver)) {
			$strSQL .= $strConnector . "blnTakeOver = ".$this->getDB()->sanitize(self::getTakeOver());
			$strConnector = ",";
		}
		if(isset($this->_blnDesignReleased)) {
			$strSQL .= $strConnector . "blnDesignReleased = ".$this->getDB()->sanitize(self::getDesignReleased());
			$strConnector = ",";
		}
		if(isset($this->_dblModelYear)) {
			$strSQL .= $strConnector . "dblModelYear = ".$this->getDB()->sanitize(self::getModelYear());
			$strConnector = ",";
		}
		if(isset($this->_dblLastModelYear)) {
			$strSQL .= $strConnector . "dblLastModelYear = ".$this->getDB()->sanitize(self::getLastModelYear());
			$strConnector = ",";
		}
		if(isset($this->_strQuoteNumber)) {
			$strSQL .= $strConnector . "strQuoteNumber = ".$this->getDB()->sanitize(self::getQuoteNumber());
			$strConnector = ",";
		}
		if(isset($this->_strBuyer)) {
			$strSQL .= $strConnector . "strBuyer = ".$this->getDB()->sanitize(self::getBuyer());
			$strConnector = ",";
		}
		if(isset($this->_intEngineeringBudget)) {
			$strSQL .= $strConnector . "intEngineeringBudget = ".$this->getDB()->sanitize(self::getEngineeringBudget());
			$strConnector = ",";
		}
		if(isset($this->_intCapitalEquipmentBudget)) {
			$strSQL .= $strConnector . "intCapitalEquipmentBudget = ".$this->getDB()->sanitize(self::getCapitalEquipmentBudget());
			$strConnector = ",";
		}
		if(isset($this->_intCapitalPackagingBudget)) {
			$strSQL .= $strConnector . "intCapitalPackagingBudget = ".$this->getDB()->sanitize(self::getCapitalPackagingBudget());
			$strConnector = ",";
		}
		if(isset($this->_intToolingBudget)) {
			$strSQL .= $strConnector . "intToolingBudget = ".$this->getDB()->sanitize(self::getToolingBudget());
			$strConnector = ",";
		}
		if(isset($this->_intCurrencyID)) {
			$strSQL .= $strConnector . "intCurrencyID = ".$this->getDB()->sanitize(self::getCurrencyID());
			$strConnector = ",";
		}
		if(isset($this->_txtNotes)) {
			$strSQL .= $strConnector . "txtNotes = ".$this->getDB()->sanitize(self::getNotes());
			$strConnector = ",";
		}
		//echo $strSQL;
		$this->getDB()->query($strSQL);
		$this->setProgramCommonID($this->getDB()->insert_id());
		return $this->getProgramCommonID();
	}

	protected function update() {
		parent::update();
		base::write_log("Program Common Update","S");
		$strSQL = "UPDATE tblProgramCommon SET ";
		$strConnector = "";
		$strSQL .= $strConnector . "intProgramCommonID = ".$this->getDB()->sanitize(self::getProgramCommonID());
		$strConnector = ",";
		if(isset($this->_intTopLevelProductID)) {
			$strSQL .= $strConnector . "intTopLevelProductID = ".$this->getDB()->sanitize(self::getTopLevelProductID());
			$strConnector = ",";
		}
		if(isset($this->_intTopLevelFolderID)) {
			$strSQL .= $strConnector . "intTopLevelFolderID = ".$this->getDB()->sanitize(self::getTopLevelFolderID());
			$strConnector = ",";
		}
		if(isset($this->_intCustomerID)) {


			$strSQL .= $strConnector . "intCustomerID = ".$this->getDB()->sanitize(self::getCustomerID());
			$strConnector = ",";
		}
		if(isset($this->_intPlatformID)) {
			$strSQL .= $strConnector . "intPlatformID = ".$this->getDB()->sanitize(self::getPlatformID());
			$strConnector = ",";
		}
		if(isset($this->_intProgramTypeID)) {
			$strSQL .= $strConnector . "intProgramTypeID = ".$this->getDB()->sanitize(self::getProgramTypeID());
			$strConnector = ",";
		}
		if(isset($this->_intProgramSubTypeID)) {
			$strSQL .= $strConnector . "intProgramSubTypeID = ".$this->getDB()->sanitize(self::getProgramSubTypeID());
			$strConnector = ",";
		}
		if(isset($this->_strStatus)) {
			$strSQL .= $strConnector . "strStatus = ".$this->getDB()->sanitize(self::getStatus());
			$strConnector = ",";
		}
		if(isset($this->_strRisk)) {
			$strSQL .= $strConnector . "strRisk = ".$this->getDB()->sanitize(self::getRisk());
			$strConnector = ",";
		}
		if(isset($this->_blnDesignResponsible) || @is_null($this->_blnDesignResponsible)) {
			$strSQL .= $strConnector . "blnDesignResponsible = ".$this->getDB()->sanitize(self::getDesignResponsible());
			$strConnector = ",";
		}
		if(isset($this->_blnTakeOver)) {
			$strSQL .= $strConnector . "blnTakeOver = ".$this->getDB()->sanitize(self::getTakeOver());
			$strConnector = ",";
		}
		if(isset($this->_blnDesignReleased)) {
			$strSQL .= $strConnector . "blnDesignReleased = ".$this->getDB()->sanitize(self::getDesignReleased());
			$strConnector = ",";
		}
		if(isset($this->_dblModelYear)) {
			$strSQL .= $strConnector . "dblModelYear = ".$this->getDB()->sanitize(self::getModelYear());
			$strConnector = ",";
		}
		if(isset($this->_dblLastModelYear)) {
			$strSQL .= $strConnector . "dblLastModelYear = ".$this->getDB()->sanitize(self::getLastModelYear());
			$strConnector = ",";
		}
		if(isset($this->_strQuoteNumber)) {
			$strSQL .= $strConnector . "strQuoteNumber = ".$this->getDB()->sanitize(self::getQuoteNumber());
			$strConnector = ",";
		}
		if(isset($this->_strBuyer)) {
			$strSQL .= $strConnector . "strBuyer = ".$this->getDB()->sanitize(self::getBuyer());
			$strConnector = ",";
		}
		if(isset($this->_intEngineeringBudget)) {
			$strSQL .= $strConnector . "intEngineeringBudget = ".$this->getDB()->sanitize(self::getEngineeringBudget());
			$strConnector = ",";
		}
		if(isset($this->_intCapitalEquipmentBudget)) {
			$strSQL .= $strConnector . "intCapitalEquipmentBudget = ".$this->getDB()->sanitize(self::getCapitalEquipmentBudget());
			$strConnector = ",";
		}
		if(isset($this->_intCapitalPackagingBudget)) {
			$strSQL .= $strConnector . "intCapitalPackagingBudget = ".$this->getDB()->sanitize(self::getCapitalPackagingBudget());
			$strConnector = ",";
		}
		if(isset($this->_intToolingBudget)) {
			$strSQL .= $strConnector . "intToolingBudget = ".$this->getDB()->sanitize(self::getToolingBudget());
			$strConnector = ",";
		}
		if(isset($this->_intCurrencyID)) {
			$strSQL .= $strConnector . "intCurrencyID = ".$this->getDB()->sanitize(self::getCurrencyID());
			$strConnector = ",";
		}
		if(isset($this->_txtNotes)) {
			$strSQL .= $strConnector . "txtNotes = ".$this->getDB()->sanitize(self::getNotes());
			$strConnector = ",";
		}
		$strSQL .= " WHERE intProgramCommonID = ".$this->getDB()->sanitize(self::getProgramCommonID())."";
		//echo $strSQL;
		return $this->getDB()->query($strSQL);
	}

	public function save() {
		if($this->_intProgramCommonID) {
			return $this->update();
		} else {
			return $this->insert();
		}
	}

	public function delete() {
		if($this->_intProgramCommonID) {
			$strSQL = "DELETE FROM tblProgramCommon
				WHERE intProgramCommonID = '$this->_intProgramCommonID'
				";
			return $this->getDB()->query($strSQL);
		}
	}

	public function load($intProgramCommonID) {
		if(!$intProgramCommonID) {
			return false;
		}

		$strSQL = "SELECT *
				FROM tblProgramCommon
				WHERE intProgramCommonID = ".self::getDB()->sanitize($intProgramCommonID)."
				LIMIT 1
			";
		$rsResult = $this->getDB()->query($strSQL);
		$arrRow = $this->getDB()->fetch_assoc($rsResult);
		$this->setVarsFromRow($arrRow);
	}

	function setVarsFromRow($arrRow) {
		if(isset($arrRow["intProgramCommonID"])) $this->_intProgramCommonID = $arrRow["intProgramCommonID"];
		if(isset($arrRow["intTopLevelProductID"])) $this->_intTopLevelProductID = $arrRow["intTopLevelProductID"];
		if(isset($arrRow["intTopLevelFolderID"])) $this->_intTopLevelFolderID = $arrRow["intTopLevelFolderID"];
		if(isset($arrRow["intCustomerID"])) $this->_intCustomerID = $arrRow["intCustomerID"];
		if(isset($arrRow["intPlatformID"])) $this->_intPlatformID = $arrRow["intPlatformID"];
		if(isset($arrRow["intProgramTypeID"])) $this->_intProgramTypeID = $arrRow["intProgramTypeID"];
		if(isset($arrRow["intProgramSubTypeID"])) $this->_intProgramSubTypeID = $arrRow["intProgramSubTypeID"];
		if(isset($arrRow["strStatus"])) $this->_strStatus = $arrRow["strStatus"];
		if(isset($arrRow["strRisk"])) $this->_strRisk = $arrRow["strRisk"];
		if(isset($arrRow["blnDesignResponsible"])) $this->_blnDesignResponsible = $arrRow["blnDesignResponsible"];
		if(isset($arrRow["blnTakeOver"])) $this->_blnTakeOver = $arrRow["blnTakeOver"];
		if(isset($arrRow["blnDesignReleased"])) $this->_blnDesignReleased = $arrRow["blnDesignReleased"];
		if(isset($arrRow["dblModelYear"])) $this->_dblModelYear = $arrRow["dblModelYear"];
		if(isset($arrRow["dblLastModelYear"])) $this->_dblLastModelYear = $arrRow["dblLastModelYear"];
		if(isset($arrRow["strQuoteNumber"])) $this->_strQuoteNumber = $arrRow["strQuoteNumber"];
		if(isset($arrRow["strBuyer"])) $this->_strBuyer = $arrRow["strBuyer"];
		if(isset($arrRow["intEngineeringBudget"])) $this->_intEngineeringBudget = $arrRow["intEngineeringBudget"];
		if(isset($arrRow["intCapitalEquipmentBudget"])) $this->_intCapitalEquipmentBudget = $arrRow["intCapitalEquipmentBudget"];
		if(isset($arrRow["intCapitalPackagingBudget"])) $this->_intCapitalPackagingBudget = $arrRow["intCapitalPackagingBudget"];
		if(isset($arrRow["intToolingBudget"])) $this->_intToolingBudget = $arrRow["intToolingBudget"];
		if(isset($arrRow["intCurrencyID"])) $this->_intCurrencyID = $arrRow["intCurrencyID"];
		if(isset($arrRow["txtNotes"])) $this->_txtNotes = $arrRow["txtNotes"];
	}

	function getProgramCommonID() {
		return $this->_intProgramCommonID;
	}
	function setProgramCommonID($value) {
		if($this->_intProgramCommonID !== $value) {
			$this->_intProgramCommonID = $value;
			$this->_blnDirty = true;
		}
	}

	function getTopLevelProductID() {
		return $this->_intTopLevelProductID;
	}
	function setTopLevelProductID($value) {
		if($this->_intTopLevelProductID !== $value) {
			$this->_intTopLevelProductID = $value;
			$this->_blnDirty = true;
		}
	}

	function getTopLevelFolderID() {
		return $this->_intTopLevelFolderID;
	}
	function setTopLevelFolderID($value) {
		if($this->_intTopLevelFolderID !== $value) {
			$this->_intTopLevelFolderID = $value;
			$this->_blnDirty = true;
		}
	}

	function getCustomerID() {
		return $this->_intCustomerID;
	}
	function setCustomerID($value) {
		if($this->_intCustomerID !== $value) {
			$this->_intCustomerID = $value;
			$this->_blnDirty = true;
		}
	}

	function getPlatformID() {
		return $this->_intPlatformID;
	}
	function setPlatformID($value) {
		if($this->_intPlatformID !== $value) {
			$this->_intPlatformID = $value;
			$this->_blnDirty = true;
		}
	}


	function getProgramTypeID() {
		return $this->_intProgramTypeID;
	}
	function setProgramTypeID($value) {
		if($this->_intProgramTypeID !== $value) {
			$this->_intProgramTypeID = $value;
			$this->_blnDirty = true;
		}
	}

	function getProgramSubTypeID() {
		return $this->_intProgramSubTypeID;
	}
	function setProgramSubTypeID($value) {
		if($this->_intProgramSubTypeID !== $value) {
			$this->_intProgramSubTypeID = $value;
			$this->_blnDirty = true;
		}
	}

	function getStatus() {
		return $this->_strStatus;
	}
	function setStatus($value) {
		if($this->_strStatus !== $value) {
			$this->_strStatus = $value;
			$this->_blnDirty = true;
		}
	}

	function getRisk() {
		return $this->_strRisk;
	}
	function setRisk($value) {
		if($this->_strRisk !== $value) {
			$this->_strRisk = $value;
			$this->_blnDirty = true;
		}
	}

	function getDesignResponsible() {
		return $this->_blnDesignResponsible;
	}
	function setDesignResponsible($value) {
		if($this->_blnDesignResponsible !== $value) {
			$this->_blnDesignResponsible = $value;
			$this->_blnDirty = true;
		}
	}

	function getTakeOver() {
		return $this->_blnTakeOver;
	}


	function setTakeOver($value) {
		if($this->_blnTakeOver !== $value) {
			$this->_blnTakeOver = $value;
			$this->_blnDirty = true;
		}
	}

	function getDesignReleased() {
		return $this->_blnDesignReleased;
	}
	function setDesignReleased($value) {
		if($this->_blnDesignReleased !== $value) {
			$this->_blnDesignReleased = $value;
			$this->_blnDirty = true;
		}
	}

	function getModelYear() {
		return $this->_dblModelYear;
	}
	function setModelYear($value) {
		if($this->_dblModelYear !== $value) {
			$this->_dblModelYear = $value;
			$this->_blnDirty = true;
		}
	}

	function getLastModelYear() {
		return $this->_dblLastModelYear;
	}
	function setLastModelYear($value) {
		if($this->_dblLastModelYear !== $value) {
			$this->_dblLastModelYear = $value;
			$this->_blnDirty = true;
		}
	}

	function getQuoteNumber() {
		return $this->_strQuoteNumber;
	}
	function setQuoteNumber($value) {
		if($this->_strQuoteNumber !== $value) {
			$this->_strQuoteNumber = $value;
			$this->_blnDirty = true;
		}
	}

	function getBuyer() {
		return $this->_strBuyer;
	}
	function setBuyer($value) {
		if($this->_strBuyer !== $value) {
			$this->_strBuyer = $value;
			$this->_blnDirty = true;
		}
	}

	function getEngineeringBudget() {
		return $this->_intEngineeringBudget;
	}
	function setEngineeringBudget($value) {
		if($this->_intEngineeringBudget !== $value) {
			$this->_intEngineeringBudget = $value;
			$this->_blnDirty = true;
		}
	}

	function getCapitalEquipmentBudget() {
		return $this->_intCapitalEquipmentBudget;
	}
	function setCapitalEquipmentBudget($value) {
		if($this->_intCapitalEquipmentBudget !== $value) {
			$this->_intCapitalEquipmentBudget = $value;
			$this->_blnDirty = true;
		}
	}

	function getCapitalPackagingBudget() {
		return $this->_intCapitalPackagingBudget;
	}
	function setCapitalPackagingBudget($value) {
		if($this->_intCapitalPackagingBudget !== $value) {
			$this->_intCapitalPackagingBudget = $value;
			$this->_blnDirty = true;
		}
	}

	function getToolingBudget() {
		return $this->_intToolingBudget;
	}
	function setToolingBudget($value) {
		if($this->_intToolingBudget !== $value) {
			$this->_intToolingBudget = $value;
			$this->_blnDirty = true;
		}
	}

	function getCurrencyID() {
		return $this->_intCurrencyID;
	}
	function setCurrencyID($value) {
		if($this->_intCurrencyID !== $value) {
			$this->_intCurrencyID = $value;
			$this->_blnDirty = true;

		}
	}

	function getNotes() {
		return $this->_txtNotes;
	}
	function setNotes($value) {
		if($this->_txtNotes !== $value) {
			$this->_txtNotes = $value;
			$this->_blnDirty = true;
		}
	}
}

include_once("Product.class.php");

class ProgramCommon extends ProgramCommonBase {
	private $_objTopLevelProduct;
	private $_objTopLevelFolder;
	private $_objCustomer;
	private $_objPlatform;
	private $_objProgramType;
	private $_objProgramSubType;
	private $_objCurrency;
	private $_objProgramProductionFacilityArray;
	private $_objHours;
	private $_objHoursArray;

	private $_intFolderStructureID;

	function __construct($intProgramCommonID=null) {
		parent::__construct($intProgramCommonID);

		$arrTrackFields = array(intCOLUMN_ID_MODEL_YEAR, intCOLUMN_ID_LAST_MODEL_YEAR);
		$this->setTrackFields($arrTrackFields);
	}

	function validate() {
		$strError = "";

		if(!$this->getCustomerID() && !$this->getCustomer()->getOrganizationName()) {
			$strError .= "Customer must be specified.<br />";
		}
		if(!$this->getPlatformID() && !$this->getPlatform()->getPlatform()) { // Set existing platform, or create new
			$strError .= "Platform must be specified.<br />";
		}
		if(!$this->getProgramTypeID()) {
			$strError .= "Type must be specified.<br />";
		}
		if(!$this->getProgramSubTypeID() && !$this->getProgramSubType()->getProgramSubType()) {
			$strError .= "Sub Type must be specified.<br />";
		}
		if(!$this->getModelYear()) {
			$strError .= "Model Year must be specified.<br />";
		} elseif(!is_numeric($this->getModelYear()) || $this->getModelYear() < 2000 || $this->getModelYear() > 3000) { // If this application lasts longer than the year 3000, I apologize, though I highly doubt it will :)
			$strError .= "Model Year has an invalid format.<br />";
		}
		if(!$this->getLastModelYear()) {
			$strError .= "Final Model Year must be specified.<br />";
		} elseif($this->getLastModelYear() && (!is_numeric($this->getLastModelYear()) || $this->getLastModelYear() < 2000 || $this->getLastModelYear() > 3000)) { // If this application lasts longer than the year 3000, I apologize, though I highly doubt it will :)
			$strError .= "Final Model Year has an invalid format.<br />";
		}

		if($strError) {
			throw new Exception($strError);
		}
	}

	function getTopLevelProduct($blnLoad=false){
		if(!$this->_objTopLevelProduct) {
			$this->_objTopLevelProduct = new Product();
		}
		if(!$this->_objTopLevelProduct->getProductID() && $this->getTopLevelProductID() && $blnLoad) {
			$this->_objTopLevelProduct->load($this->getTopLevelProductID());
		}
		return $this->_objTopLevelProduct;
	}
	function getTopLevelFolder($blnLoad=false){
		if(!$this->_objTopLevelFolder) {
			$this->_objTopLevelFolder = new Folder();
		}
		if(!$this->_objTopLevelFolder->getFolderID() && $this->getTopLevelFolderID() && $blnLoad) {
			$this->_objTopLevelFolder->load($this->getTopLevelFolderID());
		}
		return $this->_objTopLevelFolder;
	}
	function getCustomer($blnLoad=false){
		if(!$this->_objCustomer) {
			//$this->_objCustomer = new Customer();
		}
		if(!$this->_objCustomer->getCustomerID() && $this->getCustomerID() && $blnLoad) {
			$this->_objCustomer->load($this->getCustomerID());
		}
		return $this->_objCustomer;
	}
	function getPlatform($blnLoad=false){
		if(!$this->_objPlatform) {
			//$this->_objPlatform = new Platform();
		}
		//if(!$this->_objPlatform->getPlatformID() && $this->getPlatformID() && $blnLoad) {
			//$this->_objPlatform->load($this->getPlatformID());
		//}
		return $this->_objPlatform;

	}
	function getProgramType($blnLoad=false){
		if(!$this->_objProgramType) {
			$this->_objProgramType = new ProgramType();
		}
		if(!$this->_objProgramType->getProgramTypeID() && $this->getProgramTypeID() && $blnLoad) {
			$this->_objProgramType->load($this->getProgramTypeID());
		}
		return $this->_objProgramType;
	}
	function getProgramSubType(){
		if(!$this->_objProgramSubType) {
			$this->_objProgramSubType = new ProgramSubType();
		}
		return $this->_objProgramSubType;
	}
	function getProgramProductionFacilityArray(){
		if(!$this->_objProgramProductionFacilityArray) {
			$this->_objProgramProductionFacilityArray = new ProgramProductionFacilityArray();
		}
		return $this->_objProgramProductionFacilityArray;
	}
	function getHours(){
		if(!$this->_objHours) {
			$this->_objHours = new Hours();
		}
		return $this->_objHours;
	}
	function getHoursArray(){
		if(!$this->_objHoursArray) {
			$this->_objHoursArray = new HoursArray();
		}
		return $this->_objHoursArray;
	}

	function getFolderStructureID() {
		return $this->_intFolderStructureID;
	}
	function setFolderStructureID($value) {
		if($this->_intFolderStructureID !== $value) {
			$this->_intFolderStructureID = $value;
		}
	}

	function createFolderStructure($objFolderStructure){
		$objFolderArray = new FolderArray();
		$objFolderArray->loadAllChildrenRecursively($objFolderStructure->getFolderID());
		$objFolderArray->saveChildren($this->getTopLevelFolder()->getFolderID(), $this->getProgramCommonID()); // Saves folders and documents
	}

	function getID(){
		return $this->getProgramCommonID();
	}
	function setID($value){
		$this->setProgramCommonID($value);
	}
	function getName() {
		return /*$this->getPlatform()->getPlatform() . " " . */$this->getTopLevelProduct()->getProductName();
	}

	function getURL() {
		return "program.php?intProgramID=".$this->getTopLevelProductID();
	}

	function setModelYear($value) {
		if(!$this->isChangesCopy() && DataClass::getTrackChanges()) {
			$this->getChangesCopy()->setModelYear($value);
		} else {
			parent::setModelYear($value);
		}
	}
	function setLastModelYear($value) {
		if(!$this->isChangesCopy() && DataClass::getTrackChanges()) {
			$this->getChangesCopy()->setLastModelYear($value);
		} else {
			parent::setLastModelYear($value);
		}
	}
}
?>
