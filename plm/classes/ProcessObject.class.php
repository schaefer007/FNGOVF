<?php

interface ProcessObject {
	public function getProcessInstance($blnLoad);
	public function setCustomConditions($objProcessInstance, $objProcessTransitionArray);
	public function evaluateProcessTransition($objProcessTransitionInstance);
	public function loadForWorkflow($intID);
	public function validateWorkflowAction($intProcessTransitionID, $intRoleID);
	public function getBody($objProcessInstance, $objProcessTransitionInstance);
	public function getProcessObjectClass();
	public function setProcessVarsFromRow($arrRow);
	public function getID();
	public function getProcessName(); // Returns human readable name such as "K2XX Pedal Assembly - EC001"
	public function getURL();
	public function getUserArrayFromRoleIDs($arrRoleIDs, $objProcessInstance, $objProcessTransitionInstance);
	public function getRoleAndUserArrayFromRoleIDs($arrRoleIDs);
	public function afterProcessTransition($objProcessInstance, $objProcessTransitionInstance);
}

?>