<?php
	require_once("ArrayClass.class.php");

	class PermissionArray extends ArrayClass {
		function __construct(){
			parent::__construct("Permission");
		}

		function load($objSearch=null) {
			$strSQL = " SELECT * FROM dbPLM.tblPermission";
			if($objSearch && $objSearch->getSortBy())
				$strSQL .= " ORDER BY " . $objSearch->getSortBy() . " ";
			if($objSearch && $objSearch->getSortOrder())
				$strSQL .= $objSearch->getSortOrder();
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				$this->_arrObjects[$arrRow["intPermissionID"]] = new Permission();
				$this->_arrObjects[$arrRow["intPermissionID"]]->setVarsFromRow($arrRow);
			}
		}

		function loadByUserID($intUserID) {
			if(!$intUserID)
				return;

			$strSQL = " SELECT tblRole.*, tblPermission.*
				FROM dbPLM.tblPermission
				INNER JOIN dbPLM.tblRolePermission
					ON tblRolePermission.intPermissionID = tblPermission.intPermissionID
				INNER JOIN dbPLM.tblUserRoleXR
					ON tblUserRoleXR.intRoleID = tblRolePermission.intRoleID
				INNER JOIN dbPLM.tblRole
					ON tblRole.intRoleID = tblUserRoleXR.intRoleID
				WHERE intUserID = ".self::getDB()->sanitize($intUserID);
			//echo $strSQL . "<br />";
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				if(!isset($this->_arrObjects[$arrRow["intSecurityItemID"]."_".$arrRow["intOperationID"]])) {
					$this->_arrObjects[$arrRow["intSecurityItemID"]."_".$arrRow["intOperationID"]] = new Permission();
					$this->_arrObjects[$arrRow["intSecurityItemID"]."_".$arrRow["intOperationID"]]->setVarsFromRow($arrRow);
				}
				if($arrRow["blnPlantLevel"]) {
					$this->_arrObjects[$arrRow["intSecurityItemID"]."_".$arrRow["intOperationID"]]->setPlantLevel(1);
				} else {
					$this->_arrObjects[$arrRow["intSecurityItemID"]."_".$arrRow["intOperationID"]]->setCorporateLevel(1);
				}
			}
		}
	}

	require_once("DataClass.class.php");

	class PermissionBase extends DataClass {
		protected $_intPermissionID;
		protected $_intSecurityItemID;
		protected $_intOperationID;
		protected $_txtDescription;

		function __construct($intPermissionID=null) {
			$this->DataClass();
			if($intPermissionID) {
				$this->load($intPermissionID);
			}
		}

		protected function insert() {
			base::write_log("Permission created","S");
			$strSQL = "INSERT INTO dbPLM.tblPermission SET ";
			$strConnector = "";
			$strSQL .= $strConnector . "intPermissionID = ".$this->getDB()->sanitize(self::getPermissionID());
			$strConnector = ",";
			if(isset($this->_intSecurityItemID)) {
				$strSQL .= $strConnector . "intSecurityItemID = ".$this->getDB()->sanitize(self::getSecurityItemID());
				$strConnector = ",";
			}
			if(isset($this->_intOperationID)) {
				$strSQL .= $strConnector . "intOperationID = ".$this->getDB()->sanitize(self::getOperationID());
				$strConnector = ",";
			}
			if(isset($this->_txtDescription)) {
				$strSQL .= $strConnector . "txtDescription = ".$this->getDB()->sanitize(self::getDescription());
				$strConnector = ",";
			}
			//echo $strSQL;
			$this->getDB()->query($strSQL);
			$this->setPermissionID($this->getDB()->insert_id());
			return $this->getPermissionID();
		}

		protected function update() {
			base::write_log("Permission Update","S");
			$strSQL = "UPDATE dbPLM.tblPermission SET ";
			$strConnector = "";
			$strSQL .= $strConnector . "intPermissionID = ".$this->getDB()->sanitize(self::getPermissionID());
			$strConnector = ",";
			if(isset($this->_intSecurityItemID)) {
				$strSQL .= $strConnector . "intSecurityItemID = ".$this->getDB()->sanitize(self::getSecurityItemID());
				$strConnector = ",";
			}
			if(isset($this->_intOperationID)) {
				$strSQL .= $strConnector . "intOperationID = ".$this->getDB()->sanitize(self::getOperationID());
				$strConnector = ",";
			}
			if(isset($this->_txtDescription)) {
				$strSQL .= $strConnector . "txtDescription = ".$this->getDB()->sanitize(self::getDescription());
				$strConnector = ",";
			}
			$strSQL .= " WHERE intPermissionID = ".$this->getDB()->sanitize(self::getPermissionID())."";
			//echo $strSQL;
			return $this->getDB()->query($strSQL);
		}

		public function save() {
			if($this->_intPermissionID) {
				return $this->update();
			} else {
				return $this->insert();
			}
		}

		public function delete() {
			if($this->_intPermissionID) {
				base::write_log("Permission deleted","S");
				$strSQL = "DELETE FROM dbPLM.tblPermission
				WHERE intPermissionID = '$this->_intPermissionID'
				";
				return $this->getDB()->query($strSQL);
			}
		}

		public function load($intPermissionID) {
			if(!$intPermissionID) {
				return false;
			}

			$strSQL = "SELECT *
				FROM dbPLM.tblPermission
				WHERE intPermissionID = '$intPermissionID'
				LIMIT 1
			";
			$rsPermission = $this->getDB()->query($strSQL);
			$arrPermission = $this->getDB()->fetch_assoc($rsPermission);
			$this->setVarsFromRow($arrPermission);
		}

		function setVarsFromRow($arrPermission) {
			if(isset($arrPermission["intPermissionID"])) $this->_intPermissionID = $arrPermission["intPermissionID"];
			if(isset($arrPermission["intSecurityItemID"])) $this->_intSecurityItemID = $arrPermission["intSecurityItemID"];
			if(isset($arrPermission["intOperationID"])) $this->_intOperationID = $arrPermission["intOperationID"];
			if(isset($arrPermission["txtDescription"])) $this->_txtDescription = $arrPermission["txtDescription"];
		}

		function getPermissionID() {
			return $this->_intPermissionID;
		}
		function setPermissionID($value) {
			if($this->_intPermissionID !== $value) {
				$this->_intPermissionID = $value;
				$this->_blnDirty = true;
			}
		}

		function getSecurityItemID() {
			return $this->_intSecurityItemID;
		}
		function setSecurityItemID($value) {
			if($this->_intSecurityItemID !== $value) {



				$this->_intSecurityItemID = $value;
				$this->_blnDirty = true;
			}
		}

		function getOperationID() {
			return $this->_intOperationID;
		}
		function setOperationID($value) {
			if($this->_intOperationID !== $value) {
				$this->_intOperationID = $value;
				$this->_blnDirty = true;
			}
		}

		function getDescription() {
			return $this->_txtDescription;
		}
		function setDescription($value) {
			if($this->_txtDescription !== $value) {
				$this->_txtDescription = $value;
				$this->_blnDirty = true;
			}
		}
	}

	include_once("SecurityItem.class.php");
	include_once("Operation.class.php");
	include_once("RolePermission.class.php");
	include_once("Role.class.php");

	class Permission extends PermissionBase {
		private $_objSecurityItem;
		private $_objOperation;
		private $_objRolePermission;
		private $_objRoleArray;

		private $_blnPlantLevel; // True, if for a given user, they have this permission for a plant level role
		private $_blnCorporateLevel; // True, if for a given user, they have this permission for a non-plant level (corporate) role

		function __construct($intPermissionID=null) {
			parent::__construct($intPermissionID);
		}

		function insert() {
			base::write_log("Permission created","S");
			$strSQL = "INSERT INTO dbPLM.tblPermission SET ";
			$strConnector = "";
			$strSQL .= $strConnector . "intPermissionID = ".$this->getDB()->sanitize(self::getPermissionID());
			$strConnector = ",";
			if(isset($this->_intSecurityItemID)) {
				$strSQL .= $strConnector . "intSecurityItemID = ".$this->getDB()->sanitize(self::getSecurityItemID());
				$strConnector = ",";
			}
			if(isset($this->_intOperationID)) {
				$strSQL .= $strConnector . "intOperationID = ".$this->getDB()->sanitize(self::getOperationID());
				$strConnector = ",";
			}
			$strSQL .= " ON DUPLICATE KEY UPDATE intSecurityItemID = intSecurityItemID";
			//echo $strSQL;
			$this->getDB()->query($strSQL);
			$this->setPermissionID($this->getDB()->insert_id());
			return $this->getPermissionID();
		}

		function deleteBySecurityItemIDAndOperationID($intSecurityItemID, $intOperationID){
			if(!$intSecurityItemID || !$intOperationID)
				return false;

			$strSQL = "DELETE FROM dbPLM.tblPermission
				WHERE intSecurityItemID = ".self::getDB()->sanitize($intSecurityItemID)."
				AND intOperationID = ".self::getDB()->sanitize($intOperationID);
			return self::getDB()->query($strSQL);
		}

		function loadBySecurityItemIDAndOperationID($intSecurityItemID, $intOperationID){
			if(!$intSecurityItemID || !$intOperationID) {
				return false;
			}

			$strSQL = "SELECT *
				FROM dbPLM.tblPermission
				WHERE intSecurityItemID = ".$this->getDB()->sanitize($intSecurityItemID)."
				AND intOperationID = ".$this->getDB()->sanitize($intOperationID)."
				LIMIT 1
			";
			$rsPermission = $this->getDB()->query($strSQL);
			$arrPermission = $this->getDB()->fetch_assoc($rsPermission);
			$this->setVarsFromRow($arrPermission);
		}

		function loadForPermissionPage($intPermissionID){


			if(!$intPermissionID) {
				return false;
			}

			$strSQL = "SELECT tblSecurityItem.*, tblOperation.*, tblPermission.*
				FROM dbPLM.tblPermission
				INNER JOIN dbPLM.tblSecurityItem
					ON tblSecurityItem.intSecurityItemID = tblPermission.intSecurityItemID
				INNER JOIN dbPLM.tblOperation
					ON tblOperation.intOperationID = tblPermission.intOperationID
				WHERE intPermissionID = '$intPermissionID'
				LIMIT 1
			";
			$rsPermission = $this->getDB()->query($strSQL);
			$arrPermission = $this->getDB()->fetch_assoc($rsPermission);
			$this->setVarsFromRow($arrPermission);
			$this->getSecurityItem()->setVarsFromRow($arrPermission);
			$this->getOperation()->setVarsFromRow($arrPermission);
		}

		function getSecurityItem(){
			if(!isset($this->_objSecurityItem)) {
				$this->_objSecurityItem = new SecurityItem();
			}
			return $this->_objSecurityItem;
		}
		function getOperation(){
			if(!isset($this->_objOperation)) {
				$this->_objOperation = new Operation();
			}
			return $this->_objOperation;
		}
		function getRolePermission(){
			if(!isset($this->_objRolePermission)) {
				$this->_objRolePermission = new RolePermission();
			}
			return $this->_objRolePermission;
		}
		function getRoleArray(){
			if(!isset($this->_objRoleArray)) {
				$this->_objRoleArray = new RoleArray();
			}
			return $this->_objRoleArray;
		}

		function getPlantLevel() {
			return $this->_blnPlantLevel;
		}
		function setPlantLevel($value) {
			$this->_blnPlantLevel = $value;
		}
		function getCorporateLevel() {
			return $this->_blnCorporateLevel;
		}
		function setCorporateLevel($value) {
			$this->_blnCorporateLevel = $value;
		}
	}

	include_once("Search.class.php");

	class PermissionsSearch extends Search {
		private $_intRoleID;

		function __construct(){

		}

		function getRoleID(){
			return $this->_intRoleID;
		}
		function setRoleID($value){
			$this->_intRoleID = $value;
		}
	}
?>
