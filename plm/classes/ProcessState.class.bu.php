<?php
require_once("ArrayClass.class.php");

define("intPROCESS_STATE_WIDTH", 150);
define("intPROCESS_STATE_HEIGHT", 110);
define("intPROCESS_STATE_SPACE", 60);
define("intPROCESS_STATE_DIAGRAM_PADDING", 70);

class ProcessStateArray extends ArrayClass {
	protected $_intLargestXCord = 1;
	protected $_intLargestYCord = 1;

	function __construct(){
		parent::__construct("ProcessState");
	}

	function getDiagramWidth() {
		return $this->_intLargestXCord * intPROCESS_STATE_WIDTH + ($this->_intLargestXCord - 1) * intPROCESS_STATE_SPACE;
	}
	function getDiagramHeight() {
		return $this->_intLargestYCord * intPROCESS_STATE_HEIGHT + ($this->_intLargestYCord - 1) * intPROCESS_STATE_SPACE;
	}

	function load() {
		$strSQL = " SELECT * FROM dbSystem.tblProcessState";
		$rsResult = $this->getDB()->query($strSQL);
		while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
			$this->_arrObjects[$arrRow["intProcessStateID"]] = new ProcessState();
			$this->_arrObjects[$arrRow["intProcessStateID"]]->setVarsFromRow($arrRow);
		}
	}

	function loadByWFProcessID($intWFProcessID) {
		if(!$intWFProcessID) {
			return false;
		}

		$strSQL = " SELECT *
			FROM dbSystem.tblProcessState
			WHERE intWFProcessID = ".self::getDB()->sanitize($intWFProcessID);
		$rsResult = $this->getDB()->query($strSQL);
		while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
			$this->_arrObjects[$arrRow["intProcessStateID"]] = new ProcessState();
			$this->_arrObjects[$arrRow["intProcessStateID"]]->setVarsFromRow($arrRow);
		}
	}

	function loadForStateDiagram($intWFProcessID, $intProcessPathID) {
		if(!$intWFProcessID)
			return;

		include_once("ProcessTransition.class.php");
		$objProcessTransitionArray = new ProcessTransitionArray();

		$strSQL = "SELECT tblProcessPathTransition.*, tblProcessTransition.*, tblProcessState.*
			FROM dbSystem.tblProcessState
			LEFT JOIN dbSystem.tblProcessTransition
				ON tblProcessState.intProcessStateID = tblProcessTransition.intFromProcessStateID
				OR tblProcessState.intProcessStateID = tblProcessTransition.intToProcessStateID
			LEFT JOIN dbSystem.tblProcessPathTransition
				ON tblProcessPathTransition.intProcessTransitionID = tblProcessTransition.intProcessTransitionID
				AND tblProcessPathTransition.intProcessPathID = ".self::getDB()->sanitize($intProcessPathID)."
			WHERE intWFProcessID = ".self::getDB()->sanitize($intWFProcessID);
		$rsResult = $this->getDB()->query($strSQL);
		while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
			if(!isset($this->_arrObjects[$arrRow["intProcessStateID"]])) {
				$this->_arrObjects[$arrRow["intProcessStateID"]] = new ProcessState();
				$this->_arrObjects[$arrRow["intProcessStateID"]]->setVarsFromRow($arrRow);
				$this->_intLargestXCord = max($this->_intLargestXCord, $arrRow["intDiagramXCord"]);
				$this->_intLargestYCord = max($this->_intLargestYCord, $arrRow["intDiagramYCord"]);
			}
			if($arrRow["intProcessTransitionID"] && !$objProcessTransitionArray->objectIsSet($arrRow["intProcessTransitionID"])) {
				$objProcessTransitionArray->addFromRow($arrRow, "intProcessTransitionID");
				if($arrRow["intProcessPathTransitionID"] && !$objProcessTransitionArray->getObject($arrRow["intProcessTransitionID"])->getProcessPathTransition()->getProcessPathTransitionID()) {
					$objProcessTransitionArray->getObject($arrRow["intProcessTransitionID"])->getProcessPathTransition()->setVarsFromRow($arrRow);
				}
			}
		}

		return $objProcessTransitionArray;
	}

	function loadForLessonsLearnedManagement() {
		$objSearch = $_SESSION["objSearch"];
		$strSQL = LessonArray::getLessonListPageQuery($objSearch, "Process State");

		$rsResult = $this->getDB()->query($strSQL);
		while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
			$this->_arrObjects[$arrRow["intProcessStateID"]] = new ProcessState();
			$this->_arrObjects[$arrRow["intProcessStateID"]]->setVarsFromRow($arrRow);
		}

		$objSearch = $_SESSION["objSearch"];
		$strSearchValue = $objSearch->getLessonProcessStateID();
		if($strSearchValue && !isset($this->_arrObjects[$strSearchValue])) { // Search record not in options, add search record
			$this->createNew($strSearchValue)->load($strSearchValue);
		}
	}

	function loadForPurchaseOrderManagement() {
		$objSearch = $_SESSION["objSearch"];
		$strSQL = PurchaseOrderArray::getPurchaseOrderListPageQuery("Process State");

		$rsResult = $this->getDB()->query($strSQL);
		while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
			$this->_arrObjects[$arrRow["intProcessStateID"]] = new ProcessState();
			$this->_arrObjects[$arrRow["intProcessStateID"]]->setVarsFromRow($arrRow);
		}

		$objSearch = $_SESSION["objSearch"];
		$strSearchValue = $objSearch->getPOProcessStateID();
		if($strSearchValue && !isset($this->_arrObjects[$strSearchValue])) { // Search record not in options, add search record
			$this->createNew($strSearchValue)->load($strSearchValue);
		}
	}
}

require_once("DataClass.class.php");

class ProcessStateBase extends DataClass {
	protected $_intProcessStateID;
	protected $_intWFProcessID;
	protected $_strProcessStateName;
	protected $_strDescription;
	protected $_blnFinalState;
	protected $_blnCustomConditions;
	protected $_blnShowOnlyCurrentUser;
	protected $_intDiagramXCord;
	protected $_intDiagramYCord;
	protected $_intDisplayOrder;

	function __construct($intProcessStateID=null) {
		$this->DataClass();
		if($intProcessStateID) {
			$this->load($intProcessStateID);
		}
	}

	protected function insert() {
		$strSQL = "INSERT INTO dbSystem.tblProcessState SET ";
		$strConnector = "";
		$strSQL .= $strConnector . "intProcessStateID = ".$this->getDB()->sanitize(self::getProcessStateID());
		$strConnector = ",";
		if(isset($this->_intWFProcessID)) {
			$strSQL .= $strConnector . "intWFProcessID = ".$this->getDB()->sanitize(self::getWFProcessID());
			$strConnector = ",";
		}
		if(isset($this->_strProcessStateName)) {
			$strSQL .= $strConnector . "strProcessStateName = ".$this->getDB()->sanitize(self::getProcessStateName());
			$strConnector = ",";
		}
		if(isset($this->_strDescription)) {
			$strSQL .= $strConnector . "strDescription = ".$this->getDB()->sanitize(self::getDescription());
			$strConnector = ",";
		}
		if(isset($this->_blnFinalState)) {
			$strSQL .= $strConnector . "blnFinalState = ".$this->getDB()->sanitize(self::getFinalState());
			$strConnector = ",";
		}
		if(isset($this->_blnCustomConditions)) {
			$strSQL .= $strConnector . "blnCustomConditions = ".$this->getDB()->sanitize(self::getCustomConditions());
			$strConnector = ",";
		}
		if(isset($this->_blnShowOnlyCurrentUser)) {
			$strSQL .= $strConnector . "blnShowOnlyCurrentUser = ".$this->getDB()->sanitize(self::getShowOnlyCurrentUser());
			$strConnector = ",";
		}
		if(isset($this->_intDiagramXCord)) {
			$strSQL .= $strConnector . "intDiagramXCord = ".$this->getDB()->sanitize(self::getDiagramXCord());
			$strConnector = ",";
		}
		if(isset($this->_intDiagramYCord)) {
			$strSQL .= $strConnector . "intDiagramYCord = ".$this->getDB()->sanitize(self::getDiagramYCord());
			$strConnector = ",";
		}
		if(isset($this->_intDisplayOrder)) {
			$strSQL .= $strConnector . "intDisplayOrder = ".$this->getDB()->sanitize(self::getDisplayOrder());
			$strConnector = ",";
		}
		//echo $strSQL;
		$this->getDB()->query($strSQL);
		$this->setProcessStateID($this->getDB()->insert_id());
		return $this->getProcessStateID();
	}

	protected function update() {

		$strSQL = "UPDATE dbSystem.tblProcessState SET ";
		$strConnector = "";
		$strSQL .= $strConnector . "intProcessStateID = ".$this->getDB()->sanitize(self::getProcessStateID());
		$strConnector = ",";
		if(isset($this->_intWFProcessID)) {
			$strSQL .= $strConnector . "intWFProcessID = ".$this->getDB()->sanitize(self::getWFProcessID());
			$strConnector = ",";
		}
		if(isset($this->_strProcessStateName)) {
			$strSQL .= $strConnector . "strProcessStateName = ".$this->getDB()->sanitize(self::getProcessStateName());
			$strConnector = ",";
		}
		if(isset($this->_strDescription)) {
			$strSQL .= $strConnector . "strDescription = ".$this->getDB()->sanitize(self::getDescription());
			$strConnector = ",";
		}
		if(isset($this->_blnFinalState)) {
			$strSQL .= $strConnector . "blnFinalState = ".$this->getDB()->sanitize(self::getFinalState());
			$strConnector = ",";
		}
		if(isset($this->_blnCustomConditions)) {
			$strSQL .= $strConnector . "blnCustomConditions = ".$this->getDB()->sanitize(self::getCustomConditions());
			$strConnector = ",";
		}
		if(isset($this->_blnShowOnlyCurrentUser)) {
			$strSQL .= $strConnector . "blnShowOnlyCurrentUser = ".$this->getDB()->sanitize(self::getShowOnlyCurrentUser());
			$strConnector = ",";
		}
		if(isset($this->_intDiagramXCord)) {
			$strSQL .= $strConnector . "intDiagramXCord = ".$this->getDB()->sanitize(self::getDiagramXCord());
			$strConnector = ",";
		}
		if(isset($this->_intDiagramYCord)) {
			$strSQL .= $strConnector . "intDiagramYCord = ".$this->getDB()->sanitize(self::getDiagramYCord());
			$strConnector = ",";

		}
		if(isset($this->_intDisplayOrder)) {
			$strSQL .= $strConnector . "intDisplayOrder = ".$this->getDB()->sanitize(self::getDisplayOrder());
			$strConnector = ",";
		}
		$strSQL .= " WHERE intProcessStateID = ".$this->getDB()->sanitize(self::getProcessStateID())."";
		//echo $strSQL;
		return $this->getDB()->query($strSQL);

	}

	public function save() {
		if($this->_intProcessStateID) {
			return self::update();
		} else {
			return self::insert();
		}
	}

	public function delete() {
		if($this->_intProcessStateID) {
			$strSQL = "DELETE FROM dbSystem.tblProcessState
				WHERE intProcessStateID = '$this->_intProcessStateID'
				";
			return $this->getDB()->query($strSQL);
		}
	}

	public function load($intProcessStateID) {
		if(!$intProcessStateID) {
			return false;
		}

		$strSQL = "SELECT *
			FROM dbSystem.tblProcessState
			WHERE intProcessStateID = ".self::getDB()->sanitize($intProcessStateID)."
			LIMIT 1
		";
		$rsResult = $this->getDB()->query($strSQL);
		$arrRow = $this->getDB()->fetch_assoc($rsResult);
		$this->setVarsFromRow($arrRow);
	}

	function setVarsFromRow($arrRow) {
		if(isset($arrRow["intProcessStateID"])) $this->_intProcessStateID = $arrRow["intProcessStateID"];
		if(isset($arrRow["intWFProcessID"])) $this->_intWFProcessID = $arrRow["intWFProcessID"];
		if(isset($arrRow["strProcessStateName"])) $this->_strProcessStateName = $arrRow["strProcessStateName"];
		if(isset($arrRow["strDescription"])) $this->_strDescription = $arrRow["strDescription"];
		if(isset($arrRow["blnFinalState"])) $this->_blnFinalState = $arrRow["blnFinalState"];
		if(isset($arrRow["blnCustomConditions"])) $this->_blnCustomConditions = $arrRow["blnCustomConditions"];
		if(isset($arrRow["blnShowOnlyCurrentUser"])) $this->_blnShowOnlyCurrentUser = $arrRow["blnShowOnlyCurrentUser"];
		if(isset($arrRow["intDiagramXCord"])) $this->_intDiagramXCord = $arrRow["intDiagramXCord"];
		if(isset($arrRow["intDiagramYCord"])) $this->_intDiagramYCord = $arrRow["intDiagramYCord"];
		if(isset($arrRow["intDisplayOrder"])) $this->_intDisplayOrder = $arrRow["intDisplayOrder"];
	}

	function getProcessStateID() {
		return $this->_intProcessStateID;
	}
	function setProcessStateID($value) {
		if($this->_intProcessStateID !== $value) {
			$this->_intProcessStateID = $value;
			$this->_blnDirty = true;
		}
	}

	function getWFProcessID() {
		return $this->_intWFProcessID;
	}
	function setWFProcessID($value) {
		if($this->_intWFProcessID !== $value) {
			$this->_intWFProcessID = $value;
			$this->_blnDirty = true;
		}
	}

	function getProcessStateName() {
		return $this->_strProcessStateName;
	}
	function setProcessStateName($value) {
		if($this->_strProcessStateName !== $value) {
			$this->_strProcessStateName = $value;
			$this->_blnDirty = true;
		}
	}

	function getDescription() {
		return $this->_strDescription;
	}
	function setDescription($value) {
		if($this->_strDescription !== $value) {
			$this->_strDescription = $value;
			$this->_blnDirty = true;
		}
	}

	function getFinalState() {
		return $this->_blnFinalState;
	}
	function setFinalState($value) {
		if($this->_blnFinalState !== $value) {
			$this->_blnFinalState = $value;
			$this->_blnDirty = true;
		}
	}

	function getCustomConditions() {
		return $this->_blnCustomConditions;
	}
	function setCustomConditions($value) {
		if($this->_blnCustomConditions !== $value) {
			$this->_blnCustomConditions = $value;
			$this->_blnDirty = true;
		}
	}

	function getShowOnlyCurrentUser() {
		return $this->_blnShowOnlyCurrentUser;
	}
	function setShowOnlyCurrentUser($value) {
		if($this->_blnShowOnlyCurrentUser !== $value) {
			$this->_blnShowOnlyCurrentUser = $value;
			$this->_blnDirty = true;
		}
	}

	function getDiagramXCord() {
		return $this->_intDiagramXCord;
	}
	function setDiagramXCord($value) {
		if($this->_intDiagramXCord !== $value) {
			$this->_intDiagramXCord = $value;
			$this->_blnDirty = true;
		}
	}

	function getDiagramYCord() {
		return $this->_intDiagramYCord;
	}
	function setDiagramYCord($value) {
		if($this->_intDiagramYCord !== $value) {
			$this->_intDiagramYCord = $value;
			$this->_blnDirty = true;
		}
	}

	function getDisplayOrder() {
		return $this->_intDisplayOrder;
	}
	function setDisplayOrder($value) {
		if($this->_intDisplayOrder !== $value) {
			$this->_intDisplayOrder = $value;
			$this->_blnDirty = true;
		}
	}
}

include_once("ProcessTransition.class.php");
include_once("Notification.class.php");

class ProcessState extends ProcessStateBase {
	protected $_objProcessTransitionArray;

	function __construct($intProcessStateID=null) {
		parent::__construct($intProcessStateID);
	}

	function getID() {
		return $this->getProcessStateID();
	}
	function getName() {
		return $this->getProcessStateName();
	}

	function emailForNextProcessTransitions($objProcessObject, $objProcessInstance, $objProcessTransitionInstance) {
		if(!$this->getProcessStateID())
			return;

		// Get users to notify
		$this->getProcessTransitionArray()->loadByProcessStateID($this->getProcessStateID());
		$this->getProcessTransitionArray()->loadConditions();
		$objRoleArray = $this->getProcessTransitionArray()->getRoleArray();
		$objUserArray = $objProcessObject->getUserArrayFromRoleIDs($objRoleArray->getAllFields("getRoleID", true), $objProcessInstance, $objProcessTransitionInstance);

		$objProcessTransitionArray = new ProcessTransitionArray();

		$strSubject = "Action Required: " . $objProcessObject->getProcessName();
		$txtBody = "The <i><b>" . $objProcessObject->getProcessName() . "</b></i> Workflow Process has progressed.<br />";
		$txtBody .= "The following person(s) clicked <i><b>" . $objProcessTransitionInstance->getProcessTransition()->getTransitionButton() . "</b></i>:<br />";
		if($objProcessTransitionInstance->getConditionInstanceArray()->getArray()) {
			foreach($objProcessTransitionInstance->getConditionInstanceArray()->getArray() as $objConditionInstance) {
				$strEmail = "";
				if($objConditionInstance->getUser()->getEmail()) {
					$strEmail = " (<a href=\"mailto:".$objConditionInstance->getUser()->getEmail()."\">".$objConditionInstance->getUser()->getEmail()."</a>)";
				}
				$txtBody .= "&nbsp;&nbsp;&nbsp;<i><b>" . $objConditionInstance->getUser()->getDisplayName() . "</b>$strEmail</i> at <i><b>" . formatDate($objConditionInstance->getCompletedDate()) . "</b></i>";
				if($objConditionInstance->getComment())
					$txtBody .= " with comment <i><b>".htmlentities($objConditionInstance->getComment())."</b></i>";
				$txtBody .= "<br />";
			}
		} else {
			$txtBody .= "Nobody<br />";
		}
		$txtBody .= "causing the process state to change from <i><b>" . $objProcessTransitionInstance->getProcessTransition()->getFromProcessState()->getProcessStateName() . "</b></i> to <i><b>" . $objProcessTransitionInstance->getProcessTransition()->getToProcessState()->getProcessStateName() . "</b></i><br />";
		$txtBody .= "<br />";
		$txtBody .= "Now you are able to take one of the following action(s):<br />";

		$txtBody .= "<i><b>";
		if($this->getProcessTransitionArray()->getArray()) {
			$strConnector = "";
			foreach($this->getProcessTransitionArray()->getArray() as $objProcessTransition) {
				$txtBody .= $strConnector . $objProcessTransition->getTransitionButton();
				$strConnector = ", ";
			}
		}
		$txtBody .= "</b></i><br />";
		$txtBody .= "<br />";

		$txtBody .= "These people also have an action to take during this next step in the workflow.<br />";
		if($objProcessInstance->getCurrentProcessState(true)->getCustomConditions()) {
			$objProcessObject->setCustomConditions($objProcessInstance, $this->getProcessTransitionArray());
			$objRoleArray = $objProcessInstance->getRoleArray();
		} else {
			$objRoleArray = $objProcessObject->getRoleAndUserArrayFromRoleIDs($objRoleArray->getAllFields("getRoleID", true));
		}
		$txtBody .= "<b><i>";
		if($objRoleArray->getArray()) {
			foreach($objRoleArray->getArray() as $objRole) {
				if($objRole->getUserArray()->getArray()) {
					$txtBody .= $objRole->getRoleName() . ": ";
					$strConnector = "";
					foreach($objRole->getUserArray()->getArray() as $objUser) {
						$txtBody .= $strConnector . $objUser->getDisplayName();
						$strConnector = ", ";
					}
					$txtBody .= "<br />";
				}
			}
		}
		$txtBody .= "</b></i>";

		$txtProcessObjectBody = $objProcessObject->getBody($objProcessInstance, $objProcessTransitionInstance);
		if($txtProcessObjectBody) {
			$txtBody .= "------------------------------------------------<br />";
			$txtBody .= "<br />";
			$txtBody .= "<b><u>" . $objProcessObject->getProcessName() . " Workflow Process Details</u></b><br />";
			$txtBody .= "<br />";
			$txtBody .= $txtProcessObjectBody;
		}

		$objNotification = new Notification();
		$objNotification->setSubject($strSubject);
		$objNotification->setBody($txtBody);
		$objNotification->setUserArray($objUserArray);
		$objNotification->sendNotification();
	}

	function emailRoles($objProcessObject, $intProgramCommonID) {
		if(!$this->getProcessStateID() || !$intProgramCommonID)
			return;

		// Get users to notify
		$this->getProcessTransitionArray()->loadByProcessStateID($this->getProcessStateID());
		$this->getProcessTransitionArray()->loadConditions();
		$objRoleArray = $this->getProcessTransitionArray()->getRoleArray();
		$arrRoleIDs = $objRoleArray->getAllFields("getRoleID", true);

		include_once("User.class.php");
		$objUserArray = new UserArray();
		$objUserArray->loadByProgramCommonIDAndRoleIDs($intProgramCommonID, $arrRoleIDs);

		$strSubject = "Action Required: " . $objProcessObject->getProcessName();
		$txtBody = $objProcessObject->getBody();

		$objNotification = new Notification();
		$objNotification->setSubject($strSubject);
		$objNotification->setBody($txtBody);
		$objNotification->setUserArray($objUserArray);
		$objNotification->sendNotification();
	}

	function validate() {
		$arrErrors = array();

		if(strlen($this->getProcessStateName()) < 2) {
			$arrErrors[] = "State Name must be at least 2 characters long.";
		}

		return $arrErrors;
	}

	function getProcessTransitionArray() {
		if(!$this->_objProcessTransitionArray) {
			$this->_objProcessTransitionArray = new ProcessTransitionArray();
		}
		return $this->_objProcessTransitionArray;
	}

	function getLeftX() {
		return intPROCESS_STATE_DIAGRAM_PADDING + ($this->getDiagramXCord() - 1) * intPROCESS_STATE_WIDTH + ($this->getDiagramXCord() - 1) * intPROCESS_STATE_SPACE;
	}
	function getRightX() {
		return intPROCESS_STATE_DIAGRAM_PADDING + $this->getDiagramXCord() * intPROCESS_STATE_WIDTH + ($this->getDiagramXCord() - 1) * intPROCESS_STATE_SPACE;
	}
	function getTopY() {
		return intPROCESS_STATE_DIAGRAM_PADDING + ($this->getDiagramYCord() - 1) * intPROCESS_STATE_HEIGHT + ($this->getDiagramYCord() - 1) * intPROCESS_STATE_SPACE;
	}
	function getBottomY() {
		return intPROCESS_STATE_DIAGRAM_PADDING + $this->getDiagramYCord() * intPROCESS_STATE_HEIGHT + ($this->getDiagramYCord() - 1) * intPROCESS_STATE_SPACE;
	}
}
?>
