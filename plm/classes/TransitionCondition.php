<?php
require_once("ArrayClass.class.php");

class TransitionConditionArray extends ArrayClass {
//    function __construct(){
//        parent::__construct("TransitionCondition");
//    }

    function __construct($intTransitionID=null) {
        parent::__construct("TransitionCondition");
        if($intTransitionID) {
            $this->load($intTransitionID);
        }
    }

    function load($intTransitionID=null) {
        $strSQL = "SELECT *
				FROM dbSystem.tblTransitionCondition
				WHERE intTransitionID = ".self::getDB()->sanitize($intTransitionID);
        $rsResult = $this->getDB()->query($strSQL);
        while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
            $this->_arrObjects[$arrRow["intTransitionConditionID"]] = new TransitionCondition();
            $this->_arrObjects[$arrRow["intTransitionConditionID"]]->setVarsFromRow($arrRow);
        }
    }

    function evaluateTransitionConditions($vendorform) {
        if(count($this->getArray()) < 1) {
            return true;
        }
        foreach($this->getArray() as $objTransitionCondition) {
            if(trim($objTransitionCondition->getFormColumn()) != "") {
                // TODO: what do we do if the $_REQUEST variable is not set?
                if(isset($vendorform[$objTransitionCondition->getFormColumn()])) {
                    $formValue = trim($vendorform[$objTransitionCondition->getFormColumn()]);
                } else {
                    return false;
                }
            } else {
                continue;
            }
            if(trim($objTransitionCondition->getFormValueColumn()) != "") {
                $value = trim($objTransitionCondition->getFormValueColumn());
            } else if(trim($objTransitionCondition->getValue()) != "") {
                $value = trim($objTransitionCondition->getValue());
            } else {
                continue;
            }
            switch($objTransitionCondition->getCondition()) {
                case "*EQ":
                    $blnSwitchVal =  ($formValue == $value);
                    break;
                case "*NE":
                    $blnSwitchVal =  ($formValue != $value);
                    break;
                case "*GT":
                    $blnSwitchVal =  ($formValue > $value);
                    break;
                case "*GE":
                    $blnSwitchVal =  ($formValue >= $value);
                    break;
                case "*LT":
                    $blnSwitchVal =  ($formValue < $value);
                    break;
                case "*LE":
                    $blnSwitchVal =  ($formValue <= $value);
                    break;
                default:
                    $blnSwitchVal = true;
                    break;
            }
            if(!$blnSwitchVal) {
                return false;
            }
        }
        return true;
    }
}

require_once("DataClass.class.php");

class TransitionConditionBase extends DataClass {
    protected $_intTransitionConditionID;
    protected $_intTransitionID;
    protected $_strFormColumn;
    protected $_strCondition;
    protected $_strFormValueColumn;
    protected $_strValue;

    function __construct($intTransitionConditionID=null) {
        $this->DataClass();
        if($intTransitionConditionID) {
            $this->load($intTransitionConditionID);
        }
    }

    protected function insert() {
        base::write_log("Transition Condition created","S");
        $strSQL = "INSERT INTO dbSystem.tblTransitionCondition SET ";
        $strConnector = "";
        $strSQL .= $strConnector . "intTransitionConditionID = ".$this->getDB()->sanitize(self::getTransitionConditionID());
        $strConnector = ",";
        if(isset($this->_intTransitionID)) {
            $strSQL .= $strConnector . "intTransitionID = ".$this->getDB()->sanitize(self::getTransitionID());
            $strConnector = ",";
        }
        if(isset($this->_strFormColumn)) {
            $strSQL .= $strConnector . "strFormColumn = ".$this->getDB()->sanitize(self::getFormColumn());
            $strConnector = ",";
        }
        if(isset($this->_strCondition)) {
            $strSQL .= $strConnector . "strCondition = ".$this->getDB()->sanitize(self::getCondition());
            $strConnector = ",";
        }
        if(isset($this->_strFormValueColumn)) {
            $strSQL .= $strConnector . "strFormValueColumn = ".$this->getDB()->sanitize(self::getFormValueColumn());
            $strConnector = ",";
        }
        if(isset($this->_strValue)) {
            $strSQL .= $strConnector . "strValue = ".$this->getDB()->sanitize(self::getValue());
            $strConnector = ",";
        }
        echo $strSQL;
        //todo undo next line comment
//        $this->getDB()->query($strSQL);

//        $this->setTransitionConditionID($this->getDB()->insert_id());
        return $this->getTransitionConditionID();
    }

    // todo do update function [currently transitioninstance table]
    protected function update() {
        base::write_log("Transition Condition Update","S");
        $strSQL = "UPDATE dbSystem.tblProcessTransition SET ";
        $strConnector = "";
        $strSQL .= $strConnector . "intProcessTransitionID = ".$this->getDB()->sanitize(self::getProcessTransitionID());
        $strConnector = ",";
        if(isset($this->_intTransitionID)) {
            $strSQL .= $strConnector . "intTransitionID = ".$this->getDB()->sanitize(self::getTransitionID());
            $strConnector = ",";
        }
        if(isset($this->_strFormColumn)) {
            $strSQL .= $strConnector . "strFormColumn = ".$this->getDB()->sanitize(self::getFormColumn());
            $strConnector = ",";
        }
        if(isset($this->_strCondition)) {
            $strSQL .= $strConnector . "strCondition = ".$this->getDB()->sanitize(self::getCondition());
            $strConnector = ",";
        }
        if(isset($this->_strFormValueColumn)) {
            $strSQL .= $strConnector . "strFormValueColumn = ".$this->getDB()->sanitize(self::getFormValueColumn());
            $strConnector = ",";
        }
        if(isset($this->_strValue)) {
            $strSQL .= $strConnector . "strValue = ".$this->getDB()->sanitize(self::getValue());
            $strConnector = ",";
        }
        $strSQL .= " WHERE intProcessTransitionID = ".$this->getDB()->sanitize(self::getProcessTransitionID())."";
        echo $strSQL;
        //todo undo next line comment
//        return $this->getDB()->query($strSQL);
    }

    public function save() {
        if($this->_intTransitionConditionID) {
            return self::update();
        } else {
            return self::insert();
        }
    }

    public function delete() {
        if($this->_intTransitionConditionID) {
            base::write_log("Transition Condition Delete","S");
            $strSQL = "DELETE FROM dbSystem.tblTransitionCondition
				WHERE intTransitionConditionID = '$this->_intTransitionConditionID'
				";
            return $this->getDB()->query($strSQL);
        }
    }

    public function load($intTransitionConditionID) {
        if(!$intTransitionConditionID) {
            return false;
        }

        $strSQL = "SELECT *
				FROM dbSystem.tblTransitionCondition
				WHERE intTransitionConditionID = ".self::getDB()->sanitize($intTransitionConditionID)."
				LIMIT 1
			";
        $rsResult = $this->getDB()->query($strSQL);
        $arrRow = $this->getDB()->fetch_assoc($rsResult);
        $this->setVarsFromRow($arrRow);
    }

    function setVarsFromRow($arrRow) {
        if(isset($arrRow["intTransitionConditionID"])) $this->_intTransitionConditionID = $arrRow["intTransitionConditionID"];
        if(isset($arrRow["intTransitionID"])) $this->_intTransitionID = $arrRow["intTransitionID"];
        if(isset($arrRow["strFormColumn"])) $this->_strFormColumn = $arrRow["strFormColumn"];
        if(isset($arrRow["strCondition"])) $this->_strCondition = $arrRow["strCondition"];
        if(isset($arrRow["strFormValueColumn"])) $this->_strFormValueColumn= $arrRow["strFormValueColumn"];
        if(isset($arrRow["strValue"])) $this->_strValue = $arrRow["strValue"];
    }

    function getTransitionConditionID() {
        return $this->_intTransitionConditionID;
    }
    function setTransitionConditionID($value) {
        if($this->_intTransitionConditionID !== $value) {
            $this->_intTransitionConditionID = $value;
            $this->_blnDirty = true;
        }
    }

    function getTransitionID() {
        return $this->_intTransitionID;
    }
    function setTransitionID($value) {
        if($this->_intTransitionID !== $value) {
            $this->_intTransitionID = $value;
            $this->_blnDirty = true;
        }
    }
    function getFormColumn() {
        return $this->_strFormColumn;
    }
    function setFormColumn($value) {
        if($this->_strFormColumn !== $value) {
            $this->_strFormColumn = $value;
            $this->_blnDirty = true;
        }
    }
    function getCondition() {
        return $this->_strCondition;
    }
    function setCondition($value) {
        if($this->_strCondition !== $value) {
            $this->_strCondition = $value;
            $this->_blnDirty = true;
        }
    }
    function getFormValueColumn() {
        return $this->_strFormValueColumn;
    }
    function setFormValueColumn($value) {
        if($this->_strFormValueColumn !== $value) {
            $this->_strFormValueColumn = $value;
            $this->_blnDirty = true;
        }
    }
    function getValue() {
        return $this->_strValue;
    }
    function setValue($value) {
        if($this->_strValue !== $value) {
            $this->_strValue = $value;
            $this->_blnDirty = true;
        }
    }
}

class TransitionCondition extends TransitionConditionBase {
//    protected $_objFromProcessState;
//    protected $_objToProcessState;
//    protected $_objCondition;
//    protected $_objConditionArray;
//    protected $_objTransitionConditionInstance;
//    protected $_objProcessPathTransition;
//
//    function __construct($intTransitionConditionID=null) {
//        parent::__construct($intTransitionConditionID);
//    }
//
//    /*function loadConditions($intConditionID) {
//        if(!$intConditionID)
//            return;
//
//        $strSQL = "SELECT tblCondition.*
//            FROM dbSystem.tblConditionXR
//            INNER JOIN dbSystem.tblCondition
//                ON tblCondition.intConditionID = tblConditionXR.intChildConditionID
//            WHERE tblConditionXR.intParentConditionID = ".self::getDB()->sanitize($intConditionID);
//        $rsResult = $this->getDB()->query($strSQL);
//
//        while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
//            $this->getConditionArray()->addFromRow($arrRow, "intConditionID");
//            $this->getConditionArray()->getChildConditionArray()->addFromRow($arrRow, "")
//        }
//    }*/
//
//    function validate() {
//        $arrErrors = array();
//
//        if(strlen($this->getTransitionName()) < 2) {
//            $arrErrors[] = "Transition Name must be at least 2 characters long.";
//        }
//        if(strlen($this->getTransitionButton()) < 2) {
//            $arrErrors[] = "Transition Button must be at least 2 characters long.";
//        }
//        if(!$this->getFromProcessStateID()) {
//            $arrErrors[] = "From Process State must be specified.";
//        }
//        if(!$this->getToProcessStateID()) {
//            $arrErrors[] = "To Process State must be specified.";
//        }
//
//        return $arrErrors;
//    }
//
//    function getConditionText() {
//        if(!$this->getConditionArray()->getArray() || !$this->getConditionID())
//            return;
//
//        return $this->getConditionArray()->getObject($this->getConditionID())->output($this->getConditionArray());
//    }
//
//    function getID() {
//        return $this->getTransitionConditionID();
//    }
//
//    function getFromProcessState($blnLoad=false) {
//        include_once("ProcessState.class.php");
//        if(!isset($this->_objFromProcessState)) {
//            $this->_objFromProcessState = new ProcessState();
//        }
//        if($blnLoad && $this->getFromProcessStateID() && !$this->_objFromProcessState->getProcessStateID()) {
//            $this->_objFromProcessState->load($this->getFromProcessStateID());
//        }
//        return $this->_objFromProcessState;
//    }
//    function getToProcessState($blnLoad=false) {
//        include_once("ProcessState.class.php");
//        if(!isset($this->_objToProcessState)) {
//            $this->_objToProcessState = new ProcessState();
//        }
//        if($blnLoad && $this->getToProcessStateID() && !$this->_objToProcessState->getProcessStateID()) {
//            $this->_objToProcessState->load($this->getToProcessStateID());
//        }
//        return $this->_objToProcessState;
//    }
//    function getCondition() {
//        include_once("Condition.class.php");
//        if(!isset($this->_objCondition)) {
//            $this->_objCondition = new Condition();
//        }
//        return $this->_objCondition;
//    }
//    function getConditionArray() {
//        include_once("Condition.class.php");
//        if(!isset($this->_objConditionArray)) {
//            $this->_objConditionArray = new ConditionArray();
//        }
//        return $this->_objConditionArray;
//    }
//    function getTransitionConditionInstance() {
//        include_once("TransitionConditionInstance.class.php");
//        if(!isset($this->_objTransitionConditionInstance)) {
//            $this->_objTransitionConditionInstance = new TransitionConditionInstance();
//        }
//        return $this->_objTransitionConditionInstance;
//    }
}
?>
