<?php
require_once("ArrayClass.class.php");

class ProgramDataArray extends ArrayClass {
	function __construct(){
	}

	function load() {
		$strSQL = " SELECT * FROM tblProgramData";
		$rsResult = $this->getDB()->query($strSQL);
		while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
			$this->_arrObjects[$arrRow["intProgramDataID"]] = new ProgramData();
			$this->_arrObjects[$arrRow["intProgramDataID"]]->setVarsFromRow($arrRow);
		}
	}

}

require_once("DataClass.class.php");

class ProgramDataBase extends DataClass {
	protected $_intProgramDataID;
	protected $_intProgramID;
	protected $_blnHasConfigurations;

	function __construct($intProgramDataID=null) {
		$this->DataClass();
		if($intProgramDataID) {
			$this->load($intProgramDataID);
		}
	}

	protected function insert() {
		base::write_log("Program data created","S");
		$strSQL = "INSERT INTO tblProgramData SET ";
		$strConnector = "";
		$strSQL .= $strConnector . "intProgramDataID = ".$this->getDB()->sanitize(self::getProgramDataID());
		$strConnector = ",";
		if(isset($this->_intProgramID)) {
			$strSQL .= $strConnector . "intProgramID = ".$this->getDB()->sanitize(self::getProgramID());
			$strConnector = ",";
		}
		if(isset($this->_blnHasConfigurations)) {
			$strSQL .= $strConnector . "blnHasConfigurations = ".$this->getDB()->sanitize(self::getHasConfigurations());
			$strConnector = ",";
		}
		//echo $strSQL;
		$this->getDB()->query($strSQL);
		$this->setProgramDataID($this->getDB()->insert_id());
		return $this->getProgramDataID();
	}

	protected function update() {
		base::write_log("Program data Update","S");
		$strSQL = "UPDATE tblProgramData SET ";
		$strConnector = "";
		$strSQL .= $strConnector . "intProgramDataID = ".$this->getDB()->sanitize(self::getProgramDataID());
		$strConnector = ",";
		if(isset($this->_intProgramID)) {
			$strSQL .= $strConnector . "intProgramID = ".$this->getDB()->sanitize(self::getProgramID());
			$strConnector = ",";
		}
		if(isset($this->_blnHasConfigurations)) {
			$strSQL .= $strConnector . "blnHasConfigurations = ".$this->getDB()->sanitize(self::getHasConfigurations());
			$strConnector = ",";
		}
		$strSQL .= " WHERE intProgramDataID = ".$this->getDB()->sanitize(self::getProgramDataID())."";
		//echo $strSQL;
		return $this->getDB()->query($strSQL);
	}

	public function save() {
		if($this->_intProgramDataID) {
			return $this->update();
		} else {
			return $this->insert();
		}
	}

	public function delete() {
		if($this->_intProgramDataID) {
			$strSQL = "DELETE FROM tblProgramData
				WHERE intProgramDataID = '$this->_intProgramDataID'
				";
			return $this->getDB()->query($strSQL);
		}
	}

	public function load($intProgramDataID) {
		if(!$intProgramDataID) {
			return false;
		}

		$strSQL = "SELECT *
				FROM tblProgramData
				WHERE intProgramDataID = ".self::getDB()->sanitize($intProgramDataID)."
				LIMIT 1
			";
		$rsResult = $this->getDB()->query($strSQL);
		$arrRow = $this->getDB()->fetch_assoc($rsResult);
		$this->setVarsFromRow($arrRow);
	}

	function setVarsFromRow($arrRow) {
		if(isset($arrRow["intProgramDataID"])) $this->_intProgramDataID = $arrRow["intProgramDataID"];
		if(isset($arrRow["intProgramID"])) $this->_intProgramID = $arrRow["intProgramID"];
		if(isset($arrRow["blnHasConfigurations"])) $this->_blnHasConfigurations = $arrRow["blnHasConfigurations"];
	}

	function getProgramDataID() {
		return $this->_intProgramDataID;
	}
	function setProgramDataID($value) {
		if($this->_intProgramDataID !== $value) {
			$this->_intProgramDataID = $value;
			$this->_blnDirty = true;
		}
	}

	function getProgramID() {
		return $this->_intProgramID;
	}
	function setProgramID($value) {
		if($this->_intProgramID !== $value) {
			$this->_intProgramID = $value;
			$this->_blnDirty = true;
		}
	}

	function getHasConfigurations() {
		return $this->_blnHasConfigurations;
	}
	function setHasConfigurations($value) {
		if($this->_blnHasConfigurations !== $value) {
			$this->_blnHasConfigurations = $value;
			$this->_blnDirty = true;
		}
	}

}

class ProgramData extends ProgramDataBase {
	function __construct($intProgramDataID=null) {
		parent::__construct($intProgramDataID);
	}

	function loadByProgramID($intProgramID){
		if(!$intProgramID) {
			return false;
		}

		$strSQL = "SELECT *
				FROM tblProgramData
				WHERE intProgramID = ".self::getDB()->sanitize($intProgramID)."
				LIMIT 1
			";
		$rsResult = $this->getDB()->query($strSQL);
		$arrRow = $this->getDB()->fetch_assoc($rsResult);
		$this->setVarsFromRow($arrRow);
	}
}
?>
