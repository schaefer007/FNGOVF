<?php
require_once("ArrayClass.class.php");

class ProductECXRArray extends ArrayClass {
	function __construct(){
		parent::__construct("ProductECXR");
	}

	function load() {
		$strSQL = " SELECT * FROM dbEngineeringChange.tblProductECXR";
		$rsResult = $this->getDB()->query($strSQL);
		while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
			$this->_arrObjects[$arrRow["intProductECXRID"]] = new ProductECXR();
			$this->_arrObjects[$arrRow["intProductECXRID"]]->setVarsFromRow($arrRow);
		}
	}
}

require_once("DataClass.class.php");

class ProductECXRBase extends DataClass {
	protected $_intProductECXRID;
	protected $_intProductID;
	protected $_intEngineeringChangeID;

	function __construct($intProductECXRID=null) {
		$this->DataClass();
		if($intProductECXRID) {
			$this->load($intProductECXRID);
		}
	}

	protected function insert() {
		base::write_log("ProductECXR created","S");
		$strSQL = "INSERT INTO dbEngineeringChange.tblProductECXR SET ";
		$strConnector = "";
		$strSQL .= $strConnector . "intProductECXRID = ".$this->getDB()->sanitize(self::getProductECXRID());
		$strConnector = ",";
		if(isset($this->_intProductID)) {
			$strSQL .= $strConnector . "intProductID = ".$this->getDB()->sanitize(self::getProductID());
			$strConnector = ",";
		}
		if(isset($this->_intEngineeringChangeID)) {
			$strSQL .= $strConnector . "intEngineeringChangeID = ".$this->getDB()->sanitize(self::getEngineeringChangeID());
			$strConnector = ",";
		}
		//echo $strSQL;
		$this->getDB()->query($strSQL);
		$this->setProductECXRID($this->getDB()->insert_id());
		return $this->getProductECXRID();
	}

	protected function update() {
		base::write_log("ProductECXR Update","S");
		$strSQL = "UPDATE dbEngineeringChange.tblProductECXR SET ";
		$strConnector = "";
		$strSQL .= $strConnector . "intProductECXRID = ".$this->getDB()->sanitize(self::getProductECXRID());
		$strConnector = ",";
		if(isset($this->_intProductID)) {
			$strSQL .= $strConnector . "intProductID = ".$this->getDB()->sanitize(self::getProductID());
			$strConnector = ",";
		}
		if(isset($this->_intEngineeringChangeID)) {
			$strSQL .= $strConnector . "intEngineeringChangeID = ".$this->getDB()->sanitize(self::getEngineeringChangeID());
			$strConnector = ",";
		}
		$strSQL .= " WHERE intProductECXRID = ".$this->getDB()->sanitize(self::getProductECXRID())."";
		//echo $strSQL;
		return $this->getDB()->query($strSQL);
	}

	public function save() {
		if($this->_intProductECXRID) {
			return self::update();
		} else {
			return self::insert();
		}
	}

	public function delete() {
		if($this->_intProductECXRID) {
			base::write_log("ProductECXR delete","S");
			$strSQL = "DELETE FROM dbEngineeringChange.tblProductECXR
				WHERE intProductECXRID = '$this->_intProductECXRID'
				";
			return $this->getDB()->query($strSQL);
		}
	}

	public function load($intProductECXRID) {
		if(!$intProductECXRID) {
			return false;
		}

		$strSQL = "SELECT *
				FROM dbEngineeringChange.tblProductECXR
				WHERE intProductECXRID = ".self::getDB()->sanitize($intProductECXRID)."
				LIMIT 1
			";
		$rsResult = $this->getDB()->query($strSQL);
		$arrRow = $this->getDB()->fetch_assoc($rsResult);
		$this->setVarsFromRow($arrRow);
	}

	function setVarsFromRow($arrRow) {
		if(isset($arrRow["intProductECXRID"])) $this->_intProductECXRID = $arrRow["intProductECXRID"];
		if(isset($arrRow["intProductID"])) $this->_intProductID = $arrRow["intProductID"];
		if(isset($arrRow["intEngineeringChangeID"])) $this->_intEngineeringChangeID = $arrRow["intEngineeringChangeID"];
	}

	function getProductECXRID() {
		return $this->_intProductECXRID;
	}
	function setProductECXRID($value) {
		if($this->_intProductECXRID !== $value) {
			$this->_intProductECXRID = $value;
			$this->_blnDirty = true;
		}
	}

	function getProductID() {
		return $this->_intProductID;
	}
	function setProductID($value) {
		if($this->_intProductID !== $value) {
			$this->_intProductID = $value;
			$this->_blnDirty = true;
		}
	}

	function getEngineeringChangeID() {
		return $this->_intEngineeringChangeID;
	}
	function setEngineeringChangeID($value) {
		if($this->_intEngineeringChangeID !== $value) {
			$this->_intEngineeringChangeID = $value;
			$this->_blnDirty = true;
		}
	}

}

class ProductECXR extends ProductECXRBase {
	function __construct($intProductECXRID=null) {
		parent::__construct($intProductECXRID);
	}

	function loadByProductIDAndEngineeringChangeID($intProductID, $intEngineeringChangeID) {
		if(!$intProductID || !$intEngineeringChangeID) {
			return false;
		}

		$strSQL = "SELECT *
			FROM dbEngineeringChange.tblProductECXR
			WHERE intProductID = ".self::getDB()->sanitize($intProductID)."
			AND intEngineeringChangeID = ".self::getDB()->sanitize($intEngineeringChangeID)."
			LIMIT 1
		";
		$rsResult = $this->getDB()->query($strSQL);
		$arrRow = $this->getDB()->fetch_assoc($rsResult);
		$this->setVarsFromRow($arrRow);
	}
}
?>