<?php
	require_once("ArrayClass.class.php");
	include_once("UserRoleXR.class.php");
	include_once("Product.class.php");
	include_once("Search.class.php");
	include_once("ProgramCommon.class.php");

	class UserArray extends ArrayClass {
		function __construct(){
			parent::__construct("User");
		}

		function load() {
			$strSQL = " SELECT * FROM dbPLM.tblUser
				ORDER BY strDisplayName";
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				$this->_arrObjects[$arrRow["intUserID"]] = new User();
				$this->_arrObjects[$arrRow["intUserID"]]->setVarsFromRow($arrRow);
			}
		}

		// $intProgramCommonID will put program members at the top of the list
		function loadFNGEmployees($intProgramCommonID=null){
			$strTableSQL = $strOrderBySQL = "";
			if($intProgramCommonID) {
				$strTableSQL = "
					LEFT JOIN (
						SELECT tblMember.intMemberID, tblMember.intUserID
							FROM dbPLM.tblMember
							INNER JOIN dbPLM.tblProgramCommon
								ON tblProgramCommon.intTopLevelProductID = tblMember.intProgramID
							WHERE tblProgramCommon.intProgramCommonID = ".self::getDB()->sanitize($intProgramCommonID)."
					) AS tblMember2
						ON tblUser.intUserID = tblMember2.intUserID
				";
				$strOrderBySQL = " IF(tblMember2.intMemberID IS NOT NULL, 0, 1), ";
			}
			$strSQL = " SELECT tblUser.*
				FROM dbPLM.tblUser
				$strTableSQL
				WHERE blnFNGEmployee
				ORDER BY $strOrderBySQL strDisplayName";
			//echo $strSQL . "<br />";
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				$this->_arrObjects[$arrRow["intUserID"]] = new User();
				$this->_arrObjects[$arrRow["intUserID"]]->setVarsFromRow($arrRow);
			}
		}

		function loadByTimeTracking(){
			$strSQL = " SELECT * FROM dbPLM.tblUser
				WHERE blnTimeTracking
				ORDER BY strDisplayName";
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				$this->_arrObjects[$arrRow["intUserID"]] = new User();
				$this->_arrObjects[$arrRow["intUserID"]]->setVarsFromRow($arrRow);
			}
		}

		function loadForEmails($arrRoles=array()){
			$strTableSQL = $strWhereSQL = "";
			if($arrRoles) {
				$strTableSQL = " INNER JOIN dbPLM.tblUserRoleXR
 					ON tblUserRoleXR.intUserID = tblUser.intUserID ";
				$strWhereSQL = " AND intRoleID IN (".implode(",", array_map(array("Database", "sanitize"), $arrRoles)).") ";
			}
			$strSQL = " SELECT * FROM dbPLM.tblUser
				$strTableSQL
				WHERE blnFNGEmployee
				$strWhereSQL
				ORDER BY strDisplayName";
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				$this->_arrObjects[$arrRow["intUserID"]] = new User();
				$this->_arrObjects[$arrRow["intUserID"]]->setVarsFromRow($arrRow);
			}
		}

		function loadForMembers($arrRoleIDs=null) {
			$strTableSQL = $strWhereSQL = "";

			if($arrRoleIDs) {
				foreach($arrRoleIDs as $intRoleID) {
					if($intRoleID) {
						$strWhereSQL .= " AND intUserID IN (
							SELECT intUserID
								FROM dbPLM.tblUserRoleXR
							WHERE tblUserRoleXR.intRoleID = ".self::getDB()->sanitize($intRoleID)."
						) ";
					}
				}
			}

			$strSQL = " SELECT tblUser.*
				FROM dbPLM.tblUser
				WHERE blnFNGEmployee
				$strWhereSQL";
			$strSQL .= " ORDER BY tblUser.strDisplayName ";
			//echo $strSQL;
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				$this->_arrObjects[$arrRow["intUserID"]] = new User();
				$this->_arrObjects[$arrRow["intUserID"]]->setVarsFromRow($arrRow);
			}
		}

		function loadForProgramSearch() {
			$strSQL = ProductArray::getProgramListPageQuery("Member");
			//echo $strSQL;
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				$this->_arrObjects[$arrRow["intUserID"]] = new User();
				$this->_arrObjects[$arrRow["intUserID"]]->setVarsFromRow($arrRow);
			}

			$objSearch = (isset($_SESSION["objSearch"])?$_SESSION["objSearch"]:new Search());
			$strSearchValue = $objSearch->getMemberUserID();
			if($strSearchValue && !isset($this->_arrObjects[$strSearchValue])) { // Search record not in options, add search record
				$this->createNew($strSearchValue)->load($strSearchValue);
			}
		}

		function loadForIssueListPage() {
			$objSearch = $_SESSION["objSearch"];
			$strSQL = IssueArray::getIssueListPageQuery($objSearch, "Person Responsible");

			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				$this->_arrObjects[$arrRow["intUserID"]] = new User();
				$this->_arrObjects[$arrRow["intUserID"]]->setVarsFromRow($arrRow);
			}

			$strSearchValue = $objSearch->getPersonResponsibleUserID();
			if($strSearchValue && !isset($this->_arrObjects[$strSearchValue])) { // Search record not in options, add search record
				$this->createNew($strSearchValue)->load($strSearchValue);
			}
		}

		function loadForToolManagement(){
			$objSearch = $_SESSION["objSearch"];
			$strSQL = ToolArray::getToolListPageQuery($objSearch, "Tooling Engineer");
			//echo $strSQL;

			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				$this->_arrObjects[$arrRow["intUserID"]] = new User();
				$this->_arrObjects[$arrRow["intUserID"]]->setVarsFromRow($arrRow);
			}

			$strSearchValue = $objSearch->getToolingEngineerUserID();
			if($strSearchValue && !isset($this->_arrObjects[$strSearchValue])) { // Search record not in options, add search record
				$this->createNew($strSearchValue)->load($strSearchValue);
			}
		}

		function loadForProgramSearchWaitingApproval() {
			$objSearch = $_SESSION["objSearch"];
			$strSQL = ProductArray::getProgramListPageQuery("Waiting Approval User");
			//echo $strSQL;
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				$this->_arrObjects[$arrRow["intUserID"]] = new User();
				$this->_arrObjects[$arrRow["intUserID"]]->setVarsFromRow($arrRow);
			}

			$strSearchValue = $objSearch->getWaitingApprovalUserID();
			if($strSearchValue && !isset($this->_arrObjects[$strSearchValue])) { // Search record not in options, add search record
				$this->createNew($strSearchValue)->load($strSearchValue);
			}
		}

		function loadForDocumentListPage() {
			$objSearch = $_SESSION["objSearch"];
			$strSQL = DocumentArray::getDocumentListPageQuery($objSearch, "Created By");

			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				$this->_arrObjects[$arrRow["intUserID"]] = new User();
				$this->_arrObjects[$arrRow["intUserID"]]->setVarsFromRow($arrRow);
			}

			$strSearchValue = $objSearch->getDocumentCreatedByUserID();
			if($strSearchValue && !isset($this->_arrObjects[$strSearchValue])) { // Search record not in options, add search record
				$this->createNew($strSearchValue)->load($strSearchValue);
			}
		}

		function loadForUserManagement() {
			$objSearch = $_SESSION["objSearch"];

			$strSQL = " SELECT tblFacility.*, tblRole.*, tblUserRoleXR.*, tblUser.*
				FROM dbPLM.tblUser
				LEFT JOIN dbPLM.tblUserRoleXR
					ON tblUser.intUserID = tblUserRoleXR.intUserID
				LEFT JOIN dbPLM.tblRole
					ON tblRole.intRoleID = tblUserRoleXR.intRoleID
				LEFT JOIN dbPLM.tblFacility
					ON tblFacility.intFacilityID = tblUser.intProductionFacilityID
				WHERE blnFNGEmployee ";
			$strSQL .= $objSearch->getUserListPage()->getSortQuery();
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				if(!isset($this->_arrObjects[$arrRow["intUserID"]])) {
					$this->_arrObjects[$arrRow["intUserID"]] = new User();
					$this->_arrObjects[$arrRow["intUserID"]]->setVarsFromRow($arrRow);
					$this->_arrObjects[$arrRow["intUserID"]]->getProductionFacility()->setVarsFromRow($arrRow);
				}
				if($arrRow["intRoleID"]) {
					$objUserRoleXR = new UserRoleXR();
					$objUserRoleXR->setVarsFromRow($arrRow);
					$objUserRoleXR->getRole()->setVarsFromRow($arrRow);
					$this->_arrObjects[$arrRow["intUserID"]]->getUserRoleXRArray()->setObject($arrRow["intUserRoleXRID"], $objUserRoleXR);
				}
			}
		}

		function loadProgramMembership($intProgramCommonID) {
			$strSQL = "SELECT tblUser.*
				FROM dbPLM.tblUser
				INNER JOIN dbPLM.tblMember
					ON tblMember.intUserID = tblUser.intUserID
				INNER JOIN dbPLM.tblProduct
					ON tblProduct.intProgramID = tblMember.intProgramID
				INNER JOIN dbPLM.tblProgramCommon
					ON tblProgramCommon.intTopLevelProductID = tblProduct.intProgramID
				WHERE tblProgramCommon.intProgramCommonID = ".$this->getDB()->sanitize($intProgramCommonID);
			//echo $strSQL . "<br />";
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				$this->_arrObjects[$arrRow["intUserID"]] = new User();
				$this->_arrObjects[$arrRow["intUserID"]]->setVarsFromRow($arrRow);
			}
		}

		/* Deprecated, use loadProgramMembership($intProgramCommonID) */
		function loadProgramMembers($intProgramID) {
			$intTopLevelProgramID = Product::getTopLevelProgramIDFromProgramID($intProgramID);

			$strSQL = "SELECT tblUser.*
				FROM dbPLM.tblUser
				INNER JOIN dbPLM.tblMember
					ON tblMember.intUserID = tblUser.intUserID
				INNER JOIN dbPLM.tblProduct
					ON tblProduct.intProgramID = tblMember.intProgramID
				WHERE tblProduct.intProgramID = ".$this->getDB()->sanitize($intTopLevelProgramID);
			//echo $strSQL . "<br />";
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				$this->_arrObjects[$arrRow["intUserID"]] = new User();
				$this->_arrObjects[$arrRow["intUserID"]]->setVarsFromRow($arrRow);
			}
		}

		function loadPotentialAttendees($objAttendeeArray){
			$arrUserIDsToOmit = array();
			if($objAttendeeArray->getArray()) {
				foreach($objAttendeeArray->getArray() as $objAttendee) {
					$arrUserIDsToOmit[] = $objAttendee->getUserID();
				}
			}

			$strSQL = "SELECT tblUser.*
				FROM dbPLM.tblUser
				WHERE blnFNGEmployee
				AND intUserID NOT IN ('".implode("','", $arrUserIDsToOmit)."')
				ORDER By strDisplayName";
			//echo $strSQL;
			$rsResult = self::getDB()->query($strSQL);
			while ($arrRow = self::getDB()->fetch_assoc($rsResult)) {
				$this->_arrObjects[$arrRow["intUserID"]] = new User();
				$this->_arrObjects[$arrRow["intUserID"]]->setVarsFromRow($arrRow);
			}
		}

		function loadByRoleID($intRoleID) {
			if(!$intRoleID)
				return;

			$strSQL = " SELECT * FROM dbPLM.tblUser
				INNER JOIN dbPLM.tblUserRoleXR
					ON tblUserRoleXR.intUserID = tblUser.intUserID ";
				if(is_array($intRoleID)) {
					$strSQL .= " WHERE intRoleID IN ('".implode("','", $intRoleID)."')";
				} else {
					$strSQL .= " WHERE intRoleID = ".self::getDB()->sanitize($intRoleID);
				}
			$strSQL .= " ORDER BY strDisplayName";
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				$this->_arrObjects[$arrRow["intUserID"]] = new User();
				$this->_arrObjects[$arrRow["intUserID"]]->setVarsFromRow($arrRow);
			}
		}

		function loadByNotificationID($intNotificationID) {
			if(!$intNotificationID)
				return;

			$strSQL = " SELECT tblUserNotification.*, tblUser.*
				FROM dbPLM.tblUser
				INNER JOIN dbSystem.tblUserNotification
					ON tblUserNotification.intUserID = tblUser.intUserID
				WHERE intNotificationID = ".self::getDB()->sanitize($intNotificationID)."
				ORDER BY strDisplayName";
			//echo $strSQL;
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				$this->_arrObjects[$arrRow["intUserID"]] = new User();
				$this->_arrObjects[$arrRow["intUserID"]]->setVarsFromRow($arrRow);
				$this->_arrObjects[$arrRow["intUserID"]]->getUserNotification()->setVarsFromRow($arrRow);
			}
		}

		function loadProgramManagers($intProgramID){
			if(!$intProgramID)
				return false;

			$strSQL = "SELECT tblUser.*, tblMember.*
				FROM dbPLM.tblMember
				INNER JOIN dbPLM.tblUser
					ON tblUser.intUserID = tblMember.intUserID
				LEFT JOIN dbPLM.tblMemberRole
					ON tblMemberRole.intMemberID = tblMember.intMemberID
				WHERE tblMember.intProgramID = ".$this->getDB()->sanitize($intProgramID)."
				AND tblMemberRole.intRoleID = ".$this->getDB()->sanitize(intROLE_ID_PROGRAM_MANAGER);
			$strSQL .= " ORDER BY tblUser.strDisplayName ";
			//echo $strSQL;
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				if(!isset($this->_arrObjects[$arrRow["intUserID"]])) {
					$this->_arrObjects[$arrRow["intUserID"]] = new User();
					$this->_arrObjects[$arrRow["intUserID"]]->setVarsFromRow($arrRow);
				}
			}
		}

		function loadByProgramCommonIDAndRoleIDs($intProgramCommonID, $arrRoleIDs) {
			if(!$intProgramCommonID || !$arrRoleIDs)
				return;

			$strSQL = "SELECT tblUser.*
				FROM dbPLM.tblUser
				INNER JOIN dbPLM.tblMember
					ON tblMember.intUserID = tblUser.intUserID
				INNER JOIN dbPLM.tblMemberRole
					ON tblMemberRole.intMemberID = tblMember.intMemberID
				INNER JOIN dbPLM.tblProgramCommon
					ON tblProgramCommon.intTopLevelProductID = tblMember.intProgramID
				WHERE tblMemberRole.intRoleID IN ('".implode("','", $arrRoleIDs)."')
				AND tblProgramCommon.intProgramCommonID = ".self::getDB()->sanitize($intProgramCommonID);
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				$this->_arrObjects[$arrRow["intUserID"]] = new User();
				$this->_arrObjects[$arrRow["intUserID"]]->setVarsFromRow($arrRow);
			}
		}

		function removeUsers($arrUserIDs) {
			if(!$arrUserIDs)
				return;

			foreach($arrUserIDs as $intUserID) {
				if(($objUser = $this->getObject($intUserID)) && !$objUser->getUserNotification()->getUserNotificationID()) { // Remove users who haven't been notified
					$this->removeObject($intUserID);
				}
			}
		}

		function loadWeeklyHours($dtmDate) {
			if(!$dtmDate)
				return;

			$strSQL = "SELECT tblProduct.*, tblPlatform.*, tblProgramCommon.*, tblTimesheet.*, tblTime.*, tblUser.*
			FROM dbPLM.tblUser
			LEFT JOIN dbTimeTracking.tblTimesheet
				ON tblTimesheet.intUserID = tblUser.intUserID
				AND tblTimesheet.dtmWeekEndDate = '".$dtmDate."'
			LEFT JOIN dbTimeTracking.tblTime
				ON tblUser.intUserID = tblTime.intUserID
				AND dtmDate >= '".date("Y-m-d", strtotime($dtmDate . " -6 days"))."'
				AND dtmDate <= '".$dtmDate."'
			LEFT JOIN dbPLM.tblProgramCommon
				ON tblProgramCommon.intProgramCommonID = tblTime.intProgramCommonID
			LEFT JOIN dbPLM.tblPlatform
				ON tblPlatform.intPlatformID = tblProgramCommon.intPlatformID
			LEFT JOIN dbPLM.tblProduct
				ON tblProduct.intProgramID = tblProgramCommon.intTopLevelProductID
			WHERE blnTimeTracking
			ORDER BY strDisplayName";
			//echo $strSQL;
			$objProgramCommonArray = new ProgramCommonArray();
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				if(!isset($this->_arrObjects[$arrRow["intUserID"]])) {
					$this->_arrObjects[$arrRow["intUserID"]] = new User();
					$this->_arrObjects[$arrRow["intUserID"]]->setVarsFromRow($arrRow);
					$this->_arrObjects[$arrRow["intUserID"]]->getTimesheet()->setVarsFromRow($arrRow);
				}
				if($arrRow["intTimeID"]) {
					if(!$this->_arrObjects[$arrRow["intUserID"]]->getProgramCommonArray()->objectIsSet($arrRow["intProgramCommonID"])) {
						$this->_arrObjects[$arrRow["intUserID"]]->getProgramCommonArray()->addFromRow($arrRow, "intProgramCommonID");
					}
					$this->_arrObjects[$arrRow["intUserID"]]->getProgramCommonArray()->getObject($arrRow["intProgramCommonID"])->getHours()->add($arrRow["dblRegularTime"], $arrRow["dblOverTime"]);
					$this->_arrObjects[$arrRow["intUserID"]]->getProgramCommonArray()->getObject($arrRow["intProgramCommonID"])->getHoursArray()->getObject($arrRow["dtmDate"], true)->add($arrRow["dblRegularTime"], $arrRow["dblOverTime"]);
					$this->_arrObjects[$arrRow["intUserID"]]->getHours()->add($arrRow["dblRegularTime"], $arrRow["dblOverTime"]);

					if(!$objProgramCommonArray->objectIsSet($arrRow["intProgramCommonID"])) {
						$objProgramCommonArray->addFromRow($arrRow, "intProgramCommonID");
						$objProgramCommonArray->getObject($arrRow["intProgramCommonID"])->getPlatform()->setVarsFromRow($arrRow);
						$objProgramCommonArray->getObject($arrRow["intProgramCommonID"])->getTopLevelProduct()->setVarsFromRow($arrRow);
					}

					$objProgramCommonArray->getObject($arrRow["intProgramCommonID"])->getHours()->add($arrRow["dblRegularTime"], $arrRow["dblOverTime"]);
				}
			}
			return $objProgramCommonArray;
		}
	}

	require_once("DataClass.class.php");

	class UserBase extends DataClass {
		protected $_intUserID;
		protected $_strFirstName;
		protected $_strLastName;
		protected $_strEmail;
		protected $_strDisplayName;
		protected $_strLoginName;
		protected $_strPassword;
		protected $_strUserStatus;
		protected $_intProgramTypeID;
		protected $_intCustomerID;
		protected $_intProductionFacilityID;
		protected $_blnFilterByProductionFacility;
		protected $_blnMemberOf;
		protected $_blnFNGEmployee;
		protected $_dtmRegistered;
		protected $_dtmLastLogin;

		function UserBase($intUserID=null) {
			$this->DataClass();
			if($intUserID) {
				$this->load($intUserID);
			}
		}

		protected function insert() {
			//$this->setCreatedOn(now());
			//$this->setCreatedBy($_SESSION["udfUser"]->getUserID());

			$strSQL = "INSERT INTO dbPLM.tblUser SET
				intUserID = ".$this->getDB()->sanitize($this->_intUserID)."
				,strEmail = ".$this->getDB()->sanitize($this->_strEmail)."
				,strDisplayName = ".$this->getDB()->sanitize($this->_strDisplayName)."
				,strLoginName = ".$this->getDB()->sanitize($this->_strLoginName)."
				,strPassword = ".$this->getDB()->sanitize($this->getPassword())."
				,strUserStatus = ".$this->getDB()->sanitize($this->getUserStatus())."
				,intProgramTypeID = ".$this->getDB()->sanitize($this->getProgramTypeID())."
				,intCustomerID = ".$this->getDB()->sanitize($this->getCustomerID())."


				,intProductionFacilityID = ".$this->getDB()->sanitize($this->getProductionFacilityID())."
				,blnFilterByProductionFacility = ".$this->getDB()->sanitize($this->getFilterByProductionFacility())."
				,blnMemberOf = ".$this->getDB()->sanitize($this->getMemberOf())."
				,blnFNGEmployee = ".$this->getDB()->sanitize($this->getFNGEmployee())."
				,dtmRegistered = ".$this->getDB()->sanitize($this->getRegistered())."
				,dtmLastLogin = ".$this->getDB()->sanitize($this->getLastLogin())."
			";
			//echo $strSQL . "<br />";
			$this->getDB()->query($strSQL);
			$this->setUserID($this->getDB()->insert_id());
			return $this->getUserID();
		}

		protected function update() {
			//$this->setModifiedOn(now());
			//$this->setModifiedBy($_SESSION["udfUser"]->getUserID());

			$strSQL = "UPDATE dbPLM.tblUser SET
				intUserID = ".$this->getDB()->sanitize($this->_intUserID)."
				,strEmail = ".$this->getDB()->sanitize($this->_strEmail)."
				,strDisplayName = ".$this->getDB()->sanitize($this->_strDisplayName)."
				,strLoginName = ".$this->getDB()->sanitize($this->_strLoginName)."
				,strPassword = ".$this->getDB()->sanitize($this->_strPassword)."
				,strUserStatus = ".$this->getDB()->sanitize($this->getUserStatus())."
				,intProgramTypeID = ".$this->getDB()->sanitize($this->getProgramTypeID())."
				,intCustomerID = ".$this->getDB()->sanitize($this->getCustomerID())."
				,intProductionFacilityID = ".$this->getDB()->sanitize($this->getProductionFacilityID())."
				,blnFilterByProductionFacility = ".$this->getDB()->sanitize($this->getFilterByProductionFacility())."
				,blnMemberOf = ".$this->getDB()->sanitize($this->getMemberOf())."
				,blnFNGEmployee = ".$this->getDB()->sanitize($this->getFNGEmployee())."
				,dtmRegistered = ".$this->getDB()->sanitize($this->getRegistered())."
				,dtmLastLogin = ".$this->getDB()->sanitize($this->getLastLogin())."
				WHERE intUserID = '".$this->_intUserID."'
			";
			//echo $strSQL;
			return $this->getDB()->query($strSQL);
		}

		function save() {
			if($this->_intUserID) {
				$this->update();
			} else {
				$this->insert();
			}
		}

		function delete() {
			if($this->_intUserID) {
				$strSQL = "DELETE FROM dbPLM.tblUser
				WHERE intUserID = '$this->_intUserID'
				";
				return $this->getDB()->query($strSQL);
			}
		}

		function load($intUserID) {
			if($intUserID) {
				$strSQL = "SELECT *
					FROM dbPLM.tblUser
					WHERE intUserID = '$intUserID'
					LIMIT 1
				";
				$rsUser = $this->getDB()->query($strSQL);

				$arrUser = $this->getDB()->fetch_assoc($rsUser);
				$this->setVarsFromRow($arrUser);
			}
		}

		function setVarsFromRow($arrUser) {
			if(isset($arrUser["intUserID"])) $this->_intUserID = $arrUser["intUserID"];
			if(isset($arrUser["strFirstName"])) $this->_strFirstName = $arrUser["strFirstName"];
			if(isset($arrUser["strLastName"])) $this->_strLastName = $arrUser["strLastName"];
			if(isset($arrUser["strEmail"])) $this->_strEmail = $arrUser["strEmail"];
			if(isset($arrUser["strDisplayName"])) $this->_strDisplayName = $arrUser["strDisplayName"];
			if(isset($arrUser["strLoginName"])) $this->_strLoginName = $arrUser["strLoginName"];
			if(isset($arrUser["strPassword"])) $this->_strPassword = $arrUser["strPassword"];
			if(isset($arrUser["strUserStatus"])) $this->_strUserStatus = $arrUser["strUserStatus"];
			if(isset($arrUser["intProgramTypeID"])) $this->_intProgramTypeID = $arrUser["intProgramTypeID"];
			if(isset($arrUser["intCustomerID"])) $this->_intCustomerID = $arrUser["intCustomerID"];
			if(isset($arrUser["intProductionFacilityID"])) $this->_intProductionFacilityID = $arrUser["intProductionFacilityID"];
			if(isset($arrUser["blnFilterByProductionFacility"])) $this->_blnFilterByProductionFacility = $arrUser["blnFilterByProductionFacility"];
			if(isset($arrUser["blnMemberOf"])) $this->_blnMemberOf = $arrUser["blnMemberOf"];
			if(isset($arrUser["blnFNGEmployee"])) $this->_blnFNGEmployee = $arrUser["blnFNGEmployee"];
			if(isset($arrUser["dtmRegistered"])) $this->_dtmRegistered = $arrUser["dtmRegistered"];
			if(isset($arrUser["dtmLastLogin"])) $this->_dtmLastLogin = $arrUser["dtmLastLogin"];
		}
		function getUserID() {
			return $this->_intUserID;
		}
		function setUserID($value) {
			if($this->_intUserID !== $value) {
				$this->_intUserID = $value;
				$this->_blnDirty = true;
			}
		}

		function getFirstName() {
			return $this->_strFirstName;
		}
		function setFirstName($value) {
			if($this->_strFirstName !== $value) {
				$this->_strFirstName = $value;
				$this->_blnDirty = true;
			}
		}

		function getLastName() {
			return $this->_strLastName;
		}
		function setLastName($value) {
			if($this->_strLastName !== $value) {
				$this->_strLastName = $value;
				$this->_blnDirty = true;
			}
		}

		function getEmail() {
			return $this->_strEmail;
		}
		function setEmail($value) {
			if($this->_strEmail !== $value) {
				$this->_strEmail = $value;
				$this->_blnDirty = true;
			}
		}

		function getDisplayName() {
			return $this->_strDisplayName;
		}
		function setDisplayName($value) {
			if($this->_strDisplayName !== $value) {
				$this->_strDisplayName = $value;
				$this->_blnDirty = true;
			}
		}

		function getLoginName() {
			return $this->_strLoginName;
		}
		function setLoginName($value) {
			if($this->_strLoginName !== $value) {
				$this->_strLoginName = $value;
				$this->_blnDirty = true;
			}
		}

		function getPassword() {
			return $this->_strPassword;
		}
		function setPassword($strPassword) {
			if($this->_strPassword !== $strPassword) {
				$this->_strPassword = $strPassword;
				$this->_blnDirty = true;
			}
		}

		function getUserStatus() {
			return $this->_strUserStatus;
		}
		function setUserStatus($strUserStatus) {
			if($this->_strUserStatus !== $strUserStatus) {
				$this->_strUserStatus = $strUserStatus;
				$this->_blnDirty = true;
			}
		}

		function getProgramTypeID() {
			return $this->_intProgramTypeID;
		}
		function setProgramTypeID($value) {
			if($this->_intProgramTypeID !== $value) {
				$this->_intProgramTypeID = $value;
				$this->_blnDirty = true;

			}
		}

		function getCustomerID() {
			return $this->_intCustomerID;
		}
		function setCustomerID($value) {
			if($this->_intCustomerID !== $value) {
				$this->_intCustomerID = $value;
				$this->_blnDirty = true;
			}
		}

		function getProductionFacilityID() {
			return $this->_intProductionFacilityID;
		}
		function setProductionFacilityID($value) {
			if($this->_intProductionFacilityID !== $value) {
				$this->_intProductionFacilityID = $value;
				$this->_blnDirty = true;
			}
		}

		function getFilterByProductionFacility() {
			return $this->_blnFilterByProductionFacility;
		}
		function setFilterByProductionFacility($value) {
			if($this->_blnFilterByProductionFacility !== $value) {
				$this->_blnFilterByProductionFacility = $value;
				$this->_blnDirty = true;
			}
		}

		function getMemberOf() {
			return $this->_blnMemberOf;
		}
		function setMemberOf($value) {
			if($this->_blnMemberOf !== $value) {
				$this->_blnMemberOf = $value;
				$this->_blnDirty = true;
			}
		}

		function getFNGEmployee() {
			return $this->_blnFNGEmployee;
		}
		function setFNGEmployee($value) {
			if($this->_blnFNGEmployee !== $value) {
				$this->_blnFNGEmployee = $value;
				$this->_blnDirty = true;
			}
		}

		function getRegistered() {
			return $this->_dtmRegistered;
		}
		function setRegistered($dtmRegistered) {
			if($this->_dtmRegistered !== $dtmRegistered) {
				$this->_dtmRegistered = $dtmRegistered;
				$this->_blnDirty = true;
			}
		}

		function getLastLogin() {
			return $this->_dtmLastLogin;
		}
		function setLastLogin($dtmLastLogin) {
			if($this->_dtmLastLogin !== $dtmLastLogin) {
				$this->_dtmLastLogin = $dtmLastLogin;
				$this->_blnDirty = true;
			}
		}
	}

	include_once("UserRoleXR.class.php");
	include_once("ProcessInstance.class.php");
	include_once("ActivityLog.class.php");
	include_once("Product.class.php");
	include_once("Permission.class.php");
	include_once("UserNotification.class.php");
	include_once("Facility.class.php");
	include_once("ConditionInstance.class.php");
	include_once("ProgramCommon.class.php");

	class User extends UserBase {
		private $_objProfilePhoto;
		private $_objUserRoleXRArray;
		private $_objApprovalProcessInstanceArray;
		private $_objProcessInstanceArray;
		private $_objConditionInstance;
		private $_objUserCustomerArray;
		private $_objUserProgramTypeArray;
		private $_objPermissionArray;
		private $_objUserNotification; // Single notification sent to a user for a particular object, such as Issue
		private $_objProductionFacility;
		private $_objTimeArray;
		private $_objTimesheet;
		private $_objProgramCommonArray; // Used for time tracking hours
		private $_objHours; // Used for time tracking hours

		private $_intProgramCount = 0;
		private $_intIssueCount = 0;
		private $_intDocumentCount = 0;
		private $_intWaitingApprovalCount = 0;

		function User($intUserID=null) {
			$this->UserBase($intUserID);

		}

		function setVarsFromRow($arrRow, $strType="") {
			parent::setVarsFromRow($arrRow);

			if(isset($arrRow["str".$strType."DisplayName"])) $this->setDisplayName($arrRow["str".$strType."DisplayName"]);
		}

		function login($strLoginName, $strPassword) {
			if($intUserID = $this->validateLogin($strLoginName, $strPassword)) {
				$this->load($intUserID);
				$this->getProductionFacility()->load($this->getProductionFacilityID());
				//$this->getUserRoleXRArray()->loadByUserID($this->getUserID());
				$this->getPermissionArray()->loadByUserID($this->getUserID());
				$this->setLastLogin(now());
				$this->save();

				$this->loadProgramCount();
				$this->loadIssueCount();
				$this->loadDocumentCount();

				$_SESSION["objUser"] = $this;

				ActivityLog::log("Login");

				return true;
			} else {
				$this->setLoginName($strLoginName);
				$_SESSION["arrFormData"]["objLoginUser"] = $this;
				return false;
			}
		}

		function validateLogin($strLoginName, $strPassword) {
			$strSQL = "SELECT intUserID, strUserStatus
				FROM dbPLM.tblUser
				WHERE strLoginName = ".$this->getDB()->sanitize($strLoginName)."
				AND strPassword = md5(".$this->getDB()->sanitize($strPassword).")
				LIMIT 1";
			$rsExists = $this->getDB()->query($strSQL);
			$arrExists = $this->getDB()->fetch_assoc($rsExists);

			if(!$arrExists["intUserID"]) {
				triggerError("Invalid Username and Password combination.");
				return false;
			} elseif($arrExists["strUserStatus"] != "Active") {
				triggerError("Your Username has been disabled.");
				return false;
			}
			return $arrExists["intUserID"];
		}

		function register($strConfirmPassword) {
			$arrErrors = array();

			$this->validateRegistration($strConfirmPassword);

			$strPassword = $this->getPassword();
			$this->encryptPassword();
			$this->setRegistered(date("Y-m-d h:i:s"));
			$intUserID = $this->save();

			try {
				$this->login($this->getEmail(), $strPassword);
			} catch (Exception $objException) {
				triggerWarning("Registration successful, but the following error occurred with logging in:<br />" . $objException->getMessage());
			}
		}

		/** Return true if email format is valid, false otherwise */
		function validateEmail($strEmail) {
			if(preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/", $strEmail)){
				return true;
			}
			return false;
		}

		function isEmailAddressUnique($strEmail) {
			if (!$strEmail) {
				return false;
			}
			$strSQL = "SELECT 1
					   FROM dbPLM.tblUser
					   WHERE strEmail = ".$this->getDB()->sanitize($strEmail);
			$rsResult = $this->getDB()->query($strSQL);
			return !$this->getDB()->num_rows($rsResult);
		}

		function isDisplayNameUnique($strDisplayName) {
			if (!$strDisplayName) {
				return false;
			}
			$strSQL = "SELECT 1
					   FROM dbPLM.tblUser
					   WHERE strDisplayName = ".$this->getDB()->sanitize($strDisplayName);

			$rsResult = $this->getDB()->query($strSQL);
			return !$this->getDB()->num_rows($rsResult);
		}

		function encryptPassword() {
			$this->setPassword(md5($this->getPassword()));
		}

		function loadByDisplayName($strDisplayName){
			if($strDisplayName) {
				$strSQL = "SELECT *
					FROM dbPLM.tblUser
					WHERE strDisplayName = ".self::getDB()->sanitize($strDisplayName)."
					LIMIT 1
				";
				$rsUser = $this->getDB()->query($strSQL);
				$arrUser = $this->getDB()->fetch_assoc($rsUser);
				$this->setVarsFromRow($arrUser);
			}
		}

		function loadByLoginName($strLoginName){
			if($strLoginName) {
				$strSQL = "SELECT *
					FROM dbPLM.tblUser
					WHERE strLoginName = ".self::getDB()->sanitize($strLoginName)."
					LIMIT 1
				";
				$rsUser = $this->getDB()->query($strSQL);
				$arrUser = $this->getDB()->fetch_assoc($rsUser);
				$this->setVarsFromRow($arrUser);
			}
		}

		function getID(){
			return $this->getUserID();
		}
		function getName(){
			return $this->getDisplayName();
		}
		function getUserPageURL(){ // Deprecated, use getURL()
			return $this->getURL();
		}
		function getURL(){
			return "user.php?intUserID=".$this->getUserID();
		}

		function getUserRoleXRArray(){
			if(!$this->_objUserRoleXRArray) {
				$this->_objUserRoleXRArray = new UserRoleXRArray();
			}
			return $this->_objUserRoleXRArray;
		}
		function getApprovalProcessInstanceArray(){
			if(!$this->_objApprovalProcessInstanceArray) {
				$this->_objApprovalProcessInstanceArray = new ApprovalProcessInstanceArray();
			}
			return $this->_objApprovalProcessInstanceArray;
		}
		function getProcessInstanceArray(){
			if(!$this->_objProcessInstanceArray) {
				$this->_objProcessInstanceArray = new ProcessInstanceArray();
			}
			return $this->_objProcessInstanceArray;
		}
		function getConditionInstance(){
			if(!$this->_objConditionInstance) {
				$this->_objConditionInstance = new ConditionInstance();
			}
			return $this->_objConditionInstance;
		}
		function getUserCustomerArray(){
			if(!$this->_objUserCustomerArray) {
				$this->_objUserCustomerArray = new UserCustomerArray();
			}
			return $this->_objUserCustomerArray;
		}
		function getUserProgramTypeArray(){
			if(!$this->_objUserProgramTypeArray) {
				$this->_objUserProgramTypeArray = new UserProgramTypeArray();
			}
			return $this->_objUserProgramTypeArray;
		}
		function getPermissionArray(){
			if(!$this->_objPermissionArray) {
				$this->_objPermissionArray = new PermissionArray();
			}
			return $this->_objPermissionArray;
		}
		function getUserNotification(){
			if(!$this->_objUserNotification) {
				$this->_objUserNotification = new UserNotification();
			}
			return $this->_objUserNotification;
		}
		function getProductionFacility(){
			if(!$this->_objProductionFacility) {
				$this->_objProductionFacility = new Facility();
			}
			return $this->_objProductionFacility;
		}
		function getTimeArray(){
			if(!$this->_objTimeArray) {
				$this->_objTimeArray = new TimeArray();
			}
			return $this->_objTimeArray;
		}
		function getTimesheet(){
			include_once("Timesheet.class.php");
			if(!$this->_objTimesheet) {
				$this->_objTimesheet = new Timesheet();
			}
			return $this->_objTimesheet;
		}
		function getProgramCommonArray(){
			if(!$this->_objProgramCommonArray) {
				$this->_objProgramCommonArray = new ProgramCommonArray();
			}
			return $this->_objProgramCommonArray;
		}
		function getHours(){
			if(!$this->_objHours) {
				$this->_objHours = new Hours();
			}
			return $this->_objHours;
		}

		function loadProgramCount(){
			if(!$this->getUserID())
				return false;

			$strSQL = "SELECT COUNT(DISTINCT tblProgramCommon.intProgramCommonID) AS intCount
				FROM dbPLM.tblProduct
				INNER JOIN dbPLM.tblProgramCommon
					ON tblProgramCommon.intTopLevelProductID = tblProduct.intProgramID
				INNER JOIN dbPLM.tblMember
					ON tblMember.intProgramID = tblProduct.intProgramID
				WHERE tblMember.intUserID = ".self::getDB()->sanitize($this->getUserID());
			$rsRow = $this->getDB()->query($strSQL);
			$arrRow = $this->getDB()->fetch_assoc($rsRow);
			$this->setProgramCount($arrRow["intCount"]);
		}
		function loadIssueCount(){
			if(!$this->getUserID())
				return false;

			$strSQL = "SELECT COUNT(DISTINCT tblIssue.intIssueID) AS intCount
				FROM dbIssue.tblIssue
				INNER JOIN dbIssue.tblIssueResponsibility
					ON tblIssue.intIssueID = tblIssueResponsibility.intIssueID
				WHERE tblIssueResponsibility.intUserID = ".self::getDB()->sanitize($this->getUserID())."
				AND strStatus = 'Open'
				AND !blnGateIssue";
			$rsRow = $this->getDB()->query($strSQL);
			$arrRow = $this->getDB()->fetch_assoc($rsRow);
			$this->setIssueCount($arrRow["intCount"]);
		}
		function loadDocumentCount(){
			if(!$this->getUserID())
				return false;

			$strSQL = "SELECT COUNT(*) AS intCount
				FROM dbDocument.tblDocument
				WHERE intCreatedByUserID = ".self::getDB()->sanitize($this->getUserID())."
				AND intParentDocumentID IS NULL ";
			//echo $strSQL;
			$rsRow = $this->getDB()->query($strSQL);
			$arrRow = $this->getDB()->fetch_assoc($rsRow);
			$this->setDocumentCount($arrRow["intCount"]);
		}

		function getProgramCount(){
			return $this->_intProgramCount;

		}
		function setProgramCount($value){
			$this->_intProgramCount = $value;
		}
		function getIssueCount(){
			return $this->_intIssueCount;
		}
		function setIssueCount($value){
			$this->_intIssueCount = $value;
		}
		function getDocumentCount(){
			return $this->_intDocumentCount;
		}
		function setDocumentCount($value){
			$this->_intDocumentCount = $value;
		}
		function getWaitingApprovalCount(){
			return $this->_intWaitingApprovalCount;
		}
		function setWaitingApprovalCount($value){
			$this->_intWaitingApprovalCount = $value;
		}
	}
?>
