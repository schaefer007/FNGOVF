<?php
	include_once("constants_server.php");

	class Database {
		function Database() {

		}

		static public function connect_and_select() {
			self::connect(strDATABASE_HOST,strDATABASE_USERNAME, strDATABASE_PASSWORD);
			if(strDATABASE_NAME != "") {
				self::select_db(strDATABASE_NAME);
			}
		}

		static function connect($strHostName, $strUserName, $strPassword) {
			$result = mysql_connect($strHostName, $strUserName, $strPassword);
			//error_log("connection result is $result");
			return $result;
		}

		static function select_db($strDatabaseName) {
			return mysql_select_db($strDatabaseName);
		}

		function query($strSQL) {
			//echo $strSQL . "<br /><br />";
			//error_log($strSQL);
			$blnResult = mysql_query($strSQL);
//			if(!$blnResult) {
//				base::write_log("QUERY FAIL: $strSQL","E");
//			}
			if($blnResult == false) {
				base::write_log("Database action failed","E");
			}
			return $blnResult;
		}

		function fetch_assoc($rsResult) {
			return mysql_fetch_assoc($rsResult);
		}

		function fetch_row($rsResult) {
			return mysql_fetch_row($rsResult);
		}

		function fetch_object($rsResult) {
			return mysql_fetch_object($rsResult);
		}

		function num_rows($rsResult) {
			return mysql_num_rows($rsResult);
		}

		function affected_rows() {
			return mysql_affected_rows();
		}

		function insert_id() {
			return mysql_insert_id();
		}

		static function sanitize($mixValue) {
			if($mixValue===null) {
				return "null";
			} else {
				return "'".mysql_real_escape_string($mixValue)."'";
			}
		}

		static function begin() {
			return self::query("begin");
		}

		static function commit() {
			return self::query("commit");
		}

		function rollback() {
			return mysql_query("rollback");
		}

		function found_rows() {
			$rsResult = $this->query("SELECT FOUND_ROWS()");
			$arrRow = $this->fetch_row($rsResult);
			return $arrRow[0];
		}
	}
?>