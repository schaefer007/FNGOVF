<?php
require_once("ArrayClass.class.php");

class ConditionInstanceArray extends ArrayClass {
	function __construct(){
		parent::__construct("ConditionInstance");
	}

	function load() {
		$strSQL = " SELECT * FROM dbSystem.tblConditionInstance";
		$rsResult = $this->getDB()->query($strSQL);
		while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
			$this->_arrObjects[$arrRow["intConditionInstanceID"]] = new ConditionInstance();
			$this->_arrObjects[$arrRow["intConditionInstanceID"]]->setVarsFromRow($arrRow);
		}
	}

	function loadByProcessTransitionInstanceID($intProcessTransitionInstanceID) {
		if(!$intProcessTransitionInstanceID)
			return;

		$strSQL = " SELECT tblUser.*, tblConditionInstance.*
			FROM dbSystem.tblConditionInstance
			INNER JOIN dbPLM.tblUser
				ON tblUser.intUserID = tblConditionInstance.intUserID
			WHERE intProcessTransitionInstanceID = ".self::getDB()->sanitize($intProcessTransitionInstanceID);
		//echo $strSQL;
		$rsResult = $this->getDB()->query($strSQL);
		while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
			$this->_arrObjects[$arrRow["intConditionInstanceID"]] = new ConditionInstance();
			$this->_arrObjects[$arrRow["intConditionInstanceID"]]->setVarsFromRow($arrRow);
			$this->_arrObjects[$arrRow["intConditionInstanceID"]]->getUser()->setVarsFromRow($arrRow);
		}
	}

	function getUserArrayByProcessTransitionInstanceID($intProcessTransitionInstanceID) {
		include_once("User.class.php");
		$objUserArray = new UserArray();

		if(!$intProcessTransitionInstanceID)
			return $objUserArray;

		$strSQL = " SELECT DISTINCT tblUser.*
			FROM dbSystem.tblConditionInstance
			INNER JOIN dbPLM.tblUser
				ON tblUser.intUserID = tblConditionInstance.intUserID
			WHERE intProcessTransitionInstanceID = ".self::getDB()->sanitize($intProcessTransitionInstanceID);
		$rsResult = $this->getDB()->query($strSQL);
		while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
			$objUserArray->addFromRow($arrRow, "intUserID");
		}

		return $objUserArray;
	}
}

require_once("DataClass.class.php");

class ConditionInstanceBase extends DataClass {
	protected $_intConditionInstanceID;
	protected $_intProcessTransitionInstanceID;
	protected $_intConditionID;
	protected $_intRoleID;
	protected $_intUserID;
	protected $_dtmCompletedDate;
	protected $_txtComment;

	function __construct($intConditionInstanceID=null) {
		$this->DataClass();
		if($intConditionInstanceID) {
			$this->load($intConditionInstanceID);
		}
	}

	protected function insert() {
		base::write_log("Condition instance created","S");
		$strSQL = "INSERT INTO dbSystem.tblConditionInstance SET ";
		$strConnector = "";
		$strSQL .= $strConnector . "intConditionInstanceID = ".$this->getDB()->sanitize(self::getConditionInstanceID());
		$strConnector = ",";
		if(isset($this->_intProcessTransitionInstanceID)) {
			$strSQL .= $strConnector . "intProcessTransitionInstanceID = ".$this->getDB()->sanitize(self::getProcessTransitionInstanceID());
			$strConnector = ",";
		}
		if(isset($this->_intConditionID)) {
			$strSQL .= $strConnector . "intConditionID = ".$this->getDB()->sanitize(self::getConditionID());
			$strConnector = ",";
		}
		if(isset($this->_intRoleID)) {
			$strSQL .= $strConnector . "intRoleID = ".$this->getDB()->sanitize(self::getRoleID());
			$strConnector = ",";
		}
		if(isset($this->_intUserID)) {
			$strSQL .= $strConnector . "intUserID = ".$this->getDB()->sanitize(self::getUserID());
			$strConnector = ",";
		}
		if(isset($this->_dtmCompletedDate)) {
			$strSQL .= $strConnector . "dtmCompletedDate = ".$this->getDB()->sanitize(self::getCompletedDate());
			$strConnector = ",";
		}
		if(isset($this->_txtComment)) {
			$strSQL .= $strConnector . "txtComment = ".$this->getDB()->sanitize(self::getComment());
			$strConnector = ",";
		}
		//echo $strSQL;
		$this->getDB()->query($strSQL);
		$this->setConditionInstanceID($this->getDB()->insert_id());
		return $this->getConditionInstanceID();
	}

	protected function update() {
		base::write_log("Condition instance updated","S");
		$strSQL = "UPDATE dbSystem.tblConditionInstance SET ";
		$strConnector = "";
		$strSQL .= $strConnector . "intConditionInstanceID = ".$this->getDB()->sanitize(self::getConditionInstanceID());
		$strConnector = ",";
		if(isset($this->_intProcessTransitionInstanceID)) {
			$strSQL .= $strConnector . "intProcessTransitionInstanceID = ".$this->getDB()->sanitize(self::getProcessTransitionInstanceID());
			$strConnector = ",";
		}
		if(isset($this->_intConditionID)) {
			$strSQL .= $strConnector . "intConditionID = ".$this->getDB()->sanitize(self::getConditionID());
			$strConnector = ",";
		}
		if(isset($this->_intRoleID)) {
			$strSQL .= $strConnector . "intRoleID = ".$this->getDB()->sanitize(self::getRoleID());
			$strConnector = ",";
		}
		if(isset($this->_intUserID)) {
			$strSQL .= $strConnector . "intUserID = ".$this->getDB()->sanitize(self::getUserID());
			$strConnector = ",";
		}
		if(isset($this->_dtmCompletedDate)) {
			$strSQL .= $strConnector . "dtmCompletedDate = ".$this->getDB()->sanitize(self::getCompletedDate());
			$strConnector = ",";
		}
		if(isset($this->_txtComment)) {
			$strSQL .= $strConnector . "txtComment = ".$this->getDB()->sanitize(self::getComment());
			$strConnector = ",";
		}
		$strSQL .= " WHERE intConditionInstanceID = ".$this->getDB()->sanitize(self::getConditionInstanceID())."";
		//echo $strSQL;
		return $this->getDB()->query($strSQL);
	}

	public function save() {
		if($this->_intConditionInstanceID) {
			return self::update();
		} else {
			return self::insert();
		}
	}

	public function delete() {
		if($this->_intConditionInstanceID) {
			$strSQL = "DELETE FROM dbSystem.tblConditionInstance
				WHERE intConditionInstanceID = '$this->_intConditionInstanceID'
				";
			return $this->getDB()->query($strSQL);
		}
	}

	public function load($intConditionInstanceID) {
		if(!$intConditionInstanceID) {
			return false;
		}

		$strSQL = "SELECT *
				FROM dbSystem.tblConditionInstance
				WHERE intConditionInstanceID = ".self::getDB()->sanitize($intConditionInstanceID)."
				LIMIT 1
			";
		$rsResult = $this->getDB()->query($strSQL);
		$arrRow = $this->getDB()->fetch_assoc($rsResult);
		$this->setVarsFromRow($arrRow);
	}

	function setVarsFromRow($arrRow) {
		if(isset($arrRow["intConditionInstanceID"])) $this->_intConditionInstanceID = $arrRow["intConditionInstanceID"];
		if(isset($arrRow["intProcessTransitionInstanceID"])) $this->_intProcessTransitionInstanceID = $arrRow["intProcessTransitionInstanceID"];
		if(isset($arrRow["intConditionID"])) $this->_intConditionID = $arrRow["intConditionID"];
		if(isset($arrRow["intRoleID"])) $this->_intRoleID = $arrRow["intRoleID"];
		if(isset($arrRow["intUserID"])) $this->_intUserID = $arrRow["intUserID"];
		if(isset($arrRow["dtmCompletedDate"])) $this->_dtmCompletedDate = $arrRow["dtmCompletedDate"];
		if(isset($arrRow["txtComment"])) $this->_txtComment = $arrRow["txtComment"];
	}

	function getConditionInstanceID() {
		return $this->_intConditionInstanceID;
	}
	function setConditionInstanceID($value) {
		if($this->_intConditionInstanceID !== $value) {
			$this->_intConditionInstanceID = $value;
			$this->_blnDirty = true;
		}
	}

	function getProcessTransitionInstanceID() {
		return $this->_intProcessTransitionInstanceID;
	}
	function setProcessTransitionInstanceID($value) {
		if($this->_intProcessTransitionInstanceID !== $value) {
			$this->_intProcessTransitionInstanceID = $value;
			$this->_blnDirty = true;
		}

	}

	function getConditionID() {
		return $this->_intConditionID;
	}
	function setConditionID($value) {
		if($this->_intConditionID !== $value) {
			$this->_intConditionID = $value;
			$this->_blnDirty = true;
		}
	}

	function getRoleID() {
		return $this->_intRoleID;
	}
	function setRoleID($value) {
		if($this->_intRoleID !== $value) {
			$this->_intRoleID = $value;
			$this->_blnDirty = true;
		}
	}

	function getUserID() {
		return $this->_intUserID;
	}
	function setUserID($value) {
		if($this->_intUserID !== $value) {
			$this->_intUserID = $value;
			$this->_blnDirty = true;
		}
	}

	function getCompletedDate() {
		return $this->_dtmCompletedDate;
	}

	function setCompletedDate($value) {
		if($this->_dtmCompletedDate !== $value) {
			$this->_dtmCompletedDate = $value;
			$this->_blnDirty = true;
		}
	}

	function getComment() {
		return $this->_txtComment;
	}
	function setComment($value) {
		if($this->_txtComment !== $value) {
			$this->_txtComment = $value;
			$this->_blnDirty = true;
		}
	}
}

class ConditionInstance extends ConditionInstanceBase {
	protected $_objRole;
	protected $_objUser;
	protected $_objProcessTransitionInstance;

	function __construct($intConditionInstanceID=null) {
		parent::__construct($intConditionInstanceID);
	}

	function loadByProcessTransitionInstanceIDAndRoleIDAndUserID($intProcessTransitionInstanceID, $intRoleID, $intUserID) {
		if(!$intProcessTransitionInstanceID || !$intRoleID || !$intUserID) {
			return false;
		}

		$strSQL = "SELECT *
				FROM dbSystem.tblConditionInstance
				WHERE intProcessTransitionInstanceID = ".self::getDB()->sanitize($intProcessTransitionInstanceID)."
				AND intRoleiD = ".self::getDB()->sanitize($intRoleID)."
				AND intUserID = ".self::getDB()->sanitize($intUserID)."
				LIMIT 1
			";
		$rsResult = $this->getDB()->query($strSQL);
		$arrRow = $this->getDB()->fetch_assoc($rsResult);
		$this->setVarsFromRow($arrRow);
	}

	function getRole() {
		include_once("Role.class.php");
		if(!$this->_objRole) {
			$this->_objRole = new Role();
		}
		return $this->_objRole;
	}
	function getUser() {
		include_once("User.class.php");
		if(!$this->_objUser) {
			$this->_objUser = new User();
		}
		return $this->_objUser;
	}
	function getProcessTransitionInstance() {
		include_once("ProcessTransitionInstance.class.php");
		if(!$this->_objProcessTransitionInstance) {
			$this->_objProcessTransitionInstance = new ProcessTransitionInstance();
		}
		return $this->_objProcessTransitionInstance;
	}
}
?>