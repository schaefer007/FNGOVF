<?php
	require_once("ArrayClass.class.php");

	class OperationArray extends ArrayClass {
		function __construct(){
		}

		function load($objSearch=null) {
			$strSQL = " SELECT * FROM tblOperation";
			if($objSearch && $objSearch->getSortBy())
				$strSQL .= " ORDER BY " . $objSearch->getSortBy() . " ";
			if($objSearch && $objSearch->getSortOrder())
				$strSQL .= $objSearch->getSortOrder();
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				$this->_arrObjects[$arrRow["intOperationID"]] = new Operation();
				$this->_arrObjects[$arrRow["intOperationID"]]->setVarsFromRow($arrRow);
			}
		}

		function loadForPermissionManagementBySecurityItem($intSecurityItemID) {
			$strSQL = " SELECT tblOperation.*
				FROM tblOperation
				INNER JOIN dbPLM.tblPermission
					ON tblPermission.intOperationID = tblOperation.intOperationID
				WHERE intSecurityItemID = ".self::getDB()->sanitize($intSecurityItemID);
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				$this->_arrObjects[$arrRow["intOperationID"]] = new Operation();
				$this->_arrObjects[$arrRow["intOperationID"]]->setVarsFromRow($arrRow);
			}
		}
	}

	require_once("DataClass.class.php");

	class OperationBase extends DataClass {
		protected $_intOperationID;
		protected $_strOperationName;

		function __construct($intOperationID=null) {
			$this->DataClass();
			if($intOperationID) {
				$this->load($intOperationID);
			}
		}

		protected function insert() {
			base::write_log("Operation created","S");
			$strSQL = "INSERT INTO tblOperation SET ";
			$strConnector = "";
			$strSQL .= $strConnector . "intOperationID = ".$this->getDB()->sanitize(self::getOperationID());
			$strConnector = ",";
			if(isset($this->_strOperationName)) {
				$strSQL .= $strConnector . "strOperationName = ".$this->getDB()->sanitize(self::getOperationName());
				$strConnector = ",";
			}
			//echo $strSQL;
			$this->getDB()->query($strSQL);
			$this->setOperationID($this->getDB()->insert_id());
			return $this->getOperationID();
		}

		protected function update() {
			base::write_log("Operation Updated","S");
			$strSQL = "UPDATE tblOperation SET ";
			$strConnector = "";
			$strSQL .= $strConnector . "intOperationID = ".$this->getDB()->sanitize(self::getOperationID());
			$strConnector = ",";
			if(isset($this->_strOperationName)) {
				$strSQL .= $strConnector . "strOperationName = ".$this->getDB()->sanitize(self::getOperationName());
				$strConnector = ",";
			}
			$strSQL .= " WHERE intOperationID = ".$this->getDB()->sanitize(self::getOperationID())."";
			//echo $strSQL;
			return $this->getDB()->query($strSQL);
		}

		public function save() {
			if($this->_intOperationID) {
				return $this->update();
			} else {
				return $this->insert();
			}
		}

		public function delete() {
			if($this->_intOperationID) {
				$strSQL = "DELETE FROM tblOperation
				WHERE intOperationID = '$this->_intOperationID'
				";
				return $this->getDB()->query($strSQL);
			}
		}

		public function load($intOperationID) {
			if(!$intOperationID) {
				return false;
			}

			$strSQL = "SELECT *
				FROM tblOperation
				WHERE intOperationID = '$intOperationID'
				LIMIT 1
			";
			$rsOperation = $this->getDB()->query($strSQL);
			$arrOperation = $this->getDB()->fetch_assoc($rsOperation);
			$this->setVarsFromRow($arrOperation);
		}

		function setVarsFromRow($arrOperation) {
			if(isset($arrOperation["intOperationID"])) $this->_intOperationID = $arrOperation["intOperationID"];
			if(isset($arrOperation["strOperationName"])) $this->_strOperationName = $arrOperation["strOperationName"];
		}

		function getOperationID() {
			return $this->_intOperationID;
		}
		function setOperationID($value) {
			if($this->_intOperationID !== $value) {
				$this->_intOperationID = $value;
				$this->_blnDirty = true;
			}
		}

		function getOperationName() {
			return $this->_strOperationName;
		}
		function setOperationName($value) {
			if($this->_strOperationName !== $value) {
				$this->_strOperationName = $value;
				$this->_blnDirty = true;
			}
		}

	}

	class Operation extends OperationBase {
		function __construct($intOperationID=null) {
			parent::__construct($intOperationID);
		}
	}
?>
