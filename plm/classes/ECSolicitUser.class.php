<?php
require_once("ArrayClass.class.php");

class ECSolicitUserArray extends ArrayClass {
	function __construct(){
		parent::__construct("ECSolicitUser");
	}

	function load() {
		$strSQL = " SELECT * FROM dbEngineeringChange.tblECSolicitUser";
		$rsResult = $this->getDB()->query($strSQL);
		while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
			$this->_arrObjects[$arrRow["intECSolicitUserID"]] = new ECSolicitUser();
			$this->_arrObjects[$arrRow["intECSolicitUserID"]]->setVarsFromRow($arrRow);
		}
	}

	function loadByEngineeringChangeID($intEngineeringChangeID) {
		$strSQL = " SELECT tblRole.*, tblUser.*, tblECSolicitUser.*
			FROM dbEngineeringChange.tblECSolicitUser
			INNER JOIN dbPLM.tblUser
				ON tblUser.intUserID = tblECSolicitUser.intUserID
			LEFT JOIN dbPLM.tblRole
				ON tblRole.intRoleID = tblECSolicitUser.intRoleID
			WHERE intEngineeringChangeID = ".self::getDB()->sanitize($intEngineeringChangeID)."
		";
		$rsResult = $this->getDB()->query($strSQL);
		while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
			$this->_arrObjects[$arrRow["intECSolicitUserID"]] = new ECSolicitUser();
			$this->_arrObjects[$arrRow["intECSolicitUserID"]]->setVarsFromRow($arrRow);
			$this->_arrObjects[$arrRow["intECSolicitUserID"]]->getUser()->setVarsFromRow($arrRow);
			$this->_arrObjects[$arrRow["intECSolicitUserID"]]->getRole()->setVarsFromRow($arrRow);
		}
	}
}

require_once("DataClass.class.php");

class ECSolicitUserBase extends DataClass {
	protected $_intECSolicitUserID;
	protected $_intEngineeringChangeID;
	protected $_intUserID;
	protected $_intRoleID;

	function __construct($intECSolicitUserID=null) {
		$this->DataClass();
		if($intECSolicitUserID) {
			$this->load($intECSolicitUserID);
		}
	}

	protected function insert() {
		base::write_log("ECSoliticUser created","S");
		$strSQL = "INSERT INTO dbEngineeringChange.tblECSolicitUser SET ";
		$strConnector = "";
		$strSQL .= $strConnector . "intECSolicitUserID = ".$this->getDB()->sanitize(self::getECSolicitUserID());
		$strConnector = ",";
		if(isset($this->_intEngineeringChangeID)) {
			$strSQL .= $strConnector . "intEngineeringChangeID = ".$this->getDB()->sanitize(self::getEngineeringChangeID());
			$strConnector = ",";
		}
		if(isset($this->_intUserID)) {
			$strSQL .= $strConnector . "intUserID = ".$this->getDB()->sanitize(self::getUserID());
			$strConnector = ",";
		}
		if(isset($this->_intRoleID)) {
			$strSQL .= $strConnector . "intRoleID = ".$this->getDB()->sanitize(self::getRoleID());
			$strConnector = ",";
		}
		//echo $strSQL;
		$this->getDB()->query($strSQL);
		$this->setECSolicitUserID($this->getDB()->insert_id());
		return $this->getECSolicitUserID();
	}

	protected function update() {
		base::write_log("ECSolicitUser Updated","S");
		$strSQL = "UPDATE dbEngineeringChange.tblECSolicitUser SET ";
		$strConnector = "";
		$strSQL .= $strConnector . "intECSolicitUserID = ".$this->getDB()->sanitize(self::getECSolicitUserID());
		$strConnector = ",";
		if(isset($this->_intEngineeringChangeID)) {
			$strSQL .= $strConnector . "intEngineeringChangeID = ".$this->getDB()->sanitize(self::getEngineeringChangeID());
			$strConnector = ",";
		}
		if(isset($this->_intUserID)) {
			$strSQL .= $strConnector . "intUserID = ".$this->getDB()->sanitize(self::getUserID());
			$strConnector = ",";
		}
		if(isset($this->_intRoleID)) {
			$strSQL .= $strConnector . "intRoleID = ".$this->getDB()->sanitize(self::getRoleID());
			$strConnector = ",";
		}
		$strSQL .= " WHERE intECSolicitUserID = ".$this->getDB()->sanitize(self::getECSolicitUserID())."";
		//echo $strSQL;
		return $this->getDB()->query($strSQL);
	}

	public function save() {
		if($this->_intECSolicitUserID) {
			return self::update();
		} else {
			return self::insert();
		}
	}

	public function delete() {
		base::write_log("ECSolicitUser deleted");
		if($this->_intECSolicitUserID) {
			$strSQL = "DELETE FROM dbEngineeringChange.tblECSolicitUser
				WHERE intECSolicitUserID = '$this->_intECSolicitUserID'
				";
			return $this->getDB()->query($strSQL);
		}
	}

	public function load($intECSolicitUserID) {
		if(!$intECSolicitUserID) {
			return false;
		}

		$strSQL = "SELECT *
				FROM dbEngineeringChange.tblECSolicitUser
				WHERE intECSolicitUserID = ".self::getDB()->sanitize($intECSolicitUserID)."
				LIMIT 1
			";
		$rsResult = $this->getDB()->query($strSQL);
		$arrRow = $this->getDB()->fetch_assoc($rsResult);
		$this->setVarsFromRow($arrRow);
	}

	function setVarsFromRow($arrRow) {
		if(isset($arrRow["intECSolicitUserID"])) $this->_intECSolicitUserID = $arrRow["intECSolicitUserID"];
		if(isset($arrRow["intEngineeringChangeID"])) $this->_intEngineeringChangeID = $arrRow["intEngineeringChangeID"];
		if(isset($arrRow["intUserID"])) $this->_intUserID = $arrRow["intUserID"];
		if(isset($arrRow["intRoleID"])) $this->_intRoleID = $arrRow["intRoleID"];
	}

	function getECSolicitUserID() {
		return $this->_intECSolicitUserID;
	}
	function setECSolicitUserID($value) {
		if($this->_intECSolicitUserID !== $value) {
			$this->_intECSolicitUserID = $value;
			$this->_blnDirty = true;
		}
	}
	function getEngineeringChangeID() {
		return $this->_intEngineeringChangeID;
	}
	function setEngineeringChangeID($value) {
		if($this->_intEngineeringChangeID !== $value) {
			$this->_intEngineeringChangeID = $value;
			$this->_blnDirty = true;
		}
	}
	function getUserID() {
		return $this->_intUserID;
	}
	function setUserID($value) {
		if($this->_intUserID !== $value) {
			$this->_intUserID = $value;
			$this->_blnDirty = true;
		}
	}
	function getRoleID() {
		return $this->_intRoleID;
	}
	function setRoleID($value) {
		if($this->_intRoleID !== $value) {
			$this->_intRoleID = $value;
			$this->_blnDirty = true;
		}
	}
}

class ECSolicitUser extends ECSolicitUserBase {
	protected $_objUser;
	protected $_objRole;

	function __construct($intECSolicitUserID=null) {
		parent::__construct($intECSolicitUserID);
	}

	function loadByEngineeringChangeIDAndUserID($intEngineeringChangeID, $intUserID) {
		if(!$intEngineeringChangeID || !$intUserID) {
			return false;
		}

		$strSQL = "SELECT *
				FROM dbEngineeringChange.tblECSolicitUser
				WHERE intEngineeringChangeID = ".self::getDB()->sanitize($intEngineeringChangeID)."
				AND intUserID = ".self::getDB()->sanitize($intUserID)."
				LIMIT 1
			";
		$rsResult = $this->getDB()->query($strSQL);
		$arrRow = $this->getDB()->fetch_assoc($rsResult);
		$this->setVarsFromRow($arrRow);
	}

	function getUser() {
		include_once("User.class.php");
		if(!$this->_objUser) {
			$this->_objUser = new User();
		}
		return $this->_objUser;
	}
	function getRole() {
		include_once("Role.class.php");
		if(!$this->_objRole) {
			$this->_objRole = new Role();
		}
		return $this->_objRole;
	}
}
?>