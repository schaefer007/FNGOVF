<?php
require_once("ArrayClass.class.php");

class ProcessTransitionInstanceArray extends ArrayClass {
	function __construct(){
		parent::__construct("ProcessTransitionInstance");
	}

	function load() {
		$strSQL = " SELECT * FROM dbSystem.tblProcessTransitionInstance";
		$rsResult = $this->getDB()->query($strSQL);
		while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
			$this->_arrObjects[$arrRow["intProcessTransitionInstanceID"]] = new ProcessTransitionInstance();
			$this->_arrObjects[$arrRow["intProcessTransitionInstanceID"]]->setVarsFromRow($arrRow);
		}
	}
}

require_once("DataClass.class.php");

class ProcessTransitionInstanceBase extends DataClass {
	protected $_intProcessTransitionInstanceID;
	protected $_intProcessInstanceID;
	protected $_intProcessTransitionID;
	protected $_dtmTransitionCompleted;
	protected $_intNotificationID;

	function __construct($intProcessTransitionInstanceID=null) {
		$this->DataClass();
		if($intProcessTransitionInstanceID) {
			$this->load($intProcessTransitionInstanceID);
		}
	}

	protected function insert() {
		base::write_log("Process transition instance created","S");
		$strSQL = "INSERT INTO dbSystem.tblProcessTransitionInstance SET ";
		$strConnector = "";
		$strSQL .= $strConnector . "intProcessTransitionInstanceID = ".$this->getDB()->sanitize(self::getProcessTransitionInstanceID());
		$strConnector = ",";
		if(isset($this->_intProcessInstanceID)) {
			$strSQL .= $strConnector . "intProcessInstanceID = ".$this->getDB()->sanitize(self::getProcessInstanceID());
			$strConnector = ",";
		}
		if(isset($this->_intProcessTransitionID)) {
			$strSQL .= $strConnector . "intProcessTransitionID = ".$this->getDB()->sanitize(self::getProcessTransitionID());
			$strConnector = ",";
		}
		if(isset($this->_dtmTransitionCompleted)) {
			$strSQL .= $strConnector . "dtmTransitionCompleted = ".$this->getDB()->sanitize(self::getTransitionCompleted());
			$strConnector = ",";
		}
		if(isset($this->_intNotificationID)) {
			$strSQL .= $strConnector . "intNotificationID = ".$this->getDB()->sanitize(self::getNotificationID());
			$strConnector = ",";
		}
		//echo $strSQL;
		$this->getDB()->query($strSQL);
		$this->setProcessTransitionInstanceID($this->getDB()->insert_id());
		return $this->getProcessTransitionInstanceID();
	}

	protected function update() {
		base::write_log("Process transition instance update","S");
		$strSQL = "UPDATE dbSystem.tblProcessTransitionInstance SET ";
		$strConnector = "";
		$strSQL .= $strConnector . "intProcessTransitionInstanceID = ".$this->getDB()->sanitize(self::getProcessTransitionInstanceID());
		$strConnector = ",";
		if(isset($this->_intProcessInstanceID)) {
			$strSQL .= $strConnector . "intProcessInstanceID = ".$this->getDB()->sanitize(self::getProcessInstanceID());
			$strConnector = ",";
		}
		if(isset($this->_intProcessTransitionID)) {
			$strSQL .= $strConnector . "intProcessTransitionID = ".$this->getDB()->sanitize(self::getProcessTransitionID());
			$strConnector = ",";
		}
		if(isset($this->_dtmTransitionCompleted)) {
			$strSQL .= $strConnector . "dtmTransitionCompleted = ".$this->getDB()->sanitize(self::getTransitionCompleted());
			$strConnector = ",";
		}
		if(isset($this->_intNotificationID)) {
			$strSQL .= $strConnector . "intNotificationID = ".$this->getDB()->sanitize(self::getNotificationID());
			$strConnector = ",";
		}
		$strSQL .= " WHERE intProcessTransitionInstanceID = ".$this->getDB()->sanitize(self::getProcessTransitionInstanceID())."";
		//echo $strSQL;
		return $this->getDB()->query($strSQL);
	}

	public function save() {
		if($this->_intProcessTransitionInstanceID) {
			return self::update();
		} else {
			return self::insert();
		}
	}

	public function delete() {
		if($this->_intProcessTransitionInstanceID) {
			base::write_log("Process transition instance delete","S");
			$strSQL = "DELETE FROM dbSystem.tblProcessTransitionInstance
				WHERE intProcessTransitionInstanceID = '$this->_intProcessTransitionInstanceID'
				";
			return $this->getDB()->query($strSQL);
		}
	}

	public function load($intProcessTransitionInstanceID) {
		if(!$intProcessTransitionInstanceID) {
			return false;
		}

		$strSQL = "SELECT *
				FROM dbSystem.tblProcessTransitionInstance
				WHERE intProcessTransitionInstanceID = ".self::getDB()->sanitize($intProcessTransitionInstanceID)."
				LIMIT 1
			";
		$rsResult = $this->getDB()->query($strSQL);
		$arrRow = $this->getDB()->fetch_assoc($rsResult);
		$this->setVarsFromRow($arrRow);
	}

	function setVarsFromRow($arrRow) {
		if(isset($arrRow["intProcessTransitionInstanceID"])) $this->_intProcessTransitionInstanceID = $arrRow["intProcessTransitionInstanceID"];
		if(isset($arrRow["intProcessInstanceID"])) $this->_intProcessInstanceID = $arrRow["intProcessInstanceID"];
		if(isset($arrRow["intProcessTransitionID"])) $this->_intProcessTransitionID = $arrRow["intProcessTransitionID"];
		if(isset($arrRow["dtmTransitionCompleted"])) $this->_dtmTransitionCompleted = $arrRow["dtmTransitionCompleted"];
		if(isset($arrRow["intNotificationID"])) $this->_intNotificationID = $arrRow["intNotificationID"];
	}

	function getProcessTransitionInstanceID() {
		return $this->_intProcessTransitionInstanceID;
	}
	function setProcessTransitionInstanceID($value) {
		if($this->_intProcessTransitionInstanceID !== $value) {
			$this->_intProcessTransitionInstanceID = $value;
			$this->_blnDirty = true;
		}
	}

	function getProcessInstanceID() {
		return $this->_intProcessInstanceID;
	}
	function setProcessInstanceID($value) {
		if($this->_intProcessInstanceID !== $value) {
			$this->_intProcessInstanceID = $value;
			$this->_blnDirty = true;
		}
	}

	function getProcessTransitionID() {
		return $this->_intProcessTransitionID;
	}
	function setProcessTransitionID($value) {
		if($this->_intProcessTransitionID !== $value) {
			$this->_intProcessTransitionID = $value;
			$this->_blnDirty = true;
		}
	}

	function getTransitionCompleted() {
		return $this->_dtmTransitionCompleted;
	}
	function setTransitionCompleted($value) {
		if($this->_dtmTransitionCompleted !== $value) {
			$this->_dtmTransitionCompleted = $value;
			$this->_blnDirty = true;
		}
	}

	function getNotificationID() {
		return $this->_intNotificationID;
	}
	function setNotificationID($value) {
		if($this->_intNotificationID !== $value) {
			$this->_intNotificationID = $value;
			$this->_blnDirty = true;
		}
	}

}

class ProcessTransitionInstance extends ProcessTransitionInstanceBase {
	protected $_objProcessTransition;
	protected $_objConditionArray;
	protected $_objConditionInstanceArray;

	function __construct($intProcessTransitionInstanceID=null) {
		parent::__construct($intProcessTransitionInstanceID);
	}

	public function loadCurrentByProcessInstanceIDAndProcessTransitionID($intProcessInstanceID, $intProcessTransitionID) {
		if(!$intProcessInstanceID || !$intProcessTransitionID) {
			return false;
		}

		$strSQL = "SELECT *
			FROM dbSystem.tblProcessTransitionInstance
			WHERE intProcessInstanceID = ".self::getDB()->sanitize($intProcessInstanceID)."
			AND intProcessTransitionID = ".self::getDB()->sanitize($intProcessTransitionID)."
			AND dtmTransitionCompleted IS NULL
			LIMIT 1
		";
		$rsResult = $this->getDB()->query($strSQL);
		$arrRow = $this->getDB()->fetch_assoc($rsResult);
		$this->setVarsFromRow($arrRow);
	}

	function loadLastByProcessInstanceIDAndProcessTransitionID($intProcessInstanceID, $intProcessTransitionID) {
		if(!$intProcessInstanceID || !$intProcessTransitionID) {
			return false;
		}

		$strSQL = "SELECT *
			FROM dbSystem.tblProcessTransitionInstance
			WHERE intProcessInstanceID = ".self::getDB()->sanitize($intProcessInstanceID)."
			AND intProcessTransitionID = ".self::getDB()->sanitize($intProcessTransitionID)."
			AND dtmTransitionCompleted IS NOT NULL
			ORDER BY dtmTransitionCompleted DESC
			LIMIT 1
		";
		$rsResult = $this->getDB()->query($strSQL);
		$arrRow = $this->getDB()->fetch_assoc($rsResult);
		$this->setVarsFromRow($arrRow);
	}

	function loadLastByProcessInstanceID($intProcessInstanceID) {
		if(!$intProcessInstanceID) {
			return false;
		}
	
		$strSQL = "SELECT *
			FROM dbSystem.tblProcessTransitionInstance
			WHERE intProcessInstanceID = ".self::getDB()->sanitize($intProcessInstanceID)."
			AND dtmTransitionCompleted IS NOT NULL
			ORDER BY dtmTransitionCompleted DESC
			LIMIT 1
		";
		$rsResult = $this->getDB()->query($strSQL);
		$arrRow = $this->getDB()->fetch_assoc($rsResult);
		$this->setVarsFromRow($arrRow);
	}
	static function clearIncompleteProcessTransitions($intProcessInstanceID) {
		if(!$intProcessInstanceID)
			return;

		$strSQL = "DELETE FROM dbSystem.tblProcessTransitionInstance
			WHERE intProcessInstanceID = ".self::getDB()->sanitize($intProcessInstanceID)."
			AND dtmTransitionCompleted IS NULL
		";
		//echo $strSQL;
		self::getDB()->query($strSQL);
	}

	function getProcessTransition() {
		include_once("ProcessTransition.class.php");
		if(!isset($this->_objProcessTransition)) {
			$this->_objProcessTransition = new ProcessTransition();
		}
		return $this->_objProcessTransition;
	}

	function getConditionArray() {
		include_once("Condition.class.php");
		if(!isset($this->_objConditionArray)) {
			$this->_objConditionArray = new ConditionArray();
		}
		return $this->_objConditionArray;
	}
	function getConditionInstanceArray() {
		include_once("ConditionInstance.class.php");
		if(!isset($this->_objConditionInstanceArray)) {
			$this->_objConditionInstanceArray = new ConditionInstanceArray();
		}
		return $this->_objConditionInstanceArray;
	}
}
?>
