<?php
	require_once("ArrayClass.class.php");

	class FacilityArray extends ArrayClass {
		function __construct(){
			parent::__construct("Facility");
		}

		function load() {
			$strSQL = " SELECT * FROM tblFacility
				ORDER BY strFacilityName ";
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				$this->_arrObjects[$arrRow["intFacilityID"]] = new Facility();
				$this->_arrObjects[$arrRow["intFacilityID"]]->setVarsFromRow($arrRow);
			}
		}

		function loadForShipToFacilityManagement() {
			$strSQL = " SELECT tblFacility.*
				FROM tblFacility
				WHERE blnCustomer
				ORDER BY strFacilityName ";
			//echo $strSQL;
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				$this->_arrObjects[$arrRow["intFacilityID"]] = new Facility();
				$this->_arrObjects[$arrRow["intFacilityID"]]->setVarsFromRow($arrRow);
			}

			$this->loadUsedCounts();
		}

		function loadForProductionFacilityManagement() {
			$strSQL = " SELECT tblFacility.*
				FROM tblFacility
				WHERE blnOurFacility
				ORDER BY strFacilityName ";
			//echo $strSQL;
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				$this->_arrObjects[$arrRow["intFacilityID"]] = new Facility();
				$this->_arrObjects[$arrRow["intFacilityID"]]->setVarsFromRow($arrRow);
			}

			$this->loadUsedCounts();
			$this->loadUserCounts();
		}

		function loadForToolManagement(){
			$objSearch = $_SESSION["objSearch"];
			$strSQL = ToolArray::getToolListPageQuery($objSearch, "Facility");
			//echo $strSQL;

			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				$this->_arrObjects[$arrRow["intFacilityID"]] = new Facility();
				$this->_arrObjects[$arrRow["intFacilityID"]]->setVarsFromRow($arrRow);
			}

			$strSearchValue = $objSearch->getFacilityID();
			if($strSearchValue && !isset($this->_arrObjects[$strSearchValue])) { // Search record not in options, add search record
				$this->createNew($strSearchValue)->load($strSearchValue);
			}
		}

		function loadForPartManagement(){
			$objSearch = $_SESSION["objSearch"];
			$strSQL = PartArray::getPartListPageQuery($objSearch, "Facility");
			//echo $strSQL;

			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				$this->_arrObjects[$arrRow["intFacilityID"]] = new Facility();
				$this->_arrObjects[$arrRow["intFacilityID"]]->setVarsFromRow($arrRow);
			}

			$strSearchValue = $objSearch->getFacilityID();
			if($strSearchValue && !isset($this->_arrObjects[$strSearchValue])) { // Search record not in options, add search record
				$this->createNew($strSearchValue)->load($strSearchValue);
			}
		}

		function loadForECManagement(){
			$objSearch = $_SESSION["objSearch"];
			$strSQL = EngineeringChangeArray::getECListPageQuery($objSearch, "Facility");
			//echo $strSQL;

			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				$this->_arrObjects[$arrRow["intFacilityID"]] = new Facility();
				$this->_arrObjects[$arrRow["intFacilityID"]]->setVarsFromRow($arrRow);
			}

			$strSearchValue = $objSearch->getFacilityID();
			if($strSearchValue && !isset($this->_arrObjects[$strSearchValue])) { // Search record not in options, add search record
				$this->createNew($strSearchValue)->load($strSearchValue);
			}
		}

		function loadByOurFacility() {
			$strSQL = " SELECT * FROM tblFacility
				WHERE blnOurFacility
				ORDER BY strFacilityName ";
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				$this->_arrObjects[$arrRow["intFacilityID"]] = new Facility();
				$this->_arrObjects[$arrRow["intFacilityID"]]->setVarsFromRow($arrRow);
			}
		}

		function loadByMachineTypeID($intMachineTypeID) {
			if(!$intMachineTypeID)
				return;

			$strSQL = " SELECT tblFacility.*
				FROM tblFacility
				INNER JOIN dbPLM.tblProductionFacilityMachineType
					ON tblProductionFacilityMachineType.intProductionFacilityID = tblFacility.intFacilityID
				WHERE blnOurFacility
				AND tblProductionFacilityMachineType.intMachineTypeID = ".self::getDB()->sanitize($intMachineTypeID)."
				ORDER BY strFacilityName ";
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				$this->_arrObjects[$arrRow["intFacilityID"]] = new Facility();
				$this->_arrObjects[$arrRow["intFacilityID"]]->setVarsFromRow($arrRow);
			}
		}

		function loadByProductType($objProductType) {
			$strSQL = " SELECT * FROM tblFacility
				WHERE 1 ";
			if($objProductType->getSource() == "Manufactured") {
				$strSQL .= " AND blnOurFacility ";
			}
			$strSQL .= " ORDER BY strFacilityName ";
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				$this->_arrObjects[$arrRow["intFacilityID"]] = new Facility();
				$this->_arrObjects[$arrRow["intFacilityID"]]->setVarsFromRow($arrRow);
			}
		}

		function loadByCustomerID($intCustomerID) {
			if(!$intCustomerID)
				return false;

			$strSQL = " SELECT * FROM tblFacility
				LEFT JOIN dbPLM.tblOrganization
					ON tblOrganization.intOrganizationID = tblFacility.intOrganizationID
				LEFT JOIN dbPLM.tblCustomer
					ON tblCustomer.intOrganizationID = tblOrganization.intOrganizationID
				WHERE (intCustomerID = ".self::getDB()->sanitize($intCustomerID)." OR blnOurFacility)
				AND tblFacility.blnCustomer
				ORDER BY strFacilityName";
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				$this->_arrObjects[$arrRow["intFacilityID"]] = new Facility();
				$this->_arrObjects[$arrRow["intFacilityID"]]->setVarsFromRow($arrRow);
			}
		}

		function loadShipToFacilitiesByCustomerID($intCustomerID) {
			if(!$intCustomerID)
				return false;

			$strSQL = " SELECT * FROM tblFacility
				LEFT JOIN dbPLM.tblOrganization
					ON tblOrganization.intOrganizationID = tblFacility.intOrganizationID
				LEFT JOIN dbPLM.tblCustomer
					ON tblCustomer.intOrganizationID = tblOrganization.intOrganizationID
				WHERE intCustomerID = ".self::getDB()->sanitize($intCustomerID)."
				AND tblFacility.blnCustomer
				ORDER BY strFacilityName";
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				$this->_arrObjects[$arrRow["intFacilityID"]] = new Facility();
				$this->_arrObjects[$arrRow["intFacilityID"]]->setVarsFromRow($arrRow);
			}
		}

		function loadForProgramSearch() {
			$strSQL = ProductArray::getProgramListPageQuery("Facility");
			//echo $strSQL;
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				$this->_arrObjects[$arrRow["intFacilityID"]] = new Facility();
				$this->_arrObjects[$arrRow["intFacilityID"]]->setVarsFromRow($arrRow);
			}
		}

		function loadByIDs($arrIDs){
			if(!$arrIDs)
				return;

			$strSQL = " SELECT * FROM tblFacility
				WHERE intFacilityID IN ('".implode("','", $arrIDs)."')";
			//echo $strSQL;
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				$this->_arrObjects[$arrRow["intFacilityID"]] = new Facility();
				$this->_arrObjects[$arrRow["intFacilityID"]]->setVarsFromRow($arrRow);
			}
		}

		function getCapacityPlanningListPageQuery($objSearch, $strFilter=null, $intMachineTypeID=null) {
			$strTableSQL = $strSelectTableSQL = $strPlatformVolumeSQL = $strWhereSQL = "";
			if($intMachineTypeID == intMACHINE_TYPE_ID_PRESS) {
				$strTableSQL = "
					INNER JOIN dbPLM.tblPress
						ON tblMachine.intMachineID = tblPress.intMachineID ";
			} elseif($intMachineTypeID == intMACHINE_TYPE_ID_PLATING_LINE) {
				$strTableSQL = "
					INNER JOIN dbPLM.tblPlatingLine
						ON tblMachine.intMachineID = tblPlatingLine.intMachineID ";
				$strSelectTableSQL = " intBarsPerHour, ";
			} elseif($intMachineTypeID == intMACHINE_TYPE_ID_PAINTING_LINE) {
				$strTableSQL = "
					INNER JOIN dbPLM.tblPaintingLine
						ON tblMachine.intMachineID = tblPaintingLine.intMachineID ";
				$strSelectTableSQL = " dblRackSpacing, dblLineSpeed, dblAveragePaintChangesPerHour, intNumberOfRounds, ";
			}
			//$strWhereSQL .= " AND tblMachine.intMachineTypeID = ".self::getDB()->sanitize($intMachineTypeID);
			list($strWhereSQL, $strPlatformVolumeSQL) = FacilityArray::getWhereSQL($objSearch, $strFilter, $intMachineTypeID);

			//$strSelectSQL = " tblPlatformPartXR.*, tblPart.*, tblPlatformVolume.*, tblMachineSchedule.*, tblMachine.*, $strSelectTableSQL tblFacility.* ";
			$strSelectSQL = " $strSelectTableSQL intYear, intMonth, intFacilityID, tblMachine.intMachineID, tblPart.intPartID,
				strFacilityName, dblUptime, dblEfficiency, dblHoursPerShift, intShiftsPerDay, intDaysPerWeek,
				intPiecesPerCycle, intCyclesPerHour, intPiecesPerVehicle, dblQuality, dblBuildPercent, dblServicePercent, dblAverageSetupHours, intWeeklySetupFrequency,
				intPiecesPerBar, blnOutsourced, SUM(intVolume) AS intVolume ";
			$strOrderBySQL = $objSearch->getCPByPressListPage()->getSortQuery();
			switch($strFilter) {
				case "Facility":
					$strSelectSQL = "DISTINCT intFacilityID, strFacilityName ";
					$strOrderBySQL = " ORDER BY strFacilityName ";
					break;
				case "Tonnage":
					$strSelectSQL = "DISTINCT intTonnage, intTonnage ";
					$strOrderBySQL = " ORDER BY intTonnage ";
					break;
				case "Press SubType":
					$strSelectSQL = "DISTINCT strPressSubType, strPressSubType ";
					$strOrderBySQL = " ORDER BY strPressSubType ";
					break;
				case "Year":
					$strSelectSQL = "DISTINCT intYear, intYear ";
					$strOrderBySQL = " ORDER BY intYear ";
					break;
			}

			$strSQL = " SELECT $strSelectSQL
				FROM dbPLM.tblFacility
				INNER JOIN dbPLM.tblMachine
					ON tblMachine.intProductionFacilityID = tblFacility.intFacilityID
				$strTableSQL
				LEFT JOIN dbPLM.tblProductionFacilityMachineType
					ON tblProductionFacilityMachineType.intProductionFacilityID = tblFacility.intFacilityID
					AND tblProductionFacilityMachineType.intMachineTypeID = ".self::getDB()->sanitize($intMachineTypeID)."
				LEFT JOIN dbCapacity.tblMachineSchedule
					ON tblMachineSchedule.intMachineID = tblMachine.intMachineID
				LEFT JOIN dbCapacity.tblPart
					ON tblPart.intPartID = tblMachineSchedule.intPartID
				LEFT JOIN dbCapacity.tblPlatformPartXR
					ON tblPlatformPartXR.intPartID = tblPart.intPartID
				LEFT JOIN dbPLM.tblPlatformVolume
					ON tblPlatformVolume.intPlatformID = tblPlatformPartXR.intPlatformID
					$strPlatformVolumeSQL
				WHERE 1 $strWhereSQL
				GROUP BY intYear, intMonth, intFacilityID, tblMachine.intMachineID, tblPart.intPartID
			";
			$strSQL .= $strOrderBySQL;
			//echo $strSQL . "<br /><br />";
			return $strSQL;
		}

		function loadForCapacityPlanning($intMachineTypeID, $strClass, $strFilter) {
			include_once($strClass.".class.php");
			$objSearch = $_SESSION["objSearch"];
			$strSQL = FacilityArray::getCapacityPlanningListPageQuery($objSearch, $strFilter, $intMachineTypeID);
			//echo $strSQL;

			$arrCountedSetupTimes = array();
			$rsResult = $this->getDB()->query($strSQL);
			//echo date("Y-m-d G:i:s") . " A<br />";
			//echo "Num of Rows: " . $this->getDB()->num_rows($rsResult) . "<br />";
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				if(!isset($this->_arrObjects[$arrRow["intFacilityID"]])) {
					$objFacility = $this->addFromRow($arrRow, "intFacilityID");
				}
				if(!($objFacility->getMachineArray()->objectIsSet($arrRow["intMachineID"])) && !$arrRow["blnOutsourced"]) {
					//$objFacility->getMachineArray()->addFromRow($arrRow, "intMachineID");
					$objFacility->getMachineArray()->createNew($arrRow["intMachineID"]);
				}

				if($arrRow["intVolume"]) {
					$blnIncludeSetupTimes = isset($arrCountedSetupTimes[$arrRow["intPartID"]][$arrRow["intMachineID"]][$arrRow["intYear"]][$arrRow["intMonth"]])?0:1;
					$intVolume = calcVolume($arrRow, $intMachineTypeID);
					$dblHoursRequired = calcHoursRequired($arrRow, $intMachineTypeID, $blnIncludeSetupTimes);
					$dblUtilization = calcUtilization($arrRow, $intMachineTypeID, $blnIncludeSetupTimes);

					//$arrMachines[$arrRow["intMachineID"]]->getYearArray()->getObject($arrRow["intYear"], true)->add($dblUtilization, $dblHoursRequired, $intVolume);
					//$arrMachines[$arrRow["intMachineID"]]->getYearArray()->getObject($arrRow["intYear"])->getMonthArray()->getObject($arrRow["intMonth"], true)->add($dblUtilization, $dblHoursRequired, $intVolume);
					//$arrMachines[$arrRow["intMachineID"]]->getPartArray()->getObject($arrRow["intPartID"], true)->getYearArray()->getObject($arrRow["intYear"], true)->add($dblUtilization, $dblHoursRequired, $intVolume);
					//$arrMachines[$arrRow["intMachineID"]]->getPartArray()->getObject($arrRow["intPartID"])->getYearArray()->getObject($arrRow["intYear"])->getMonthArray()->getObject($arrRow["intMonth"], true)->add($dblUtilization, $dblHoursRequired, $intVolume);
					if(!$arrRow["blnOutsourced"]) {
						$this->_arrObjects[$arrRow["intFacilityID"]]->getYearArray()->getObject($arrRow["intYear"], true)->add($dblUtilization, $dblHoursRequired, $intVolume);
						$this->_arrObjects[$arrRow["intFacilityID"]]->getYearArray()->getObject($arrRow["intYear"])->getMonthArray()->getObject($arrRow["intMonth"], true)->add($dblUtilization, $dblHoursRequired, $intVolume);
					}

					$arrCountedSetupTimes[$arrRow["intPartID"]][$arrRow["intMachineID"]][$arrRow["intYear"]][$arrRow["intMonth"]] = 1;
				}
			}
			//echo date("Y-m-d G:i:s") . " B<br />";
		}

		function getWhereSQL($objSearch, $strFilter, $intMachineTypeID) {
			$strWhereSQL = $strPlatformVolumeSQL = "";
			if($objSearch) {
				if($objSearch->getFirstYear() && $strFilter != "Year") {

					$strPlatformVolumeSQL .= " AND intYear >= ".self::getDB()->sanitize($objSearch->getFirstYear());
				}
				if($objSearch->getLastYear() && $strFilter != "Year") {
					$strPlatformVolumeSQL .= " AND intYear <= ".self::getDB()->sanitize($objSearch->getLastYear());
				}
				if($objSearch->getFacilityID() && $strFilter != "Facility") {
					$strWhereSQL .= " AND tblFacility.intFacilityID = ".self::getDB()->sanitize($objSearch->getFacilityID());
				}
				if($objSearch->getTonnage() && $strFilter != "Tonnage" && $intMachineTypeID == intMACHINE_TYPE_ID_PRESS) {
					$strWhereSQL .= " AND tblPress.intTonnage = ".self::getDB()->sanitize($objSearch->getTonnage());
				}
				if($objSearch->getPressSubType() && $strFilter != "Press SubType" && $intMachineTypeID == intMACHINE_TYPE_ID_PRESS) {
					$strWhereSQL .= " AND tblPress.strPressSubType = ".self::getDB()->sanitize($objSearch->getPressSubType());
				}
				if($objSearch->getBedSizeLR() && $intMachineTypeID == intMACHINE_TYPE_ID_PRESS) {
					$strWhereSQL .= " AND tblPress.intBedSizeLR >= ".self::getDB()->sanitize($objSearch->getBedSizeLR());
				}
				if($objSearch->getBedSizeFB() && $intMachineTypeID == intMACHINE_TYPE_ID_PRESS) {
					$strWhereSQL .= " AND tblPress.intBedSizeFB >= ".self::getDB()->sanitize($objSearch->getBedSizeFB());
				}
				if($objSearch->getMaxCoilWidth() && $intMachineTypeID == intMACHINE_TYPE_ID_PRESS) {
					$strWhereSQL .= " AND tblPress.intMaxCoilWidth >= ".self::getDB()->sanitize($objSearch->getMaxCoilWidth());
				}
				if($objSearch->getWindowSizeLR() && $intMachineTypeID == intMACHINE_TYPE_ID_PRESS) {
					$strWhereSQL .= " AND tblPress.intWindowSizeLR >= ".self::getDB()->sanitize($objSearch->getWindowSizeLR());
				}
				if($objSearch->getStrokeLength() && $intMachineTypeID == intMACHINE_TYPE_ID_PRESS) {
					$strWhereSQL .= " AND tblPress.intStrokeLength >= ".self::getDB()->sanitize($objSearch->getStrokeLength());
				}
				if(!$objSearch->getOutsourced()) {
					$strWhereSQL .= " AND !tblMachine.blnOutsourced ";
				} elseif($objSearch->getOutsourced() == "Show Only Outsourced") {
					$strWhereSQL .= " AND tblMachine.blnOutsourced ";
				}
			}
			return array($strWhereSQL, $strPlatformVolumeSQL);
		}

		function loadForCapacityPlanningFilter($intMachineTypeID) {
			$objSearch = $_SESSION["objSearch"];
			$strSQL = FacilityArray::getCapacityPlanningListPageQuery($objSearch, "Facility", $intMachineTypeID);
			//echo $strSQL;

			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				$this->_arrObjects[$arrRow["intFacilityID"]] = new Facility();
				$this->_arrObjects[$arrRow["intFacilityID"]]->setVarsFromRow($arrRow);
			}

			$strSearchValue = $objSearch->getFacilityID();
			if($strSearchValue && !isset($this->_arrObjects[$strSearchValue])) { // Search record not in options, add search record
				$this->createNew($strSearchValue)->load($strSearchValue);
			}
		}

		function loadFilterOptions($mixMethod, $strInclude) {
			if($strInclude) include_once($strInclude);
			$objSearch = $_SESSION["objSearch"];
			$strSQL = call_user_func($mixMethod, $objSearch, "Facility");
			//echo $strSQL;

			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				$this->_arrObjects[$arrRow["intFacilityID"]] = new Facility();
				$this->_arrObjects[$arrRow["intFacilityID"]]->setVarsFromRow($arrRow);
			}

			$strSearchValue = $objSearch->getFacilityID();
			if($strSearchValue && !isset($this->_arrObjects[$strSearchValue])) { // Search record not in options, add search record
				$this->createNew($strSearchValue)->load($strSearchValue);
			}
		}

		function loadUsedCounts() {
			if(!$this->getArray())
				return;

			$arrFacilityIDs = getArrayFromObjectField($this, "getFacilityID");

			$strSQL = " SELECT intProductionFacilityID AS intFacilityID, COUNT(tblProductXR.intProgramXRID) AS intUsedCount
				FROM dbPLM.tblProductXR
				WHERE intProductionFacilityID IN ('".implode("','", $arrFacilityIDs)."')
				GROUP BY 1";
			$rsResult = self::getDB()->query($strSQL);
			while($arrRow = self::getDB()->fetch_assoc($rsResult)) {
				$this->_arrObjects[$arrRow["intFacilityID"]]->setUsedCount($this->_arrObjects[$arrRow["intFacilityID"]]->getUsedCount() + $arrRow["intUsedCount"]);
			}

			$strSQL = " SELECT intShipToFacilityID AS intFacilityID, COUNT(tblProductXR.intProgramXRID) AS intUsedCount
				FROM dbPLM.tblProductXR
				WHERE intShipToFacilityID IN ('".implode("','", $arrFacilityIDs)."')
				GROUP BY 1";
			$rsResult = self::getDB()->query($strSQL);
			while($arrRow = self::getDB()->fetch_assoc($rsResult)) {
				$this->_arrObjects[$arrRow["intFacilityID"]]->setUsedCount($this->_arrObjects[$arrRow["intFacilityID"]]->getUsedCount() + $arrRow["intUsedCount"]);
			}
		}

		function loadUserCounts() {
			if(!$this->getArray())
				return;

			$arrFacilityIDs = getArrayFromObjectField($this, "getFacilityID");

			$strSQL = " SELECT intProductionFacilityID, COUNT(*) AS intUserCount
				FROM dbPLM.tblUser
				WHERE intProductionFacilityID IN ('".implode("','", $arrFacilityIDs)."')
				GROUP BY 1";
			$rsResult = self::getDB()->query($strSQL);
			while($arrRow = self::getDB()->fetch_assoc($rsResult)) {
				$this->_arrObjects[$arrRow["intProductionFacilityID"]]->setUserCount($arrRow["intUserCount"]);
			}
		}
	}

	require_once("DataClass.class.php");

	class FacilityBase extends DataClass {
		protected $_intFacilityID;
		protected $_intOrganizationID;
		protected $_strFacilityNumber;
		protected $_strFacilityName;
		protected $_txtAddress;
		protected $_blnOurFacility;
		protected $_blnCustomer;
		protected $_intCreatedByUserID;
		protected $_intTier;
		protected $_dblHoursPerShift;
		protected $_intShiftsPerDay;
		protected $_intDaysPerWeek;
		protected $_intWeeksPerYear;
		protected $_dtmCreatedOn;
		protected $_dtmModifiedOn;

		function __construct($intFacilityID=null) {
			$this->DataClass();
			if($intFacilityID) {
				$this->load($intFacilityID);
			}
		}

		protected function insert() {
			base::write_log("Facility created","S");
			$strSQL = "INSERT INTO tblFacility SET ";
			$strConnector = "";
			$strSQL .= $strConnector . "intFacilityID = ".$this->getDB()->sanitize(self::getFacilityID());
			$strConnector = ",";
			if(isset($this->_intOrganizationID)) {
				$strSQL .= $strConnector . "intOrganizationID = ".$this->getDB()->sanitize(self::getOrganizationID());
				$strConnector = ",";
			}
			if(isset($this->_strFacilityNumber)) {
				$strSQL .= $strConnector . "strFacilityNumber = ".$this->getDB()->sanitize(self::getFacilityNumber());
				$strConnector = ",";
			}
			if(isset($this->_strFacilityName)) {
				$strSQL .= $strConnector . "strFacilityName = ".$this->getDB()->sanitize(self::getFacilityName());
				$strConnector = ",";
			}
			if(isset($this->_txtAddress)) {
				$strSQL .= $strConnector . "txtAddress = ".$this->getDB()->sanitize(self::getAddress());
				$strConnector = ",";
			}
			if(isset($this->_blnOurFacility)) {
				$strSQL .= $strConnector . "blnOurFacility = ".$this->getDB()->sanitize(self::getOurFacility());
				$strConnector = ",";
			}
			if(isset($this->_blnCustomer)) {
				$strSQL .= $strConnector . "blnCustomer = ".$this->getDB()->sanitize(self::getCustomer());
				$strConnector = ",";
			}
			if(isset($this->_intCreatedByUserID)) {
				$strSQL .= $strConnector . "intCreatedByUserID = ".$this->getDB()->sanitize(self::getCreatedByUserID());
				$strConnector = ",";
			}
			if(isset($this->_intTier)) {
				$strSQL .= $strConnector . "intTier = ".$this->getDB()->sanitize(self::getTier());
				$strConnector = ",";
			}
			if(isset($this->_dblHoursPerShift)) {
				$strSQL .= $strConnector . "dblHoursPerShift = ".$this->getDB()->sanitize(self::getHoursPerShift());
				$strConnector = ",";
			}
			if(isset($this->_intShiftsPerDay)) {
				$strSQL .= $strConnector . "intShiftsPerDay = ".$this->getDB()->sanitize(self::getShiftsPerDay());
				$strConnector = ",";
			}
			if(isset($this->_intDaysPerWeek)) {
				$strSQL .= $strConnector . "intDaysPerWeek = ".$this->getDB()->sanitize(self::getDaysPerWeek());
				$strConnector = ",";
			}
			if(isset($this->_intWeeksPerYear)) {
				$strSQL .= $strConnector . "intWeeksPerYear = ".$this->getDB()->sanitize(self::getWeeksPerYear());
				$strConnector = ",";
			}
			if(isset($this->_dtmCreatedOn)) {
				$strSQL .= $strConnector . "dtmCreatedOn = ".$this->getDB()->sanitize(self::getCreatedOn());
				$strConnector = ",";
			}
			if(isset($this->_dtmModifiedOn)) {
				$strSQL .= $strConnector . "dtmModifiedOn = ".$this->getDB()->sanitize(self::getModifiedOn());
				$strConnector = ",";
			}
			//echo $strSQL;
			$this->getDB()->query($strSQL);
			$this->setFacilityID($this->getDB()->insert_id());
			return $this->getFacilityID();
		}

		protected function update() {
			base::write_log("Facility Update","S");
			$strSQL = "UPDATE tblFacility SET ";
			$strConnector = "";
			$strSQL .= $strConnector . "intFacilityID = ".$this->getDB()->sanitize(self::getFacilityID());
			$strConnector = ",";
			if(isset($this->_intOrganizationID)) {
				$strSQL .= $strConnector . "intOrganizationID = ".$this->getDB()->sanitize(self::getOrganizationID());
				$strConnector = ",";
			}
			if(isset($this->_strFacilityNumber)) {
				$strSQL .= $strConnector . "strFacilityNumber = ".$this->getDB()->sanitize(self::getFacilityNumber());
				$strConnector = ",";
			}
			if(isset($this->_strFacilityName)) {
				$strSQL .= $strConnector . "strFacilityName = ".$this->getDB()->sanitize(self::getFacilityName());
				$strConnector = ",";
			}
			if(isset($this->_txtAddress)) {
				$strSQL .= $strConnector . "txtAddress = ".$this->getDB()->sanitize(self::getAddress());
				$strConnector = ",";
			}
			if(isset($this->_blnOurFacility)) {
				$strSQL .= $strConnector . "blnOurFacility = ".$this->getDB()->sanitize(self::getOurFacility());
				$strConnector = ",";
			}
			if(isset($this->_blnCustomer)) {
				$strSQL .= $strConnector . "blnCustomer = ".$this->getDB()->sanitize(self::getCustomer());
				$strConnector = ",";
			}
			if(isset($this->_intCreatedByUserID)) {
				$strSQL .= $strConnector . "intCreatedByUserID = ".$this->getDB()->sanitize(self::getCreatedByUserID());
				$strConnector = ",";
			}
			if(isset($this->_intTier)) {
				$strSQL .= $strConnector . "intTier = ".$this->getDB()->sanitize(self::getTier());
				$strConnector = ",";
			}
			if(isset($this->_dblHoursPerShift)) {
				$strSQL .= $strConnector . "dblHoursPerShift = ".$this->getDB()->sanitize(self::getHoursPerShift());
				$strConnector = ",";
			}
			if(isset($this->_intShiftsPerDay)) {
				$strSQL .= $strConnector . "intShiftsPerDay = ".$this->getDB()->sanitize(self::getShiftsPerDay());
				$strConnector = ",";
			}
			if(isset($this->_intDaysPerWeek)) {
				$strSQL .= $strConnector . "intDaysPerWeek = ".$this->getDB()->sanitize(self::getDaysPerWeek());
				$strConnector = ",";
			}
			if(isset($this->_intWeeksPerYear)) {
				$strSQL .= $strConnector . "intWeeksPerYear = ".$this->getDB()->sanitize(self::getWeeksPerYear());
				$strConnector = ",";
			}
			if(isset($this->_dtmCreatedOn)) {
				$strSQL .= $strConnector . "dtmCreatedOn = ".$this->getDB()->sanitize(self::getCreatedOn());
				$strConnector = ",";
			}
			if(isset($this->_dtmModifiedOn)) {
				$strSQL .= $strConnector . "dtmModifiedOn = ".$this->getDB()->sanitize(self::getModifiedOn());
				$strConnector = ",";
			}
			$strSQL .= " WHERE intFacilityID = ".$this->getDB()->sanitize(self::getFacilityID())."";
			//echo $strSQL;
			return $this->getDB()->query($strSQL);
		}

		public function save() {
			if($this->_intFacilityID) {
				return $this->update();
			} else {
				return $this->insert();
			}
		}

		public function delete() {
			if($this->_intFacilityID) {
				$strSQL = "DELETE FROM tblFacility
				WHERE intFacilityID = '$this->_intFacilityID'
				";
				return $this->getDB()->query($strSQL);
			}
		}

		public function load($intFacilityID) {
			if(!$intFacilityID) {
				return false;
			}

			$strSQL = "SELECT *
				FROM tblFacility
				WHERE intFacilityID = '$intFacilityID'
				LIMIT 1
			";
			$rsFacility = $this->getDB()->query($strSQL);
			$arrFacility = $this->getDB()->fetch_assoc($rsFacility);
			$this->setVarsFromRow($arrFacility);
		}

		function setVarsFromRow($arrFacility) {
			if(isset($arrFacility["intFacilityID"])) $this->_intFacilityID = $arrFacility["intFacilityID"];
			if(isset($arrFacility["intOrganizationID"])) $this->_intOrganizationID = $arrFacility["intOrganizationID"];
			if(isset($arrFacility["strFacilityNumber"])) $this->_strFacilityNumber = $arrFacility["strFacilityNumber"];
			if(isset($arrFacility["strFacilityName"])) $this->_strFacilityName = $arrFacility["strFacilityName"];
			if(isset($arrFacility["txtAddress"])) $this->_txtAddress = $arrFacility["txtAddress"];
			if(isset($arrFacility["blnOurFacility"])) $this->_blnOurFacility = $arrFacility["blnOurFacility"];
			if(isset($arrFacility["blnCustomer"])) $this->_blnCustomer = $arrFacility["blnCustomer"];
			if(isset($arrFacility["intCreatedByUserID"])) $this->_intCreatedByUserID = $arrFacility["intCreatedByUserID"];
			if(isset($arrFacility["intTier"])) $this->_intTier = $arrFacility["intTier"];
			if(isset($arrFacility["dblHoursPerShift"])) $this->_dblHoursPerShift = $arrFacility["dblHoursPerShift"];
			if(isset($arrFacility["intShiftsPerDay"])) $this->_intShiftsPerDay = $arrFacility["intShiftsPerDay"];
			if(isset($arrFacility["intDaysPerWeek"])) $this->_intDaysPerWeek = $arrFacility["intDaysPerWeek"];
			if(isset($arrFacility["intWeeksPerYear"])) $this->_intWeeksPerYear = $arrFacility["intWeeksPerYear"];
			if(isset($arrFacility["dtmCreatedOn"])) $this->_dtmCreatedOn = $arrFacility["dtmCreatedOn"];

			if(isset($arrFacility["dtmModifiedOn"])) $this->_dtmModifiedOn = $arrFacility["dtmModifiedOn"];
		}

		function getFacilityID() {
			return $this->_intFacilityID;
		}
		function setFacilityID($value) {
			if($this->_intFacilityID !== $value) {
				$this->_intFacilityID = $value;
				$this->_blnDirty = true;
			}

		}

		function getOrganizationID() {
			return $this->_intOrganizationID;
		}
		function setOrganizationID($value) {
			if($this->_intOrganizationID !== $value) {
				$this->_intOrganizationID = $value;
				$this->_blnDirty = true;
			}
		}

		function getFacilityNumber() {
			return $this->_strFacilityNumber;
		}
		function setFacilityNumber($value) {
			if($this->_strFacilityNumber !== $value) {
				$this->_strFacilityNumber = $value;
				$this->_blnDirty = true;
			}
		}

		function getFacilityName() {
			return $this->_strFacilityName;
		}
		function setFacilityName($value) {
			if($this->_strFacilityName !== $value) {
				$this->_strFacilityName = $value;
				$this->_blnDirty = true;
			}
		}

		function getAddress() {
			return $this->_txtAddress;
		}
		function setAddress($value) {
			if($this->_txtAddress !== $value) {
				$this->_txtAddress = $value;
				$this->_blnDirty = true;
			}
		}

		function getOurFacility() {
			return $this->_blnOurFacility;
		}
		function setOurFacility($value) {
			if($this->_blnOurFacility !== $value) {
				$this->_blnOurFacility = $value;
				$this->_blnDirty = true;
			}
		}

		function getCustomer() {
			return $this->_blnCustomer;
		}
		function setCustomer($value) {
			if($this->_blnCustomer !== $value) {
				$this->_blnCustomer = $value;
				$this->_blnDirty = true;
			}
		}

		function getTier() {
			return $this->_intTier;
		}
		function setTier($value) {
			if($this->_intTier !== $value) {
				$this->_intTier = $value;
				$this->_blnDirty = true;
			}
		}

		function getHoursPerShift() {
			return $this->_dblHoursPerShift;
		}
		function setHoursPerShift($value) {
			if($this->_dblHoursPerShift !== $value) {
				$this->_dblHoursPerShift = $value;
				$this->_blnDirty = true;
			}
		}

		function getShiftsPerDay() {
			return $this->_intShiftsPerDay;
		}
		function setShiftsPerDay($value) {
			if($this->_intShiftsPerDay !== $value) {
				$this->_intShiftsPerDay = $value;
				$this->_blnDirty = true;
			}
		}

		function getDaysPerWeek() {
			return $this->_intDaysPerWeek;
		}
		function setDaysPerWeek($value) {
			if($this->_intDaysPerWeek !== $value) {
				$this->_intDaysPerWeek = $value;
				$this->_blnDirty = true;
			}
		}

		function getWeeksPerYear() {
			return $this->_intWeeksPerYear;
		}
		function setWeeksPerYear($value) {
			if($this->_intWeeksPerYear !== $value) {
				$this->_intWeeksPerYear = $value;
				$this->_blnDirty = true;
			}
		}

		function getCreatedByUserID() {
			return $this->_intCreatedByUserID;
		}
		function setCreatedByUserID($value) {
			if($this->_intCreatedByUserID !== $value) {
				$this->_intCreatedByUserID = $value;
				$this->_blnDirty = true;
			}

		}

		function getCreatedOn() {
			return $this->_dtmCreatedOn;
		}
		function setCreatedOn($value) {
			if($this->_dtmCreatedOn !== $value) {
				$this->_dtmCreatedOn = $value;
				$this->_blnDirty = true;
			}
		}

		function getModifiedOn() {

			return $this->_dtmModifiedOn;
		}
		function setModifiedOn($value) {
			if($this->_dtmModifiedOn !== $value) {
				$this->_dtmModifiedOn = $value;
				$this->_blnDirty = true;
			}
		}

	}

	include_once("User.class.php");
	include_once("Year.class.php");
	include_once("Machine.class.php");
	include_once("ProductionFacilityMachineType.class.php");

	class Facility extends FacilityBase {
		protected $_objUserArray;
		protected $_objPressArray;
		protected $_objMachineArray;
		protected $_objMachineScheduleArray;
		protected $_objYearArray;
		protected $_objProductionFacilityMachineTypeArray;

		protected $_intUsedCount = 0;
		protected $_intUserCount = 0;

		public $_arrUtil;

		function __construct($intFacilityID=null) {
			parent::__construct($intFacilityID);
		}

		public function loadByProgramID($intProgramID) {

			if(!$intProgramID) {
				return false;
			}

			$strSQL = "SELECT tblFacility.*
				FROM dbPLM.tblFacility
				INNER JOIN dbPLM.tblProduct
					ON tblProduct.intFacilityID = tblFacility.intFacilityID
				WHERE intProgramID = ".self::getDB()->sanitize($intProgramID)."
				LIMIT 1
			";
			$rsCustomer = $this->getDB()->query($strSQL);
			$arrCustomer = $this->getDB()->fetch_assoc($rsCustomer);
			$this->setVarsFromRow($arrCustomer);
		}

		public function loadByFacilityName($strFacilityName) {
			if(!$strFacilityName) {
				return false;
			}

			$strSQL = "SELECT tblFacility.*
				FROM dbPLM.tblFacility
				WHERE strFacilityName = ".self::getDB()->sanitize($strFacilityName)."
				LIMIT 1
			";
			$rsCustomer = $this->getDB()->query($strSQL);
			$arrCustomer = $this->getDB()->fetch_assoc($rsCustomer);
			$this->setVarsFromRow($arrCustomer);
		}

		function loadForProductionFacilityPage($intFacilityID) {
			if(!$intFacilityID) {
				return false;
			}

			$strSQL = "SELECT tblMachineType.*, tblProductionFacilityMachineType.*, tblUser.*, tblFacility.*
				FROM dbPLM.tblFacility
				LEFT JOIN dbPLM.tblUser
					ON tblUser.intProductionFacilityID = tblFacility.intFacilityID
				LEFT JOIN dbPLM.tblProductionFacilityMachineType
					ON tblProductionFacilityMachineType.intProductionFacilityID = tblFacility.intFacilityID
				LEFT JOIN dbPLM.tblMachineType
					ON tblProductionFacilityMachineType.intMachineTypeID = tblMachineType.intMachineTypeID
				WHERE intFacilityID = ".self::getDB()->sanitize($intFacilityID)."
				AND blnOurFacility
				ORDER BY strDisplayName
			";
			//echo $strSQL . "<br />";
			$rsResult = $this->getDB()->query($strSQL);
			while($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				if(!$this->getFacilityID()) {
					$this->setVarsFromRow($arrRow);
				}
				if($arrRow["intUserID"] && !$this->getUserArray()->objectIsSet($arrRow["intUserID"])) {
					$this->getUserArray()->addFromRow($arrRow, "intUserID");
				}
				if($arrRow["intProductionFacilityMachineTypeID"] && !$this->getProductionFacilityMachineTypeArray()->objectIsSet($arrRow["intProductionFacilityMachineTypeID"])) {
					$objPFMT = $this->getProductionFacilityMachineTypeArray()->addFromRow($arrRow, "intProductionFacilityMachineTypeID");
					$objPFMT->getMachineType()->setVarsFromRow($arrRow);
				}
			}

			$this->getMachineArray()->loadByProductionFacilityID($intFacilityID);
		}

		static function consolidate($intOldID, $intNewID) {
			if(!$intOldID || !$intNewID)
				return;
			$strSQL = "UPDATE dbPLM.tblProductXR SET
					intProductionFacilityID = ".self::getDB()->sanitize($intNewID)."
				WHERE intProductionFacilityID = ".self::getDB()->sanitize($intOldID);
			self::getDB()->query($strSQL);
			$strSQL = "UPDATE dbPLM.tblProductXR SET
					intShipToFacilityID = ".self::getDB()->sanitize($intNewID)."
				WHERE intShipToFacilityID = ".self::getDB()->sanitize($intOldID);
			self::getDB()->query($strSQL);

			$objFacility = new Facility();
			$objFacility->setFacilityID($intOldID);
			$objFacility->delete();
		}

		function setVarsFromRow($arrRow, $strFacilityType="") {
			parent::setVarsFromRow($arrRow);

			if(isset($arrRow["str{$strFacilityType}FacilityName"])) $this->setFacilityName($arrRow["str{$strFacilityType}FacilityName"]);
			if(isset($arrRow["intUsedCount"])) $this->setUsedCount($arrRow["intUsedCount"]);
		}

		function validate() {
			$arrErrors = array();


			if(!$this->getHoursPerShift()) {
				$arrErrors[] = "Hours Per Shift must be specified.";
			} elseif(!is_numeric($this->getHoursPerShift())) {
				$arrErrors[] = "Hours Per Shift must be a number.";
			}

			if(!$this->getDaysPerWeek()) {
				$arrErrors[] = "Days Per Week must be specified.";
			} elseif(!is_numeric($this->getDaysPerWeek())) {
				$arrErrors[] = "Days Per Week must be a number.";
			}

			if(!$this->getWeeksPerYear()) {
				$arrErrors[] = "Weeks Per Year must be specified.";
			} elseif(!is_numeric($this->getWeeksPerYear())) {
				$arrErrors[] = "Weeks Per Year must be a number.";
			}

			return $arrErrors;
		}

		function getProductionFacilityURL() { // Deprecated, use ProductionFacility->getURL() or ShipToFacility->getURL() (not yet created)
			return "production_facility.php?intFacilityID=".$this->getFacilityID();
		}

		function getUserArray() {
			if(!$this->_objUserArray) {
				$this->_objUserArray = new UserArray();
			}
			return $this->_objUserArray;
		}
		function getMachineArray() {
			if(!$this->_objMachineArray) {
				$this->_objMachineArray = new MachineArray();
			}
			return $this->_objMachineArray;
		}
		function getMachineScheduleArray() {
			include_once("MachineSchedule.class.php");
			if(!$this->_objMachineScheduleArray) {
				$this->_objMachineScheduleArray = new MachineScheduleArray();
			}
			return $this->_objMachineScheduleArray;
		}
		function getYearArray() {
			if(!$this->_objYearArray) {
				$this->_objYearArray = new YearArray();
			}
			return $this->_objYearArray;
		}
		function getProductionFacilityMachineTypeArray() {
			if(!$this->_objProductionFacilityMachineTypeArray) {
				$this->_objProductionFacilityMachineTypeArray = new ProductionFacilityMachineTypeArray();
			}
			return $this->_objProductionFacilityMachineTypeArray;
		}

		function getUsedCount() {
			return $this->_intUsedCount;
		}
		function setUsedCount($value) {
			$this->_intUsedCount = $value;
		}
		function getUserCount() {
			return $this->_intUserCount;
		}
		function setUserCount($value) {
			$this->_intUserCount = $value;
		}

		function getID() {
			return $this->getFacilityID();
		}
		function getName() {
			return $this->getFacilityName();
		}

		function getWeeklyAvailableHours() {
			return $this->getHoursPerShift() * $this->getShiftsPerDay() * $this->getDaysPerWeek();
		}
		function getYearlyAvailableHours() {
			return $this->getWeeklyAvailableHours() * $this->getWeeksPerYear();
		}
	}
?>
