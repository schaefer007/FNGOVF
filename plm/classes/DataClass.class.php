<?php
	include_once("Database.class.php");

	abstract class DataClass {
		static private $_objDB;
		protected $_blnDirty = false;
		private $_blnNew = 0; // Used in the UI to determine if a new object was selected, or an existing one (ex: The current page's object cross references this object)
		protected $_blnDeleted; // Used in copy operations to denote whether an object should be deleted on save (ex: Attendence of programmilestone.php)

		private $_objOriginal;

		private $_intTableID;

		// Used for Engineering Changes
		static private $_blnTrackChanges = false;
		private $_objChangesCopy;
		private $_blnChangesCopy = false;
		private $_objChangeArray;
		private $_arrTrackFields = array();
		private $_blnECDisplay = false;
		private $_intChangeStatusID;

		public function DataClass($intTableID=null) {
			if($intTableID) $this->_intTableID = $intTableID;
		}


		static private function connectDB() {
			self::$_objDB = new Database();
			self::$_objDB->connect_and_select();
		}

		protected function insert() {
			$objEC = DataClass::getEC();
			if(!$objEC)
				return;

			include_once("AdditionDeletion.class.php");
			$objChange = new AdditionDeletion();
			$objChange->loadByTableIDAndEngineeringChangeIDAndID($this->getTableID(), $objEC->getEngineeringChangeID(), $this->getID());
			if($objChange->getChangeType() == "Deletion") { // This was deleted this EC, the tblAdditionDeletion record can be deleted (This probably never gets called because a tblXXX record will exist in the database, and thus, update() is called, not insert())
				$objChange->delete();
			} else {
				$objChange->setTableID($this->getTableID());
				$objChange->setEngineeringChangeID($objEC->getEngineeringChangeID());
				$objChange->setID($this->getID());
				$objChange->setChangeType("Addition");
				$objChange->save();
			}
		}

		protected function update() {
			if(!$this->getTrackFields())
				return;

			$objEC = DataClass::getEC();
			if(!$objEC)
				return;

			global $arrColumnIDToNameMap;
			foreach($this->getTrackFields() as $intColumnID) {
				$strField = isset($arrColumnIDToNameMap[$intColumnID])?$arrColumnIDToNameMap[$intColumnID]:$intColumnID;
				$strAttribute = "_".$strField;
				if(isset($this->getChangesCopy()->$strAttribute)/* || @is_null($this->getChangesCopy()->$strAttribute)*/) {
					$objColumn = new Column();
					$objColumn->loadByTableIDAndColumnName($this->getTableID(), $strField);
					if(!$objColumn->getColumnID())
						continue;

					$objChange = new Modification();
					$objChange->loadByColumnIDAndEngineeringChangeIDAndID($objColumn->getColumnID(), $objEC->getEngineeringChangeID(), $this->getID());

					if($this->$strAttribute == $this->getChangesCopy()->$strAttribute) {
						$objChange->delete(); // Delete tblModification record if it exists
					} else {
						if(!$objChange->getModificationID()) { // New modification
							$objChange->setOriginalValue($this->$strAttribute);
						}
						$objChange->setColumnID($objColumn->getColumnID());
						$objChange->setEngineeringChangeID($objEC->getEngineeringChangeID());
						$objChange->setID($this->getID());
						$objChange->setChangeType("Modification");
						$objChange->setNewValue($this->getChangesCopy()->$strAttribute);
						$objChange->save();
					}
				}
			}
		}

		protected function delete() {
			$objEC = DataClass::getEC();
			if(!$objEC)
				return;

			$objChange = new AdditionDeletion();
			$objChange->loadByTableIDAndEngineeringChangeIDAndID($this->getTableID(), $objEC->getEngineeringChangeID(), $this->getID());
			if($objChange->getChangeType() == "Addition") { // This was added this EC, the tblAdditionDeletion record can be deleted
				$objChange->delete();
			} else {
				$objChange->setTableID($this->getTableID());
				$objChange->setEngineeringChangeID($objEC->getEngineeringChangeID());
				$objChange->setID($this->getID());
				$objChange->setChangeType("Deletion");
				$objChange->save();
				return false;
			}
			return true;
		}

		function deleteDeletion() {
			$objEC = DataClass::getEC();
			if(!$objEC)
				return;

			// Delete tblAdditionDeletion record of strChangeType Deletion if it exists
			$objAdditionDeletion = new AdditionDeletion();
			$objAdditionDeletion->loadByTableIDAndEngineeringChangeIDAndID($this->getTableID(), $objEC->getEngineeringChangeID(), $this->getID());
			if($objAdditionDeletion->getChangeType() == "Deletion") {
				$objAdditionDeletion->delete();
			}
		}

		function setVarsFromRow($arrRow) {
			if(isset($arrRow["intChangeStatusID"])) $this->_intChangeStatusID = $arrRow["intChangeStatusID"];
		}

		public static function getDB() {
			if(!self::$_objDB) {
				self::connectDB();
			}
			return self::$_objDB;
		}
		public function isDirty() {
			return $this->_blnDirty;
		}
		public function setDirty($blnDirty) {
			$this->_blnDirty = $blnDirty;
		}
		function isNew(){
			return $this->_blnNew;
		}
		function setNew($value){
			$this->_blnNew = $value;
		}

		static function getTrackChanges(){
			return self::$_blnTrackChanges;
		}
		static function setTrackChanges($value) {
			self::$_blnTrackChanges = $value;
		}
		function getChangesCopy(){
			if(!$this->_objChangesCopy) {
				$strClass = get_class($this);
				$this->_objChangesCopy = new $strClass();
				$this->_objChangesCopy->setChangesCopy(true);
			}
			return $this->_objChangesCopy;
		}
		function isChangesCopy() {
			return $this->_blnChangesCopy;
		}
		function setChangesCopy($value) {
			$this->_blnChangesCopy = $value;
		}
		function getChangeArray(){
			if(!$this->_objChangeArray)
				$this->_objChangeArray = new ChangeArray();
			return $this->_objChangeArray;
		}
		function getTrackFields(){
			return $this->_arrTrackFields;
		}
		function setTrackFields($value){
			$this->_arrTrackFields = $value;
		}
		function getECDisplay() {
			return $this->_blnECDisplay;

		}
		function setECDisplay($value) {
			$this->_blnECDisplay = $value;
		}
		function getChangeStatusID() {
			return $this->_intChangeStatusID;
		}
		function setChangeStatusID($value) {
			$this->_intChangeStatusID = $value;
		}
		function getTableID() {
			return $this->_intTableID;
		}

		function applyChanges($objChangeArray, $objEC) {
			if(!$objChangeArray->getArray())
				return;

			$blnTrackChanges = DataClass::getTrackChanges();
			DataClass::setTrackChanges(false);

			$arrChangeArray = $objChangeArray->getArray();

			if($objEC && $objEC->getEngineeringChangeID()) {
				foreach($arrChangeArray["Modification"]->getArray() as $arrECChanges) {
					foreach($arrECChanges as $objChange) {
						if($objChange->getColumn()->getTableID() == $this->getTableID() && $objChange->getID() == $this->getID()) {
							$strSetFunction = "set".substr($objChange->getColumn()->getColumnName(), 3); // CODE CLEAN: Not crazy about this dependency...
							$this->getChangeArray()->addNew($objChange);
							if(method_exists($this, $strSetFunction)) {
								//echo $objChange->getNewValue() . "|" . $strSetFunction."|"."<br />";
								if($objEC->getECStatus() == "Approved") { // Old Approved EC
									if($objEC->getEngineeringChangeID() != $objChange->getEngineeringChangeID()) {
										$this->$strSetFunction($objChange->getOriginalValue());
										$this->setHumanReadableValue($objChange, $objChange->getOriginalValue());
									}
								} else {
									$this->$strSetFunction($objChange->getNewValue()); // New, unapproved (New or Pending Approval) EC
									$this->setHumanReadableValue($objChange, $objChange->getNewValue());
								}
							}
						}
					}
				}
			}

			//print_r($arrChangeArray);
			foreach($arrChangeArray["AdditionDeletion"]->getArray() as $arrECChanges) {
				foreach($arrECChanges as $objChange) {
					if($objChange->getTable()->getTableID() == $this->getTableID() && $objChange->getID() == $this->getID()) {
						$this->getChangeArray()->addNew($objChange);

						if($objChange->getChangeType() == "Addition") {
							$this->setECDisplay(true);
							$this->setChangeStatusID(intCHANGE_STATUS_ID_ACTIVE);
						} elseif($objChange->getChangeType() == "Deletion") {
							$this->setECDisplay(false);
							$this->setChangeStatusID(intCHANGE_STATUS_ID_DELETED);
						}
					}
				}
			}

			DataClass::setTrackChanges($blnTrackChanges);
		}

		function setHumanReadableValue($objChange, $intID) {
			if($objChange->getColumnID() == intCOLUMN_ID_PRODUCTION_FACILITY_ID) {
				if(Reference::getFacilityArray()->getObject($intID)) {
					$this->setProductionFacility(Reference::getFacilityArray()->getObject($intID));
				}
			} elseif($objChange->getColumnID() == intCOLUMN_ID_SHIP_TO_FACILITY_ID) {
				if(Reference::getFacilityArray()->getObject($intID)) {
					$this->setShipToFacility(Reference::getFacilityArray()->getObject($intID));
				}
			} elseif($objChange->getColumnID() == intCOLUMN_ID_PRODUCT_SUPPLIER || $objChange->getColumnID() == intCOLUMN_ID_TOOL_SUPPLIER ) {
				if(Reference::getSupplierArray()->getObject($intID)) {
					$this->setSupplier(Reference::getSupplierArray()->getObject($intID));
				}
			} elseif($objChange->getColumnID() == intCOLUMN_ID_PRODUCT_TOOL_XR_PRODUCT_ID) {
				if(Reference::getProductArray()->getObject($intID)) {
					$this->setProduct(Reference::getProductArray()->getObject($intID));
				}
			}
		}

		static function getEC() {
			$objProgramCommon = isset($_SESSION["arrWorkingData"]["objProgramCommon"])?$_SESSION["arrWorkingData"]["objProgramCommon"]:null;
			$objEC = isset($_SESSION["arrWorkingData"]["objCurrentEC"])?$_SESSION["arrWorkingData"]["objCurrentEC"]:null;

			if(!$objProgramCommon || !$objProgramCommon->getProgramCommonID()) {
				return null;
			}

			//echo $objEC->getEngineeringChangeID() ."|". $objProgramCommon->getProgramCommonID() ."|". $objEC->getProgramCommonID(). "\n";
			if(!$objEC || !$objEC->getEngineeringChangeID() || $objProgramCommon->getProgramCommonID() != $objEC->getProgramCommonID()) {
				include_once("EngineeringChange.class.php");
				$objEC = new EngineeringChange();
				$objEC->loadByProgramCommonIDAndECNumber($objProgramCommon->getProgramCommonID(), 0);
			}

			return $objEC;
		}

		function getOriginal() {
			if(!$this->_objOriginal) {
				$strClass = get_class($this);
				$this->_objOriginal = new $strClass();
			}
			return $this->_objOriginal;
		}
		function setOriginal($value) {
			$this->_objOriginal = $value;
		}

		function getDeleted() {
			return $this->_blnDeleted;
		}
		function setDeleted($value) {
			$this->_blnDeleted = $value;
		}
	}
?>