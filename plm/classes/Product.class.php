<?php
	require_once("ArrayClass.class.php");
	include_once("Search.class.php");

	class ProductArray extends ArrayClass {
		function __construct() {
			parent::__construct("Product");
		}

		static function getProgramListPageQuery($strFilter){
			$objSearch = (isset($_SESSION["objSearch"])?$_SESSION["objSearch"]:new Search()); // For cron jobs that use all programs

			$strSelectSQL = " tblBOMDocument.strFileName strBOMFileName, tblBOMDocument.intFolderID intBOMFolderID,
				tblISODocument.strFileName strISOFileName, tblISODocument.intFolderID intISOFolderID,
				tblPlatform.*, tblOrganization.*, tblCustomer.*, tblFacility.*, tblProduct.*, tblProgramCommon.* ";
			$strTableSQL = "";
			$strWhereSQL = "";
			$strGroupBySQL = "";
			$strOrderBySQL = "";
			if($strFilter == "Member" || ($objSearch && $objSearch->getMemberUserID())) {
				$strTableSQL .= "
					INNER JOIN dbPLM.tblMember
						ON tblProduct.intProgramID = tblMember.intProgramID
				";
				if($strFilter != "Member") {
					$strWhereSQL .= " AND tblMember.intUserID = ".Database::sanitize($objSearch->getMemberUserID());
				} else {
					$strTableSQL .= "
						INNER JOIN dbPLM.tblUser
							ON tblMember.intUserID = tblUser.intUserID
					";
					$strSelectSQL = " DISTINCT tblUser.intUserID, tblUser.strDisplayName ";
					$strOrderBySQL = " ORDER BY tblUser.strDisplayName ";
				}
			}
			if($strFilter == "Customer" || $strFilter == "Program" || ($objSearch && $objSearch->getCustomerID())) {
				$strJoinType = " LEFT ";
				if($strFilter != "Customer" && ($objSearch && $objSearch->getCustomerID())) {
					$strWhereSQL .= " AND tblProgramCommon.intCustomerID = ".Database::sanitize($objSearch->getCustomerID());
				} elseif($strFilter == "Customer") {
					$strSelectSQL = " DISTINCT tblCustomer.intCustomerID, tblOrganization.strOrganizationShortName ";
					$strJoinType = " INNER ";
					$strOrderBySQL = " ORDER BY tblOrganization.strOrganizationName ";
				}
				$strTableSQL .= "
					$strJoinType JOIN dbPLM.tblCustomer
						ON tblProgramCommon.intCustomerID = tblCustomer.intCustomerID
					$strJoinType JOIN dbPLM.tblOrganization
						ON tblOrganization.intOrganizationID = tblCustomer.intOrganizationID
				";
			}
			if($strFilter == "Facility" || $strFilter == "Program" || ($objSearch && $objSearch->getFacilityID())) {
				//$strJoinType = " LEFT ";
				if($strFilter != "Facility" && ($objSearch && $objSearch->getFacilityID())) {
					$strWhereSQL .= " AND tblProgramProductionFacility.intProductionFacilityID = ".Database::sanitize($objSearch->getFacilityID());
				} elseif($strFilter == "Facility") {
					$strSelectSQL = " DISTINCT tblFacility.intFacilityID, tblFacility.strFacilityName ";
					$strJoinType = " INNER ";
					$strOrderBySQL = " ORDER BY tblFacility.strFacilityName ";
				}
				/*$strTableSQL .= "
					$strJoinType JOIN dbPLM.tblTool
						ON tblProduct.intProgramID = tblTool.intProgramID
					$strJoinType JOIN dbPLM.tblFacility
						ON tblTool.intFacilityID = tblFacility.intFacilityID
				";*/
			}
			if($strFilter == "Platform" || $strFilter == "Program" || ($objSearch && $objSearch->getPlatformID())) {
				$strJoinType = " LEFT ";
				if($strFilter != "Platform" && ($objSearch && $objSearch->getPlatformID())) {
					$strWhereSQL .= " AND tblProgramCommon.intPlatformID = ".Database::sanitize($objSearch->getPlatformID());
				} elseif($strFilter == "Platform") {
					$strSelectSQL = " DISTINCT tblPlatform.intPlatformID, tblPlatform.strPlatform ";
					$strJoinType = " INNER ";
					$strOrderBySQL = " ORDER BY tblPlatform.strPlatform ";
				}
				$strTableSQL .= "
					$strJoinType JOIN dbPLM.tblPlatform
						ON tblProgramCommon.intPlatformID = tblPlatform.intPlatformID
				";
			}
			if($objSearch) {
				if($objSearch->getProgramTypeID() && $strFilter != "Program Type") {
					$strWhereSQL .= " AND tblProgramCommon.intProgramTypeID = ".Database::sanitize($objSearch->getProgramTypeID());
				}
				if($objSearch->getProgramSubTypeID() && $strFilter != "Program Sub Type") {
					$strWhereSQL .= " AND tblProgramCommon.intProgramSubTypeID = ".Database::sanitize($objSearch->getProgramSubTypeID());
				}
				if($objSearch->getDesignResponsible()) {
					$strWhereSQL .= " AND tblProgramCommon.blnDesignResponsible ";
				} elseif($objSearch->getDesignResponsible() === "0") {
					$strWhereSQL .= " AND !tblProgramCommon.blnDesignResponsible ";
				}
				if($objSearch->getWaitingApprovalUserID()) {
				}
			}
			if($strFilter == "Waiting Approval User" || ($objSearch && $objSearch->getWaitingApprovalUserID())) {
				$strTableSQL .= "
					INNER JOIN dbPLM.tblApprovalProcessInstance
						ON tblApprovalProcessInstance.intApprovalProcessInstanceID = tblProduct.intApprovalProcessInstanceID
					INNER JOIN dbPLM.tblApprovalProcess
						ON tblApprovalProcess.intApprovalProcessID = tblApprovalProcessInstance.intApprovalProcessID
					INNER JOIN dbPLM.tblApprovalProcessRole
						ON tblApprovalProcessRole.intApprovalProcessID = tblApprovalProcess.intApprovalProcessID
					INNER JOIN dbPLM.tblMemberRole
						ON tblMemberRole.intRoleID = tblApprovalProcessRole.intRoleID
					INNER JOIN dbPLM.tblMember tblMemberWaitingApproval
						ON tblMemberWaitingApproval.intMemberID = tblMemberRole.intMemberID
						AND tblMemberWaitingApproval.intProgramID = tblProduct.intProgramID
					INNER JOIN dbPLM.tblUser tblUserWaitingApproval
						ON tblMemberWaitingApproval.intUserID = tblUserWaitingApproval.intUserID
					LEFT JOIN dbPLM.tblApprovalProcessRoleAction
						ON tblApprovalProcessRole.intApprovalProcessRoleID = tblApprovalProcessRoleAction.intApprovalProcessRoleID
						AND tblApprovalProcessRoleAction.intApprovalProcessInstanceID = tblApprovalProcessInstance.intApprovalProcessInstanceID
				";
				$strWhereSQL .= "AND (tblApprovalProcessRoleAction.strAction != 'Approved' OR tblApprovalProcessRoleAction.strAction IS NULL) ";
				if($strFilter != "Waiting Approval User") {
					$strWhereSQL .= " AND tblMemberWaitingApproval.intUserID = ".self::getDB()->sanitize($objSearch->getWaitingApprovalUserID());
				} else {
					$strSelectSQL = " DISTINCT tblUserWaitingApproval.intUserID, tblUserWaitingApproval.strDisplayName ";
					$strOrderBySQL = " ORDER BY tblUserWaitingApproval.strDisplayName ";
					$strWhereSQL .= " AND tblProgramCommon.strStatus = 'Pending Approval' ";
				}
			}
			if($objSearch && $objSearch->getProgramStatus()) {
				$strWhereSQL .= " AND tblProgramCommon.strStatus = ".Database::sanitize($objSearch->getProgramStatus());
			}
			if($objSearch && $objSearch->getProgramRisk()) {
				$strWhereSQL .= " AND tblProgramCommon.strRisk = ".Database::sanitize($objSearch->getProgramRisk());
			}
			if($strFilter == "Program") {
				$strSelectSQL .= ", (SELECT GROUP_CONCAT(DISTINCT strDisplayName SEPARATOR ' , ')
					FROM dbPLM.tblUser tblUser2
					INNER JOIN dbPLM.tblMember tblMember2
						USING (intUserID)
					INNER JOIN dbPLM.tblMemberRole tblMemberRole2
						USING (intMemberID)
					WHERE dbPLM.tblMemberRole2.intRoleID = ".Database::sanitize(intROLE_ID_PROGRAM_MANAGER)."
					AND tblMember2.intProgramID = tblProduct.intProgramID
				) AS strProgramManager ";
				$strSelectSQL = " tblProgramMilestone.*, tblMilestone.*, tblMilestone.intProgramTypeID tblMilestoneProgramTypeID, " . $strSelectSQL;
				$strTableSQL .= "
					LEFT JOIN dbPLM.tblProgramMilestone
						ON tblProgramMilestone.intProgramID = tblProduct.intProgramID
					LEFT JOIN dbPLM.tblMilestone
						ON tblMilestone.intMilestoneID = tblProgramMilestone.intMilestoneID ";
				$strGroupBySQL = " GROUP BY tblProduct.intProgramID, tblProgramMilestone.intProgramMilestoneID, tblFacility.intFacilityID ";
				//$strOrderBySQL = $objSearch->getProgramListPage()->getSortQuery();
				$strConnector = "";
				if($objSearch->getProgramListPage()->getSorts()) {
					$arrSorts = $objSearch->getProgramListPage()->getSorts();
					$intSortsCount = count($arrSorts);
					for($i=$intSortsCount-1; $i>=0; $i--) {
						$strOrderBySQL .= $strConnector . $arrSorts[$i]->getSortBy() . " " . $arrSorts[$i]->getSortOrder();
						$strConnector = ", ";
					}
				}
				//$strOrderBySQL = " ORDER BY intProgramXRID " . $strConnector . $strOrderBySQL;
				if($strOrderBySQL) $strOrderBySQL = " ORDER BY $strOrderBySQL ";
			} elseif($strFilter == "Program Type") {
				$strTableSQL .= "
					INNER JOIN dbPLM.tblProgramType
						ON tblProgramCommon.intProgramTypeID = tblProgramType.intProgramTypeID
				";
				$strSelectSQL = " DISTINCT tblProgramType.intProgramTypeID, tblProgramType.strProgramType ";
				$strOrderBySQL = " ORDER BY tblProgramType.strProgramType ";
			} elseif($strFilter == "Program Sub Type") {
				$strTableSQL .= "
					INNER JOIN dbPLM.tblProgramSubType
						ON tblProgramCommon.intProgramSubTypeID = tblProgramSubType.intProgramSubTypeID
				";
				$strSelectSQL = " DISTINCT tblProgramSubType.intProgramSubTypeID, tblProgramSubType.strProgramSubType ";
				$strOrderBySQL = " ORDER BY tblProgramSubType.strProgramSubType ";
			}

			$strSQL = " SELECT $strSelectSQL
				FROM dbPLM.tblProgramCommon
				INNER JOIN dbPLM.tblProduct
					ON tblProduct.intProgramID = tblProgramCommon.intTopLevelProductID
				LEFT JOIN dbPLM.tblProgramProductionFacility
					ON tblProgramProductionFacility.intProgramCommonID = tblProgramCommon.intProgramCommonID
				LEFT JOIN dbPLM.tblFacility
					ON tblFacility.intFacilityID = tblProgramProductionFacility.intProductionFacilityID
				LEFT JOIN dbDocument.tblDocument tblBOMDocument
					ON tblProduct.intBOMDocumentID = tblBOMDocument.intDocumentID
				LEFT JOIN dbDocument.tblDocument tblISODocument
					ON tblProduct.intISODocumentID = tblISODocument.intDocumentID
				$strTableSQL
				WHERE 1 $strWhereSQL
				$strGroupBySQL
				$strOrderBySQL";
			//if(strpos($_SERVER["PHP_SELF"], "cron_milestoneCSSClass.php") !== false) echo $strSQL."<br /><br />";
			return $strSQL;
		}

		function load() {
			$strSQL = " SELECT * FROM tblProduct";
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				$this->_arrObjects[$arrRow["intProgramID"]] = new Product();
				$this->_arrObjects[$arrRow["intProgramID"]]->setVarsFromRow($arrRow);
			}
		}

		function loadFlattenedArrayByProductIDRecursively($intProductID) {
			if(!$intProductID)
				return;

			$strSQL = "SELECT tblProduct.*
				FROM dbPLM.tblProduct
				INNER JOIN dbPLM.tblProductXR
					ON tblProduct.intProgramID = tblProductXR.intChildProgramID
				WHERE tblProductXR.intParentProgramID = ".self::getDB()->sanitize($intProductID)."
				AND intProductTypeID IN ('".intPRODUCT_TYPE_ID_ASSEMBLY."','".intPRODUCT_TYPE_ID_PLASTIC_PART."','".intPRODUCT_TYPE_ID_METAL_PART."')";
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				$this->_arrObjects[$arrRow["intProgramID"]] = new Product();
				$this->_arrObjects[$arrRow["intProgramID"]]->setVarsFromRow($arrRow);
				$this->loadFlattenedArrayByProductIDRecursively($arrRow["intProgramID"]);
			}
		}

		function loadParentsByProductID($intProductID, $intProgramCommonID=null) {
			$strSelectSQL = $strTableSQL = "";
			if($intProgramCommonID) {
				$strSelectSQL= " tblProgramProduct.*, ";
				$strTableSQL = "
					LEFT JOIN dbPLM.tblProgramProduct
						ON tblProgramProduct.intProductID = tblProduct.intProgramID
						AND tblProgramProduct.intProgramCommonID = ".self::getDB()->sanitize($intProgramCommonID)."
				";
			}
			$strSQL = " SELECT tblProductionFacility.strFacilityName AS strProductionFacilityName, tblShipToFacility.strFacilityName AS strShipToFacilityName,
				$strSelectSQL tblProductType.*, tblProduct.*, tblParentProductXR.*
				FROM tblProductXR tblParentProductXR
				INNER JOIN dbPLM.tblProduct
					ON tblProduct.intProgramID = tblParentProductXR.intChildProgramID
				INNER JOIN dbPLM.tblProductXR
					ON tblProduct.intProgramID = tblProductXR.intParentProgramID
				INNER JOIN dbPLM.tblProductType
					ON tblProductType.intProductTypeID = tblProduct.intProductTypeID
				LEFT JOIN dbPLM.tblFacility tblProductionFacility
					ON tblParentProductXR.intProductionFacilityID = tblProductionFacility.intFacilityID
				LEFT JOIN dbPLM.tblFacility tblShipToFacility
					ON tblParentProductXR.intShipToFacilityID = tblShipToFacility.intFacilityID
				$strTableSQL
				WHERE tblProductXR.intChildProgramID = ".self::getDB()->sanitize($intProductID);
			//echo $strSQL . "<br />";
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				if(!isset($this->_arrObjects[$arrRow["intProgramID"]])) {
					$this->_arrObjects[$arrRow["intProgramID"]] = new Product();
					$this->_arrObjects[$arrRow["intProgramID"]]->setVarsFromRow($arrRow);
					$this->_arrObjects[$arrRow["intProgramID"]]->getProductType()->setVarsFromRow($arrRow);
					//$this->_arrObjects[$arrRow["intProgramID"]]->getProgramProduct()->setVarsFromRow($arrRow);
				}
				$objParentProductXR = $this->_arrObjects[$arrRow["intProgramID"]]->getParentProductXRArray()->addFromRow($arrRow, "intProgramXRID");
				$objParentProductXR->getProductionFacility()->setVarsFromRow($arrRow, "Production");
				$objParentProductXR->getShipToFacility()->setVarsFromRow($arrRow, "ShipTo");
			}
		}

		function loadParentPrograms() {
			$strSQL = "SELECT tblProgramProduct.*, tblPlatform.*, tblProduct.*
				FROM tblProduct
				INNER JOIN tblProgramCommon
					ON tblProgramCommon.intProgramCommonID = tblProduct.intProgramCommonID
				INNER JOIN tblPlatform
					ON tblProgramCommon.intPlatformID = tblPlatform.intPlatformID
				INNER JOIN tblProgramData
					ON tblProgramData.intProgramID = tblProduct.intProgramID
				LEFT JOIN tblProgramProduct
					ON tblProgramProduct.intProductID = tblProduct.intProgramID
					AND tblProgramProduct.intProgramCommonID = tblProgramCommon.intProgramCommonID
				ORDER BY strPlatform, strProgramName";
			//echo $strSQL;
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				$objProduct = new Product();
				$objProduct->setVarsFromRow($arrRow);
				//$objProduct->getProgramProduct()->setVarsFromRow($arrRow);
				$objProduct->getProgramCommon()->getPlatform()->setVarsFromRow($arrRow);
				$this->_arrObjects[$arrRow["intProgramID"]] = $objProduct;
			}
		}

		function loadForECAdd() {
			$strSQL = "SELECT tblPlatform.*, tblProgramCommon.*, tblProduct.*
				FROM tblProduct
				INNER JOIN tblProgramCommon
					ON tblProgramCommon.intProgramCommonID = tblProduct.intProgramCommonID
				INNER JOIN tblPlatform
					ON tblProgramCommon.intPlatformID = tblPlatform.intPlatformID
				INNER JOIN tblProgramData
					ON tblProgramData.intProgramID = tblProduct.intProgramID
				WHERE blnDesignReleased
				ORDER BY strPlatform, strProgramName";
			//echo $strSQL;
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				$objProduct = new Product();
				$objProduct->setVarsFromRow($arrRow);
				$objProduct->getProgramCommon()->setVarsFromRow($arrRow);
				//$objProduct->getProgramCommon()->getPlatform()->setVarsFromRow($arrRow);
				$this->_arrObjects[$arrRow["intProgramID"]] = $objProduct;
			}
		}

		function loadChildPrograms(){
			include_once("ProductXR.class.php");
			if(!$this->getArray())
				return;

			$arrProgramIDs = array_keys($this->getArray());

			$strSQL = "SELECT tblProduct.*, tblProductXR.*
					FROM tblProductXR
					INNER JOIN tblProduct
						ON tblProductXR.intChildProgramID = tblProduct.intProgramID
					INNER JOIN tblProgramCommon
						ON tblProduct.intProgramCommonID = tblProgramCommon.intProgramCommonID
					WHERE tblProductXR.intParentProgramID IN ('".implode("','", $arrProgramIDs)."')
				";
			//echo $strSQL . "<br />";
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				$objProductXR = new ProductXR();
				$objProductXR->setVarsFromRow($arrRow);
				$objProductXR->getChildProduct()->setVarsFromRow($arrRow);
				$this->_arrObjects[$arrRow["intParentProgramID"]]->getChildProductXRArray()->setObject($arrRow["intProgramID"], $objProductXR);
			}
		}

		function loadChildrenByParentProgramID($intProgramID){
			if(!$intProgramID)
				return;

			$strSQL = "SELECT tblDocument.*, tblFacility.*, tblProduct.*, tblProductXR.*
					FROM tblProductXR
					INNER JOIN tblProduct
						ON tblProductXR.intChildProgramID = tblProduct.intProgramID
					INNER JOIN tblProgramCommon
						ON tblProgramCommon.intProgramCommonID = tblProgramCommon.intProgramCommonID
					LEFT JOIN dbPLM.tblFacility
						ON tblFacility.intFacilityID = tblProductXR.intProductionFacilityID
					LEFT JOIN dbDocument.tblDocument
						ON tblDocument.intDocumentID = tblProduct.intISODocumentID
					WHERE tblProductXR.intParentProgramID = ".self::getDB()->sanitize($intProgramID)."
				";
			//echo $strSQL . "<br />";
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				$objProduct = new Product();
				$objProduct->setVarsFromRow($arrRow);
				$objProduct->getParentProductXR()->setVarsFromRow($arrRow);
				$objProduct->getParentProductXR()->getProductionFacility()->setVarsFromRow($arrRow);
				$objProduct->getUser()->setVarsFromRow($arrRow);
				$objProduct->getISODocument()->setVarsFromRow($arrRow);
				$objProduct->getProgramCommon()->setVarsFromRow($arrRow);
				$objProduct->getProgramCommon()->getCustomer()->setVarsFromRow($arrRow);
				$objProduct->getProgramCommon()->getPlatform()->setVarsFromRow($arrRow);
				//$objProduct->loadChildrenRecursivelyWithMilestones();
				$this->_arrObjects[$arrRow["intProgramID"]] = $objProduct;
			}
		}

		function loadChildrenRecursivelyWithMilestones(){
			if(!$this->getArray())
				return;

			foreach($this->getArray() as $objObject) {
				$objObject->loadChildrenRecursivelyWithMilestones();
			}
		}

		function loadForProgramDashboard($objSearch=null) {
			$strSQL = ProductArray::getProgramListPageQuery("Program");

			//echo $strSQL;
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				if(!isset($this->_arrObjects[$arrRow["intProgramID"]])) {
					$objProduct = new Product();
					$objProduct->setVarsFromRow($arrRow);
					$objProduct->getUser()->setVarsFromRow($arrRow);
					$objProduct->getBOMDocument()->setVarsFromRow($arrRow, "BOM");
					$objProduct->getISODocument()->setVarsFromRow($arrRow, "ISO");
					$objProduct->getProgramCommon()->setVarsFromRow($arrRow);
					$objProduct->getProgramCommon()->getCustomer()->setVarsFromRow($arrRow);
					$objProduct->getProgramCommon()->getPlatform()->setVarsFromRow($arrRow);
					//$objProduct->loadChildrenRecursivelyWithMilestones();
				} else {
					$objProduct = $this->_arrObjects[$arrRow["intProgramID"]];
				}
				if($arrRow["intMilestoneID"]) {
					$objProduct->getProgramMilestoneArray()->addFromRow($arrRow, "intMilestoneID");
					$objProduct->getProgramMilestoneArray()->getObject($arrRow["intMilestoneID"])->getMilestone()->setVarsFromRow($arrRow, "Milestone");
				}
				if($arrRow["intFacilityID"]) {
					$objProduct->getProgramCommon()->getProgramProductionFacilityArray()->addFromRow($arrRow, "intFacilityID");
					$objProduct->getProgramCommon()->getProgramProductionFacilityArray()->getObject($arrRow["intFacilityID"])->getProductionFacility()->setVarsFromRow($arrRow);
				}

				$this->_arrObjects[$arrRow["intProgramID"]] = $objProduct;
			}
		}

		function loadProgramMilestones(){
			$arrProgramIDs = array_keys($this->getArray());

			$strSQL = " SELECT tblMilestone.*, tblProgramMilestone.*
				FROM dbPLM.tblProgramMilestone
				INNER JOIN dbPLM.tblMilestone
					ON tblMilestone.intMilestoneID = tblProgramMilestone.intMilestoneID
				WHERE intProgramID IN ('".implode("','", $arrProgramIDs)."')
				ORDER BY tblMilestone.intMilestoneID";
			//echo $strSQL;
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				if($arrRow["intMilestoneID"]) {
					$this->_arrObjects[$arrRow["intProgramID"]]->getProgramMilestoneArray()->addFromRow($arrRow, "intMilestoneID");
					$this->_arrObjects[$arrRow["intProgramID"]]->getProgramMilestoneArray()->getObject($arrRow["intMilestoneID"])->getMilestone()->setVarsFromRow($arrRow);
				}
			}
		}

		function setProgramMilestoneColors($blnSaveToDB=false){
			if(!$this->getArray())
				return false;

			foreach($this->getArray() as $objProgram) {
				$objProgram->setProgramMilestoneColors($blnSaveToDB);
			}
		}

		function loadForIssueListPage() {
			$objSearch = $_SESSION["objSearch"];
			$strSQL = IssueArray::getIssueListPageQuery($objSearch, "Program");

			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				$this->_arrObjects[$arrRow["intProgramID"]] = new Product();
				$this->_arrObjects[$arrRow["intProgramID"]]->setVarsFromRow($arrRow);
				$this->_arrObjects[$arrRow["intProgramID"]]->getProgramCommon()->getPlatform()->setVarsFromRow($arrRow);
			}

			$strSearchValue = $objSearch->getProgramID();
			if($strSearchValue && !isset($this->_arrObjects[$strSearchValue])) { // Search record not in options, add search record
				$this->createNew($strSearchValue)->load($strSearchValue);
			}
		}

		function loadForProgramMilestoneListPage() {
			$objSearch = $_SESSION["objSearch"];
			$strSQL = ProgramMilestoneArray::getProgramMilestoneListPageQuery($objSearch, "Program");

			$rsResult = $this->getDB()->query($strSQL);

			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				$this->_arrObjects[$arrRow["intProgramID"]] = new Product();
				$this->_arrObjects[$arrRow["intProgramID"]]->setVarsFromRow($arrRow);
				$this->_arrObjects[$arrRow["intProgramID"]]->getProgramCommon()->getPlatform()->setVarsFromRow($arrRow);
			}

			$strSearchValue = $objSearch->getProgramID();
			if($strSearchValue && !isset($this->_arrObjects[$strSearchValue])) { // Search record not in options, add search record
				$this->createNew($strSearchValue)->load($strSearchValue);
			}
		}

		function loadForIssueDetail() {
			$strSQL = "SELECT tblPlatform.*, tblProduct.*, tblProgramCommon.*
				FROM dbPLM.tblProduct
				INNER JOIN dbPLM.tblProgramCommon
					ON tblProduct.intProgramCommonID = tblProgramCommon.intProgramCommonID
				LEFT JOIN dbPLM.tblPlatform
					ON tblProgramCommon.intPlatformID = tblPlatform.intPlatformID
				ORDER BY strPlatform, strProgramName ";
			//echo $strSQL;
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				$this->_arrObjects[$arrRow["intProgramID"]] = new Product();
				$this->_arrObjects[$arrRow["intProgramID"]]->setVarsFromRow($arrRow);
				$this->_arrObjects[$arrRow["intProgramID"]]->getProgramCommon()->getPlatform()->setVarsFromRow($arrRow);
			}
		}

		function loadGroupByProductTypeID($intCustomerID=null, $intProductTypeID=null){
			$strSQL = "SELECT tblProduct.*
				FROM dbPLM.tblProduct
				INNER JOIN dbPLM.tblProductType
					ON tblProductType.intProductTypeID = tblProduct.intProductTypeID
				LEFT JOIN dbPLM.tblProgramCommon
					ON tblProgramCommon.intProgramCommonID = tblProduct.intProgramCommonID
				WHERE !blnAlwaysNewProduct";
			if($intCustomerID)
				$strSQL .= " AND tblProduct.intCustomerID = ".self::getDB()->sanitize($intCustomerID);
			if($intProductTypeID)
				$strSQL .= " AND tblProduct.intProductTypeID = ".self::getDB()->sanitize($intProductTypeID);
			$strSQL .= " ORDER BY strProgramNumber, strProgramName ";
			//echo $strSQL;
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				if($arrRow["intProductTypeID"]) {
					$this->_arrObjects[$arrRow["intProductTypeID"]][$arrRow["intProgramID"]] = new Product();
					$this->_arrObjects[$arrRow["intProductTypeID"]][$arrRow["intProgramID"]]->setVarsFromRow($arrRow);
				}
			}
		}

		function loadForProductSelect($intProgramCommonID, $intProductTypeID, $intCustomerID, $arrProductIDs=null){
			$strSQL = "SELECT tblProgramProduct.*, tblProduct.*
				FROM dbPLM.tblProduct
				LEFT JOIN dbPLM.tblProgramProduct

					ON tblProgramProduct.intProductID = tblProduct.intProgramID
					AND tblProgramProduct.intProgramCommonID = ".self::getDB()->sanitize($intProgramCommonID)."
				WHERE intProductTypeID = ".self::getDB()->sanitize($intProductTypeID)."
				AND intCustomerID = ".self::getDB()->sanitize($intCustomerID);
			if(is_array($arrProductIDs)) {
				$strSQL .= " AND intProgramID IN ('".implode("','", $arrProductIDs)."')";
			}
			$strSQL .= " ORDER BY strProgramName ";
			//echo $strSQL;
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				$this->_arrObjects[$arrRow["intProgramID"]] = new Product();
				$this->_arrObjects[$arrRow["intProgramID"]]->setVarsFromRow($arrRow);
				//$this->_arrObjects[$arrRow["intProgramID"]]->getProgramProduct()->setVarsFromRow($arrRow);
			}
		}

		function getPurchasedProductListPageQuery($strFilter){
			$objSearch = (isset($_SESSION["objSearch"])?$_SESSION["objSearch"]:new Search()); // For cron jobs that use all products

			$strTableSQL = $strWhereSQL = "";
			$strSelectSQL = " tblProductType.*, tblProduct.* ";
			if($objSearch) {
				if($objSearch->getProductTypeID() && $strFilter != "Product Type") {
					$strWhereSQL .= " AND tblProduct.intProductTypeID = ".Database::sanitize($objSearch->getProductTypeID());
				}
				if($objSearch->getProductName()) {
					$strWhereSQL .= " AND (tblProduct.strProgramName LIKE ".Database::sanitize("%".$objSearch->getProductName()."%")."
 						OR tblProduct.strProgramNumber LIKE ".Database::sanitize("%".$objSearch->getProductName()."%").")";
				}
			}
			switch($strFilter) {
				case "Product Type":
					$strSelectSQL = " DISTINCT tblProductType.intProductTypeID, tblProductType.strProductType ";
					$strOrderBySQL = " ORDER BY tblProductType.strProductType ";
					break;
				case "Product":
					$strSelectSQL = " tblOrganization.*, tblSupplier.*, tblProgram.strProgramName strTopLevelProgramName, tblPlatform.*, tblProgramCommon.*, tblProgramProduct.*, " . $strSelectSQL;
					$strTableSQL = "
						INNER JOIN dbPLM.tblProgramProduct
							ON tblProduct.intProgramID = tblProgramProduct.intProductID
						INNER JOIN dbPLM.tblProgramCommon
							ON tblProgramCommon.intProgramCommonID = tblProgramProduct.intProgramCommonID
						INNER JOIN dbPLM.tblPlatform
							ON tblPlatform.intPlatformID = tblProgramCommon.intPlatformID
						INNER JOIN dbPLM.tblProduct tblProgram
							ON tblProgram.intProgramID = tblProgramCommon.intTopLevelProductID
						LEFT JOIN dbPLM.tblSupplier
							ON tblSupplier.intSupplierID = tblProgramProduct.intSupplierID
						LEFT JOIN dbPLM.tblOrganization
							ON tblOrganization.intOrganizationID = tblSupplier.intOrganizationID
					";
					$strOrderBySQL = $objSearch->getPurchasedProductListPage()->getSortQuery();
					break;
			}

			$strSQL = " SELECT $strSelectSQL
				FROM dbPLM.tblProduct
				INNER JOIN dbPLM.tblProductType
					ON tblProductType.intProductTypeID = tblProduct.intProductTypeID
				$strTableSQL

				LEFT JOIN dbPLM.tblProgramCommon tblProgramCommon2
					ON tblProgramCommon2.intTopLevelProductID = tblProduct.intProgramID
				WHERE tblProgramCommon2.intProgramCommonID IS NULL
				AND tblProduct.intProductTypeID NOT IN ('".intPRODUCT_TYPE_ID_ASSEMBLY."','".intPRODUCT_TYPE_ID_PLASTIC_PART."','".intPRODUCT_TYPE_ID_METAL_PART."')
				$strWhereSQL
				$strOrderBySQL ";

			//echo $strSQL."<br /><br />";
			return $strSQL;
		}

		function loadForPurchasedProductManagement(){
			$strSQL = ProductArray::getPurchasedProductListPageQuery("Product");

			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				if(!isset($this->_arrObjects[$arrRow["intProgramID"]])) {
					$this->_arrObjects[$arrRow["intProgramID"]] = new Product();
					$this->_arrObjects[$arrRow["intProgramID"]]->setVarsFromRow($arrRow);
					$this->_arrObjects[$arrRow["intProgramID"]]->getProductType()->setVarsFromRow($arrRow);
				}
				//$this->_arrObjects[$arrRow["intProgramID"]]->getProgramProductArray()->addFromRow($arrRow, "intProgramProductID");
				//$this->_arrObjects[$arrRow["intProgramID"]]->getProgramProductArray()->getObject($arrRow["intProgramProductID"])->getProgramCommon()->setVarsFromRow($arrRow);
				//$this->_arrObjects[$arrRow["intProgramID"]]->getProgramProductArray()->getObject($arrRow["intProgramProductID"])->getProgramCommon()->getPlatform()->setVarsFromRow($arrRow);
				//$this->_arrObjects[$arrRow["intProgramID"]]->getProgramProductArray()->getObject($arrRow["intProgramProductID"])->getProgramCommon()->getTopLevelProduct()->setVarsFromRow($arrRow, "TopLevel");
				//$this->_arrObjects[$arrRow["intProgramID"]]->getProgramProductArray()->getObject($arrRow["intProgramProductID"])->getSupplier()->setVarsFromRow($arrRow, "TopLevel");
			}
		}

		function getManufacturedProductListPageQuery($strFilter){
			$objSearch = (isset($_SESSION["objSearch"])?$_SESSION["objSearch"]:new Search()); // For cron jobs that use all products

			$strTableSQL = $strWhereSQL = $strOrderBySQL = "";
			$strSelectSQL = " tblProductType.*, tblProduct.* ";
			if($objSearch) {
				if($objSearch->getProductTypeID() && $strFilter != "Product Type") {
					$strWhereSQL .= " AND tblProduct.intProductTypeID = ".Database::sanitize($objSearch->getProductTypeID());
				}
				if($objSearch->getProductName()) {
					$strWhereSQL .= " AND (tblProduct.strProgramName LIKE ".Database::sanitize("%".$objSearch->getProductName()."%")."
 						OR tblProduct.strProgramNumber LIKE ".Database::sanitize("%".$objSearch->getProductName()."%").")";
				}
			}
			switch($strFilter) {
				case "Product Type":
					$strSelectSQL = " DISTINCT tblProductType.intProductTypeID, tblProductType.strProductType ";
					$strOrderBySQL = " ORDER BY tblProductType.strProductType ";
					break;
				case "Product":
					$strSelectSQL = " tblFacility.*, tblTopLevelProduct.intProgramID intTopLevelProgramID, tblTopLevelProduct.strProgramName strTopLevelProgramName,
						tblPlatform.*, tblProgramCommon.*, tblProgramProduct.*, " . $strSelectSQL;
					$strTableSQL = "
						INNER JOIN dbPLM.tblProgramCommon
							ON tblProgramCommon.intProgramCommonID = tblProduct.intProgramCommonID
						INNER JOIN dbPLM.tblPlatform
							ON tblPlatform.intPlatformID = tblProgramCommon.intPlatformID
						INNER JOIN dbPLM.tblProduct tblTopLevelProduct
							ON tblProgramCommon.intTopLevelProductID = tblTopLevelProduct.intProgramID
						LEFT JOIN dbPLM.tblProgramProduct
							ON tblProduct.intProgramID = tblProgramProduct.intProductID
							AND tblProgramCommon.intProgramCommonID = tblProgramProduct.intProgramCommonID
						LEFT JOIN dbPLM.tblProductXR
							ON tblProduct.intProgramID = tblProductXR.intChildProgramID
						LEFT JOIN dbPLM.tblFacility
							ON tblProductXR.intProductionFacilityID = tblFacility.intFacilityID
					";
					$strWhereSQL .= " AND tblProduct.intProgramID != tblTopLevelProduct.intProgramID ";
					$strOrderBySQL = $objSearch->getManufacturedProductListPage()->getSortQuery();

					break;
			}

			$strSQL = " SELECT $strSelectSQL
				FROM dbPLM.tblProduct
				INNER JOIN dbPLM.tblProductType
					ON tblProductType.intProductTypeID = tblProduct.intProductTypeID
				$strTableSQL
				WHERE tblProduct.intProductTypeID IN ('".intPRODUCT_TYPE_ID_ASSEMBLY."','".intPRODUCT_TYPE_ID_PLASTIC_PART."','".intPRODUCT_TYPE_ID_METAL_PART."')
				$strWhereSQL
				$strOrderBySQL ";
			//echo $strSQL."<br /><br />";
			return $strSQL;
		}

		function loadForManufacturedProductManagement(){
			$strSQL = ProductArray::getManufacturedProductListPageQuery("Product");

			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				if(!isset($this->_arrObjects[$arrRow["intProgramID"]])) {
					$this->_arrObjects[$arrRow["intProgramID"]] = new Product();
					$this->_arrObjects[$arrRow["intProgramID"]]->setVarsFromRow($arrRow);
					$this->_arrObjects[$arrRow["intProgramID"]]->getProductType()->setVarsFromRow($arrRow);
					//$this->_arrObjects[$arrRow["intProgramID"]]->getProgramProduct()->setVarsFromRow($arrRow);
					$this->_arrObjects[$arrRow["intProgramID"]]->getProgramCommon()->setVarsFromRow($arrRow);
					$this->_arrObjects[$arrRow["intProgramID"]]->getProgramCommon()->getTopLevelProduct()->setVarsFromRow($arrRow, "TopLevel");
					$this->_arrObjects[$arrRow["intProgramID"]]->getProgramCommon()->getPlatform()->setVarsFromRow($arrRow);
				}
				if(isset($arrRow["intFacilityID"])) {
					$this->_arrObjects[$arrRow["intProgramID"]]->getProductionFacilityArray()->addFromRow($arrRow, "intFacilityID");
				}
			}
		}

		function loadByCustomerID($intCustomerID, $intOmitProgramID) {
			if(!$intCustomerID)
				return;

			$strSQL = "SELECT DISTINCT tblProduct.intProgramID, tblProduct.strProgramName, tblProduct.strProgramNumber
				FROM dbPLM.tblProduct
				INNER JOIN dbPLM.tblProgramCommon
					ON tblProgramCommon.intProgramCommonID = tblProduct.intProgramCommonID
				INNER JOIN dbPLM.tblProgramMilestone
					ON tblProgramMilestone.intProgramID = tblProduct.intProgramID
				WHERE tblProgramCommon.intCustomerID = ".self::getDB()->sanitize($intCustomerID)."
				AND tblProduct.intProgramID != ".self::getDB()->sanitize($intOmitProgramID)."
				ORDER BY strProgramName ";
			//echo $strSQL;
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				$this->_arrObjects[$arrRow["intProgramID"]] = new Product();
				$this->_arrObjects[$arrRow["intProgramID"]]->setVarsFromRow($arrRow);
			}
		}

		function loadByPlatformID($intPlatformID) {
			if(!$intPlatformID)
				return;

			$strSQL = "SELECT DISTINCT tblProduct.intProgramID, tblProduct.strProgramName, tblProduct.strProgramNumber
				FROM dbPLM.tblProduct
				INNER JOIN dbPLM.tblProgramCommon
					ON tblProgramCommon.intProgramCommonID = tblProduct.intProgramCommonID
				INNER JOIN dbPLM.tblProgramMilestone
					ON tblProgramMilestone.intProgramID = tblProduct.intProgramID
				WHERE tblProgramCommon.intPlatformID = ".self::getDB()->sanitize($intPlatformID)."
				ORDER BY strProgramName ";
			//echo $strSQL;
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				$this->_arrObjects[$arrRow["intProgramID"]] = new Product();
				$this->_arrObjects[$arrRow["intProgramID"]]->setVarsFromRow($arrRow);
			}
		}

		function loadByIDs($arrIDs){
			if(!$arrIDs)
				return;

			$strSQL = " SELECT * FROM tblProduct
				WHERE intProgramID IN ('".implode("','", $arrIDs)."')";
			//echo $strSQL;
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				$this->_arrObjects[$arrRow["intProgramID"]] = new Product();
				$this->_arrObjects[$arrRow["intProgramID"]]->setVarsFromRow($arrRow);
			}
		}

		function getECAffectedProducts($intEngineeringChangeID) {
			if(!$intEngineeringChangeID)
				return;

			$strSQL = "SELECT tblProduct.*
				FROM dbPLM.tblProduct
				INNER JOIN dbEngineeringChange.tblProductECXR
					ON tblProductECXR.intProductID = tblProduct.intProgramID
				WHERE tblProductECXR.intEngineeringChangeID = ".self::getDB()->sanitize($intEngineeringChangeID);
			$rsResult = self::getDB()->query($strSQL);
			while($arrRow = self::getDB()->fetch_assoc($rsResult)) {
				$this->_arrObjects[$arrRow["intProgramID"]] = new Product();
				$this->_arrObjects[$arrRow["intProgramID"]]->setVarsFromRow($arrRow);
			}

			$objEC = new EngineeringChange($intEngineeringChangeID);

			include_once("classes/Change.class.php");
			$objChangeArray = new ChangeArray();
			$objChangeArray->loadChanges($objEC->getProgramCommonID(), $objEC);
			$this->applyChanges($objChangeArray, $objEC);
		}
	}

	require_once("DataClass.class.php");

	class ProductBase extends DataClass {
		protected $_intProgramID;
		protected $_intProgramCommonID;
		protected $_strProgramNumber;
		protected $_strProgramName;
		protected $_intProductTypeID;
		protected $_intUserID;
		protected $_strPiecePricePONumber;
		protected $_strPiecePricePOURL;
		protected $_strToolingContractNumber;
		protected $_strToolingContractURL;
		protected $_intBOMDocumentID;
		protected $_intPrintDocumentID;
		protected $_intISODocumentID;
		protected $_intCustomerID;
		protected $_intContainerFacilityID;
		protected $_intApprovalProcessInstanceID;
		protected $_dtmCreatedOn;

		function __construct($intProductID=null) {
			$this->DataClass(intTABLE_ID_PRODUCT);
			if($intProductID) {
				$this->load($intProductID);

			}
		}

		protected function insert() {
			base::write_log("Product created","S");
			$strSQL = "INSERT INTO dbPLM.tblProduct SET ";
			$strConnector = "";
			$strSQL .= $strConnector . "intProgramID = ".$this->getDB()->sanitize(self::getProgramID());
			$strConnector = ",";
			if(isset($this->_intProgramCommonID)) {
				$strSQL .= $strConnector . "intProgramCommonID = ".$this->getDB()->sanitize(self::getProgramCommonID());
				$strConnector = ",";
			}
			if(isset($this->_strProgramNumber)) {
				$strSQL .= $strConnector . "strProgramNumber = ".$this->getDB()->sanitize(self::getProgramNumber());
				$strConnector = ",";
			}
			if(isset($this->_strProgramName)) {
				$strSQL .= $strConnector . "strProgramName = ".$this->getDB()->sanitize(self::getProgramName());
				$strConnector = ",";
			}
			if(isset($this->_intProductTypeID)) {
				$strSQL .= $strConnector . "intProductTypeID = ".$this->getDB()->sanitize(self::getProductTypeID());
				$strConnector = ",";
			}
			if(isset($this->_intUserID)) {
				$strSQL .= $strConnector . "intUserID = ".$this->getDB()->sanitize(self::getUserID());
				$strConnector = ",";
			}
			if(isset($this->_strPiecePricePONumber)) {
				$strSQL .= $strConnector . "strPiecePricePONumber = ".$this->getDB()->sanitize(self::getPiecePricePONumber());
				$strConnector = ",";
			}
			if(isset($this->_strPiecePricePOURL)) {
				$strSQL .= $strConnector . "strPiecePricePOURL = ".$this->getDB()->sanitize(self::getPiecePricePOURL());
				$strConnector = ",";
			}
			if(isset($this->_strToolingContractNumber)) {
				$strSQL .= $strConnector . "strToolingContractNumber = ".$this->getDB()->sanitize(self::getToolingContractNumber());
				$strConnector = ",";
			}
			if(isset($this->_strToolingContractURL)) {
				$strSQL .= $strConnector . "strToolingContractURL = ".$this->getDB()->sanitize(self::getToolingContractURL());
				$strConnector = ",";
			}
			if(isset($this->_intBOMDocumentID)) {
				$strSQL .= $strConnector . "intBOMDocumentID = ".$this->getDB()->sanitize(self::getBOMDocumentID());
				$strConnector = ",";
			}
			if(isset($this->_intPrintDocumentID)) {
				$strSQL .= $strConnector . "intPrintDocumentID = ".$this->getDB()->sanitize(self::getPrintDocumentID());
				$strConnector = ",";
			}
			if(isset($this->_intISODocumentID)) {
				$strSQL .= $strConnector . "intISODocumentID = ".$this->getDB()->sanitize(self::getISODocumentID());
				$strConnector = ",";
			}
			if(isset($this->_intCustomerID)) {
				$strSQL .= $strConnector . "intCustomerID = ".$this->getDB()->sanitize(self::getCustomerID());
				$strConnector = ",";
			}
			if(isset($this->_intContainerFacilityID) || @is_null($this->_intContainerFacilityID)) {
				$strSQL .= $strConnector . "intContainerFacilityID = ".$this->getDB()->sanitize(self::getContainerFacilityID());
				$strConnector = ",";
			}
			if(isset($this->_intApprovalProcessInstanceID)) {
				$strSQL .= $strConnector . "intApprovalProcessInstanceID = ".$this->getDB()->sanitize(self::getApprovalProcessInstanceID());
				$strConnector = ",";
			}
			if(isset($this->_dtmCreatedOn)) {
				$strSQL .= $strConnector . "dtmCreatedOn = ".$this->getDB()->sanitize(self::getCreatedOn());
				$strConnector = ",";
			}
			//echo $strSQL . "<br />";
			$this->getDB()->query($strSQL);
			$this->setProgramID($this->getDB()->insert_id());

			parent::insert();

			return $this->getProgramID();
		}

		protected function update() {
			parent::update();
			base::write_log("Product Update","S");
			$strSQL = "UPDATE dbPLM.tblProduct SET ";
			$strConnector = "";
			$strSQL .= $strConnector . "intProgramID = ".$this->getDB()->sanitize(self::getProgramID());
			$strConnector = ",";
			if(isset($this->_intProgramCommonID)) {
				$strSQL .= $strConnector . "intProgramCommonID = ".$this->getDB()->sanitize(self::getProgramCommonID());
				$strConnector = ",";
			}
			if(isset($this->_strProgramNumber)) {
				$strSQL .= $strConnector . "strProgramNumber = ".$this->getDB()->sanitize(self::getProgramNumber());
				$strConnector = ",";
			}
			if(isset($this->_strProgramName)) {

				$strSQL .= $strConnector . "strProgramName = ".$this->getDB()->sanitize(self::getProgramName());
				$strConnector = ",";
			}
			if(isset($this->_intProductTypeID)) {
				$strSQL .= $strConnector . "intProductTypeID = ".$this->getDB()->sanitize(self::getProductTypeID());
				$strConnector = ",";
			}
			if(isset($this->_intUserID)) {
				$strSQL .= $strConnector . "intUserID = ".$this->getDB()->sanitize(self::getUserID());
				$strConnector = ",";
			}
			if(isset($this->_strPiecePricePONumber)) {
				$strSQL .= $strConnector . "strPiecePricePONumber = ".$this->getDB()->sanitize(self::getPiecePricePONumber());
				$strConnector = ",";
			}
			if(isset($this->_strPiecePricePOURL)) {
				$strSQL .= $strConnector . "strPiecePricePOURL = ".$this->getDB()->sanitize(self::getPiecePricePOURL());
				$strConnector = ",";
			}
			if(isset($this->_strToolingContractNumber)) {
				$strSQL .= $strConnector . "strToolingContractNumber = ".$this->getDB()->sanitize(self::getToolingContractNumber());
				$strConnector = ",";
			}
			if(isset($this->_strToolingContractURL)) {
				$strSQL .= $strConnector . "strToolingContractURL = ".$this->getDB()->sanitize(self::getToolingContractURL());
				$strConnector = ",";
			}
			if(isset($this->_intBOMDocumentID)) {
				$strSQL .= $strConnector . "intBOMDocumentID = ".$this->getDB()->sanitize(self::getBOMDocumentID());
				$strConnector = ",";
			}
			if(isset($this->_intPrintDocumentID)) {
				$strSQL .= $strConnector . "intPrintDocumentID = ".$this->getDB()->sanitize(self::getPrintDocumentID());
				$strConnector = ",";
			}
			if(isset($this->_intISODocumentID)) {
				$strSQL .= $strConnector . "intISODocumentID = ".$this->getDB()->sanitize(self::getISODocumentID());
				$strConnector = ",";
			}
			if(isset($this->_intCustomerID)) {
				$strSQL .= $strConnector . "intCustomerID = ".$this->getDB()->sanitize(self::getCustomerID());
				$strConnector = ",";
			}
			if(isset($this->_intContainerFacilityID) || @is_null($this->_intContainerFacilityID)) {
				$strSQL .= $strConnector . "intContainerFacilityID = ".$this->getDB()->sanitize(self::getContainerFacilityID());
				$strConnector = ",";
			}
			if(isset($this->_intApprovalProcessInstanceID)) {
				$strSQL .= $strConnector . "intApprovalProcessInstanceID = ".$this->getDB()->sanitize(self::getApprovalProcessInstanceID());
				$strConnector = ",";
			}
			if(isset($this->_dtmCreatedOn)) {
				$strSQL .= $strConnector . "dtmCreatedOn = ".$this->getDB()->sanitize(self::getCreatedOn());
				$strConnector = ",";
			}
			$strSQL .= " WHERE intProgramID = ".$this->getDB()->sanitize(self::getProgramID())."";
			//echo $strSQL . "<br />";
			$blnReturn = $this->getDB()->query($strSQL);
			return $blnReturn;
		}

		public function save() {
			if($this->_intProgramID) {
				return self::update();
			} else {
				return self::insert();
			}
		}

		public function delete() {
			if($this->_intProgramID) {
				if(DataClass::getTrackChanges()) {
					$blnDeleteRecord = parent::delete();
					if($blnDeleteRecord) {
						$this->delete2();
					}
				} else {
					$this->delete2();
				}
			}
		}

		private function delete2() {
			$strSQL = "DELETE FROM dbPLM.tblProduct
				WHERE intProgramID = '$this->_intProgramID'
				";
			return $this->getDB()->query($strSQL);
		}

		public function load($intProductID) {
			if($intProductID) {
				$strSQL = "SELECT *
					FROM dbPLM.tblProduct
					LEFT JOIN dbPLM.tblProgramCommon
						ON tblProgramCommon.intProgramCommonID = tblProduct.intProgramCommonID
					WHERE intProgramID = '$intProductID'
					LIMIT 1
				";

				$rsResult = $this->getDB()->query($strSQL);
				$arrRow = $this->getDB()->fetch_assoc($rsResult);
				$this->setVarsFromRow($arrRow);
				$this->getProgramCommon()->setVarsFromRow($arrRow);
			}
		}

		function setVarsFromRow($arrRow, $strType="") {
			if(isset($arrRow["int{$strType}ProgramID"])) $this->_intProgramID = $arrRow["int{$strType}ProgramID"];
			if(isset($arrRow["int{$strType}ProgramCommonID"])) $this->_intProgramCommonID = $arrRow["int{$strType}ProgramCommonID"];
			if(isset($arrRow["str{$strType}ProgramNumber"])) $this->_strProgramNumber = $arrRow["str{$strType}ProgramNumber"];
			if(isset($arrRow["str{$strType}ProgramName"])) $this->_strProgramName = $arrRow["str{$strType}ProgramName"];
			if(isset($arrRow["int{$strType}ProductTypeID"])) $this->_intProductTypeID = $arrRow["int{$strType}ProductTypeID"];
			if(isset($arrRow["int{$strType}UserID"])) $this->_intUserID = $arrRow["int{$strType}UserID"];
			if(isset($arrRow["str{$strType}PiecePricePONumber"])) $this->_strPiecePricePONumber = $arrRow["str{$strType}PiecePricePONumber"];
			if(isset($arrRow["str{$strType}PiecePricePOURL"])) $this->_strPiecePricePOURL = $arrRow["str{$strType}PiecePricePOURL"];
			if(isset($arrRow["str{$strType}ToolingContractNumber"])) $this->_strToolingContractNumber = $arrRow["str{$strType}ToolingContractNumber"];
			if(isset($arrRow["str{$strType}ToolingContractURL"])) $this->_strToolingContractURL = $arrRow["str{$strType}ToolingContractURL"];
			if(isset($arrRow["int{$strType}BOMDocumentID"])) $this->_intBOMDocumentID = $arrRow["int{$strType}BOMDocumentID"];
			if(isset($arrRow["int{$strType}PrintDocumentID"])) $this->_intPrintDocumentID = $arrRow["int{$strType}PrintDocumentID"];
			if(isset($arrRow["int{$strType}ISODocumentID"])) $this->_intISODocumentID = $arrRow["int{$strType}ISODocumentID"];
			if(isset($arrRow["int{$strType}CustomerID"])) $this->_intCustomerID = $arrRow["int{$strType}CustomerID"];
			if(isset($arrRow["int{$strType}ContainerFacilityID"])) $this->_intContainerFacilityID = $arrRow["int{$strType}ContainerFacilityID"];
			if(isset($arrRow["int{$strType}ApprovalProcessInstanceID"])) $this->_intApprovalProcessInstanceID = $arrRow["int{$strType}ApprovalProcessInstanceID"];
			if(isset($arrRow["dtm{$strType}CreatedOn"])) $this->_dtmCreatedOn = $arrRow["dtm{$strType}CreatedOn"];
		}

		function getProgramID() {
			return $this->_intProgramID;
		}
		function setProgramID($value) {
			if($this->_intProgramID !== $value) {
				$this->_intProgramID = $value;
				$this->_blnDirty = true;
			}
		}

		function getProgramCommonID() {
			return $this->_intProgramCommonID;
		}
		function setProgramCommonID($value) {
			if($this->_intProgramCommonID !== $value) {
				$this->_intProgramCommonID = $value;
				$this->_blnDirty = true;
			}
		}

		function getProgramNumber() {
			return $this->_strProgramNumber;
		}
		function setProgramNumber($value) {
			if($this->_strProgramNumber !== $value) {
				$this->_strProgramNumber = $value;
				$this->_blnDirty = true;
			}
		}

		function getProgramName() {
			return $this->_strProgramName;

		}
		function setProgramName($value) {
			if($this->_strProgramName !== $value) {
				$this->_strProgramName = $value;
				$this->_blnDirty = true;
			}
		}

		function getProductTypeID() {
			return $this->_intProductTypeID;
		}
		function setProductTypeID($value) {
			if($this->_intProductTypeID !== $value) {
				$this->_intProductTypeID = $value;
				$this->_blnDirty = true;
			}
		}

		function getUserID() {
			return $this->_intUserID;
		}
		function setUserID($value) {
			if($this->_intUserID !== $value) {
				$this->_intUserID = $value;
				$this->_blnDirty = true;
			}
		}

		function getPiecePricePONumber() {
			return $this->_strPiecePricePONumber;
		}
		function setPiecePricePONumber($value) {

			if($this->_strPiecePricePONumber !== $value) {
				$this->_strPiecePricePONumber = $value;
				$this->_blnDirty = true;
			}
		}

		function getPiecePricePOURL() {
			return $this->_strPiecePricePOURL;
		}
		function setPiecePricePOURL($value) {
			if($this->_strPiecePricePOURL !== $value) {
				$this->_strPiecePricePOURL = $value;
				$this->_blnDirty = true;
			}
		}

		function getToolingContractNumber() {
			return $this->_strToolingContractNumber;
		}
		function setToolingContractNumber($value) {
			if($this->_strToolingContractNumber !== $value) {
				$this->_strToolingContractNumber = $value;
				$this->_blnDirty = true;
			}
		}

		function getToolingContractURL() {
			return $this->_strToolingContractURL;
		}
		function setToolingContractURL($value) {
			if($this->_strToolingContractURL !== $value) {
				$this->_strToolingContractURL = $value;
				$this->_blnDirty = true;
			}
		}

		function getBOMDocumentID() {
			return $this->_intBOMDocumentID;
		}
		function setBOMDocumentID($value) {
			if($this->_intBOMDocumentID !== $value) {
				$this->_intBOMDocumentID = $value;
				$this->_blnDirty = true;
			}
		}

		function getPrintDocumentID() {
			return $this->_intPrintDocumentID;
		}
		function setPrintDocumentID($value) {
			if($this->_intPrintDocumentID !== $value) {
				$this->_intPrintDocumentID = $value;
				$this->_blnDirty = true;


			}
		}

		function getISODocumentID() {
			return $this->_intISODocumentID;
		}
		function setISODocumentID($value) {
			if($this->_intISODocumentID !== $value) {
				$this->_intISODocumentID = $value;
				$this->_blnDirty = true;
			}
		}

		function getCustomerID() {
			return $this->_intCustomerID;
		}
		function setCustomerID($value) {
			if($this->_intCustomerID !== $value) {
				$this->_intCustomerID = $value;
				$this->_blnDirty = true;
			}
		}

		function getContainerFacilityID() {
			return $this->_intContainerFacilityID;
		}
		function setContainerFacilityID($value) {
			if($this->_intContainerFacilityID !== $value) {
				$this->_intContainerFacilityID = $value;
				$this->_blnDirty = true;
			}
		}

		function getApprovalProcessInstanceID() {
			return $this->_intApprovalProcessInstanceID;
		}
		function setApprovalProcessInstanceID($value) {
			if($this->_intApprovalProcessInstanceID !== $value) {
				$this->_intApprovalProcessInstanceID = $value;
				$this->_blnDirty = true;
			}
		}

		function getCreatedOn() {
			return $this->_dtmCreatedOn;
		}
		function setCreatedOn($value) {
			if($this->_dtmCreatedOn !== $value) {
				$this->_dtmCreatedOn = $value;

				$this->_blnDirty = true;

			}
		}
	}

	include_once("User.class.php");
	include_once("ProgramCommon.class.php");
	include_once("ProgramData.class.php");
	include_once("Member.class.php");
	include_once("EngineeringChange.class.php");

	class Product extends ProductBase {
		private $_objProgramCommon;
		private $_objProductType;
		private $_objProgramData;
		private $_objFolder;
		private $_objProductToolXRArray;
		private $_objToolArray;
		private $_objProcessArray;
		private $_objParentProductXR; // Used for top level programs, when there is only ONE tblParentProductXR record
		private $_objParentProductXRArray;
		private $_objParentProductArray;
		private $_objChildProductXRArray;
		private $_objConfigurationProductXRArray;
		private $_objProductArray;
		private $_objPackagingArray;
		private $_objUser;
		private $_objMemberArray;
		private $_objProgramMilestoneArray;
		private $_objApprovalProcessInstance;
		private $_objEngineeringChangeArray;
		private $_objBudgetCostArray;
		private $_objBOMDocument;
		private $_objPrintDocument;
		private $_objISODocument;
		private $_objNotification;
		private $_objChangeArray;
		private $_objProgramProduct; // For Mfg Parts, with only one
		private $_objProgramProductArray; // For Purchased Parts, with many
		private $_objProductionFacilityArray; // All production facilities across all uses of a mfg part in a single program
		private $_objContainerArray;

		private $_strProgramManager;
		private $_intConfigurationProductIndex;

		private $_blnIsChild;

		function __construct($intProductID=null) {
			parent::__construct($intProductID);

			$arrTrackFields = array(intCOLUMN_ID_PRODUCT_NAME, intCOLUMN_ID_PRODUCT_NUMBER);
			$this->setTrackFields($arrTrackFields);
		}

		function saveChildren() {
			if(!$this->getChildProductXRArray()->getArray())
				return;

			foreach($this->getChildProductXRArray()->getArray() as $objProductXR) {
				$objProductXR->save();
				$objProductXR->getChildProduct()->save();
				//$objProductXR->getChildProduct()->getProgramProduct()->save();
				$objProductXR->getChildProduct()->saveChildren();
			}
		}

		function loadTopLevelProgramByProgramCommonID($intProgramCommonID) {
			$strSQL = "SELECT tblProduct.*
				FROM tblProduct
				INNER JOIN tblProgramData
					ON tblProduct.intProgramID = tblProgramData.intProgramID
				WHERE tblProduct.intProgramCommonID = ".self::getDB()->sanitize($intProgramCommonID)."
				LIMIT 1 "; // Theoretically this should only return 1 record anyway...

			//echo $strSQL . "<br />";
			$rsResult = self::getDB()->query($strSQL);
			$arrRow = self::getDB()->fetch_assoc($rsResult);
			$this->setVarsFromRow($arrRow);
			//$this->getProgramCommon()->setVarsFromRow($arrRow);
		}

		public function loadForProgramHeaderFull($intProductID) {
			if($intProductID) {
				$strSQL = "SELECT tblUser.*,
					tblPrintDocument.intFolderID intPrintFolderID, tblPrintDocument.strFileName strPrintFileName,
					tblISODocument.intFolderID intISOFolderID, tblISODocument.strFileName strISOFileName,
					tblProgramData.*, tblCurrency.*, tblProgramSubType.*, tblProgramType.*, tblPlatform.*, tblCustomer.*, tblOrganization.*,
					tblProgramProduct.*, tblProductXR.*, tblProductType.*, tblProduct.*, tblProgramCommon.*
				FROM dbPLM.tblProduct
				INNER JOIN dbPLM.tblProgramCommon
					ON tblProgramCommon.intProgramCommonID = tblProduct.intProgramCommonID
				INNER JOIN dbPLM.tblProductType
					ON tblProductType.intProductTypeID = tblProduct.intProductTypeID
				LEFT JOIN dbPLM.tblProductXR
					ON tblProductXR.intChildProgramID = tblProduct.intProgramID
					AND tblProductXR.intParentProgramID IS NULL
				LEFT JOIN dbPLM.tblProgramProduct
					ON tblProduct.intProgramID = tblProgramProduct.intProductID
					AND tblProduct.intProgramCommonID = tblProgramProduct.intProgramCommonID
				LEFT JOIN dbPLM.tblCustomer
					ON tblCustomer.intCustomerID = tblProgramCommon.intCustomerID
				LEFT JOIN dbPLM.tblOrganization
					ON tblCustomer.intOrganizationID = tblOrganization.intOrganizationID
				LEFT JOIN dbPLM.tblPlatform
					ON tblPlatform.intPlatformID = tblProgramCommon.intPlatformID
				LEFT JOIN dbPLM.tblProgramType
					ON tblProgramType.intProgramTypeID = tblProgramCommon.intProgramTypeID
				LEFT JOIN dbPLM.tblProgramSubType
					ON tblProgramSubType.intProgramSubTypeID = tblProgramCommon.intProgramSubTypeID
				LEFT JOIN dbPLM.tblCurrency
					ON tblCurrency.intCurrencyID = tblProgramCommon.intCurrencyID
				LEFT JOIN dbPLM.tblProgramData
					ON tblProgramData.intProgramID = tblProduct.intProgramID
				LEFT JOIN dbDocument.tblDocument tblISODocument
					ON tblProduct.intISODocumentID = tblISODocument.intDocumentID
				LEFT JOIN dbDocument.tblDocument tblPrintDocument
					ON tblProduct.intPrintDocumentID = tblPrintDocument.intDocumentID
				LEFT JOIN dbPLM.tblUser

					ON tblProduct.intUserID = tblUser.intUserID
				WHERE tblProduct.intProgramID = ".self::getDB()->sanitize($intProductID)."
				LIMIT 1
				";
				//echo $strSQL;
				$rsResult = $this->getDB()->query($strSQL);
				$arrRow = $this->getDB()->fetch_assoc($rsResult);
				$this->setVarsFromRow($arrRow);
				$this->getParentProductXR()->setVarsFromRow($arrRow);
				$this->getProductType()->setVarsFromRow($arrRow);
				//$this->getProgramProduct()->setVarsFromRow($arrRow);
				$this->getProgramCommon()->setVarsFromRow($arrRow);
				$this->getProgramCommon()->getCustomer()->setVarsFromRow($arrRow);
				$this->getProgramCommon()->getPlatform()->setVarsFromRow($arrRow);
				$this->getProgramCommon()->getProgramType()->setVarsFromRow($arrRow);
				$this->getProgramCommon()->getProgramSubType()->setVarsFromRow($arrRow);
				//$this->getProgramCommon()->getCurrency()->setVarsFromRow($arrRow);
				$this->getProgramData()->setVarsFromRow($arrRow);
				//$this->getPrintDocument()->setVarsFromRow($arrRow, "Print");
				//$this->getISODocument()->setVarsFromRow($arrRow, "ISO");
				$this->getUser()->setVarsFromRow($arrRow, "ISO");
			}
		}

		public function loadForProgramHeaderEdit($intProductID) {
			if($intProductID) {
				$strSQL = "SELECT
						tblPrintDocument.intFolderID intPrintFolderID, tblPrintDocument.strFileName strPrintFileName,
						tblISODocument.intFolderID intISOFolderID, tblISODocument.strFileName strISOFileName,
						tblProductType.*, tblProgramData.*, tblProduct.*, tblProgramCommon.*
					FROM dbPLM.tblProduct
					INNER JOIN dbPLM.tblProgramCommon
						ON tblProgramCommon.intProgramCommonID = tblProduct.intProgramCommonID
					INNER JOIN dbPLM.tblProductType
						ON tblProductType.intProductTypeID = tblProduct.intProductTypeID
					LEFT JOIN dbPLM.tblProgramData
						ON tblProgramData.intProgramID = tblProduct.intProgramID
					LEFT JOIN dbDocument.tblDocument tblISODocument
						ON tblProduct.intISODocumentID = tblISODocument.intDocumentID
					LEFT JOIN dbDocument.tblDocument tblPrintDocument
						ON tblProduct.intPrintDocumentID = tblPrintDocument.intDocumentID
					WHERE tblProduct.intProgramID = ".self::getDB()->sanitize($intProductID)."
					LIMIT 1
				";
				//echo $strSQL;
				$rsResult = $this->getDB()->query($strSQL);
				$arrRow = $this->getDB()->fetch_assoc($rsResult);
				$this->setVarsFromRow($arrRow);

				$this->getProductType()->setVarsFromRow($arrRow);
				$this->getProgramCommon()->setVarsFromRow($arrRow);
				$this->getProgramData()->setVarsFromRow($arrRow);
				//$this->getPrintDocument()->setVarsFromRow($arrRow, "Print");
				//$this->getISODocument()->setVarsFromRow($arrRow, "ISO");
			}
		}

		function loadForProgramHeader($intProductID){
			if(!$intProductID)
				return;

			$strSQL = "SELECT tblOrganization.*, tblCustomer.*, tblPlatform.*, tblProgramCommon.*, tblProduct.*
				FROM dbPLM.tblProduct
				INNER JOIN dbPLM.tblProgramCommon
					ON tblProgramCommon.intProgramCommonID = tblProduct.intProgramCommonID
				INNER JOIN dbPLM.tblPlatform
					ON tblPlatform.intPlatformID = tblProgramCommon.intPlatformID
				INNER JOIN dbPLM.tblCustomer
					ON tblCustomer.intCustomerID = tblProgramCommon.intCustomerID
				INNER JOIN dbPLM.tblOrganization
					ON tblCustomer.intOrganizationID = tblOrganization.intOrganizationID
				WHERE tblProduct.intProgramID = ".self::getDB()->sanitize($intProductID)."
				LIMIT 1
			";
			//echo $strSQL;
			$rsResult = $this->getDB()->query($strSQL);
			$arrRow = $this->getDB()->fetch_assoc($rsResult);
			$this->setVarsFromRow($arrRow);
			$this->getProgramCommon()->setVarsFromRow($arrRow);
			$this->getProgramCommon()->getPlatform()->setVarsFromRow($arrRow);
			$this->getProgramCommon()->getCustomer()->setVarsFromRow($arrRow);
			//$this->getParentProductXR()->getProductionFacility()->setVarsFromRow($arrRow);
		}

		function loadByPlatformIDAndProgramName($intPlatformID, $strProgramName) {
			if(!$intPlatformID || !$strProgramName) {
				return false;
			}


			$strSQL = "SELECT *
				FROM dbPLM.tblProduct
				INNER JOIN dbPLM.tblProgramCommon
					ON tblProgramCommon.intProgramCommonID = tblProduct.intProgramCommonID
				WHERE tblProgramCommon.intPlatformID = ".self::getDB()->sanitize($intPlatformID)."
				AND strProgramName = ".self::getDB()->sanitize($strProgramName)."
				LIMIT 1

			";
			//echo $strSQL . "<br />";
			$rsResult = $this->getDB()->query($strSQL);
			$arrRow = $this->getDB()->fetch_assoc($rsResult);

			$this->setVarsFromRow($arrRow);
		}

		function loadForProductEdit($intProductID, $intProgramCommonID) {
			if(!$intProductID || !$intProgramCommonID)
				return;

			$strSQL = "SELECT tblProgramProduct.*, tblProgramCommon.*, tblProductType.*, tblProduct.*
				FROM dbPLM.tblProduct
				LEFT JOIN dbPLM.tblProductType
					ON tblProductType.intProductTypeID = tblProduct.intProductTypeID
				LEFT JOIN dbPLM.tblProgramCommon
					ON tblProgramCommon.intProgramCommonID = tblProduct.intProgramCommonID
				LEFT JOIN dbPLM.tblProgramProduct
					ON tblProgramProduct.intProgramCommonID = ".self::getDB()->sanitize($intProgramCommonID)."
					AND tblProgramProduct.intProductID = tblProduct.intProgramID
				WHERE tblProduct.intProgramID = ".self::getDB()->sanitize($intProductID)."
				LIMIT 1
			";
			$rsResult = $this->getDB()->query($strSQL);
			$arrRow = $this->getDB()->fetch_assoc($rsResult);
			$this->setVarsFromRow($arrRow);
			$this->getProductType()->setVarsFromRow($arrRow);
			//$this->getProgramProduct()->setVarsFromRow($arrRow);
			$this->getProgramCommon()->setVarsFromRow($arrRow);
		}

		function loadByProductNumber($strProductNumber) {
			if(!$strProductNumber) {
				return false;
			}

			$strSQL = "SELECT *
				FROM dbPLM.tblProduct
				WHERE strProgramNumber = ".self::getDB()->sanitize($strProductNumber)."
				LIMIT 1
			";
			//echo $strSQL . "<br />";
			$rsResult = self::getDB()->query($strSQL);
			$arrRow = self::getDB()->fetch_assoc($rsResult);
			$this->setVarsFromRow($arrRow);

			return isset($arrRow["intProductID"])?$arrRow["intProductID"]:null;
		} // TODO: Add unique key constraint on (strProductNumber) - SHOULD ONLY HAPPEN IF THIS IS AN INTERNAL PART NUMBER. Need to incorperate IPN and customer based CPN

		function loadChildrenRecursivelyProgramOnly($intProgramID) {
			include_once("ProductXR.class.php");
			if(!$intProgramID)
				return false;

			$strSQL = "SELECT tblProductType.*, tblProduct.*, tblProductXR.*
					FROM tblProductXR
					INNER JOIN tblProduct
						ON tblProductXR.intChildProgramID = tblProduct.intProgramID
					INNER JOIN tblProductType
						ON tblProduct.intProductTypeID = tblProductType.intProductTypeID
					WHERE tblProductXR.intParentProgramID = ".self::getDB()->sanitize($intProgramID)."
				";
			//echo $strSQL . "<br />";
			$rsResult = $this->getDB()->query($strSQL);

			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				$objProductXR = new ProductXR();
				$objProductXR->setVarsFromRow($arrRow);
				$objProductXR->getChildProduct()->setVarsFromRow($arrRow);
				$objProductXR->getChildProduct()->getProductType()->setVarsFromRow($arrRow);
				$objProductXR->getChildProduct()->loadChildrenRecursivelyProgramOnly($arrRow["intProgramID"]);
				$this->getChildProductXRArray()->setObject($arrRow["intProgramID"], $objProductXR);
			}
		}

		function loadChildrenRecursively() {
			include_once("ProductXR.class.php");
			if(!$this->getProgramID())
				return false;

			$strSQL = "SELECT tblProductType.*, tblProduct.*, tblProductXR.*
				FROM tblProductXR
				INNER JOIN tblProduct
					ON tblProductXR.intChildProgramID = tblProduct.intProgramID
				LEFT JOIN tblProductType
					ON tblProductType.intProductTypeID = tblProduct.intProductTypeID
				WHERE tblProductXR.intParentProgramID = ".self::getDB()->sanitize($this->getProgramID())."
				ORDER BY tblProductXR.intDisplayOrder ASC
			";
			//echo $strSQL . "<br />";
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				$objProductXR = new ProductXR();
				$objProductXR->setVarsFromRow($arrRow);
				$objProductXR->getChildProduct()->setVarsFromRow($arrRow);
				$objProductXR->getChildProduct()->getProductType()->setVarsFromRow($arrRow);
				$objProductXR->getChildProduct()->loadChildrenRecursively();
				$this->getChildProductXRArray()->setObject($arrRow["intProgramID"], $objProductXR);
			}
		}

		function loadChildrenRecursivelyForBOM($intProgramCommonID, $blnClearProductID=false) {
			include_once("ProductXR.class.php");
			if(!$this->getProgramID() || !$intProgramCommonID)
				return false;

			$strSQL = "SELECT tblOrganization.*, tblProgramProduct.*, tblShipToFacility.strFacilityName strShipToFacilityName, tblProductionFacility.strFacilityName strProductionFacilityName,
					tblProductType.*, tblProduct.*, tblProductXR.*
				FROM tblProductXR
				INNER JOIN tblProduct
					ON tblProductXR.intChildProgramID = tblProduct.intProgramID
				LEFT JOIN tblProductType
					ON tblProductType.intProductTypeID = tblProduct.intProductTypeID
				LEFT JOIN dbPLM.tblFacility tblShipToFacility
					ON tblShipToFacility.intFacilityID = tblProductXR.intShipToFacilityID
				LEFT JOIN dbPLM.tblFacility tblProductionFacility
					ON tblProductionFacility.intFacilityID = tblProductXR.intProductionFacilityID
				LEFT JOIN dbPLM.tblProgramProduct
					ON tblProgramProduct.intProductID = tblProduct.intProgramID
					AND tblProgramProduct.intProgramCommonID = ".self::getDB()->sanitize($intProgramCommonID)."
				LEFT JOIN dbPLM.tblSupplier
					ON tblSupplier.intSupplierID = tblProgramProduct.intSupplierID
				LEFT JOIN dbPLM.tblOrganization
					ON tblSupplier.intOrganizationID = tblOrganization.intOrganizationID
				WHERE tblProductXR.intParentProgramID = ".self::getDB()->sanitize($this->getProgramID())."
				ORDER BY tblProductXR.intDisplayOrder ASC
			";
			//echo $strSQL . "<br />";
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				$objProductXR = new ProductXR();
				$objProductXR->setVarsFromRow($arrRow);
				$objProductXR->getChildProduct()->setVarsFromRow($arrRow);
				$objProductXR->getChildProduct()->getProductType()->setVarsFromRow($arrRow);
				$objProductXR->getShipToFacility()->setVarsFromRow($arrRow, "ShipTo");
				$objProductXR->getProductionFacility()->setVarsFromRow($arrRow, "Production");
				//$objProductXR->getChildProduct()->getProgramProduct()->setVarsFromRow($arrRow);
				//$objProductXR->getChildProduct()->getProgramProduct()->getSupplier()->setVarsFromRow($arrRow);
				$objProductXR->getChildProduct()->loadChildrenRecursivelyForBOM($intProgramCommonID);
				$objProductXR->getChildProduct()->getParentProductXRArray()->addNew($objProductXR);
				$this->getChildProductXRArray()->setObject($arrRow["intProgramID"], $objProductXR);
				if($blnClearProductID) {
					$objProductXR->setProductXRID("");
					$objProductXR->getChildProduct()->setProductID("");
					$objProductXR->getChildProduct()->setNew(1);
				}
			}
			//if($blnClearProductID) $this->setProductID("");

		}

		function loadChildrenRecursivelyWithMilestones() {
			include_once("ProductXR.class.php");
			if(!$this->getProgramID())
				return false;


			$arrProgramProductTypes = array(intPRODUCT_TYPE_ID_ASSEMBLY, intPRODUCT_TYPE_ID_PLASTIC_PART, intPRODUCT_TYPE_ID_METAL_PART);
			$strSQL = "SELECT tblProductType.*, tblProgramMilestone.*, tblMilestone.*, tblProgramCommon.*, tblProduct.*, tblProductXR.*
				FROM tblProductXR
				INNER JOIN tblProduct
					ON tblProductXR.intChildProgramID = tblProduct.intProgramID
				INNER JOIN tblProgramCommon
					ON tblProgramCommon.intProgramCommonID = tblProduct.intProgramCommonID
				LEFT JOIN tblProductType
					ON tblProductType.intProductTypeID = tblProduct.intProductTypeID
				LEFT JOIN tblProgramMilestone
					ON tblProgramMilestone.intProgramID = tblProduct.intProgramID
				LEFT JOIN tblMilestone
					ON tblMilestone.intMilestoneID = tblProgramMilestone.intMilestoneID
				WHERE tblProductXR.intParentProgramID = ".self::getDB()->sanitize($this->getProgramID())."
				AND tblProduct.intProductTypeID IN ('".implode("','", $arrProgramProductTypes)."')
				ORDER BY tblProductXR.intDisplayOrder ASC
			";
			//echo $strSQL . "<br />";
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				if(!$this->getChildProductXRArray()->getObject($arrRow["intProgramID"])) {
					$objProductXR = new ProductXR();
					$objProductXR->setVarsFromRow($arrRow);
					$objProductXR->getChildProduct()->setVarsFromRow($arrRow);
					$objProductXR->getChildProduct()->setIsChild(true);
					$objProductXR->getChildProduct()->getProductType()->setVarsFromRow($arrRow);
					$objProductXR->getChildProduct()->getProgramCommon()->setVarsFromRow($arrRow);

					$objProductXR->getChildProduct()->loadChildrenRecursivelyWithMilestones();
					$this->getChildProductXRArray()->setObject($arrRow["intProgramID"], $objProductXR);
				}
				if($arrRow["intProgramMilestoneID"] && $arrRow["intMilestoneID"]) {
					$this->getChildProductXRArray()->getObject($arrRow["intProgramID"])->getChildProduct()->getProgramMilestoneArray()->addFromRow($arrRow, "intMilestoneID");

					$this->getChildProductXRArray()->getObject($arrRow["intProgramID"])->getChildProduct()->getProgramMilestoneArray()->getObject($arrRow["intMilestoneID"])->getMilestone()->setVarsFromRow($arrRow);
				}
			}
		}

		function loadParentsRecursivelyWithMilestones() {
			include_once("ProductXR.class.php");
			if(!$this->getProgramID())
				return false;

			$strSQL = "SELECT tblProgramMilestone.intApprovalProcessInstanceID intProgramMilestoneApprovalProcessInstanceID, tblProgramMilestone.*, tblMilestone.*, tblProduct.*, tblProductXR.*
				FROM tblProductXR
				INNER JOIN tblProduct
					ON tblProductXR.intParentProgramID = tblProduct.intProgramID
				LEFT JOIN tblProgramMilestone

					ON tblProgramMilestone.intProgramID = tblProduct.intProgramID
				LEFT JOIN tblMilestone
					ON tblMilestone.intMilestoneID = tblProgramMilestone.intMilestoneID
				WHERE tblProductXR.intChildProgramID = ".self::getDB()->sanitize($this->getProgramID())."
			";
			//echo $strSQL . "<br />";
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				if(!$this->getParentProductXRArray()->getObject($arrRow["intProgramID"])) {
					$objProductXR = new ProductXR();
					$objProductXR->setVarsFromRow($arrRow);
					$objProductXR->getParentProduct()->setVarsFromRow($arrRow);
					//$objProductXR->getParentProduct()->setIsChild(true);
					//$objProductXR->getParentProduct()->getProductType()->setVarsFromRow($arrRow);
					//$objProductXR->getParentProduct()->getProgramCommon()->setVarsFromRow($arrRow);
					$objProductXR->getParentProduct()->loadParentsRecursivelyWithMilestones();
					$this->getParentProductXRArray()->setObject($arrRow["intProgramID"], $objProductXR);
				}
				if($arrRow["intProgramMilestoneID"]) {
					$this->getParentProductXRArray()->getObject($arrRow["intProgramID"])->getParentProduct()->getProgramMilestoneArray()->addFromRow($arrRow, "intMilestoneID", "ProgramMilestone");
					$this->getParentProductXRArray()->getObject($arrRow["intProgramID"])->getParentProduct()->getProgramMilestoneArray()->getObject($arrRow["intMilestoneID"])->getMilestone()->setVarsFromRow($arrRow);
				}
			}
		}

		function loadParentsRecursively($intProgramID=null) {
			include_once("ProductXR.class.php");
			if($intProgramID) {
				$this->setProgramID($intProgramID);
			}
			if(!$this->getProgramID()) {
				return false;
			}

			$strSQL = "SELECT tblProduct.*, tblProductXR.*
				FROM tblProductXR
				INNER JOIN tblProduct
					ON tblProductXR.intParentProgramID = tblProduct.intProgramID
				WHERE tblProductXR.intChildProgramID = ".self::getDB()->sanitize($this->getProgramID())."
			";
			//echo $strSQL . "<br />";
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				$objProductXR = new ProductXR();
				$objProductXR->setVarsFromRow($arrRow);
				$objProductXR->getParentProduct()->setVarsFromRow($arrRow);
				$objProductXR->getParentProduct()->loadParentsRecursively();
				$this->getParentProductXRArray()->setObject($arrRow["intProgramID"], $objProductXR);
			}
		}

		function delete() {
			if(!$this->getProductID())
				return;

			$this->loadChildrenRecursivelyProgramOnly($this->getProductID());

			$this->deleteRecursively();
		}

		function deleteRecursively() {
			$this->getParentProductXRArray()->loadByChildProgramID($this->getProductID());
			$blnHasParent = false;
			foreach($this->getParentProductXRArray()->getArray() as $objParentProductXR) {
				if($objParentProductXR->getParentProductID()) { // If the intParentProgramID is NULL, we can still delete the product, because it is a top level product
					$blnHasParent = true;
				}
			}
			if(!$blnHasParent) { // Delete Product: If product has no parent products, delete, and try to delete children.
				parent::delete(); // Delete product

				// Try to delete children
				if(!$this->getChildProductXRArray()->getArray())
					return;
				foreach($this->getChildProductXRArray()->getArray() as $objProductXR) {
					$objProductXR->getChildProduct()->deleteRecursively();
				}
			}
		}

		function setVarsFromRow($arrRow, $strType=""){
			parent::setVarsFromRow($arrRow, $strType);

			if(isset($arrRow["str{$strType}ProgramManager"])) $this->_strProgramManager = $arrRow["str{$strType}ProgramManager"];
		}

		function validate() {
			$strError = "";

			if($this->getProgramID()) { // Existing program
			} else {
				$objProgram = new Product();
				$objProgram->loadByPlatformIDAndProgramName($this->getProgramCommon()->getPlatformID(), $this->getProgramName());
				if($objProgram->getProgramID()) {
					$strError .= "A program with the same platform and name exists.<br />";
				}
			}

			if(strlen($this->getProgramName()) < 2) {
				$strError .= "Program Name must be at least 2 characters long.<br />";
			}


			if($strError) {
				throw new Exception($strError);
			}
		}

		function validateProduct($blnTopLevelProduct) {
			$arrErrors = array();

			if(!$this->getProductTypeID()) {
				$arrErrors[] = "Part Type must be specified.";
			}
			if(strlen($this->getProductName()) < 1) {
				$arrErrors[] = "Part Name must be at least 2 characters long.";
			}

			// Make sure there is at least one configuration quantity for top level part list products.
			if($blnTopLevelProduct) {
				$strQuantityError = "You must specify a quantity for at least one of the configurations.";
				$blnNoQuantities = true;

				if($this->getParentProductXRArray()->getArray()) {
					foreach($this->getParentProductXRArray()->getArray() as $objProductXR) {
						if(is_numeric($objProductXR->getQuantity()) || (DataClass::getTrackChanges() && $objProductXR->getChangesCopy()->getQuantity())) {
							$blnNoQuantities = false;
							break;
						}
					}
				} else {
					$arrErrors[] = $strQuantityError;

				}
				if($blnNoQuantities) {
					$arrErrors[] = $strQuantityError;
				}
			}

			// Check for infinite recursion
			if($this->getProductID()) {
				include_once("ProductXR.class.php");
				// Get child IDs
				$this->loadChildrenRecursively();
				$arrChildIDs = ProductXRArray::getChildIDs($this->getChildProductXRArray());
				$arrChildIDs[] = $this->getProductID();


				// Get parent IDs
				foreach($this->getParentProductXRArray()->getArray() as $objParentProductXR) {
					$objParentProductXR->getParentProduct()->loadParentsRecursively($objParentProductXR->getParentProductID());
				}
				$arrParentIDs = ProductXRArray::getParentIDs($this->getParentProductXRArray());

				if(array_intersect($arrParentIDs, $arrChildIDs)) { // One of the children exist in the parent, infinite recursion detected
					$arrErrors[] = "Cannot add part. You have a loop in your BOM structure. For example Part A is parent to Part B, and Part B is parent to Part A.";
				}
			}

			// Check for duplicate product number
			if(!$this->getProductID() && $this->getProgramNumber()) { // New Product
				$objProduct = new Product();
				$objProduct->loadByProductNumber($this->getProgramNumber());
				if($objProduct->getProductID()) {
					$arrErrors[] = "Duplicate Part Number. Choose 'Select Existing Part', and select ".$this->getProgramNumber()." from the list if you want to use it.";
				}
			}

			return $arrErrors;
		}

		function getID(){
			return $this->getProgramID();
		}
		function setID($value){
			$this->_intProgramID = $value;
		}
		function getName(){
			return /*$this->getProgramNumber() . ": " . */$this->getProgramCommon(true)->getPlatform(true)->getPlatform() . " " . $this->getProgramName();
		}

		function getProductArrayForTool() {
			$objProductArrayForTool = new ProductArray();
			if(!$this->getProductType()->getAlwaysNewProduct() && $this->getProductTypeID() != intPRODUCT_TYPE_ID_PURCHASED_PART) // This Product is also a Program (or Purchased Part, for Chris Depa, this might not be good)
				return $objProductArrayForTool;
			$objProductArrayForTool->setObject($this->getProductID(), $this);

			if($this->getConfigurationProductXRArray()->getArray()) {
				foreach($this->getConfigurationProductXRArray()->getArray() as $objProductXR) {
					$objProduct = $objProductXR->getChildProduct();
					$objProductArrayForTool->setObject($objProduct->getProductID(), $objProduct);
				}
			}

			if($this->getChildProductXRArray()->getArray()) {
				foreach($this->getChildProductXRArray()->getArray() as $objProductXR) {
					$objProduct = $objProductXR->getChildProduct();
					$objProductArrayForTool->setArray($objProductArrayForTool->getArray() + $objProduct->getProductArrayForTool()->getArray());
				}
			}

			// Sort, TODO: Could be done more efficiently by not ordering during EACH recursion step

			$arrObjects = $objProductArrayForTool->getArray();
			uasort($arrObjects, "Product::sort");
			$objProductArrayForTool->setArray($arrObjects);

			return $objProductArrayForTool;
		}

		function sort($a, $b){
			if ($a->getProductName() == $b->getProductName()) {
				return 0;
			}
			return ($a->getProductName() < $b->getProductName()) ? -1 : 1;
		}

		function getProgramCommon($blnLoad=false){
			if(!$this->_objProgramCommon) {
				$this->_objProgramCommon = new ProgramCommon();
			}
			if(!$this->_objProgramCommon->getProgramCommonID() && $this->getProgramCommonID() && $blnLoad) {
				$this->_objProgramCommon->load($this->getProgramCommonID());
			}
			return $this->_objProgramCommon;
		}
		function getProductType(){
			if(!$this->_objProductType) {
				//$this->_objProductType = new ProductType();
			}
			return $this->_objProductType;
		}
		function getProgramData($blnLoad=false){
			if(!$this->_objProgramData) {
				$this->_objProgramData = new ProgramData();
			}
			if(!$this->_objProgramData->getProgramDataID() && $this->getProgramID() && $blnLoad) {
				$this->_objProgramData->loadByProgramID($this->getProgramID());
			}
			return $this->_objProgramData;
		}
		function getProductToolXRArray(){
			if(!$this->_objProductToolXRArray) {
				$this->_objProductToolXRArray = new ProductToolXRArray();
			}
			return $this->_objProductToolXRArray;
		}
		function getToolArray(){
			if(!$this->_objToolArray) {
				$this->_objToolArray = new ToolArray();
			}
			return $this->_objToolArray;
		}
		function getProcessArray() {
			if(!$this->_objProcessArray) {
				$this->_objProcessArray = new ProcessArray();
			}
			return $this->_objProcessArray;
		}
		function getUser(){
			if(!$this->_objUser) {
				$this->_objUser = new User();
			}
			return $this->_objUser;
		}
		function getParentProductXR() {
			//include_once("ProductXR.class.php");
			if(!$this->_objParentProductXR) {
				//$this->_objParentProductXR = new ProductXR();
			}
			return $this->_objParentProductXR;
		}

		function getChildProductXRArray(){
			include_once("ProductXR.class.php");
			if(!$this->_objChildProductXRArray) {
				$this->_objChildProductXRArray = new ProductXRArray();
			}
			return $this->_objChildProductXRArray;
		}
		function getParentProductXRArray(){
			include_once("ProductXR.class.php");
			if(!$this->_objParentProductXRArray) {
				$this->_objParentProductXRArray = new ProductXRArray();
			}
			return $this->_objParentProductXRArray;
		}
		function getParentProductArray(){
			if(!$this->_objParentProductArray) {
				$this->_objParentProductArray = new ProductArray();
			}
			return $this->_objParentProductArray;
		}
		function getConfigurationProductXRArray(){
			include_once("ProductXR.class.php");
			if(!$this->_objConfigurationProductXRArray) {
				$this->_objConfigurationProductXRArray = new ProductXRArray();
			}
			return $this->_objConfigurationProductXRArray;
		}
		function getProductArray(){
			if(!$this->_objProductArray) {
				$this->_objProductArray = new ProductArray();
			}
			return $this->_objProductArray;
		}
		function getPackagingArray(){
			include_once("Packaging.class.php");
			if(!$this->_objPackagingArray) {
				$this->_objPackagingArray = new PackagingArray();
			}
			return $this->_objPackagingArray;
		}
		function getMemberArray(){
			if(!$this->_objMemberArray) {
				$this->_objMemberArray = new MemberArray();
			}
			return $this->_objMemberArray;
		}
		function getProgramMilestoneArray(){
			if(!$this->_objProgramMilestoneArray) {
				$this->_objProgramMilestoneArray = new ProgramMilestoneArray();
			}
			return $this->_objProgramMilestoneArray;
		}
		function getApprovalProcessInstance(){
			if(!$this->_objApprovalProcessInstance) {
				$this->_objApprovalProcessInstance = new ApprovalProcessInstance();
			}
			return $this->_objApprovalProcessInstance;
		}
		function getEngineeringChangeArray(){
			if(!$this->_objEngineeringChangeArray) {
				$this->_objEngineeringChangeArray = new EngineeringChangeArray();
			}
			return $this->_objEngineeringChangeArray;
		}
		function getBudgetCostArray(){
			if(!$this->_objBudgetCostArray) {
				$this->_objBudgetCostArray = new BudgetCostArray();
			}
			return $this->_objBudgetCostArray;
		}
		function getBOMDocument(){
			if(!$this->_objBOMDocument) {
				$this->_objBOMDocument = new Document();
			}
			return $this->_objBOMDocument;
		}
		function getPrintDocument(){
			if(!$this->_objPrintDocument) {
				$this->_objPrintDocument = new Document();
			}
			return $this->_objPrintDocument;
		}
		function getISODocument(){
			if(!$this->_objISODocument) {
				$this->_objISODocument = new Document();
			}
			return $this->_objISODocument;
		}
		function getNotification(){
			if(!$this->_objNotification) {
				$this->_objNotification = new NJSNNotification();
			}
			return $this->_objNotification;
		}
		function getChangeArray(){
			if(!$this->_objChangeArray) {
				$this->_objChangeArray = new ChangeArray();
			}
			return $this->_objChangeArray;
		}
		function getProgramProduct(){
			if(!$this->_objProgramProduct) {
				//$this->_objProgramProduct = new ProgramProduct();
			}
			return $this->_objProgramProduct;
		}
		function getProgramProductArray(){
			if(!$this->_objProgramProductArray) {
				$this->_objProgramProductArray = new ProgramProductArray();
			}
			return $this->_objProgramProductArray;
		}
		function getProductionFacilityArray(){
			if(!$this->_objProductionFacilityArray) {
				$this->_objProductionFacilityArray = new FacilityArray();
			}
			return $this->_objProductionFacilityArray;
		}
		function getContainerArray(){
			if(!$this->_objContainerArray) {
				$this->_objContainerArray = new ContainerArray();
			}
			return $this->_objContainerArray;
		}

		function setParentProgramMilestoneColors() {
			//echo $this->getProgramName() . "<br />";
			if(!$this->getParentProductXRArray()->getArray())
				return;

			foreach($this->getParentProductXRArray()->getArray() as $objParentProductXR) {

				$objParentProduct = $objParentProductXR->getParentProduct();
				if(Milestone::getMilestoneArray()->getArray()) {
					foreach(Milestone::getMilestoneArray()->getArray() as $intMilestoneID => $objMilestone) {
						if($objParentProgramMilestone = $objParentProduct->getProgramMilestoneArray()->objectIsSet($intMilestoneID)) {
							//$blnMilestoneWasSet = true;
						} else {
							$objParentProgramMilestone = new ProgramMilestone();
							$objParentProgramMilestone->getMilestone()->setMilestoneID($intMilestoneID); // TODO: All this setting seems like a bad way to do this
							$objParentProgramMilestone->setMilestoneID($intMilestoneID);
							$objParentProgramMilestone->setUseParent(1);
							$objParentProduct->getProgramMilestoneArray()->setObject($intMilestoneID, $objParentProgramMilestone);
						}

						$objChildProgramMilestone = $this->getProgramMilestoneArray()->getObject($objParentProgramMilestone->getMilestoneID());
						$strChildCSSClass = $objChildProgramMilestone->getCSSClass();
						$strParentCSSClass = $objParentProgramMilestone->getCSSClass();

						$strParentIndividualCSSClass = $objParentProgramMilestone->calcIndividualCSSClass();

						//echo $objParentProduct->getProgramName() . "|" . $objParentProgramMilestone->getMilestoneID() . "<br />";

						$blnDontChangeCSSClass = $strParentIndividualCSSClass == "overdue";

						$blnDontChangeCSSClass = $blnDontChangeCSSClass || $strParentIndividualCSSClass == "upcoming" && $strChildCSSClass != "overdue";
						$blnDontChangeCSSClass = $blnDontChangeCSSClass || $strParentIndividualCSSClass == $strChildCSSClass;

						if($blnDontChangeCSSClass)
							continue;

						//echo "Changing P ".$objParentProduct->getProgramName()." MS ".$objParentProgramMilestone->getMilestoneID() . " to ".$strChildCSSClass."<br />";

						$objParentProgramMilestone->setCSSClass($strChildCSSClass);
						$objParentProgramMilestone->save();
					}
				}

				$objParentProduct->setParentProgramMilestoneColors();
			}
		}


		function setProgramMilestoneColors($blnSaveToDB=false) {
			if($this->getChildProductXRArray()->getArray()) {
				foreach($this->getChildProductXRArray()->getArray() as $objProductXR) {
					$objProductXR->getChildProduct()->setProgramMilestoneColors($blnSaveToDB);
				}
			}
			if(Milestone::getMilestoneArray()->getArray()) {
				foreach(Milestone::getMilestoneArray()->getArray() as $intMilestoneID => $objMilestone) {
					$blnMilestoneWasSet = false;

					$objEarliestDueProgramMilestone = new ProgramMilestone();
					if($objParentProgramMilestone = $this->getProgramMilestoneArray()->objectIsSet($intMilestoneID)) {
						$blnMilestoneWasSet = true;
					} else {
						$objParentProgramMilestone = new ProgramMilestone();
						$objParentProgramMilestone->getMilestone()->setMilestoneID($intMilestoneID); // TODO: All this setting seems like a bad way to do this
						$objParentProgramMilestone->setMilestoneID($intMilestoneID);
						$objParentProgramMilestone->setUseParent(1);
						$this->getProgramMilestoneArray()->setObject($intMilestoneID, $objParentProgramMilestone);
					}
					$strParentIndividualCSSClass = $objParentProgramMilestone->calcIndividualCSSClass();

					if($this->getChildProductXRArray()->getArray()) {
						foreach($this->getChildProductXRArray()->getArray() as $objProductXR) {
							$objChildProgramMilestone = $objProductXR->getChildProduct()->getProgramMilestoneArray()->getObject($intMilestoneID);
							//echo $this->getProgramName() . " | " . $objProductXR->getChildProduct()->getProgramName() . "|" . $objChildProgramMilestone->getMilestoneID() . "<br />";

							/*
							$objEarliestDueProgramMilestone = $objEarliestDueProgramMilestone->getProgramMilestoneID()?$objEarliestDueProgramMilestone:$objChildProgramMilestone;
							if($objChildProgramMilestone->getDueDate() && $objChildProgramMilestone->getDueDate() < $objEarliestDueProgramMilestone->getDueDate()) {
								$objEarliestDueProgramMilestone = $objChildProgramMilestone;
							}
							*/

							if(!$objChildProgramMilestone || !$objProductXR->getChildProduct()->isProgram())
								continue;

							$strChildCSSClass = $objChildProgramMilestone->getCSSClass();

							$blnDontChangeCSSClass = $strParentIndividualCSSClass == "overdue";
							$blnDontChangeCSSClass = $blnDontChangeCSSClass || $strParentIndividualCSSClass == "upcoming" && $strChildCSSClass != "overdue";
							$blnDontChangeCSSClass = $blnDontChangeCSSClass || $strParentIndividualCSSClass == "complete" && $strChildCSSClass != "overdue" && $strChildCSSClass != "upcoming";

							$blnDontChangeCSSClass = $blnDontChangeCSSClass || $strParentIndividualCSSClass == $strChildCSSClass;

							if($blnDontChangeCSSClass)
								continue;

							//echo $objParentProgramMilestone->getCSSClass() . " | " . $strChildIndividualCSSClass . "<br />";
							//echo "Setting MS: " . $objParentProgramMilestone->getMilestoneID() . " of P ".$this->getProgramName(). " from MS ".$objChildProgramMilestone->getMilestoneID()." of P ".$objProductXR->getChildProduct()->getProgramName()  . " to " . $strChildIndividualCSSClass . "<br />";

							$strParentIndividualCSSClass = $strChildCSSClass;
						}
					}
					//echo $objParentProgramMilestone->getMilestoneID() . "|".$objParentProgramMilestone->getCSSClass() ."|". $strParentIndividualCSSClass."<br />";
					if($objParentProgramMilestone->getCSSClass() != $strParentIndividualCSSClass) {
						$objParentProgramMilestone->setCSSClass($strParentIndividualCSSClass);
					}

					$blnDateSet = false;
					/*
					if($objParentProgramMilestone->getMeetingStatus() == "N/A") {
						$objParentProgramMilestone->setDueDate($objEarliestDueProgramMilestone->getDueDate());
						$objParentProgramMilestone->setScheduledDate($objEarliestDueProgramMilestone->getScheduledDate());
						$blnDateSet = true;
					}
					*/

					if(($blnMilestoneWasSet || $blnDateSet) && $blnSaveToDB) {
						$objParentProgramMilestone->save();
					}
				}
			}
		}

		function loadForApprovalProcess($intID){
			if(!$intID)
				return false;

			$this->load($intID);
		}

		function setCanTakeAction() {

			if(!$this->getApprovalProcessInstance()->getApprovalProcessRoleArray()->getArray() || !$this->getProgramID() || !isset($_SESSION["objUser"]))
				return false;

			foreach($this->getApprovalProcessInstance()->getApprovalProcessRoleArray()->getArray() as $objApprovalProcessRole) {
				$blnIsMemberRole = Security::isMemberRole($this->getProgramID(), $objApprovalProcessRole->getRoleID());


				$blnOverrideApproval = Security::hasPermission(intSECURITY_ITEM_ID_PROGRAM, intOPERATION_ID_OVERRIDE_APPROVAL);
				$blnCanTakeAction = $blnIsMemberRole || $blnOverrideApproval;
				$blnCanTakeAction = $blnCanTakeAction && ($this->getProgramCommon()->getStatus() == "Pending Approval");
				$blnCanTakeAction = $blnCanTakeAction && ($this->getApprovalProcessInstance()->getApprovalStatus() == "Pending Approval");
				$objApprovalProcessRole->setCanTakeAction($blnCanTakeAction);
			}
		}
		function loadApprovals() {
			$this->getApprovalProcessInstance()->loadApprovals($this->getApprovalProcessInstanceID());

			$this->setCanTakeAction();
			$this->getApprovalProcessInstance()->getApprovalProcessRoleArray()->loadPotentialApprovers($this->getApprovalProcessInstanceID(), $this->getProgramID());
		}
		function submitApprovalProcess(){
			createProgramReport($this->getProgramID());
			$this->getProgramCommon()->setStatus("Pending Approval");
			$this->getProgramCommon()->save();
		}
		function approveApprovalProcess() {
			include_once("classes/Notification.class.php");

			// Set Eng Dev state
			$this->getProgramCommon()->setStatus("Engineering Development");

			$this->save();
			$this->getProgramCommon()->save();

			// Send notifications
			$this->getNotification()->notify($this);
		}
		function rejectApprovalProcess($objApprovalProcessRoleAction){
			$this->getProgramCommon()->load($this->getProgramCommonID());
			$this->getProgramCommon()->setStatus("Unannounced");
			$this->getProgramCommon()->save();

			$objApprovalProcessRoleAction->getApprovalProcessInstance()->getUser()->load($objApprovalProcessRoleAction->getApprovalProcessInstance()->getSubmittedByUserID());
			$strEmail = $objApprovalProcessRoleAction->getApprovalProcessInstance()->getUser()->getEmail();
			if($strEmail) {
				$strSubject = $this->getName() . " has a resubmit request.";
				$strURL = "http://".$_SERVER["HTTP_HOST"]."/".strFOLDER_PATH;
				$txtBody = $strSubject . "<br /><br />"
					. "<div class=\"label\">Resubmit Request Comment:&nbsp;</div><div class=\"input\">".$objApprovalProcessRoleAction->getNotes()."</div><br /><br />"
					. "<a href=\"".$strURL."program.php?intProgramID=".$this->getID()."\">View</a>";

				$objNotification = new Notification();
				$objNotification->setSubject($strSubject);
				$objNotification->setBody($txtBody);
				$objNotification->getUserArray()->addNew($objApprovalProcessRoleAction->getApprovalProcessInstance()->getUser());
				$objNotification->sendNotification();
			}
		}
		function approvalProcessNotificationInfo($objApprovalProcessInstance) {
			include_once("ApprovalProcessInstance.class.php");
			$this->loadForApprovalNotification($this->getProgramID());
			$objUserArray = ApprovalProcessInstance::getApproversLeft($this->getApprovalProcessInstanceID(), $this->getProgramID()); // TODO: If approvals ever occur on children, we'll need to get top level program ID

			$strURL = $this->getURL();
			$strSubject = $this->getProgramCommon()->getPlatform()->getPlatform() ." ". $this->getProgramName() . " requires your approval.";
			$txtBody  = "Program: <a href=\"$strURL\">" . $this->getProgramCommon()->getPlatform()->getPlatform() . " - " . $this->getProgramName() . "</a> requires your approval.<br />";
			$txtBody .= "Customer: " . $this->getProgramCommon()->getCustomer()->getOrganizationName() . "<br />";
			$txtBody .= "Program Type: " . $this->getProgramCommon()->getProgramType()->getProgramType() . "<br />";
			$txtBody .= "Platform: " . $this->getProgramCommon()->getPlatform()->getPlatform() . "<br />";
			$txtBody .= "Model Year: " . formatModelYear($this->getProgramCommon()->getModelYear()) . "<br />";
			$txtBody .= "Submitted By: " . $objApprovalProcessInstance->getUser()->getDisplayName() . "<br />";
			$txtBody .= "Submitter Comment: " . $objApprovalProcessInstance->getSubmittedComment() . "<br />";


			return array($strSubject, $txtBody, $objUserArray);
		}
		function loadForApprovalNotification($intProgramID){
			if(!$intProgramID)
				return;

			$strSQL = "SELECT tblPlatform.*, tblProgramType.*, tblOrganization.*, tblCustomer.*, tblProgramCommon.*, tblProduct.*
					FROM dbPLM.tblProduct
					INNER JOIN dbPLM.tblProgramCommon
						ON tblProgramCommon.intProgramCommonID = tblProduct.intProgramCommonID
					LEFT JOIN dbPLM.tblCustomer
						ON tblCustomer.intCustomerID = tblProgramCommon.intCustomerID
					LEFT JOIN dbPLM.tblOrganization
						ON tblOrganization.intOrganizationID = tblCustomer.intOrganizationID

					LEFT JOIN dbPLM.tblProgramType
						ON tblProgramType.intProgramTypeID = tblProgramCommon.intProgramTypeID
					LEFT JOIN dbPLM.tblPlatform
						ON tblPlatform.intPlatformID = tblProgramCommon.intPlatformID
					WHERE intProgramID = '$intProgramID'
					LIMIT 1
				";
			$rsResult = $this->getDB()->query($strSQL);

			$arrRow = $this->getDB()->fetch_assoc($rsResult);
			$this->setVarsFromRow($arrRow);
			$this->getProgramCommon()->setVarsFromRow($arrRow);
			$this->getProgramCommon()->getCustomer()->setVarsFromRow($arrRow);
			$this->getProgramCommon()->getProgramType()->setVarsFromRow($arrRow);
			$this->getProgramCommon()->getPlatform()->setVarsFromRow($arrRow);
		}

		function organizeConfigurations() {
			include_once("ProductXR.class.php");
			if(!$this->getChildProductXRArray()->getArray())
				return;

			// Copy top level of getChildProductXRArray to getConfigurationProductXRArray
			// Take all second levels of getChildProductXRArray (aka the children of the new getConfigurationProductXRArray) and put them into a single objChildProductXRArray
			$objChildProductXRArray = new ProductXRArray();
			$intConfigurationProductIndex = 1;
			$this->getConfigurationProductXRArray()->clear();
			foreach($this->getChildProductXRArray()->getArray() as $strKey => $objConfigurationProductXR) {
				$objConfigurationProductXR->getChildProduct()->setConfigurationProductIndex($intConfigurationProductIndex);
				$this->getConfigurationProductXRArray()->setObject($strKey, $objConfigurationProductXR); // TODO: Consider copying _objChildProductXRArray over _objConfigurationProductXRArray because this does the same thing (copies all the children too), but this probably doesn't make a reference, the full out copy might, be careful!

				if($objConfigurationProductXR->getChildProduct()->getChildProductXRArray()->getArray()) {
					foreach($objConfigurationProductXR->getChildProduct()->getChildProductXRArray()->getArray() as $objChildProductXR) {
						$intProductID = $objChildProductXR->getChildProduct()->getProductID();
						$objChildProductXR->getChildProduct()->setConfigurationProductIndex($intConfigurationProductIndex);
						if(!$objChildProductXRArray->objectIsSet($intProductID)) {
							$objChildProductXRArray->setObject($intProductID, $objChildProductXR);
						} else {
							$objChildProductXRArray->getObject($intProductID)->getChildProduct()->getParentProductXRArray()->addNew($objChildProductXR);
						}

						$objChildProductXRArray->getObject($intProductID)->getChildProduct()->getConfigurationProductXRArray()->setObject($intConfigurationProductIndex, $objChildProductXR);
					}
				}
				$intConfigurationProductIndex++;
			}

			// Create blank ProductXRs in the objChildProductXRArray
			if($objChildProductXRArray->getArray() && $this->getConfigurationProductXRArray()->getArray()) {
				foreach($objChildProductXRArray->getArray() as $intProductID => $objChildProductXR) {
					foreach($this->getConfigurationProductXRArray()->getArray() as $objConfigurationProductXR) {

						$intConfigurationProductIndex = $objConfigurationProductXR->getChildProduct()->getConfigurationProductIndex();
						if(!$objChildProductXRArray->getObject($intProductID)->getChildProduct()->getConfigurationProductXRArray()->getObject($intConfigurationProductIndex)) {
							$objProductXR = new ProductXR();
							$objProductXR->getChildProduct()->setConfigurationProductIndex($intConfigurationProductIndex);
							$objChildProductXRArray->getObject($intProductID)->getChildProduct()->getConfigurationProductXRArray()->setObject($intConfigurationProductIndex, $objProductXR);
						}
					}

				}
			}

			$this->getChildProductXRArray()->setArray($objChildProductXRArray->getArray());
		}

		function isProgram(){
			if($this->getProductTypeID() == intPRODUCT_TYPE_ID_ASSEMBLY
				|| $this->getProductTypeID() == intPRODUCT_TYPE_ID_PLASTIC_PART
				|| $this->getProductTypeID() == intPRODUCT_TYPE_ID_METAL_PART)
				return true;
			return false;
		}

		function getAllToolIDs(){
			$arrToolIDs = array();

			if($this->getProductToolXRArray()->getArray()) {
				foreach($this->getProductToolXRArray()->getArray() as $objProductToolXR) {
					$arrToolIDs[$objProductToolXR->getToolID()] = $objProductToolXR->getToolID();
				}
			}

			if($this->getToolArray()->getArray()) {
				foreach($this->getToolArray()->getArray() as $objTool) {
					$arrToolIDs[$objTool->getToolID()] = $objTool->getToolID();
				}
			}

			return $arrToolIDs;
		}

		static function getTopLevelProgramIDFromProgramID($intProgramID) {
			$strSQL = "SELECT intParentProgramID
				FROM dbPLM.tblProductXR
				WHERE tblProductXR.intChildProgramID = ".self::getDB()->sanitize($intProgramID);
			//echo $strSQL . "<br />";
			$rsResult = self::getDB()->query($strSQL);
			$arrRow = self::getDB()->fetch_assoc($rsResult);
			$intParentProgramID = $arrRow["intParentProgramID"]?$arrRow["intParentProgramID"]:null;

			if(!$intParentProgramID) {
				return $intProgramID;
			} else {
				return self::getTopLevelProgramIDFromProgramID($intParentProgramID);
			}
		}

		/*static function getTopLevelProgramIDFromProgramCommonID($intProgramCommonID) {
			$strSQL = "SELECT tblProduct.intProgramID
				FROM dbPLM.tblProduct
				INNER JOIN dbPLM.tblProgramData
					ON tblProgramData.intProgramID = tblProduct.intProgramID
				WHERE intProgramCommonID = ".self::getDB()->sanitize($intProgramCommonID)."
				LIMIT 1"; // Should theoretically be only 1 result anyway.
			//echo $strSQL . "<br />";
			$rsResult = self::getDB()->query($strSQL);

			$arrRow = self::getDB()->fetch_assoc($rsResult);
			return $arrRow["intProgramID"];
		}*/



		static function getTopLevelProgramIDFromProgramMilestoneID($intProgramMilestoneID) {
			$strSQL = "SELECT intProgramID
				FROM dbPLM.tblProgramMilestone
				WHERE intProgramMilestoneID = ".self::getDB()->sanitize($intProgramMilestoneID);
			//echo $strSQL . "<br />";
			$rsResult = self::getDB()->query($strSQL);
			$arrRow = self::getDB()->fetch_assoc($rsResult);

			return Product::getTopLevelProgramIDFromProgramID($arrRow["intProgramID"]);
		}

		function getReportURL($strReportName){
			switch($strReportName) {
				case "NJSN":
					return strREPORT_FOLDER_NJSN.$this->getProgramID().".html"; // TODO: throw error if intProgramID doesn't exist
				break;
			}
		}

		function getFormattedCreatedOn(){
			return formatDate($this->_dtmCreatedOn);
		}

		function getChildIDs(){
			return array_unique($this->getChildProductXRArray()->getChildIDs($this->getChildProductXRArray()));
		}

		function getParentIDs(){
			return array_unique($this->getParentProductXRArray()->getParentIDs($this->getParentProductXRArray()));
		}

		static function isLastChild($intProductID, $intChildProductID) {
			$strSQL = "SELECT 1 FROM dbPLM.tblProductXR
				WHERE intParentProgramID = ".self::getDB()->sanitize($intProductID)."
				AND intChildProgramID != ".self::getDB()->sanitize($intChildProductID);
			//echo $strSQL;
			$rsResult = self::getDB()->query($strSQL);
			return !self::getDB()->num_rows($rsResult);
		}

		function getProgramManager(){
			return $this->_strProgramManager;
		}
		function setProgramManager(){
			if($this->_strProgramManager !== $value) {
				$this->_strProgramManager = $value;
				$this->_blnDirty = true;
			}
		}
		function getConfigurationProductIndex() {
			return $this->_intConfigurationProductIndex;
		}
		function setConfigurationProductIndex($value) {
			if($this->_intConfigurationProductIndex !== $value) {
				$this->_intConfigurationProductIndex = $value;
			}
		}

		function getIsChild() {
			return $this->_blnIsChild;
		}
		function setIsChild($value) {
			if($this->_blnIsChild !== $value) {
				$this->_blnIsChild = $value;
			}
		}

		function canWriteECFields() {
			$objEC = isset($_SESSION["arrWorkingData"]["objCurrentEC"])?$_SESSION["arrWorkingData"]["objCurrentEC"]:null;
			$blnEditingEC =  $objEC && $objEC->getEngineeringChangeID() && $objEC->getProgramCommonID() == $this->getProgramCommonID() && $objEC->getECStatus() == "New";
			$blnEditingEC = $blnEditingEC && Security::hasPermission(intSECURITY_ITEM_ID_ENGINEERING_CHANGE, intOPERATION_ID_WRITE);
			return (!$this->getProgramCommon(true)->getDesignReleased() || $blnEditingEC);
		}

		function getTopLevelFolderName() {
			if(!$this->getProgramCommon()->getPlatform()->getPlatformID())
				$this->getProgramCommon()->getPlatform()->load($this->getProgramCommon()->getPlatformID());
			return $this->getProgramCommon()->getPlatform()->getPlatform() . " " . $this->getProgramName(); // TODO: Should I strip out the slashes here? Am I doing the stripping elsewhere?
		}

		function getCSSClass(){
			if($this->getProgramCommon()->getStatus() == "Unannounced" || $this->getProgramCommon()->getStatus() == "Pending Approval")

				return "program_not_awarded";
			else
				return "program_awarded";
		}

		function getURL(){
			if(!$this->getProductType()->getProductTypeID())
				if($this->getProductTypeID())
					$this->getProductType()->setProductTypeID($this->getProductTypeID());


			if($this->getProductType()->getSource() == "Manufactured")
				return strSITE_URL."program.php?intProgramID=".$this->getProgramID();
			else
				return strSITE_URL."product.php?intProductID=".$this->getProductID();
		}
		function getApprovalName() {
			return $this->getProgramCommon()->getPlatform()->getPlatform() . " " . $this->getProgramName();
		}

		function getNextProductNumber() {
			$intNumber = 1;
			$objProduct = new Product();
			$objProduct->setProductID(1);
			while($objProduct->getProductID()) {
				$intNumber++;
				$strProductNumber = $this->getProductNumber() . " (".$intNumber.")";

				$objProduct = new Product();
				$objProduct->loadByProductNumber($strProductNumber);
			}

			return $strProductNumber;
		}

		// These product functions will help transition to the splitting of tblProduct/tblProgram
		function getProductID() {

			return $this->_intProgramID;
		}
		function setProductID($value) {
			if($this->_intProgramID !== $value) {
				$this->_intProgramID = $value;
				$this->_blnDirty = true;
			}
		}
		function getProductNumber() {
			return $this->_strProgramNumber;
		}
		function setProductNumber($value) {
			if(!$this->isChangesCopy() && DataClass::getTrackChanges() && !AdditionDeletion::isAddition($this->getTableID(), $this->getID())) {
				$this->getChangesCopy()->setProgramNumber($value);
			} else {
				parent::setProgramNumber($value);
			}
		}
		function setProgramNumber($value) {
			if(!$this->isChangesCopy() && DataClass::getTrackChanges() && !AdditionDeletion::isAddition($this->getTableID(), $this->getID())) {
				$this->getChangesCopy()->setProgramNumber($value);
			} else {
				parent::setProgramNumber($value);
			}
		}
		function getProductName() {
			return $this->_strProgramName;
		}
		function setProductName($value) {
			if(!$this->isChangesCopy() && DataClass::getTrackChanges() && !AdditionDeletion::isAddition($this->getTableID(), $this->getID())) {
				$this->getChangesCopy()->setProgramName($value);
			} else {
				parent::setProgramName($value);
			}
		}
		function setProgramName($value) {
			if(!$this->isChangesCopy() && DataClass::getTrackChanges() && !AdditionDeletion::isAddition($this->getTableID(), $this->getID())) {
				$this->getChangesCopy()->setProgramName($value);
			} else {
				parent::setProgramName($value);
			}
		}

		function applyChanges($objChangeArray, $objEC) {
			parent::applyChanges($objChangeArray, $objEC);
			$this->getProgramProduct()->applyChanges($objChangeArray, $objEC);
			$this->getParentProductXR()->applyChanges($objChangeArray, $objEC);

			if($this->getChildProductXRArray()->getArray()) {
				foreach($this->getChildProductXRArray()->getArray() as $objProductXR) {
					$objProductXR->applyChanges($objChangeArray, $objEC);
					$objProductXR->getChildProduct()->applyChanges($objChangeArray, $objEC);
				}
			}

			if($this->getParentProductXRArray()->getArray()) {
				foreach($this->getParentProductXRArray()->getArray() as $objParentProductXR) {
					$objParentProductXR->applyChanges($objChangeArray, $objEC);
				}
			}
		}
	}

?>