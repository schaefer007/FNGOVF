<?php
require_once("ArrayClass.class.php");

class RoleNotificationTypeArray extends ArrayClass {
	function __construct(){
		parent::__construct("RoleNotificationType");
	}

	function load() {
		$strSQL = " SELECT * FROM dbSystem.tblRoleNotificationType";
		$rsResult = $this->getDB()->query($strSQL);
		while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
			$this->_arrObjects[$arrRow["intRoleNotificationTypeID"]] = new RoleNotificationType();
			$this->_arrObjects[$arrRow["intRoleNotificationTypeID"]]->setVarsFromRow($arrRow);
		}
	}

	function loadByNotificationTypeID($intNotificationTypeID){
		if(!$intNotificationTypeID)
			return false;

		$strSQL = " SELECT * FROM dbSystem.tblRoleNotificationType
			WHERE intNotificationTypeID = ".self::getDB()->sanitize($intNotificationTypeID);
		$rsResult = $this->getDB()->query($strSQL);
		while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
			$this->_arrObjects[$arrRow["intRoleNotificationTypeID"]] = new RoleNotificationType();
			$this->_arrObjects[$arrRow["intRoleNotificationTypeID"]]->setVarsFromRow($arrRow);
		}
	}
}

require_once("DataClass.class.php");

class RoleNotificationTypeBase extends DataClass {
	protected $_intRoleNotificationTypeID;
	protected $_intNotificationTypeID;
	protected $_intRoleID;

	function __construct($intRoleNotificationTypeID=null) {
		$this->DataClass();
		if($intRoleNotificationTypeID) {
			$this->load($intRoleNotificationTypeID);
		}
	}

	protected function insert() {
		base::write_log("Database created","S");
		$strSQL = "INSERT INTO dbSystem.tblRoleNotificationType SET ";
		$strConnector = "";
		$strSQL .= $strConnector . "intRoleNotificationTypeID = ".$this->getDB()->sanitize(self::getRoleNotificationTypeID());
		$strConnector = ",";
		if(isset($this->_intNotificationTypeID)) {
			$strSQL .= $strConnector . "intNotificationTypeID = ".$this->getDB()->sanitize(self::getNotificationTypeID());
			$strConnector = ",";
		}
		if(isset($this->_intRoleID)) {
			$strSQL .= $strConnector . "intRoleID = ".$this->getDB()->sanitize(self::getRoleID());
			$strConnector = ",";
		}
		//echo $strSQL;
		$this->getDB()->query($strSQL);
		$this->setRoleNotificationTypeID($this->getDB()->insert_id());
		return $this->getRoleNotificationTypeID();
	}

	protected function update() {
		base::write_log("Database Update","S");
		$strSQL = "UPDATE dbSystem.tblRoleNotificationType SET ";
		$strConnector = "";
		$strSQL .= $strConnector . "intRoleNotificationTypeID = ".$this->getDB()->sanitize(self::getRoleNotificationTypeID());
		$strConnector = ",";
		if(isset($this->_intNotificationTypeID)) {
			$strSQL .= $strConnector . "intNotificationTypeID = ".$this->getDB()->sanitize(self::getNotificationTypeID());
			$strConnector = ",";
		}
		if(isset($this->_intRoleID)) {
			$strSQL .= $strConnector . "intRoleID = ".$this->getDB()->sanitize(self::getRoleID());
			$strConnector = ",";
		}
		$strSQL .= " WHERE intRoleNotificationTypeID = ".$this->getDB()->sanitize(self::getRoleNotificationTypeID())."";
		//echo $strSQL;
		return $this->getDB()->query($strSQL);
	}

	public function save() {
		if($this->_intRoleNotificationTypeID) {
			return $this->update();
		} else {
			return $this->insert();
		}
	}

	public function delete() {
		if($this->_intRoleNotificationTypeID) {
			$strSQL = "DELETE FROM dbSystem.tblRoleNotificationType
				WHERE intRoleNotificationTypeID = '$this->_intRoleNotificationTypeID'
				";
			return $this->getDB()->query($strSQL);
		}
	}

	public function load($intRoleNotificationTypeID) {
		if(!$intRoleNotificationTypeID) {
			return false;
		}

		$strSQL = "SELECT *
				FROM dbSystem.tblRoleNotificationType
				WHERE intRoleNotificationTypeID = ".self::getDB()->sanitize($intRoleNotificationTypeID)."
				LIMIT 1
			";
		$rsResult = $this->getDB()->query($strSQL);
		$arrRow = $this->getDB()->fetch_assoc($rsResult);
		$this->setVarsFromRow($arrRow);
	}

	function setVarsFromRow($arrRow) {
		if(isset($arrRow["intRoleNotificationTypeID"])) $this->_intRoleNotificationTypeID = $arrRow["intRoleNotificationTypeID"];
		if(isset($arrRow["intNotificationTypeID"])) $this->_intNotificationTypeID = $arrRow["intNotificationTypeID"];
		if(isset($arrRow["intRoleID"])) $this->_intRoleID = $arrRow["intRoleID"];
	}

	function getRoleNotificationTypeID() {
		return $this->_intRoleNotificationTypeID;
	}
	function setRoleNotificationTypeID($value) {
		if($this->_intRoleNotificationTypeID !== $value) {
			$this->_intRoleNotificationTypeID = $value;
			$this->_blnDirty = true;
		}
	}

	function getNotificationTypeID() {
		return $this->_intNotificationTypeID;
	}
	function setNotificationTypeID($value) {
		if($this->_intNotificationTypeID !== $value) {
			$this->_intNotificationTypeID = $value;
			$this->_blnDirty = true;
		}
	}

	function getRoleID() {
		return $this->_intRoleID;
	}
	function setRoleID($value) {
		if($this->_intRoleID !== $value) {
			$this->_intRoleID = $value;
			$this->_blnDirty = true;
		}
	}

}

include_once("Role.class.php");

class RoleNotificationType extends RoleNotificationTypeBase {
	private $_objRole;

	function __construct($intRoleNotificationTypeID=null) {
		parent::__construct($intRoleNotificationTypeID);
	}

	public function loadByRoleIDAndNotificationTypeID($intRoleID, $intNotificationTypeID) {
		if(!$intRoleID || !$intNotificationTypeID) {
			return false;
		}

		$strSQL = "SELECT *
				FROM dbSystem.tblRoleNotificationType
				WHERE intRoleID = ".self::getDB()->sanitize($intRoleID)."
				AND intNotificationTypeID = ".self::getDB()->sanitize($intNotificationTypeID)."
				LIMIT 1
			";
		$rsResult = $this->getDB()->query($strSQL);
		$arrRow = $this->getDB()->fetch_assoc($rsResult);
		$this->setVarsFromRow($arrRow);
	}

	function getRole(){
		if(!$this->_objRole)
			$this->_objRole = new Role();
		return $this->_objRole;
	}
}
?>
