<?php
	require_once("ArrayClass.class.php");

	class UserRoleXRArray extends ArrayClass {
		function __construct(){
		}

		function load() {
			$strSQL = " SELECT * FROM tblUserRoleXR";
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				$this->_arrObjects[$arrRow["intUserRoleXRID"]] = new UserRoleXR();
				$this->_arrObjects[$arrRow["intUserRoleXRID"]]->setVarsFromRow($arrRow);
			}
		}

		function loadByUserID($intUserID) {
			if(!$intUserID)
				return false;

			$strSQL = " SELECT tblRole.*, tblUserRoleXR.*
				FROM tblUserRoleXR
				INNER JOIN tblRole
					ON tblRole.intRoleID = tblUserRoleXR.intRoleID
				WHERE intUserID = ".self::getDB()->sanitize($intUserID);
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				$this->_arrObjects[$arrRow["intUserRoleXRID"]] = new UserRoleXR();
				$this->_arrObjects[$arrRow["intUserRoleXRID"]]->setVarsFromRow($arrRow);
				$this->_arrObjects[$arrRow["intUserRoleXRID"]]->getRole()->setVarsFromRow($arrRow);
			}
		}
	}

	require_once("DataClass.class.php");

	class UserRoleXRBase extends DataClass {
		protected $_intUserRoleXRID;
		protected $_intUserID;
		protected $_intRoleID;

		function __construct($intUserRoleXRID=null) {
			$this->DataClass();
			if($intUserRoleXRID) {
				$this->load($intUserRoleXRID);
			}
		}

		protected function insert() {
			base::write_log("User Role XR created","S");
			$strSQL = "INSERT INTO tblUserRoleXR SET ";
			$strConnector = "";
			$strSQL .= $strConnector . "intUserRoleXRID = ".$this->getDB()->sanitize(self::getUserRoleXRID());
			$strConnector = ",";
			if(isset($this->_intUserID)) {
				$strSQL .= $strConnector . "intUserID = ".$this->getDB()->sanitize(self::getUserID());
				$strConnector = ",";
			}
			if(isset($this->_intRoleID)) {
				$strSQL .= $strConnector . "intRoleID = ".$this->getDB()->sanitize(self::getRoleID());
				$strConnector = ",";
			}
			$strSQL .= " ON DUPLICATE KEY UPDATE intUserID = intUserID ";
			//echo $strSQL;
			$this->getDB()->query($strSQL);
			$this->setUserRoleXRID($this->getDB()->insert_id());
			return $this->getUserRoleXRID();
		}

		protected function update() {
			base::write_log("User Role XR Update","S");
			$strSQL = "UPDATE tblUserRoleXR SET ";
			$strConnector = "";
			$strSQL .= $strConnector . "intUserRoleXRID = ".$this->getDB()->sanitize(self::getUserRoleXRID());
			$strConnector = ",";
			if(isset($this->_intUserID)) {
				$strSQL .= $strConnector . "intUserID = ".$this->getDB()->sanitize(self::getUserID());
				$strConnector = ",";
			}
			if(isset($this->_intRoleID)) {
				$strSQL .= $strConnector . "intRoleID = ".$this->getDB()->sanitize(self::getRoleID());
				$strConnector = ",";
			}
			$strSQL .= " WHERE intUserRoleXRID = ".$this->getDB()->sanitize(self::getUserRoleXRID())."";
			//echo $strSQL;
			return $this->getDB()->query($strSQL);
		}

		public function save() {
			if($this->_intUserRoleXRID) {
				return $this->update();
			} else {
				return $this->insert();
			}
		}

		public function delete() {
			if($this->_intUserRoleXRID) {
				base::write_log("User Role XR Delete","S");
				$strSQL = "DELETE FROM tblUserRoleXR
				WHERE intUserRoleXRID = '$this->_intUserRoleXRID'
				";
				return $this->getDB()->query($strSQL);
			}
		}

		public function load($intUserRoleXRID) {
			if(!$intUserRoleXRID) {
				return false;
			}

			$strSQL = "SELECT *
				FROM tblUserRoleXR
				WHERE intUserRoleXRID = '$intUserRoleXRID'
				LIMIT 1
			";
			$rsUserRoleXR = $this->getDB()->query($strSQL);
			$arrUserRoleXR = $this->getDB()->fetch_assoc($rsUserRoleXR);
			$this->setVarsFromRow($arrUserRoleXR);
		}

		function setVarsFromRow($arrUserRoleXR) {
			if(isset($arrUserRoleXR["intUserRoleXRID"])) $this->_intUserRoleXRID = $arrUserRoleXR["intUserRoleXRID"];
			if(isset($arrUserRoleXR["intUserID"])) $this->_intUserID = $arrUserRoleXR["intUserID"];
			if(isset($arrUserRoleXR["intRoleID"])) $this->_intRoleID = $arrUserRoleXR["intRoleID"];
		}

		function getUserRoleXRID() {
			return $this->_intUserRoleXRID;
		}
		function setUserRoleXRID($value) {
			if($this->_intUserRoleXRID !== $value) {
				$this->_intUserRoleXRID = $value;
				$this->_blnDirty = true;
			}
		}

		function getUserID() {
			return $this->_intUserID;
		}
		function setUserID($value) {
			if($this->_intUserID !== $value) {
				$this->_intUserID = $value;
				$this->_blnDirty = true;
			}
		}

		function getRoleID() {
			return $this->_intRoleID;
		}
		function setRoleID($value) {
			if($this->_intRoleID !== $value) {
				$this->_intRoleID = $value;
				$this->_blnDirty = true;
			}
		}

	}

	include_once("User.class.php");
	include_once("Role.class.php");

	class UserRoleXR extends UserRoleXRBase {
		private $_objUser;
		private $_objRole;

		function __construct($intUserRoleXRID=null) {
			parent::__construct($intUserRoleXRID);
		}

		function getUser(){
			if(!$this->_objUser) {
				$this->_objUser = new User();
			}
			return $this->_objUser;
		}
		function getRole(){
			if(!$this->_objRole) {
				$this->_objRole = new Role();
			}
			return $this->_objRole;
		}

		function loadByUserIDAndRoleID($intUserID, $intRoleID){
			if(!$intUserID || !$intRoleID) {
				return false;
			}

			$strSQL = "SELECT *
				FROM tblUserRoleXR
				WHERE intUserID = ".$this->getDB()->sanitize($intUserID)."
				AND intRoleID = ".$this->getDB()->sanitize($intRoleID)."
				LIMIT 1
			";
			$rsUserRoleXR = $this->getDB()->query($strSQL);
			$arrUserRoleXR = $this->getDB()->fetch_assoc($rsUserRoleXR);
			$this->setVarsFromRow($arrUserRoleXR);
		}
	}
?>