<?php
require_once("ArrayClass.class.php");

class ConditionXRArray extends ArrayClass {
	function __construct(){
		parent::__construct("ConditionXR");
	}

	function load() {
		$strSQL = " SELECT * FROM dbSystem.tblConditionXR";
		$rsResult = $this->getDB()->query($strSQL);
		while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
			$this->_arrObjects[$arrRow["intConditionXRID"]] = new ConditionXR();
			$this->_arrObjects[$arrRow["intConditionXRID"]]->setVarsFromRow($arrRow);
		}
	}

	function loadParentsByConditionID($intChildConditionID) {
		$strSQL = " SELECT *
			FROM dbSystem.tblConditionXR
			WHERE intChildConditionID = ".self::getDB()->sanitize($intChildConditionID);
		echo $strSQL;
		$rsResult = $this->getDB()->query($strSQL);
		while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
			$this->_arrObjects[$arrRow["intConditionXRID"]] = new ConditionXR();
			$this->_arrObjects[$arrRow["intConditionXRID"]]->setVarsFromRow($arrRow);
		}
	}
}

require_once("DataClass.class.php");

class ConditionXRBase extends DataClass {
	protected $_intConditionXRID;
	protected $_intTopLevelConditionID;
	protected $_intParentConditionID;
	protected $_intChildConditionID;
	protected $_intLeft;
	protected $_intRight;

	function __construct($intConditionXRID=null) {
		$this->DataClass();
		if($intConditionXRID) {
			$this->load($intConditionXRID);
		}
	}

	protected function insert() {
		base::write_log("ConditionXR created","S");
		$strSQL = "INSERT INTO dbSystem.tblConditionXR SET ";
		$strConnector = "";
		$strSQL .= $strConnector . "intConditionXRID = ".$this->getDB()->sanitize(self::getConditionXRID());
		$strConnector = ",";
		if(isset($this->_intTopLevelConditionID)) {
			$strSQL .= $strConnector . "intTopLevelConditionID = ".$this->getDB()->sanitize(self::getTopLevelConditionID());
			$strConnector = ",";
		}
		if(isset($this->_intParentConditionID)) {
			$strSQL .= $strConnector . "intParentConditionID = ".$this->getDB()->sanitize(self::getParentConditionID());
			$strConnector = ",";
		}
		if(isset($this->_intChildConditionID)) {
			$strSQL .= $strConnector . "intChildConditionID = ".$this->getDB()->sanitize(self::getChildConditionID());
			$strConnector = ",";
		}
		if(isset($this->_intLeft)) {
			$strSQL .= $strConnector . "intLeft = ".$this->getDB()->sanitize(self::getLeft());
			$strConnector = ",";
		}
		if(isset($this->_intRight)) {
			$strSQL .= $strConnector . "intRight = ".$this->getDB()->sanitize(self::getRight());
			$strConnector = ",";
		}
		//echo $strSQL;
		$this->getDB()->query($strSQL);
		$this->setConditionXRID($this->getDB()->insert_id());
		return $this->getConditionXRID();
	}

	protected function update() {
		base::write_log("ConditionXR updated","S");
		$strSQL = "UPDATE dbSystem.tblConditionXR SET ";
		$strConnector = "";
		$strSQL .= $strConnector . "intConditionXRID = ".$this->getDB()->sanitize(self::getConditionXRID());
		$strConnector = ",";
		if(isset($this->_intTopLevelConditionID)) {
			$strSQL .= $strConnector . "intTopLevelConditionID = ".$this->getDB()->sanitize(self::getTopLevelConditionID());
			$strConnector = ",";
		}
		if(isset($this->_intParentConditionID)) {
			$strSQL .= $strConnector . "intParentConditionID = ".$this->getDB()->sanitize(self::getParentConditionID());
			$strConnector = ",";
		}
		if(isset($this->_intChildConditionID)) {
			$strSQL .= $strConnector . "intChildConditionID = ".$this->getDB()->sanitize(self::getChildConditionID());
			$strConnector = ",";
		}
		if(isset($this->_intLeft)) {
			$strSQL .= $strConnector . "intLeft = ".$this->getDB()->sanitize(self::getLeft());
			$strConnector = ",";
		}
		if(isset($this->_intRight)) {
			$strSQL .= $strConnector . "intRight = ".$this->getDB()->sanitize(self::getRight());
			$strConnector = ",";
		}
		$strSQL .= " WHERE intConditionXRID = ".$this->getDB()->sanitize(self::getConditionXRID())."";
		//echo $strSQL;
		return $this->getDB()->query($strSQL);
	}

	public function save() {
		if($this->_intConditionXRID) {
			return self::update();
		} else {
			return self::insert();
		}
	}

	public function delete() {
		base::write_log("ConditionXR deleted");
		if($this->_intConditionXRID) {
			$strSQL = "DELETE FROM dbSystem.tblConditionXR
				WHERE intConditionXRID = '$this->_intConditionXRID'
				";
			return $this->getDB()->query($strSQL);
		}
	}

	public function load($intConditionXRID) {
		if(!$intConditionXRID) {
			return false;
		}

		$strSQL = "SELECT *
				FROM dbSystem.tblConditionXR
				WHERE intConditionXRID = ".self::getDB()->sanitize($intConditionXRID)."
				LIMIT 1
			";
		$rsResult = $this->getDB()->query($strSQL);
		$arrRow = $this->getDB()->fetch_assoc($rsResult);
		$this->setVarsFromRow($arrRow);
	}

	function setVarsFromRow($arrRow) {
		if(isset($arrRow["intConditionXRID"])) $this->_intConditionXRID = $arrRow["intConditionXRID"];
		if(isset($arrRow["intTopLevelConditionID"])) $this->_intTopLevelConditionID = $arrRow["intTopLevelConditionID"];
		if(isset($arrRow["intParentConditionID"])) $this->_intParentConditionID = $arrRow["intParentConditionID"];
		if(isset($arrRow["intChildConditionID"])) $this->_intChildConditionID = $arrRow["intChildConditionID"];
		if(isset($arrRow["intLeft"])) $this->_intLeft = $arrRow["intLeft"];
		if(isset($arrRow["intRight"])) $this->_intRight = $arrRow["intRight"];
	}

	function getConditionXRID() {
		return $this->_intConditionXRID;
	}
	function setConditionXRID($value) {
		if($this->_intConditionXRID !== $value) {
			$this->_intConditionXRID = $value;
			$this->_blnDirty = true;
		}
	}

	function getTopLevelConditionID() {
		return $this->_intTopLevelConditionID;
	}
	function setTopLevelConditionID($value) {
		if($this->_intTopLevelConditionID !== $value) {
			$this->_intTopLevelConditionID = $value;
			$this->_blnDirty = true;
		}
	}

	function getParentConditionID() {
		return $this->_intParentConditionID;
	}
	function setParentConditionID($value) {
		if($this->_intParentConditionID !== $value) {
			$this->_intParentConditionID = $value;
			$this->_blnDirty = true;
		}
	}

	function getChildConditionID() {
		return $this->_intChildConditionID;
	}
	function setChildConditionID($value) {
		if($this->_intChildConditionID !== $value) {
			$this->_intChildConditionID = $value;
			$this->_blnDirty = true;
		}
	}

	function getLeft() {
		return $this->_intLeft;
	}
	function setLeft($value) {
		if($this->_intLeft !== $value) {
			$this->_intLeft = $value;
			$this->_blnDirty = true;
		}
	}

	function getRight() {
		return $this->_intRight;
	}
	function setRight($value) {
		if($this->_intRight !== $value) {
			$this->_intRight = $value;
			$this->_blnDirty = true;
		}
	}

}

class ConditionXR extends ConditionXRBase {
	protected $_objChildCondition;

	function __construct($intConditionXRID=null) {
		parent::__construct($intConditionXRID);
	}

	function loadByTopLevelConditionIDAndChildConditionID($intTopLevelConditionID, $intChildConditionID) {
		$strSQL = " SELECT *
			FROM dbSystem.tblConditionXR
			WHERE intTopLevelConditionID = ".self::getDB()->sanitize($intTopLevelConditionID)."
			AND intChildConditionID = ".self::getDB()->sanitize($intChildConditionID)."
			AND intParentConditionID IS NULL
			LIMIT 1";
		//echo $strSQL . "<br />";
		$rsResult = $this->getDB()->query($strSQL);
		$arrRow = $this->getDB()->fetch_assoc($rsResult);
		$this->setVarsFromRow($arrRow);
	}

	function getChildCondition() {
		include_once("Condition.class.php");
		if(!$this->_objChildCondition) {
			$this->_objChildCondition = new Condition();
		}
		return $this->_objChildCondition;
	}
}
?>