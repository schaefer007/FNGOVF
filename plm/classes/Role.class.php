<?php
	require_once("ArrayClass.class.php");
	require_once("UserRoleXR.class.php");

	class RoleArray extends ArrayClass {
		function __construct(){
			parent::__construct("Role");
		}

		function load() {
			$strSQL = " SELECT * FROM dbPLM.tblRole
				ORDER BY tblRole.strRoleName";
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				$this->_arrObjects[$arrRow["intRoleID"]] = new Role();
				$this->_arrObjects[$arrRow["intRoleID"]]->setVarsFromRow($arrRow);
			}
		}

		function loadActiveRoles($intUserID=null) {
			$strTableSQL = $strWhereSQL = "";
			if($intUserID) {
				$strTableSQL = "
					INNER JOIN dbPLM.tblUserRoleXR
						ON tblUserRoleXR.intRoleID = tblRole.intRoleID ";
				$strWhereSQL = " AND tblUserRoleXR.intUserID = ".self::getDB()->sanitize($intUserID);
			}

			$strSQL = " SELECT * FROM dbPLM.tblRole
				$strTableSQL
				WHERE strStatus = 'Active'
				$strWhereSQL
				GROUP BY tblRole.intRoleID
				ORDER BY tblRole.strRoleName";
			//echo $strSQL;
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				$this->_arrObjects[$arrRow["intRoleID"]] = new Role();
				$this->_arrObjects[$arrRow["intRoleID"]]->setVarsFromRow($arrRow);
			}
		}

		function loadByIDs($arrRoleIDs, $blnLoadUsers=false, $blnCurrentUserOnly=false) {
			$strTableSQL = "";
			$strSelectSQL = " tblRole.* ";
			if($blnLoadUsers) {
				$strSelectSQL = " tblUser.*, " . $strSelectSQL;
				$strTableSQL = "
					LEFT JOIN dbPLM.tblUserRoleXR
						ON tblRole.intRoleID = tblUserRoleXR.intRoleID
					LEFT JOIN dbPLM.tblUser
						ON tblUser.intUserID = tblUserRoleXR.intUserID
				";
			}
			$strSQL = " SELECT $strSelectSQL
				FROM dbPLM.tblRole
				$strTableSQL
				WHERE tblRole.intRoleID IN ('".implode("','", $arrRoleIDs)."')";
			//echo $strSQL;
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				if(!isset($this->_arrObjects[$arrRow["intRoleID"]])) {
					$this->_arrObjects[$arrRow["intRoleID"]] = new Role();
					$this->_arrObjects[$arrRow["intRoleID"]]->setVarsFromRow($arrRow);
				}
				if($blnLoadUsers && (!$blnCurrentUserOnly || $_SESSION["objUser"]->getUserID() == $arrRow["intUserID"])) {
					$this->_arrObjects[$arrRow["intRoleID"]]->getUserArray()->addFromRow($arrRow, "intUserID");
				}
			}
		}

		function loadForPermissionManagementBySecurityItem($intSecurityItemID) {
			$strSQL = " SELECT tblPermission.*, tblRolePermission.*, tblRole.*
				FROM dbPLM.tblRole
				LEFT JOIN dbPLM.tblRolePermission
					ON tblRolePermission.intRoleID = tblRole.intRoleID
				LEFT JOIN dbPLM.tblPermission
					ON tblPermission.intPermissionID = tblRolePermission.intPermissionID
					AND tblPermission.intSecurityItemID = ".self::getDB()->sanitize($intSecurityItemID)."
				WHERE strStatus = 'Active'
				ORDER BY tblRole.strRoleName";
			//echo $strSQL;
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				if(!isset($this->_arrObjects[$arrRow["intRoleID"]])) {
					$this->_arrObjects[$arrRow["intRoleID"]] = new Role();
					$this->_arrObjects[$arrRow["intRoleID"]]->setVarsFromRow($arrRow);
				}
				if($arrRow["intPermissionID"]) {
					$this->_arrObjects[$arrRow["intRoleID"]]->getPermissionArray()->addFromRow($arrRow);
				}
			}
		}

		function loadForRoleManagement() {
			$objSearch = $_SESSION["objSearch"];

			$strSQL = " SELECT tblUser.*, tblUserRoleXR.*, tblRole.*
				FROM dbPLM.tblRole
				LEFT JOIN dbPLM.tblUserRoleXR
					ON tblRole.intRoleID = tblUserRoleXR.intRoleID
				LEFT JOIN dbPLM.tblUser
					ON tblUser.intUserID = tblUserRoleXR.intUserID";
			$strSQL .= $objSearch->getRoleListPage()->getSortQuery() . ", strDisplayName ";
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				if(!isset($this->_arrObjects[$arrRow["intRoleID"]])) {
					$this->_arrObjects[$arrRow["intRoleID"]] = new Role();
					$this->_arrObjects[$arrRow["intRoleID"]]->setVarsFromRow($arrRow);
				}
				if($arrRow["intUserID"]) {
					$objUserRoleXR = new UserRoleXR();
					$objUserRoleXR->setVarsFromRow($arrRow);
					$objUserRoleXR->getUser()->setVarsFromRow($arrRow);
					$this->_arrObjects[$arrRow["intRoleID"]]->getUserRoleXRArray()->setObject($arrRow["intUserRoleXRID"], $objUserRoleXR);
				}
			}
		}

		function loadForRoleNotificationType($intNotificationTypeID){
			if(!$intNotificationTypeID)
				return false;

			$strSQL = "SELECT tblRole.*
				FROM tblRole
				LEFT JOIN dbSystem.tblRoleNotificationType
					ON tblRoleNotificationType.intRoleID = tblRole.intRoleID
					AND intNotificationTypeID = ".self::getDB()->sanitize($intNotificationTypeID)."
				WHERE tblRoleNotificationType.intRoleNotificationTypeID IS NULL
				ORDER BY tblRole.strRoleName ";
			//echo $strSQL;
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				$this->_arrObjects[$arrRow["intRoleID"]] = new Role();
				$this->_arrObjects[$arrRow["intRoleID"]]->setVarsFromRow($arrRow);
			}
		}

		function loadByProgramID($intProgramID, $arrRoleIDs){
			if(!$intProgramID)
				return false;

			$strSQL = "SELECT tblRole.*, tblUser.*
				FROM dbPLM.tblRole
				INNER JOIN dbPLM.tblMemberRole
					ON tblMemberRole.intRoleID = tblRole.intRoleID
				INNER JOIN dbPLM.tblMember
					ON tblMember.intMemberID = tblMemberRole.intMemberID
				INNER JOIN dbPLM.tblProduct
					ON tblProduct.intProgramID = tblMember.intProgramID
				INNER JOIN dbPLM.tblUser
					ON tblMember.intUserID = tblUser.intUserID
				WHERE tblProduct.intProgramID = ".self::getDB()->sanitize($intProgramID);
			if(is_array($arrRoleIDs)) {
				$strSQL .= " AND tblRole.intRoleID IN ('".implode("','", $arrRoleIDs)."')";
			}
			//echo $strSQL;
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				if(!isset($this->_arrObjects[$arrRow["intRoleID"]])) {
					$this->_arrObjects[$arrRow["intRoleID"]] = new Role();
					$this->_arrObjects[$arrRow["intRoleID"]]->setVarsFromRow($arrRow);
				}
				$this->_arrObjects[$arrRow["intRoleID"]]->getUserArray()->addFromRow($arrRow, "intUserID");
			}
		}

		function loadByPermissionID($intPermissionID){
			if(!$intPermissionID)
				return false;

			$strSQL = "SELECT tblRole.*
				FROM dbPLM.tblRolePermission
				INNER JOIN dbPLM.tblRole
					ON tblRole.intRoleID = tblRolePermission.intRoleID
				WHERE tblRolePermission.intPermissionID = ".self::getDB()->sanitize($intPermissionID);
			//echo $strSQL . "<br />";
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				$this->_arrObjects[$arrRow["intRoleID"]] = new Role();
				$this->_arrObjects[$arrRow["intRoleID"]]->setVarsFromRow($arrRow);
			}
		}

		function loadByProcessTransitionInstanceID($intProcessTransitionInstanceID) {
			if(!$intProcessTransitionInstanceID)
				return false;

			$strSQL = "SELECT tblRole.*
				FROM dbPLM.tblRole
				INNER JOIN dbSystem.tblCondition
					ON tblCondition.intRoleID = tblRole.intRoleID
				INNER JOIN dbSystem.tblConditionInstance
					ON tblConditionInstance.intConditionID = tblCondition.intConditionID
				WHERE intProcessTransitionInstanceID = ".$this->getDB()->sanitize($intProcessTransitionInstanceID)."
			";
			$rsResult = $this->getDB()->query($strSQL);
			while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
				$this->_arrObjects[$arrRow["intRoleID"]] = new Role();
				$this->_arrObjects[$arrRow["intRoleID"]]->setVarsFromRow($arrRow);
			}
		}
	}

	require_once("DataClass.class.php");

	class RoleBase extends DataClass {
		protected $_intRoleID;
		protected $_strRoleName;
		protected $_strStatus;
		protected $_blnPlantLevel;
		protected $_blnOverrideProgramSecurity;
		protected $_intCreatedByUserID;
		protected $_dtmCreatedOn;
		protected $_alertInterval;

		function __construct($intRoleID=null) {
			$this->DataClass();
			if($intRoleID) {
				$this->load($intRoleID);
			}
		}

		protected function insert() {
			base::write_log("Role created","S");
			$strSQL = "INSERT INTO dbPLM.tblRole SET ";
			$strConnector = "";
			$strSQL .= $strConnector . "intRoleID = ".$this->getDB()->sanitize(self::getRoleID());
			$strConnector = ",";
			if(isset($this->_strRoleName)) {
				$strSQL .= $strConnector . "strRoleName = ".$this->getDB()->sanitize(self::getRoleName());
				$strConnector = ",";
			}
			if(isset($this->_strStatus)) {
				$strSQL .= $strConnector . "strStatus = ".$this->getDB()->sanitize(self::getStatus());
				$strConnector = ",";
			}
			if(isset($this->_blnPlantLevel)) {
				$strSQL .= $strConnector . "blnPlantLevel = ".$this->getDB()->sanitize(self::getPlantLevel());
				$strConnector = ",";
			}
			if(isset($this->_blnOverrideProgramSecurity)) {
				$strSQL .= $strConnector . "blnOverrideProgramSecurity = ".$this->getDB()->sanitize(self::getOverrideProgramSecurity());
				$strConnector = ",";
			}
			if(isset($this->_intCreatedByUserID)) {
				$strSQL .= $strConnector . "intCreatedByUserID = ".$this->getDB()->sanitize(self::getCreatedByUserID());
				$strConnector = ",";
			}
			if(isset($this->_dtmCreatedOn)) {
				$strSQL .= $strConnector . "dtmCreatedOn = ".$this->getDB()->sanitize(self::getCreatedOn());
				$strConnector = ",";
			}
			//echo $strSQL;
			$this->getDB()->query($strSQL);
			$this->setRoleID($this->getDB()->insert_id());
			return $this->getRoleID();
		}

		protected function update() {
			base::write_log("Role Update","S");
			$strSQL = "UPDATE dbPLM.tblRole SET ";
			$strConnector = "";
			$strSQL .= $strConnector . "intRoleID = ".$this->getDB()->sanitize(self::getRoleID());
			$strConnector = ",";
			if(isset($this->_strRoleName)) {
				$strSQL .= $strConnector . "strRoleName = ".$this->getDB()->sanitize(self::getRoleName());
				$strConnector = ",";
			}
			if(isset($this->_strStatus)) {
				$strSQL .= $strConnector . "strStatus = ".$this->getDB()->sanitize(self::getStatus());
				$strConnector = ",";
			}
			if(isset($this->_blnPlantLevel)) {
				$strSQL .= $strConnector . "blnPlantLevel = ".$this->getDB()->sanitize(self::getPlantLevel());
				$strConnector = ",";


			}
			if(isset($this->_blnOverrideProgramSecurity)) {
				$strSQL .= $strConnector . "blnOverrideProgramSecurity = ".$this->getDB()->sanitize(self::getOverrideProgramSecurity());
				$strConnector = ",";
			}
			if(isset($this->_intCreatedByUserID)) {
				$strSQL .= $strConnector . "intCreatedByUserID = ".$this->getDB()->sanitize(self::getCreatedByUserID());
				$strConnector = ",";
			}
			if(isset($this->_dtmCreatedOn)) {
				$strSQL .= $strConnector . "dtmCreatedOn = ".$this->getDB()->sanitize(self::getCreatedOn());
				$strConnector = ",";
			}
			$strSQL .= " WHERE intRoleID = ".$this->getDB()->sanitize(self::getRoleID())."";
			//echo $strSQL;
			return $this->getDB()->query($strSQL);
		}

		public function save() {
			if($this->_intRoleID) {
				return $this->update();
			} else {
				return $this->insert();

			}
		}

		public function delete() {
			if($this->_intRoleID) {
				base::write_log("Role delete","S");
				$strSQL = "DELETE FROM dbPLM.tblRole
				WHERE intRoleID = '$this->_intRoleID'
				";
				return $this->getDB()->query($strSQL);
			}
		}

		public function load($intRoleID) {
			if($intRoleID) {
				$strSQL = "SELECT *
					FROM dbPLM.tblRole
					WHERE intRoleID = '$intRoleID'
					LIMIT 1
				";
				$rsRole = $this->getDB()->query($strSQL);
				$arrRole = $this->getDB()->fetch_assoc($rsRole);
				$this->setVarsFromRow($arrRole);
			}

		}

		function setVarsFromRow($arrRole) {
			parent::setVarsFromRow($arrRole);
			if(isset($arrRole["intRoleID"])) $this->_intRoleID = $arrRole["intRoleID"];
			if(isset($arrRole["strRoleName"])) $this->_strRoleName = $arrRole["strRoleName"];
			if(isset($arrRole["strStatus"])) $this->_strStatus = $arrRole["strStatus"];
			if(isset($arrRole["blnPlantLevel"])) $this->_blnPlantLevel = $arrRole["blnPlantLevel"];
			if(isset($arrRole["blnOverrideProgramSecurity"])) $this->_blnOverrideProgramSecurity = $arrRole["blnOverrideProgramSecurity"];
			if(isset($arrRole["intCreatedByUserID"])) $this->_intCreatedByUserID = $arrRole["intCreatedByUserID"];
			if(isset($arrRole["dtmCreatedOn"])) $this->_dtmCreatedOn = $arrRole["dtmCreatedOn"];
			if(isset($arrRole["alert_interval"])) $this->_alertInterval = $arrRole["alert_interval"];
		}
		function getAlertInterval() {
			return $this->_alertInterval;
		}
		function setAlertInterval($value) {
			if($this->_alertInterval !== $value) {
				$this->_alertInterval = $value;
				$this->_blnDirty = true;
			}
		}

		function getRoleID() {
			return $this->_intRoleID;
		}
		function setRoleID($value) {
			if($this->_intRoleID !== $value) {
				$this->_intRoleID = $value;
				$this->_blnDirty = true;
			}
		}

		function getRoleName() {
			return $this->_strRoleName;
		}
		function setRoleName($value) {
			if($this->_strRoleName !== $value) {
				$this->_strRoleName = $value;
				$this->_blnDirty = true;
			}
		}

		function getStatus() {
			return $this->_strStatus;
		}
		function setStatus($value) {
			if($this->_strStatus !== $value) {
				$this->_strStatus = $value;
				$this->_blnDirty = true;
			}
		}

		function getPlantLevel() {
			return $this->_blnPlantLevel;
		}
		function setPlantLevel($value) {
			if($this->_blnPlantLevel !== $value) {
				$this->_blnPlantLevel = $value;
				$this->_blnDirty = true;
			}
		}

		function getOverrideProgramSecurity() {
			return $this->_blnOverrideProgramSecurity;
		}
		function setOverrideProgramSecurity($value) {
			if($this->_blnOverrideProgramSecurity !== $value) {
				$this->_blnOverrideProgramSecurity = $value;
				$this->_blnDirty = true;
			}
		}

		function getCreatedByUserID() {
			return $this->_intCreatedByUserID;

		}
		function setCreatedByUserID($value) {
			if($this->_intCreatedByUserID !== $value) {
				$this->_intCreatedByUserID = $value;
				$this->_blnDirty = true;
			}
		}

		function getCreatedOn() {
			return $this->_dtmCreatedOn;
		}
		function setCreatedOn($value) {
			if($this->_dtmCreatedOn !== $value) {
				$this->_dtmCreatedOn = $value;
				$this->_blnDirty = true;
			}
		}

	}

	include_once("UserRoleXR.class.php");
	include_once("User.class.php");
	include_once("Permission.class.php");
	include_once("ProcessTransition.class.php");

	class Role extends RoleBase {
		private $_objUserRoleXRArray;
		private $_objUserArray; // Users who  have this role (in a given program with RoleArray::loadByProgramID())
		private $_objPermissionArray;
		private $_objProcessTransitionArray;

		protected $_blnHideByDefault = true;

		function __construct($intRoleID=null) {
			parent::__construct($intRoleID);
		}

		function loadByRoleName($strRoleName){
			if(!$strRoleName)
				return false;

			$strSQL = "SELECT *
					FROM dbPLM.tblRole
					WHERE strRoleName = '".$this->getDB()->sanitize($strRoleName)."
					LIMIT 1
				";
			$rsRole = $this->getDB()->query($strSQL);
			$arrRole = $this->getDB()->fetch_assoc($rsRole);
			$this->setVarsFromRow($arrRole);
		}

		function getID(){
			return $this->getRoleID();
		}
		function getName(){
			return $this->getRoleName();
		}

		function getURL() {
			return "role.php?intRoleID=".$this->getRoleID();
		}

		function getUserRoleXRArray(){
			if(!$this->_objUserRoleXRArray) {
				$this->_objUserRoleXRArray = new UserRoleXRArray();
			}
			return $this->_objUserRoleXRArray;
		}
		function getUserArray(){
			if(!$this->_objUserArray) {
				$this->_objUserArray = new UserArray();
			}
			return $this->_objUserArray;
		}
		function getPermissionArray(){
			if(!$this->_objPermissionArray) {
				$this->_objPermissionArray = new PermissionArray();
			}
			return $this->_objPermissionArray;
		}
		function getProcessTransitionArray(){
			if(!$this->_objProcessTransitionArray) {
				$this->_objProcessTransitionArray = new ProcessTransitionArray();
			}
			return $this->_objProcessTransitionArray;
		}

		function getHideByDefault() {
			return $this->_blnHideByDefault;
		}
		function setHideByDefault($value) {
			$this->_blnHideByDefault = $value;
		}
	}
?>