<?php
require_once("ArrayClass.class.php");

class ProcessPathTransitionArray extends ArrayClass {
	function __construct(){
		parent::__construct("ProcessPathTransition");
	}

	function load() {
		$strSQL = " SELECT * FROM dbSystem.tblProcessPathTransition";
		$rsResult = $this->getDB()->query($strSQL);
		while ($arrRow = $this->getDB()->fetch_assoc($rsResult)) {
			$this->_arrObjects[$arrRow["intProcessPathTransitionID"]] = new ProcessPathTransition();
			$this->_arrObjects[$arrRow["intProcessPathTransitionID"]]->setVarsFromRow($arrRow);
		}
	}
}

require_once("DataClass.class.php");

class ProcessPathTransitionBase extends DataClass {
	protected $_intProcessPathTransitionID;
	protected $_intProcessPathID;
	protected $_intProcessTransitionID;

	function __construct($intProcessPathTransitionID=null) {
		$this->DataClass();
		if($intProcessPathTransitionID) {
			$this->load($intProcessPathTransitionID);
		}
	}

	protected function insert() {
		base::write_log("Process path transition created","S");
		$strSQL = "INSERT INTO dbSystem.tblProcessPathTransition SET ";
		$strConnector = "";
		$strSQL .= $strConnector . "intProcessPathTransitionID = ".$this->getDB()->sanitize(self::getProcessPathTransitionID());
		$strConnector = ",";
		if(isset($this->_intProcessPathID)) {
			$strSQL .= $strConnector . "intProcessPathID = ".$this->getDB()->sanitize(self::getProcessPathID());
			$strConnector = ",";
		}
		if(isset($this->_intProcessTransitionID)) {
			$strSQL .= $strConnector . "intProcessTransitionID = ".$this->getDB()->sanitize(self::getProcessTransitionID());
			$strConnector = ",";
		}
		//echo $strSQL;
		$this->getDB()->query($strSQL);
		$this->setProcessPathTransitionID($this->getDB()->insert_id());
		return $this->getProcessPathTransitionID();
	}

	protected function update() {
		base::write_log("Process path transition updated","S");
		$strSQL = "UPDATE dbSystem.tblProcessPathTransition SET ";
		$strConnector = "";
		$strSQL .= $strConnector . "intProcessPathTransitionID = ".$this->getDB()->sanitize(self::getProcessPathTransitionID());
		$strConnector = ",";
		if(isset($this->_intProcessPathID)) {
			$strSQL .= $strConnector . "intProcessPathID = ".$this->getDB()->sanitize(self::getProcessPathID());
			$strConnector = ",";
		}
		if(isset($this->_intProcessTransitionID)) {
			$strSQL .= $strConnector . "intProcessTransitionID = ".$this->getDB()->sanitize(self::getProcessTransitionID());
			$strConnector = ",";
		}
		$strSQL .= " WHERE intProcessPathTransitionID = ".$this->getDB()->sanitize(self::getProcessPathTransitionID())."";
		//echo $strSQL;
		return $this->getDB()->query($strSQL);
	}

	public function save() {
		if($this->_intProcessPathTransitionID) {
			return self::update();
		} else {
			return self::insert();
		}
	}

	public function delete() {
		if($this->_intProcessPathTransitionID) {
			base::write_log("Process path transition deleted");
			$strSQL = "DELETE FROM dbSystem.tblProcessPathTransition
				WHERE intProcessPathTransitionID = '$this->_intProcessPathTransitionID'
				";
			return $this->getDB()->query($strSQL);
		}
	}

	public function load($intProcessPathTransitionID) {
		if(!$intProcessPathTransitionID) {
			return false;
		}

		$strSQL = "SELECT *
				FROM dbSystem.tblProcessPathTransition
				WHERE intProcessPathTransitionID = ".self::getDB()->sanitize($intProcessPathTransitionID)."
				LIMIT 1
			";
		$rsResult = $this->getDB()->query($strSQL);
		$arrRow = $this->getDB()->fetch_assoc($rsResult);
		$this->setVarsFromRow($arrRow);
	}

	function setVarsFromRow($arrRow) {
		if(isset($arrRow["intProcessPathTransitionID"])) $this->_intProcessPathTransitionID = $arrRow["intProcessPathTransitionID"];
		if(isset($arrRow["intProcessPathID"])) $this->_intProcessPathID = $arrRow["intProcessPathID"];
		if(isset($arrRow["intProcessTransitionID"])) $this->_intProcessTransitionID = $arrRow["intProcessTransitionID"];
	}

	function getProcessPathTransitionID() {
		return $this->_intProcessPathTransitionID;
	}
	function setProcessPathTransitionID($value) {
		if($this->_intProcessPathTransitionID !== $value) {
			$this->_intProcessPathTransitionID = $value;
			$this->_blnDirty = true;
		}
	}
	function getProcessPathID() {
		return $this->_intProcessPathID;
	}
	function setProcessPathID($value) {
		if($this->_intProcessPathID !== $value) {
			$this->_intProcessPathID = $value;
			$this->_blnDirty = true;
		}
	}
	function getProcessTransitionID() {
		return $this->_intProcessTransitionID;
	}
	function setProcessTransitionID($value) {
		if($this->_intProcessTransitionID !== $value) {
			$this->_intProcessTransitionID = $value;
			$this->_blnDirty = true;
		}
	}
}

class ProcessPathTransition extends ProcessPathTransitionBase {
	function __construct($intProcessPathTransitionID=null) {
		parent::__construct($intProcessPathTransitionID);
	}
}
?>