/* Error/Warning Handling */
.messages {
	font-size:14px;
	font-weight:bold;
}

.error_messages, .warning_messages, .success_messages {
	padding:5px;
}
.error_message, .warning_message, .success_message, .warning_messages_header, .error_messages_header, .success_messages_header {
	color:white;
}
.warning_messages_header, .error_messages_header, .success_messages_header {
	font-weight:bold;
}

.error_messages {
	background-color:#FF0000;
}
.warning_messages {
	background-color:#EE8800;
}
.success_messages {
	background-color:#00EE00;
}
/* ENd Error/Warning Handling */


/* Help */
#help {
	font-size:14px;
	font-weight:bold;
	position:absolute;
	border:2px solid #303030;
	background-color:#ffffc4;
	padding:12px;
	z-index:3;

	border-radius:5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	box-shadow:8px 8px 8px rgba(0, 0, 0, 0.2);
	-moz-box-shadow:8px 8px 8px rgba(0, 0, 0, 0.2);
	-webkit-box-shadow:8px 8px 8px rgba(0, 0, 0, 0.2);
}

#floating_save {
	padding:12px;
	background-color:#E3E4FA;
	border:1px solid #000;
}

#save_menu {
	border-top:1px solid #C0C0C0;
	width:100%;
	padding-top:5px;
	padding-left:6px;
	background-image:url('images/links_bg.png');
	bottom:0;
	left:0;
	position:fixed;
	height:30px;
	z-index:2;
}
#save_menu input {
	height:25px;
	font-size:16px;
}

/* Enlarged Photo */
#enlarged_photo {
	position:absolute;
	top:10px;
	left:10px;
	padding:10px;
	background-color:white;
	border:1px solid #B0B0B0;
}
#enlarged_photo .photo {
	border:1px solid #D0D0D0;
}
#enlarged_photo .close {
	width:100%;
	text-align:center;
	background-color:#303030;
}
#enlarged_photo .close div {
	padding:8px;
}
#enlarged_photo .close a {
	color:white;
}
/* End Enlarged Photo */