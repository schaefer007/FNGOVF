.edit_field .label, .subject .label { width:150px; }
.edit_field .input { padding-right:5px; }
.edit_field_program select { width:180px; }

.subject { padding-bottom:7px; }
.subject input { width:700px; }

.edit_field_description textarea {
	width:700px;
	height:100px;
}

.attachment_column {
	width:480px;
	float:left;
}

.line_item .icon_name_actions {
	width:525px !important;
}
.line_item .name {
	width:340px !important;
}