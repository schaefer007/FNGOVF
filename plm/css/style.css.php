<?php
	include_once("constants.php");
?>
html {
	overflow-y:scroll;
	height:100%;
}
body {
	margin:0px;
	background-image:url('<?php echo strSITE_URL; ?>images/bg.png');
	font-family:arial;
	font-size:12px;
	height:100%;
	text-align:left;
	min-width:1135px;
}
div.content {
	padding:112px 10px 30px 10px;
}

div, span {
	padding:0px;
	margin:0px;
}

table {
	font-size:12px;
	border-collapse:collapse;
	margin:0px;
	padding:0px;
}

tr {
	margin:0px;
	padding:0px;
}

td {
	text-align:left;
	vertical-align:top;
	margin:0px;
	padding:0px;
}

th {
	text-align:left;
}

form {
	display:inline;
	padding:0px;
	margin:0px;
}
input, select {
	margin:0px 0px 0px 0px;
}
textarea {
	padding:0px;
	behavior:url('<?php echo strSITE_URL; ?>css/maxlength.htc');
}

img {
	border:0px;
}

hr {
	margin-top:4px;
	margin-bottom:4px;
}

h1, h2, h3, h4, h5, h6 {
	display:inline; /* remove carriage return */
	font-family:calibri;
}
h2 {
	font-size:26px;
}
h3 {
	font-size:20px;
	margin:0px;
	float:left;
}
h4 {
	font-size:16px;
	margin:0px;
	float:left;
}
h5 {
	font-size:14px;
	margin:0px;
	float:left;
}

.clL { clear:left; }
.clB { clear:both; }