/*.table_wf_processes .process_history { display:none; }*/

.table_wf_processes .state { width:340px; font-weight:bold; }
.table_wf_processes .spacer { width:20px; }
.table_wf_processes .role { width:160px; }
.table_wf_processes .user { width:160px; }
.table_wf_processes .user_undefined { color:red; }
.table_wf_processes .actions { width:320px; }
.table_wf_processes .actions input { margin:0px 2px 0px 2px; }
.table_wf_processes .action { width:210px; }
.table_wf_processes .date { width:110px; }
.table_wf_processes .comment { width:260px; }
.table_wf_processes .comment input { width:260px; }

.table_wf_processes td.state, .table_wf_processes td.state_type { border-top:1px solid #A0A0A0; }

.table_wf_processes .state_type { text-align:right; }
.table_wf_processes .old_state { color:#A0A0A0; }
.table_wf_processes .current_state { color:green; }
.table_wf_processes .future_state { color:#AA0000; }
.table_wf_processes .final_state { color:#0000AA; }