<?php
	$intBorderRadius = "3px";
?>
/* Old Table CSS, applied everywhere (for now) */
table.output_table {
	-moz-border-radius:<?php echo $intBorderRadius; ?>;
	-webkit-border-radius:<?php echo $intBorderRadius; ?>;
	border-radius:<?php echo $intBorderRadius; ?>;
	font-size:12px;
	background-color:#B9D9FF;
	table-layout:fixed;
}
table.output_table tr:last-child td:first-child {
	-moz-border-radius-bottomleft:<?php echo $intBorderRadius; ?>;
	-webkit-border-bottom-left-radius:<?php echo $intBorderRadius; ?>;
	border-bottom-left-radius:<?php echo $intBorderRadius; ?>
}
table.output_table tr:last-child td:last-child {
	-moz-border-radius-bottomright:<?php echo $intBorderRadius; ?>;
	-webkit-border-bottom-right-radius:<?php echo $intBorderRadius; ?>;
	border-bottom-right-radius:<?php echo $intBorderRadius; ?>;
}
table.no_rounded_corners {
	-moz-border-radius:0px;
	-webkit-border-radius:0px;
	border-radius:0px;
}
table.no_rounded_corners tr:last-child td:first-child {
	-moz-border-radius-bottomleft:0px;
	-webkit-border-bottom-left-radius:0px;
	border-bottom-left-radius:0px
}
table.no_rounded_corners tr:last-child td:last-child {
	-moz-border-radius-bottomright:0px;
	-webkit-border-bottom-right-radius:0px;
	border-bottom-right-radius:0px;
}
table.output_table tr.output_table_header {
}
table.output_table tr.padding th, table.output_table tr.padding td {
	padding:4px;
}
/*table.output_table tr.padding_small th, table.output_table tr.padding_small td {
	padding:3px;
}*/
table.output_table_input tr.padding th, table.output_table_input tr.padding td, table.output_table .padding_input td, table.output_table td.padding_input {
	padding:1px 3px 1px 3px !important;
}
table.output_table .no_padding {
	padding:0px !important;
}
table.output_table .action {
	padding:0px 2px 0px 2px !important;
}
table.output_table_input .action {
	padding:2px 2px 0px 2px !important;
}
table.output_table .expander {
	background-color:#B9D9FF;
	padding:0px !important;
}

table.table .subheader { background-color:#DAE3F7 !important; }
.altBG0_0 { background-color:#E3EDFF !important; }
.altBG0_1 { background-color:#F6F9FA !important; }
.altBG1_0 { background-color:#EAF4FF !important; }
.altBG1_1 { background-color:#FAFDFF !important; }

table.output_subtable {
	width:100%;
	font-size:12px;
	background-color:#C9E9FF;
}

table.output_table .total {
	font-weight:bold;
	border-bottom:1px solid #808080;
}
table.output_table .subtotal {
	font-weight:bold;
	border-bottom:1px solid #A0A0A0;
}


.input_table .row {
	padding-top:2px;
	padding-bottom:4px;
}
.input_table .cell {
	padding-right:4px;
	float:left;
}
.input_table .label_horizontal {
	padding-top:4px;
	float:left;
}
.input_table .label_vertical, .input_table .input_vertical {
	padding-right:6px;
}



/* New table CSS */
table.table {
	-moz-border-radius:<?php echo $intBorderRadius; ?>;
	-webkit-border-radius:<?php echo $intBorderRadius; ?>;
	border-radius:<?php echo $intBorderRadius; ?>;
	font-size:12px;
	background-color:#B9D9FF;
	table-layout:fixed;
}
table.table tr:last-child td:first-child {
	-moz-border-radius-bottomleft:<?php echo $intBorderRadius; ?>;
	-webkit-border-bottom-left-radius:<?php echo $intBorderRadius; ?>;
	border-bottom-left-radius:<?php echo $intBorderRadius; ?>
}
table.table tr:last-child td:last-child {
	-moz-border-radius-bottomright:<?php echo $intBorderRadius; ?>;
	-webkit-border-bottom-right-radius:<?php echo $intBorderRadius; ?>;
	border-bottom-right-radius:<?php echo $intBorderRadius; ?>;
}
table.table_input td,
table.table td.input {
	padding:1px 4px 1px 4px;
}
table.table th,
table.table_output td,
table.table td.output {
	padding:4px;
}
table.table tr.table_header th {
}
table.table .input {
	float:none !important;/* This is temporary, because of .label, .input, .file { */
}
table.table .radio { padding-top:4px !important; }