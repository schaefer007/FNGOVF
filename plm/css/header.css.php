#header {
	width:100%;
	min-width:1030px;
	position:fixed;
	z-index:2;
}
.header, .footer {
	width:100%;
	background-color:#5b5b5b;
	color:white;
}
.header {
	height:75px;
}
.header .logo, .header .site_name {
	float:left;
	margin:0px 10px 0px 10px;
}
.header .logo {
}
.header .login {
	float:right;
	padding-right:15px;
	color:white;
}
.header .login .label {
	width:80px;
}
.header .login div {
	float:left;
}
.footer {
	height:45px;
	text-align:center;
}

.header_links a {
	color:white;
}

.links {
	width:100%;
	min-height:30px;
}
.links_main {
	background-image:url('images/links_bg.png');
	background-repeat:x-repeat;
}
.links a {
	float:left;
}
.links .logo {
	padding-top:2px;
	padding-left:14px;
	padding-right:14px;
}
.links .link {
	padding:5px 15px 5px 15px;
	float:left;
}
.links .link:hover {
	background-color:#66BBEE;
}
.links .link .text {
	height:17px;
	padding-top:3px;
}
.links .link .add {
	width:20px;
	height:20px;
	position:relative;
	left:5px;
}
.links_divider {
	font-size:1px;
	height:1px;
	background-color:#C0C0C0;
}
.links .link_header {
	font-weight:bold;
}
.links .sub_link {
	padding-left:25px;
}

.submenu {
	display:none;
	background-color:#e1e6e2;
	border-left:1px solid #C0C0C0;
	border-bottom:1px solid #C0C0C0;
	border-right:1px solid #C0C0C0;
}
.submenu .link {
	width:160px;
	border-top:1px solid #D5D5D5;
}