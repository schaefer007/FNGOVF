@media print {
	#header {
		display:none;
	}
	div.content {
		padding-top:2px !important; /* Remove padding for header */
	}
}
input[type="text"], input[type="date"], input[type="datetime"], input[type="password"], select, textarea {
	border:1px solid #92ADC9;
}
*[disabled="true"] {
	background-color:#F0F0F0;
	color:#202020;
}
input[readonly="true"], input[disabled="true"]:not([type="button"]), select[disabled="true"] {
	background-color:#E7E7E7;
	color:#777;
}
input[disabled="true"][type="button"] {
	color:#AAA;
}
h1 {
	font-size:36px;
	letter-spacing:6px;
}
input[type="date"], input[class~="date"] {
	width:100px;
}
a {
	color:blue;
}
/*.textarea {
	width:800px;
	height:48px;
}*/

.h2_caption {
	font-size:11px;
	color:#707070;
}

.box {
	background-color:#FEFEFE;
	border:1px solid #B0D0E0;
	padding:8px;
	margin-right:8px;
	display:table-cell;

	border-radius:5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	box-shadow:8px 8px 8px rgba(100, 180, 200, 0.07);
	-moz-box-shadow:8px 8px 8px rgba(100, 180, 200, 0.07);
	-webkit-box-shadow:8px 8px 8px rgba(100, 180, 200, 0.07);
}

.heading {
	padding-bottom:4px;
}
.heading .expand_collapse_icon {
	float:left;
	padding-top:6px;
	padding-right:4px;
}
.heading .text {
	float:left;
}
.heading .edit_button {
	float:left;
	padding-left:20px;
}

.command * { /* For IE7 and perhaps lower */
	cursor:pointer;
}
.command {
	float:left;
	margin:0px 3px;
	padding:1px 15px 0px 13px;
	background-color:#F3F3F3;
	border-radius:5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	box-shadow:1px 1px 1px rgba(0, 0, 0, 0.2);
	-moz-box-shadow:1px 1px 1px rgba(0, 0, 0, 0.2);
	-webkit-box-shadow:1px 1px 1px rgba(0, 0, 0, 0.2);
}
.command_h2 { margin-top:6px; }
.command_h4 { padding-top:0px !important; }
.command div, .command { height:20px; }
.command div { float:left; }
.command .icon { padding-right:4px; }
.command .text { line-height:22px; }
.command_divider {
	width:2px;
	height:20px;
	background-color:#E0E0E0;
	float:left;
	margin-left:4px;
	margin-right:3px;
}
.command_divider_h2 {
	margin-top:7px;
}

.column {
	float:left;
}
.big_column {
	float:left
}

.output_data .field {
	padding-bottom:4px;
	display:table-cell;
}
.output_data .label {
	font-weight:bold;
}

.label, .input, .file {
	float:left;
}




.header_output {
	font-size:20px;
	font-weight:bold;
	float:left;
}
.file {
	padding-top:4px;
	padding-left:20px;
}
.field .label {
	padding-right:4px;
}

.filter, .filter div {
	float:left;
}
.filter .label {
	padding-right:4px;
	padding-top:4px;
	float:none;
}
.filter .input {
	padding-right:9px;
	float:none;
}

.clickable {
	cursor:pointer;
}

.de .number, .de .price, .input .number, .input_number {
	text-align:right;
}
.report .number, .report .price {
	text-align:center;
}

.small_print {
	border:1px solid #C0C0C0;
	max-width:30px;
	max-height:28px;
}
.medium_print {
	padding:2px;
	border:1px solid #C0C0C0;
	max-width:400px;
	max-height:180px;
	border-radius:5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
}
.not_submitted {
	color:red;
	font-weight:bold;
	float:left;
}
.no_upload {
	color:red;
}

.vertical_text {
	height:100%;
	overflow:hidden;
	float:left;
	-moz-transform: rotate(-90deg);  /* FF3.5+ */
	-o-transform: rotate(-90deg);  /* Opera 10.5 */
	-webkit-transform: rotate(-90deg);  /* Saf3.1+, Chrome */
	filter:  progid:DXImageTransform.Microsoft.BasicImage(rotation=3);  /* IE6,IE7 */
	-ms-filter: "progid:DXImageTransform.Microsoft.BasicImage(rotation=3)"; /* IE8 */
}

.unsubmitted_text {
	color:red;
	float:left;
	font-size:12px;
	font-weight:bold;
	padding-top:5px;
	padding-left:10px;
}

.undefined_approver {
	color:red;
	font-weight:bold;
}

/* Detail pages css */
.edit_field {
	display:table-cell;
	padding-bottom:7px;
}
.edit_field .label {
	padding-top:4px;
}
.edit_field_vertical {
	float:left;
	padding-right:2px;
}
.edit_field_vertical .label {
	/*padding-right:2px;*/
}
.edit_field_vertical .input {
	clear:left;
}
.field {
	padding-top:2px;
	padding-bottom:4px;
}
.add_option, .edit_option {
	float:left;
	width:18px;
	height:20px;
}
/* End Detail pages css */


/* Management Pages */
.filter_program_name select { width:180px !important; }
.filter_program_type select { width:80px !important; }
.filter_program_sub_type select { width:135px !important; }
.filter_program_risk select { width:90px !important; }
.filter_customer select { width:130px !important; }
.filter_platform select { width:100px !important; }
.filter_facility select { width:230px !important; }
.filter_press select { width:100px !important; }
.filter_member select {	width:150px !important; }
.filter_program select { width:200px !important; }
.filter_issue_status select { width:80px !important; }
.filter_issue_health select { width:120px !important; }
.filter_regular_gate_issues .label { width:135px !important; }
.filter_tool_category select { width:110px !important; }
.filter_supplier select { width:180px !important; }
.filter_tool_type select { width:110px !important; }
.filter_person_responsible select, .filter_person_responsible .label { width:180px !important; }
.filter_design_responsible select {	width:80px; }
.filter_container_name input { width:120px; }
.management_buttons {
	float:left;
	white-space:nowrap;
}
.filter_button {
	height:38px;
}
/* End Management Pages */


/* Milestone/Issue status classes */
.unspecified, .upcoming, .warning, .ontime, .complete {
	color:black;
}
.unspecified {
	background-color:;
}
.overdue {
	background-color:#e33939 !important;
	color:white;
}
.upcoming {
	background-color:#ecc94e !important;
}
.warning {

	background-color:#ff9500 !important;
}
.ontime {
	/*background-color:#40e54f !important;*/
	background-color:;
}
.complete, .na {
	background-color:#CFEEFE !important;
	/*background-color:#F0F0F0 !important;*/
}
.na {
	color:#A0A0A0;
}
/* Milestone status classes end */


.program_awarded {
	color:blue;
}
.program_not_awarded {
	color:red;
}

.expansion {
	width:20px !important;
}

.action { width:20px; }
.small_action { width:9px; }

/* Legend */
.legend {
	padding-right:30px;
	float:left;
}
.legend .title {
	font-weight:bold;
	text-decoration:underline;
	padding-bottom:3px;
	font-size:13px;
}
.legend_item {
	padding:3px;
}
/* End Legend */

.email_link .email_icon {

	display:none;
}

#save_menu {
	min-width:1000px;
}
#save_menu .button {
	float:left;
}
.editing_for_ec {
	color:red;
	float:left;
	padding-top:5px;
	padding-left:10px;
	font-weight:bold;
	font-size:14px;
}

.submitted_comment {
	font-size:12px !important;
	height:18px !important;
	width:300px;
}

/* EC */
.ec_change {
	background-color:#FFF380 !important;
}
.ec_deletion {
	text-decoration:line-through;
}
/* End EC */