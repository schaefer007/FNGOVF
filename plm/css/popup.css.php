.popup {
	position:absolute;
	top:20px;
	left:20px;
	width:500px;
	height:300px;
	z-index:3;
	background-color:white;
	border:3px solid #555;
	padding:20px !important;
	background-image:url('images/bg.png');
	overflow:auto;

    border-radius:8px;
    -webkit-border-radius:8px;
    -moz-border-radius:8px;
    box-shadow:7px 7px 7px rgba(0, 0, 0, 0.26);
    -moz-box-shadow:7px 7px 7px rgba(0, 0, 0, 0.26);
    -webkit-box-shadow:7px 7px 7px rgba(0, 0, 0, 0.26);
}

#shadow {
	position:absolute;
	background-color:black;
	z-index:2;
	-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=30)";
	filter: alpha(opacity=30);
	-moz-opacity:0.3;
	-khtml-opacity: 0.3;
	opacity: 0.3;
}

.edit_mode {
	display:none;
}