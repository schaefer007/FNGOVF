.process_path {
	float:left;
	padding-right:20px;
}

.table_process_states .process_state_name { width:240px; }
.table_process_states .description { width:600px; }

.table_process_transitions .transition_name { width:200px; }
.table_process_transitions .transition_button { width:140px; }
.table_process_transitions .process_state { width:200px; }
.table_process_transitions .condition { width:250px; }
.table_process_transitions .process_transition .condition_text { overflow:hidden; }

.table_conditions .condition_text { width:450px; }