<?php
include_once("common.php");
include_once("classes/ProcessState.class.php");
include_once("classes/PopupReturn.class.php");

include_once("authenticate.php");

// Load original data
$arrProcessState = $_POST["arrProcessState"];
$intProcessStateID = isset($arrProcessState["intProcessStateID"])?$arrProcessState["intProcessStateID"]:null;
$objProcessState = new ProcessState($intProcessStateID);
$objProcessState->setVarsFromRow($arrProcessState);
$objProcessState->setWFProcessID($_POST["intWFProcessID"]);

// Validate data
$arrErrors = $objProcessState->validate();
if($arrErrors) {
	$objPopupReturn = new PopupReturn();
	$objPopupReturn->setStatus("Failure");
	$objPopupReturn->setError(implode("\n", $arrErrors));
	echo $objPopupReturn->toJSON();
	exit();
}

$objProcessState->save();

// Return JSON
include("smarty.php");

$g_objSmarty->assign("objProcessState", $objProcessState);
$g_objSmarty->assign("blnEditable", 1);

$objPopupReturn = new PopupReturn();
$objPopupReturn->setStatus("Success");
if($arrProcessState["intProcessStateID"]) { // Editing existing tool
	$arrChanges[0]["strSelector"] = "#process_state_".$arrProcessState["intProcessStateID"];
	$arrChanges[0]["strChangeType"] = "Modification";
} else { // Addition
	$arrChanges[0]["strSelector"] = "#process_states";
	$arrChanges[0]["strChangeType"] = "Addition";
}
$arrChanges[0]["txtHTML"] = $g_objSmarty->fetch("process_state_line.tpl");
$objPopupReturn->setChanges($arrChanges);

echo $objPopupReturn->toJSON();
?>

