<?php
	$pathArray = explode('/',__FILE__);
	foreach ($pathArray as $pathElement) {
		if (strtolower($pathElement) != 'plm') {
			$filePath .= $pathElement.'/';
		} else {
			break;
		}
	}
	set_include_path(get_include_path() . PATH_SEPARATOR . $filePath);
//    set_include_path(get_include_path() . PATH_SEPARATOR . '/www/zendsvr6/htdocs/Development/FNGForms/');
	include_once("constants.php");
	include_once("classes/User.class.php");
	include_once("classes/Search.class.php");
	include_once("classes/Security.class.php");
	include_once("classes/EngineeringChange.class.php");
	include_once("classes/class.mail.php");
	include_once("includes/c_config.php");
	if (session_status() == PHP_SESSION_NONE) {
	  session_start();
	}
    //error_log('include path is: '.get_include_path());
	// Set up global Search object
	if(!isset($_SESSION["objSearch"])) $_SESSION["objSearch"] = new Search();
	$objSearch = $_SESSION["objSearch"];

	ini_set('arg_separator.output','&amp;');

	include_once("common_allsites.php");

	function sendAlertNotification($strEmail, $strSubject, $txtBody) {
		$strSubject = strEMAIL_SUBJECT_PREFIX . ": $strSubject";
		$txtBody = "<html>"
			. "<head><style text=\"text/css\">".file_get_contents("css/email.css.php")."</style></head>"
			. "<body>" . $txtBody . "</body>"
			. "</html>";
		$txtHeaders  = "From: ".strEMAIL_FROM_NAME." <".strEMAIL_FROM_ADDRESS.">\r\n";
		$txtHeaders .= "Content-Type:text/html;";

		//echo "Sending email notification to: $strEmail<br />Subject: $strSubject<br />Body: $txtBody<br /><br />";
		if(blnSEND_ALERT_NOTIFICATIONS) {
			if($strEmail) {

				$mail = new email();
				$mail->setType('HTML');
				$mail->addMessage($txtBody);
				$mail->addSender(strEMAIL_FROM_ADDRESS);
				$mail->addRecipient($strEmail);
				$mail->addSubject($strSubject);
				if (!$mail->send()) {
					error_log('Mail send to '.$strEmail.' failed');
					base::write_log("Alert Fail: $strSubject","E");
				} else {
					base::write_log("Alert Success: $strSubject","S");
					error_log('Mail send to '.$strEmail.' Successful');
				}
				//	error_log('SMTP setting is '.ini_get('SMTP'));
				//	if (!mail($strEmail, $strSubject, $txtBody, $txtHeaders)) {
				//		error_log('Mail send with headers '.$txtHeaders.' Failed');
				//		error_log('Mail to is set to '.$strEmail);
				//	} else {
				//		error_log('Mail send with headers '.$txtHeaders.' Successful');
				//	}
			}
		} else {
			//echo $strEmail . "<br />" . $strSubject . "<br />" . $txtBody . "<br />";
		}
	}
	function sendEmailNotification($strEmail, $strSubject, $txtBody) {
		$strSubject = strEMAIL_SUBJECT_PREFIX . ": $strSubject";
		$txtBody = "<html>"
			. "<head><style text=\"text/css\">".file_get_contents("css/email.css.php")."</style></head>"
			. "<body>" . $txtBody . "</body>"
			. "</html>";
		$txtHeaders  = "From: ".strEMAIL_FROM_NAME." <".strEMAIL_FROM_ADDRESS.">\r\n";
		$txtHeaders .= "Content-Type:text/html;";

		//echo "Sending email notification to: $strEmail<br />Subject: $strSubject<br />Body: $txtBody<br /><br />";
		if(blnSEND_EMAIL_NOTIFICATIONS) {
			if($strEmail) {

				$mail = new email();
				$mail->setType('HTML');
				$mail->addMessage($txtBody);
				$mail->addSender(strEMAIL_FROM_ADDRESS);
				$mail->addRecipient($strEmail);
				$mail->addSubject($strSubject);
				if (!$mail->send()) {
					error_log('Mail send to '.$strEmail.' failed');
					base::write_log("Notification Fail: $strSubject","E");
				} else {
					error_log('Mail send to '.$strEmail.' Successful');
					base::write_log("Notification Success: $strSubject","S");
				}
			//	error_log('SMTP setting is '.ini_get('SMTP'));
			//	if (!mail($strEmail, $strSubject, $txtBody, $txtHeaders)) {
			//		error_log('Mail send with headers '.$txtHeaders.' Failed');
			//		error_log('Mail to is set to '.$strEmail);
			//	} else {
			//		error_log('Mail send with headers '.$txtHeaders.' Successful');
			//	}
			}
		} else {
			//echo $strEmail . "<br />" . $strSubject . "<br />" . $txtBody . "<br />";
		}
	}

	function getInitials($strName){
		$arrNames = explode(" ", $strName);
		$strInitials = "";
		if($arrNames) {
			foreach($arrNames as $strName2) {
				$strInitial = substr($strName2, 0, 1);
				if($strInitial==",") $strInitial .= " ";
				$strInitials .= $strInitial;
			}
		}
		return $strInitials;
	}

	function uploadFile($strName, $strTempName, $strError, $strFolder, $objParentFolder, $strDerivedClass="Document", $intID=null, $intProgramCommonID=null, $intDocumentTypeID=null){
		include_once("classes/$strDerivedClass.class.php");
		include_once("classes/Folder.class.php");

		$strDocumentName = isset($strName)?$strName:null;
		if($strDocumentName && !$strError) {
			$objDocument = new $strDerivedClass();

			$objDocument->setFolderID(Folder::getChildFolderID($strFolder, $objParentFolder->getFolderID(), $intProgramCommonID));
			$objDocument->setFileName($strDocumentName);
			$objDocument->setDocumentName($strDocumentName);
			$objDocument->setDocumentTypeID($intDocumentTypeID);
			$objDocument->setProgramCommonID($intProgramCommonID);
			$objDocument->setCreatedByUserID($_SESSION["objUser"]->getUserID());
			$objDocument->setCreatedOn(now());
			if($intID)
				$objDocument->setID($intID);
			$objDocument->save();
			$objDocument->setFileName($objDocument->getDocumentID()."_".$strDocumentName);

			$strFrom = $strTempName;
			$strURL = strDOCUMENT_FOLDER . $objParentFolder->getFolder() ."/". $strFolder . $objDocument->getFileName();
			$strTo = strFULL_FOLDER_PATH . $strURL;
			$objDocument->save($strFrom, $strTo);

			return $objDocument;
		}

		switch($strError) {
			case 1:
			case 2:
				triggerWarning("The file \"$strDocumentName\" exceeds the maximum upload size.<br />");
		}

		return null;
	}

	function uploadContainerFile($strName, $strTempName, $strError, $objContainer, $strDerivedClass="Document", $intID=null){
		include_once("classes/$strDerivedClass.class.php");
		include_once("classes/Folder.class.php");

		$strDocumentName = isset($strName)?$strName:null;
		if($strDocumentName && !$strError) {
			$objDocument = new $strDerivedClass();
			//$objDocument->setFolderID(Folder::getChildFolderID($strFolder, $objParentFolder->getFolderID()));
			$objDocument->setFileName($strDocumentName);
			$objDocument->setDocumentName($strDocumentName);
			$objDocument->setCreatedByUserID($_SESSION["objUser"]->getUserID());
			$objDocument->setCreatedOn(now());
			if($intID)
				$objDocument->setID($intID);
			$objDocument->save();
			$objDocument->setFileName($objDocument->getDocumentID()."_".$strDocumentName);

			$strFrom = $strTempName;
			$strTo = strFULL_FOLDER_PATH . strDOCUMENT_FOLDER . strCONTAINER_FOLDER . $objContainer->getFolderName() ."/". $objDocument->getFileName();
			$objDocument->save($strFrom, $strTo);

			return $objDocument;
		}

		switch($strError) {
			case 1:
			case 2:
				triggerWarning("The file \"$strDocumentName\" exceeds the maximum upload size.<br />");
		}

		return null;
	}

	function isGateThatDesignFreezes($intMilestoneID) {
		global $arrGatesThatDesignFreeze;
		return in_array($intMilestoneID, $arrGatesThatDesignFreeze);
	}

function GetDateFromResult($result,$format="Y-m-d-H.i.s") {
	if (strlen($result) > 19) {
		$date = substr($result,0,-7);
	} else {
		$date = $result;
		$format = 'Y-m-d H:i:s';
	}
	$date = date_create_from_format($format, $date);
	return $date;
}

function GetDifferenceInMinutes($oldtime, $newtime) {
	$since_start = $oldtime->diff($newtime);
	$minutes = $since_start->days * (24 * 60 * 60);
	$minutes += $since_start->h * 60;
	$minutes += $since_start->i;
	return $minutes;
}
?>
