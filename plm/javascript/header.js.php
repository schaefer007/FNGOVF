$(document).ready(function() {
	$('div[submenu]').hover(function() {
		$('div[menu="true"]').hide();
		$('div[submenu]').css('background-color','');

		if($(this).attr('submenu') == '')
			return;
		var jqySubMenu = $('#'+$(this).attr('submenu'));
		jqySubMenu.css('position','absolute');
		var arrMenuOffset = $(this).offset();
		var arrSubMenuOffset = jqySubMenu.offset();
		jqySubMenu.css('position','fixed');
		jqySubMenu.css('left', arrMenuOffset.left);
		jqySubMenu.css('top', $('#header').outerHeight()-1);
		jqySubMenu.show();
		$(this).css('background-color','#66BBEE');
	}, function() {
		$('div[submenu]').css('background-color','');

		if($(this).attr('submenu') == '')
			return;
		var jqySubMenu = $('#'+$(this).attr('submenu'));
		jqySubMenu.hide();
	});

	$('div[menu]').hover(function() {
		$(this).show();
	}, function() {
		$('div[submenu]').css('background-color','');
		$(this).hide();
	});
});

var intContainerEditWindowWidth = 955;
var intContainerEditWindowHeight = 310;
function loadContainer(intContainerID) {
	var objPopupOpenData = {
		intContainerID:intContainerID
	};
	var objPopupOpenCallback = function() {
		loadCSS('container_edit.css.php');
	}
	var objPopupOpen = {
		strFile:'container_edit.php',
		objData:objPopupOpenData,
		fncCallback:objPopupOpenCallback
	};

	var objPopupOkData = {
	};
	var objPopupOk = {
		strFile:'container_edit_process.php',
		objData:objPopupOkData,
		blnRedirect:true
	};

	loadPopup(objPopupOpen, objPopupOk, intContainerEditWindowWidth, intContainerEditWindowHeight);
}