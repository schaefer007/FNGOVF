$(document).ready(function() {
	var jqyConditionTexts = $('.table_process_transitions .process_transition .condition_text');
	jqyConditionTexts.each(function() {
		var jqyConditionText = $(this);
		if(jqyConditionText.height() > 46) {
			jqyConditionText.css('max-height', 46);
			jqyConditionText.parent().prev().append($('<div><a href="javascript:showProcessTransitionRoles(\''+jqyConditionText.parent().parent().prop('id')+'\')">show all roles</a></div>'));
		}
	});
});

function showProcessTransitionRoles(strElementID) {
	$('#'+strElementID+' .condition_text').css('max-height','none');
}

var intWFProcessWindowWidth = 550;
var intWFProcessWindowHeight = 300;
function loadWorkflowProcess(strAction) {
	var objPopupOpenData = {};
	if(strAction == 'Edit') {
		objPopupOpenData['intWFProcessID'] = $('select[name="intWFProcessID"]').val();
	}
	var objPopupOpen = {
		strFile:'<?php echo strSITE_URL; ?>wf_process_edit.php',
		objData:objPopupOpenData
	};
	var objPopupOk = {
		strFile:'<?php echo strSITE_URL; ?>wf_process_edit_process.php',
		blnRedirect:true
	};
	loadPopup(objPopupOpen, objPopupOk, intWFProcessWindowWidth, intWFProcessWindowHeight);
}

var intProcessTransitionWindowWidth = 650;
var intProcessTransitionWindowHeight = 300;
function loadProcessTransition(intProcessTransitionID, intWFProcessID) {
	var objPopupOpenData = {
		intProcessTransitionID:intProcessTransitionID,
		intWFProcessID:intWFProcessID
	};
	var objPopupOpen = {
		strFile:'<?php echo strSITE_URL; ?>process_transition_edit.php',
		objData:objPopupOpenData
	};

	var objPopupOkData = {
		intWFProcessID:intWFProcessID
	};
	var fncCallback = function() {
		$('#no_process_transitions').remove();
	};
	var objPopupOk = {
		strFile:'<?php echo strSITE_URL; ?>process_transition_edit_process.php',
		objData:objPopupOkData,
		fncCallback:fncCallback
	};
	loadPopup(objPopupOpen, objPopupOk, intProcessTransitionWindowWidth, intProcessTransitionWindowHeight);
}
function deleteProcessTransition(intProcessTransitionID) {
	if(!confirm('Are you sure you want to delete this process transition?'))
		return;

	var objData = {
		strAction:'Delete Process Transition',
		intProcessTransitionID:intProcessTransitionID
	};
	var fncCallback = function() {
		$('#process_transition_'+intProcessTransitionID).remove();
	};
	$.post('<?php echo strSITE_URL; ?>wfprocess_processes.php', objData, fncCallback);
}
var intConditionIndex = 0;
function addConditionXR(intConditionLevel, intParentConditionID, intConditionXRID) {
	while($('select[name^="arrConditions['+intConditionIndex+']"]').length > 0) intConditionIndex++;

	var objData = {
		strAction:'Add Condition XR',
		intConditionLevel:intConditionLevel,
		intConditionIndex:intConditionIndex,
		intParentConditionID:intParentConditionID/*,
		intConditionXRID:intConditionXRID*/
	};
	var fncCallback = function(htmReturn) {
		$('#condition_xr_'+intConditionXRID).after($(htmReturn));
	};
	$.post('<?php echo strSITE_URL; ?>wfprocess_processes.php', objData, fncCallback);
	intConditionIndex++;
}
function removeConditionXR(intConditionXRID) {
	if(!confirm('Are you sure you want to delete this condition?\nThis condition will automatically be removed from the database if a) it is not used anywhere else and b) it is not a role condition. The same applies to any child conditions that are removed.'))
		return;

	var objData = {
		strAction:'Delete Condition XR',
		intConditionXRID:intConditionXRID
	};
	var fncCallback = function() {
		$('.condition_xr_'+intConditionXRID).remove();
	};
	$.post('<?php echo strSITE_URL; ?>wfprocess_processes.php', objData, fncCallback);
}


var intProcessStateWindowWidth = 630;
var intProcessStateWindowHeight = 300;
function loadProcessState(intProcessStateID) {
	var objPopupOpenData = {
		intProcessStateID:intProcessStateID
	};
	var objPopupOpen = {
		strFile:'<?php echo strSITE_URL; ?>process_state_edit.php',
		objData:objPopupOpenData
	};
	var objPopupOkData = {
		intWFProcessID:$('select[name="intWFProcessID"]').val()
	};
	var objPopupOk = {
		strFile:'<?php echo strSITE_URL; ?>process_state_edit_process.php',
		objData:objPopupOkData
	};
	loadPopup(objPopupOpen, objPopupOk, intProcessStateWindowWidth, intProcessStateWindowHeight);
}
function deleteProcessState(intProcessStateID) {
	if(!confirm('Are you sure you want to delete this process state?'))
		return;

	var objData = {
		strAction:'Delete Process State',
		intProcessStateID:intProcessStateID
	};
	var fncCallback = function() {
		$('#process_state_'+intProcessStateID).remove();
	};
	$.post('<?php echo strSITE_URL; ?>wfprocess_processes.php', objData, fncCallback);
}

function updateDiagram(intProcessPathID, intWFProcessID) {
	$('#state_diagram').prop('src', 'state_diagram.php?intProcessPathID='+intProcessPathID+'&intWFProcessID='+intWFProcessID);
}