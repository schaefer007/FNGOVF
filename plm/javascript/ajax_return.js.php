function applyChanges(arrChanges) {
	for(i in arrChanges) {
		var arrChange = arrChanges[i];
		if(arrChange.strChangeType == 'Addition') {
			$(arrChange.strSelector).append($(arrChange.txtHTML));
		} else if(arrChange.strChangeType == 'Modification') {
			$(arrChange.strSelector).outerHTML(arrChange.txtHTML);
		} else if(arrChange.strChangeType == 'Deletion') {
			$(arrChange.strSelector).remove(); // TODO CODE CLEAN: Should we hide, or remove? Perhaps we can have "Deletion" and "Hide"
		}
	}
}