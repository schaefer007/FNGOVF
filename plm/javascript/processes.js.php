var intProcessWindowWidth = 470;
var intProcessWindowHeight = 310;

function loadProcess(intProcessID) {
	var objPopupOpenData = {
		intProgramID:$('input[name="intProgramID"]').val()
	};
	if(intProcessID != undefined) objPopupOpenData['intProcessID'] = intProcessID;
	if($('select[name="intProcessTypeID"]').val() != undefined) objPopupOpenData['intProcessTypeID'] = $('select[name="intProcessTypeID"]').val();
	var objPopupOpen = {
		strFile:'process_edit.php',
		objData:objPopupOpenData
	};

	var objPopupOkData = {
		intProgramID:$('input[name="intProgramID"]').val(),
		blnEditMode:1
	};
	var objPopupOk = {
		strFile:'process_edit_process.php',
		objData:objPopupOkData,
		fncCallback:addProcessLine
	};

	loadPopup(objPopupOpen, objPopupOk, intProcessWindowWidth, intProcessWindowHeight);
}

function addProcessLine(objReturn) {
	$('#no_processes').remove();
}

var intProcessProductIndex = 0;
function addProcessProduct() {
	while($('input[name^="arrProducts['+intProcessProductIndex+']"]').length > 0) { intProcessProductIndex++; } // Make sure intProcessProductIndex isn't in use

	var objData = {
		intProgramID:$('input[name="intProgramID"]').val(),
		intProcessProductIndex:intProcessProductIndex
	};
	var fncCallback = function(txtReturn) {
		$('#process_products').append(txtReturn);
	};
	$.post('process_product_edit.php', objData, fncCallback);
	intProcessProductIndex++;
}

function deleteProcessProduct(intProcessProductIndex, intProcessProductID) {
	if($('input[name="arrProducts['+intProcessProductIndex+'][intProcessProductID]"]').length > 0) {
		var objData = {
			strAction:'Delete Process Product',
			intProcessProductID:intProcessProductID
		};
		$.post('process_processes.php', objData);
	}

	if($('#process_products select').length > 1) {
		$('#process_product_'+intProcessProductIndex).remove();
	} else {
		$('#process_product_'+intProcessProductIndex+' select').val('');
	}
}
var intProcessToolIndex = 0;
function addProcessTool() {
	while($('input[name^="arrTools['+intProcessToolIndex+']"]').length > 0) { intProcessToolIndex++; } // Make sure intProcessToolIndex isn't in use

	var objData = {
		intProgramID:$('input[name="intProgramID"]').val(),
		intProcessToolIndex:intProcessToolIndex
	};
	var fncCallback = function(txtReturn) {
		$('#process_tools').append(txtReturn);
	};
	$.post('process_tool_edit.php', objData, fncCallback);
	intProcessToolIndex++;
}

function deleteProcessTool(intProcessToolIndex, intProcessToolID) {
	if($('input[name="arrTools['+intProcessToolIndex+'][intProcessToolID]"]').length > 0) {
		var objData = {
			strAction:'Delete Process Tool',
			intProcessToolID:intProcessToolID
		};
		$.post('process_processes.php', objData);
	}

	if($('#process_tools select').length > 1) {
		$('#process_tool_'+intProcessToolIndex).remove();
	} else {
		$('#process_tool_'+intProcessToolIndex+' select').val('');
	}
}

function deleteProcess(intProcessID) {
	if(!confirm('Are you sure you want to delete this process?'))
		return;

	var objData = {
		strAction:'Delete Process',
		intProcessID:intProcessID
	};
	$.post('process_processes.php', objData);

	$('#process_'+intProcessID).remove();
}
