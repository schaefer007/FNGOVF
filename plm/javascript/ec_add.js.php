$(document).ready(function() {
	$('input[name="blnBankBuildRequired"]').click(function() { toggleDisabled($(this)); });
	toggleDisabled($('input[name="blnBankBuildRequired"]'));

	cancelEnter();
});

function toggleDisabled(objThis){
	if(objThis.is(':checked')) {
		$('input[name="strTimeRequired"]').removeAttr('disabled');
	} else {
		$('input[name="strTimeRequired"]').attr('disabled',true);
	}
}