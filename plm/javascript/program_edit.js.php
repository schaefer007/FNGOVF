$(document).ready(function() {
	if($('#no_members').length != 0) { // TODO: Make sure so it check if the role is already defined. If it isn't, then add it, if it is, don't add it
		<?php
			foreach($arrDefaultRoles as $intRoleID) {
				$strUser = "";
				//if($intRoleID == 21) // TODO: Temporary, remove this Paula hardcoding
				//	$strUser = ", 24";
				echo "addMember('".$intRoleID."' $strUser);";
			}
		?>
	}
});

var intMemberIndex = 0;
function addMember(intRoleID, intUserID) {
	$('#no_members').hide();

	var objData = {
		strMemberIndex:'new_'+intMemberIndex,
		intUserID:intUserID==undefined?false:intUserID
	};
	if(intRoleID && intRoleID != undefined) {
		objData['arrRoleIDs[]'] = intRoleID
		objData['strChanged'] = 'Role';
	}
	var fncCallback = function(htmReturn) {
		$('#members').append($(htmReturn));
	};

	$.post('member_role_edit.php', objData, fncCallback);
	intMemberIndex++;
}

function deleteMember(intMemberID) {
	$.post('member_processes.php', {strAction:'Delete',intMemberID:intMemberID});
	$('input[name="arrMembers['+intMemberID+'][blnDeleted]"]').val(1);
	$('#member_'+intMemberID).remove();
}

var arrMemberRoleIndex = new Array();
function addRole(strMemberIndex) {
	var intUserID = $('select[name="arrMembers['+strMemberIndex+'][intUserID]"]').val();
	arrMemberRoleIndex[strMemberIndex] = arrMemberRoleIndex[strMemberIndex]?arrMemberRoleIndex[strMemberIndex]:1;
	var objData = {
		strMemberIndex:strMemberIndex,
		intIndex:'new_'+arrMemberRoleIndex[strMemberIndex],
		intUserID:intUserID
	};

	$.post('role_select.php', objData, function(strHTML) {
		$('#member_roles_'+strMemberIndex).append($(strHTML));
	});
	arrMemberRoleIndex[strMemberIndex]++;
}

function updateMemberRole(strChanged, strMemberIndex) {
	var intUserID = $('select[name="arrMembers['+strMemberIndex+'][intUserID]"]').val();
	var objData = {
		strMemberIndex:strMemberIndex,
		strChanged:strChanged,
		intUserID:intUserID
	};
	$('select[name^="arrMembers['+strMemberIndex+'][arrMemberRoles]"]').each(function(index, element) {
		objData['arrRoleIDs['+index+']'] = this.value;
	});
	var fncCallback = function(htmReturn) {
		$('#member_'+strMemberIndex).outerHTML($(htmReturn));
	};

	$.post('member_role_edit.php', objData, fncCallback);
}

function addPlatform() {
	$('#intPlatformID').toggle();
	$('#strPlatform').toggle();
	$('#strPlatform').focus();
	$('#blnNewPlatform').val($('#blnNewPlatform').val()=="1"?"0":"1");
}

function addCustomer() {
	$('#intCustomerID').toggle();
	$('#strCustomer').toggle();
	$('#strCustomer').focus();
	$('#blnNewCustomer').val($('#blnNewCustomer').val()=="1"?"0":"1");

	if($('#blnNewPlatform').val() != '1')
		addPlatform();
}

function toggleUseParent(intProgramMilestoneIndex) {
	if($('input[name="arrProgramMilestones['+intProgramMilestoneIndex+'][blnUseParent]"]:checked').length > 0) {
		$('input[name="arrProgramMilestones['+intProgramMilestoneIndex+'][dtmDueDate]"]').attr('disabled',true);
		$('input[name="arrProgramMilestones['+intProgramMilestoneIndex+'][dtmScheduledDate]"]').attr('disabled',true);
		$('input[name="arrProgramMilestones['+intProgramMilestoneIndex+'][dtmCompletedDate]"]').attr('disabled',true);
	} else {
		$('input[name="arrProgramMilestones['+intProgramMilestoneIndex+'][dtmDueDate]"]').attr('disabled',false);
		$('input[name="arrProgramMilestones['+intProgramMilestoneIndex+'][dtmScheduledDate]"]').attr('disabled',false);
		$('input[name="arrProgramMilestones['+intProgramMilestoneIndex+'][dtmCompletedDate]"]').attr('disabled',false);
	}
}