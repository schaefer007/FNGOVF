function submitEC() {
	var objData = {
		strAction:'Submit'
	};
	var fncCallback = function(htmReturn) {
		window.location.reload();
		//$('#ecs').outerHTML(htmReturn);
	};
	$.post('ec_processes.php', objData, fncCallback);
}

function editEC(intEngineeringChangeID, intProgramID){
	var objData = {
		strAction:'Change Editing',
		intEngineeringChangeID:intEngineeringChangeID,
		intProgramID:intProgramID
	}
	var fncCallback = function(htmReturn) {
		window.location.href = 'program.php?intProgramID='+intProgramID;
		//window.location.reload();
	};
	$.post('ec_processes.php', objData, fncCallback);
}

function stopEditing(){
	var objData = {
		strAction:'Done Editing'
	}
	var fncCallback = function() {
		window.location.reload();
	};
	$.post('ec_processes.php', objData, fncCallback);
}

function showECAffectedProducts(intEngineeringChangeID) {
	var jqyAffectedProducts = $('#affected_products_'+intEngineeringChangeID);
	if(jqyAffectedProducts.is(':visible')) {
		jqyAffectedProducts.hide();
	} else if(jqyAffectedProducts.html().length > 0) {
		jqyAffectedProducts.show();
	} else {
		var objData = {
			strAction:'Get Affected Products',
			intEngineeringChangeID:intEngineeringChangeID
		};
		jqyAffectedProducts.load('ec_processes.php', objData).show();
	}
}