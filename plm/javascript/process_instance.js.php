function workflowAction(intProcessInstanceID, intProcessTransitionID, intRoleID, intUserID, strProcessObjectClass, intID, txtComment, fncUserCallback) {
	if(txtComment == undefined)
		txtComment = '';
	if(!txtComment && $('#comment_required_'+intProcessTransitionID).val() == "1") {
		var strTransitionButton = $('input[name="arrConditionInstances['+intProcessTransitionID+']['+intRoleID+']['+intUserID+'][strTransitionButton]"]').val();
		var strTransitionText = '';
		if(strTransitionButton)
			strTransitionText = ' in order to ' + strTransitionButton;
		alert('You must enter a comment' + strTransitionText + '.');
		return;
	}
	$('input[name="arrConditionInstances['+intProcessTransitionID+']['+intRoleID+']['+intUserID+'][strTransitionButton]"]').prop('disabled',true);

	var objData = {
		strAction:'Fulfill Requirement',
		intProcessInstanceID:intProcessInstanceID,
		intProcessTransitionID:intProcessTransitionID,
		intRoleID:intRoleID,
		txtComment:txtComment,
		strProcessObjectClass:strProcessObjectClass,
		intID:intID
	};
	var fncCallback = function(objAjaxReturn) {
		if(objAjaxReturn) {
			if(objAjaxReturn.strStatus == 'Failure') {
			    if(objAjaxReturn.arrColumns && objAjaxReturn.arrColumns.length > 0) {
			        flagColumns(objAjaxReturn.arrColumns);
			    }
				if(objAjaxReturn.strError != undefined) {
					alert(objAjaxReturn.strError);
					$('input[name="arrConditionInstances['+intProcessTransitionID+']['+intRoleID+']['+intUserID+'][strTransitionButton]"]').prop('disabled',false);
				}
			} else if(objAjaxReturn.strStatus == 'Success') {
				if(objAjaxReturn.arrChanges && objAjaxReturn.arrChanges.length > 0)
					applyChanges(objAjaxReturn.arrChanges);
				if(typeof fncUserCallback == 'function') fncUserCallback();
				if(typeof fncWorkflowCallback == 'function') fncWorkflowCallback();
			}
		}
		cancelEnter();
	};
	$.post('<?php echo strSITE_URL; ?>process_instance_processes.php', objData, fncCallback, 'json');
}
function flagColumns(errColumns) {
    for (var i=0;i<errColumns.length;i++) {
      $("#row_" + errColumns[i]).addClass("fieldError");
    }
}
function processDefinition(intWFProcessID) {
	var objPopupOpenData = {
		intWFProcessID:intWFProcessID
	};
	var objPopupOpen = {
		strFile:'<?php echo strSITE_URL; ?>workflow_definition.php',
		objData:objPopupOpenData,
		fncCallback:setHelpHover
	};

	var objPopupOkData = {
	};
	var objPopupOk = {
		objData:objPopupOkData
	};

	loadPopup(objPopupOpen, objPopupOk, 950, 500);
}