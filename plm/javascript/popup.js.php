var objPopupOk2 = new Array();
function loadPopup(objPopupOpen, objPopupOk, intWidth, intHeight) {
	// Popup
	var jqyPopup = $('#popup');
	if(jqyPopup.length <= 0) {
		jqyPopup = $('<div id="popup" class="popup">');
		$('body').append(jqyPopup);
	} else {
		jqyPopup.show();
	}

	// Shadow
	var jqyShadow = $('#shadow');
	if(jqyShadow.length <= 0) {
		jqyShadow = $('<div id="shadow">');
		$('body').append(jqyShadow);
	} else {
		jqyShadow.show();
	}
	setupShadow();

	objPopupOk2 = objPopupOk;

	var strFile = objPopupOpen['strFile'];
	var objData = objPopupOpen['objData']?objPopupOpen['objData']:{};
	var fncCallback = objPopupOpen['fncCallback']?objPopupOpen['fncCallback']:function(){};
	jqyPopup.load(strFile, objData, fncCallback);

	setupPopupDimensions(intWidth, intHeight);
	setupPopupPosition();
}

function setupPopupDimensions(intWidth, intHeight) {
	var jqyPopup = $('#popup');
	if(isNaN(intWidth)) {
		intWidth = $(window).width() - (intPopupPadding * 2) - intScrollWidth;


	}
	if(isNaN(intHeight)) {
		intHeight = $(window).height() - intHeaderHeight - intFooterHeight - (intPopupPadding * 2) - intScrollWidth;
	}
	if(intWidth > $(window).width() - 50) {
		//intWidth = $(window).width() - 50;
	}
	if(intHeight > $(window).height() - 50) {
		//intHeight = $(window).height() - 50;
	}
	jqyPopup.css('width',intWidth);
	jqyPopup.css('height',intHeight);
}
function setupPopupPosition() {
	var jqyPopup = $('.popup');
	var intTop = ($(window).height() - jqyPopup.outerHeight()) / 2;
	var intLeft = ($(window).width() - jqyPopup.outerWidth()) / 2;
	jqyPopup.css('top',$(window).scrollTop() + intTop);
	jqyPopup.css('left',$(window).scrollLeft() + intLeft);
}
function setupShadow() {
	// Position
	var jqyShadow = $('#shadow');
	jqyShadow.css('top',$(window).scrollTop()-300);
	jqyShadow.css('left',$(window).scrollLeft()-300);

	// Dimensions
	jqyShadow.css('height',$(window).height()+300);
	jqyShadow.css('width',$(window).width()+300);
}

function popupOk() {
	var objData = $('#frmPopup').getFormValues();
	var strFile = objPopupOk2['strFile'];
	if(objPopupOk2['objData']) {
		for(strKey in objPopupOk2['objData']) {
			objData[strKey] = objPopupOk2['objData'][strKey];
		}
	}
	var fncCallback = objPopupOk2['fncCallback']?objPopupOk2['fncCallback']:function(){};
	var blnRedirect = objPopupOk2['blnRedirect']?objPopupOk2['blnRedirect']:null;
	var fncPopupCallback = function(objPopupReturn) {
		if(objPopupReturn.strStatus == 'Success') {
			if(objPopupReturn.arrChanges && objPopupReturn.arrChanges.length > 0)
				applyChanges(objPopupReturn.arrChanges);
			fncCallback(objPopupReturn.arrData);
			removePopup();

			if(blnRedirect)
				window.location.href = objPopupReturn.strRedirect;
		} else {
			if(objPopupReturn.strError) {
				alert(objPopupReturn.strError);
			}
		}
	}
	if(strFile) {
		$.post(strFile, objData, fncPopupCallback, 'json');
	}
}

// An alternate to the AJAX popupOk(), used primarily when uploading files with popup form.
function popupOkSubmit() {
	var objData = $('#frmPopup').getFormValues();
	objData['blnValidate'] = true;
	var strFile = objPopupOk2['strFile'];

	var fncPopupCallback = function(objPopupReturn) {
		if(objPopupReturn.strStatus == 'Failure' && objPopupReturn.strError) {
			alert(objPopupReturn.strError);
		} else if(objPopupReturn.strStatus == 'Success') {
			$('#frmPopup').attr('action', strFile).submit();
		}
	}
	if(strFile) {
		$.post(strFile, objData, fncPopupCallback, 'json');
	}
}

function applyChanges(arrChanges) {
	for(i in arrChanges) {
		var arrChange = arrChanges[i];
		if(arrChange.strChangeType == 'Addition') {
			$(arrChange.strSelector).append($(arrChange.txtHTML));
		} else if(arrChange.strChangeType == 'Modification') {
			$(arrChange.strSelector).outerHTML(arrChange.txtHTML);
		} else if(arrChange.strChangeType == 'Deletion') {
			$(arrChange.strSelector).remove(); // TODO CODE CLEAN: Should we hide, or remove? Perhaps we can have "Deletion" and "Hide"
		}
	}
}

function popupCancel(){
	removePopup();
}

function removePopup(){
	$('#popup').remove();
	$('#shadow').remove();
}

var intHeaderHeight = 0;
var intFooterHeight = 0;
var intPopupPadding = 0;
var intScrollWidth = 0;
$(document).ready(function() {
	intHeaderHeight = $('#header').outerHeight();
	intFooterHeight = $('#save_menu').outerHeight();
	intPopupPadding = 15;
	intScrollWidth = 40;

	$(window).scroll(function() {
		setupPopupPosition();
		setupShadow();
	});
	$(window).resize(function() {
		setupPopupPosition();
		setupShadow();
	});
});