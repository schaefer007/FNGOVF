$(document).ready(function() {
	$('#btnCopyMilestones').click(function() {
		var intProgramID = $('input[name="intProgramID"]').val();
		var intCopyProgramID = $('#intCopyProgramID').val();

		if(intProgramID && intCopyProgramID) {
			var objData = {
				intProgramID:intProgramID,
				intCopyProgramID:intCopyProgramID
			}
			var fncCallback = function() {
				$('input').keypress(function() { return noenter(); });
			}
			$('#milestones').load('programmilestones_copy.php', objData, fncCallback);
		}
	});
});