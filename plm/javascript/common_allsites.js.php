$(document).ready(function() {
	setHelpHover();
	setDatePicker();
});

function fncWorkflowCallback() {
	setHelpHover();
}

function setHelpHover() {
	// Help Hover
	var blnHelpHover = false;
	$('*[help]').hover(function() {
		// Create help div
		var jqyHelp = $('<div id="help"></div>').append($(this).attr('help'));
		jqyHelp.css('top',$(document).scrollTop()+20);
		jqyHelp.css('left',$(document).scrollLeft()+20);

		jqyHelp.hover(function() {}, function() { $('#help').remove(); });

		$('body').append(jqyHelp);
	},
	function(e) {
		var offset = $('#help').offset();
        var leftEdge = offset.left;
        var topEdge = offset.top;
        var rightEdge = leftEdge + $('#help').width();
        var bottomEdge = topEdge + $('#help').height();
        if (e.pageX > rightEdge || e.pageX < leftEdge || e.pageY > bottomEdge || e.pageY < topEdge) {
        	$('#help').remove();
		}
	});
}

function setDatePicker() {
	$('input[class~="date"]').datepicker({
		changeMonth: true,
		changeYear: true,
		showOtherMonths: true,
		selectOtherMonths: true
	});
}

jQuery.fn.outerHTML = function(s) {
	return (s)
	? this.before(s).remove()
	: jQuery("&lt;p&gt;").append(this.eq(0).clone()).html();
}

function addGetParam(strURL, strName, strValue) {
	var strQuestion = '?'+strName+'=';
	var strAmpersand = '&'+strName+'=';
	var intQuestionPosition = strURL.indexOf(strQuestion);
	var intAmpersandPosition = strURL.indexOf(strAmpersand);
	if(intAmpersandPosition == -1 && intQuestionPosition == -1) {
		if(strURL.indexOf('?') == -1)
			strURL += '?';
		strURL += '&'+strName+'='+strValue;
	} else if(intAmpersandPosition != -1) {
		strURL = _replaceGetParam(strURL, strValue, intAmpersandPosition, strAmpersand);
	} else if(intQuestionPosition != -1) {
		strURL = _replaceGetParam(strURL, strValue, intQuestionPosition, strQuestion);
	}
	return strURL;
}

function _replaceGetParam(strURL, strValue, intPosition, strSearchFor) {
	var intValueStart = intPosition + strSearchFor.length;
	var strAfterName = strURL.substring(intValueStart);
	var intValueEnd = strAfterName.indexOf('&amp;');
	if(intValueEnd == -1) {
		intValueEnd = strAfterName.length;
	}
	strURL = strURL.substring(0, intValueStart) + strValue + strAfterName.substring(intValueEnd);
	return strURL;
}

function reloadWithGetParam(strName, strValue) {
	window.location = addGetParam(window.location.toString(), strName, strValue);
}

function clearDropDown(objDropDown) {
	if(objDropDown.options)
		objDropDown.options.length = 0;
}

function count(mixObjectOrArray) {
	var intProperties = 0;
	if(mixObjectOrArray instanceof Object) {
		for(i in mixObjectOrArray) {
			intProperties++;
		}
	} else if (mixObjectOrArray instanceof Array) {
		intProperties = mixObjectOrArray.length;
	}
	return intProperties;
}

function populateDD(strFromID, strToID, objParams) {
	var fncOnChange = function() {
		var objData = {'intID':$('#'+strFromID).val(), 'strFromID':strFromID, 'strToID':strToID};
		if(objParams && objParams['data']) {
			for(key in objParams['data']) {
				objData[key] = objParams['data'][key];
			}
		}
		$.ajax({
			url: 'get_options.php',
			dataType: 'json',
			data: objData,
			success: function(arrOptions) {
				var objOptions = $('#'+strToID)[0];
				if(objOptions != undefined) objOptions.options.length = 0;
				if(objParams != undefined && objParams['blank']) {
					$('#'+strToID).append($('<option></option>'));
				}
				$.each(arrOptions, function(key, value) {
				     $('#'+strToID).
				          append($("<option></option>").
				          attr("value",key).
				          text(value));
				});
				if(objParams != undefined && objParams['sort_by_value']) {
					$('#'+strToID).html($('#'+strToID+' option').sort(function (a, b) {
					    return a.value == b.value ? 0 : a.value < b.value ? -1 : 1
					}));
				} else {
					$('#'+strToID).html($('#'+strToID+' option').sort(function (a, b) {
					    return a.text == b.text ? 0 : a.text < b.text ? -1 : 1
					}));
				}

				var fncCallback = (objParams != undefined && objParams['callback'])?objParams['callback']:function(){};
				fncCallback();
			}
		});
	}
	$('#'+strFromID).change(fncOnChange);
	fncOnChange();
}

function triggerError(strError) {
	alert(strError);
}

// CODECLEAN, this function name is terrible given how the function has evolved
function getIndexFromName(strName, intIndexNumber, blnParse) {
	if(blnParse == undefined)
		blnParse = true;

	var intReturn = null;
	var strRemainingName = strName;

	if(!intIndexNumber) {
		intIndexNumber = 1;
	}
	for(var i=intIndexNumber-1;i>=0;i--) {
		var intOpeningBracket = strRemainingName.indexOf("[");
		var intClosingBracket = strRemainingName.indexOf("]");
		var strReturn = strRemainingName.substring(intOpeningBracket+1, intClosingBracket);
		if(blnParse) strReturn = parseInt(strReturn, 10);
		strRemainingName = strName.substring(intClosingBracket+1);
	}

	return strReturn;
}

// Jquery regular expression function (from http://james.padolsey.com/javascript/regex-selector-for-jquery/)
jQuery.expr[':'].regex = function(elem, index, match) {
    var matchParams = match[3].split(','),
        validLabels = /^(data|css):/,
        attr = {
            method: matchParams[0].match(validLabels) ?
                        matchParams[0].split(':')[0] : 'attr',
            property: matchParams.shift().replace(validLabels,'')
        },
        regexFlags = 'ig',
        regex = new RegExp(matchParams.join('').replace(/^\s+|\s+$/g,''), regexFlags);
    return regex.test(jQuery(elem)[attr.method](attr.property));
}

function cancelEnter(){
	$('input').keypress(function() { return noenter(); });
}
function noenter() {
	return !(window.event && window.event.keyCode == 13);
}

function enlargePhoto(intDocumentID) {
	closeEnlargedPhoto();

	var objData = {
		intDocumentID:intDocumentID
	}
	var fncCallback = function() {
		$('#enlarged_photo .photo img')
			.css('max-width',$(window).width() - 42)
			.css('max-height',$(window).height() - 75 - 125);
	}

	var jqyEnlargedPhoto = $('<div id="enlarged_photo"></div>')
		.css('top',$(window).scrollTop()+10+110)
		.css('left',$(window).scrollLeft()+10);
	$('body').append(jqyEnlargedPhoto);
	$('#enlarged_photo').load('photo.php',objData, fncCallback);
}
function closeEnlargedPhoto() {
	$('#enlarged_photo').remove();
}

function toggle(strElementID) {
	var jqyElement = $('#'+strElementID);
	if(jqyElement.is(':visible'))
		jqyElement.hide();
	else
		jqyElement.show();
}
function toggleClass(strElementClass) {
	$('.'+strElementClass).toggle();
}
function slideToggleClass(strElementClass) {
	$('.'+strElementClass).slideToggle();
}
function showClass(strElementClass) {
	$('.'+strElementClass).show();
}
function hideClass(strElementClass) {
	$('.'+strElementClass).hide();
}

function toggleExtraFields(strElementIDPrefix) {
	//$('.extra_fields').toggle();
}

function addNewRecord(strID, strName, strNew) {
	$('#'+strID).toggle();

	$('#'+strName).toggle();

	$('#'+strName+' input').focus();
	$('#'+strNew).val($('#'+strNew).val()=="1"?"0":"1");

}

function print_r(arr,level) {
	var dumped_text = "";
	if(!level) level = 0;

	//The padding given at the beginning of the line.
	var level_padding = "";
	for(var j=0;j<level+1;j++) level_padding += "    ";

	if(typeof(arr) == 'object') { //Array/Hashes/Objects
		for(var item in arr) {
			var value = arr[item];

			if(typeof(value) == 'object') { //If it is an array,
				dumped_text += level_padding + "'" + item + "' ...\n";
				dumped_text += print_r(value,level+1);
			} else {
				dumped_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
			}
		}
	} else { //Stings/Chars/Numbers etc.
		dumped_text = "===>"+arr+"<===("+typeof(arr)+")";
	}
	return dumped_text;
}

/* Has PHP counterpart in common_allsites.php */
function budgetNumberFormat(strNumber) {
	if(!isNaN(strNumber)) {
		var strAbsNumber = Math.abs(strNumber);
		return (strNumber < 0?"-":"")+'$'+strAbsNumber;
	}
	return "";
}

/*
function getObjectListFromForm(strFormName) {
	arrObjectList = {};
	$('*[name^="'+strFormName+'"]').each(function() {
		var strName = $(this).attr('name');

		var intPosition = strName.indexOf(']');
		strName = strName.substring(intPosition+1);


		var intPosition = strName.indexOf(']');
		var strKey = strName.substring(1, intPosition);
		var strRemainder = strName.substring(intPosition+1);
		arrObjectList[strKey] = $(this).val();
		// Next Level
		//popKey(strName, $(this).val());

		/*var arrPop = popKey(strName);
		while(arrPop['strRemainder'] != '') {
			arrObjectList[arrPop['strKey']] = new Array();
		} else {
			arrObjectList[arrPop['strKey']] = $(this).val();
		}

		if(strRemainder != '') {
			var strRemainder = strName.substring(intPosition+1);
		}/
	});

	return arrObjectList;
}
*/

jQuery.fn.getFormValues = function(){
    var formvals = {};
    jQuery.each(jQuery(':input',this).serializeArray(),function(i,obj){
        if (formvals[obj.name] == undefined)
            formvals[obj.name] = obj.value;
        else if (typeof formvals[obj.name] == Array)
            formvals[obj.name].push(obj.value);
        else formvals[obj.name] = [formvals[obj.name],obj.value];
    });
    return formvals;
}

function loadCSS(strCSSFile) {
	$.ajax({
		url:'css/'+strCSSFile,
		success:function(data){
			$('<style></style>').appendTo('head').html(data);
		}
	});
}

function deleteRecord(strProcessURL, objData, strObject, jqyObjectToDelete) {
	if(!confirm('Are you sure you want to delete this '+strObject+'?'))
		return;

	$.post(strProcessURL, objData);

	jqyObjectToDelete.remove();
}