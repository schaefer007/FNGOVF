<?php
	include_once("common.php");
	$arrCSS = array();
	$arrJS = array();
	include_once("smarty.php");
	include_once("header.php");
	include_once("classes/ProcessTransition.class.php");
	include_once("classes/ProcessState.class.php");
	include_once("classes/ConditionXR.class.php");

	$intProcessTransitionID = isset($_POST["intProcessTransitionID"])?$_POST["intProcessTransitionID"]:null;
	$objProcessTransition = new ProcessTransition($intProcessTransitionID);
	$objProcessTransition->getConditionArray()->loadByTopLevelConditionID($objProcessTransition->getConditionID());
	//print_r($objProcessTransition->getConditionArray()->getArray());
	//$objProcessTransition->getCondition()->load($objProcessTransition->getConditionID());
	//$objProcessTransition->getCondition()->loadChildConditions();
	$g_objSmarty->assign("objProcessTransition", $objProcessTransition);

	$objParentConditionXR = new ConditionXR();
	$objParentConditionXR->loadByTopLevelConditionIDAndChildConditionID($objProcessTransition->getConditionID(), $objProcessTransition->getConditionID());
	$g_objSmarty->assign("objParentConditionXR", $objParentConditionXR);

	$intWFProcessID = isset($_POST["intWFProcessID"])?$_POST["intWFProcessID"]:null;
	$objProcessStateArray = new ProcessStateArray();
	$objProcessStateArray->loadByWFProcessID($intWFProcessID);
	$g_objSmarty->assign("objProcessStateArray", $objProcessStateArray);

	$g_objSmarty->display("process_transition_edit.tpl");
?>
