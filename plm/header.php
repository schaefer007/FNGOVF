<?php
	include_once("constants.php");

	$blnIsIE = (isset($_SERVER['HTTP_USER_AGENT']) && (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false));

	$arrCSS[] = "style.css.php";
	$arrCSS[] = "common.css.php";
	$arrCSS[] = "common_allsites.css.php";
	$arrCSS[] = "header.css.php";
	$arrCSS[] = "jquery-ui.css.php";
	$arrCSS[] = "popup.css.php";
	if($blnIsIE) $arrCSS[] = "ie_only.css.php";

	$arrJS2[] = "jquery.js.php";
	$arrJS2[] = "jquery-ui.js.php";
	$arrJS2[] = "header.js.php";
	$arrJS2[] = "common_allsites.js.php";
	$arrJS2[] = "popup.js.php";
	if(isset($arrJS))
		$arrJS2 = array_merge($arrJS2, $arrJS);

	$g_objSmarty->assign("arrCSS", isset($arrCSS)?$arrCSS:array());
	$g_objSmarty->assign("arrJS", isset($arrJS2)?$arrJS2:array());
	$g_objSmarty->assign("strDescription", isset($g_strDescription)?$g_strDescription:"");
	$g_objSmarty->assign("strKeywords", isset($g_strKeywords)?$g_strKeywords:strSITE_KEYWORDS);
	$g_objSmarty->assign("strTitle", (isset($g_strTitle)?$g_strTitle:strSITE_TITLE));
	$g_objSmarty->assign("objSessionUser", isset($_SESSION["objUser"])?$_SESSION["objUser"]:null);
	$g_objSmarty->assign("blnIsIE", $blnIsIE);

	$g_objSmarty->assign("objLoginUser", isset($_SESSION["arrFormData"]["objLoginUser"])?$_SESSION["arrFormData"]["objLoginUser"]:new User());

	$g_objSmarty->registerPlugin("function","printCSSFiles","printCSSFiles");
	$g_objSmarty->registerPlugin("function","printJSFiles","printJSFiles");

	// Load report configurations for Capacity Planning (they are part of the Capacity Planning pull down navigation menu)
	//if(!isset($_SESSION["objReportConfigurationCPArray"])) {
	//	$objReportConfigurationCPArray = new ReportConfigurationCPArray();
	//	$objReportConfigurationCPArray->loadByReportID(intREPORT_ID_CAPACITY_PLANNING);
	//	$_SESSION["objReportConfigurationCPArray"] = $objReportConfigurationCPArray;
	//}
?>