<table class="output_table output_table_members">
	<tr class="padding output_table_header">
		<th class="name">Member Name</th>
		<th class="email_link"></th>
		<th class="role">Role</th>
	</tr>

	{foreach from=$objProgram->getMemberArray()->getArray() item=objMember}
		<tr class="padding {cycle values="altBG0_0,altBG0_1"}" onmouseover="javascript:toggle('email_{$objMember->getMemberID()}')" onmouseout="javascript:toggle('email_{$objMember->getMemberID()}')">
			<td class="name">
				<a href="user.php?intUserID={$objMember->getUserID()}">
					{$objMember->getUser()->getDisplayName()}
				</a>
			</td>
			<td class="email_link">
				<div id="email_{$objMember->getMemberID()}" style="display:none;">
					<a href="mailto:{$objMember->getUser()->getEmail()}">
						<img src="{$smarty.const.strSITE_URL}images/email.png" alt="Email" />
					</a>
				</div>
			</td>
			<td class="role">
				{foreach from=$objMember->getMemberRoleArray()->getArray() item=objMemberRole}
					{$objMemberRole->getRole()->getRoleName()} <br />
				{/foreach}
			</td>
		</tr>
	{foreachelse}
		<tr class="padding altBG0_0"><td class="none" colspan="3">There are no members assigned to this program.</td></tr>
	{/foreach}
</table>