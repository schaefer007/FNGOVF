{include file="header.tpl"}

<div class="box">
	<div class="left">
		<h2>Login</h2><br />
		<br />

		<form method="post" action="login_process.php">
			<div class="edit_field">
				<div class="label">Username:</div>
				<div class="input"><input type="text" name="strLoginName" tabindex="1" value="{$objLoginUser->getLoginName()}" /></div>
			</div>
			<div class="clL"></div>

			<div class="edit_field">
				<div class="label">Password:</div>
				<div class="input"><input type="password" name="strPassword" tabindex="2" /></div>
			</div>
			<div class="clL"></div>

			<div>
				<input type="hidden" name="strRequestURI" value="{$strRequestURI}" />
				<input type="submit" value="Login" tabindex="3" />
			</div>
		</form>
		<div class="clL"></div>
		<br />
	</div>

	<div class="right">
		<h2>Welcome to the Flex-N-Gate PLM</h2><br />
		<br />

		<div class="welcome_text">
			The PLM is a tool to help manage programs during the engineering development / APQP phase and forward.<br />
			<br />

			The software is an in-house development that is being created and rolled out in phases. The current version includes Program Timing, BOM, Engineering Change, Document Management, Open Issues, Management Review and Tooling Budget.<br />
			<br />
			If you require a login, or have questions / concerns, please contact TJ Bourke at <a href="mailto:tbourke@flexngate-mi.com">tbourke@flexngate-mi.com</a>
		</div>
		<div class="clL"></div>
		<br />
	</div>
	<div class="clL"></div>
</div>

{include file="footer.tpl"}