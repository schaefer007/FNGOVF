<tr class="{cycle values="altBG0_0,altBG0_1"}" id="process_state_{$objProcessState->getProcessStateID()}">
	<td class="output process_state_name">{$objProcessState->getProcessStateName()}</td>
	<td class="output description">{$objProcessState->getDescription()}</td>
	{if $blnEditable}
		<td class="actions"><a href="javascript:loadProcessState('{$objProcessState->getProcessStateID()}')"><img src="images/edit.png" alt="Edit State" /></a></td>
		<td class="actions"><a href="javascript:deleteProcessState('{$objProcessState->getProcessStateID()}')"><img src="images/delete.png" alt="Delete State" /></a></td>
	{/if}
</tr>