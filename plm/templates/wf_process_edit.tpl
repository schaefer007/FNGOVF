<form id="frmPopup" method="post">
	<div class="heading">
		<h3>Add / Edit Workflow Process</h3>
		<div class="clL"></div>
	</div>

	<input type="hidden" name="arrWFProcess[intWFProcessID]" value="{$objWFProcess->getWFProcessID()}" />

	<div class="input_table input_table_wf_process">
		<div class="row">
			<div class="cell">
				<div class="label label_horizontal">Process Name:</div>
				<div class="input input_horizontal">
					<input type="text" name="arrWFProcess[strWFProcessName]" value="{$objWFProcess->getWFProcessName()}" />
				</div>
			</div>

			{*<div class="cell">
				<div class="label label_horizontal">Process Class:</div>
				<div class="input input_horizontal">
					<input type="text" name="arrWFProcess[strProcessClass]" value="{$objWFProcess->getProcessClass()}" />
				</div>
			</div>*}
			<div class="clL"></div>
		</div>
		<div class="clL"></div>

		<div class="row">
			<div class="label label_horizontal">Starting Process:</div>
			<div class="input input_horizontal" id="process_tools">
				<input type="hidden" name="arrWFProcess[intStartingProcessStateID]" value="{$objWFProcess->getStartingProcessStateID()}" />
				<input type="text" name="arrWFProcess[strStartingProcessState]" value="{$objWFProcess->getStartingProcessState()->getProcessStateName()}" />
			</div>
			<div class="clL"></div>
		</div>
		<div class="clL"></div>

		<div class="row">
			<input type="button" value="Save" onclick="popupOkSubmit()" />
			<input type="button" value="Cancel" onclick="popupCancel()" />
		</div>
	</div>
</form>