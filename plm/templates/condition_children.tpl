{if $objCondition}
	<tr class="{cycle values="altBG0_0,altBG0_1"} condition_xr_{$objParentConditionXR->getConditionXRID()}" id="condition_xr_{$objParentConditionXR->getConditionXRID()}">
		<td class="output condition_text">
			{*<input type="hidden" name="arrConditions[{$intConditionIndex}][intConditionID]" value="{$objCondition->getConditionID()}" />*}
			{section start=0 loop=$intConditionLevel name="condition_indent"}
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			{/section}
			{if $objCondition->getOperation()}
				{*<select name="arrConditions[{$intConditionIndex}][strOperation]">
					<option value="And" {if $objCondition->getOperation() == "And"}selected{/if}>And</option>
					<option value="Or" {if $objCondition->getOperation() == "Or"}selected{/if}>Or</option>
				</select>*}
				{$objCondition->getOperation()}
			{else}
				{*<select name="arrConditions[{$intConditionIndex}][intConditionID]">
					{foreach from=$objConditionArray->getArray() item=objConditionSelect}
						<option value="{$objConditionSelect->getConditionID()}" {if $objConditionSelect->getConditionID() == $objCondition->getConditionID()}selected{/if}>{$objConditionSelect->getRole()->getRoleName()}</option>
					{/foreach}
				</select>*}
				{$objCondition->getRole()->getRoleName()}
			{/if}
		</td>
		<td class="action">
			<a href="javascript:removeConditionXR('{$objParentConditionXR->getConditionXRID()}')"><img src="images/delete.png" alt="Remove" title="Remove Condition" /></a>
		</td>
		<td class="action">
			{if $objCondition->getOperation()}
				<a href="javascript:addConditionXR('{$intConditionLevel}', '{$objCondition->getConditionID()}', '{$objParentConditionXR->getConditionXRID()}')"><img src="images/add.png" alt="Add" title="Add Condition" /></a>
			{/if}
		</td>
	</tr>
	{$intConditionLevel=$intConditionLevel+1}
	{$intConditionIndex=$intConditionIndex+1 scope="parent"}
	{foreach from=$objCondition->getChildConditionXRArray()->getArray() item=objChildConditionXR}
		{$objCondition=$objConditionArray->getObject($objChildConditionXR->getChildConditionID())}
		{$objParentConditionXR=$objChildConditionXR} {* In the context of the child displayed in condition_children.tpl, $objChildConditionXR is the parent *}
		{include file="condition_children.tpl"}
	{/foreach}
{/if}