{if !$objSessionUser}

{else}
	<div class="header_links">
		Welcome <b><a href="user.php?intUserID={$objSessionUser->getUserID()}">{$objSessionUser->getDisplayName()} ({$objSessionUser->getWaitingApprovalCount()})</a></b><br />
		<a href="{$smarty.const.strSITE_URL}account.php">Account</a> | <a href="{$smarty.const.strSITE_URL}logout.php">Logout</a>
	</div>
{/if}