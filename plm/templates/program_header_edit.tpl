{include file="header.tpl"}

{$strECFieldExtra=""}
{if !$blnProgramWriteECFields}
	{$strECFieldExtra="disabled=\"true\""}
{/if}
<form id="frmProgram" action="program_header_edit_process.php" method="post" enctype="multipart/form-data">
	<input type="hidden" name="intProgramID" value="{$objProgram->getProgramID()}" />
	<input type="hidden" name="intProductTypeID" value="{$objProgram->getProductTypeID()}" />
	<input type="hidden" name="blnIsChild" value="{$objProgram->getIsChild()}" />

	<div class="heading">
		<div class="text">
			<h2>Edit Program Members</h2>
		</div>

		<div class="clL"></div>
	</div>
	<div class="clL"></div>
	<br />

	{if !$objProgram->getIsChild()} {* TODO: Consider having this for child programs *}
		<div class="box box_members">
			<div class="heading">
				<div class="text">
					<h3 help="Program membership determines what security access people have for the program.<br />Once a program is approved, all existing members are sent an email notification. When new members are added, they are also notified via email.">
						Program Members
					</h3>
				</div>

				<div class="command">
					<a href="javascript:addMember()" class="text_action">
						<div class="icon"><img src="images/add.png" alt="" title="Add Member" /></div>
						<div class="text">Add Member</div>
					</a>
					<div class="clL"></div>
				</div>
				<div class="clL"></div>
			</div>

			<table class="table table_input table_members">
				<tr class="table_header">
					<th class="name">Member Name</th>
					<th class="action"></th>
					<th class="role">Role</th>
					<th class="action"></th>
				</tr>

				<tbody id="members">
					{foreach from=$objProgram->getMemberArray()->getArray() item=objMember}
						{assign var=strMemberIndex value=$objMember->getMemberID()}
						{include file="member_role_edit.tpl"}
					{foreachelse}
						<tr id="no_members" class="altBG0_0"><td colspan="4">There are no members in this program.</td></tr>
					{/foreach}
				</tbody>
			</table>

			<input type="hidden" name="strAction" value="Add Member" />
			<input type="hidden" name="intProgramID" value="{$objProgram->getProgramID()}" />
		</div>
	{/if}
	<div class="clL"></div>
	<br />

	{if $blnProgramWrite}
		<div id="save_menu">
			<div class="button"><input type="submit" name="btnSave" id="btnSubmit" value="Save Program Membership" /></div>
		</div>
	{/if}
</form>

{include file="footer.tpl"}