{if !isset($blnEditable)}
	{$blnEditable=0}
{/if}
{cycle values="altBG0_0,altBG0_1" assign="strBGClass"}
<tbody id="process_transition_{$objProcessTransition->getProcessTransitionID()}">
	<tr class="process_transition {$strBGClass}">
		<td class="output transition_name">{$objProcessTransition->getTransitionName()}</td>
		<td class="output condition"><div class="condition_text">{$objProcessTransition->getConditionText()}</div></td>
		<td class="output transition_button">{$objProcessTransition->getTransitionButton()}</td>
		<td class="output process_state">{$objFromProcessState->getProcessStateName()}</td>
		<td class="output process_state">{$objToProcessState->getProcessStateName()}</td>
		{if $blnEditable}
			<td class="actions"><a href="javascript:loadProcessTransition('{$objProcessTransition->getProcessTransitionID()}', '{$intWFProcessID}')"><img src="images/edit.png" alt="Edit Transition" /></a></td>
			<td class="actions"><a href="javascript:deleteProcessTransition('{$objProcessTransition->getProcessTransitionID()}')"><img src="images/delete.png" alt="Delete Transition" /></a></td>
		{/if}
	</tr>
	{if $objProcessTransition->getDescription()}
		<tr class="{$strBGClass}">
			<td colspan="7" class="output"><b>Description:</b> {$objProcessTransition->getDescription()|htmlentities|nl2br}</td>
		</tr>
	{/if}
</tbody>