{if isset($smarty.session.arrWorkingData) && isset($smarty.session.arrWorkingData.objCurrentEC) && $intCurrentProgramCommonID == $smarty.session.arrWorkingData.objCurrentEC->getProgramCommonID()}
	{$objCurrentEC=$smarty.session.arrWorkingData.objCurrentEC}
	{if $objCurrentEC->getECStatus() == "New" && isset($blnEngineeringChangeWrite) && $blnEngineeringChangeWrite}
		{if strpos($smarty.server.PHP_SELF, "program.php") !== false || strpos($smarty.server.PHP_SELF, "tooling_budget.php") !== false}
			<div style="float:left;">
				{if strpos($smarty.server.PHP_SELF, "program.php") !== false}<input type="button" value="Submit EC" onclick="javascript:submitEC();" />{/if}
				<input type="button" value="Stop Editing" onclick="javascript:stopEditing();" />
			</div>
		{/if}
		<div class="editing_for_ec">
			Currently editing <a href="ec_add.php?intEngineeringChangeID={$objCurrentEC->getEngineeringChangeID()}">EC {$objCurrentEC->getECNumber()}</a>, any modifications to this program will be logged against this engineering change.
		</div>
	{else}
		{if strpos($smarty.server.PHP_SELF, "program.php") !== false || strpos($smarty.server.PHP_SELF, "tooling_budget.php") !== false}
			<div style="float:left;">
				<input type="button" value="Stop Viewing" onclick="javascript:stopEditing();" />
			</div>
		{/if}
		<div class="editing_for_ec">Currently viewing <a href="ec_add.php?intEngineeringChangeID={$objCurrentEC->getEngineeringChangeID()}">EC {$objCurrentEC->getECNumber()}</a>.</div>
	{/if}
{/if}