{include file="header_barebones.tpl"}
<div id="header">
	<div class="header">
		<br />
		<div class="logo">
			<a href="{$smarty.const.strSITE_URL}"><img src="{$smarty.const.strSITE_URL}images/header_logo.jpg" alt="Flex-N-Gate" /></a>
		</div>
		<h1 class="site_name">
			{$smarty.const.strSITE_NAME}
		</h1>
		<div class="login">
			{include file="login.tpl"}
		</div>
		<div class="clL"></div>
	</div>
	<div class="clL"></div>

	{include file="links.tpl"}
</div>

<div class="content">
{php}
	echo showFeedback();
{/php}
<div style="font-size:2px;"><br /></div>