<div class="heading">
	<div class="text">
		<h2>Workflow Management</h2>
	</div>
 
	<div class="command command_h2">
		<a href="javascript:loadWorkflowProcess()" class="text_action">
			<div class="icon"><img src="images/add.png" alt="" title="Create Workflow Process" /></div>
			<div class="text">Create Workflow Process</div>
		</a>
		<div class="clL"></div>
	</div>
	<div class="clL"></div>
</div>

<div class="box">
	<form action="{$smarty.server.PHP_SELF}" method="get">
		<div class="filter filter_wf_process">
			<div class="label">
				Workflow Process:&nbsp;
				<a href="javascript:loadWorkflowProcess('Edit');"><img src="images/edit.png" alt="Edit" title="Edit Workflow Process" /></a>
			</div>
			<div class="input">
				<select name="intWFProcessID">
					<option></option>
					{foreach from=$objWFProcessArray->getArray() item=objWFProcessSelect}
						<option value="{$objWFProcessSelect->getWFProcessID()}" {if $objWFProcessSelect->getWFProcessID() == $objWFProcess->getWFProcessID()}selected{/if}>{$objWFProcessSelect->getWFProcessName()}</option>
					{/foreach}
				</select>
			</div>
		</div>

		<div class="management_buttons">
			<input type="submit" value="Go" class="filter_button" />
		</div>
		<div class="clL"></div>
	</form>
	<div class="clL"></div>
	<br />

	{$blnEditable=1}
	{include file="workflow.tpl"}
</div>
<div class="clL"></div>
<br />

{*<div id="save_menu">
	<input type="submit" id="btnSubmit" value="Save Workflow" />
</div>
<div class="clL"></div>*}

{include file="footer.tpl"}