{$blnProgramRead=Security::hasPermission($smarty.const.intSECURITY_ITEM_ID_PROGRAM, $smarty.const.intOPERATION_ID_READ)}
{$blnDocumentRead=Security::hasPermission($smarty.const.intSECURITY_ITEM_ID_DOCUMENT, $smarty.const.intOPERATION_ID_READ)}
{$blnIssueRead=Security::hasPermission($smarty.const.intSECURITY_ITEM_ID_ISSUE, $smarty.const.intOPERATION_ID_READ)}
{$blnCapacityPlanningRead=Security::hasPermission($smarty.const.intSECURITY_ITEM_ID_CAPACITY_PLANNING, $smarty.const.intOPERATION_ID_READ)}
{*$blnCapacityPlanningWrite=Security::hasPermission($smarty.const.intSECURITY_ITEM_ID_CAPACITY_PLANNING, $smarty.const.intOPERATION_ID_WRITE)*}
{$blnMilestoneRead=Security::hasPermission($smarty.const.intSECURITY_ITEM_ID_MILESTONE, $smarty.const.intOPERATION_ID_READ)}
{$blnProductRead=Security::hasPermission($smarty.const.intSECURITY_ITEM_ID_PRODUCT, $smarty.const.intOPERATION_ID_READ)}
{$blnToolRead=Security::hasPermission($smarty.const.intSECURITY_ITEM_ID_TOOL, $smarty.const.intOPERATION_ID_READ)}
{$blnContainerRead=Security::hasPermission($smarty.const.intSECURITY_ITEM_ID_CONTAINER, $smarty.const.intOPERATION_ID_READ)}
{$blnTimeTrackingWrite=Security::hasPermission($smarty.const.intSECURITY_ITEM_ID_TIME_TRACKING, $smarty.const.intOPERATION_ID_WRITE)}
{$blnNotificationRead=Security::hasPermission($smarty.const.intSECURITY_ITEM_ID_NOTIFICATION, $smarty.const.intOPERATION_ID_READ)}
{$blnEngineeringChange=Security::hasPermission($smarty.const.intSECURITY_ITEM_ID_ENGINEERING_CHANGE, $smarty.const.intOPERATION_ID_WRITE)}
{$blnPurchaseOrderRead=Security::hasPermission($smarty.const.intSECURITY_ITEM_ID_PURCHASE_ORDER, $smarty.const.intOPERATION_ID_READ)}
{$blnOperationalChangeRead=Security::hasPermission($smarty.const.intSECURITY_ITEM_ID_OPERATIONAL_CHANGE, $smarty.const.intOPERATION_ID_READ)}
{$blnPressRead=Security::hasPermission($smarty.const.intSECURITY_ITEM_ID_PRESS, $smarty.const.intOPERATION_ID_READ)}
{$blnPlatingLineRead=Security::hasPermission($smarty.const.intSECURITY_ITEM_ID_PLATING_LINE, $smarty.const.intOPERATION_ID_READ)}
{$blnPaintingLineRead=Security::hasPermission($smarty.const.intSECURITY_ITEM_ID_PAINTING_LINE, $smarty.const.intOPERATION_ID_READ)}
{$blnPlatformRead=Security::hasPermission($smarty.const.intSECURITY_ITEM_ID_PLATFORM, $smarty.const.intOPERATION_ID_READ)}

{$blnUserRead=Security::hasPermission($smarty.const.intSECURITY_ITEM_ID_USER, $smarty.const.intOPERATION_ID_READ)}
{$blnRoleRead=Security::hasPermission($smarty.const.intSECURITY_ITEM_ID_ROLE, $smarty.const.intOPERATION_ID_READ)}

{$blnDocumentAdministrate=Security::hasPermission($smarty.const.intSECURITY_ITEM_ID_DOCUMENT, $smarty.const.intOPERATION_ID_ADMINISTRATE)}
{$blnUserAdministrate=Security::hasPermission($smarty.const.intSECURITY_ITEM_ID_USER, $smarty.const.intOPERATION_ID_ADMINISTRATE)}
{$blnRoleAdministrate=Security::hasPermission($smarty.const.intSECURITY_ITEM_ID_ROLE, $smarty.const.intOPERATION_ID_ADMINISTRATE)}
{$blnPermissionAdministrate=Security::hasPermission($smarty.const.intSECURITY_ITEM_ID_PERMISSION, $smarty.const.intOPERATION_ID_ADMINISTRATE)}
{$blnSecurityItemAdministrate=Security::hasPermission($smarty.const.intSECURITY_ITEM_ID_SECURITY_ITEM, $smarty.const.intOPERATION_ID_ADMINISTRATE)}
{$blnWorkflowAdministrate=Security::hasPermission($smarty.const.intSECURITY_ITEM_ID_WORKFLOW, $smarty.const.intOPERATION_ID_ADMINISTRATE)}
{$blnNotificationAdministrate=Security::hasPermission($smarty.const.intSECURITY_ITEM_ID_NOTIFICATION, $smarty.const.intOPERATION_ID_ADMINISTRATE)}
{$blnMilestoneAdministrate=Security::hasPermission($smarty.const.intSECURITY_ITEM_ID_MILESTONE, $smarty.const.intOPERATION_ID_ADMINISTRATE)}
{$blnCustomerRead=Security::hasPermission($smarty.const.intSECURITY_ITEM_ID_CUSTOMER, $smarty.const.intOPERATION_ID_READ)}
{$blnShipToFacilityRead=Security::hasPermission($smarty.const.intSECURITY_ITEM_ID_SHIP_TO_FACILITY, $smarty.const.intOPERATION_ID_READ)}
{$blnProductionFacilityRead=Security::hasPermission($smarty.const.intSECURITY_ITEM_ID_PRODUCTION_FACILITY, $smarty.const.intOPERATION_ID_READ)}
{$blnCronjobAdministrate=Security::hasPermission($smarty.const.intSECURITY_ITEM_ID_CRONJOBS, $smarty.const.intOPERATION_ID_ADMINISTRATE)}
{$blnAdmin=$blnDocumentAdministrate || $blnUserAdministrate || $blnRoleAdministrate || $blnPermissionAdministrate || $blnSecurityItemAdministrate || $blnWorkflowAdministrate || $blnNotificationAdministrate || $blnMilestoneAdministrate || $blnCustomerRead || $blnShipToFacilityRead || $blnCronjobAdministrate}

{$blnProgramWrite=Security::hasPermission($smarty.const.intSECURITY_ITEM_ID_PROGRAM, $smarty.const.intOPERATION_ID_WRITE)}

<div class="links links_main">
	{*<div class="logo">
		<a href="{$smarty.const.strSITE_URL}"><img src="{$smarty.const.strSITE_URL}images/header_logo.png" alt="Flex-N-Gate" /></a>
	</div>*}
	{if !isset($smarty.session.objUser)}
		<div class="link" submenu="">
			<a href="index.php" class="text">Login</a>
		</div>
	{/if}
	<div class="link">
		<a href="ec_manage.php" class="text">Engineering Changes</a>
	</div>

	<div class="link">
		<a href="workflow_manage.php" class="text">Workflow Management</a>
	</div>

	<div class="clL"></div>
</div>
<div class="links_divider">&nbsp;</div>


<div id="menu_lists" class="submenu" menu="true">
	<div class="links">
		{if $blnProductRead}
			<div class="link">
				<a href="manufactured_product_manage.php" class="text">Manufactured Parts</a>
			</div>
			<div class="clL"></div>
		{/if}
		{if $blnProductRead}
			<div class="link">
				<a href="purchased_product_manage.php" class="text">Purchased Parts</a>
			</div>
			<div class="clL"></div>
		{/if}
		{if $blnToolRead}
			<div class="link">
				<a href="tool_manage.php" class="text">Tools</a>
			</div>
			<div class="clL"></div>
		{/if}
		{if $blnPurchaseOrderRead}
			<div class="link">
				<a href="purchase_order_manage.php" class="text">Purchase Orders</a>
				{if Security::hasPermission($smarty.const.intSECURITY_ITEM_ID_PURCHASE_ORDER, $smarty.const.intOPERATION_ID_WRITE)}
					<a href="purchase_order_add.php" class="add"><img src="images/add.png" alt="Add" title="Create Purchase Order" /></a>
				{/if}
			</div>
			<div class="clL"></div>
		{/if}
		{if $blnProgramRead}
		{/if}
		{if $blnOperationalChangeRead}
			<div class="link">
				<a href="oc_manage.php" class="text">Operational Changes</a>

				{if Security::hasPermission($smarty.const.intSECURITY_ITEM_ID_OPERATIONAL_CHANGE, $smarty.const.intOPERATION_ID_WRITE)}
					<a href="oc_add.php" class="add"><img src="images/add.png" alt="Add" title="Create Operational Change" /></a>
				{/if}
			</div>

			<div class="clL"></div>
		{/if}
		{if $blnPlatformRead}
			<div class="link">
				<a href="platform_manage.php" class="text">Platforms</a>
			</div>
			<div class="clL"></div>
		{/if}
		{if $blnMilestoneRead}
			<div class="link">
				<a href="program_milestone_manage.php" class="text">Milestones</a>
			</div>
			<div class="clL"></div>

		{/if}
		{if $blnProductionFacilityRead}
			<div class="link">
				<a href="production_facility_manage.php" class="text">Plants</a>
			</div>
			<div class="clL"></div>
		{/if}
		{if $blnPressRead}
			<div class="link">
				<a href="press_manage.php" class="text">Presses</a>
				{if Security::hasPermission($smarty.const.intSECURITY_ITEM_ID_PRESS, $smarty.const.intOPERATION_ID_WRITE)}
					<a href="press_add.php" class="add"><img src="images/add.png" alt="Add" title="Add Press" /></a>
				{/if}
			</div>
			<div class="clL"></div>
		{/if}
		{if $blnUserRead}
			<div class="link">
				<a href="users.php" class="text">Users</a>
			</div>
			<div class="clL"></div>
		{/if}
		{if $blnRoleRead}
			<div class="link">
				<a href="roles.php" class="text">Roles</a>
			</div>
			<div class="clL"></div>
		{/if}
	</div>
	<div class="clL"></div>
	<div class="links_divider">&nbsp;</div>
</div>

<div id="menu_help" class="submenu" menu="true">
	<div class="links">
		<div class="link">
			<a href="help_plm_overview.php" class="text">PLM Overview</a>
		</div>
		<div class="clL"></div>

		<div class="link link_header">Processes</div>
		<div class="clL"></div>

		<div class="link sub_link">
			<a href="help_wi_njsn.php" class="text">Create Program / NJSN</a>
		</div>
		<div class="clL"></div>

		<div class="link sub_link">
			<a href="help_wi_gate.php" class="text">Gate Review</a>
		</div>
		<div class="clL"></div>

		<div class="link sub_link">
			<a href="help_wi_ec.php" class="text">Engineering Change</a>
		</div>
		<div class="clL"></div>

		<div class="link link_header">How To</div>
		<div class="clL"></div>

		<div class="link sub_link">
			<a href="help_ht_create_program.php" class="text">Create Program / NJSN</a>
		</div>

		<div class="clL"></div>

		<div class="link sub_link">
			<a href="help_ht_documents.php" class="text">Documents</a>
		</div>
		<div class="clL"></div>

		<div class="link sub_link">
			<a href="help_ht_open_issues.php" class="text">Open Issues</a>
		</div>
		<div class="clL"></div>

		<div class="link link_header">Reference</div>
		<div class="clL"></div>
		<div class="link sub_link">
			<a href="help_common_functionality.php" class="text">Common Functionality</a>
		</div>
		<div class="clL"></div>

		<div class="link sub_link">
			<a href="help_ai_milestones.php" class="text">Milestones</a>
		</div>
		<div class="clL"></div>
	</div>
	<div class="clL"></div>
	<div class="links_divider">&nbsp;</div>
</div>

<div id="menu_my_plm" class="submenu" menu="true">
	<div class="links">
		{if isset($objSessionUser)}
			{if $blnProgramRead}
				<div class="link">
					<a href="program_manage.php?blnFromUserPage=1&intUserID={$objSessionUser->getUserID()}" class="text">Programs ({$objSessionUser->getProgramCount()})</a>
				</div>
				<div class="clL"></div>
			{/if}

			{if $blnIssueRead}
				<div class="link">
					<a href="issue_manage.php?blnFromUserPage=1&intUserID={$objSessionUser->getUserID()}" class="text">Open Issues ({$objSessionUser->getIssueCount()})</a>
				</div>
				<div class="clL"></div>
			{/if}

			<div class="link">
				<a href="{$objSessionUser->getUserPageURL()}#pending_approvals" class="text">Pending Approvals ({$objSessionUser->getWaitingApprovalCount()})</a>
			</div>
			<div class="clL"></div>


			{if $blnDocumentRead}
				<div class="link">
					<a href="document_manage.php?blnFromUserPage=1&intCreatedByUserID={$objSessionUser->getUserID()}" class="text">Documents ({$objSessionUser->getDocumentCount()})</a>
				</div>
				<div class="clL"></div>
			{/if}

			<div class="link">
				<a href="refresh_counts.php" class="text">Refresh Counts</a>
			</div>

			<div class="clL"></div>
		{/if}
	</div>
	<div class="clL"></div>
	<div class="links_divider">&nbsp;</div>
</div>

<div id="menu_admin" class="submenu" menu="true">
	<div class="links">
		<div class="link link_header">System</div>
		<div class="clL"></div>
		{if $blnUserAdministrate}
			<div class="link sub_link">

				<a href="users_manage.php" class="text">User Management</a>
			</div>
			<div class="clL"></div>
		{/if}
		{if $blnRoleAdministrate}
			<div class="link sub_link">
				<a href="roles_manage.php" class="text">Role Management</a>
			</div>
			<div class="clL"></div>
		{/if}
		{if $blnPermissionAdministrate}
			<div class="link sub_link">
				<a href="permissions_manage.php" class="text">Permission Management</a>
			</div>
			<div class="clL"></div>
		{/if}
		{if $blnSecurityItemAdministrate}
			<div class="link sub_link">
				<a href="security_items_manage.php" class="text">Security Item Management</a>
			</div>
			<div class="clL"></div>
		{/if}
		{if $blnWorkflowAdministrate}
			<div class="link sub_link">
				<a href="workflow_manage.php" class="text">Workflow Management</a>
			</div>
			<div class="clL"></div>
		{/if}
		{if $blnDocumentAdministrate}
			<div class="link sub_link">
				<a href="document_security_manage.php" class="text">Document Security</a>

			</div>
			<div class="clL"></div>
		{/if}
		{if $blnDocumentAdministrate}
			<div class="link sub_link">
				<a href="folderstructure_manage.php" class="text">Document Templates</a>
			</div>
			<div class="clL"></div>
		{/if}
		{if $blnNotificationAdministrate}
			<div class="link sub_link">
				<a href="notification_subscription_manage.php" class="text">Notification Subscription</a>
			</div>
			<div class="clL"></div>
		{/if}

		<div class="link link_header">Master Tables</div>
		<div class="clL"></div>

		{if $blnMilestoneAdministrate}
			<div class="link sub_link">
				<a href="milestone_manage.php" class="text">Milestone Management</a>
			</div>
			<div class="clL"></div>
		{/if}
		{if $blnCustomerRead}
			<div class="link sub_link">
				<a href="customer_manage.php" class="text">Customer Management</a>
			</div>
			<div class="clL"></div>
		{/if}
		{if $blnShipToFacilityRead}
			<div class="link sub_link">
				<a href="facility_manage.php" class="text">Ship To Management</a>
			</div>
			<div class="clL"></div>
		{/if}

		<div class="link link_header">Tools</div>
		<div class="clL"></div>

		{if $blnUserAdministrate}
			<div class="link sub_link">
				<a href="emails.php" class="text">Emails</a>
			</div>
			<div class="clL"></div>
		{/if}

		<div class="link link_header">Cronjobs</div>
		<div class="clL"></div>

		{if $blnCronjobAdministrate}
			<div class="link sub_link">
				<a href="cron_milestoneCSSClass.php" class="text" target="_blank">Milestone Colors</a>
			</div>
			<div class="clL"></div>
		{/if}

		{if $blnCronjobAdministrate}
			<div class="link sub_link">
				<a href="cron_scheduledContainerMovement.php" class="text" target="_blank">Container Movement</a>
			</div>
			<div class="clL"></div>
		{/if}

		{if $blnCronjobAdministrate}
			<div class="link sub_link">
				<a href="cron_platformStatus.php" class="text" target="_blank">Platform Statuses</a>
			</div>
			<div class="clL"></div>
		{/if}
		<div class="clL"></div>
	</div>
	<div class="clL"></div>
	<div class="links_divider">&nbsp;</div>
</div>