<form id="frmPopup" class="edit_tool">
	<div class="heading">
		<h3>Add / Edit Process State</h3>
		<div class="clL"></div>
	</div>

	<input type="hidden" name="arrProcessState[intProcessStateID]" value="{$objProcessState->getProcessStateID()}" />

	<div class="input_table input_table_process_state">
		<div class="row">
			<div class="cell process_state_name">
				<div class="label label_horizontal">State Name:</div>
				<div class="input input_horizontal">
					<input type="text" name="arrProcessState[strProcessStateName]" value="{$objProcessState->getProcessStateName()|htmlentities}" />
				</div>
			</div>
		</div>
		<div class="clL"></div>

		<div class="row">
			<div class="cell description">
				<div class="label label_horizontal">Description:</div>
				<div class="input input_horizontal">
					<input type="text" name="arrProcessState[strDescription]" value="{$objProcessState->getDescription()|htmlentities}" />
				</div>
			</div>
		</div>
		<div class="clL"></div>

		<div class="row">
			<div class="cell cord">
				<div class="label label_horizontal">Diagram X Cord:</div>
				<div class="input input_horizontal">
					<input type="text" name="arrProcessState[intDiagramXCord]" value="{$objProcessState->getDiagramXCord()|htmlentities}" />
				</div>
			</div>

			<div class="cell cord">
				<div class="label label_horizontal">Diagram Y Cord:</div>
				<div class="input input_horizontal">
					<input type="text" name="arrProcessState[intDiagramYCord]" value="{$objProcessState->getDiagramYCord()|htmlentities}" />
				</div>
			</div>
		</div>
		<div class="clL"></div>

		<div class="row">
			<input type="button" value="Save" onclick="popupOk()" />
			<input type="button" value="Cancel" onclick="popupCancel()" />
		</div>
	</div>
</form>