<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title>{if $strTitle}{$strTitle}{else}{$smarty.const.strSITE_TITLE}{/if}</title>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<meta name="description" content="{$strDescription}" />
		<meta name="keywords" content="{$strKeywords}" />
		<meta name="author" content="TJ Bourke" />

		<link href="{$smarty.const.strSITE_URL}/images/favicon.ico" rel="icon" type="image/x-icon" />

		{if isset($blnEmbedExternalFiles) && $blnEmbedExternalFiles}
			<style type="text/css">
				{printCSSFiles($arrCSS)}
			</style>
			<script type="text/javascript" language="javascript">
				{printJSFiles($arrJS)}
			</script>
		{else}
			<link rel="stylesheet" type="text/css" href="{$smarty.const.strSITE_URL}css.php?{foreach from=$arrCSS item=strCSSFile}&amp;arrCSS&#91;&#93;={$strCSSFile}{/foreach}" />
			<script type="text/javascript" src="{$smarty.const.strSITE_URL}js.php?{foreach from=$arrJS item=strJSFile}&amp;arrJS&#91;&#93;={$strJSFile}{/foreach}"></script>
		{/if}
	</head>
	<body>