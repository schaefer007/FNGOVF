<tr class="{cycle values="altBG0_0,altBG0_1"}" id="member_{$strMemberIndex}">
	<td class="name">
		<input type="hidden" name="arrMembers[{$strMemberIndex}][intMemberID]" value="{$objMember->getMemberID()}" />
		<input type="hidden" name="arrMembers[{$strMemberIndex}][blnDeleted]" value="0" />
		<select name="arrMembers[{$strMemberIndex}][intUserID]" onchange="updateMemberRole('User', '{$strMemberIndex}')">
			<option></option>
			{foreach from=$objUserArray->getArray() item=objUser}
				<option value="{$objUser->getUserID()}" {if $objMember->getUserID() == $objUser->getUserID() || count($objUserArray->getArray()) == 1}selected{/if}>{$objUser->getDisplayName()}</option>
			{/foreach}
		</select>
	</td>
	<td class="action">
		<a href="javascript:deleteMember('{$strMemberIndex}');"><img src="images/delete.png" alt="Delete Member" /></a>
	</td>
	<td class="role" id="member_roles_{$strMemberIndex}">
		{foreach from=$objMember->getMemberRoleArray()->getArray() item=objMemberRole}
			<select name="arrMembers[{$strMemberIndex}][arrMemberRoles][{$objMemberRole->getMemberRoleID()}]" onchange="updateMemberRole('Role', '{$strMemberIndex}')">
				<option></option>
				{foreach from=$objRoleArray->getArray() item=objRole}
					<option value="{$objRole->getRoleID()}" {if $objMemberRole->getRoleID() == $objRole->getRoleID() || count($objRoleArray->getArray()) == 1}selected{/if}>{$objRole->getRoleName()}</option>
				{/foreach}
			</select>
			<br />
		{foreachelse}
			<select name="arrMembers[{$strMemberIndex}][arrMemberRoles][new_0]" onchange="updateMemberRole('Role', '{$strMemberIndex}')">
				<option></option>
				{foreach from=$objRoleArray->getArray() item=objRole}
					<option value="{$objRole->getRoleID()}" {if count($objRoleArray->getArray()) == 1}selected{/if}>{$objRole->getRoleName()}</option>
				{/foreach}
			</select>
			<br />
		{/foreach}
	</td>
	<td class="action">
		<a href="javascript:addRole('{$strMemberIndex}');"><img src="images/add.png" alt="Add Role" title="Add Role" /></a>
	</td>
</tr>