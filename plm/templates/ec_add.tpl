{include file="header.tpl"}

{$strDisabled=""}
{if $objEC->getECStatus() != "New" || !$blnEngineeringChangeWrite}
	{$strDisabled="disabled=\"true\""}
{/if}

<div class="heading">
	<div class="text">
		<h2>Engineering Change Detail</h2>
	</div>

	<div class="command command_h2">
		<a href="{$objEC->getProgramCommon(true)->getURL()}" class="text_action">
			<div class="icon"><img src="images/back.png" alt="" title="Back to Program" /></div>
			<div class="text">Back to Program</div>
		</a>
		<div class="clL"></div>
	</div>
	<div class="clL"></div>
</div>

{$objProgram=$objEC->getProgram()}

<form id="frmEC" action="ec_edit_process.php" method="post" enctype="multipart/form-data">
	<input type="hidden" name="intEngineeringChangeID" value="{$objEC->getEngineeringChangeID()}" />
	<input type="hidden" name="intProgramCommonID" value="{$objEC->getProgramCommonID()|htmlentities}" />

	<div class="box">
		<div class="heading">
			<div class="text">
				<h3>Engineering Change Header</h3>
			</div>

			<div class="command">
				<a href="javascript:editEC('{$objEC->getEngineeringChangeID()}', '{$objProgram->getProgramID()}')" class="text_action">
					{if $blnEngineeringChangeWrite && $objEC->getECStatus() == "New"}
						<div class="icon"><img src="images/edit.png" alt="" title="Edit Program Changes" /></div>
						<div class="text">Edit Program Changes</div>
					{else}
						<div class="icon"><img src="images/view.png" alt="" title="View Program Changes" /></div>
						<div class="text">View Program Changes</div>
					{/if}
				</a>
				<div class="clL"></div>
			</div>
			<div class="clL"></div>
		</div>
		<div class="clL"></div>

		<div class="program_header">
			<div class="big_column">
				<div class="edit_field">
					<div class="label">Internal No.:</div>
					<div class="input">
						<input type="text" name="strECNumber" value="{if $objEC->getEngineeringChangeID()}EC {$objEC->getECNumber()|htmlentities}{else}Auto-assigned on save{/if}" disabled="true" />
					</div>
				</div>
				<div class="clL"></div>

				<div class="edit_field">
					<div class="label">Customer No.:</div>
					<div class="input">
						<input type="text" name="strCustomerChangeNotice" value="{$objEC->getCustomerChangeNotice()|htmlentities}" {$strDisabled} />
					</div>
				</div>
				<div class="clL"></div>

				<div class="edit_field">
					<div class="label">Source:</div>
					<div class="input">
						<select name="blnInternal" {$strDisabled}>
							<option value="1" {if $objEC->getInternal()}selected{/if}>Internal</option>
							<option value="0" {if !$objEC->getInternal()}selected{/if}>Customer</option>
						</select>
					</div>
				</div>
				<div class="clL"></div>

				<div class="edit_field">
					<div class="label">PPAP Submission Level:</div>
					<div class="input">
						<select name="intPPAPSubmissionLevel" {$strDisabled}>
							<option value=""></option>
							<option value="1" {if $objEC->getPPAPSubmissionLevel() == 1}selected{/if}>1</option>
							<option value="2" {if $objEC->getPPAPSubmissionLevel() == 2}selected{/if}>2</option>
							<option value="3" {if $objEC->getPPAPSubmissionLevel() == 3}selected{/if}>3</option>
							<option value="4" {if $objEC->getPPAPSubmissionLevel() == 4}selected{/if}>4</option>
							<option value="5" {if $objEC->getPPAPSubmissionLevel() == 5}selected{/if}>5</option>
						</select>
					</div>
				</div>
				<div class="clL"></div>
			</div>

			<div class="big_column">
				<div class="edit_field">
					<div class="label">PPAP Date:</div>
					<div class="input">
						<input type="text" class="date" name="dtmPPAPDate" value="{dateFieldFormat($objEC->getPPAPDate(), true)}" {$strDisabled} />
					</div>
				</div>
				<div class="clL"></div>

				<div class="edit_field">
					<div class="label">Design Release Date:</div>
					<div class="input">
						<input type="text" class="date" name="dtmDesignReleaseDate" value="{dateFieldFormat($objEC->getDesignReleaseDate(), true)}" {$strDisabled} />
					</div>
				</div>
				<div class="clL"></div>

				<div class="edit_field">
					<div class="label">Material Availability Date:</div>
					<div class="input">
						<input type="text" class="date" name="dtmMaterialAvailabilityDate" value="{dateFieldFormat($objEC->getMaterialAvailabilityDate(), true)}" {$strDisabled} />
					</div>
				</div>
				<div class="clL"></div>

				<div class="edit_field">
					<div class="label">Created By:</div>
					<div class="input">
						<input type="text" name="intCreatedByUserID" value="{$objEC->getCreatedByUser()->getDisplayName()}" disabled="true" />
					</div>
				</div>
				<div class="clL"></div>
			</div>

			<div class="big_column">
				<div class="edit_field">
					<div class="label">Kick Off Date:</div>
					<div class="input">
						<input type="text" class="date" name="dtmIssueDate" value="{dateFieldFormat($objEC->getIssueDate(), true)}" {$strDisabled} />
					</div>
				</div>
				<div class="clL"></div>

				<div class="edit_field">
					<div class="label">Implementation Date*:</div>
					<div class="input">
						<input type="text" class="date" name="dtmEffectiveDate" value="{dateFieldFormat($objEC->getEffectiveDate(), true)}" />
					</div>
				</div>
				<div class="clL"></div>

				<div class="edit_field">
					<div class="label">Status:</div>
					<div class="input">
						<input type="text" name="strStatus" value="{$objEC->getECStatus()|htmlentities}" disabled="true" />
					</div>
				</div>
				<div class="clL"></div>

				<div class="edit_field">
					<div class="label">Created Date:</div>
					<div class="input">
						<input type="text" class="date" name="dtmIssueDate" value="{dateFieldFormat($objEC->getCreatedOn(), true)}" disabled="true" />
					</div>
				</div>
				<div class="clL"></div>
			</div>
			<div class="clL"></div>

			<div class="subject">
				<div class="label">One Line Description*:</div>
				<div class="input">
					<input type="text" name="strSubject" value="{$objEC->getSubject()|htmlentities}" maxlength="100" {$strDisabled} />
				</div>
			<div class="clL"></div>
			</div>

			<div>
				<div class="edit_field edit_field_description">
					<div class="label">Detailed Description of Change*:</div>
					<div class="input">
						<textarea name="txtDescription" {$strDisabled}>{$objEC->getDescription()}</textarea>
					</div>
				</div>
				<div class="clL"></div>
			</div>

			{*if $objEC->getProcessInstance()->getCurrentProcessStateID() != $smarty.const.intPROCESS_STATE_ID_EC_NEW}
				{$strPlantInputDisabled="disabled=\"true\""}
				{if $objSessionUser->getUserRoleXRArray()->objectIsSet($smarty.const.intROLE_ID_PLANT_MANAGER)
					|| $objSessionUser->getUserRoleXRArray()->objectIsSet($smarty.const.intROLE_ID_GENERAL_MANAGER)}
					{$strPlantInputDisabled=""}
				{/if}
				<div>
					<div class="edit_field edit_field_description">
						<div class="label">Plant Input:</div>
						<div class="input">
							<textarea name="txtPlantInput" {$strDisabled} {$strPlantInputDisabled}>{$objEC->getPlantInput()}</textarea>
						</div>
					</div>
					<div class="clL"></div>
				</div>

				{$strSalesInputDisabled="disabled=\"true\""}
				{if $objSessionUser->getUserRoleXRArray()->objectIsSet($smarty.const.intROLE_ID_ACCOUNT_MANAGER)
					|| $objSessionUser->getUserRoleXRArray()->objectIsSet($smarty.const.intROLE_ID_DIRECTOR_OF_SALES)}
					{$strSalesInputDisabled=""}
				{/if}
				<div>
					<div class="edit_field edit_field_description">
						<div class="label">Sales Input:</div>
						<div class="input">
							<textarea name="txtSalesInput" {$strDisabled} {$strSalesInputDisabled}>{$objEC->getSalesInput()}</textarea>
						</div>
					</div>
					<div class="clL"></div>
				</div>
			{/if*}
		</div>
		<div class="clL"></div>

		<div class="heading">
			<div class="text">
				<h3>Additional Information</h3>
			</div>
			<div class="clL"></div>
		</div>

		<div class="big_column">
			<div class="edit_field">
				<div class="label">Bank Build Required:</div>
				<div class="input">
					<input type="checkbox" name="blnBankBuildRequired" {if $objEC->getBankBuildRequired()}checked="true"{/if} {$strDisabled} />
				</div>
			</div>
			<div class="clL"></div>

			<div class="edit_field">
				<div class="label">Service Affected:</div>
				<div class="input">
					<input type="checkbox" name="blnServiceAffected" {if $objEC->getServiceAffected()}checked="true"{/if} {$strDisabled} />
				</div>
			</div>
			<div class="clL"></div>
		</div>

		<div class="big_column">
			<div class="edit_field">
				<div class="label">Build Time Required:</div>
				<div class="input">
					<input type="text" name="strTimeRequired" value="{$objEC->getTimeRequired()}" {$strDisabled} />
				</div>
			</div>
			<div class="clL"></div>

			<div class="edit_field">
				<div class="label">Running Change:</div>
				<div class="input">
					<input type="checkbox" name="blnRunningChange" {if $objEC->getRunningChange()}checked="true"{/if} {$strDisabled} />
				</div>
			</div>
			<div class="clL"></div>
		</div>

		<div class="clL"></div>
	</div>
	<div class="clL"></div>
	<br />


	{if $objEC->getEngineeringChangeID() && $objEC->getProcessInstanceID()}
		<div class="box">
			{$strProcessTitle="Engineering Change"}
			<div class="heading">
				<h3>{$strProcessTitle} Process</h3>
				<div class="clL"></div>
			</div>
			<div class="clL"></div>

			{$objProcessInstance=$objEC->getProcessInstance()}
			{$blnOverrideWorkflow=Security::hasPermission($smarty.const.intSECURITY_ITEM_ID_ENGINEERING_CHANGE, $smarty.const.intOPERATION_ID_OVERRIDE_APPROVAL)}
			{include file="process_instance.tpl"}
			<div class="clL"></div>
		</div>
		<div class="clL"></div>
		<br />
	{/if}

	<br />

	{if $blnEngineeringChangeWrite && !$strDisabled}
		<div id="save_menu">
			<div class="button"><input type="submit" name="btnSave" id="btnSubmit" value="Save Engineering Change" /></div>

			{*if $blnEngineeringChangeSubmitForApproval && $objEC->getECStatus() == "New"}
				<input type="submit" name="btnSubmitForApproval" value="Save & Submit for Approval" />

				<input type="text" name="txtSubmittedComment" value="{if $objEC->getApprovalProcessInstance()->getSubmittedComment()}{$objEC->getApprovalProcessInstance()->getSubmittedComment()}{else}Submitter comment{/if}" class="submitted_comment" onclick="if(this.value=='Submitter comment') this.value=''" />
			{/if*}
			{$intCurrentProgramCommonID=$objEC->getProgramCommonID()}
			{include file="ec_currently_editing.tpl"}
		</div>
	{/if}
</form>

{include file="footer.tpl"}