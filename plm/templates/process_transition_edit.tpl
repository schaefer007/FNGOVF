<form id="frmPopup" class="edit_tool">
	<div class="heading">
		<h3>Add / Edit Process Transition</h3>
		<div class="clL"></div>
	</div>

	<input type="hidden" name="arrProcessTransition[intProcessTransitionID]" value="{$objProcessTransition->getProcessTransitionID()}" />

	<div class="input_table input_table_process_transition">
		<div class="row">
			<div class="cell transition_name">
				<div class="label label_horizontal">Transition Name:</div>
				<div class="input input_horizontal">
					<input type="text" name="arrProcessTransition[strTransitionName]" value="{$objProcessTransition->getTransitionName()|htmlentities}" />
				</div>
			</div>

			<div class="cell transition_button">
				<div class="label label_horizontal">Transition Button:</div>
				<div class="input input_horizontal">
					<input type="text" name="arrProcessTransition[strTransitionButton]" value="{$objProcessTransition->getTransitionButton()|htmlentities}" />
				</div>
			</div>
			<div class="clL"></div>
		</div>
		<div class="clL"></div>

		<div class="row">
			<div class="cell state">
				<div class="label label_horizontal">From State:</div>
				<div class="input input_horizontal">
					<select name="arrProcessTransition[intFromProcessStateID]">
						{foreach from=$objProcessStateArray->getArray() item=objProcessState}
							<option value="{$objProcessState->getProcessStateID()}" {if $objProcessState->getProcessStateID() == $objProcessTransition->getFromProcessStateID()}selected{/if}>
								{$objProcessState->getProcessStateName()|htmlentities}
							</option>
						{/foreach}
					</select>
				</div>
			</div>

			<div class="cell state">
				<div class="label label_horizontal">To State:</div>
				<div class="input input_horizontal">
					<select name="arrProcessTransition[intToProcessStateID]">
						{foreach from=$objProcessStateArray->getArray() item=objProcessState}
							<option value="{$objProcessState->getProcessStateID()}" {if $objProcessState->getProcessStateID() == $objProcessTransition->getToProcessStateID()}selected{/if}>
								{$objProcessState->getProcessStateName()|htmlentities}
							</option>
						{/foreach}
					</select>
				</div>
			</div>
			<div class="clL"></div>
		</div>
		<div class="clL"></div>

		<div class="row">
			<div class="cell state">
				<div class="label label_horizontal">Display Order:</div>
				<div class="input input_horizontal">
					<input type="text" name="arrProcessTransition[intDisplayOrder]" value="{$objProcessTransition->getDisplayOrder()}" />
				</div>
			</div>
		</div>
		<div class="clL"></div>
		<br />

		<div class="heading">
			<h4>Conditions</h4>
			<div class="clL"></div>
		</div>
		<div class="clL"></div>

		<input type="hidden" name="intTopLevelConditionID" value="{$objProcessTransition->getConditionID()}" />
		<table class="table table_conditions">
			{$intConditionLevel=0}
			{$intConditionIndex=0}
			{*$objCondition=$objProcessTransition->getCondition()*}
			{$objConditionArray=$objProcessTransition->getConditionArray()}
			{$objCondition=$objConditionArray->getObject($objProcessTransition->getConditionID())}
			{include file="condition_children.tpl"}
		</table>
		<br />

		<div class="row">
			<input type="button" value="Save" onclick="popupOk()" />
			<input type="button" value="Cancel" onclick="popupCancel()" />
		</div>
	</div>
</form>