{include file="header.tpl"}

<div class="heading">
	<div class="text">
		<h2>Engineering Change Management ({$objECArray->getArray()|@count})</h2>
	</div>

	<div class="command command_h2">
		<a href="ec_add.php" class="text_action">
			<div class="icon"><img src="{$smarty.const.strSITE_URL}images/add.png" alt="" title="Create Engineering Change" /></div>
			<div class="text">Create Engineering Change</div>
		</a>
		<div class="clL"></div>
	</div>

	<div class="clL"></div>
</div>

<div class="box">
	<form id="frmSearch" method="get" action="{$smarty.server.PHP_SELF}">
		{*<div class="filter filter_product_type">
			<div class="label">Type:&nbsp;</div>
			<div class="input">
				{assign var=strName value=intProductTypeID}
				{assign var=arrSource value=$objProductTypeArray->getArray()}
				{assign var=strValue value=$objSearch->getProductTypeID()}
				{include file="select.tpl"}
			</div>
			<div class="clL"></div>
		</div>*}
		<div class="clL"></div>
	</form>

	<table class="table table_products">
		<tr class="">
			<th class=""><a href="?strSortBy=tblProduct.strProgramName">Program</a></th>
			<th class=""><a href="?strSortBy=tblEngineeringChange.intECNumber">Number</a></th>
			<th class=""><a href="?strSortBy=tblEngineeringChange.strCustomerChangeNotice">Cust. Number</a></th>
			<th class=""><a href="?strSortBy=tblEngineeringChange.strECStatus">Status</a></th>
			<th class=""><a href="?strSortBy=tblEngineeringChange.blnInternal">Source</a></th>
			<th class=""><a href="?strSortBy=tblEngineeringChange.strSubject">One Line Description</a></th>
			<th class=""><a href="?strSortBy=tblEngineeringChange.dtmEffectiveDate">Imp. Date</a></th>
		</tr>

		{foreach from=$objECArray->getArray() item=objEC}
			{cycle values="altBG0_0,altBG0_1" assign=strBGClass}
			<tr class="{$strBGClass}">
				<td class="output"><a href="program_header_edit.php?intProgramID={$objEC->getProgram()->getProductID()}">{$objEC->getProgram()->getProgramName()|htmlentities}</a></td>
				<td class="output"><a href="{$objEC->getURL()}">{$objEC->getFormattedECNumber()|htmlentities}</a></td>
				<td class="output">{$objEC->getCustomerChangeNotice()|htmlentities}</td>
				<td class="output">{$objEC->getECStatus()}</td>
				<td class="output">{if !$objEC->getInternal()}Customer{else}Internal{/if}</td>
				<td class="output">{$objEC->getSubject()|htmlentities}</td>
				<td class="output">{formatDate($objEC->getEffectiveDate(), true)}</td>
			</tr>
		{foreachelse}
			<tr class="padding altBG0_0"><td colspan="3">There are no engineering changes that match your search.</td></tr>
		{/foreach}
	</div>
	<div class="clL"></div>
</div>
<div class="clL"></div>
<br />

{include file="footer.tpl"}
