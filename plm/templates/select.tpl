{if !isset($strName)}
	Error: The $strName variable was not set for the select.tpl template.
{/if}

<select name="{$strName}" {if isset($strID)} id="{$strID}"{/if} {if isset($blnDisabled) && $blnDisabled}disabled="true"{/if}>
	{if !isset($blnNoBlank) || $blnNoBlank}
		<option value=""></option>
	{/if}
	{foreach from=$arrSource item=objSource}
		{if is_object($objSource)}
			<option value="{$objSource->getID()}" {if $strValue == $objSource->getID()}selected{/if}>{$objSource->getName()|htmlentities}</option>
		{else}
			<option value="{$objSource}" {if $strValue == $objSource}selected{/if}>{$objSource|htmlentities}</option>
		{/if}
	{/foreach}
</select>
