{if !isset($blnOverrideWorkflow)}
	{$blnOverrideWorkflow=0}
{/if}

<div id="process_instance">
	<a name="workflow"></a>
	{foreach from=$objProcessInstance->getCurrentProcessState()->getProcessTransitionArray()->getArray() item=objProcessTransition}
		<input type="hidden" id="comment_required_{$objProcessTransition->getProcessTransitionID()}" name="arrProcessTransitions[{$objProcessTransition->getProcessTransitionID()}][blnCommentRequired]" value="{$objProcessTransition->getCommentRequired()}" />
	{/foreach}
	<table class="table table_wf_processes">
		<tr>
			<th class="state" colspan="4">
				State &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<a href="javascript:processDefinition('{$objProcessInstance->getWFProcessID()}')">View {$objProcessInstance->getWFProcess()->getWFProcessName()} Process Definition</a>
			</th>
			<th class="output">{*<a href="javascript:toggleClass('process_history');">View Process History</a>*}</th>
			<th class="state_type output">State Type</th>
		</tr>
		<tr>
			<th class="spacer">&nbsp;</th>

			<th class="role">Role</th>
			<th class="user">User</th>
			<th class="comment">Comment</th>
			<th class="action">Actions</th>
			<th class="date">Date</th>
		</tr>

		{foreach from=$objProcessInstance->getProcessTransitionInstanceArray()->getArray() item=objProcessTransitionInstance}
			<tr class="subheader process_history">
				<td class="state output" colspan="5">
					{$objProcessTransitionInstance->getProcessTransition()->getFromProcessState()->getProcessStateName()}
					{if $objProcessTransitionInstance->getProcessTransition()->getFromProcessState()->getDescription()}
						<img src="images/help.png" alt="?" help="{$objProcessTransitionInstance->getProcessTransition()->getFromProcessState()->getDescription()}" />
					{/if}
				</td>
				<td class="state_type old_state output">Old State</td>
			</tr>
			{$blnFirstRole=true}
			{foreach from=$objProcessTransitionInstance->getConditionInstanceArray()->getArray() item=objConditionInstance}
				{$objRole=$objConditionInstance->getRole()}
				{cycle values="altBG0_1,altBG0_0" assign="strBGClass" name="old_states_`$objProcessTransitionInstance->getProcessTransitionInstanceID()`"}
				<tr class="{$strBGClass} process_history">
					<td class="spacer">{if $blnFirstRole}<img src="images/transition.png" alt="" />{else}&nbsp;{/if}{$blnFirstRole=false}</td>
					<td class="role output">
						<a href="{$objRole->getURL()}">{$objRole->getRoleName()}</a>
					</td>
					<td class="user output">
						{$objUser=$objConditionInstance->getUser()}
						<a href="{$objUser->getUserPageURL()}">{$objUser->getDisplayName()}</a><br />
					</td>
					<td class="comment output">{$objConditionInstance->getComment()}</td>
					<td class="action output">{$objProcessTransitionInstance->getProcessTransition()->getTransitionButton()}</td>
					<td class="date output">{formatDate($objConditionInstance->getCompletedDate())}</td>
				</tr>
			{/foreach}
		{foreachelse}
		{/foreach}

		<tr class="subheader">
			<td class="state output" colspan="5">
				{$objProcessInstance->getCurrentProcessState()->getProcessStateName()}
				{if $objProcessInstance->getCurrentProcessState()->getDescription()}
					<img src="images/help.png" alt="?" help="{$objProcessInstance->getCurrentProcessState()->getDescription()}" />
				{/if}
			</td>
			{if $objProcessInstance->getRoleArray()->getArray() && !$objProcessInstance->getCurrentProcessState()->getFinalState()}
				<td class="state_type current_state output">Current State</td>
			{else}
				<td class="state_type final_state output">Final State</td>
			{/if}
		</tr>
		{foreach from=$objProcessInstance->getRoleArray()->getArray() item=objRole}
			{if $objProcessInstance->getCurrentProcessState()->getShowOnlyCurrentUser() && !$objRole->getUserArray()->objectIsSet($smarty.session.objUser->getUserID())}
				{continue}
			{/if}

			{cycle values="altBG0_1,altBG0_0" assign="strBGClass" name="current_state"}
			{$intRowspan=max(1, count($objRole->getUserArray()->getArray()))}
			{$blnFirstUser=true}
			{foreach from=$objRole->getUserArray()->getArray() item=objProcessUser}
				{if $objProcessInstance->getCurrentProcessState()->getShowOnlyCurrentUser()}
					{$intRowspan=1}
				 	{if $objProcessUser->getUserID() != $smarty.session.objUser->getUserID()}
						{continue}
					{/if}
				{/if}
				<tr class="{$strBGClass}">
					{if $blnFirstUser}
						<td class="spacer" rowspan="{$intRowspan}">&nbsp;</td>
						<td class="role output" rowspan="{$intRowspan}">
							<a href="{$objRole->getURL()}">{$objRole->getRoleName()}</a>
						</td>
						{$blnFirstUser=false}
					{/if}
					<td class="user output">
						<a href="{$objProcessUser->getUserPageURL()}">{$objProcessUser->getDisplayName()}</a><br />
					</td>
					{if $objProcessUser->getConditionInstance()->getConditionInstanceID()}
						<td class="comment output">{$objProcessUser->getConditionInstance()->getComment()}</td>
						<td class="action output">{$objProcessUser->getConditionInstance()->getProcessTransitionInstance()->getProcessTransition()->getTransitionButton()}</td>
						<td class="date output">{formatDate($objProcessUser->getConditionInstance()->getCompletedDate())}</td>
					{else}
						<td class="comment">
							{if $smarty.session.objUser->getUserID() == $objProcessUser->getUserID() || $blnOverrideWorkflow}
								<input type="text" id="comments_{$objRole->getRoleID()}_{$objProcessUser->getUserID()}" name="arrComments[{$objRole->getRoleID()}][{$objProcessUser->getUserID()}][txtComment]" />
							{/if}
						</td>
						<td class="actions" colspan="2">
							{foreach from=$objRole->getProcessTransitionArray()->getArray() item=objProcessTransition}
								<input type="button" name="arrConditionInstances[{$objProcessTransition->getProcessTransitionID()}][{$objRole->getRoleID()}][{$objProcessUser->getUserID()}][strTransitionButton]" value="{$objProcessTransition->getTransitionButton()}"
									{if $smarty.session.objUser->getUserID() == $objProcessUser->getUserID() || $blnOverrideWorkflow}
										onclick="workflowAction('{$objProcessInstance->getProcessInstanceID()}', '{$objProcessTransition->getProcessTransitionID()}', '{$objRole->getRoleID()}', '{$objProcessUser->getUserID()}', '{$objProcessInstance->getProcessObject()->getProcessObjectClass()}', '{$objProcessInstance->getProcessObject()->getID()}', $('#comments_{$objRole->getRoleID()}_{$objProcessUser->getUserID()}').val())"
									{else}
										disabled="true"
									{/if}
								/>
							{/foreach}
						</td>
					{/if}
					</td>
				</tr>
			{foreachelse}
				<tr class="{$strBGClass}">
					<td class="spacer">&nbsp;</td>
					<td class="role output">{$objRole->getRoleName()}</td>
					<td class="user user_undefined output">Undefined</td>
					<td class="comment">
						{if $blnOverrideWorkflow}
							<input type="text" id="comments_{$objRole->getRoleID()}_no_user" name="arrComments[{$objRole->getRoleID()}][no_user][txtComment]" />
						{/if}
					</td>
					<td class="actions" colspan="2">
					{foreach from=$objRole->getProcessTransitionArray()->getArray() item=objProcessTransition}
						<input type="button" name="arrConditionInstances[{$objProcessTransition->getProcessTransitionID()}][{$objRole->getRoleID()}][no_user][strTransitionButton]" value="{$objProcessTransition->getTransitionButton()}"
							{if $blnOverrideWorkflow}
								onclick="workflowAction('{$objProcessInstance->getProcessInstanceID()}', '{$objProcessTransition->getProcessTransitionID()}', '{$objRole->getRoleID()}', 'no_user', '{$objProcessInstance->getProcessObject()->getProcessObjectClass()}', '{$objProcessInstance->getProcessObject()->getID()}', $('#comments_{$objRole->getRoleID()}_no_user').val())"
							{else}
								disabled="true"
							{/if}
						/>
					{/foreach}
					</td>
				</tr>
			{/foreach}
		{foreachelse}
			{*<tr class="altBG0_1"><td></td><td class="output" colspan="3">No roles have been defined for this state.</td></tr>*}
		{/foreach}

		{*foreach from=$objProcessInstance->getProcessTransitionArray()->getArray() item=objProcessTransition}
			<tr class="subheader">
				<td class="state output" colspan="3">{$objProcessTransition->getToProcessState()->getProcessStateName()}</td>
				<td class="state_type future_state output">Possible Future State</td>
			</tr>
			<tr class="altBG0_1">
				<td class="spacer">&nbsp;</td>
				<td class="output" colspan="3">
					If
					{$strAndConnector=""}
					{foreach from=$objProcessTransition->getProcessTransitionRequirementArray()->getArray() item=objProcessTransitionRequirement}
						{$strAndConnector}
						{$strAndConnector="<br />and "}
						{$strOrConnector=""}
						{foreach from=$objProcessTransitionRequirement->getProcessRequirement()->getProcessRequirementRoleArray()->getArray() item=objProcessRequirementRole}
							{$strOrConnector} {$objProcessRequirementRole->getRole()->getRoleName()}
							{$strOrConnector=" or "}
						{/foreach}
					{/foreach}
					click {$objProcessTransition->getTransitionButton()}.
				</td>
			</tr>
		{/foreach*}
	</table>

	{*foreach from=$objProcessInstance->getProcessTransitionArray()->getArray() item=objProcessTransition}
		<b>{$objProcessTransition->getTransitionName()} ({$objProcessTransition->getTransitionButton()}) - {$objProcessTransition->getToProcessState()->getProcessStateName()}</b><br />
		{$strAndConnector=""}
		{foreach from=$objProcessTransition->getProcessTransitionRequirementArray()->getArray() item=objProcessTransitionRequirement}
			{$strAndConnector}
			{$strAndConnector=" and "}
			{$strOrConnector=""}
			{foreach from=$objProcessTransitionRequirement->getProcessRequirement()->getProcessRequirementRoleArray()->getArray() item=objProcessRequirementRole}
				{$strOrConnector} {$objProcessRequirementRole->getRole()->getRoleName()}
				{$strOrConnector=" or "}
			{/foreach}
		{/foreach}
		<br />
		<br />
	{/foreach*}
</div>