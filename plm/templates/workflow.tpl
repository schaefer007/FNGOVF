{if !isset($blnEditable)}
	{$blnEditable=0}
{/if}

{if $objWFProcess->getWFProcessID()}
	<a name="process_diagram"></a>
	<div class="heading">
		<div class="text">
			<h3>Diagram</h3>
		</div>
		<div class="clL"></div>
	</div>
	<div class="clL"></div>

	<div style="float:left;">
		Process Path:
	</div>
	<div style="float:left;padding-right:20px;">
		<img src="{$smarty.const.strSITE_URL}images/help.png" alt="?" help="Select a path and the diagram will update, highlighting the primary transitions for that path." />
	</div>
	<div class="process_path">
		<input type="radio" name="intProcessPathID" value="" checked="true" onclick="updateDiagram('', '{$objWFProcess->getWFProcessID()}')" /> Show All
	</div>
	{foreach from=$objWFProcess->getProcessPathArray()->getArray() item=objProcessPath}
		<div class="process_path">
			<input type="radio" name="intProcessPathID" value="{$objProcessPath->getProcessPathID()}" onclick="updateDiagram('{$objProcessPath->getProcessPathID()}', '{$objWFProcess->getWFProcessID()}')" /> {$objProcessPath->getPathName()}
		</div>
	{/foreach}
	<br />

	<img src="{$smarty.const.strSITE_URL}state_diagram.php?intWFProcessID={$objWFProcess->getWFProcessID()}" id="state_diagram" alt="" />
	<br />

	<a name="process_states"></a>
	<div class="heading">
		<div class="text">
			<h3>Process States</h3>
			<img src="{$smarty.const.strSITE_URL}images/help.png" alt="?" help="Process States are the boxes from the diagram above." />
		</div>

		{if $blnEditable}
			<div class="command">
				<a href="javascript:loadProcessState()" class="text_action">
					<div class="icon"><img src="{$smarty.const.strSITE_URL}images/add.png" alt="" title="Add Process State" /></div>
					<div class="text">Add Process State</div>
				</a>
				<div class="clL"></div>
			</div>
		{/if}
		<div class="clL"></div>
	</div>
	<div class="clL"></div>

	<table class="table table_process_states" id="process_states">
		<tr>
			<th>State Name</th>
			<th>Description</th>
			{if $blnEditable}
				<th></th>
				<th></th>
			{/if}
		</tr>
		{foreach from=$objWFProcess->getProcessStateArray()->getArray() item=objProcessState}
			{include file="process_state_line.tpl"}
		{foreachelse}
			<tr class="altBG0_0" id="no_process_states"><td class="output" colspan="3">There are no states for this workflow process.</td></tr>
		{/foreach}
	</table>
	<br />

	<a name="process_transitions"></a>
	<div class="heading">
		<div class="text">
			<h3>Process Transitions</h3>
			<img src="{$smarty.const.strSITE_URL}images/help.png" alt="?" help="Process Transitions are the directed lines from the diagram above. Transitions occur when the the specified <i>Roles</i> take the necessary <i>Actions</i>." />
		</div>

		{if $blnEditable}
			<div class="command">
				<a href="javascript:loadProcessTransition(null, '{$objWFProcess->getWFProcessID()}')" class="text_action">
					<div class="icon"><img src="{$smarty.const.strSITE_URL}images/add.png" alt="" title="Add Process Transition" /></div>
					<div class="text">Add Process Transition</div>
				</a>
				<div class="clL"></div>
			</div>
		{/if}
		<div class="clL"></div>
	</div>
	<div class="clL"></div>

	<li>Process transitions are essentially state changes that occur when a user (or group of users) takes action in the system.</li>
	<li>After each process transition, emails are sent out to those who are required to take the next action.</li>
	<div class="clL"></div>
	<br />

	<table class="table table_process_transitions" id="process_transitions">
		<tr>
			<th>Transition Name</th>
			<th>if Roles <img src="{$smarty.const.strSITE_URL}images/help.png" alt="?" help="These roles are the ones who can take the corresponding action in order to make the transition occur.<br />Note: Not all roles are required, notice the ANDs and ORs between roles." /></th>
			<th>take Action <img src="{$smarty.const.strSITE_URL}images/help.png" alt="?" help="This is the action the user is taking.<br />This text matches the exact text on the button the user clicks to take action." /></th>
			<th>move From State</th>
			<th>To State</th>
			{if $blnEditable}
				<th></th>
				<th></th>
			{/if}
		</tr>
		{foreach from=$objWFProcess->getProcessTransitionArray()->getArray() item=objProcessTransition}
			{$objFromProcessState=$objWFProcess->getProcessStateArray()->getObject($objProcessTransition->getFromProcessStateID())}
			{$objToProcessState=$objWFProcess->getProcessStateArray()->getObject($objProcessTransition->getToProcessStateID())}
			{$intWFProcessID=$objWFProcess->getWFProcessID()}
			{include file="process_transition_line.tpl"}
		{foreachelse}
			<tr class="altBG0_0" id="no_process_transitions"><td class="output" colspan="7">There are no transitions for this workflow process.</td></tr>
		{/foreach}
	</table>
{/if}