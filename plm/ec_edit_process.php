<?php
include_once("common.php");
include_once("classes/EngineeringChange.class.php");
include_once("classes/Product.class.php");

include_once("authenticate.php");

$intEngineeringChangeID = isset($_POST["intEngineeringChangeID"]) ? $_POST["intEngineeringChangeID"] : null;
$objEC = new EngineeringChange($intEngineeringChangeID);
$blnNewEC = false;
if(isset($_POST["intProgramCommonID"])) $objEC->setProgramCommonID($_POST["intProgramCommonID"]);
if(isset($_POST["strCustomerChangeNotice"])) $objEC->setCustomerChangeNotice($_POST["strCustomerChangeNotice"]);
if(isset($_POST["blnInternal"])) $objEC->setInternal($_POST["blnInternal"]);
if(isset($_POST["strSubject"])) $objEC->setSubject(trim($_POST["strSubject"]));
if(isset($_POST["txtDescription"])) $objEC->setDescription(trim($_POST["txtDescription"]));
if(isset($_POST["txtPlantInput"])) $objEC->setPlantInput(trim($_POST["txtPlantInput"]));
if(isset($_POST["txtSalesInput"])) $objEC->setSalesInput(trim($_POST["txtSalesInput"]));
if(isset($_POST["txtFeedback"])) $objEC->setFeedback($_POST["txtFeedback"]);
if(isset($_POST["intPPAPSubmissionLevel"])) $objEC->setPPAPSubmissionLevel($_POST["intPPAPSubmissionLevel"]);
if(isset($_POST["dtmPPAPDate"])) $objEC->setPPAPDate(isset($_POST["dtmPPAPDate"])?formatDateForDB($_POST["dtmPPAPDate"]):null);
if(isset($_POST["dtmDesignReleaseDate"])) $objEC->setDesignReleaseDate(isset($_POST["dtmDesignReleaseDate"])?formatDateForDB($_POST["dtmDesignReleaseDate"]):null);
if(isset($_POST["dtmMaterialAvailabilityDate"])) $objEC->setMaterialAvailabilityDate(isset($_POST["dtmMaterialAvailabilityDate"])?formatDateForDB($_POST["dtmMaterialAvailabilityDate"]):null);
if(isset($_POST["dtmIssueDate"])) $objEC->setIssueDate(isset($_POST["dtmIssueDate"])?formatDateForDB($_POST["dtmIssueDate"]):null);
if(isset($_POST["dtmEffectiveDate"])) $objEC->setEffectiveDate(isset($_POST["dtmEffectiveDate"])?formatDateForDB($_POST["dtmEffectiveDate"]):null);
$objEC->setBankBuildRequired(isset($_POST["blnBankBuildRequired"])?1:0);
if(isset($_POST["strTimeRequired"]) && $objEC->getBankBuildRequired())
	$objEC->setTimeRequired($_POST["strTimeRequired"]);
else
	$objEC->setTimeRequired(null);
$objEC->setServiceAffected(isset($_POST["blnServiceAffected"])?1:0);
$objEC->setRunningChange(isset($_POST["blnRunningChange"])?1:0);
if(!$objEC->getEngineeringChangeID()) {
	$blnNewEC = true;
	$objEC->setCreatedOn(now());
	$objEC->setCreatedByUserID($objSessionUser->getUserID());
}

if(isset($_POST["intChecklistID"]) && $_POST["intChecklistID"]) {
	include_once("classes/ChecklistInstance.class.php");

	$objEC->getChecklistInstance()->loadChecklistInstance($_POST["intChecklistInstanceID"]);
	if(!$objEC->getChecklistInstance()->getChecklistInstanceID()) {
		$objEC->getChecklistInstance()->setChecklistInstanceName($_POST["strChecklistInstanceName"]);
		$objEC->getChecklistInstance()->setChecklistID($_POST["intChecklistID"]);
		$objEC->getChecklistInstance()->setChecklistAnswerGroupID($_POST["intChecklistAnswerGroupID"]);
	}

	if($_POST["arrChecklistGroupInstances"]) {
		foreach($_POST["arrChecklistGroupInstances"] as $intChecklistGroupID => $arrChecklistGroupInstance) {
			if(!$objEC->getChecklistInstance()->getChecklistGroupInstanceArray()->objectIsSet($intChecklistGroupID)) {
				$objChecklistGroupInstance = $objEC->getChecklistInstance()->getChecklistGroupInstanceArray()->createNew($intChecklistGroupID);
				$objChecklistGroupInstance->setChecklistGroupInstanceName($arrChecklistGroupInstance["strChecklistGroupInstanceName"]);
				$objChecklistGroupInstance->setChecklistGroupID($arrChecklistGroupInstance["intChecklistGroupID"]);
			}

			if($arrChecklistGroupInstance["arrChecklistItemInstances"]) {
				foreach($arrChecklistGroupInstance["arrChecklistItemInstances"] as $intChecklistItemID => $arrChecklistItemInstance) {
					if(!$objEC->getChecklistInstance()->getChecklistGroupInstanceArray()->getObject($intChecklistGroupID)->getChecklistItemInstanceArray()->objectIsSet($intChecklistItemID)) {
						$objChecklistItemInstance = $objEC->getChecklistInstance()->getChecklistGroupInstanceArray()->getObject($intChecklistGroupID)->getChecklistItemInstanceArray()->createNew($intChecklistItemID);
						$objChecklistItemInstance->setChecklistItemInstance($arrChecklistItemInstance["strChecklistItemInstance"]);
						$objChecklistItemInstance->setChecklistItemID($arrChecklistItemInstance["intChecklistItemID"]);
					} else {
						$objChecklistItemInstance = $objEC->getChecklistInstance()->getChecklistGroupInstanceArray()->getObject($intChecklistGroupID)->getChecklistItemInstanceArray()->getObject($intChecklistItemID);
					}
					$objChecklistItemInstance->setChecklistAnswerID(isset($arrChecklistItemInstance["intChecklistAnswerID"])?$arrChecklistItemInstance["intChecklistAnswerID"]:null);
					$objChecklistItemInstance->setCompletionDate(formatDateForDB($arrChecklistItemInstance["dtmCompletionDate"]));
					$objChecklistItemInstance->setChampionUserID($arrChecklistItemInstance["intChampionUserID"]?$arrChecklistItemInstance["intChampionUserID"]:null);
					$objChecklistItemInstance->setComments($arrChecklistItemInstance["txtComments"]);
				}
			}
		}
	}
}

// Validate
try {
	$objEC->validate();
} catch(Exception $e) {
	triggerError($e->getMessage());
	$_SESSION["arrFormData"]["objEC"] = $objEC;
	$strURL = $_SERVER["HTTP_REFERER"];
	header("location:$strURL");
	exit();
}
/*
$objEC->getChecklistInstance()->save();
$objEC->getChecklistInstance()->getChecklistGroupInstanceArray()->setAllFields("setChecklistInstanceID", $objEC->getChecklistInstance()->getChecklistInstanceID());
$objEC->getChecklistInstance()->getChecklistGroupInstanceArray()->save();
if($objEC->getChecklistInstance()->getChecklistGroupInstanceArray()->getArray()) {
	foreach($objEC->getChecklistInstance()->getChecklistGroupInstanceArray()->getArray() as $objChecklistGroupInstance) {
		$objChecklistGroupInstance->getChecklistItemInstanceArray()->setAllFields("setChecklistGroupInstanceID", $objChecklistGroupInstance->getChecklistGroupInstanceID());
		$objChecklistGroupInstance->getChecklistItemInstanceArray()->save();
	}
}
$objEC->setChecklistInstanceID($objEC->getChecklistInstance()->getChecklistInstanceID());
*/
$objEC->save();

if(isset($_FILES["arrDocuments"]) && $_FILES["arrDocuments"]["name"]) {
	foreach($_FILES["arrDocuments"]["name"] as $intAttachmentIndex => $arrNames) {
		$strName = $_FILES["arrDocuments"]["name"][$intAttachmentIndex]["strFile"];
		$strTempName = $_FILES["arrDocuments"]["tmp_name"][$intAttachmentIndex]["strFile"];
		$strError = $_FILES["arrDocuments"]["error"][$intAttachmentIndex]["strFile"];
		$intDocumentTypeID = $_POST["arrDocuments"][$intAttachmentIndex]["intDocumentTypeID"];
		$objECDocument = uploadFile($strName, $strTempName, $strError, strFOLDER_ENGINEERING_CHANGE.$objEC->getDocumentFolder()."/", $objEC->getProgram(true)->getProgramCommon(true)->getTopLevelFolder(true), "ECDocument", $objEC->getEngineeringChangeID(), $_POST["intProgramCommonID"], $intDocumentTypeID);
	}
}

// Create Approval Process Instance
if($blnNewEC) {
	$objEC->setECStatus("New");

	$objEC->getProcessInstance()->setWFProcessID(intWFPROCESS_ID_ENGINEERING_CHANGE);
	$objEC->getProcessInstance()->getWFProcess()->load($objEC->getProcessInstance()->getWFProcessID());
	$objEC->getProcessInstance()->setCurrentProcessStateID($objEC->getProcessInstance()->getWFProcess()->getStartingProcessStateID());
	$objEC->getProcessInstance()->save();
	$objEC->setProcessInstanceID($objEC->getProcessInstance()->getProcessInstanceID());

	$objEC->save();

	$_SESSION["arrWorkingData"]["objCurrentEC"] = $objEC;

	// Add top level program to tblProductECXR
	include_once("classes/ProductECXR.class.php");
	$objProductECXR = new ProductECXR();
	$objProductECXR->setProductID($objEC->getProgram(true)->getProductID());
	$objProductECXR->setEngineeringChangeID($objEC->getEngineeringChangeID());
	$objProductECXR->save();
}

/*
// Submit for approval
if(isset($_POST["btnSubmitForApproval"]) && $objEC->getECStatus() == "New") {
	$objEC->getApprovalProcessInstance()->load($objEC->getApprovalProcessInstanceID());
	$objEC->getApprovalProcessInstance()->submit($objEC);
	$objEC->submitApprovalProcess();

	$objEC->save();
}
*/

triggerSuccess("Successfully saved.");
header("location:ec_add.php?intEngineeringChangeID=".$objEC->getEngineeringChangeID());
?>