<?php
	include_once("classes/EngineeringChange.class.php");
	include_once("common.php");

	$strAction = isset($_POST["strAction"])?$_POST["strAction"]:(isset($_GET["strAction"])?$_GET["strAction"]:null);
	$intEngineeringChangeID = isset($_POST["intEngineeringChangeID"])?$_POST["intEngineeringChangeID"]:(isset($_GET["intEngineeringChangeID"])?$_GET["intEngineeringChangeID"]:null);

	switch($strAction){
		case "Submit":
			if(!isset($_SESSION["arrWorkingData"]["objCurrentEC"]))
				break;

			$objEC = $_SESSION["arrWorkingData"]["objCurrentEC"];
			$objEC->getApprovalProcessInstance()->load($objEC->getApprovalProcessInstanceID());
			$objEC->submitApprovalProcess();
			$objEC->getApprovalProcessInstance()->submit($objEC);

			//echo getECSection($objEC);
			break;
		case "View":
		case "Change Editing":
			include_once("classes/EngineeringChange.class.php");
			$objEC = new EngineeringChange($intEngineeringChangeID);
			$_SESSION["arrWorkingData"]["objCurrentEC"] = $objEC;

			//$intProgramID = isset($_GET["intProgramID"])?$_GET["intProgramID"]:null;
			//if($intProgramID) { // This means we are coming from an email notification, not just AJAX from the program.php page
			//	header("location:program.php?intProgramID=".$intProgramID);
			//}
			//echo getECSection($objEC);
			break;
		case "Done Editing":
			if(isset($_SESSION["arrWorkingData"]["objCurrentEC"]))
				unset($_SESSION["arrWorkingData"]["objCurrentEC"]);
			break;
		case "Get Affected Products":
			$intEngineeringChangeID = isset($_POST["intEngineeringChangeID"])?$_POST["intEngineeringChangeID"]:null;
			if(!$intEngineeringChangeID)
				break;

			include_once("classes/Product.class.php");
			$objProductArray = new ProductArray();
			$objProductArray->getECAffectedProducts($intEngineeringChangeID);

			if($objProductArray->getArray()) {
				foreach($objProductArray->getArray() as $objProduct) {
					echo $objProduct->getProductNumber() . "<br />";
				}
			}


			break;
	}

	function getECSection($objEC) {
		include("smarty.php");
		$objEC->getProgram()->getEngineeringChangeArray()->loadByProgramID($objEC->getProgramID());
		$g_objSmarty->assign("objEngineeringChangeArray", $objEC->getProgram()->getEngineeringChangeArray());
		$g_objSmarty->assign("blnEngineeringChangeWrite", Security::hasPermission(intSECURITY_ITEM_ID_ENGINEERING_CHANGE, intOPERATION_ID_WRITE));
		return $g_objSmarty->fetch("ecs.tpl");
	}
?>