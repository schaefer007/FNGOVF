<?php
	include_once("common.php");
	$arrCSS = array();
	$arrJS = array();
	include_once("smarty.php");
	include_once("header.php");
	include_once("classes/User.class.php");
	include_once("classes/Role.class.php");
	include_once("classes/Member.class.php");
	include_once("classes/MemberRole.class.php");

	$strMemberIndex = isset($_POST["strMemberIndex"])?$_POST["strMemberIndex"]:null;
	$strChanged = isset($_POST["strChanged"])?$_POST["strChanged"]:null;
	$intUserID = isset($_POST["intUserID"])?$_POST["intUserID"]:null;
	$arrRoleIDs = isset($_POST["arrRoleIDs"])?$_POST["arrRoleIDs"]:null;

	if($strChanged == "User") {
		$objRoleArray = new RoleArray();
		$objRoleArray->loadActiveRoles($intUserID);

		$objUserArray = new UserArray();
		$objUserArray->loadForMembers();
	} elseif($strChanged == "Role") {
		$objRoleArray = new RoleArray();
		$objRoleArray->loadActiveRoles();

		$objUserArray = new UserArray();
		$objUserArray->loadForMembers($arrRoleIDs);
	} else {
		$objRoleArray = new RoleArray();
		$objRoleArray->loadActiveRoles();

		$objUserArray = new UserArray();
		$objUserArray->loadForMembers();
	}


	$objMember = new Member();
	$objMember->setUserID($intUserID);
	if($arrRoleIDs) {
		foreach($arrRoleIDs as $intRoleID) {
			$objMemberRole = new MemberRole();
			$objMemberRole->setRoleID($intRoleID);
			$objMember->getMemberRoleArray()->addNew($objMemberRole);
		}
	}

	$g_objSmarty->assign("strMemberIndex", $strMemberIndex);
	$g_objSmarty->assign("objMember", $objMember);
	$g_objSmarty->assign("objUserArray", $objUserArray);
	$g_objSmarty->assign("objRoleArray", $objRoleArray);
	$g_objSmarty->assign("arrRoleIDs", $arrRoleIDs);

	$g_objSmarty->display("member_role_edit.tpl");
?>