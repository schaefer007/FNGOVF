<?php
	include_once("common.php");
	$objSessionUser = Security::validateLogin();
	Security::hasPermissions(array(array(intSECURITY_ITEM_ID_ENGINEERING_CHANGE, intOPERATION_ID_READ)));

	$arrCSS = array("table.css.php","process_instance.css.php", "program_common.css.php", "program_header_full.css.php", "ec_add.css.php");
	$arrJS = array("process_instance.js.php", "program_ec.js.php", "ec_add.js.php"); // CODE CLEAN: Using program_ec only for viewEC(). Could put this somewhere common
	$g_strTitle = "Engineering Change";
	include_once("smarty.php");
	include_once("header.php");
	include_once("classes/EngineeringChange.class.php");

	if(!$objSearch->getProgramID()) $objSearch->setProgramID(841);

	$objEC = isset($_SESSION["arrFormData"]["objEC"])?$_SESSION["arrFormData"]["objEC"]:null;
	if($objEC) { // Failed save
		$objEC->setECStatus("New");
	} else {
		$intEngineeringChangeID = isset($_GET["intEngineeringChangeID"])?$_GET["intEngineeringChangeID"]:null;
		$objEC = new EngineeringChange();
		$objEC->loadForECPage($intEngineeringChangeID);
		if(!$objEC->getEngineeringChangeID()) {
			$objProgram = new Product($objSearch->getProgramID());
			$objEC->setProgramCommonID($objProgram->getProgramCommonID());
			$objEC->setECStatus("New");
		}
		/*if(!$objEC->getChecklistInstanceID()) {
			include_once("classes/Checklist.class.php");
			$objChecklist = new Checklist();
			$objChecklist->loadChecklist(intCHECKLIST_ID_ENGINEERING_CHANGE);
			$objEC->getChecklistInstance()->createFromChecklist($objChecklist);
		} else {
			$objEC->getChecklistInstance()->loadChecklistInstance($objEC->getChecklistInstanceID());
		}*/
	}

	$objEC->getProcessInstance()->loadForDisplay($objEC->getProcessInstanceID(), $objEC);

	$g_objSmarty->assign("objEC", $objEC);

	include_once("classes/Product.class.php");
	$objProgramArray = new ProductArray();
	$objProgramArray->loadForECAdd();
	$g_objSmarty->assign("objProgramArray", $objProgramArray);

	include_once("classes/User.class.php");
	$objUserArray = new UserArray();
	$objUserArray->loadProgramMembership($objEC->getProgramCommonID());
	$g_objSmarty->assign("objUserArray", $objUserArray);

	$g_objSmarty->assign("blnEngineeringChangeWrite", Security::hasPermission(intSECURITY_ITEM_ID_ENGINEERING_CHANGE, intOPERATION_ID_WRITE));
	$g_objSmarty->assign("blnEngineeringChangeSubmitForApproval", Security::hasPermission(intSECURITY_ITEM_ID_ENGINEERING_CHANGE, intOPERATION_ID_SUBMIT_FOR_APPROVAL));

	$g_objSmarty->display("ec_add.tpl");
?>
