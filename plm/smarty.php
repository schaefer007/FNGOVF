<?php
	require_once('Smarty/libs/Smarty.class.php');

	if(!defined("TEMPLATE_DIR")) {
		define('TEMPLATE_DIR'   ,'templates');
		define('COMPILE_DIR'    ,'templates_c');
		define('CONFIG_DIR'     ,'configs');
		define('CACHE_DIR'      ,'cache');
	}

	$g_objSmarty = new Smarty;
//    $g_objSmarty->debugging = true;
	$g_objSmarty->template_dir = TEMPLATE_DIR;
	$g_objSmarty->config_dir = CONFIG_DIR;
	$g_objSmarty->cache_dir = CACHE_DIR;
	$g_objSmarty->compile_dir = COMPILE_DIR;
?>
