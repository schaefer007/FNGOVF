<?php
	include_once("constants_server.php");

	define("intAPPROVAL_PROCESS_PROGRAM", 1); // Represents the intApprovalProcessID associated to programs (NJSN)
	define("intAPPROVAL_PROCESS_GATE_1", 2);
	define("intAPPROVAL_PROCESS_GATE_3", 5);
	define("intAPPROVAL_PROCESS_OTHER_GATES", 3);
	define("intAPPROVAL_PROCESS_ENGINEERING_CHANGE", 4);
	define("intAPPROVAL_PROCESS_GATE_MA_HIGH_RISK", 9);
	define("intAPPROVAL_PROCESS_GATE_MA_LOW_RISK", 10);

	define("intWFPROCESS_ID_ENGINEERING_CHANGE", 1);
	define("intWFPROCESS_ID_OPERATIONAL_CHANGE", 2);
	define("intWFPROCESS_ID_CAD_TIME_TRACKING", 3);

	define("intPROCESS_STATE_ID_EC_NEW", 1);
	define("intPROCESS_STATE_ID_EC_SOLICITING_FEEDBACK", 2);
	define("intPROCESS_STATE_ID_EC_WAITING_ON_PROGRAM_CHANGES", 3);
	define("intPROCESS_STATE_ID_EC_PENDING_ACCOUNT_MANAGER_APPROVAL", 4);
	define("intPROCESS_STATE_ID_EC_PENDING_DIRECTOR_OF_SALES_APPROVAL", 14);
	define("intPROCESS_STATE_ID_EC_APPROVED", 5);
	define("intPROCESS_STATE_ID_EC_REJECTED", 6);

	define("strPROCESS_STATE_EC_NEW", "Request");
	define("strPROCESS_STATE_EC_SOLICITING_FEEDBACK", "Soliciting Feedback");
	define("strPROCESS_STATE_EC_WAITING_ON_PROGRAM_CHANGES", "Waiting on Program Changes");
	define("strPROCESS_STATE_EC_PENDING_ACCOUNT_MANAGER_APPROVAL", "Pending Account Manager Approval");
	define("strPROCESS_STATE_EC_PENDING_DIRECTOR_OF_SALES_APPROVAL", "Pending Sales Director Approval");
	define("strPROCESS_STATE_EC_APPROVED", "Approved");
	define("strPROCESS_STATE_EC_REJECTED", "Rejected");

	define("intPROCESS_TRANSITION_ID_EC_SKIP_FEEDBACK_SUBMIT_FOR_APPROVAL", 22);
	define("intPROCESS_TRANSITION_ID_EC_SOLICIT_FEEDBACK", 1);
	define("intPROCESS_TRANSITION_ID_EC_APPROVE_FEEDBACK", 2);
	define("intPROCESS_TRANSITION_ID_EC_REQUEST_RESUBMIT_FEEDBACK", 7);
	define("intPROCESS_TRANSITION_ID_EC_REJECT_FEEDBACK", 5);
	define("intPROCESS_TRANSITION_ID_EC_SUBMIT_PROGRAM_CHANGES", 3);
	define("intPROCESS_TRANSITION_ID_EC_ACCOUNT_MANAGER_APPROVE", 4);
	define("intPROCESS_TRANSITION_ID_EC_ACCOUNT_MANAGER_REQUEST_RESUBMIT", 8);
	define("intPROCESS_TRANSITION_ID_EC_ACCOUNT_MANAGER_REJECT", 6);
	define("intPROCESS_TRANSITION_ID_EC_DIRECTOR_OF_SALES_APPROVE", 16);
	define("intPROCESS_TRANSITION_ID_EC_DIRECTOR_OF_SALES_REQUEST_RESUBMIT", 17);
	define("intPROCESS_TRANSITION_ID_EC_DIRECTOR_OF_SALES_REJECT", 18);
	define("intPROCESS_TRANSITION_ID_SUBMIT_TIMESHEET", 13);
	define("intPROCESS_TRANSITION_ID_APPROVE_TIMESHEET", 14);
	define("intPROCESS_TRANSITION_ID_REQUEST_RESUBMIT_TIMESHEET", 15);
	define("intPROCESS_TRANSITION_ID_UNAPPROVE_TIMESHEET", 21);

	define("intSECURITY_ITEM_ID_PROGRAM", 1);
	define("intSECURITY_ITEM_ID_DOCUMENT", 2);
	define("intSECURITY_ITEM_ID_MILESTONE", 3);
	define("intSECURITY_ITEM_ID_ISSUE", 4);
	define("intSECURITY_ITEM_ID_USER", 6);
	define("intSECURITY_ITEM_ID_ROLE", 7);
	define("intSECURITY_ITEM_ID_PERMISSION", 8);
	define("intSECURITY_ITEM_ID_SECURITY_ITEM", 9);
	define("intSECURITY_ITEM_ID_TOOL", 10);
	define("intSECURITY_ITEM_ID_NOTIFICATION", 11);
	define("intSECURITY_ITEM_ID_PRODUCT", 12);
	define("intSECURITY_ITEM_ID_ENGINEERING_CHANGE", 13);
	define("intSECURITY_ITEM_ID_CONTAINER", 15);
	define("intSECURITY_ITEM_ID_FINANCIAL", 16);
	define("intSECURITY_ITEM_ID_CAPACITY_PLANNING", 17);
	define("intSECURITY_ITEM_ID_CUSTOMER", 18);
	define("intSECURITY_ITEM_ID_SHIP_TO_FACILITY", 19);
	define("intSECURITY_ITEM_ID_PLATFORM", 20);
	define("intSECURITY_ITEM_ID_CRONJOBS", 21);
	define("intSECURITY_ITEM_ID_PURCHASE_ORDER", 22);
	define("intSECURITY_ITEM_ID_PRODUCTION_FACILITY", 23);
	define("intSECURITY_ITEM_ID_OPERATIONAL_CHANGE", 24);
	define("intSECURITY_ITEM_ID_TIME_TRACKING", 25);
	define("intSECURITY_ITEM_ID_PRESS", 26);
	define("intSECURITY_ITEM_ID_WORKFLOW", 27);
	define("intSECURITY_ITEM_ID_PLATING_LINE", 28);
	define("intSECURITY_ITEM_ID_PAINTING_LINE", 29);
	define("intSECURITY_ITEM_ID_LESSONS_LEARNED", 30);
	define("intSECURITY_ITEM_ID_CHECKLIST", 31);
	define("intSECURITY_ITEM_ID_SUPPLIER", 32);
	define("intSECURITY_ITEM_ID_SCENARIO", 33);
	define("intSECURITY_ITEM_ID_DEV_TOOLS", 34);
	define("intSECURITY_ITEM_ID_BUDGET_ENGINEERING", 35);
	define("intSECURITY_ITEM_ID_BUDGET_TOOLING", 36);
	define("intSECURITY_ITEM_ID_BUDGET_CAPITAL_EQUIPMENT", 37);
	define("intSECURITY_ITEM_ID_BUDGET_CAPITAL_PACKAGING", 38);

	define("intOPERATION_ID_READ", 1);
	define("intOPERATION_ID_WRITE", 2);
	define("intOPERATION_ID_WRITE_CREATED", 3);
	define("intOPERATION_ID_SUBMIT_FOR_APPROVAL", 4);
	define("intOPERATION_ID_OVERRIDE_APPROVAL", 5);
	define("intOPERATION_ID_ADMINISTRATE", 7);
	define("intOPERATION_ID_PURCHASING", 10);

	define("intROLE_ID_SYSTEM_ADMINISTRATOR", 1);
	define("intROLE_ID_PROGRAM_MANAGER", 2);
	define("intROLE_ID_DIRECTOR_OF_ENGINEERING", 3);
	define("intROLE_ID_ACCOUNT_MANAGER", 4);
	define("intROLE_ID_DIRECTOR_OF_SALES", 5);
	define("intROLE_ID_PLANT_MANAGER", 6);
	define("intROLE_ID_SENIOR_PROGRAM_MANAGER", 7);
	define("intROLE_ID_ENGINEERING_MANAGER", 8);
	define("intROLE_ID_SALES_COORDINATOR", 9);
	define("intROLE_ID_DIRECTOR_OF_TOOLING", 10);
	define("intROLE_ID_TOOLING_MANAGER", 11);
	define("intROLE_ID_CAD_MANAGER", 12);
	define("intROLE_ID_CAD_DESIGN_LEADER", 13);
	define("intROLE_ID_CAD_DESIGNER", 14);
	define("intROLE_ID_ESTIMATING_ENGINEER", 15);
	define("intROLE_ID_COST_ESTIMATING", 16);
	define("intROLE_ID_DIRECTORY_OF_QUALITY", 17);
	define("intROLE_ID_QUALITY_ENGINEER", 18);
	define("intROLE_ID_RESOURCE_PLANNING_MANAGER", 19);
	define("intROLE_ID_VP_OF_PURCHASING", 20);
	define("intROLE_ID_DIRECTOR_OF_PURCHASING", 21);
	define("intROLE_ID_PURCHASING", 22);
	define("intROLE_ID_MANUFACTURING_MANAGER", 23);
	define("intROLE_ID_MANUFACTURING_ENGINEER", 24);
	define("intROLE_ID_PACKAGING_ENGINEER", 25);
	define("intROLE_ID_SCHEDULER", 26);
	define("intROLE_ID_MATERIALS_MANAGER", 27);
	define("intROLE_ID_TOOLING_ENGINEER", 30);
	define("intROLE_ID_SECONDARY_ENGINEER", 33);
	define("intROLE_ID_GENERAL_MANAGER", 36);
	define("intROLE_ID_APQP_COORDINATOR", 50);



	define("intPROGRAM_TYPE_ID_MECHANICAL_ASSEMBLY", 3);

	define("intPRODUCT_TYPE_ID_ASSEMBLY", 2);
	define("intPRODUCT_TYPE_ID_PLASTIC_PART", 3);
	define("intPRODUCT_TYPE_ID_PURCHASED_PART", 4);
	define("intPRODUCT_TYPE_ID_RESIN", 7);
	define("intPRODUCT_TYPE_ID_METAL_PART", 8);
	define("intPRODUCT_TYPE_ID_PAINT", 9);
	define("intPRODUCT_TYPE_ID_COIL", 10);

	define("intMILESTONE_ID_GATE_1", 1);
	define("intMILESTONE_ID_GATE_2", 2);
	define("intMILESTONE_ID_TOOL_RELEASE", 4);
	define("intMILESTONE_ID_1ST_PARTS_TRIAL", 5);
	define("intMILESTONE_ID_GATE_3", 6);
	define("intMILESTONE_ID_BUILD_EVENT_1", 7);
	define("intMILESTONE_ID_GATE_4", 8);
	define("intMILESTONE_ID_PPAP", 9);
	define("intMILESTONE_ID_BUILD_EVENT_2", 10);
	define("intMILESTONE_ID_BUILD_EVENT_3", 11);
	define("intMILESTONE_ID_GATE_5", 14);
	define("intMILESTONE_ID_PKG_GATE_1", 15);
	define("intMILESTONE_ID_PKG_GATE_2", 16);
	define("intMILESTONE_ID_PKG_GATE_3", 17);
	define("intMILESTONE_ID_PKG_GATE_4", 18);
	define("intMILESTONE_ID_TOOL_INVOICE", 19);
	define("intMILESTONE_ID_PROTOTYPE_START", 20);
	define("intMILESTONE_ID_DV_START", 21);
	define("intMILESTONE_ID_CAPITAL_RELEASE", 22);
	define("intMILESTONE_ID_PV_START", 23);
	define("intMILESTONE_ID_GATE_1_MA", 24);
	define("intMILESTONE_ID_GATE_2_MA", 25);
	define("intMILESTONE_ID_GATE_3_MA", 26);
	define("intMILESTONE_ID_GATE_4_MA", 27);
	define("intMILESTONE_ID_GATE_5_MA", 28);

	define("intSUPPLIER_ID_TBD", 69);

	define("intTOOL_CATEGORY_ID_MOLD", 1);
	define("intTOOL_CATEGORY_ID_DIE", 4);

	define("intNOTIFICATION_TYPE_ID_NJSN", 1);
	define("intNOTIFICATION_TYPE_ID_GATE", 2);
	define("intNOTIFICATION_TYPE_ID_ENGINEERING_CHANGE", 3);
	define("intNOTIFICATION_TYPE_ID_ISSUE", 4);

	define("intREPORT_ID_CAPACITY_PLANNING", 1);

	define("intTABLE_ID_PROGRAM_COMMON", 1);
	define("intTABLE_ID_PRODUCT_XR", 2);
	define("intTABLE_ID_PROCESS", 3);
	define("intTABLE_ID_TOOL", 4);
	define("intTABLE_ID_PROGRAM_PRODUCT", 5);
	define("intTABLE_ID_PRODUCT", 6);
	define("intTABLE_ID_PRODUCT_TOOL_XR", 7);
	define("intTABLE_ID_PROCESS_TOOL", 8);
	define("intTABLE_ID_PROCESS_PRODUCT", 9);
	define("intTABLE_ID_BUDGET_COST", 10);
	define("intTABLE_ID_PART", 11);
	define("intTABLE_ID_PLATFORM_PART_XR", 12);
	define("intTABLE_ID_MACHINE_SCHEDULE", 13);
	define("intTABLE_ID_MACHINE", 17);
	define("intTABLE_ID_PLATFORM_VOLUME", 18);
	define("intTABLE_ID_PRODUCTION_FACILITY_MACHINE_TYPE", 19);
	define("intTABLE_ID_MACHINE_SCHEDULE_DATE_RANGE", 20);

	define("intCOLUMN_ID_MODEL_YEAR", 1);
	define("intCOLUMN_ID_LAST_MODEL_YEAR", 2);
	define("intCOLUMN_ID_PRODUCT_NUMBER", 17);
	define("intCOLUMN_ID_PRODUCT_NAME", 18);
	define("intCOLUMN_ID_QUANTITY", 3);
	define("intCOLUMN_ID_VOLUME", 29);
	define("intCOLUMN_ID_PIECE_PRICE", 13);
	define("intCOLUMN_ID_PRODUCT_SUPPLIER", 14);
	define("intCOLUMN_ID_PRODUCTION_FACILITY_ID", 15);
	define("intCOLUMN_ID_SHIP_TO_FACILITY_ID", 16);
	define("intCOLUMN_ID_INTERNAL_OPERATORS", 7);
	define("intCOLUMN_ID_CUSTOMER_OPERATORS", 6);
	define("intCOLUMN_ID_ACTUAL_OPERATORS", 4);
	define("intCOLUMN_ID_INTERNAL_CYCLE_TIME", 9);
	define("intCOLUMN_ID_CUSTOMER_CYCLE_TIME", 8);
	define("intCOLUMN_ID_ACTUAL_CYCLE_TIME", 5);
	define("intCOLUMN_ID_TOOL_NUMBER", 21);
	define("intCOLUMN_ID_TOOL_NAME", 22);
	define("intCOLUMN_ID_TOOL_TYPE", 23);
	define("intCOLUMN_ID_TOOL_SUPPLIER", 24);
	define("intCOLUMN_ID_TOOL_QUANTITY", 25);
	define("intCOLUMN_ID_PART_PIECES_PER_VEHICLE", 30);
	define("intCOLUMN_ID_PART_PIECES_PER_CYCLE", 31);
	define("intCOLUMN_ID_PART_CYCLES_PER_HOUR", 32);
	define("intCOLUMN_ID_PART_NUMBER_OF_ROUNDS", 33);
	define("intCOLUMN_ID_PART_AVERAGE_SETUP_HOURS", 34);
	define("intCOLUMN_ID_PART_WEEKLY_SETUP_FREQUENCY", 35);
	define("intCOLUMN_ID_PART_QUALITY", 36);
	define("intCOLUMN_ID_INTERNAL_TONNAGE", 10);
	define("intCOLUMN_ID_CUSTOMER_TONNAGE", 11);
	define("intCOLUMN_ID_ACTUAL_TONNAGE", 12);
	define("intCOLUMN_ID_PRODUCT_TOOL_XR_PRODUCT_ID", 20);
	define("intCOLUMN_ID_PART_CAVITIES", 19);
	define("intCOLUMN_ID_BUDGET_COST_DESCRIPTION", 26);
	define("intCOLUMN_ID_REVENUE", 27);
	define("intCOLUMN_ID_COST", 28);

	define("intTIME_TYPE_ID_PROGRAM", 1);

	define("intCUSTOMER_ID_FORD", 2);

	define("intCHECKLIST_ID_OPERATIONAL_CHANGE", 1);
	define("intCHECKLIST_ID_ENGINEERING_CHANGE", 2);

	define("intDOCUMENT_TYPE_ID_PART_PRINT", 13);
	define("intDOCUMENT_TYPE_ID_ISOMETRIC", 14);
	define("intDOCUMENT_TYPE_ID_TOOL_PHOTO", 15);
	define("intDOCUMENT_TYPE_ID_TOOL_TAG_PHOTO", 16);

	define("intCORPORATE_LEVEL_ONLY", 1);
	define("intPLANT_LEVEL_ONLY", 2);

	define("strTRACK_CHANGES_SCENARIO", "Scenario");
	define("intCHANGE_STATUS_ID_ADDED", 1);

	define("intUOM_TYPE_ID_LENGTH", 2);
	define("intUOM_TYPE_ID_WEIGHT", 4);

	define("intMACHINE_TYPE_ID_PRESS", 1);
	define("intMACHINE_TYPE_ID_PLATING_LINE", 2);
	define("intMACHINE_TYPE_ID_PAINTING_LINE", 3);

	define("strMILESTONE_STATUS_SYSTEM", "System Managed");
	define("strMILESTONE_STATUS_USER", "User Managed");
	define("strMILESTONE_STATUS_NA", "N/A");

	define("intBINARY_READ", 001);

	define("intBINARY_WRITE", 010);
	define("intBINARY_DELETE", 100);

	define("intCONTAINER_FOLDER_ID", 1844);
	define("strCONTAINER_FOLDER", "containers/");

	define("strDOCUMENT_FOLDER", "documents/");
	define("strBOM_FOLDER", "BOM/");
	define("strPRINT_FOLDER", "Prints & Drawings/");
	define("strFOLDER_ENGINEERING_CHANGE", "Engineering Changes/");
	define("strFOLDER_GATES", "Gates/");
	define("strFOLDER_OPEN_ISSUES", "Open Issues/");
	define("strFOLDER_TOOLING", "Tooling/");
	define("strFOLDER_LESSONS_LEARNED", "Tooling/");

	define("strDOCUMENTS_FOLDER", "E:/Documents/");
	define("strTEMPLATES_FOLDER", strDOCUMENTS_FOLDER."Templates/");
	define("strFORMS_FOLDER", strDOCUMENTS_FOLDER."Forms/");

	define("strFOLDER_CHARACTERS", "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890,.-_+=()!@#$%^&';{}[]`~ ");

	define("strERROR_UNEXPECTED", "An unexpected error has occured.");
	define("strERROR_NO_ACCESS", "You do not have access to this page.");

	define("intFIRST_MILESTONE_YEAR", 2011);
	define("intFIRST_TIME_TRACKING_YEAR", 2012);
	define("intFIRST_CAPACITY_PLANNING_YEAR", 2012);

	define("strREPORT_FOLDER", "reports/");
	define("strREPORT_FOLDER_NJSN", strREPORT_FOLDER."NJSN/");
	define("strREPORT_FOLDER_GATE", strREPORT_FOLDER."Gates/");

	$arrExtensionsWithIcons = array("xlsx", "xls", "docx", "doc", "ppt", "pptx", "txt", "jpg", "gif", "png", "bmp", "pdf", "mpp", "msg", "zip", "htm", "html", "igs");

	$arrDefaultRoles = array(
		intROLE_ID_DIRECTOR_OF_ENGINEERING,
		intROLE_ID_ENGINEERING_MANAGER,
		intROLE_ID_SENIOR_PROGRAM_MANAGER,
		intROLE_ID_PROGRAM_MANAGER,
		intROLE_ID_DIRECTOR_OF_SALES,
		intROLE_ID_ACCOUNT_MANAGER,
	);

	$arrProgramStatuses = array(
		"Unannounced",
		"Pending Approval",
		"Engineering Development",
		"Production",
		"Service",
		"Obsolete"
	);// Program Management Program Statuses, excludes Opportunity for now.
	$arrAllProgramStatuses = array(
		"Opportunity",
		"Unannounced",
		"Pending Approval",
		"Engineering Development",
		"Production",
		"Service",
		"Obsolete"
	);

	$arrPOStatuses = array("Request", "Approved");
	$arrProgramRisks = array("Low Risk", "Medium Risk", "High Risk");
	$arrIssueStatuses = array("Open", "Closed", "N/A");
	$arrIssuePriorities = array(
		"Low" => array("Name" => "Low", "CSSClass" => ""),
		"Medium" => array("Name" => "Medium", "CSSClass" => "upcoming"),
		"High" => array("Name" => "High", "CSSClass" => "overdue")
	);
	$arrToolTypes = array("Production", "Prototype");
	$arrToolShots = array("Standard Injection", "2-shot", "3-shot", "4-shot");
	$arrMeetingStatuses = array("Waiting Submittal", "Pending Approval", "Approved");
	$arrUserStatuses = array("Active", "Disabled");

	$arrGatesThatDesignFreeze = array(
		intMILESTONE_ID_GATE_2,
		intMILESTONE_ID_GATE_3,
		intMILESTONE_ID_GATE_4,
		intMILESTONE_ID_GATE_5
	);
	$arrPMGates = array(
		intMILESTONE_ID_GATE_1,
		intMILESTONE_ID_GATE_2,
		intMILESTONE_ID_GATE_3,
		intMILESTONE_ID_GATE_4,
		intMILESTONE_ID_GATE_5
	);
	$arrMAGates = array(
		intMILESTONE_ID_GATE_1_MA,
		intMILESTONE_ID_GATE_2_MA,
		intMILESTONE_ID_GATE_3_MA,
		intMILESTONE_ID_GATE_4_MA,
		intMILESTONE_ID_GATE_5_MA
	);
	$arrMAMilestones = array(
		intMILESTONE_ID_GATE_1_MA,
		intMILESTONE_ID_GATE_2_MA,
		intMILESTONE_ID_GATE_3_MA,
		intMILESTONE_ID_GATE_4_MA,
		intMILESTONE_ID_GATE_5_MA,
		intMILESTONE_ID_PROTOTYPE_START,
		intMILESTONE_ID_DV_START,
		intMILESTONE_ID_CAPITAL_RELEASE,
		intMILESTONE_ID_PV_START,
	);
	$arrPackagingGates = array(
		intMILESTONE_ID_PKG_GATE_1,
		intMILESTONE_ID_PKG_GATE_2,
		intMILESTONE_ID_PKG_GATE_3,
		intMILESTONE_ID_PKG_GATE_4,
	);

	$arrContainerOwners = array("Flex-N-Gate", "Customer");
	$arrContainerMaintenances = array("Flex-N-Gate", "Customer", "Outside Maintenance");

	$arrPressSubTypes = array("Automatic", "CFT", "DFT", "DFT/CFT", "DFT/RDFT", "Hydroforming", "MFT", "Prog", "RDFT", "RDFT/CFT", "RFT", "Tandem");
	$arrPlatformStatuses = array("Current Model", "Future Model", "Past Model", "Aftermarket");

	$arrDaysOfWeek = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");

	$arrDocumentClasses = array(
		array("strDocumentClass" => "Issue", "strDocumentPrefix" => "Issue"),
		array("strDocumentClass" => "EngineeringChange", "strDocumentPrefix" => "EC"),
		array("strDocumentClass" => "Container", "strDocumentPrefix" => "Container"),
		array("strDocumentClass" => "OperationalChange", "strDocumentPrefix" => "OC")
	);

	define("dtmDEFAULT_DATE_DESIGN_RELEASE", date("m/d/Y", mktime(0,0,0,date("n")+12,date("j"),date("Y"))));
	define("dtmDEFAULT_DATE_1ST_BUILD_MRD", date("m/d/Y", mktime(0,0,0,date("n")+18,date("j"),date("Y"))));
	define("dtmDEFAULT_DATE_PPAP", date("m/d/Y", mktime(0,0,0,date("n")+24,date("j"),date("Y"))));
	define("dtmDEFAULT_DATE_CUSTOMER_SOP", date("m/d/Y", mktime(0,0,0,date("n")+30,date("j"),date("Y"))));

	$arrToolManageMilestoneIDs = array(intMILESTONE_ID_TOOL_RELEASE,intMILESTONE_ID_1ST_PARTS_TRIAL,intMILESTONE_ID_BUILD_EVENT_1,intMILESTONE_ID_PPAP,intMILESTONE_ID_BUILD_EVENT_2,intMILESTONE_ID_BUILD_EVENT_3);
?>