<?php
	include_once("common.php");
	include_once("classes/Product.class.php");
	include_once("classes/Member.class.php");

	include_once("authenticate.php");

	$strAction = isset($_GET["strAction"])?$_GET["strAction"]:(isset($_POST["strAction"])?$_POST["strAction"]:null);
	switch($strAction){
		case "Add":
			$intProgramID = isset($_GET["intProgramID"])?$_GET["intProgramID"]:null;
			$objProgram = new Product($intProgramID);
			if(!$objProgram->getProgramID()) {
				triggerError("There was an error adding members, an invalid program was specified.");
				header("location:programs_manage.php");
				exit();
			}

			if(isset($_GET["arrNewMembers"])) {
				foreach($_GET["arrNewMembers"] as $intUserID) {
					$objMember = new Member();
					$objMember->loadByProgramIDAndUserID($intProgramID, $intUserID);
					$objMember->setProgramID($intProgramID);
					$objMember->setUserID($intUserID);
					$objProgram->getMemberArray()->addNew($objMember);
					$objProgram->getMemberArray()->save();
				}
			}

			triggerSuccess("Program members successfully saved.");
			header("location:members_manage.php?intProgramID=".$intProgramID);
			exit();
			break;

		case "Delete":
			$intMemberID = isset($_POST["intMemberID"])?$_POST["intMemberID"]:null;
			$objMember = new Member($intMemberID);
			$objMember->delete();
			break;
	}
?>
