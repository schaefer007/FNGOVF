<?php
include_once("common.php");
include_once("classes/ProcessTransition.class.php");
include_once("classes/ConditionXR.class.php");
include_once("classes/PopupReturn.class.php");

include_once("authenticate.php");

// Load original data
$arrProcessTransition = $_POST["arrProcessTransition"];
$intProcessTransitionID = isset($arrProcessTransition["intProcessTransitionID"])?$arrProcessTransition["intProcessTransitionID"]:null;
$objProcessTransition = new ProcessTransition($intProcessTransitionID);
$objProcessTransition->setVarsFromRow($arrProcessTransition);

$arrConditionXRs = isset($_POST["arrConditions"])?$_POST["arrConditions"]:null;
$objConditionXRArray = new ConditionXRArray();
if($arrConditionXRs) {
	foreach($arrConditionXRs as $arrConditionXR) {
		$objConditionXR = $objConditionXRArray->createNew();
		$objConditionXR->setVarsFromRow($arrConditionXR);
		$objConditionXR->setTopLevelConditionID($_POST["intTopLevelConditionID"]);

		if($arrConditionXR["strConditionType"] == "Operation") {
			$objConditionXR->getChildCondition()->setOperation($arrConditionXR["strOperation"]);
		} else {
			$objConditionXR->getChildCondition()->load($arrConditionXR["intChildConditionID"]);
		}
	}
}

// Validate data
$arrErrors = $objProcessTransition->validate();
if($arrErrors) {
	$objPopupReturn = new PopupReturn();
	$objPopupReturn->setStatus("Failure");
	$objPopupReturn->setError(implode("\n", $arrErrors));
	echo $objPopupReturn->toJSON();
	exit();
}

$objProcessTransition->save();
foreach($objConditionXRArray->getArray() as $objConditionXR) {
	$objConditionXR->getChildCondition()->save();
	$objConditionXR->setChildConditionID($objConditionXR->getChildCondition()->getConditionID());
	$objConditionXR->save();
}

// Return JSON
include("smarty.php");

$g_objSmarty->assign("objProcessTransition", $objProcessTransition);
$objProcessTransition->getFromProcessState()->load($objProcessTransition->getFromProcessStateID());
$g_objSmarty->assign("objFromProcessState", $objProcessTransition->getFromProcessState());
$objProcessTransition->getToProcessState()->load($objProcessTransition->getToProcessStateID());
$g_objSmarty->assign("objToProcessState", $objProcessTransition->getToProcessState());
$g_objSmarty->assign("intWFProcessID", $_POST["intWFProcessID"]);
$g_objSmarty->assign("blnEditable", 1);

$objPopupReturn = new PopupReturn();
$objPopupReturn->setStatus("Success");
if($arrProcessTransition["intProcessTransitionID"]) { // Editing existing tool
	$arrChanges[0]["strSelector"] = "#process_transition_".$arrProcessTransition["intProcessTransitionID"];
	$arrChanges[0]["strChangeType"] = "Modification";
} else { // Addition
	$arrChanges[0]["strSelector"] = "#process_transitions";
	$arrChanges[0]["strChangeType"] = "Addition";
}
$arrChanges[0]["txtHTML"] = $g_objSmarty->fetch("process_transition_line.tpl");
$objPopupReturn->setChanges($arrChanges);

echo $objPopupReturn->toJSON();
?>

