<?php
	// TODO: Remove this file over time, it is deprecated, use Security::validateLogin() and Security::hasPermissions()

	$objSessionUser = isset($_SESSION["objUser"])?$_SESSION["objUser"]:new User();
	if(!$objSessionUser->getUserID()) {
		triggerError("You must be logged in to access this page."); // TODO: Throw this error instead, this will fix how this is used in program_add_process.php
		header("location:index.php?strRequestURI=".$_SERVER["REQUEST_URI"]);
		exit();
	}

	// Automatically check for read permission of security item
	$intSecurityItemID = isset($intSecurityItemID)?$intSecurityItemID:null;
	if($intSecurityItemID) {
		if(!Security::hasPermission($intSecurityItemID, intOPERATION_ID_READ)) {
			triggerError("You do not have access to this page.");
			header("location:index.php");
			exit();
		}
	}
?>