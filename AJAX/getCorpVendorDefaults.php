<?php
// Determine the name of the currently running script.
$thisScriptPath = __FILE__;
$scriptElements = explode('/',$thisScriptPath);
$scriptName = $scriptElements[count($scriptElements)-1];
//error_log('running getCorpVendorDefault');
// initialize values that need defaults.
$value = null;
$keyChange = null;
$parms = array();
// retrieve request array and load fields appropriately
foreach($_REQUEST as $key=>$data) {
	switch ($key) {
		case 'table' 		: 	$table = $data;
								break;
		case 'column'		:	$column = $data;
								break;
		case 'value'		:	$value = $data;
								break;
		case 'keyChange'	: 	$keyChange = $data;
								break;
		case 'AJAXFunction'	:	$AJAXFunction = $data.'.php';
								break;
		case 'sessionObj'   :   $sessionObj = $data;
								break;
		default				:	$$key = $data;
								$parms[$key] = $data;
								break;
	}
}
$responseData[] = array();
$termCode = '';
$termDesc = '';
$minority = '';
//error_log('retrieving defaults for corp vendor '.$corpVendor);
$CorpVend = new vmcrvncd();
$CorpVend->select(array('CRVNMINR','CRVNTERM'),array("WHERE"=>array("CORPVEND"=>$corpVendor)));
if ($cvRow = $CorpVend->getnext()) {
	$minority = $cvRow['CRVNMINR'];
	$termCode = $cvRow['CRVNTERM'];
	unset($CorpVend);
	// Added retrieval of CRVNTERM above.  This now replaces the code that retrieved it from vmtermcd and
	//   vmtrmdbs.
	//$vmTermCd = new vmtermcd();
	//$vmTermCd->select(array('VM3TRMCD'),array('WHERE'=>array('VM3CVEND'=>$corpVendor)));
	//if ($dftTermRow = $vmTermCd->getnext()) {
	//	$termCode = $dftTermRow['VM3TRMCD'];
	//}
	//unset($vmTermCd);
	//$vmTrmDbs = new vmtrmdbs();
	//$vmTrmDbs->select(array('VM8TRMCD'),array('WHERE'=>array('VM8CVEND'=>$corpVendor,'VM8DATAB'=>$schema)));
	//if ($vmTrmDbsRow = $vmTrmDbs->getnext()) {
	//	$termCode = $vmTrmDbsRow['VM8TRMCD'];
	//}
	//unset($vmTrmDbs);
	if (!empty($termCode)) {
		$vmTrmCod = new vmtrmcod();
		$vmTrmCod->select(array('VMCTRMD'),array('WHERE'=>array('VMCTRMCD'=>$termCode)));
		if ($vmTrmCodRow = $vmTrmCod->getnext()) {
			$termDesc = $vmTrmCodRow['VMCTRMD'];
		}
		unset($vmTrmCod);
	}
	//error_log('termCode is '.$termCode);
	//error_log('termDesc is '.$termDesc);
	//error_log('minority is '.$minority);
	$responseData[] = array('termCode'=>$termCode,'termDesc'=>$termDesc,'minority'=>$minority);
}
$response = $_GET["callback"]."(".json_encode($responseData).")";
header('Content-type: application/json');
echo $response;
unset($CorpVend);
?>