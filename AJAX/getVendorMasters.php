<?php
// Determine the name of the currently running script.
$thisScriptPath = __FILE__;
$scriptElements = explode('/',$thisScriptPath);
$scriptName = $scriptElements[count($scriptElements)-1];

// initialize values that need defaults.
$value = null;
$keyChange = null;
$parms = array();
function setFacility($facility) {
	if (isset($facility)) {
		error_log('facility is '.$facility);
		$_SESSION[APPLICATION]['FACILITY'] = $facility;
		$FacilityArray = explode(':',$facility);
		$_SESSION[APPLICATION]['dbCode'] = $FacilityArray[0];
		if (trim($FacilityArray[0]) == 'CMSDATMX') {
			$_SESSION[APPLICATION]['dbSchema'] = 'CMSDAT';
		} else {
			$_SESSION[APPLICATION]['dbSchema'] = $FacilityArray[0];
		}
		$_SESSION[APPLICATION]['plant'] = $FacilityArray[1];
		$_SESSION[APPLICATION]['company'] = $FacilityArray[2];
		$fwCompId = new fwcompid();
		$fwCompId->select(array('FWFUT02'),array('WHERE'=>array('FWDATC'=>$FacilityArray[0],'FWPLTC'=>$FacilityArray[1],'FWGLCO'=>$FacilityArray[2])));
		if ($fwCompRow = $fwCompId->getnext()) {
			$_SESSION[APPLICATION]['dbName'] = $fwCompRow['FWFUT02'];
		}
	}
}
// retrieve request array and load fields appropriately
foreach($_REQUEST as $key=>$data) {
	switch ($key) {
		case 'table' 		: 	$table = $data;
								break;
		case 'column'		:	$column = $data;
								break;
		case 'value'		:	$value = $data;
								break;
		case 'keyChange'	: 	$keyChange = $data;
								break;
		case 'AJAXFunction'	:	$AJAXFunction = $data.'.php';
								break;
		case 'sessionObj'   :   $sessionObj = $data;
								break;
		default				:	$$key = $data;
								$parms[$key] = $data;
								break;
	}
}
$vendors = array();
if (isset($allowNew) and $allowNew == 'Y') {
	$vendors[] = array('BTVEND'=>'New Vendor - NEW','BTSTAT'=>'*');
}
if (isset($_SESSION[APPLICATION]['FACILITY'])) {
	$oldFacility = $_SESSION[APPLICATION]['FACILITY'];
}
if (isset($facility)) {
	setFacility($facility);
}
error_log('-- selecting vendors for list');
$Vendor = new vend();
if ($Vendor->select(array('BTVEND','BTSTAT','BTNAME','strip(BTNAME)||\' - \'||BTVEND AS BTDESC'),array("ORDERBY"=>array("BTNAME","BTVEND")),"WHERE UCASE(BTVEND) like '%".strtoupper(trim($_GET['term']))."%' or UCASE(BTNAME) like '%".strtoupper(trim($_GET['term']))."%'")) {
	while ($vendorRow = $Vendor->getnext()) {
		if (trim($vendorRow['BTVEND']) != '*ONETIME') {
			$vendors[] = array('BTVEND'=>$vendorRow['BTDESC'],'BTSTAT'=>$vendorRow['BTSTAT']);
		}
	}
} else {
	unset($vendors);
	$vendors[] = array('BTVEND'=>'*** An Error occurred retrieving data','BTSTAT'=>'');
}
if (isset($facility) and isset($oldFacility)) {
	setFacility($oldFacility);
}
$response = $_GET["callback"]."(".json_encode($vendors).")";
header('Content-type: application/json');
echo $response;
unset($Vendor);
?>