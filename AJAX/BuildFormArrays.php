<?php
// Retrieve value of browserOutput directive.  If not provided default to false.
if (isset($_REQUEST['browserOutput'])) {
	$browserOutput = $_REQUEST['browserOutput'];
} else {
	$browserOutput = false;
}
// Retrieve value of verbose directive.  If not provided default to false.
if (isset($_REQUEST['verbose'])) {
	$verbose = $_REQUEST['verbose'];
} else {
	$verbose = false;
}
// Set the time limit for the script to run so that it does not timeout
set_time_limit(0);

// Begin buffering the output.
if (ob_get_level() == 0) {
	ob_start();
}

// Initialize array and output filename.
$valuesArray = array();
$dataFile = 'includes/vendorArrays.php';

// Build the array from retrieved data
function buildArray ($dBase, $file, $selector='none', $valueField, $optionField, $whereSelect) {
	global $valuesArray;
	global $selectStatus;
	$tempArray = explode('as ',$valueField);
	if(count($tempArray) > 0) {
		$valueFieldName = $tempArray[count($tempArray)-1];
	} else {
		$valueFieldName = $valueField;
	}
	unset($tempArray);
	$tempArray = explode('as ',$optionField);
	if(count($tempArray) > 0) {
		$optionFieldName = $tempArray[count($tempArray)-1];
	} else {
		$optionFieldName = $optionField;
	}

	$table = new $file();
	$found = false;
	if ($table->select(array($valueField,$optionField),$whereSelect)) {
		sendMessage("Processing Data for $file","&nbsp;&nbsp;&nbsp;&nbsp;---->");
		while($tableRow = $table->getnext()) {
			$valuesArray[$dBase][$file][$selector][addslashes($tableRow[$valueFieldName])] = addslashes($tableRow[$optionFieldName]);
			$found = true;
		}
	} else {
		sendMessage("Unable to select data from $file (value=$valueField, option=$optionField, where=$whereSelect","&nbsp;&nbsp;&nbsp;&nbsp;---->");
		$selectStatus = 'Failed';
	}
	if (!$found) {
		$valuesArray[$dBase][$file][$selector] = array();
		sendMessage("No Data Found for $file","&nbsp;&nbsp;&nbsp;&nbsp;---->");
	}
	if (isset($table)) {
		unset($table);
	}
}

// Build the array for default values
function buildDefaults ($dBase, $whereSelect) {
	global $valuesArray;

	$poDft = new podft();
	$found = false;
	if ($poDft->select(null,$whereSelect)) {
		if ($poDftRow = $poDft->getnext()) {
			$found = true;
			$valuesArray[$dBase]['podft']['none']['BTFOBC'] = $poDftRow['JLFOBC'];
			$valuesArray[$dBase]['podft']['none']['BTPRCL'] = $poDftRow['JLPRCL'];
			$valuesArray[$dBase]['podft']['none']['BTOSSL'] = $poDftRow['JLOSTK'];
			$valuesArray[$dBase]['podft']['none']['BTTAXG'] = $poDftRow['JLTAXG'];
			$valuesArray[$dBase]['podft']['none']['BTTAXR'] = $poDftRow['JLTAXR'];
			$valuesArray[$dBase]['podft']['none']['BTTAXS'] = $poDftRow['JLTAXS'];
		}
	}
	if (!$found) {
		$valuesArray[$dBase]['podft']['none'] = array();
	}
}

// write a message to the browser window.  This will output a script that runs to insert text into
//  the scrollDiv section of the page and then repositions to the bottom of that section.
function sendMessage($messageText, $padding = null) {
	global $browserOutput;
	global $verbose;
	if ($browserOutput) {
		if ($padding != null) {
			$message = $padding.$messageText."<br>";
		} else {
			$message = $messageText."<br>";
		}
			echo "<script>$(\"#scrollDiv\").append(\"$message\");\n";
			echo "   height = $(\"#scrollDiv\")[0].scrollHeight;\n";
			echo "   $(\"#scrollDiv\").scrollTop(height);\n";
			echo "</script>\n";
		ob_flush();
		flush();
	} else {
		if ($verbose) {
			error_log($messageText);
		}
	}
}

// set the progress bar width
function setProgressBar($progressPercent) {
	global $browserOutput;
	if ($browserOutput) {
		echo "<script>$(\"#progressBar\").css('width','".$progressPercent."%');";
		echo "$(\"#progressLabel\").html(\"".intval($progressPercent)."% complete\");</script>";
	}
}
// Create the browser output if the directive is set to output data to the browser.
if ($browserOutput) {
	include ("includes/header.php");
	echo "<div class=\"progressContent\">";
	echo "<div class=\"progress-bar gold shine\">";
	echo "<span id=\"progressBar\"></span>";
	echo "</div>";
	echo "<div id=\"progressLabel\">&nbsp;</div>";
	echo "</div>";
	echo "<div id=\"scrollDiv\">\n";
	echo "</div>\n";
	for ($out=1;$out<8192;$out++) {
		echo " ";
	}
	include ("includes/footer.php");
	ob_flush();
	flush();
}
sendMessage('Running Array Build . . .');

// Get a list of all the available servers used by this application
$dBase = new fwcompid();
$dBase->select(null,null);
while ($dBaseRow = $dBase->getnext()) {
	if (trim($dBaseRow['FWFUT05']) != 'I') {
		$databases[] = $dBaseRow;
	}
}
if (isset($dBase)) {
	unset($dBase);
}

// Determine the percentage each database represents and initialize the progressWidth;
$dbPct = 100 / count($databases);
$progressWidth = 0;
setProgressBar($progressWidth);

// For each server retrieve all of the necessary table data and build the arrays
foreach ($databases as $row) {
	// Retrieve Database connection information
	$server = new server();
	$server->select(null,array('WHERE'=>array('DBNAME'=>$row['FWFUT02'])));
	if ($serverRow = $server->getnext()) {
		if (isset($server)) {
			unset($server);
		}
		// establish session data used by the connection process
		$_SESSION[APPLICATION]['dbName'] = $row['FWFUT02'];
		$_SESSION[APPLICATION]['plant'] = $row['FWPLTC'];
		$_SESSION[APPLICATION]['company'] = $row['FWGLCO'];
		$_SESSION[APPLICATION]['dbCode'] = $row['FWDATC'];
		if (trim($row['FWDATC']) == 'CMSDATMX') {
			$_SESSION[APPLICATION]['dbSchema'] = 'CMSDAT';
		} else {
			$_SESSION[APPLICATION]['dbSchema'] = $row['FWDATC'];
		}
		$system = $row['FWDATC'].':'.$row['FWPLTC'].':'.$row['FWGLCO'];
		$dataFile = 'includes/va_'.str_replace(array(' ',':'), array('_','-'), $system).'.php';
		if (isset($valuesArray)) {
			$valuesArray = null;
			$valuesArray = array();
		}
		sendMessage("Beginning Build for ".trim($system)." . . .","&nbsp;&nbsp;");
		error_log("Beginning Build for ".trim($system)." . . .");
		// Establish the physical connection
		$db = new db();
		$cxn[$system] = $db->Get_Db_Connection($serverRow['DBNAME'],$serverRow['DBUSER'],$serverRow['DBPWD']);
		if (is_resource($cxn[$system])) {
			$selectStatus = 'Success';
			// Begin retrieval of array data
			sendMessage("Retrieving data for TXGP . . .","&nbsp;&nbsp;&nbsp;-->");
			error_log("Retrieving data for TXGP . . .");
			buildArray($system,'txgp','none','NKGRP','NKDES',null);
			$extractCount = 20 + count($valuesArray[$system]['txgp']['none']);
			$extractPct = $dbPct / $extractCount;
			$progressWidth += $extractPct;
			setProgressBar($progressWidth);
			sendMessage("Retrieving data for PROV . . .","&nbsp;&nbsp;&nbsp;-->");
			error_log("Retrieving data for PROV . . .");
			buildArray($system,'prov','none','B57CODE||\':\'||B57CTRY as B57CODE','Strip(B57NAME)||\', \'||B57CTRY as B57NAME',null);
			$progressWidth += $extractPct;
			setProgressBar($progressWidth);
			sendMessage("Retrieving data for CORG . . .","&nbsp;&nbsp;&nbsp;-->");
			buildArray($system,'corg','none','P6COD','P6DES',null);
			$progressWidth += $extractPct;
			setProgressBar($progressWidth);
			sendMessage("Retrieving data for KYIC . . .","&nbsp;&nbsp;&nbsp;-->");
			buildArray($system,'kyic','none','C8INSP','C8DESC',null);
			$progressWidth += $extractPct;
			setProgressBar($progressWidth);
			sendMessage("Retrieving data for CODE 'NN' . . .","&nbsp;&nbsp;&nbsp;-->");
			buildArray($system,'code','NN','A9','A30',array('WHERE'=>array('A2'=>'NN')));
			$progressWidth += $extractPct;
			setProgressBar($progressWidth);
			sendMessage("Retrieving data for CODE 'FF' . . .","&nbsp;&nbsp;&nbsp;-->");
			buildArray($system,'code','FF','A9','A30',array('WHERE'=>array('A2'=>'FF')));
			$progressWidth += $extractPct;
			setProgressBar($progressWidth);
			sendMessage("Retrieving data for TXRT . . .","&nbsp;&nbsp;&nbsp;-->");
			foreach($valuesArray[$system]['txgp']['none'] as $taxVal=>$taxOpt) {
				sendMessage("Retrieving data for TXRT '$taxVal' . . .","&nbsp;&nbsp;&nbsp;-->");
				buildArray($system,'txrt',$taxVal,'NIRTC','NIDES',array('WHERE'=>array('NIGRP'=>$taxVal)));
				$progressWidth += $extractPct;
				setProgressBar($progressWidth);
			}
			sendMessage("Retrieving data for STKR . . .","&nbsp;&nbsp;&nbsp;-->");
			buildArray($system,'stkr','none','AXSTKL','AXLOCN',array('WHERE'=>array('AXPLNT'=>$row['FWPLTC'])));
			$progressWidth += $extractPct;
			setProgressBar($progressWidth);
			sendMessage("Retrieving data for Repair Location . . .","&nbsp;&nbsp;&nbsp;-->");
			buildArray($system,'stkr','RPRL','AXSTKL','AXLOCN',array('WHERE'=>array('AXPLNT'=>$row['FWPLTC'],'AXCNSN'=>'4')));
			$progressWidth += $extractPct;
			setProgressBar($progressWidth);
			sendMessage("Retrieving data for TMZN . . .","&nbsp;&nbsp;&nbsp;-->");
			buildArray($system,'tmzn','none','A82TMZN','A82DESC',null);
			$progressWidth += $extractPct;
			setProgressBar($progressWidth);
			sendMessage("Retrieving data for VDCL . . .","&nbsp;&nbsp;&nbsp;-->");
			buildArray($system,'vdcl','none','BK9CODE','BK9DES1',null);
			$progressWidth += $extractPct;
			setProgressBar($progressWidth);
			sendMessage("Retrieving data for SHCR . . .","&nbsp;&nbsp;&nbsp;-->");
			buildArray($system,'shcr','none','RMCARC','RMDES1',null);
			$progressWidth += $extractPct;
			setProgressBar($progressWidth);
			sendMessage("Retrieving data for CBCR . . .","&nbsp;&nbsp;&nbsp;-->");
			buildArray($system,'cbcr','none','XBCBRC','XBDES1',null);
			$progressWidth += $extractPct;
			setProgressBar($progressWidth);
			sendMessage("Retrieving data for VRTH . . .","&nbsp;&nbsp;&nbsp;-->");
			buildArray($system,'vrth','none','G3CODE','G3DES1',null);
			$progressWidth += $extractPct;
			setProgressBar($progressWidth);
			sendMessage("Retrieving data for PGTP level 1. . .","&nbsp;&nbsp;&nbsp;-->");
			buildArray($system,'pgtp','1','B26CCDE','B26DES1',array('WHERE'=>array('B26CLVL'=>1)));
			$progressWidth += $extractPct;
			setProgressBar($progressWidth);
			sendMessage("Retrieving data for PGTP level 2 . . .","&nbsp;&nbsp;&nbsp;-->");
			buildArray($system,'pgtp','2','B26CCDE','B26DES1',array('WHERE'=>array('B26CLVL'=>2)));
			$progressWidth += $extractPct;
			setProgressBar($progressWidth);
			sendMessage("Retrieving data for PGTP level 3 . . .","&nbsp;&nbsp;&nbsp;-->");
			buildArray($system,'pgtp','3','B26CCDE','B26DES1',array('WHERE'=>array('B26CLVL'=>3)));
			$progressWidth += $extractPct;
			setProgressBar($progressWidth);
			sendMessage("Retrieving data for PGTP level 4 . . .","&nbsp;&nbsp;&nbsp;-->");
			buildArray($system,'pgtp','4','B26CCDE','B26DES1',array('WHERE'=>array('B26CLVL'=>4)));
			$progressWidth += $extractPct;
			setProgressBar($progressWidth);
			sendMessage("Retrieving data for CONT . . .","&nbsp;&nbsp;&nbsp;-->");
			buildArray($system,'cont','none','IFBNK#','IFBKNM',array('WHERE'=>array('IFCOM#'=>$row['FWGLCO'])));
			$progressWidth += $extractPct;
			setProgressBar($progressWidth);
			sendMessage("Retrieving data for IREA . . .","&nbsp;&nbsp;&nbsp;-->");
			buildArray($system,'irea','3','WEREAS','WEDESC',array('WHERE'=>array('WETYPE'=>3)));
			$progressWidth += $extractPct;
			setProgressBar($progressWidth);
			sendMessage("Building Vendor Defaults . . .","&nbsp;&nbsp;&nbsp;-->");
			buildDefaults($system, array('WHERE'=>array('JLPLNT'=>$row['FWPLTC'])));
			$progressWidth += $extractPct;
			setProgressBar($progressWidth);

			// Done retrieving data.
			if ($selectStatus == 'Failed') {
				sendMessage("Abandoning data due to failure . . .");
				error_log("Abandoning data due to failure . . .");
			} else {
				sendMessage("Writing output to file . . .");
				error_log("Writing output to file . . . ");
				$close = '';
				// Delete the existing file
				if (file_exists($dataFile)) {
					unlink($dataFile);
				}
				// Write the new file
				$fHandle = fopen($dataFile,'a');
				fwrite($fHandle,"<?php\n");
				foreach($valuesArray as $system=>$systemArray) {
					foreach($systemArray as $file=>$fileArray) {
						foreach($fileArray as $segment=>$segmentArray) {
							fwrite($fHandle,"\t$close\n\$vendorArray['$system']['$file']['$segment'] = array(\n");
							$close = ");\n";
							foreach($segmentArray as $value=>$opt) {
								if ($file == 'podft') {
									fwrite($fHandle,"\t\t\t\t\t'".trim($value)."'=>'".trim($opt)."',\n");
								} else {
									fwrite($fHandle,"\t\t\t\t\t'".trim($value)."'=>'".trim($value).' - '.trim($opt)."',\n");
								}
							}
						}
					}
				}
			}

			fwrite($fHandle,"$close?>");
		} else {
			$progressWidth += $dbPct;
			setProgressBar($progressWidth);
		}
	} else {
		$progressWidth += $dbPct;
		setProgressBar($progressWidth);
	}

}
$progressWidth = 100;
setProgressBar($progressWidth);

// close out the process
sendMessage("Array build complete !");
error_log("Array build complete !");
if ($browserOutput) {
	echo "</body>\n";
	echo "</html>\n";
	echo "<script>\n";
	echo " endScroll = true;\n";
	echo "</script>\n";
	include ("includes/footer.php");
}
ob_end_flush();
?>
