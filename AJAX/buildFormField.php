<?php
// Determine the name of the currently running script.
$thisScriptPath = __FILE__;
$scriptElements = explode('/',$thisScriptPath);
$scriptName = $scriptElements[count($scriptElements)-1];
// initialize values that need defaults.
$value = null;
$keyChange = null;
$parms = array();
// retrieve request array and load fields appropriately
foreach($_REQUEST as $key=>$data) {
	switch ($key) {
		case 'table' 		: 	$table = $data;
								break;
		case 'column'		:	$column = $data;
								break;
		case 'value'		:	$value = $data;
								break;
		case 'keyChange'	: 	$keyChange = $data;
								break;
		case 'AJAXFunction'	:	$AJAXFunction = $data.'.php';
								break;
		case 'sessionObj'   :   $sessionObj = $data;
								break;
		default				:	$$key = $data;
								$parms[$key] = $data;
								break;
	}
}
switch ($table) {
	case 'vendorform'	:	if (!isset($vendorArray) or !array_key_exists($_SESSION[APPLICATION]['FACILITY'],$vendorArray)) {
								if (isset($vendorArray)) {
									unset($vendorArray);
								}
								$vendorArrayFile = 'va_'.str_replace(array(' ',':'),array('_','-'),$_SESSION[APPLICATION]['FACILITY']).'.php';
								//error_log('vendor array file is: '.$vendorArrayFile);
								if (file_exists('includes/'.$vendorArrayFile)) {
									require_once('includes/'.$vendorArrayFile);
									//error_log('vendor array file found');
								}
							}
							//include ('includes/vendorArrays.php');
							break;
}
// include the appropriate table class if table is defined and this is the
//  script that was called from the AJAX request
if ($AJAXFunction == $scriptName and isset($table) and isset($column)) {
    if (file_exists($module.'/classes/class.'.$table.'.php')) {
    	include_once($module.'/classes/class.'.$table.'.php');
    } else {
    	if (file_exists('classes/class.'.$table.'.php')) {
    		include_once ('classes/class.'.$table.'.php');
    	}
    }
}
// if the table class exists from the above include then instantiate it and
//   execute the request.

if (class_exists($table)) {
	if (!isset($sessionObj) or !array_key_exists($sessionObj,$_SESSION[APPLICATION])) {
		$tableObj = new $table();
		if (method_exists($tableObj, 'resetColumns')) {
			$tableObj->resetColumns($column);
		}
		echo $tableObj->renderFormLine($column,$value,$keyChange,$parms);
	} else {
		if (method_exists($_SESSION[APPLICATION][$sessionObj],'resetColumns')) {
			$_SESSION[APPLICATION][$sessionObj]->resetColumns($column);
		}
		echo $_SESSION[APPLICATION][$sessionObj]->renderFormLine($column,$value,$keyChange,$parms);
	}
}
?>