<?php 
// Determine the name of the currently running script.
$thisScriptPath = __FILE__;
$scriptElements = explode('/',$thisScriptPath);
$scriptName = $scriptElements[count($scriptElements)-1];

// initialize values that need defaults.
$value = null;
$keyChange = null;
$parms = array();
// retrieve request array and load fields appropriately
foreach($_REQUEST as $key=>$data) {
	switch ($key) {
		case 'table' 		: 	$table = $data;
								break;
		case 'column'		:	$column = $data;
								break;
		case 'value'		:	$value = $data;
								break;
		case 'keyChange'	: 	$keyChange = $data;
								break;
		case 'AJAXFunction'	:	$AJAXFunction = $data.'.php';
								break;
		case 'sessionObj'   :   $sessionObj = $data;
								break;
		default				:	$$key = $data;
								$parms[$key] = $data;
								break;
	}
}
$vendors[] = array('CORPVEND'=>'New Corporate Vendor - NEW','VENDSTAT'=>'*');
$CorpVend = new vmcrvncd();
$CorpVend->select(array('CORPVEND','CRVNSTAT','CRVNDESC','strip(CRVNDESC)||\' - \'||CORPVEND AS CRVNDESC2'),array("ORDERBY"=>array("CRVNDESC","CORPVEND")),"WHERE UCASE(CORPVEND) like '%".strtoupper(trim($_GET['term']))."%' or UCASE(CRVNDESC) like '%".strtoupper(trim($_GET['term']))."%'");
while ($cvRow = $CorpVend->getnext()) {
	$vendors[] = array('CORPVEND'=>$cvRow['CRVNDESC2'],'VENDSTAT'=>$cvRow['CRVNSTAT']);
}
$response = $_GET["callback"]."(".json_encode($vendors).")";
header('Content-type: application/json');
echo $response;
unset($CorpVend);
?>