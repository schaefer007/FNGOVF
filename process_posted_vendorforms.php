<?php
/**
 * This script's purpose is to analyze each posted form in
 * the dbsystem.formrequest table and compare it with it's
 * remote counterpart. The vendormap array is used to map
 * each field from the local row to the remote row.
 *
 * Conditions for setting the verified column:
 * -Perfect match between local and remote rows: Y
 * -Remote row not found or mismatching columns: Increment
 */
require_once "includes/initialize.php";
require_once "includes/vendorMap.php";
unset($_REQUEST);
$FacilityBU = $_SESSION[APPLICATION]['FACILITY'];

function setFacility($facility) {
    if (isset($facility)) {
        $_SESSION[APPLICATION]['FACILITY'] = $facility;
        $FacilityArray = explode(':',$facility);
        $_SESSION[APPLICATION]['dbCode'] = $FacilityArray[0];
        if (trim($FacilityArray[0]) == 'CMSDATMX') {
            $_SESSION[APPLICATION]['dbSchema'] = 'CMSDAT';
        } else {
            $_SESSION[APPLICATION]['dbSchema'] = $FacilityArray[0];
        }
        $_SESSION[APPLICATION]['plant'] = $FacilityArray[1];
        $_SESSION[APPLICATION]['company'] = $FacilityArray[2];
        $fwCompId = new fwcompid();
        $fwCompId->select(array('FWFUT02'),array('WHERE'=>array('FWDATC'=>$FacilityArray[0],'FWPLTC'=>$FacilityArray[1],'FWGLCO'=>$FacilityArray[2])));
        if ($fwCompRow = $fwCompId->getnext()) {
            $_SESSION[APPLICATION]['dbName'] = $fwCompRow['FWFUT02'];
        }
    }
}

function setVerified($VFID, $facility, $action) {
    if(!isset($VFID) || !isset($facility) || !isset($action)) return false;
    $request_backup = $_REQUEST;
    unset($_REQUEST);
    setFacility($facility);

    $formRequest = new formrequest();
    $formRequest->select(null,array('WHERE'=>array('formID'=>$VFID)));
    if ($formRow = $formRequest->getnext()) {
        $_REQUEST['processingInstanceID'] = $formRow['processingInstanceID'];
        switch($action) {
            case "increment":
                if(is_numeric($formRow["verified"])) {
                    $_REQUEST["verified"] = $formRow["verified"] + 1;
                } else {
                    $_REQUEST["verified"] = 1;
                }
                break;
            case "verify":
                $_REQUEST["verified"] = "Y";
                break;
            default:
                break;
        }
        if ($formRequest->update()) {
            base::write_log("Vendor Form Verified Flag Updated","S");
        } else {
            base::write_log("Update of Verified Flag for Form Request failed","E");
        }
    }
    $_REQUEST = $request_backup;
}

$formrequests = new formrequest();
$formrequests->select(null,array("WHERE"=>array("status"=>"Posted")));
while(($formrequest = $formrequests->getnext()) != false) {
    if($formrequest["verified"] == "Y" OR is_numeric($formrequest["verified"]) AND $formrequest["verified"] >= 3 ) continue;
    $Facility = implode(":", array($formrequest["dbSchema"], $formrequest["plant"], $formrequest["company"]));
    setFacility($Facility);
    $vendorform = new vendorform();
    $vendorform->select(null, array("WHERE" => array("VFID" => $formrequest["formID"])));
    if (($formRow = $vendorform->getnext()) != false) {
//        if($formRow["VFID"] != 2246) { continue; }
//        echo "Facility [$Facility] - ";
        echo "Processing VFID " . $formRow["VFID"] . " - {$formRow["VENDORACTION"]}";
        if (isset($formRow['VENDORCODE'])) {
            $VendorCode = $formRow['VENDORCODE'];
            $vendor = new vend();
            $vendor->select(null, array('WHERE' => array('BTVEND' => $VendorCode)));
            $FacilityArray = explode(':', $_SESSION[APPLICATION]['FACILITY']);
            // If state is a combined state/country then swap it back to just the state
            if (isset($formRow['VENDORSTATE']) and strPos($formRow['VENDORSTATE'], ':') !== false) {
                $tempArray = array();
                $tempArray = explode(':', $formRow['VENDORSTATE']);
                $formRow['VENDORSTATE'] = $tempArray[0];
            }
            // End special state handling for combined state/country

            // Verify the vendor file fields match
            if ($vendorInfo = $vendor->getnext()) {
                foreach($vendorMap as $vendorCol=>$webCol) {
                    switch($formRow["VENDORACTION"]) {
                        case "Add":
                            if (isset($formRow[$webCol]) and (trim($vendorInfo[$vendorCol]) != trim($formRow[$webCol]) and trim($vendorInfo[$vendorCol]) != trim($vendorDefault[$vendorCol]))) {
//                                echo "$webCol != $vendorCol [";
//                                echo $formRow[$webCol] . " - " . $vendorInfo[$vendorCol] . "]<br>";
                                $_REQUEST[$vendorCol] = $formRow[$webCol];
                            }
                            break;
                        case "Edit":
                            if (in_array($webCol,$updateMap) and isset($formRow[$webCol]) and trim($vendorInfo[$vendorCol]) != trim($formRow[$webCol]) and trim($vendorInfo[$vendorCol]) != trim($vendorDefault[$vendorCol])) {
//                                echo "$webCol != $vendorCol [";
//                                echo $formRow[$webCol] . " - " . $vendorInfo[$vendorCol] . "]<br>";
                                $_REQUEST[$vendorCol] = $formRow[$webCol];
                            }
                            break;
                        case "Default":
                            break;
                    }
                }
            } else {
                echo " - \$vendorInfo could not be found.<br>";
                continue;
            }
            // End vendor file verification
            if ($vendorInfo['BTCURR'] == '  ' and $vendorInfo['BTBANK'] != ' ') {
                $bank = new cont();
                $bank->select(array('IFCURR'),array('WHERE'=>array('IFCOM#'=>$FacilityArray[2],'IFBNK#'=>$vendorInfo['BTBANK'])));
                if($bankRow = $bank->getnext()) {
                    $_REQUEST['BTCURR'] = $bankRow['IFCURR'];
                }
                unset($bank);
            }
            $usrc = new usrc();
            $FacilityArray = explode(':',$_SESSION[APPLICATION]['FACILITY']);
            $fwCompId = new fwcompid();
            $fwCompId->select(array('FWFUT04'),array('WHERE'=>array('FWDATC'=>$FacilityArray[0],'FWPLTC'=>$FacilityArray[1],'FWGLCO'=>$FacilityArray[2])));
            if ($fwCompRow = $fwCompId->getnext()) {
                $_REQUEST['MFENT#'] = trim($fwCompRow['FWFUT04']);
            }
            if (empty($_REQUEST['MFENT#'])) {
                $_REQUEST['MFENT#'] = 100;
            }
            $usrc->select(null,array('WHERE'=>array('MFSRCE'=>'MN','MFKEY1'=>'VENDOR MASTER','MFKEY2'=>$VendorCode,'MFENT#'=>$_REQUEST['MFENT#'])));
            unset($_REQUEST['MFENT#']);
            if ($usrcRow = $usrc->getnext()) {
                if (trim($usrcRow["MFRESP"]) != trim($formRow["CORPVENDMAST"])) {
                    echo "<br>>>\$usrcRow[\"MFRESP\"] != \$formRow[\"CORPVENDMAST\"] - {$usrcRow["MFRESP"]} != {$formRow["CORPVENDMAST"]}";
                }
            } else {
                echo "No usrc row found for vendor code.<br>";
            }
            if($formRow["VENDORACTION"] == "Add") {
                $venx = new venx();
                $venx->select(null, array('WHERE' => array('DA0VEND' => $VendorCode)));
                if (($venxRow = $venx->getnext()) == false) {
                    echo "No venx row found for vendor code.<br>";
                }
            }
            if(count($_REQUEST) > 0) {
                setVerified($formRow["VFID"],$FacilityBU,"increment");
                //TODO update formrequest row SET verified = verified + 1
                echo "<pre>";
                print_r($_REQUEST);
                echo "</pre>";
            } else {
                setVerified($formRow["VFID"],$FacilityBU,"verify");
                //TODO update formrequest row SET verified = "Y"
                echo "<br>>>No issues found.";
            }
            echo "<br>";
        } else {
            setVerified($formRow["VFID"],$FacilityBU,"increment");
            echo "NOT FOUND: VFID " . $formrequest["formID"] . "<br>";
        }
    }
    unset($vendorform);
    unset($_REQUEST);
}
setFacility($FacilityBU);

//$error = error_get_last();
//echo "<pre>";
//print_r($error);
//echo "</pre>";

exit(0);