<?php
if (isset($_REQUEST['instanceID'])) {
	$instanceID = $_REQUEST['instanceID'];
} else {
	$instanceID = '';
}
$_SESSION[APPLICATION]['lastInstanceID'] = $instanceID;
$thisFormEmail = $config['formEmail']['vendorform'];
$formDb = new formrequest();
$formDb->select(null,array('WHERE'=>array('processingInstanceID'=>$instanceID)));
if (!$formRow = $formDb->getnext()) {
	$formRow = array();
}

$cssToLoad = array("table.css.php","process_instance.css.php", "program_common.css.php", "program_header_full.css.php", "ec_add.css.php");
$jsToLoad = array("popup.js.php","process_instance.js.php", "program_ec.js.php", "ec_add.js.php","jquery-ui.js.php","common_allsites.js.php"); // CODE CLEAN: Using program_ec only for viewEC(). Could put this somewhere common
$blnIsIE = (isset($_SERVER['HTTP_USER_AGENT']) && (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false));
if($blnIsIE) $cssToLoad[] = "ie_only.css.php";
//$cssToLoad = array("plm/css.php?&amp;arrCSS&#91;&#93;=index.css.php&amp;arrCSS&#91;&#93;=style.css.php&amp;arrCSS&#91;&#93;=common.css.php&amp;arrCSS&#91;&#93;=common_allsites.css.php&amp;arrCSS&#91;&#93;=header.css.php&amp;arrCSS&#91;&#93;=jquery-ui.css.php&amp;arrCSS&#91;&#93;=popup.css.php");
$cssInclude = "plm/css.php?";
foreach ($cssToLoad as $cssFile) {
	$cssInclude .= '&amp;arrCSS&#91;&#93;='.$cssFile;
}
$cssArray = array($cssInclude);
$cssArray[] = 'styles/jquery-ui.1.9.2.css';
$jsInclude = "plm/js.php?";
foreach ($jsToLoad as $jsFile) {
	$jsInclude .= '&amp;arrJS&#91;&#93;='.$jsFile;
}
$jsArray = array($jsInclude);
$_SESSION[APPLICATION]['requestedForm'] = intWFPROCESS_ID_VENDOR_REQUEST;
$dbName = $formRow['dbName'];
$_SESSION[APPLICATION]['dbName'] = $formRow['dbName'];
$_SESSION[APPLICATION]['dbCode'] = $formRow['dbSchema'];
if (trim($formRow['dbSchema']) == 'CMSDATMX') {
	$_SESSION[APPLICATION]['dbSchema'] = 'CMSDAT';
} else {
	$_SESSION[APPLICATION]['dbSchema'] = $formRow['dbSchema'];
}
$_SESSION[APPLICATION]['plant'] = $formRow['plant'];
$_SESSION[APPLICATION]['company'] = $formRow['company'];
$ID = $formRow['formID'];
$FACILITY = $formRow['dbSchema'].':'.str_pad($formRow['plant'],3).':'.$formRow['company'];
$_REQUEST['FACILITY'] = $FACILITY;
$_SESSION[APPLICATION]['FACILITY'] = $FACILITY;

$step=3;
error_log('formType is '.$formRow['formType']);
if (!isset($_SESSION[APPLICATION][$formRow['formType']])) {
	$_SESSION[APPLICATION][$formRow['formType']] = new $formRow['formType']();
} else {
	$_SESSION[APPLICATION][$formRow['formType']]->reset();
}
$_SESSION[APPLICATION][$formRow['formType']]->select(null,array('WHERE'=>array('VFID'=>$ID)));
if ($editRow = $_SESSION[APPLICATION][$formRow['formType']]->getnext()) {
	foreach ($editRow as $formField=>$formValue) {
//		echo "$formField => $formValue<br />";
		$$formField = trim($formValue);
	}
	$action = 'Edit';
}

if ((!array_key_exists('module',$_SESSION[APPLICATION]) or $_SESSION[APPLICATION]['module'] != 'admin') and $status == 'New' and $REQUSERID != $_SESSION[APPLICATION]['user']) {
	$view = 'noAuth.php';
	$_REQUEST['view'] = 'noAuth.php';
}
$jsArray[] = 'js/vendorRequest.js';
$viewTitle = "$action Vendor";
if (file_exists('includes/view.php')) {
	include('includes/view.php');
} else {
	base::missingPage('view.php');
}
?>