<?php
// In process forms section
set_include_path(get_include_path().PATH_SEPARATOR.'/www/zendsvr/htdocs/FNG/plm/classes/'.PATH_SEPARATOR.'/www/zendsvr/htdocs/FNG/plm/');
include_once ('classes/class.workFlowInstance.php');
include_once ('includes/constants.vendorform.php');
if (isset($_REQUEST['listFilter'])) {
	$listFilter = $_REQUEST['listFilter'];
} else {
	if (isset($_SESSION[APPLICATION]['formsListFilter']) and $_SESSION[APPLICATION]['formsListFilter'] != '') {
		$listFilter = $_SESSION[APPLICATION]['formsListFilter'];
	} else {
		$listFilter = 'sbm';
	}
}
if (isset($_REQUEST['listStatusFilter'])) {
	$listStatusFilter = $_REQUEST['listStatusFilter'];
} else {
	if (isset($_SESSION[APPLICATION]['formsListStatusFilter']) and $_SESSION[APPLICATION]['formsListStatusFilter'] != '') {
		$listStatusFilter = $_SESSION[APPLICATION]['formsListStatusFilter'];
	} else {
		$listStatusFilter = 'active';
	}
}
if (isset($_REQUEST['listFacilityFilter'])) {
	$listFacilityFilter = $_REQUEST['listFacilityFilter'];
} else {
	if (isset($_SESSION[APPLICATION]['formsListFacilityFilter']) and $_SESSION[APPLICATION]['formsListFacilityFilter'] != '') {
		$listFacilityFilter = $_SESSION[APPLICATION]['formsListFacilityFilter'];
	} else {
		$listFacilityFilter = 'All';
	}
}
$facilityDDArray = array('All');
foreach ($_SESSION[APPLICATION]['userFacilities'] as $facilityNum=>$facilityRow) {
	$facilityDDArray[] = $facilityRow['Name'];
}
$facilityArray = array();
if ($listFacilityFilter == 'All') {
	$facilityArray = $facilityDDArray;
} else {
	$facilityArray = array($listFacilityFilter);
}

$stsArray = array();
switch ($listStatusFilter) {
	case 'active': $stsArray = array('New','Active','Re-Submit');
	break;
	case 'approve': $stsArray = array('Approved');
	break;
	case 'reject' : $stsArray = array('Rejected');
	break;
	case 'any'	  : $stsArray = array('New','Active','Approved','Rejected','Posted','Deleted','Re-Submit');
	break;
}

$_SESSION[APPLICATION]['formsListStatusFilter'] = $listStatusFilter;
$_SESSION[APPLICATION]['formsListFilter'] = $listFilter;
$_SESSION[APPLICATION]['formsListFacilityFilter'] = $listFacilityFilter;
$formsDb = new formrequest();
$formsInfo = array();
switch ($listFilter) {
	case 'sbm'		:	if ($formsDb->select(null,array('WHERE'=>array('status'=>$stsArray,'reqID'=>$_SESSION['intUser'],'facilityName'=>$facilityArray)))) {
		while ($formsRow = $formsDb->getnext()) {
			$formsInfo[$formsRow['processingInstanceID']] = $formsRow;
		}
	}
	break;
	case 'all'		:   if ($formsDb->select(null,array('WHERE'=>array('status'=>$stsArray,'facilityName'=>$facilityArray)))) {
	    foreach($_SESSION[APPLICATION]['userRoles'] as $myRole) {
	        $myRoles[] = $myRole['intRoleID'];
	    }
	    while ($formsRow = $formsDb->getnext()) {
	       	if (($formsRow['status'] == "New" and $formsRow['reqID'] == $_SESSION['intUser']) or 
			     ($formsRow['status'] == "Re-Submit" and ($formsRow['reqID'] == $_SESSION['intUser'] or 
			         in_array(vf_BITeamRole,$myRoles))) or !in_array($formsRow['status'],array('New','Re-Submit'))) {
			  $formsInfo[$formsRow['processingInstanceID']] = $formsRow;
			}
		}
	}
	break;
	case 'rma'		:	$workFlowArray = new workFlowInstanceArray();
		$tempForms = $workFlowArray->loadForUserPage($_SESSION['intUser'],$facilityArray);
		foreach($_SESSION[APPLICATION]['userRoles'] as $myRole) {
		    $myRoles[] = $myRole['intRoleID'];
		}
		foreach ($tempForms as $formsRow) {
		 	if (($formsRow['status'] == "New" and $formsRow['reqID'] == $_SESSION['intUser']) or 
			     ($formsRow['status'] == "Re-Submit" and ($formsRow['reqID'] == $_SESSION['intUser'] or 
			         in_array(vf_BITeamRole,$myRoles))) or !in_array($formsRow['status'],array('New','Re-Submit'))) {
			$formsInfo[$formsRow['processingInstanceID']] = $formsRow;
	      }
		}
	break;
}
$userDb = new tbluser();
foreach($formsInfo as $instance=>$rowData) {
	$userDb->select(array('"strFirstName"','"strLastName"'),array('WHERE'=>array('intUserID'=>$rowData['reqID'])));
	if ($userRow = $userDb->getnext()) {
		$formsInfo[$instance]['requestorName'] = $userRow['strFirstName'].' '.$userRow['strLastName'];
	}
	$userDb->select(array('"strFirstName"','"strLastName"'),array('WHERE'=>array('intUserID'=>$rowData['stsID'])));
	if ($userRow = $userDb->getnext()) {
		$formsInfo[$instance]['statusName'] = $userRow['strFirstName'].' '.$userRow['strLastName'];
	}
}

if (isset($userDb)) {
	unset($userDb);
}
if (isset($formsDb)) {
	unset($formsDb);
}

// new forms submission section
$formsArray = array();
foreach ($availableForms as $formText=>$formController) {
	if (base::isFormAuthorized($_SESSION['intUser'],$formText)) {
		$formsArray[$formText] = $formController;
	}
}
if (file_exists('includes/view.php')) {
	include('includes/view.php');
} else {
	base::missingPage('view.php');
}
?>
