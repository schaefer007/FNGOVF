<?php 
require_once('classes/class.account.php');

$action = 'Add';
$editRow = array();
$passwordText = '';
$passwordReqd = '*';

foreach ($_REQUEST as $rKey=>$rValue) {
	if (in_array($rKey,array('EMAIL','FNAME','LNAME','NICKNAME','PHONE','EXT','MOBILE','FAX'))) {
		$$rKey = $rValue;
	}
}

$viewTitle = "Register for Access";
if (file_exists('modules/'.$_SESSION['module'].'/includes/view.php')) {
	include ('modules/'.$_SESSION['module'].'/includes/view.php');
} else {
	if (file_exists('includes/view.php')) {
		include ('includes/view.php');
	} else {
		base::missingPage('view.php');
	}
}
?>