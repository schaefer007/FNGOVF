<?php 
$schema = "";
$mailFrom = "";
$mailHost = "";
$mailAuth = "";
$mailPort = "";
$mailUser = "";
$mailPwd = "";
$mailSSl = "";
$webURL = "";
$adminUser = "";
$delayTime = "";
$email_first = "";
$email_second = "";
$email_third = "";

if (isset($config)) {
    if (array_key_exists('schema',$config)) {
    	$schema = $config['schema'];
    }
    if (array_key_exists('webURL',$config)) {
    	$webURL = $config['webURL'];
    }
    if (array_key_exists('adminUser',$config)) {
    	$adminUser = $config['adminUser'];
    }
	if (array_key_exists('mail',$config)) {
		if (array_key_exists('from', $config['mail'])) {
			$mailFrom = $config['mail']['from'];
		}
		if (array_key_exists('host', $config['mail'])) {
			if (strpos($config['mail']['host'],'ssl://') !== false) {
				$mailHost = substr($config['mail']['host'],6);
			} else {
				$mailHost = $config['mail']['host'];
			}
		}
		if (array_key_exists('auth', $config['mail'])) {
			if ($config['mail']['auth'] === true) {
				$mailAuth = 'Y';
			} else {
				$mailAuth = 'N';
			}
	    if (array_key_exists('ssl', $config['mail'])) {
	    	if ($config['mail']['ssl'] === true) {
	    		$mailSSl = 'Y';
	    	} else {
	    		$mailSSl = 'N';
	    	}
	    }
		}
		if (array_key_exists('port',$config['mail'])) {
			$mailPort = $config['mail']['port'];
		}
		if (array_key_exists('username', $config['mail'])) {
			$mailUser = $config['mail']['username'];
		}
		if (array_key_exists('password', $config['mail'])) {
			$mailPwd = $config['mail']['password'];
		}
	}
}

$viewTitle = "Maintain Configuration Options";
if (file_exists('modules/'.$_SESSION['module'].'/includes/view.php')) {
	include ('modules/'.$_SESSION['module'].'/includes/view.php');
} else {
	if (file_exists('includes/view.php')) {
		include ('includes/view.php');
	} else {
		base::missingPage('view.php');
	}
}
?>