<?php
require_once('classes/class.vendorform.php');
require_once('classes/class.formrequest.php');
include_once('includes/vendorMap.php');
function setFacility($facility) {
	if (isset($facility)) {
		$_SESSION[APPLICATION]['FACILITY'] = $facility;
		$FacilityArray = explode(':',$facility);
		$_SESSION[APPLICATION]['dbCode'] = $FacilityArray[0];
		if (trim($FacilityArray[0]) == 'CMSDATMX') {
			$_SESSION[APPLICATION]['dbSchema'] = 'CMSDAT';
		} else {
			$_SESSION[APPLICATION]['dbSchema'] = $FacilityArray[0];
		}
		$_SESSION[APPLICATION]['plant'] = $FacilityArray[1];
		$_SESSION[APPLICATION]['company'] = $FacilityArray[2];
		$fwCompId = new fwcompid();
		$fwCompId->select(array('FWFUT02'),array('WHERE'=>array('FWDATC'=>$FacilityArray[0],'FWPLTC'=>$FacilityArray[1],'FWGLCO'=>$FacilityArray[2])));
		if ($fwCompRow = $fwCompId->getnext()) {
			$_SESSION[APPLICATION]['dbName'] = $fwCompRow['FWFUT02'];
		}
	}
}
//require_once('includes/vendorArrays.php');
$thisFormEmail = $config['formEmail']['vendorform'];
$_SESSION[APPLICATION]['requestedForm'] = intWFPROCESS_ID_VENDOR_REQUEST;
if (isset($_REQUEST['DBNAME'])) {
	$dbName = $_REQUEST['DBNAME'];
}
if (isset($_REQUEST['Id#'])) {
	$ID = $_REQUEST['Id#'];
}
if (isset($_REQUEST['FACILITY'])) {
	$FACILITY = $_REQUEST['FACILITY'];
	$step = 2;
} else {
	$FACILITY = '';
	$step = 1;
}
if (isset($_REQUEST['CPYFACILITY'])) {
	$cpyFacility = $_REQUEST['CPYFACILITY'];
} else {
	$cpyFacility = '';
}
if (isset($_REQUEST['cpyVendor'])) {
	$cpyVendor = $_REQUEST['cpyVendor'];
} else {
	$cpyVendor = '';
}
if (isset($vendorArray)) {
	unset($vendorArray);
}
//___________________________________________Before_______________________________________________
if (isset($FACILITY) and !empty($FACILITY)) {
	$vendorArrayFile = 'va_'.str_replace(array(' ',':'),array('_','-'),$FACILITY).'.php';
} else {
	if (isset($_SESSION[APPLICATION]['FACILITY'])) {
		$vendorArrayFile = 'va_'.str_replace(array(' ',':'),array('_','-'),$_SESSION[APPLICATION]['FACILITY']).'.php';
	}
}
if (!empty($vendorArrayFile) and file_exists('includes/'.$vendorArrayFile)) {
	require('includes/'.$vendorArrayFile);
}
if (($step == 2 and isset($_REQUEST['selectVendor']) and trim($_REQUEST['selectVendor']) != '') or (isset($_REQUEST['cmd']) and $_REQUEST['cmd'] == 'Save')) {
	$step = 3;
}
if (isset($_REQUEST['VENDORCODE'])) {
	$VENDORCODE = $_REQUEST['VENDORCODE'];
} else {
	$VENDORCODE = '';
}
$VENDORACTION = 'Edit';
if (isset($_REQUEST['selectVendor']) and $_REQUEST['selectVendor'] != '' and $_REQUEST['selectVendor'] != 'NEW') {
	$VENDORCODE = $_REQUEST['selectVendor'];
} else {
	if (isset($_REQUEST['selectVendor']) and $_REQUEST['selectVendor'] == 'NEW') {
		$selectVendor = $_REQUEST['selectVendor'];
		$VENDORACTION = 'Add';
	}
}

if ($VENDORCODE != '' and $VENDORACTION == 'Edit') {
	$vendor = new vend();
	$vendor->select(null,array('WHERE'=>array('BTVEND'=>$VENDORCODE)));
	if ($vendorInfo = $vendor->getnext()) {
		$venx = new venx();
		$venx->select(null,array('WHERE'=>array('DA0VEND'=>$VENDORCODE)));
		if($venxRow = $venx->getnext()) {
			$vendorInfo = array_merge($vendorInfo,$venxRow);
		}
		unset($venx);
	//	$vendorAction = 'Edit';
		foreach ($vendorMap as $vendorCol=>$webCol) {
			if (isset($vendorInfo[$vendorCol])) {
				$$webCol = $vendorInfo[$vendorCol];
			}
		}
		$commodity = trim($vendorInfo['BTPGRP']);
		$position = 1;
		$searchTerm = '';
		$fullTerm = '';
		for ($i=0;$i<strlen($commodity);$i++) {
			$searchTerm.=substr($commodity,$i,1);
			$fullTerm.=substr($commodity,$i,1);
			if (array_key_exists($fullTerm,$vendorArray[$FACILITY]['pgtp'][$position])) {
				switch ($position) {
					case 	1: $COMMODITYCAT = $searchTerm;
							   $_REQUEST['COMMODITYCAT'] = $searchTerm;
							   break;
					case	2: $COMMODITYCAT2 = $searchTerm;
							   $_REQUEST['COMMODITYCAT2'] = $searchTerm;
							   break;
					case	3: $COMMODITYCAT3 = $searchTerm;
							   $_REQUEST['COMMODITYCAT3'] = $searchTerm;
							   break;
					case	4: $COMMODITYCAT4 = $searchTerm;
							   $_REQUEST['COMMODITYCAT4'] = $searchTerm;
							   break;
				}
				$searchTerm = '';
				$position++;
			}
		}
		if (!isset($_REQUEST['CORPVENDMAST']) or empty($_REQUEST['CORPVENDMAST'])) {
			$FacilityArray = explode(':',$_SESSION[APPLICATION]['FACILITY']);
			$fwCompId = new fwcompid();
			$fwCompId->select(array('FWFUT04'),array('WHERE'=>array('FWDATC'=>$FacilityArray[0],'FWPLTC'=>$FacilityArray[1],'FWGLCO'=>$FacilityArray[2])));
			if ($fwCompRow = $fwCompId->getnext()) {
				$mfEnt = trim($fwCompRow['FWFUT04']);
			}
			if (empty($mfEnt)) {
				$mfEnt = 100;
			}
			unset($fwCompId);
        $userC = new usrc();
        $userC->select(null,array('WHERE'=>array('MFSRCE'=>'MN','MFKEY1'=>'VENDOR MASTER','MFKEY2'=>$vendorInfo['BTVEND'],'MFENT#'=>$mfEnt)));
        if ($userCRow = $userC->getnext()) {
        	$CORPVENDMAST = trim($userCRow['MFRESP']);
        } else {
        	$CORPVENDMAST   = '';
        }
        if (isset($userC)) {
        	unset($userC);
        }
		} else {
			$CORPVENDMAST = $_REQUEST['CORPVENDMAST'];
		}
	} else {
		$VENDORACTION = 'Add';
	}
}
if ($VENDORACTION == 'Add') {
	if (isset($_REQUEST['FACILITY'])) {
		setFacility($_REQUEST['FACILITY']);
	}
	if (!array_key_exists('vendorform',$_SESSION[APPLICATION])) {
		$_SESSION[APPLICATION]['vendorform'] = new vendorform();
	}
	$optionalFields = $_SESSION[APPLICATION]['vendorform']->getFieldOptional();
	// set defaults for this facility
	foreach ($vendorMap as $vendorCol=>$webCol) {
		if (!isset($$webCol) and array_key_exists($vendorCol, $vendorDefault)) {
			$$webCol = $vendorDefault[$vendorCol];
		}
		if ((!isset($$webCol) or $$webCol == '') and array_key_exists($vendorCol,$vendorArray[$FACILITY]['podft']['none']) and !in_array($webCol,$optionalFields)) {
			$$webCol = $vendorArray[$FACILITY]['podft']['none'][$vendorCol];
		}
	}
	// If copy is specified get vendor from this/other facility and load values
	if ($cpyFacility != '' and $cpyVendor != '' and $VENDORACTION == 'Add') {
		setFacility($cpyFacility);
		$vendor = new vend();
		$vendor->select(null,array('WHERE'=>array('BTVEND'=>$cpyVendor)));
		if ($vendorInfo = $vendor->getnext()) {
			$venx = new venx();
			$venx->select(null,array('WHERE'=>array('DA0VEND'=>$cpyVendor)));
			if($venxRow = $venx->getnext()) {
				$vendorInfo = array_merge($vendorInfo,$venxRow);
			}
			unset($venx);
			foreach ($vendorMap as $vendorCol=>$webCol) {
				if (isset($vendorInfo[$vendorCol]) and ($cpyFacility == $_REQUEST['FACILITY']) or !in_array($webCol, $copyMap)) {
					$$webCol = $vendorInfo[$vendorCol];
				}
			}
			if (!isset($_REQUEST['CORPVENDMAST']) or empty($_REQUEST['CORPVENDMAST'])) {
				$FacilityArray = explode(':',$_SESSION[APPLICATION]['FACILITY']);
				$fwCompId = new fwcompid();
				$fwCompId->select(array('FWFUT04'),array('WHERE'=>array('FWDATC'=>$FacilityArray[0],'FWPLTC'=>$FacilityArray[1],'FWGLCO'=>$FacilityArray[2])));
				if ($fwCompRow = $fwCompId->getnext()) {
					$mfEnt = trim($fwCompRow['FWFUT04']);
				}
				if (empty($mfEnt)) {
					$mfEnt = 100;
				}
				unset($fwCompId);
				$userC = new usrc();
				$userC->select(null,array('WHERE'=>array('MFSRCE'=>'MN','MFKEY1'=>'VENDOR MASTER','MFKEY2'=>$vendorInfo['BTVEND'],'MFENT#'=>$mfEnt)));
				if ($userCRow = $userC->getnext()) {
					$CORPVENDMAST = trim($userCRow['MFRESP']);
				} else {
					$CORPVENDMAST   = '';
				}
				if (isset($userC)) {
					unset($userC);
				}
			} else {
				$CORPVENDMAST = $_REQUEST['CORPVENDMAST'];
			}
		}
		// reset to correct facility
		setFacility($_REQUEST['FACILITY']);
	}
}

foreach ($_REQUEST as $rKey=>$rValue) {
	if ($rKey != 'VENDORACTION' and ($rKey != 'CORPVENDMAST' or !empty($rValue)) and (!isset($cpyVendor) or empty($cpyVendor))) {
		$$rKey = $rValue;
	}
}
if (isset($vendor)) {
	unset($vendor);
}
if (isset($_REQUEST['REQVENDTYPE'])) {
	$REQVENDTYPE = $_REQUEST['REQVENDTYPE'];
} else {
	$REQVENDTYPE = '';
}
if (isset($_REQUEST['REQPURCHFREQ'])) {
	$REQPURCHFREQ = $_REQUEST['REQPURCHFREQ'];
} else {
	$REQPURCHFREQ = '';
}
if (isset($_REQUEST['REQFIRSTTIME'])) {
	$REQFIRSTTIME = $_REQUEST['REQFIRSTTIME'];
} else {
	$REQFIRSTTIME = '';
}
if (isset($_REQUEST['REQOLDVENDCD'])) {
	$REQOLDVENDCD = $_REQUEST['REQOLDVENDCD'];
} else {
	$REQOLDVENDCD = '';
}
if (isset($_REQUEST['REQINACTIVEOLD'])) {
	$REQINACTIVEOLD = $_REQUEST['REQINACTIVEOLD'];
} else {
	$REQINACTIVEOLD = '';
}
if (isset($_REQUEST['FACILITY'])) {
	setFacility($_REQUEST['FACILITY']);
}
if (empty($REQVENDTYPE) or empty($REQPURCHFREQ) or empty($FACILITY) or empty($REQBUYDESC) or empty($REQFIRSTTIME) or empty($REQOLDVENDCD)) {
	$validated = false;
} else {
	$validated = true;
}
if (isset($ID)) {
	if (!array_key_exists('vendorform',$_SESSION[APPLICATION])) {
		$_SESSION[APPLICATION]['vendorform'] = new vendorform();
	}
	$_SESSION[APPLICATION]['vendorform']->select(null,array('WHERE'=>array('ID#'=>$ID)));
	if ($editRow = $_SESSION[APPLICATION]['vendorform']->getnext()) {
		$action = 'Edit';
		foreach($editRow as $key=>$value) {
			if (!isset($_REQUEST[$key])) {
				$$key = $value;
			}
		}
	} else {
		$action = 'Add';
	}
} else {
	$action = 'Add';
	$editRow = array();
}
$userDb = new account();
$userDb->select(null, array('WHERE'=>array('ID#'=>$_SESSION[APPLICATION]['user'])));
if ($userRow = $userDb->getnext()) {
	$editRow['REQID'] = $userRow['ID#'];
	$editRow['REQNAME'] = $userRow['FNAME'].' '.$userRow['LNAME'];
	$editRow['REQEMAIL'] = $userRow['EMAIL'];
	$editRow['REQPHONE'] = $userRow['PHONE'];
}
if (isset($userDb)) {
	unset($userDb);
}

$jsArray[] = 'js/vendorRequest.js';
$jsArray[] = 'js/jquery_ui.js';
$cssArray[] = 'styles/jquery-ui.1.9.2.css';
//$viewTitle = "$VENDORACTION Vendor";

$viewTitle = "Maintain Vendor";
if (array_key_exists('module',$_SESSION[APPLICATION]) and file_exists('modules/'.$_SESSION[APPLICATION]['module'].'/includes/view.php')) {
	include ('modules/'.$_SESSION[APPLICATION]['module'].'/includes/view.php');
} else {
	if (file_exists('includes/view.php')) {
		include ('includes/view.php');
	} else {
		base::missingPage('view.php');
	}
}
?>

