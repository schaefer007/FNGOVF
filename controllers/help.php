<?php 
$targetPage = $_REQUEST['helpView'];

$viewTitle = "Help (".str_replace('.php','',$targetPage).')';
// Find and include the header from the includes folder.

if (file_exists('includes/header.php')) {
	include('includes/header.php');
} else {
	base::missingPage('header.php');
}	

echo '<input type="button" name="backButtonBottom" value="Back to Content" onclick="document.backToContent.submit();"></input>';

// Find and include the view from the views folder.
if (array_key_exists('module', $_SESSION[APPLICATION]) and file_exists($_SESSION[APPLICATION]['module']."/controllers/".$targetPage)) {
	if (file_exists($_SESSION[APPLICATION]['module']."/views/help.$targetPage"))
		include($_SESSION[APPLICATION]['module']."/views/help.$targetPage");
	} else {
		if (file_exists('views/help.'.$targetPage)) {
			include('views/help.'.$targetPage);
		} else {
			base::missingPage('help');
		}
	}

?>
<form name="backToContent" method="post" action="<?php echo $_SERVER['PHP_SELF']?>">
<input type="hidden" name="view" value="<?php echo str_replace('.php','',$targetPage)?>"></input>
<input type="hidden" name="fromPage" value="<?php echo $fromPage;?>"></input>
<input type="hidden" name="NAME" value="<?php echo $NAME;?>"></input>
</form>
<input type="button" name="backButtonBottom" value="Back to Content" onclick="document.backToContent.submit();"></input>
<?php 
// Find and include the Footer from the includes folder. 
//  Footer is optional so no error message is displayed if
//  it cannot be found.

if (file_exists('includes/footer.php')) {
	include('includes/footer.php');
}

?>