<?php 
require_once('classes/class.account.php');
if (isset($_REQUEST['PASSWORD1'])) {
	$password1 = $_REQUEST['PASSWORD1'];
} else {
	$password1 = '';
}
if (isset($_REQUEST['PASSWORD22'])) {
	$password2 = $_REQUEST['PASSWORD2'];
} else {
	$password2 = '';
}
if (isset($_SESSION[APPLICATION]['user'])) {
	$ID = $_SESSION[APPLICATION]['user'];}
if (isset($ID)) {
	$editUser = new account();
	$editUser->select(null,array('WHERE'=>array('ID#'=>$ID)));
	if ($editRow = $editUser->getnext()) {
		$action = 'Edit';
		$passwordText = 'Password is optional.  If not provided the current password is kept.  ';
		$passwordReqd = '&nbsp;';
	} else {
		$passwordText = '';
		$action = 'Add';
		$passwordReqd = '*';
	}
} else {
	$action = 'Add';
	$editRow = array();
	$passwordText = '';
	$passwordReqd = '*';
}
$viewTitle = "$action User";
if (file_exists('modules/'.$_SESSION['module'].'/includes/view.php')) {
	include ('modules/'.$_SESSION['module'].'/includes/view.php');
} else {
	if (file_exists('includes/view.php')) {
		include ('includes/view.php');
	} else {
		base::missingPage('view.php');
	}
}
?>