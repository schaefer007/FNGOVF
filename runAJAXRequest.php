<?php
// initialze connections and setup session
include ('includes/initialize.php');
// establish user module for searching.
//error_log('calling AJAX function');
//error_log('running for '.$_REQUEST['AJAXFunction']);
if (array_key_exists('module',$_SESSION[APPLICATION])) {
	$module = $_SESSION[APPLICATION]['module'];
} else {
	$module = '';
}
if (isset($_REQUEST['AJAXFunction']) and file_exists('AJAX/'.$_REQUEST['AJAXFunction'].'.php')) {
	include('AJAX/'.$_REQUEST['AJAXFunction'].'.php');
}
//error_log('AJAX function complete');
?>