<div class="pageHeading">Database Servers - Help</div>
<div>

<div class="headingLvl1">Content</div>

<p class="indent1">The Servers page lists all servers currently defined for access.  The user and password assigned are not displayed but may be modified by using the edit function.  Any facility available by a form must connect to a valid Database Server or errors will occur when attempting to access information from that facility.</p>

<p class="indent1">For additional information regarding setting up the database servers, please press help on the Database Server entry/maintenance display.</p>	  

<div class="headingLvl1">Actions</div>
<p class="indent1">The following actions are available on this display</p>
<div class="headingLvl2">Add Database Server</div>
<p class="indent1">Add a new Database Server definition</p>
<div class="headingLvl2">Edit (by clicking the pencil icon)</div>
<p class="indent1">Edit the selected Database Server properties</p>
</div>