<script>
document.configForm.schema.focus();
</script>
<?php

require_once('classes/class.dropDown.php');

if (isset($errorText) and count($errorText) > 0) {
	echo "<div class=\"errorDiv\">";
	foreach($errorText as $message) {
		echo $message."<br>";
	}
	echo "</div>";
}
?>
<form name="configForm" method="post" action="<?php echo $_SERVER['PHP_SELF']?>">
<div class="formHeader">Configuration Settings</div>
<div class="formHeaderIndent">Database Settings</div>
<table width="100%">
<tr>
<td class="configFormLabel">Schema Name:</td>
<td class="fromData"><input type="text" name="schema" size="50" maxlength="50" value="<?php echo htmlentities($schema)?>"></td>
</tr>
</table>
<div class="formHeaderIndent">Email Settings</div>
<table width="100%">
<tr>
<td class="configFormLabel">Mail Host:</td>
<td class="formData"><input type="text" name="mailHost" size="50" maxlength="50" value="<?php echo $mailHost?>"></td>
</tr>
<tr>
<td class="configFormLabel">Use SSL:</td>
<td class="formData">
<?php
$mailSSlDD = new dropDown('mailSSl', array('N'=>'No', 'Y'=>'Yes'));
echo $mailSSlDD->bldDropDown($mailSSl);
unset($mailSSlDD);
?>
</td>
</tr>
<tr>
<td class="configFormLabel">Mail Port:</td>
<td class="formData"><input type="text" name="mailPort" size="5" maxlength="5" value="<?php echo $mailPort?>"></td>
</tr>
<tr>
<td class="configFormLabel">Mail Requires Authentication:</td>
<td class="formData">
<?php
$mailAuthDD = new dropDown('mailAuth', array('N'=>'No', 'Y'=>'Yes'));
echo $mailAuthDD->bldDropDown($mailAuth);
unset($mailAuthDD);
?>
</td>
</tr>
<td class="configFormLabel">Auth User:</td>
<td class="formData"><input type="text" name="mailUser" size="50" maxlength="50" value="<?php echo $mailUser?>"></td>
</tr>
<tr>
<td class="configFormLabel">Auth Pwd:</td>
<td class="formData"><input type="text" name="mailPwd" size="20" maxlength="20" value="<?php echo $mailPwd?>"></td>
</tr>
<tr>
<td class="configFormLabel">Mail From:</td>
<td class="formData"><input type="text" name="mailFrom" size="50" maxlength="50" value="<?php echo $mailFrom?>"></td>
</tr>
</table>
<div class="formHeaderIndent">Miscellaneous Settings</div>
<table width="100%">
<tr>
<td class="configFormLabel">Login Attemps Allowed:</td>
<td class="formData"><input type="text" name="loginattemptsallowed" size="2" maxlength="2" value="<?php echo $loginattemptsallowed?>"></td>
</tr>
<tr>
<td class="configFormLabel">Application Root:</td>
<td class="formData"><input type="text" name="applicationRoot" size="50" maxlength="50" value="<?php echo $applicationRoot?>"></td>
</tr>
<tr>
<td class="configFormLabel">Default Email Address:</td>
<td class="formData"><input type="text" name="defaultEmail" size="50" maxlength="50" value="<?php echo $defaultEmail?>"></td>
</tr>
<?php foreach ($availableForms as $formText=>$formController) {?>
<tr>
<td class="configFormLabel">Default Email for <?php echo $formText?>:</td>
<td class="formData"><input type="text" name="mail_<?php echo $formController?>" size="50" maxlength="50" value="<?php echo $formEmail[$formController]?>"></td>
</tr>
<?php }?>
<tr>
<td class="configFormLabel">Web URL:</td>
<td class="formData"><input type="text" name="webURL" size="50" maxlength="50" value="<?php echo $webURL?>"></td>
</tr>
</table>
	<div class="formHeaderIndent">Form Delay Settings</div>
	<table width="100%">
		<tr>
			<td class="configFormLabel">Notification Delay Time (minutes):</td>
			<td class="formData"><input type="text" name="delayTime"  size="4" maxlength="4" value="<?=$delayTime?>"></td>
		</tr>
		<tr>
			<td class="configFormLabel">Notification Email:</td>
			<td class="formData"><input type="text" name="email_first" size="50" maxlength="50" value="<?=$email_first?>"></td>
		</tr>
		<tr>
			<td class="configFormLabel">Notification Email:</td>
			<td class="formData"><input type="text" name="email_second" size="50" maxlength="50" value="<?=$email_second?>"></td>
		</tr>
		<tr>
			<td class="configFormLabel">Notification Email:</td>
			<td class="formData"><input type="text" name="email_third" size="50" maxlength="50" value="<?=$email_third?>"></td>
		</tr>
	</table>
<input type="submit" name="cmd" value="Save"></input>
<input type="hidden" name="view" value="config"></input>
</form>
</div>

</body>
</html>
