<?php
function showErrors() {
	global $errorText;
	if (is_array($errorText) and count($errorText) > 0) {
		echo "<div class=\"errorDiv\">";
		foreach ($errorText as $message) {
			echo "$message<br>";
		}
		echo "</div>";
	}
}
function showList() {
	global $databaseInfo;
	$idx = 0;
	foreach ($databaseInfo as $reportRow) {
			$idx++;
	$delimiter = "";
	echo "<tr id=\"row{$idx}\" onmouseover=\"javascript:qzhighlight('row{$idx}')\" onmouseout=\"javascript:qznormal('row{$idx}')\">\n",
		"<td>".$reportRow['DBNAME']."</td>\n",
		"<td>".$reportRow['DBDESC']."</td>\n",
        "<td>".$reportRow['DBUSER']."</td>\n",
        "<td>".$reportRow['DBPWD']."</td>\n",
        "<td><img src=\"plm/images/edit.png\" class=\"editImageNoBorder\" onclick=\"goToEdit(document.getElementById('databases'),'".$reportRow['DBNAME']."');\"></td>\n",
        "</tr>\n";
	}
}
?>
<div style="margin-bottom:0px;width:200px;float:left;">
<form style="margin-top:5pt; margin-bottom:0px;" name="databaseAddTop" method="post" action="<?php echo $_SERVER['PHP_SELF']?>">
<input type="hidden" name="view" value="databaseEdit"></input>
<input type="submit" name="cmd" value="Add Database"></input>
</form>
</div>
<div style="margin:0px;padding:0px;clear:both"></div>
</form>
<form name="databases" id="databases" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
<input type="hidden" name="view" value="databaseEdit"></input>
<input type="hidden" name="NAME" value=""></input>
<?php 
showErrors();
?>
<table width=100% cellspacing="8">
<tr class="tableHeading">
<th>Name (RDB)</th>
<th>Description</th>
<th>User</th>
<th>Password</th>
<th>&nbsp;</th>
</tr>
<?php 
showList();
?>
</table>
</form>
<form name="databaseAdd" method="post" action="<?php echo $_SERVER['PHP_SELF']?>">
<input type="hidden" name="view" value="databaseEdit"></input>
<input type="submit" name="cmd" value="Add Database"></input>
</form>
