<div class="pageHeading">Maintain Database Servers - Help</div>
<div>

<div class="headingLvl1">Content</div>

<p class="indent1">The Maintain Database Servers display is used to modify or add database servers that you may access.  Any facility used by a form must have an associated Database Server definition or errors will occur.
<div class="headingLvl2">Name (RDB)</div>
<p class="indent1">The server name uniquely identifies each server.  This must be the RDB Directory name established on the system for access to this server.
</p>
<div class="headingLvl2">Description</div>
<p class="indent1">Enter the discription that will help you identify this server.  This is used for documentation purposes only and does not affect any connections to the Database Server.</p>
<div class="headingLvl2">User</div>
<p class="indent1">Enter a User Id that is authorized to the iVP data on the Server.  This user name will be used to make connections to this server.  If you are maintaining an existing server definition, leaving this field blank retains the existing value.  If you are adding a new server definition, this field is required.</p>
<div class="headingLvl2">Password</div>
<p class="indent1">Enter the Password to use in combination with the user provided above for access to this server.  If you are maintaining an existing server definition, leaving the password fields blank will retain the existing value.  When adding a new server definition the password fields are required.</p>
<div class="headingLvl2">Confirm Password</div>
<p class="indent1">Re-Enter the password above.  Both password fields are checked and the password is only added/changed if both of them agree.</p>
<div class="headingLvl1">Actions</div>
<p class="indent1">The following Actions are available on this display:</p>
<div class="headingLvl2">Save</div>
<p class="indent1">Save the Server Definition</p>
<div class="headingLvl2">Delete</div>
<p class="indent1">Delete the Server Definition</p>
<div class="headingLvl2">Back to Servers</div>
<p class="indent1">Return to the list of Servers</p>
</div>