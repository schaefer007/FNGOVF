<div class="pageHeading">User List - Help</div>
<div>

<div class="headingLvl1">Content</div>
<p class="indent1"><img src="media/userList.jpg"></p>
<p class="indent1">The screen lists all users defined for this application.</p>

<div class="headingLvl1">Search</div>
<p class="indent1"><img src="media/userListSearch.jpg"></p>
<p class="headingLvl2" style="margin-bottom:0px;">Status</p>
<p class="indent1" style="margin-top:0px;">Select a status from the drop down list.  Only users in that Status will be display.  
This initially defaults to "Pending" so that pending users can be quickly found and activated for access.</p>
<p class="headingLvl2" style="margin-bottom:0px;">Last Name</p>
<p class="indent1" style="margin-top:0px;">Enter any portion of the users last name and 
click "Find" to get a list of users with a last name that contains the characters you entered.</p>
<p class="headingLvl2" style="margin-bottom:0px;">Primary Phone</p>
<p class="indent1" style="margin-top:0px;">Enter any portion of the users phone (such as their extension) and click 
"Find" to locate all users with matching phone numbers.</p>
<p class="indent1" style="margin-top:0px;">Both the phone and last name fields can be used at the same time to locate a user.  
To view all users again, clear the two search fields and click "Find" with them empty.</p>

<div class="headingLvl1">Actions</div>
<p class="headingLvl2" style="margin-bottom:0px;">Add User</p>
<p class="indent1" style="margin-top:0px;">Click the "Add User" button at the top or bottom of the screen to add a new user.</p>
<p class="headingLvl2" style="margin-bottom:0px;">Edit</p>
<p class="indent1" style="margin-top:0px;">Click the Edit button on any line to edit the user displayed on that line.</p>

</div>
