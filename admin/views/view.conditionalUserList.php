<?php
function showErrors() {
	global $errorText;
	if (is_array($errorText) and count($errorText) > 0) {
		echo "<div class=\"errorDiv\">";
		foreach ($errorText as $message) {
			echo "$message<br>";
		}
		echo "</div>";
	}
}
function showList() {
	global $conditionalUserInfo;
	$idx = 0;
	foreach ($conditionalUserInfo as $reportRow) {
			$idx++;
	$delimiter = "";
	echo "<tr id=\"row{$idx}\" onmouseover=\"javascript:qzhighlight('row{$idx}')\" onmouseout=\"javascript:qznormal('row{$idx}')\">\n",
 	    "<td>".$reportRow['strConditionName']."</td>\n",
		"<td>".$reportRow['strConditionText']."</td>\n",
	 	"<td>".$reportRow['strDisplayName']."</td>\n",
		"<td><img src=\"plm/images/edit.png\" class=\"editImageNoBorder\" onclick=\"goToEdit(document.getElementById('conditionalUsers'),'".$reportRow['intConditionalUserID']."');\"></td>\n",
        "</tr>\n";
	}
}
?>
<div id="contentLegend">Conditional Users</div>
<div style="clear:both;padding:0px;margin:0px;">&nbsp;</div>
<div style="margin-top:12pt;margin-bottom:0px;width:100px;">
<form style="margin-top:15pt; margin-bottom:0px;" name="conditionalUserAddTop" method="post" action="<?php echo $_SERVER['PHP_SELF']?>">
<input type="hidden" name="view" value="conditionalUserEdit"></input>
<input type="hidden" name="cmd" value="Add Conditional User"></input>
<input type="submit" value="Add Conditional User"></input>
<div style="position:relative; top: -23px; left: 800px; min-width:200px;">Sort By: <select name="condSort" onChange="this.form.cmd.value='';this.form.view.value='<?php echo $_REQUEST['view']?>';this.form.submit();"><option <?php if ($_SESSION[APPLICATION]['conditionSort'] == 'Condition') {echo 'selected';}?>>Condition</option><option <?php if ($_SESSION[APPLICATION]['conditionSort'] == 'User') {echo "selected";}?>>User</option></select></div>
</form>
</div>
<form name="conditionalUsers" id="conditionalUsers" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
<input type="hidden" name="view" value="conditionalUserEdit"></input>
<input type="hidden" name="NAME" value=""></input>
<?php
showErrors();
?>
<table id="conditionalUserList" width=100%>
<thead>
<tr class="tableHeading">
<th>Condition</th>
<th>Conditional Value</th>
<th>User</th>
<th>&nbsp;</th>
</tr>
</thead>
<tbody>
<?php
showList();
?>
</tbody>
</table>
</form>
<form name="conditionalUserAdd" method="post" action="<?php echo $_SERVER['PHP_SELF']?>">
<input type="hidden" name="view" value="conditionalUserEdit"></input>
<input type="submit" name="cmd" value="Add Conditional User"></input>
</form>
<script>
$("#conditionalUserList").chromatable({
	width: "940px",
	height: "320px",
	scrolling: "yes"});
</script>