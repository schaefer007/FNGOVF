<?php 
	$cssArray = array("plm/css/table.css.php", "plm/css/popup.css.php", "plm/css/workflow_manage.css.php", "plm/css/process_states.css.php", "plm/css/process_transitions.css.php");
	$jsArray = array("plm/javascript/popup.js.php", "plm/javascript/workflow.js.php");
	$g_strTitle = "Workflow Management";
	include_once("smarty.php");
	include_once("header.php");
	include_once("classes/WFProcess.class.php");

	$intWFProcessID = isset($_GET["intWFProcessID"])?$_GET["intWFProcessID"]:null;
	$objWFProcess = new WFProcess();
	$objWFProcess->loadForWorkflowManagement($intWFProcessID);

	$g_objSmarty->assign("objWFProcess", $objWFProcess);

	$objWFProcessArray = new WFProcessArray();
	$objWFProcessArray->load();
	$g_objSmarty->assign("objWFProcessArray", $objWFProcessArray);
	$g_objSmarty->display("forms_workflow_manage.tpl");
?>