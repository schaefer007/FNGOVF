<?php
function showErrors() {
	global $errorText;
	if (is_array($errorText) and count($errorText) > 0) {
		echo "<div class=\"errorDiv\">";
		foreach ($errorText as $message) {
			echo "$message<br>";
		}
		echo "</div>";
	}
}

?>
<div id="contentLegend"><?php echo $action?> Database</div>
<div style="clear:both;padding:0px;margin:0px;">&nbsp;</div>
<form name="databaseForm" id="databaseForm" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
<input type="hidden" name="view" value="<?php echo $_REQUEST['view']?>"></input>
<input type="hidden" name="action" value="<?php echo $action?>"></input>
<?php 
showErrors();

if (!isset($server)) {
  $server = new server();
}
echo '<table name="databaseTable">';
echo $server->renderFormData($databaseInfo,true);
echo $server->renderFormLine('DBPWD','',false, array('NAME'=>'DBPWD2','TEXT'=>'&nbsp;&nbsp; Confirm Password'));
echo '</table>';
?>
<input type="hidden" name="cmd" value="Save"></input>
<input type="submit" name="submitButton" value="Save"></input>
<input type="button" name="deleteButton" value="Delete" onclick="this.form.cmd.value = 'Delete'; this.form.action.value = 'Delete'; this.form.submit();"></input>
<input type="button" name="backButton" value="Back to Databases" onclick="goToView('databaseList');"></input>
</form>
