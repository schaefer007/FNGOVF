<div class="pageHeading">Configuration Maintenance - Help</div>
<div>

<div class="headingLvl1">Database Settings</div>
<p class="indent1"><img src="media/configurationDatabase.jpg"></p>
<p class="headingLvl2" style="margin-bottom:0px;">Schema Name</p>
<p class="indent1" style="margin-top:0px;">Schema Name contains the name of the library/schema on the system where the data is stored.
  Any change in this value requires corresponding changes in the database.  The default value for this setting is '"dbplm"'.  Note that
  in this instance the double quotes are part of the schema name and must be included.  This is dependant on the configuration and may not
  always be the case.  Since this application uses MySql tables (via the IBMDB2i storage engine) all schema and table names used inside
  MySql will be in lower case and surrounded by double quotes.</p>
  
<div class="headingLvl1">Email Settings</div>
<p class="indent1"><img src="media/configurationEmail.jpg"></p>
<p class="indent1">Enter the appropriate settings for the email server that will be used to send out email messages.
<p class="headingLvl2" style="margin-bottom:0px;">Mail Host</p>
<p class="indent1" style="margin-top:0px;">Enter the host name of the Email Server you will be using.</p>
<p class="headingLvl2" style="margin-bottom:0px;">Use SSL</p>
<p class="indent1" style="margin-top:0px;">Select "Yes" to use an SSL connection to the mail server or "No" to use an unencrypted connection.</p>
<p class="headingLvl2" style="margin-bottom:0px;">Mail Port</p>
<p class="indent1" style="margin-top:0px;">Enter the port number used to send mail.</p>
<p class="headingLvl2" style="margin-bottom:0px;">Mail Requires Authentication</p>
<p class="indent1" style="margin-top:0px;">Select "Yes" if the selected Email Server requires authenitcation.</p>
<p class="headingLvl2" style="margin-bottom:0px;">Auth User</p>
<p class="indent1" style="margin-top:0px;">Enter the user needed to log into the mail server.</p>
<p class="headingLvl2" style="margin-bottom:0px;">Auth Pwd</p>
<p class="indent1" style="margin-top:0px;">Enter the password to use when logging into the mail server.</p>
<p class="headingLvl2" style="margin-bottom:0px;">Mail From</p>
<p class="indent1" style="margin-top:0px;">Enter the email address used as the From address for Email being sent by the system.</p>
</p>

<div class="headingLvl1">Miscellaneous Settings</div>
<p class="indent1"><img src="media/configurationMiscellaneous.jpg"></p>
<p class="indent1">Enter the appropriate miscellaneous settings.
<p class="headingLvl2" style="margin-bottom:0px;">Login Attempts Allowed</p>
<p class="indent1" style="margin-top:0px;">Set the number of login attempts allowed for a given user Email address before that Email Address is marked "Inactive".
 When a user exceeds this number of failed login attempts, they will no longer be able to access the system until their password is reset.</p>
<p class="headingLvl2" style="margin-bottom:0px;">Applicatin Root</p>
<p class="indent1" style="margin-top:0px;">Define the path to the application root here.  This may be different than the document root or it may be the same depending upon
how the webserver and root folder structure are configured.</p>
<p class="headingLvl2" style="margin-bottom:0px;">Default Email Address</p>
<p class="indent1" style="margin-top:0px;">This setting defines a default email address that will appear as a contact email address in the page footer.  This value is 
used for any email contacts or emails sent that are not on a page specific to a given form.  A given form may use a separate email address if required.</p>
<p class="headingLvl2" style="margin-bottom:0px;">Default Email Address For . . .</p>
<p class="indent1" style="margin-top:0px;">There may be one or more lines for form specific email addresses based on the number of forms defined in the application.  Each
form can have it's own email address.  Each form MUST have an email address defined since the application will not default back to the global email address if it is left
blank.</p>
 <p class="headingLvl2" style="margin-bottom:0px;">Web URL</p>
<p class="indent1" style="margin-top:0px;">Enter the URL to the Vendor Portal here.  This is used when generating links to the site.</p>
</p>

</div>
