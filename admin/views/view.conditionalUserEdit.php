<?php
function showErrors() {
	global $errorText;
	if (is_array($errorText) and count($errorText) > 0) {
		echo "<div class=\"errorDiv\">";
		foreach ($errorText as $message) {
			echo "$message<br>";
		}
		echo "</div>";
	}
}
?>
<div id="contentLegend"><?php echo $viewTitle?></div>
<div style="clear:both;margin-top:20pt;padding-top:10pt;">&nbsp;</div>
<?php
showErrors();
?>
<form name="conditionalUser" id="conditionalUser" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
<input type="hidden" name="view" value="<?php echo $_REQUEST['view']?>"></input>
<input type="hidden" name="ID#" value="<?php echo $editRow['ID#']?>"></input>
<input type="hidden" name="action" value="<?php echo $action?>"></input>
<input type="hidden" name="formName" value="conditionalUser"></input>
<input type="hidden" name="updKey" value="<?php echo mktime()?>"></input>
<input type="hidden" name="NAME" value="<?php echo $Name?>"></input>
<table width=100%>
<?php
if (!isset($editConditionalUser)) {
	$editConditionalUser = new tblconditionaluser();
}
echo $editConditionalUser->renderFormData($editRow,false);
?>
</table>
<div>&nbsp;</div>
<input type="hidden" name="cmd"></input>
<input type="button" name="saveButton" value="Save" onclick="this.form.cmd.value = 'Save';this.form.submit();"></input>
<?php if ($action == 'Edit') {?>
<input type="button" name="deleteButton" value="Delete" onclick="this.form.cmd.value = 'Delete';this.form.submit();"></input>
<?php }?>
<input type="button" name="backButton" value="Back to Conditional Users" onclick="goToView('conditionalUserList');"></input>
</form>
