<?php
function showErrors() {
	global $errorText;
	if (is_array($errorText) and count($errorText) > 0) {
		echo "<div class=\"errorDiv\">";
		foreach ($errorText as $message) {
			echo "$message<br>";
		}
		echo "</div>";
	}
}
function showList() {
	global $userInfo;
	$idx = 0;
	foreach ($userInfo as $reportRow) {
			$idx++;
	$delimiter = "";
	$Phone = "";
    if (trim($reportRow['PHONE'])) {
   	    $Phone .= trim($reportRow['PHONE']);
   	    if (trim($reportRow['EXT'])) {
   	    	$Phone .= ' x: '.trim($reportRow['EXT']);
   	    }
    	$delimiter = "<br>";
    }
    if (trim($reportRow['MOBILE'])) {
    	$Phone .= $delimiter.'Cell: '.trim($reportRow['MOBILE']);
    	$delimiter = "<br>";
    }
    if (trim($reportRow['FAX'])) {
    	$Phone .= $delimiter.'Fax: '.trim($reportRow['FAX']);
    	$delimiter = "<br>";
    }

    $lastLoginTimestamp = DateTime::createFromFormat('Y-m-d-H.i.s', substr($reportRow['LASTLOGIN'],0,19));
	echo "<tr id=\"row{$idx}\" onmouseover=\"javascript:qzhighlight('row{$idx}')\" onmouseout=\"javascript:qznormal('row{$idx}')\">\n",
 		"<td align=\"center\">".$reportRow['FNAME']." ".$reportRow['LNAME']."</td>\n",
	    "<td>".$reportRow['NICKNAME']."</td>\n",
        "<td style=\"white-space:nowrap;\">".$Phone."</td>\n",
		"<td>".$reportRow['EMAIL']."</td>\n",
	 	"<td>".$reportRow['AUTHORITY']."</td>\n",
		"<td style=\"white-space:nowrap;\">".$lastLoginTimestamp->format('Y-m-d h:i:s a')."</td>\n",
		"<td>".$reportRow['STATUS']."</td>\n",
		"<td><img src=\"plm/images/edit.png\" class=\"editImageNoBorder\" onclick=\"goToEdit(document.getElementById('users'),'".$reportRow['ID#']."');\"></td>\n",
        "</tr>\n";
	}
}
?>
<div id="contentLegend">Users</div>
<div style="clear:both;padding:0px;margin:0px;">&nbsp;</div>
<div style="margin-top:12pt;margin-bottom:0px;width:100px;float:left;">
<form style="margin-top:15pt; margin-bottom:0px;" name="userAddTop" method="post" action="<?php echo $_SERVER['PHP_SELF']?>">
<input type="hidden" name="view" value="userEdit"></input>
<input type="submit" name="cmd" value="Add User"></input>
</form>
</div>
<div style="margin-top:12pt;margin-bottom:0px;width:80%;float:right;text-align:right;>
<p style="text-align:right;">
<form style"margin-top:15pt; margin-bottom:0px;" name="findForm" method="post" action="<?php echo $_SERVER['PHP_SELF']?>">
Status:<select style="margin-right:10pt;" name="s_STATUS" onchange="this.form.submit()";><option value="" <?php echo $noneSelected?>>All</option><option value="P" <?php echo $pSelected?>>Pending</option><option value="A" <?php echo $aSelected?>>Active</option><option value="I" <?php echo $iSelected?>>Inactive</option><option value="D" <?php echo $dSelected?>>Disabled</option></select>
Last Name:<input style="margin-right:10pt;" type="text" name="s_LNAME" value="<?php echo $s_LName?>">
Primary Phone:<input style="margin-right:10pt;" type="text" name="s_PHONE" value="<?php echo $s_Phone?>" onkeydown="javascript:backspacerDOWN(this,event);" onkeyup="javascript:backspacerUP(this,event);">
<input style="margin-right:10pt;" type="submit" name="find" value="Find">
<input type="hidden" name="view" value="<?php echo $view?>"></input>
</form>
</p>
</div>
<div style="margin:0px;padding:0px;clear:both"></div>
</form>
<form name="users" id="users" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
<input type="hidden" name="view" value="userEdit"></input>
<input type="hidden" name="NAME" value=""></input>
<?php 
showErrors();
?>
<table id="userList" width=100%>
<thead>
<tr class="tableHeading">
<th>Name</th>
<th>Nickname</th>
<th>Phone</th>
<th>Email Address</th>
<th>Authority</th>
<th>Last Login</th>
<th>Status</th>
<th>&nbsp;</th>
</tr>
</thead>
<tbody>
<?php 
showList();
?>
</tbody>
</table>
</form>
<form name="userAdd" method="post" action="<?php echo $_SERVER['PHP_SELF']?>">
<input type="hidden" name="view" value="userEdit"></input>
<input type="submit" name="cmd" value="Add User"></input>
</form>
<script>
$("#userList").chromatable({
	width: "940px",
	height: "320px",
	scrolling: "yes"});
</script>