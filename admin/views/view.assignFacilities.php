<?php
if (!function_exists('showErrors')) {
function showErrors() {
	global $errorText;
	if (is_array($errorText) and count($errorText) > 0) {
		echo "<div class=\"errorDiv\">";
		foreach ($errorText as $message) {
			echo "$message<br>";
		}
		echo "</div>";
	}
}
} 
function showFacilitiesList() {
	global $userInfo;
	global $facilitiesInfo;
	$idx = 0;
	foreach ($facilitiesInfo as $reportRow) {
			$idx++;
	$delimiter = "";
	if (array_key_exists('checked',$reportRow) and $reportRow['checked'] == true) {
		$checked = "checked";
	} else {
		$checked = "";
	}

	echo "<tr id=\"row{$idx}\" onmouseover=\"javascript:qzhighlight('row{$idx}')\" onmouseout=\"javascript:qznormal('row{$idx}')\">\n",
    	"<td><input type=\"checkbox\" name=\"facilityId[]\" value=\"".$reportRow['FWDATC'].':'.$reportRow['FWPLTC'].':'.$reportRow['FWGLCO']."\" $checked></td>\n",
		"<td>".$reportRow['FWDATC']."</td>\n",
	    "<td>".$reportRow['FWDATBMN']."</td>\n",
        "<td>".$reportRow['FWPLTC']."</td>\n",
		"<td>".$reportRow['FWGLCO']."</td>\n",
        "</tr>\n";
	}
}
?>
<div id="contentLegend">Assign User Facilities</div>
<div style="clear:both;padding:0px;margin:0px;">&nbsp;</div>
<div style="margin:0px;padding:0px;clear:both"></div>
</form>
<form name="userFacilities" id="userFacilities" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
<input type="hidden" name="view" value="userEdit"></input>
<input type="hidden" name="NAME" value="<?php echo $editRow['ID#']?>"></input>
<input type="hidden" name="ID#" value="<?php echo $tblUserRow['intUserID']?>"></input>
<input type="hidden" name="formName" value="userFacilities"></input>
<input type="hidden" name="oldFacilities" value="<?php if (isset($userFacilities) and is_array($userFacilities)) {echo implode(",",$userFacilities);}?>"></input>
<input type="hidden" name="updKey" value="<?php echo mktime()?>"></input>
<?php 
showErrors();
?>
<table id="assignUserFacilitiesList" width=100%>
<thead>
<tr class="tableHeading">
<th><input type="checkbox" id="checkall" onclick="checkAll(this,this.form.id)">check/uncheck All</th>
<th>Database</th>
<th>Description</th>
<th>Plant</th>
<th>Company</th>
</tr>
</thead>
<tbody>
<?php 
showFacilitiesList();
?>
</tbody>
</table>
<input type="submit" name="cmd" value="Save"></input>
<input type="reset" name="reset" value="Reset"></input>
</form>
<script>
$("#assignUserFacilitiesList").chromatable({
	width: "940px",
	height: "320px",
	scrolling: "yes"});
</script>