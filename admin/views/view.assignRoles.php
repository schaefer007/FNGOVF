<?php
if (!function_exists('showErrors')) {
function showErrors() {
	global $errorText;
	if (is_array($errorText) and count($errorText) > 0) {
		echo "<div class=\"errorDiv\">";
		foreach ($errorText as $message) {
			echo "$message<br>";
		}
		echo "</div>";
	}
}
} 
function showRolesList() {
	global $userInfo;
	global $roleInfo;
	$idx = 0;
	foreach ($roleInfo as $reportRow) {
			$idx++;
	$delimiter = "";
	if (array_key_exists('checked',$reportRow) and $reportRow['checked'] == true) {
		$checked = "checked";
	} else {
		$checked = "";
	}
	switch($reportRow['strStatus']) {
		case 1	:   $roleStatus = 'Active';
					break;
		case 2  :	$roleStatus = 'Disabled';
					break;
		default :	$roleStatus = "";
					break;
	}
	echo "<tr id=\"row{$idx}\" onmouseover=\"javascript:qzhighlight('row{$idx}')\" onmouseout=\"javascript:qznormal('row{$idx}')\">\n",
    	"<td><input type=\"checkbox\" name=\"roleId[]\" value=\"".$reportRow['intRoleID']."\" $checked></td>\n",
		"<td align=\"center\">".$reportRow['strRoleName']."</td>\n",
	    "<td>".$roleStatus."</td>\n",
        "<td>".$reportRow['blnPlantLevel']."</td>\n",
		"<td>".$reportRow['blnOverrideProgramSecurity']."</td>\n",
        "</tr>\n";
	}
}
?>
<div id="contentLegend">Assign User Roles</div>
<div style="clear:both;padding:0px;margin:0px;">&nbsp;</div>
<div style="margin:0px;padding:0px;clear:both"></div>
</form>
<form name="userRoles" id="userRoles" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
<input type="hidden" name="view" value="userEdit"></input>
<input type="hidden" name="NAME" value="<?php echo $editRow['ID#']?>"></input>
<input type="hidden" name="ID#" value="<?php echo $tblUserRow['intUserID']?>"></input>
<input type="hidden" name="formName" value="userRoles"></input>
<input type="hidden" name="oldRoles" value="<?php if (isset($userRoles) and is_array($userRoles)) {echo implode(",",$userRoles);}?>"></input>
<input type="hidden" name="updKey" value="<?php echo mktime()?>"></input>
<?php 
showErrors();
?>
<table id="assignUserRolesList" width=100%>
<thead>
<tr class="tableHeading">
<th><input type="checkbox" id="checkall" onclick="checkAll(this,this.form.id)">check/uncheck All</th>
<th>Role</th>
<th>Status</th>
<th>Plant Level</th>
<th>Override Program Security</th>
</tr>
</thead>
<tbody>
<?php 
showRolesList();
?>
</tbody>
</table>
<input type="submit" name="cmd" value="Save"></input>
<input type="reset" name="reset" value="Reset"></input>
</form>
<script>
$("#assignUserRolesList").chromatable({
	width: "940px",
	height: "320px",
	scrolling: "yes"});
</script>