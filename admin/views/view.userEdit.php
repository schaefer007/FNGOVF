<?php 
function showErrors() {
	global $errorText;
	if (is_array($errorText) and count($errorText) > 0) {
		echo "<div class=\"errorDiv\">";
		foreach ($errorText as $message) {
			echo "$message<br>";
		}
		echo "</div>";
	}
} 
if (isset($_REQUEST['PASSWORD1'])) {
	$password1 = trim($_REQUEST['PASSWORD1']);
} else {
	$password1 = '';
}
if (isset($_REQUEST['PASSWORD2'])) {
	$password2 = trim($_REQUEST['PASSWORD2']);
} else {
	$password2 = '';
}
?> 
<div id="contentLegend"><?php echo $viewTitle?></div>
<div style="clear:both;margin-top:20pt;padding-top:10pt;"><p><?php echo $passwordText?>Passwords must be at least 5 characters long</p></div>
<?php 
showErrors();
?> 
<form name="user" id="user" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
<input type="hidden" name="view" value="<?php echo $_REQUEST['view']?>"></input>
<input type="hidden" name="ID#" value="<?php echo $editRow['ID#']?>"></input>
<input type="hidden" name="action" value="<?php echo $action?>"></input>
<input type="hidden" name="formName" value="user"></input>
<input type="hidden" name="updKey" value="<?php echo mktime()?>"></input>
<input type="hidden" name="NAME" value="<?php echo $Name?>"></input>
<table width=100%>
<?php 
if (!isset($editUser)) {
	$editUser = new account();
}
echo $editUser->renderFormLine('EMAIL',trim($editRow['EMAIL']),true)."\n";
echo $editUser->renderFormLine('FNAME',trim($editRow['FNAME']),false)."\n";
echo $editUser->renderFormLine('LNAME',trim($editRow['LNAME']),false)."\n";
echo $editUser->renderFormLine('NICKNAME',trim($editRow['NICKNAME']),false)."\n";
echo $editUser->renderFormLine('PHONE',trim($editRow['PHONE']),false)."\n";
echo $editUser->renderFormLine('EXT',trim($editRow['EXT']),false)."\n";
echo $editUser->renderFormLine('MOBILE',trim($editRow['MOBILE']),false)."\n";
echo $editUser->renderFormLine('FAX',trim($editRow['FAX']),false)."\n";
echo $editUser->renderFormLine('AUTHORITY',$editRow['AUTHORITY'],false)."\n";
echo $editUser->renderFormLine('PASSWORD',$password1,false,array('TEXT'=>"$passwordReqd Password",'NAME'=>'PASSWORD1'))."\n";
echo $editUser->renderFormLine('PASSWORD',$password2,false,array('TEXT'=>"$passwordReqd Re-Enter Password",'NAME'=>'PASSWORD2'))."\n";
echo $editUser->renderFormLine('PCHNGREQ',trim($editRow['PCHNGREQ']),false)."\n";
echo $editUser->renderFormLine('STATUS',trim($editRow['STATUS']),false)."\n";
?>
</table>
<div>&nbsp;</div>
<input type="hidden" name="cmd"></input>
<input type="button" name="saveButton" value="Save" onclick="this.form.cmd.value = 'Save';this.form.submit();"></input>
<input type="button" name="backButton" value="Back to Users" onclick="goToView('userList');"></input>
<input type="button" name="ResetButton" value="Reset Password" onclick="this.form.action.value = 'Reset';this.form.cmd.value = 'Save'; this.form.submit();"></input>
<?php
if (trim($editRow['STATUS']) == 'P') {
?>
<input type="button" name="Delete" value="Delete" onclick="this.form.cmd.value = 'Delete';this.form.submit();"></input>
<?php
}
?>
</form>
<?php if ($action != 'Add' and $editRow['STATUS'] <> 'P') {?>
<hr class="divider">
<?php 
if (file_exists($_SESSION[APPLICATION]['module']."/controllers/assignRoles.php")) {
  $_REQUEST['AJAX'] = 'Y';
  if (isset($errorText)) {
	 unset($errorText);
  }
  include ($_SESSION[APPLICATION]['module']."/controllers/assignRoles.php");
  } else {
echo "assignRoles not found<br>";
}
?>
<hr class="divider">
<?php 
if (file_exists($_SESSION[APPLICATION]['module']."/controllers/assignFacilities.php")) {
	$_REQUEST['AJAX'] = 'Y';
	if (isset($errorText)) {
	 unset($errorText);
	}
	include ($_SESSION[APPLICATION]['module']."/controllers/assignFacilities.php");
} else {
	echo "assignFacilities not found<br>";
}
}
?>