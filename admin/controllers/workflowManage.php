<?php 
$viewTitle = 'Manage Workflows';
set_include_path(get_include_path().PATH_SEPARATOR.'/www/zendsvr/htdocs/FNG/plm/classes/'.PATH_SEPARATOR.'/www/zendsvr/htdocs/FNG/plm/');
require_once('User.class.php');
$user = new User();
$user->load($_SESSION['intUser']);
$_SESSION['objUser'] = $user;
	$blnIsIE = (isset($_SERVER['HTTP_USER_AGENT']) && (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false));
	$cssToLoad = array("index.css.php","style.css.php","common.css.php","common_allsites.css.php","header.css.php","jquery-ui.css.php","table.css.php", "popup.css.php", "workflow_manage.css.php", "process_states.css.php", "process_transitions.css.php");
	if($blnIsIE) $cssToLoad[] = "ie_only.css.php";
	//$cssToLoad = array("plm/css.php?&amp;arrCSS&#91;&#93;=index.css.php&amp;arrCSS&#91;&#93;=style.css.php&amp;arrCSS&#91;&#93;=common.css.php&amp;arrCSS&#91;&#93;=common_allsites.css.php&amp;arrCSS&#91;&#93;=header.css.php&amp;arrCSS&#91;&#93;=jquery-ui.css.php&amp;arrCSS&#91;&#93;=popup.css.php");
	$jsToLoad[] = "workflow.js.php";
	$jsToLoad[] = "jquery.js.php";
	$jsToLoad[] = "jquery-ui.js.php";
	$jsToLoad[] = "header.js.php";
	$jsToLoad[] = "common_allsites.js.php";
	$jsToLoad[] = "popup.js.php";
	$cssInclude = "plm/css.php?";
	foreach ($cssToLoad as $cssFile) {
		$cssInclude .= '&amp;arrCSS&#91;&#93;='.$cssFile;
	}
	$cssArray = array($cssInclude);
	$jsInclude = "plm/js.php?";
	foreach ($jsToLoad as $jsFile) {
		$jsInclude .= '&amp;arrJS&#91;&#93;='.$jsFile;
	}
	$jsArray = array($jsInclude);
if (file_exists($_SESSION[APPLICATION]['module'].'/includes/view.php')) {
	include ($_SESSION[APPLICATION]['module'].'/includes/view.php');
} else {
	if (file_exists('includes/view.php')) {
		include ('includes/view.php');
	} else {
		base::missingPage('view.php');
	}
}
?>