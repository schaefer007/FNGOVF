<?php 
$schema = "";
$mailFrom = "";
$mailHost = "";
$mailAuth = "";
$mailPort = "";
$mailUser = "";
$mailPwd = "";
$mailSSl = "";
$loginattemptsallowed = 0;
$webURL = "";
$defaultEmail = "";
$applicationRoot = "";
unset ($formEmail);
foreach ($availableForms as $formText=>$formController) {
	$formEmail[$formController] = "";
}

if (isset($config)) {
    if (array_key_exists('schema',$config)) {
    	$schema = $config['schema'];
    }
    if (array_key_exists('loginattemptsallowed',$config)) {
    	$loginattemptsallowed = $config['loginattemptsallowed'];
    }
    if (array_key_exists('webURL',$config)) {
    	$webURL = $config['webURL'];
    }
	if (array_key_exists('applicationRoot', $config)) {
		$applicationRoot = $config['applicationRoot'];
	}
	if (array_key_exists('defaultEmail',$config)) {
		$defaultEmail = $config['defaultEmail'];
	}
	foreach ($availableForms as $formText=>$formController) {
		if (array_key_exists($formController,$config['formEmail'])) {
			$formEmail[$formController] = $config['formEmail'][$formController];
		} 
	}
	if (array_key_exists('mail',$config)) {
		if (array_key_exists('from', $config['mail'])) {
			$mailFrom = $config['mail']['from'];
		}
		if (array_key_exists('host', $config['mail'])) {
			if (strpos($config['mail']['host'],'ssl://') !== false) {
				$mailHost = substr($config['mail']['host'],6);
			} else {
				$mailHost = $config['mail']['host'];
			}
		}
		if (array_key_exists('auth', $config['mail'])) {
			if ($config['mail']['auth'] === true) {
				$mailAuth = 'Y';
			} else {
				$mailAuth = 'N';
			}
	    if (array_key_exists('ssl', $config['mail'])) {
	    	if ($config['mail']['ssl'] === true) {
	    		$mailSSl = 'Y';
	    	} else {
	    		$mailSSl = 'N';
	    	}
	    }
		}
		if (array_key_exists('port',$config['mail'])) {
			$mailPort = $config['mail']['port'];
		}
		if (array_key_exists('username', $config['mail'])) {
			$mailUser = $config['mail']['username'];
		}
		if (array_key_exists('password', $config['mail'])) {
			$mailPwd = $config['mail']['password'];
		}
	}
	if(array_key_exists('delayTime',$config)) {
		$delayTime = $config['delayTime'];
	}
	if(array_key_exists('email',$config)) {
		if (array_key_exists('first',$config['email'])) {
			$email_first = $config['email']['first'];
		}
		if (array_key_exists('second',$config['email'])) {
			$email_second = $config['email']['second'];
		}
		if (array_key_exists('third',$config['email'])) {
			$email_third = $config['email']['third'];
		}
	}
}

$viewTitle = "Maintain Configuration Options";
if (file_exists('modules/'.$_SESSION['module'].'/includes/view.php')) {
	include ('modules/'.$_SESSION['module'].'/includes/view.php');
} else {
	if (file_exists('includes/view.php')) {
		include ('includes/view.php');
	} else {
		base::missingPage('view.php');
	}
}
?>