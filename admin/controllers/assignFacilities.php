<?php 
require_once('classes/class.account.php');
require_once('classes/class.fwcompid.php');
require_once('classes/class.userfacilityxr.php');
require_once('classes/class.tbluser.php');

if (!isset($ID) and isset($_REQUEST['NAME'])) {
	$ID = $_REQUEST['NAME'];
}

$where = null;

$userDb = new account();
$userInfo = array();
if ($userDb->select(null,array('WHERE'=>array('ID#'=>$ID)))) {
	if ($userRow = $userDb->getnext()) {
		$userInfo = $userRow;
	}
} 
if (isset($userDb)) {
	unset($userDb);
}
$tblUser = new tbluser();
$userFacilities = array();
if ($tblUser->select(null,array('WHERE'=>array('strEmail'=>$userInfo['EMAIL'])))) {
	if ($tblUserRow = $tblUser->getnext()) {
		$tblUserFacility = new userfacilityxr();
		if ($tblUserFacility->select(null,array('WHERE'=>array('userID'=>$tblUserRow['intUserID'])))) {
			while($userFacilityRow = $tblUserFacility->getnext()) {
				$userFacilities[] = $userFacilityRow['DBase'].':'.$userFacilityRow['Plant'].':'.$userFacilityRow['Company'];
			}
		}
	}
}

if (isset($tblUser)) {
	unset($tblUser);
}
$facilities = new fwcompid();
if ($facilities->select(null,null)) {
	while($facilitiesRow = $facilities->getnext()) {
		if (trim($facilitiesRow['FWFUT05']) != 'I') {
			$facilitiesInfo[] = $facilitiesRow;
			if (in_array($facilitiesRow['FWDATC'].':'.$facilitiesRow['FWPLTC'].':'.$facilitiesRow['FWGLCO'],$userFacilities)) {
				$facilitiesInfo[count($facilitiesInfo)-1]['checked'] = true;
			}
		}
	}
} 
if (isset($facilities)) {
	unset($facilities);
}

$viewTitle = 'Assign User Facilities';
$saveTarget = $targetPage;
$targetPage = 'assignFacilities.php';
if (file_exists($_SESSION[APPLICATION]['module'].'/includes/view.php')) {
	include ($_SESSION[APPLICATION]['module'].'/includes/view.php');
} else {
	if (file_exists('includes/view.php')) {
		include ('includes/view.php');
	} else {
		base::missingPage('view.php');
	}
}
$targetPage = $saveTarget;

?>