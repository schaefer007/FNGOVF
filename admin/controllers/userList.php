<?php 
require_once('classes/class.account.php');
if (isset($_REQUEST['s_LNAME'])) {
	$s_LName = $_REQUEST['s_LNAME'];
} else {
	if (isset($_SESSION[APPLICATION]['s_LName'])) {
		$s_LName = $_SESSION[APPLICATION]['s_LName'];
	} else {
		$s_LName = "";
	}
}
if (isset($_REQUEST['s_PHONE'])) {
	$s_Phone = $_REQUEST['s_PHONE'];
} else {
	if (isset($_SESSION[APPLICATION]['s_Phone'])) {
		$s_Phone = $_SESSION[APPLICATION]['s_Phone'];
	} else {
		$s_Phone = "";
	}
}
if (isset($_REQUEST['s_STATUS'])) {
	$s_Status = $_REQUEST['s_STATUS']; 
} else {
	if (isset($_SESSION[APPLICATION]['s_Status'])) {
		$s_Status = $_SESSION[APPLICATION]['s_Status'];
	} else {
		$s_Status = "P";
	}
}
$_SESSION[APPLICATION]['s_LName'] = $s_LName;
$_SESSION[APPLICATION]['s_Phone'] = $s_Phone;
$_SESSION[APPLICATION]['s_Status'] = $s_Status;
$where = null;
$delimiter = ' Where ';
if (isset($_REQUEST['find'])) {
	if ($s_LName != "") {
		$where .= $delimiter.' LNAME like \'%'.trim($s_LName).'%\'';
		$delimiter = ' and ';
	//	$where['WHERE']['LNAME'] = $s_LName;
	}
	if ($s_Phone != "") {
		$where .= $delimiter.' PHONE like \'%'.trim($s_Phone).'%\'';
		$delimiter = ' and ';
	//	$where['WHERE']['PHONE'] = $s_Phone;
	}
}
if ($s_Status != "") {
	$where .= $delimiter.' STATUS = \''.$s_Status.'\'';
	//$where['WHERE']['STATUS'] = $s_Status;
}
$pSelected = "";
$aSelected = "";
$iSelected = "";
$dSelected = "";
$noneSelected = "";
if (isset($s_Status)) {
	switch($s_Status) {
		case 'P':	$pSelected = 'selected';
				    break;
		case 'A':	$aSelected = 'selected';
					break;
		case 'I':	$iSelected = 'selected';
					break;
		case 'D':   $dSelected = 'selected';
					break;
		default:    $noneSelected = 'selected';
					break;
	}
}
//$where['ORDERBY'] = array('LNAME','FNAME');
$where .= ' Order By FNAME,LNAME';
$userDb = new account();
$userInfo = array();
if ($userDb->select(null,null,$where)) {
	while ($userRow = $userDb->getnext()) {
		$userInfo[] = $userRow;
	}
}
$viewTitle = 'Users';
if (file_exists($_SESSION[APPLICATION]['module'].'/includes/view.php')) {
	include ($_SESSION[APPLICATION]['module'].'/includes/view.php');
} else {
	if (file_exists('includes/view.php')) {
		include ('includes/view.php');
	} else {
		base::missingPage('view.php');
	}
}

?>