<?php 
require_once('classes/class.server.php');

if (isset($_REQUEST['NAME'])) {
	$databaseName = $_REQUEST['NAME'];
} else {
	$databaseName = "";
}

$server = new server();
$server->select(array("DBNAME","DBDESC"),array('WHERE'=>array('DBNAME'=>$databaseName)));
if ($databaseInfo = $server->getnext()) {
	$action = 'Edit';
} else {
	$action = 'Add';
}
$viewTitle = 'Maintain Database Servers';
include ('includes/view.php');

if (isset($server)) {
	unset($server);
}
?>