<?php
require_once('classes/class.tblconditionaluser.php');

if (isset($_REQUEST['NAME'])) {
	$ID = $_REQUEST['NAME'];}
	$Name = $_REQUEST['NAME'];
if ((!isset($ID) or trim($ID) =='') and isset($_REQUEST['NAME'])) {
	$ID = $_REQUEST['NAME'];}
if (isset($ID) and trim($ID) != '') {
	$editConditionalUser = new tblconditionaluser();
	$editConditionalUser->select(null,array('WHERE'=>array('intConditionalUserID'=>$ID)));
	if ($editRow = $editConditionalUser->getnext()) {
		$action = 'Edit';
	} else {
		$action = 'Add';
	}
} else {
	$action = 'Add';
	$editRow = array();
}
$viewTitle = "$action Conditional User";
if (file_exists($_SESSION[APPLICATION]['module'].'/includes/view.php')) {
	include ($_SESSION[APPLICATION]['module'].'/includes/view.php');
} else {
	if (file_exists('includes/view.php')) {
		include ('includes/view.php');
	} else {
		base::missingPage('view.php');
	}
}
?>