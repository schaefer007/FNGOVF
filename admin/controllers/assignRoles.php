<?php 
require_once('classes/class.account.php');
require_once('classes/class.tbluser.php');
require_once('classes/class.tblrole.php');
require_once('classes/class.tbluserrolexr.php');

if (!isset($ID) and isset($_REQUEST['NAME'])) {
	$ID = $_REQUEST['NAME'];
}

$where = null;

$userDb = new account();
$userInfo = array();
if ($userDb->select(null,array('WHERE'=>array('ID#'=>$ID)))) {
	if ($userRow = $userDb->getnext()) {
		$userInfo = $userRow;
	}
} 
if (isset($userDb)) {
	unset($userDb);
}
$tblUser = new tbluser();
$userRoles = array();
if ($tblUser->select(null,array('WHERE'=>array('strEmail'=>$userInfo['EMAIL'])))) {
	if ($tblUserRow = $tblUser->getnext()) {
		$tblUserRole = new tbluserrolexr();
		if ($tblUserRole->select(null,array('WHERE'=>array('intUserID'=>$tblUserRow['intUserID'])))) {
			while($userRoleRow = $tblUserRole->getnext()) {
				$userRoles[] = $userRoleRow['intRoleID'];
			}
		}
	}
}

if (isset($tblUser)) {
	unset($tblUser);
}
$tblRole = new tblrole();
if ($tblRole->select(null,array('WHERE'=>array('strStatus'=>1)))) {
	while($roleRow = $tblRole->getnext()) {
		$roleInfo[] = $roleRow;
		if (in_array($roleRow['intRoleID'],$userRoles)) {
			$roleInfo[count($roleInfo)-1]['checked'] = true;
		}
	}
} 
if (isset($tblRole)) {
	unset($tblRole);
}

$viewTitle = 'Assign User Roles';
$saveTarget = $targetPage;
$targetPage = 'assignRoles.php';
if (file_exists($_SESSION[APPLICATION]['module'].'/includes/view.php')) {
	include ($_SESSION[APPLICATION]['module'].'/includes/view.php');
} else {
	if (file_exists('includes/view.php')) {
		include ('includes/view.php');
	} else {
		base::missingPage('view.php');
	}
}
$targetPage = $saveTarget;

?>