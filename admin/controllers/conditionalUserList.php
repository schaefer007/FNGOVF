<?php
require_once('classes/class.tblconditionaluser.php');
require_once('classes/class.tbluser.php');
function cmp($a, $b)
{
    if ($a['strDisplayName'] == $b['strDisplayName']) {
        return 0;
    }
    return ($a['strDisplayName'] < $b['strDisplayName']) ? -1 : 1;
}

$conditionalUserDb = new tblconditionaluser();
$conditionalUserInfo = array();
if ($conditionalUserDb->select(null,null)) {
	while ($conditionalUserRow = $conditionalUserDb->getnext()) {
		$conditionalUserInfo[] = $conditionalUserRow;
	}
}
if (isset($conditionalUserDb)) {
	unset($conditionalUserDb);
}
$idx = 0;
$userDb = new tbluser();
foreach ($conditionalUserInfo as $row) {
	$userDb->select(array('"strDisplayName"'),array('WHERE'=>array('intUserID'=>$row['intUserID'])));
	if ($userRow = $userDb->getnext()) {
		$conditionalUserInfo[$idx]['strDisplayName'] = $userRow['strDisplayName'];
	}
	$idx++;
}
if (isset($userDb)) {
	unset($userDb);
}
if (isset($_REQUEST['condSort'])) {
    $_SESSION[APPLICATION]['conditionSort'] = $_REQUEST['condSort'];
} else {
    if (!isset($_SESSION[APPLICATION]['conditionSort'])) {
        $_SESSION[APPLICATION]['conditionSort'] == 'Condition';
    }
}
if ($_SESSION[APPLICATION]['conditionSort'] == 'User') {
    usort($conditionalUserInfo, "cmp");
}
$viewTitle = 'Conditional Users';
if (file_exists($_SESSION[APPLICATION]['module'].'/includes/view.php')) {
	include ($_SESSION[APPLICATION]['module'].'/includes/view.php');
} else {
	if (file_exists('includes/view.php')) {
		include ('includes/view.php');
	} else {
		base::missingPage('view.php');
	}
}

?>