<?php 
require_once('classes/class.account.php');

if (isset($_REQUEST['NAME'])) {
	$ID = $_REQUEST['NAME'];}
	$Name = $_REQUEST['NAME'];
if ((!isset($ID) or trim($ID) =='') and isset($_REQUEST['NAME'])) {
	$ID = $_REQUEST['NAME'];}
if (isset($ID) and trim($ID) != '') {
	$editUser = new account();
	$editUser->select(null,array('WHERE'=>array('ID#'=>$ID)));
	if ($editRow = $editUser->getnext()) {
		$action = 'Edit';
		$passwordText = 'Password is optional.  If not provided the current password is kept.  ';
		$passwordReqd = '&nbsp;';
	} else {
		$passwordText = '';
		$action = 'Add';
		$passwordReqd = '*';
	}
} else {
	$action = 'Add';
	$editRow = array();
	$passwordText = '';
	$passwordReqd = '*';
}
$viewTitle = "$action User";
if (file_exists($_SESSION[APPLICATION]['module'].'/includes/view.php')) {
	include ($_SESSION[APPLICATION]['module'].'/includes/view.php');
} else {
	if (file_exists('includes/view.php')) {
		include ('includes/view.php');
	} else {
		base::missingPage('view.php');
	}
}
?>