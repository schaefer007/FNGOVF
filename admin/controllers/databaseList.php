<?php 
require_once('classes/class.server.php');

$server = new server();
$server->select(array("DbName","DbDesc","'unavailable' as DbUser","'unavailable' as DbPwd"));
while ($databaseRow = $server->getnext()) {
	$databaseInfo[] = $databaseRow;
}
$viewTitle = 'Database Servers';
include 'includes/view.php';

if (isset($server)) {
	unset($server);
}

?>