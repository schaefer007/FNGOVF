<?php 
require_once('classes/class.server.php');

if (isset($_REQUEST['action'])) {
	$action = $_REQUEST['action'];
} else {
	$action = '';
}

$server = new server();
if (isset($_REQUEST['old_DBNAME']) and $_REQUEST['old_DBNAME'] != '') {
	$key = $_REQUEST['old_DBNAME'];
} else {
	$key = $_REQUEST['DBNAME'];
}
$server->select(array('DBNAME'),array('WHERE'=>array('DBNAME'=>$key)));
if ($server->getnext()) {
	if ($action != 'Delete') {
		$action = 'Edit';
		if (!isset($_REQUEST['old_DBNAME']) or $_REQUEST['old_DBNAME'] == '') {
			$_REQUEST['old_DBNAME'] = $_REQUEST['DBNAME'];
		}
	}
} else {
	if ($action != 'Delete') {
		$action = 'Add';
	} else {
		$errorText[] = 'Database not found.  Delete not possible.';
	}		
} 
if (isset($_REQUEST['DBUSER']) and empty($_REQUEST['DBUSER'])) {
	unset($_REQUEST['DBUSER']);
}
if (isset($_REQUEST['DBPWD']) and empty($_REQUEST['DBPWD'])) {
	unset($_REQUEST['DBPWD']);
}
if (isset($_REQUEST['DBPWD2']) and empty($_REQUEST['DBPWD2'])) {
	unset($_REQUEST['DBPWD2']);
}
if (isset($_REQUEST['DBUSER']) and (!isset($_REQUEST['DBPWD']) and !isset($_REQUEST['DBPWD2']))) {
	$errorText[] = "If User is specified a new password must also be provided.";
	unset($_REQUEST['DBUSER']);
}
if ((isset($_REQUEST['DBPWD']) and !isset($_REQUEST['DBPWD2'])) or
    (isset($_REQUEST['DBPWD2']) and !isset($_REQUEST['DBPWD'])) or 
	(isset($_REQUEST['DBPWD']) and isset($_REQUEST['DBPWD2']) and $_REQUEST['DBPWD'] != $_REQUEST['DBPWD2'])) {
	$errorText[] = "Passwords don't Match.";
	if (isset($_REQUEST['DBPWD'])) {
		unset($_REQUEST['DBPWD']);
	}
	if (isset($_REQUEST['DBPWD2'])) {
		unset($_REQUEST['DBPWD2']);
	}
}
if (count($errorText) == 0) {
	switch ($action) {
		case 'Add'   	: 	if ($server->insert()) {
						 	 	$errorText[] = 'Database Successfully Added.';
						 	 	$view = 'databaseList';
						 	 	$_REQUEST['view'] = 'databaseList';
							} else {
								$errorText[] = 'Database Add Failed.';
							}
							break;
		case 'Edit'		:	if ($server->update()) {
								$errorText[] = 'Database Successfully Updated.';
								$view = 'databaseList';
								$_REQUEST['view'] = 'databaseList';
							} else {
								$errorText[] = 'Database Update Failed.';
							}
							break;
		case 'Delete'	:   if ($server->delete()) {
								$errorText[] = 'Database Successfully deleted.';
								$view = 'databaseList';
								$_REQUEST['view'] = 'databaseList';
								break;
		}
	}
}
if (isset($server)) {
	unset($server);
}
?>