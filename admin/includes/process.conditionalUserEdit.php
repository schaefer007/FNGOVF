<?php
require_once('classes/class.tblconditionaluser.php');

if (isset($_REQUEST['formName'])) {
	$formName = $_REQUEST['formName'];
} else {
	$formName = '';
}

$errorText = array();

if ($formName == 'conditionalUser') {
	$conditionalUserDb = new tblconditionaluser();
	if ($_REQUEST['intConditionalUserID'] and $_REQUEST['intConditionalUserID'] > 0) {
		$intConditionalUserID = $_REQUEST['intConditionalUserID'];
	}
	if (count($errorText) == 0) {
			$_REQUEST['LASTUPDDT'] = date('Y-m-d-H.i.s.'.'000000');
			$_REQUEST['LASTUPDBY'] = $_SESSION[APPLICATION]['user'];
			switch ($_REQUEST['action']) {
				case 'Edit':  if (isset($_REQUEST['cmd']) and $_REQUEST['cmd'] == 'Delete') {
								if (!$conditionalUserDb->delete()) {
									$errorText[] = 'Delete of conditional user failed';
								}
							  } else {
							  	if (!$conditionalUserDb->update()) {
							  		$errorText[] = 'Update of conditional user failed.';
							  	}
							  }
				 			   break;
				case 'Add':	 if (!isset($intConditionalUserID)) {
								$conditionalUserSeq = new Db();
								$conditionalUserSeq->connect($cxn['*LOCAL']);
								$_REQUEST['intConditionalUserID'] = $conditionalUserSeq->getNextSequence($config['schema'], '"conditionalUserSeq"');
									if (isset($conditionalUserSeq)) {
										unset($conditionalUserSeq);
									}
						   	  }
							  if ($conditionalUserDb->insert()) {
						       } else {
						   		$errorText[] = 'Add of conditional user failed.';
						  	   }
							   break;
							}
		}
	if (isset($conditionalUserDb)) {
		unset($conditionalUserDb);
	}
}
?>