<?php
require_once('classes/class.account.php');
require_once('classes/class.tbluser.php');
require_once('classes/class.tbluserrolexr.php');
require_once('classes/class.mail.php');
require_once('classes/class.userfacilityxr.php');

function updateTblUser($EmailAddress) {
	global $errorText;
	global $userDb;
	$tblUser = new tbluser();
	$tblUser->select(array('"intUserID"'),array('WHERE'=>array('strEmail'=>$EmailAddress)));
	if ($tblUserRow = $tblUser->getnext()) {
		$_REQUEST['intUserID'] = $tblUserRow['intUserID'];
	}
	$_REQUEST['strFirstName'] = $_REQUEST['FNAME'];
	$_REQUEST['strLastName']  = $_REQUEST['LNAME'];
	$_REQUEST['strEmail']     = $_REQUEST['EMAIL'];
	$_REQUEST['strDisplayName']= trim($_REQUEST['FNAME']).' '.trim($_REQUEST['LNAME']);
	$_REQUEST['strLoginName'] = $_REQUEST['EMAIL'];
	$_REQUEST['blnTimeTracking'] = 0;
	if (isset($_REQUEST['PASSWORD1']) and !empty($_REQUEST['PASSWORD1'])) {
		$_REQUEST['strPassword'] = md5($_REQUEST['PASSWORD1']);
	} else {
		$_REQUEST['strPassword'] = md5('a2qjsicl58c03');
	}

	if (!isset($_REQUEST['intUserID'])) {
		$success = $tblUser->insert();
		if ($success) {
			$errorText[] = 'User successfully activated with a status of '.$_REQUEST['STATUS'];
		} else {
			$errorText[] = 'User activation with a Status of '.$_REQUEST['STATUS'].' failed.';
		}
	} else {
		$success = $tblUser->update();
		if (!$success) {
			$errorText[] = 'User update failed on table "tbluser".';
		}
	}
	return $success;
}
if (isset($_REQUEST['formName'])) {
	$formName = $_REQUEST['formName'];
} else {
	$formName = '';
}

$errorText = array();

if ($formName == 'user') {
	$userDb = new account();
	if ($_REQUEST['ID#'] and $_REQUEST['ID#'] > 0) {
		$ID = $_REQUEST['ID#'];
	}
	if ($_REQUEST['action'] == 'Reset') {
		include('includes/process.pwdReset.php');
	} else {
		if ($_REQUEST['PASSWORD1']) {
			$Password1 = $_REQUEST['PASSWORD1'];
		} else {
			$Password1 = "";
		}
		if ($_REQUEST['PASSWORD2']) {
			$Password2 = $_REQUEST['PASSWORD2'];
		} else {
			$Password2 = "";
		}
		if ($Password1 or $Password2) {
			if ($Password1 == $Password2) {
				if (strlen($Password1) < 5) {
					$errorText[] = 'Password must contain between 5 and 10 characters';
				} else {
					$_REQUEST['PASSWORD'] = base::hashPassword($Password1);
				}
			} else {
				$errorText[] = 'Passwords do not match.';
			}
		}
		$dupCount = 0;
		//$userDb->select(array('ID#'),array('WHERE'=>array('EMAIL'=>$_REQUEST['EMAIL'])));
		$userDb->select(array('ID#'),null,"WHERE lcase(EMAIL) = '".strtolower(trim($_REQUEST['EMAIL']))."'");
		while($chkRow = $userDb->getnext()) {
			if ($chkRow['ID#'] != $_REQUEST['ID#']) {
				$dupCount ++;
			}
		}
		if ($dupCount > 0) {
			$errorText[] = "A user already exists with that Email address. {$_REQUEST['action']} not processed.";
		}
		if (count($errorText) == 0) {
			$_REQUEST['LASTUPDDT'] = date('Y-m-d-H.i.s.'.'000000');
			$_REQUEST['LASTUPDBY'] = $_SESSION[APPLICATION]['user'];
			switch ($_REQUEST['action']) {
				case 'Edit':  $userDb->select(null,array('WHERE'=>array('ID#'=>$_REQUEST['ID#'])));
					if (!$userRow = $userDb->getnext()) {
						$userRow = array();
					}
					if ($_REQUEST['cmd'] == 'Delete') {
						if (!$userDb->delete()) {
							$errorText[] = 'Delete of user failed.';
						} else {
							$view = 'userList';
						}
					} else {
						if ($userDb->update()) {
							if(isset($_REQUEST['STATUS']) and $_REQUEST['STATUS'] != 'P') {
								updateTblUser($userRow['EMAIL']);
								if ($userRow['STATUS'] == 'P' and $_REQUEST['STATUS'] == 'A') {
									$userName = trim($_REQUEST['FNAME']).' '.trim($_REQUEST['LNAME']);
									$email = new email();
									$email->setType('HTML');
									$email->addSubject('Account Approval');
									$email->addSender($config['defaultEmail']);
									$email->addRecipient($config['defaultEmail']);
									$message = file_get_contents('includes/emailHeader.php');
									$message = str_replace('"styles/','"'.$config['webURL'].'styles/',$message);
									$message = str_replace('"js/','"'.$config['webURL'].'js/',$message);
									$message = str_replace('"media/','"'.$config['webURL'].'media/',$message);
									$message .= "<p>The account for $userName at <a href=\"".$config['webURL']."\">Flex-n-gate Forms Portal</a> has been activated by {$_SESSION[APPLICATION]['user']}.  Click the link provided to access the site.</p>\n";
									$message .= file_get_contents('includes/emailFooter.php');
									$email->addMessage($message);
									$email->send();
								}
							}
						} else {
							$errorText[] = 'Update of user failed.';
						}
					}
					break;
				case 'Add':	 if (!isset($ID)) {
					$userSeq = new Db();
					$userSeq->connect($cxn['*LOCAL']);
					$_REQUEST['ID#'] = $userSeq->getNextSequence($config['schema'], 'userSeq');
					if (isset($userSeq)) {
						unset($userSeq);
					}
				}
					if ($userDb->insert()) {
						if(isset($_REQUEST['STATUS']) and $_REQUEST['STATUS'] != 'P') {
							updateTblUser($userRow['EMAIL']);
							if ($userRow['STATUS'] == 'P' and $_REQUEST['STATUS'] == 'A') {
								$userName = trim($_REQUEST['FNAME']).' '.trim($_REQUEST['LNAME']);
								$email = new email();
								$email->setType('HTML');
								$email->addSubject('Account Approval');
								$email->addSender($config['defaultEmail']);
								$email->addRecipient($config['defaultEmail']);
								$message = file_get_contents('includes/emailHeader.php');
								$message = str_replace('"styles/','"'.$config['webURL'].'styles/',$message);
								$message = str_replace('"js/','"'.$config['webURL'].'js/',$message);
								$message = str_replace('"media/','"'.$config['webURL'].'media/',$message);
								$message .= "<p>The account for $userName at <a href=\"".$config['webURL']."\">Flex-n-gate Forms Portal</a> has been activated by {$_SESSION[APPLICATION]['user']}.  Click the link provided to access the site.</p>\n";
								$message .= file_get_contents('includes/emailFooter.php');
								$email->addMessage($message);
								$email->send();
							}
						}
					} else {
						$errorText[] = 'Add of user failed.';
					}
					break;
			}
			$_REQUEST['PASSWORD1'] = "";
			$_REQUEST['PASSWORD2'] = "";
			$_REQUEST['NAME'] = $_REQUEST['ID#'];
		}
	}
	if (isset($userDb)) {
		unset($userDb);
	}

	if (isset($tblUser)) {
		unset($tblUser);
	}
}
if ($formName == 'userRoles') {
	if (isset($_REQUEST['oldRoles'])) {
		$oldRoles = explode(',',$_REQUEST['oldRoles']);
	} else {
		$oldRoles = array();
	}
	if (isset($_REQUEST['roleId'])) {
		$roleId = $_REQUEST['roleId'];
	} else {
		$roleId = array();
	}
	if (isset($_REQUEST['ID#'])) {
		$ID = $_REQUEST['ID#'];
	}
	if (isset($ID) and isset($roleId)) {
		$userRole = new tbluserrolexr();

		foreach($roleId as $role) {
			if (!in_array($role,$oldRoles)) {
				$_REQUEST['intUserID'] = $ID;
				$_REQUEST['intRoleID'] = $role;
				$userRole->insert();
			}
		}
		foreach($oldRoles as $oldRole) {
			if ($oldRole != '' and !in_array($oldRole,$roleId)) {
				$userRole->deleteAll(array('WHERE'=>array('intUserID'=>$ID,'intRoleID'=>$oldRole)));
			}
		}
	}
}
if ($formName == 'userFacilities') {
	if (isset($_REQUEST['oldFacilities'])) {
		$oldFacilities = explode(',',$_REQUEST['oldFacilities']);
	} else {
		$oldFacilities = array();
	}
	if (isset($_REQUEST['facilityId'])) {
		$facilityId = $_REQUEST['facilityId'];
	} else {
		$facilityId = array();
	}
	if (isset($_REQUEST['ID#'])) {
		$ID = $_REQUEST['ID#'];
	}
	if (isset($ID) and isset($facilityId)) {
		$userFacilities = new userfacilityxr();
		foreach($facilityId as $facility) {
			if (!in_array($facility,$oldFacilities)) {
				$_REQUEST['userID'] = $ID;
				$facilityArray = explode(':',$facility);
				if (count($facilityArray) == 3) {
					$_REQUEST['DBase'] = $facilityArray[0];
					$_REQUEST['Plant'] = $facilityArray[1];
					$_REQUEST['Company'] = $facilityArray[2];
					$userFacilities->insert();
				}
			}
		}
		foreach($oldFacilities as $facility) {
			if ($facility != '' and !in_array($facility,$facilityId)) {
				$facilityArray = explode(':',$facility);
				$userFacilities->deleteAll(array('WHERE'=>array('userID'=>$ID,'DBase'=>$facilityArray[0],'Plant'=>$facilityArray[1],'Company'=>$facilityArray[2])));
			}
		}
	}

}
?>