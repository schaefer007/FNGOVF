<?php 
$configFile = "includes/c_config.php";
$configData = "<?php\n";
$out_schema = $_REQUEST['schema'];
$configData .= '$config[\'schema\'] = \''.$out_schema.'\';'."\n";
$out_applicationRoot = $_REQUEST['applicationRoot'];
$configData .= '$config[\'applicationRoot\'] = \''.$out_applicationRoot.'\';'."\n";
$out_defaultEmail = $_REQUEST['defaultEmail'];
$configData .= '$config[\'defaultEmail\'] = \''.$out_defaultEmail.'\';'."\n";
foreach($availableForms as $formText=>$formController) {
	if (isset($_REQUEST['mail_'.$formController])) {
		$out_formEmail[$formController] = $_REQUEST['mail_'.$formController];
		$configData .= '$config[\'formEmail\'][\''.$formController.'\'] = \''.$out_formEmail[$formController].'\';'."\n";
	}
}
$out_loginattemptsallowed = $_REQUEST['loginattemptsallowed'];
$configData .= '$config[\'loginattemptsallowed\'] = \''.$out_loginattemptsallowed.'\';'."\n";
$out_webURL = $_REQUEST['webURL'];
if (trim($out_webURL) != "" and substr($out_webURL,strlen(trim($out_webURL))-1,1) != '/') {
	$out_webURL = trim($out_webURL).'/';
} 
$configData .= '$config[\'webURL\'] = \''.$out_webURL.'\';'."\n";
$out_mailHost = $_REQUEST['mailHost'];
if ($_REQUEST['mailSSl'] == 'Y') {
	$out_mailSSl = 'true';
	$out_mailHost = 'ssl://'.$out_mailHost;
} else {
	$out_mailSSl = 'false';
}
$out_mailPort = $_REQUEST['mailPort'];
$out_mailFrom = $_REQUEST['mailFrom'];
$out_mailUser = $_REQUEST['mailUser'];
$out_mailPwd = $_REQUEST['mailPwd'];
if ($_REQUEST['mailAuth'] == 'Y') {
	$out_mailAuth = 'true';
} else {
	$out_mailAuth = 'false';
}
$configData .= '$config[\'mail\'] = array(\'from\'=>\''.$out_mailFrom.'\',\'auth\'=>'.$out_mailAuth.',\'host\'=>\''.$out_mailHost.'\',\'port\'=>'.$out_mailPort.',\'username\'=>\''.$out_mailUser.'\',\'password\'=>\''.$out_mailPwd.'\',\'ssl\'=>'.$out_mailSSl.');'."\n";
$delayTime = $_REQUEST['delayTime'];
$configData .= "\$config['delayTime'] = '$delayTime';\n";
$email_first = $_REQUEST['email_first'];
$email_second = $_REQUEST['email_second'];
$email_third = $_REQUEST['email_third'];
$configData .= "\$config['email'] = array('first'=>'$email_first','second'=>'$email_second','third'=>'$email_third');\n";
if ($handle = fopen($configFile, 'w')) {
	if (fwrite($handle, $configData)) {
	  fclose($handle);
	  $config['schema'] = $out_schema;
	  $config['loginattemptsallowed'] = $out_loginattemptsallowed;
	  $config['webURL'] = $out_webURL;
	  $config['applicationRoot'] = $out_applicationRoot;
	  $config['defaultEmail'] = $out_defaultEmail;
	  foreach($out_formEmail as $formController=>$formEmail) {
	  	$config['formEmail'][$formController] = $formEmail;
	  }
	  if ($out_mailAuth == 'true') {
	  	$out_mailAuth = true;
	  } else {
	  	$out_mailAuth = false;
	  }
	  if ($out_mailSSl == 'true') {
	  	$out_mailSSl = true;
	  } else {
	  	$out_mailSSl = false;
	  }
	  $config['mail'] = array('from'=>$out_mailFrom,'auth'=>$out_mailAuth,'host'=>$out_mailHost,'port'=>$out_mailPort,'username'=>$out_mailUser,'password'=>$out_mailPwd,'ssl'=>$out_mailSSl);
	  $config['delayTime'] = $delayTime;
	  $config['email'] = array('first'=>$email_first,'second'=>$email_second,'third'=>$email_third);
	  $errorText[] = "Data successfully saved.";
	} else {
		$errorText[] = "Error occured saving data.";	
	}
} else {
	$error_last = error_get_last();
	$errorText[] = "Sorry, there was an issue saving the configuration file.";
	$errorText[] = $error_last['message'];
}
?>